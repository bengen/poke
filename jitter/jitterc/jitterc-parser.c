/* A Bison parser, made by GNU Bison 3.6.2.  */

/* Bison implementation for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015, 2018-2020 Free Software Foundation,
   Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* DO NOT RELY ON FEATURES THAT ARE NOT DOCUMENTED in the manual,
   especially those whose name start with YY_ or yy_.  They are
   private implementation details that can be changed or removed.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "3.6.2"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 1

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1

/* Substitute the type names.  */
#define YYSTYPE         JITTERC_STYPE
#define YYLTYPE         JITTERC_LTYPE
/* Substitute the variable and function names.  */
#define yyparse         jitterc_parse
#define yylex           jitterc_lex
#define yyerror         jitterc_error
#define yydebug         jitterc_debug
#define yynerrs         jitterc_nerrs

/* First part of user prologue.  */
#line 27 "../../jitter/jitterc/jitterc.y"

/* Include the Gnulib header. */
#include <config.h>

#include <stdio.h>
#include <ctype.h>
#include <c-ctype.h>
#include <jitter/jitter-bitwise.h>
#include <jitter/jitter-malloc.h>
#include <jitter/jitter-fatal.h>
#include <jitter/jitter-parse-int.h>
#include <jitter/jitter-string.h>
#include <gl_xlist.h>
#include <gl_array_list.h>

#include "jitterc-vm.h"
#include "jitterc-mangle.h"
#include "jitterc-rewrite.h"
#include "jitterc-utility.h"
#include "jitterc-parser.h"
#include "jitterc-scanner.h"

/* This is currently a fatal error.  I could longjmp away instead. */
static void
jitterc_error (YYLTYPE *locp, struct jitterc_vm *vm,
               yyscan_t scanner, char *message)
  __attribute__ ((noreturn));

#define JITTERC_PARSE_ERROR(message)                      \
  do                                                      \
    {                                                     \
      jitterc_error (jitterc_get_lloc (jitterc_scanner),  \
                       vm, jitterc_scanner, message);     \
    }                                                     \
  while (false)

/* Set the given property of the last instruction to the given enum case,
   checking that each property is not set more than once.  This is useful
   for enumerate-valued properties such as hotness and relocatability. */
#define JITTERC_SET_PROPERTY(property, value)                  \
  do                                                           \
    {                                                          \
      enum jitterc_ ## property *property                      \
        = & jitterc_vm_last_instruction (vm)->property;        \
      if (* property != jitterc_ ## property ## _unspecified)  \
        JITTERC_PARSE_ERROR("duplicate " # property);          \
      * property = jitterc_ ## property ## _ ## value;         \
    }                                                          \
  while (false)

/* What would be yytext in a non-reentrant scanner. */
#define JITTERC_TEXT \
  (jitterc_get_text (jitterc_scanner))

 /* What would be yylineno in a non-reentrant scanner. */
#define JITTERC_LINENO \
  (jitterc_get_lineno (jitterc_scanner))

/* A copy of what would be yytext in a non-reentrant scanner. */
#define JITTERC_TEXT_COPY \
  (jitter_clone_string (JITTERC_TEXT))

/* Assign the given lvalue with a string concatenation of its current value and
   the new string from the code block, preceded by a #line CPP directive unless
   #line-generation was disabled.  Free both strings (but not the pointed
   struct, which normally comes from internal Bison data structures). */
#define JITTERC_APPEND_CODE(lvalue, code_block_pointerq)                        \
  do                                                                            \
    {                                                                           \
       struct jitterc_code_block *code_block_pointer = code_block_pointerq;     \
       int line_number = code_block_pointer->line_number;                       \
       char *new_code = code_block_pointer->code;                               \
       char *line_line = xmalloc (strlen (vm->source_file_name) + 100);         \
       if (vm->generate_line)                                                   \
         sprintf (line_line, "#line %i \"%s\"\n",                               \
                  line_number,                                                  \
                  vm->source_file_name);                                        \
       else                                                                     \
         line_line [0] = '\0';                                                  \
       size_t line_line_length = strlen (line_line);                            \
       size_t lvalue_length = strlen (lvalue);                                  \
       char *concatenation                                                      \
         = xrealloc (lvalue,                                                    \
                     lvalue_length + line_line_length                           \
                     + strlen (new_code) + 1);                                  \
       strcpy (concatenation + lvalue_length,                                   \
               line_line);                                                      \
       strcpy (concatenation + lvalue_length + line_line_length,                \
               new_code);                                                       \
       free (line_line);                                                        \
       free (new_code);                                                         \
       /* Poison the pointer still in the struct, just for defensiveness. */    \
       code_block_pointer->code = NULL;                                         \
       lvalue = concatenation;                                                  \
    }                                                                           \
  while (false)                                                                 \

/* FIXME: unfactor this code back into the only rule which should need it. */
#define KIND_CASE(character, suffix)                      \
  case character:                                         \
    if (k & jitterc_instruction_argument_kind_ ## suffix) \
      JITTERC_PARSE_ERROR("duplicate " #suffix " kind");  \
    k |= jitterc_instruction_argument_kind_ ## suffix;    \
  break;
#define KIND_CASE_DEFAULT(out, character)                   \
  default:                                                  \
    if (c_isupper (character))                                \
      {                                                     \
        if (k & jitterc_instruction_argument_kind_register) \
          JITTERC_PARSE_ERROR("duplicate register kind");   \
        k |= jitterc_instruction_argument_kind_register;    \
        out.register_class_letter = tolower (character);    \
      }                                                     \
    else                                                    \
      JITTERC_PARSE_ERROR("invalid kind letter");


#line 196 "../../jitter/jitterc/jitterc-parser.c"

# ifndef YY_CAST
#  ifdef __cplusplus
#   define YY_CAST(Type, Val) static_cast<Type> (Val)
#   define YY_REINTERPRET_CAST(Type, Val) reinterpret_cast<Type> (Val)
#  else
#   define YY_CAST(Type, Val) ((Type) (Val))
#   define YY_REINTERPRET_CAST(Type, Val) ((Type) (Val))
#  endif
# endif
# ifndef YY_NULLPTR
#  if defined __cplusplus
#   if 201103L <= __cplusplus
#    define YY_NULLPTR nullptr
#   else
#    define YY_NULLPTR 0
#   endif
#  else
#   define YY_NULLPTR ((void*)0)
#  endif
# endif

/* Use api.header.include to #include this header
   instead of duplicating it here.  */
#ifndef YY_JITTERC_JITTER_JITTERC_JITTERC_PARSER_H_INCLUDED
# define YY_JITTERC_JITTER_JITTERC_JITTERC_PARSER_H_INCLUDED
/* Debug traces.  */
#ifndef JITTERC_DEBUG
# if defined YYDEBUG
#if YYDEBUG
#   define JITTERC_DEBUG 1
#  else
#   define JITTERC_DEBUG 0
#  endif
# else /* ! defined YYDEBUG */
#  define JITTERC_DEBUG 0
# endif /* ! defined YYDEBUG */
#endif  /* ! defined JITTERC_DEBUG */
#if JITTERC_DEBUG
extern int jitterc_debug;
#endif
/* "%code requires" blocks.  */
#line 175 "../../jitter/jitterc/jitterc.y"

/* The value associated to a bare_argument nonterminal -- which is to say, an
   argument without a mode.  This is only used within the parser, but needs go
   the header as well as one of the %type cases. */
struct jitterc_bare_argument
{
  /* The argument kind. */
  enum jitterc_instruction_argument_kind kind;

  /* The register letter, lower-case.  Only meaningful if the kind contains the
     register case. */
  char register_class_letter;
};

/* A code block to copy in the output.  This is only used within the parser, but
   needs go the header as well as one of the %type cases. */
struct jitterc_code_block
{
  /* The line number where the code block begins, in the Jitter VM specification
     file.  This is useful for friendlier error reporting thru the #line CPP
     feature. */
  int line_number;

  /* A malloc-allocated string. */
  char *code;
};

/* Simplified error-reporting facilities calling jitterc_error, suitable to be
   called from the scanner and the parser without the complicated and
   irrelevant parameters needed by jitterc_error . */
void
jitterc_scan_error (void *jitterc_scanner) __attribute__ ((noreturn));

/* Return a pointer to a fresh VM data structure parsed from the pointed stream,
   or fail fatally.  Don't generate #line directives iff generate_line is false.
   Rationale: unfortunately some C code generation already happens in the
   parser, so generate_line must be supplied early. */
struct jitterc_vm *
jitterc_parse_file_star (FILE *input_file, bool generate_line);

/* Like jitterc_parse_file_star, but parsing from a file whose pathname is
   given. */
struct jitterc_vm *
jitterc_parse_file (const char *input_file_name, bool generate_line);

#line 285 "../../jitter/jitterc/jitterc-parser.c"

/* Token kinds.  */
#ifndef JITTERC_TOKENTYPE
# define JITTERC_TOKENTYPE
  enum jitterc_tokentype
  {
    JITTERC_EMPTY = -2,
    JITTERC_EOF = 0,               /* "end of file"  */
    JITTERC_error = 256,           /* error  */
    JITTERC_UNDEF = 257,           /* "invalid token"  */
    VM = 258,                      /* VM  */
    END = 259,                     /* END  */
    CODE = 260,                    /* CODE  */
    STRING = 261,                  /* STRING  */
    SET = 262,                     /* SET  */
    INITIAL_HEADER_C = 263,        /* INITIAL_HEADER_C  */
    INITIAL_VM1_C = 264,           /* INITIAL_VM1_C  */
    INITIAL_VM2_C = 265,           /* INITIAL_VM2_C  */
    INITIAL_VM_MAIN_C = 266,       /* INITIAL_VM_MAIN_C  */
    EARLY_HEADER_C = 267,          /* EARLY_HEADER_C  */
    LATE_HEADER_C = 268,           /* LATE_HEADER_C  */
    PRINTER_C = 269,               /* PRINTER_C  */
    REWRITER_C = 270,              /* REWRITER_C  */
    EARLY_C = 271,                 /* EARLY_C  */
    LATE_C = 272,                  /* LATE_C  */
    INITIALIZATION_C = 273,        /* INITIALIZATION_C  */
    FINALIZATION_C = 274,          /* FINALIZATION_C  */
    STATE_EARLY_C = 275,           /* STATE_EARLY_C  */
    STATE_BACKING_STRUCT_C = 276,  /* STATE_BACKING_STRUCT_C  */
    STATE_RUNTIME_STRUCT_C = 277,  /* STATE_RUNTIME_STRUCT_C  */
    STATE_INITIALIZATION_C = 278,  /* STATE_INITIALIZATION_C  */
    STATE_FINALIZATION_C = 279,    /* STATE_FINALIZATION_C  */
    INSTRUCTION_BEGINNING_C = 280, /* INSTRUCTION_BEGINNING_C  */
    INSTRUCTION_END_C = 281,       /* INSTRUCTION_END_C  */
    BARE_ARGUMENT = 282,           /* BARE_ARGUMENT  */
    IDENTIFIER = 283,              /* IDENTIFIER  */
    WRAPPED_FUNCTIONS = 284,       /* WRAPPED_FUNCTIONS  */
    WRAPPED_GLOBALS = 285,         /* WRAPPED_GLOBALS  */
    INSTRUCTION = 286,             /* INSTRUCTION  */
    OPEN_PAREN = 287,              /* OPEN_PAREN  */
    CLOSE_PAREN = 288,             /* CLOSE_PAREN  */
    COMMA = 289,                   /* COMMA  */
    SEMICOLON = 290,               /* SEMICOLON  */
    IN = 291,                      /* IN  */
    OUT = 292,                     /* OUT  */
    RULE = 293,                    /* RULE  */
    WHEN = 294,                    /* WHEN  */
    REWRITE = 295,                 /* REWRITE  */
    INTO = 296,                    /* INTO  */
    TRUE_ = 297,                   /* TRUE_  */
    FALSE_ = 298,                  /* FALSE_  */
    RULE_PLACEHOLDER = 299,        /* RULE_PLACEHOLDER  */
    HOT = 300,                     /* HOT  */
    COLD = 301,                    /* COLD  */
    RELOCATABLE = 302,             /* RELOCATABLE  */
    NON_RELOCATABLE = 303,         /* NON_RELOCATABLE  */
    CALLER = 304,                  /* CALLER  */
    CALLEE = 305,                  /* CALLEE  */
    COMMUTATIVE = 306,             /* COMMUTATIVE  */
    NON_COMMUTATIVE = 307,         /* NON_COMMUTATIVE  */
    TWO_OPERANDS = 308,            /* TWO_OPERANDS  */
    REGISTER_CLASS = 309,          /* REGISTER_CLASS  */
    FAST_REGISTER_NO = 310,        /* FAST_REGISTER_NO  */
    REGISTER_OR_STACK_LETTER = 311, /* REGISTER_OR_STACK_LETTER  */
    SLOW_REGISTERS = 312,          /* SLOW_REGISTERS  */
    NO_SLOW_REGISTERS = 313,       /* NO_SLOW_REGISTERS  */
    STACK = 314,                   /* STACK  */
    C_TYPE = 315,                  /* C_TYPE  */
    C_INITIAL_VALUE = 316,         /* C_INITIAL_VALUE  */
    C_ELEMENT_TYPE = 317,          /* C_ELEMENT_TYPE  */
    LONG_NAME = 318,               /* LONG_NAME  */
    ELEMENT_NO = 319,              /* ELEMENT_NO  */
    NON_TOS_OPTIMIZED = 320,       /* NON_TOS_OPTIMIZED  */
    TOS_OPTIMIZED = 321,           /* TOS_OPTIMIZED  */
    NO_GUARD_OVERFLOW = 322,       /* NO_GUARD_OVERFLOW  */
    NO_GUARD_UNDERFLOW = 323,      /* NO_GUARD_UNDERFLOW  */
    GUARD_OVERFLOW = 324,          /* GUARD_OVERFLOW  */
    GUARD_UNDERFLOW = 325,         /* GUARD_UNDERFLOW  */
    FIXNUM = 326,                  /* FIXNUM  */
    BITSPERWORD = 327,             /* BITSPERWORD  */
    BYTESPERWORD = 328,            /* BYTESPERWORD  */
    LGBYTESPERWORD = 329           /* LGBYTESPERWORD  */
  };
  typedef enum jitterc_tokentype jitterc_token_kind_t;
#endif
/* Token kinds.  */
#define JITTERC_EOF 0
#define JITTERC_error 256
#define JITTERC_UNDEF 257
#define VM 258
#define END 259
#define CODE 260
#define STRING 261
#define SET 262
#define INITIAL_HEADER_C 263
#define INITIAL_VM1_C 264
#define INITIAL_VM2_C 265
#define INITIAL_VM_MAIN_C 266
#define EARLY_HEADER_C 267
#define LATE_HEADER_C 268
#define PRINTER_C 269
#define REWRITER_C 270
#define EARLY_C 271
#define LATE_C 272
#define INITIALIZATION_C 273
#define FINALIZATION_C 274
#define STATE_EARLY_C 275
#define STATE_BACKING_STRUCT_C 276
#define STATE_RUNTIME_STRUCT_C 277
#define STATE_INITIALIZATION_C 278
#define STATE_FINALIZATION_C 279
#define INSTRUCTION_BEGINNING_C 280
#define INSTRUCTION_END_C 281
#define BARE_ARGUMENT 282
#define IDENTIFIER 283
#define WRAPPED_FUNCTIONS 284
#define WRAPPED_GLOBALS 285
#define INSTRUCTION 286
#define OPEN_PAREN 287
#define CLOSE_PAREN 288
#define COMMA 289
#define SEMICOLON 290
#define IN 291
#define OUT 292
#define RULE 293
#define WHEN 294
#define REWRITE 295
#define INTO 296
#define TRUE_ 297
#define FALSE_ 298
#define RULE_PLACEHOLDER 299
#define HOT 300
#define COLD 301
#define RELOCATABLE 302
#define NON_RELOCATABLE 303
#define CALLER 304
#define CALLEE 305
#define COMMUTATIVE 306
#define NON_COMMUTATIVE 307
#define TWO_OPERANDS 308
#define REGISTER_CLASS 309
#define FAST_REGISTER_NO 310
#define REGISTER_OR_STACK_LETTER 311
#define SLOW_REGISTERS 312
#define NO_SLOW_REGISTERS 313
#define STACK 314
#define C_TYPE 315
#define C_INITIAL_VALUE 316
#define C_ELEMENT_TYPE 317
#define LONG_NAME 318
#define ELEMENT_NO 319
#define NON_TOS_OPTIMIZED 320
#define TOS_OPTIMIZED 321
#define NO_GUARD_OVERFLOW 322
#define NO_GUARD_UNDERFLOW 323
#define GUARD_OVERFLOW 324
#define GUARD_UNDERFLOW 325
#define FIXNUM 326
#define BITSPERWORD 327
#define BYTESPERWORD 328
#define LGBYTESPERWORD 329

/* Value type.  */
#if ! defined JITTERC_STYPE && ! defined JITTERC_STYPE_IS_DECLARED
union JITTERC_STYPE
{
#line 222 "../../jitter/jitterc/jitterc.y"

  char character;
  char* string;
  gl_list_t string_list;
  enum jitterc_instruction_argument_mode mode;
  struct jitterc_bare_argument bare_argument;
  jitter_int fixnum;
  bool boolean;
  struct jitterc_code_block code_block;

  struct jitterc_argument_pattern *argument_pattern;
  struct jitterc_template_expression *template_expression;
  struct jitterc_instruction_pattern *instruction_pattern;
  struct jitterc_instruction_template *instruction_template;

  /* List elements are pointers to struct jitterc_argument_pattern . */
  gl_list_t argument_patterns;

  /* List elements are pointers to struct jitterc_template_expression . */
  gl_list_t template_expressions;

  /* List elements are pointers to struct jitterc_instruction_pattern . */
  gl_list_t instruction_patterns;

  /* List elements are pointers to struct jitterc_instruction_template . */
  gl_list_t instruction_templates;

  /* Register-class section contents and stack section contents consist in
     pointers to VM structs holding all the data. */
  struct jitterc_register_class *register_class;
  struct jitterc_stack *stack;

#line 485 "../../jitter/jitterc/jitterc-parser.c"

};
typedef union JITTERC_STYPE JITTERC_STYPE;
# define JITTERC_STYPE_IS_TRIVIAL 1
# define JITTERC_STYPE_IS_DECLARED 1
#endif

/* Location type.  */
#if ! defined JITTERC_LTYPE && ! defined JITTERC_LTYPE_IS_DECLARED
typedef struct JITTERC_LTYPE JITTERC_LTYPE;
struct JITTERC_LTYPE
{
  int first_line;
  int first_column;
  int last_line;
  int last_column;
};
# define JITTERC_LTYPE_IS_DECLARED 1
# define JITTERC_LTYPE_IS_TRIVIAL 1
#endif



int jitterc_parse (struct jitterc_vm *vm, void* jitterc_scanner);

#endif /* !YY_JITTERC_JITTER_JITTERC_JITTERC_PARSER_H_INCLUDED  */
/* Symbol kind.  */
enum yysymbol_kind_t
{
  YYSYMBOL_YYEMPTY = -2,
  YYSYMBOL_YYEOF = 0,                      /* "end of file"  */
  YYSYMBOL_YYerror = 1,                    /* error  */
  YYSYMBOL_YYUNDEF = 2,                    /* "invalid token"  */
  YYSYMBOL_VM = 3,                         /* VM  */
  YYSYMBOL_END = 4,                        /* END  */
  YYSYMBOL_CODE = 5,                       /* CODE  */
  YYSYMBOL_STRING = 6,                     /* STRING  */
  YYSYMBOL_SET = 7,                        /* SET  */
  YYSYMBOL_INITIAL_HEADER_C = 8,           /* INITIAL_HEADER_C  */
  YYSYMBOL_INITIAL_VM1_C = 9,              /* INITIAL_VM1_C  */
  YYSYMBOL_INITIAL_VM2_C = 10,             /* INITIAL_VM2_C  */
  YYSYMBOL_INITIAL_VM_MAIN_C = 11,         /* INITIAL_VM_MAIN_C  */
  YYSYMBOL_EARLY_HEADER_C = 12,            /* EARLY_HEADER_C  */
  YYSYMBOL_LATE_HEADER_C = 13,             /* LATE_HEADER_C  */
  YYSYMBOL_PRINTER_C = 14,                 /* PRINTER_C  */
  YYSYMBOL_REWRITER_C = 15,                /* REWRITER_C  */
  YYSYMBOL_EARLY_C = 16,                   /* EARLY_C  */
  YYSYMBOL_LATE_C = 17,                    /* LATE_C  */
  YYSYMBOL_INITIALIZATION_C = 18,          /* INITIALIZATION_C  */
  YYSYMBOL_FINALIZATION_C = 19,            /* FINALIZATION_C  */
  YYSYMBOL_STATE_EARLY_C = 20,             /* STATE_EARLY_C  */
  YYSYMBOL_STATE_BACKING_STRUCT_C = 21,    /* STATE_BACKING_STRUCT_C  */
  YYSYMBOL_STATE_RUNTIME_STRUCT_C = 22,    /* STATE_RUNTIME_STRUCT_C  */
  YYSYMBOL_STATE_INITIALIZATION_C = 23,    /* STATE_INITIALIZATION_C  */
  YYSYMBOL_STATE_FINALIZATION_C = 24,      /* STATE_FINALIZATION_C  */
  YYSYMBOL_INSTRUCTION_BEGINNING_C = 25,   /* INSTRUCTION_BEGINNING_C  */
  YYSYMBOL_INSTRUCTION_END_C = 26,         /* INSTRUCTION_END_C  */
  YYSYMBOL_BARE_ARGUMENT = 27,             /* BARE_ARGUMENT  */
  YYSYMBOL_IDENTIFIER = 28,                /* IDENTIFIER  */
  YYSYMBOL_WRAPPED_FUNCTIONS = 29,         /* WRAPPED_FUNCTIONS  */
  YYSYMBOL_WRAPPED_GLOBALS = 30,           /* WRAPPED_GLOBALS  */
  YYSYMBOL_INSTRUCTION = 31,               /* INSTRUCTION  */
  YYSYMBOL_OPEN_PAREN = 32,                /* OPEN_PAREN  */
  YYSYMBOL_CLOSE_PAREN = 33,               /* CLOSE_PAREN  */
  YYSYMBOL_COMMA = 34,                     /* COMMA  */
  YYSYMBOL_SEMICOLON = 35,                 /* SEMICOLON  */
  YYSYMBOL_IN = 36,                        /* IN  */
  YYSYMBOL_OUT = 37,                       /* OUT  */
  YYSYMBOL_RULE = 38,                      /* RULE  */
  YYSYMBOL_WHEN = 39,                      /* WHEN  */
  YYSYMBOL_REWRITE = 40,                   /* REWRITE  */
  YYSYMBOL_INTO = 41,                      /* INTO  */
  YYSYMBOL_TRUE_ = 42,                     /* TRUE_  */
  YYSYMBOL_FALSE_ = 43,                    /* FALSE_  */
  YYSYMBOL_RULE_PLACEHOLDER = 44,          /* RULE_PLACEHOLDER  */
  YYSYMBOL_HOT = 45,                       /* HOT  */
  YYSYMBOL_COLD = 46,                      /* COLD  */
  YYSYMBOL_RELOCATABLE = 47,               /* RELOCATABLE  */
  YYSYMBOL_NON_RELOCATABLE = 48,           /* NON_RELOCATABLE  */
  YYSYMBOL_CALLER = 49,                    /* CALLER  */
  YYSYMBOL_CALLEE = 50,                    /* CALLEE  */
  YYSYMBOL_COMMUTATIVE = 51,               /* COMMUTATIVE  */
  YYSYMBOL_NON_COMMUTATIVE = 52,           /* NON_COMMUTATIVE  */
  YYSYMBOL_TWO_OPERANDS = 53,              /* TWO_OPERANDS  */
  YYSYMBOL_REGISTER_CLASS = 54,            /* REGISTER_CLASS  */
  YYSYMBOL_FAST_REGISTER_NO = 55,          /* FAST_REGISTER_NO  */
  YYSYMBOL_REGISTER_OR_STACK_LETTER = 56,  /* REGISTER_OR_STACK_LETTER  */
  YYSYMBOL_SLOW_REGISTERS = 57,            /* SLOW_REGISTERS  */
  YYSYMBOL_NO_SLOW_REGISTERS = 58,         /* NO_SLOW_REGISTERS  */
  YYSYMBOL_STACK = 59,                     /* STACK  */
  YYSYMBOL_C_TYPE = 60,                    /* C_TYPE  */
  YYSYMBOL_C_INITIAL_VALUE = 61,           /* C_INITIAL_VALUE  */
  YYSYMBOL_C_ELEMENT_TYPE = 62,            /* C_ELEMENT_TYPE  */
  YYSYMBOL_LONG_NAME = 63,                 /* LONG_NAME  */
  YYSYMBOL_ELEMENT_NO = 64,                /* ELEMENT_NO  */
  YYSYMBOL_NON_TOS_OPTIMIZED = 65,         /* NON_TOS_OPTIMIZED  */
  YYSYMBOL_TOS_OPTIMIZED = 66,             /* TOS_OPTIMIZED  */
  YYSYMBOL_NO_GUARD_OVERFLOW = 67,         /* NO_GUARD_OVERFLOW  */
  YYSYMBOL_NO_GUARD_UNDERFLOW = 68,        /* NO_GUARD_UNDERFLOW  */
  YYSYMBOL_GUARD_OVERFLOW = 69,            /* GUARD_OVERFLOW  */
  YYSYMBOL_GUARD_UNDERFLOW = 70,           /* GUARD_UNDERFLOW  */
  YYSYMBOL_FIXNUM = 71,                    /* FIXNUM  */
  YYSYMBOL_BITSPERWORD = 72,               /* BITSPERWORD  */
  YYSYMBOL_BYTESPERWORD = 73,              /* BYTESPERWORD  */
  YYSYMBOL_LGBYTESPERWORD = 74,            /* LGBYTESPERWORD  */
  YYSYMBOL_YYACCEPT = 75,                  /* $accept  */
  YYSYMBOL_vm = 76,                        /* vm  */
  YYSYMBOL_sections = 77,                  /* sections  */
  YYSYMBOL_section = 78,                   /* section  */
  YYSYMBOL_vm_section = 79,                /* vm_section  */
  YYSYMBOL_vm_section_contents = 80,       /* vm_section_contents  */
  YYSYMBOL_setting = 81,                   /* setting  */
  YYSYMBOL_c_section = 82,                 /* c_section  */
  YYSYMBOL_wrapped_functions_section = 83, /* wrapped_functions_section  */
  YYSYMBOL_wrapped_globals_section = 84,   /* wrapped_globals_section  */
  YYSYMBOL_identifiers = 85,               /* identifiers  */
  YYSYMBOL_rule_section = 86,              /* rule_section  */
  YYSYMBOL_optional_identifier = 87,       /* optional_identifier  */
  YYSYMBOL_rule_guard = 88,                /* rule_guard  */
  YYSYMBOL_rule_instruction_pattern = 89,  /* rule_instruction_pattern  */
  YYSYMBOL_rule_instruction_patterns_zero_or_more = 90, /* rule_instruction_patterns_zero_or_more  */
  YYSYMBOL_rule_instruction_patterns_one_or_more = 91, /* rule_instruction_patterns_one_or_more  */
  YYSYMBOL_rule_argument_pattern = 92,     /* rule_argument_pattern  */
  YYSYMBOL_placeholder = 93,               /* placeholder  */
  YYSYMBOL_optional_placeholder = 94,      /* optional_placeholder  */
  YYSYMBOL_rule_argument_patterns_zero_or_more = 95, /* rule_argument_patterns_zero_or_more  */
  YYSYMBOL_rule_argument_patterns_one_or_more = 96, /* rule_argument_patterns_one_or_more  */
  YYSYMBOL_rule_expressions_zero_or_more = 97, /* rule_expressions_zero_or_more  */
  YYSYMBOL_rule_expressions_one_or_more = 98, /* rule_expressions_one_or_more  */
  YYSYMBOL_rule_expression = 99,           /* rule_expression  */
  YYSYMBOL_rule_operation = 100,           /* rule_operation  */
  YYSYMBOL_rule_instruction_templates_zero_or_more = 101, /* rule_instruction_templates_zero_or_more  */
  YYSYMBOL_rule_instruction_template = 102, /* rule_instruction_template  */
  YYSYMBOL_register_class_section = 103,   /* register_class_section  */
  YYSYMBOL_register_class_section_contents = 104, /* register_class_section_contents  */
  YYSYMBOL_stack_section = 105,            /* stack_section  */
  YYSYMBOL_stack_section_contents = 106,   /* stack_section_contents  */
  YYSYMBOL_register_or_stack_letter = 107, /* register_or_stack_letter  */
  YYSYMBOL_instruction_section = 108,      /* instruction_section  */
  YYSYMBOL_109_1 = 109,                    /* $@1  */
  YYSYMBOL_arguments = 110,                /* arguments  */
  YYSYMBOL_one_or_more_arguments = 111,    /* one_or_more_arguments  */
  YYSYMBOL_argument = 112,                 /* argument  */
  YYSYMBOL_113_2 = 113,                    /* $@2  */
  YYSYMBOL_optional_printer_name = 114,    /* optional_printer_name  */
  YYSYMBOL_modes = 115,                    /* modes  */
  YYSYMBOL_modes_rest = 116,               /* modes_rest  */
  YYSYMBOL_mode_character = 117,           /* mode_character  */
  YYSYMBOL_bare_argument = 118,            /* bare_argument  */
  YYSYMBOL_properties = 119,               /* properties  */
  YYSYMBOL_hotness = 120,                  /* hotness  */
  YYSYMBOL_relocatability = 121,           /* relocatability  */
  YYSYMBOL_callingness = 122,              /* callingness  */
  YYSYMBOL_identifier = 123,               /* identifier  */
  YYSYMBOL_string = 124,                   /* string  */
  YYSYMBOL_code = 125,                     /* code  */
  YYSYMBOL_literal = 126,                  /* literal  */
  YYSYMBOL_literals = 127,                 /* literals  */
  YYSYMBOL_128_3 = 128                     /* $@3  */
};
typedef enum yysymbol_kind_t yysymbol_kind_t;




#ifdef short
# undef short
#endif

/* On compilers that do not define __PTRDIFF_MAX__ etc., make sure
   <limits.h> and (if available) <stdint.h> are included
   so that the code can choose integer types of a good width.  */

#ifndef __PTRDIFF_MAX__
# include <limits.h> /* INFRINGES ON USER NAME SPACE */
# if defined __STDC_VERSION__ && 199901 <= __STDC_VERSION__
#  include <stdint.h> /* INFRINGES ON USER NAME SPACE */
#  define YY_STDINT_H
# endif
#endif

/* Narrow types that promote to a signed type and that can represent a
   signed or unsigned integer of at least N bits.  In tables they can
   save space and decrease cache pressure.  Promoting to a signed type
   helps avoid bugs in integer arithmetic.  */

#ifdef __INT_LEAST8_MAX__
typedef __INT_LEAST8_TYPE__ yytype_int8;
#elif defined YY_STDINT_H
typedef int_least8_t yytype_int8;
#else
typedef signed char yytype_int8;
#endif

#ifdef __INT_LEAST16_MAX__
typedef __INT_LEAST16_TYPE__ yytype_int16;
#elif defined YY_STDINT_H
typedef int_least16_t yytype_int16;
#else
typedef short yytype_int16;
#endif

#if defined __UINT_LEAST8_MAX__ && __UINT_LEAST8_MAX__ <= __INT_MAX__
typedef __UINT_LEAST8_TYPE__ yytype_uint8;
#elif (!defined __UINT_LEAST8_MAX__ && defined YY_STDINT_H \
       && UINT_LEAST8_MAX <= INT_MAX)
typedef uint_least8_t yytype_uint8;
#elif !defined __UINT_LEAST8_MAX__ && UCHAR_MAX <= INT_MAX
typedef unsigned char yytype_uint8;
#else
typedef short yytype_uint8;
#endif

#if defined __UINT_LEAST16_MAX__ && __UINT_LEAST16_MAX__ <= __INT_MAX__
typedef __UINT_LEAST16_TYPE__ yytype_uint16;
#elif (!defined __UINT_LEAST16_MAX__ && defined YY_STDINT_H \
       && UINT_LEAST16_MAX <= INT_MAX)
typedef uint_least16_t yytype_uint16;
#elif !defined __UINT_LEAST16_MAX__ && USHRT_MAX <= INT_MAX
typedef unsigned short yytype_uint16;
#else
typedef int yytype_uint16;
#endif

#ifndef YYPTRDIFF_T
# if defined __PTRDIFF_TYPE__ && defined __PTRDIFF_MAX__
#  define YYPTRDIFF_T __PTRDIFF_TYPE__
#  define YYPTRDIFF_MAXIMUM __PTRDIFF_MAX__
# elif defined PTRDIFF_MAX
#  ifndef ptrdiff_t
#   include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  endif
#  define YYPTRDIFF_T ptrdiff_t
#  define YYPTRDIFF_MAXIMUM PTRDIFF_MAX
# else
#  define YYPTRDIFF_T long
#  define YYPTRDIFF_MAXIMUM LONG_MAX
# endif
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif defined __STDC_VERSION__ && 199901 <= __STDC_VERSION__
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned
# endif
#endif

#define YYSIZE_MAXIMUM                                  \
  YY_CAST (YYPTRDIFF_T,                                 \
           (YYPTRDIFF_MAXIMUM < YY_CAST (YYSIZE_T, -1)  \
            ? YYPTRDIFF_MAXIMUM                         \
            : YY_CAST (YYSIZE_T, -1)))

#define YYSIZEOF(X) YY_CAST (YYPTRDIFF_T, sizeof (X))


/* Stored state numbers (used for stacks). */
typedef yytype_uint8 yy_state_t;

/* State numbers in computations.  */
typedef int yy_state_fast_t;

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(Msgid) dgettext ("bison-runtime", Msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(Msgid) Msgid
# endif
#endif


#ifndef YY_ATTRIBUTE_PURE
# if defined __GNUC__ && 2 < __GNUC__ + (96 <= __GNUC_MINOR__)
#  define YY_ATTRIBUTE_PURE __attribute__ ((__pure__))
# else
#  define YY_ATTRIBUTE_PURE
# endif
#endif

#ifndef YY_ATTRIBUTE_UNUSED
# if defined __GNUC__ && 2 < __GNUC__ + (7 <= __GNUC_MINOR__)
#  define YY_ATTRIBUTE_UNUSED __attribute__ ((__unused__))
# else
#  define YY_ATTRIBUTE_UNUSED
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(E) ((void) (E))
#else
# define YYUSE(E) /* empty */
#endif

#if defined __GNUC__ && ! defined __ICC && 407 <= __GNUC__ * 100 + __GNUC_MINOR__
/* Suppress an incorrect diagnostic about yylval being uninitialized.  */
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN                            \
    _Pragma ("GCC diagnostic push")                                     \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")              \
    _Pragma ("GCC diagnostic ignored \"-Wmaybe-uninitialized\"")
# define YY_IGNORE_MAYBE_UNINITIALIZED_END      \
    _Pragma ("GCC diagnostic pop")
#else
# define YY_INITIAL_VALUE(Value) Value
#endif
#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
# define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif

#if defined __cplusplus && defined __GNUC__ && ! defined __ICC && 6 <= __GNUC__
# define YY_IGNORE_USELESS_CAST_BEGIN                          \
    _Pragma ("GCC diagnostic push")                            \
    _Pragma ("GCC diagnostic ignored \"-Wuseless-cast\"")
# define YY_IGNORE_USELESS_CAST_END            \
    _Pragma ("GCC diagnostic pop")
#endif
#ifndef YY_IGNORE_USELESS_CAST_BEGIN
# define YY_IGNORE_USELESS_CAST_BEGIN
# define YY_IGNORE_USELESS_CAST_END
#endif


#define YY_ASSERT(E) ((void) (0 && (E)))

#if !defined yyoverflow

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined EXIT_SUCCESS
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
      /* Use EXIT_SUCCESS as a witness for stdlib.h.  */
#     ifndef EXIT_SUCCESS
#      define EXIT_SUCCESS 0
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's 'empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined YYMALLOC || defined malloc) \
             && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined EXIT_SUCCESS
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* !defined yyoverflow */

#if (! defined yyoverflow \
     && (! defined __cplusplus \
         || (defined JITTERC_LTYPE_IS_TRIVIAL && JITTERC_LTYPE_IS_TRIVIAL \
             && defined JITTERC_STYPE_IS_TRIVIAL && JITTERC_STYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yy_state_t yyss_alloc;
  YYSTYPE yyvs_alloc;
  YYLTYPE yyls_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (YYSIZEOF (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (YYSIZEOF (yy_state_t) + YYSIZEOF (YYSTYPE) \
             + YYSIZEOF (YYLTYPE)) \
      + 2 * YYSTACK_GAP_MAXIMUM)

# define YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)                           \
    do                                                                  \
      {                                                                 \
        YYPTRDIFF_T yynewbytes;                                         \
        YYCOPY (&yyptr->Stack_alloc, Stack, yysize);                    \
        Stack = &yyptr->Stack_alloc;                                    \
        yynewbytes = yystacksize * YYSIZEOF (*Stack) + YYSTACK_GAP_MAXIMUM; \
        yyptr += yynewbytes / YYSIZEOF (*yyptr);                        \
      }                                                                 \
    while (0)

#endif

#if defined YYCOPY_NEEDED && YYCOPY_NEEDED
/* Copy COUNT objects from SRC to DST.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(Dst, Src, Count) \
      __builtin_memcpy (Dst, Src, YY_CAST (YYSIZE_T, (Count)) * sizeof (*(Src)))
#  else
#   define YYCOPY(Dst, Src, Count)              \
      do                                        \
        {                                       \
          YYPTRDIFF_T yyi;                      \
          for (yyi = 0; yyi < (Count); yyi++)   \
            (Dst)[yyi] = (Src)[yyi];            \
        }                                       \
      while (0)
#  endif
# endif
#endif /* !YYCOPY_NEEDED */

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  73
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   209

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  75
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  54
/* YYNRULES -- Number of rules.  */
#define YYNRULES  135
/* YYNSTATES -- Number of states.  */
#define YYNSTATES  238

#define YYMAXUTOK   329


/* YYTRANSLATE(TOKEN-NUM) -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex, with out-of-bounds checking.  */
#define YYTRANSLATE(YYX)                                \
  (0 <= (YYX) && (YYX) <= YYMAXUTOK                     \
   ? YY_CAST (yysymbol_kind_t, yytranslate[YYX])        \
   : YYSYMBOL_YYUNDEF)

/* YYTRANSLATE[TOKEN-NUM] -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex.  */
static const yytype_int8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    56,    57,    58,    59,    60,    61,    62,    63,    64,
      65,    66,    67,    68,    69,    70,    71,    72,    73,    74
};

#if JITTERC_DEBUG
  /* YYRLINEYYN -- Source line where rule number YYN was defined.  */
static const yytype_int16 yyrline[] =
{
       0,   310,   310,   313,   315,   319,   320,   321,   322,   323,
     324,   325,   326,   330,   335,   337,   342,   347,   349,   351,
     353,   355,   357,   359,   361,   363,   365,   367,   369,   371,
     373,   375,   377,   379,   381,   383,   388,   393,   398,   401,
     406,   423,   424,   428,   429,   434,   440,   441,   446,   449,
     455,   464,   475,   489,   494,   495,   500,   501,   506,   509,
     516,   517,   520,   526,   529,   535,   537,   539,   542,   544,
     546,   551,   557,   558,   563,   569,   574,   581,   582,   585,
     588,   591,   594,   597,   603,   610,   611,   614,   617,   620,
     623,   626,   629,   632,   635,   638,   645,   650,   649,   674,
     676,   680,   681,   686,   685,   731,   732,   736,   744,   745,
     752,   753,   760,   774,   792,   794,   795,   796,   800,   801,
     805,   806,   810,   811,   821,   822,   823,   827,   834,   851,
     854,   855,   856,   860,   861,   861
};
#endif

/** Accessing symbol of state STATE.  */
#define YY_ACCESSING_SYMBOL(State) YY_CAST (yysymbol_kind_t, yystos[State])

#if JITTERC_DEBUG || 0
/* The user-facing name of the symbol whose (internal) number is
   YYSYMBOL.  No bounds checking.  */
static const char *yysymbol_name (yysymbol_kind_t yysymbol) YY_ATTRIBUTE_UNUSED;

/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "\"end of file\"", "error", "\"invalid token\"", "VM", "END", "CODE",
  "STRING", "SET", "INITIAL_HEADER_C", "INITIAL_VM1_C", "INITIAL_VM2_C",
  "INITIAL_VM_MAIN_C", "EARLY_HEADER_C", "LATE_HEADER_C", "PRINTER_C",
  "REWRITER_C", "EARLY_C", "LATE_C", "INITIALIZATION_C", "FINALIZATION_C",
  "STATE_EARLY_C", "STATE_BACKING_STRUCT_C", "STATE_RUNTIME_STRUCT_C",
  "STATE_INITIALIZATION_C", "STATE_FINALIZATION_C",
  "INSTRUCTION_BEGINNING_C", "INSTRUCTION_END_C", "BARE_ARGUMENT",
  "IDENTIFIER", "WRAPPED_FUNCTIONS", "WRAPPED_GLOBALS", "INSTRUCTION",
  "OPEN_PAREN", "CLOSE_PAREN", "COMMA", "SEMICOLON", "IN", "OUT", "RULE",
  "WHEN", "REWRITE", "INTO", "TRUE_", "FALSE_", "RULE_PLACEHOLDER", "HOT",
  "COLD", "RELOCATABLE", "NON_RELOCATABLE", "CALLER", "CALLEE",
  "COMMUTATIVE", "NON_COMMUTATIVE", "TWO_OPERANDS", "REGISTER_CLASS",
  "FAST_REGISTER_NO", "REGISTER_OR_STACK_LETTER", "SLOW_REGISTERS",
  "NO_SLOW_REGISTERS", "STACK", "C_TYPE", "C_INITIAL_VALUE",
  "C_ELEMENT_TYPE", "LONG_NAME", "ELEMENT_NO", "NON_TOS_OPTIMIZED",
  "TOS_OPTIMIZED", "NO_GUARD_OVERFLOW", "NO_GUARD_UNDERFLOW",
  "GUARD_OVERFLOW", "GUARD_UNDERFLOW", "FIXNUM", "BITSPERWORD",
  "BYTESPERWORD", "LGBYTESPERWORD", "$accept", "vm", "sections", "section",
  "vm_section", "vm_section_contents", "setting", "c_section",
  "wrapped_functions_section", "wrapped_globals_section", "identifiers",
  "rule_section", "optional_identifier", "rule_guard",
  "rule_instruction_pattern", "rule_instruction_patterns_zero_or_more",
  "rule_instruction_patterns_one_or_more", "rule_argument_pattern",
  "placeholder", "optional_placeholder",
  "rule_argument_patterns_zero_or_more",
  "rule_argument_patterns_one_or_more", "rule_expressions_zero_or_more",
  "rule_expressions_one_or_more", "rule_expression", "rule_operation",
  "rule_instruction_templates_zero_or_more", "rule_instruction_template",
  "register_class_section", "register_class_section_contents",
  "stack_section", "stack_section_contents", "register_or_stack_letter",
  "instruction_section", "$@1", "arguments", "one_or_more_arguments",
  "argument", "$@2", "optional_printer_name", "modes", "modes_rest",
  "mode_character", "bare_argument", "properties", "hotness",
  "relocatability", "callingness", "identifier", "string", "code",
  "literal", "literals", "$@3", YY_NULLPTR
};

static const char *
yysymbol_name (yysymbol_kind_t yysymbol)
{
  return yytname[yysymbol];
}
#endif

#ifdef YYPRINT
/* YYTOKNUM[NUM] -- (External) token number corresponding to the
   (internal) symbol number NUM (which must be that of a token).  */
static const yytype_int16 yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290,   291,   292,   293,   294,
     295,   296,   297,   298,   299,   300,   301,   302,   303,   304,
     305,   306,   307,   308,   309,   310,   311,   312,   313,   314,
     315,   316,   317,   318,   319,   320,   321,   322,   323,   324,
     325,   326,   327,   328,   329
};
#endif

#define YYPACT_NINF (-190)

#define yypact_value_is_default(Yyn) \
  ((Yyn) == YYPACT_NINF)

#define YYTABLE_NINF (-1)

#define yytable_value_is_error(Yyn) \
  0

  /* YYPACTSTATE-NUM -- Index in YYTABLE of the portion describing
     STATE-NUM.  */
static const yytype_int16 yypact[] =
{
      73,    -3,    11,    11,    11,    11,    11,    11,    11,    11,
      11,    11,    11,    11,    11,    11,    11,    11,    11,    11,
      11,   -18,   -18,  -190,   -18,   -11,   -11,    62,  -190,    73,
    -190,  -190,  -190,  -190,  -190,  -190,  -190,  -190,   -18,    60,
      -3,  -190,    68,    96,   103,   106,   108,   110,   111,   112,
     113,   116,   117,   118,   122,   127,   129,   134,   145,   146,
     147,  -190,  -190,  -190,   153,   -18,   154,   -18,   135,  -190,
    -190,     8,   -12,  -190,  -190,   157,  -190,  -190,  -190,  -190,
    -190,  -190,  -190,  -190,  -190,  -190,  -190,  -190,  -190,  -190,
    -190,  -190,  -190,  -190,  -190,  -190,  -190,  -190,  -190,  -190,
     142,   -18,   -59,     8,     8,   157,   157,   157,   172,   157,
     157,   157,   -59,   -12,   -12,   -12,   -12,   -12,   -12,   173,
    -190,  -190,    69,   143,   138,  -190,    74,  -190,  -190,  -190,
    -190,     8,  -190,  -190,     8,     8,     8,  -190,   -12,   -12,
     -12,   -12,  -190,  -190,  -190,  -190,  -190,  -190,  -190,  -190,
    -190,   148,  -190,   149,   -16,    69,   -18,   -18,  -190,  -190,
    -190,   150,  -190,  -190,  -190,   136,   136,  -190,  -190,  -190,
    -190,  -190,  -190,  -190,  -190,   123,    69,  -190,  -190,    69,
    -190,   151,   152,    81,    74,  -190,  -190,  -190,  -190,  -190,
    -190,  -190,  -190,  -190,    11,   123,   123,   123,  -190,   -59,
    -190,    81,   181,   -18,    81,  -190,  -190,  -190,  -190,   155,
    -190,   156,  -190,  -190,   182,  -190,  -190,  -190,  -190,   -18,
    -190,  -190,  -190,   158,    81,    81,  -190,   -59,  -190,  -190,
    -190,  -190,   159,   161,  -190,    81,  -190,  -190
};

  /* YYDEFACTSTATE-NUM -- Default reduction number in state STATE-NUM.
     Performed when YYTABLE does not specify something else to do.  Zero
     means the default is an error.  */
static const yytype_uint8 yydefact[] =
{
       3,    14,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,    38,    38,    97,    41,     0,     0,     0,     2,     3,
       5,     6,     7,     8,    12,     9,    10,    11,     0,     0,
      14,   128,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   124,   126,   125,     0,    38,     0,     0,     0,    42,
      96,    77,    85,     1,     4,     0,    13,    15,    17,    18,
      19,    20,    21,    22,    23,    24,    25,    26,    27,    28,
      29,    30,    31,    32,    33,    34,    35,    36,    39,    37,
       0,    46,     0,    77,    77,     0,     0,     0,     0,     0,
       0,     0,     0,    85,    85,    85,    85,    85,    85,     0,
     127,    16,    99,    48,     0,    47,    56,   129,   130,   131,
     132,    77,    83,    82,    77,    77,    77,    76,    85,    85,
      85,    85,    95,    94,    91,    90,    93,    92,    84,   110,
     111,     0,   100,   101,     0,   108,     0,    72,   113,    53,
     112,    58,    52,    45,    57,    54,    54,    81,    79,    80,
      78,    89,    87,    86,    88,   114,     0,   103,   107,   108,
      49,    43,    73,    60,     0,    55,    50,    51,   118,   119,
     120,   121,   122,   123,     0,   114,   114,   114,   102,   133,
     109,     0,     0,    72,     0,    65,    66,    68,    75,    61,
      70,     0,    67,    59,     0,   115,   116,   117,   134,   105,
      44,    40,    74,     0,     0,    60,    98,   133,   104,   106,
      69,    62,    63,     0,   135,     0,    71,    64
};

  /* YYPGOTONTERM-NUM.  */
static const yytype_int16 yypgoto[] =
{
    -190,  -190,   163,  -190,  -190,   160,  -190,  -190,  -190,  -190,
     -17,  -190,  -190,  -190,  -190,  -190,    39,  -190,  -124,    30,
    -190,    13,   -26,   -34,  -165,  -190,    -1,  -190,  -190,    25,
    -190,    26,   177,  -190,  -190,  -190,    28,  -190,  -190,  -190,
    -190,    27,  -112,    51,  -189,  -190,  -190,  -190,   -21,   -32,
      15,   -65,   -20,  -190
};

  /* YYDEFGOTONTERM-NUM.  */
static const yytype_int16 yydefgoto[] =
{
      -1,    27,    28,    29,    30,    39,    40,    31,    32,    33,
      64,    34,    68,   202,   123,   124,   125,   161,   207,   186,
     163,   164,   208,   231,   209,   210,   181,   182,    35,   108,
      36,   119,    71,    37,    67,   151,   152,   153,   199,   228,
     154,   178,   155,   165,   194,   195,   196,   197,   211,   121,
      42,   212,   219,   227
};

  /* YYTABLEYYPACT[STATE-NUM] -- What to do in state STATE-NUM.  If
     positive, shift that token.  If negative, reduce the rule whose
     number is the opposite.  If YYTABLE_NINF, syntax error.  */
static const yytype_uint8 yytable[] =
{
      65,    65,   162,    69,    38,    66,   215,   216,   217,    61,
      62,   158,   127,   128,   129,   130,    41,    75,    43,    44,
      45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    56,    57,    58,    59,    60,   220,   131,    63,   223,
     160,   185,   185,   179,    65,    70,   100,   141,    98,   109,
     110,   111,   112,   113,   114,   115,   116,   117,   118,   232,
     162,   166,    73,   102,    76,   103,   104,   179,   105,   106,
     232,   107,    78,   134,   135,   136,     1,   138,   139,   140,
     126,     2,     3,     4,     5,     6,     7,     8,     9,    10,
      11,    12,    13,    14,    15,    16,    17,    18,    19,    20,
      79,   158,    21,    22,    23,   149,   150,    80,    61,    62,
      81,    24,    82,   204,    83,    84,    85,    86,   159,   166,
      87,    88,    89,   205,   206,   159,    90,    25,   132,   133,
     160,    91,    26,    92,   218,   126,   183,    63,    93,   142,
     143,   144,   145,   146,   147,   127,   128,   129,   130,    94,
      95,    96,   127,   128,   129,   130,   167,    97,    99,   168,
     169,   170,   218,   120,   171,   172,   173,   174,   188,   189,
     190,   191,   192,   193,   122,   101,   137,   148,   156,   157,
     159,   175,   183,   176,   184,   221,   226,   203,   225,   224,
     201,   230,    74,   235,   236,   180,   187,   213,   229,   233,
      77,   237,   222,    72,   198,   177,   200,   234,     0,   214
};

static const yytype_int16 yycheck[] =
{
      21,    22,   126,    24,     7,    22,   195,   196,   197,    27,
      28,    27,    71,    72,    73,    74,     5,    38,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,   201,   102,    56,   204,
      56,   165,   166,   155,    65,    56,    67,   112,    65,    61,
      62,    63,    64,    65,    66,    67,    68,    69,    70,   224,
     184,   126,     0,    55,     4,    57,    58,   179,    60,    61,
     235,    63,     4,   105,   106,   107,     3,   109,   110,   111,
     101,     8,     9,    10,    11,    12,    13,    14,    15,    16,
      17,    18,    19,    20,    21,    22,    23,    24,    25,    26,
       4,    27,    29,    30,    31,    36,    37,     4,    27,    28,
       4,    38,     4,    32,     4,     4,     4,     4,    44,   184,
       4,     4,     4,    42,    43,    44,     4,    54,   103,   104,
      56,     4,    59,     4,   199,   156,   157,    56,     4,   113,
     114,   115,   116,   117,   118,    71,    72,    73,    74,     4,
       4,     4,    71,    72,    73,    74,   131,     4,     4,   134,
     135,   136,   227,     6,   138,   139,   140,   141,    45,    46,
      47,    48,    49,    50,    32,    40,     4,     4,    35,    41,
      44,    33,   203,    34,    34,     4,     4,    35,    32,    34,
      39,    33,    29,    34,    33,   156,   166,   184,   219,   225,
      40,   235,   203,    26,   176,   154,   179,   227,    -1,   194
};

  /* YYSTOSSTATE-NUM -- The (internal number of the) accessing
     symbol of state STATE-NUM.  */
static const yytype_uint8 yystos[] =
{
       0,     3,     8,     9,    10,    11,    12,    13,    14,    15,
      16,    17,    18,    19,    20,    21,    22,    23,    24,    25,
      26,    29,    30,    31,    38,    54,    59,    76,    77,    78,
      79,    82,    83,    84,    86,   103,   105,   108,     7,    80,
      81,     5,   125,   125,   125,   125,   125,   125,   125,   125,
     125,   125,   125,   125,   125,   125,   125,   125,   125,   125,
     125,    27,    28,    56,    85,   123,    85,   109,    87,   123,
      56,   107,   107,     0,    77,   123,     4,    80,     4,     4,
       4,     4,     4,     4,     4,     4,     4,     4,     4,     4,
       4,     4,     4,     4,     4,     4,     4,     4,    85,     4,
     123,    40,    55,    57,    58,    60,    61,    63,   104,    61,
      62,    63,    64,    65,    66,    67,    68,    69,    70,   106,
       6,   124,    32,    89,    90,    91,   123,    71,    72,    73,
      74,   126,   104,   104,   124,   124,   124,     4,   124,   124,
     124,   126,   106,   106,   106,   106,   106,   106,     4,    36,
      37,   110,   111,   112,   115,   117,    35,    41,    27,    44,
      56,    92,    93,    95,    96,   118,   126,   104,   104,   104,
     104,   106,   106,   106,   106,    33,    34,   118,   116,   117,
      91,   101,   102,   123,    34,    93,    94,    94,    45,    46,
      47,    48,    49,    50,   119,   120,   121,   122,   111,   113,
     116,    39,    88,    35,    32,    42,    43,    93,    97,    99,
     100,   123,   126,    96,   125,   119,   119,   119,   126,   127,
      99,     4,   101,    99,    34,    32,     4,   128,   114,   123,
      33,    98,    99,    97,   127,    34,    33,    98
};

  /* YYR1YYN -- Symbol number of symbol that rule YYN derives.  */
static const yytype_uint8 yyr1[] =
{
       0,    75,    76,    77,    77,    78,    78,    78,    78,    78,
      78,    78,    78,    79,    80,    80,    81,    82,    82,    82,
      82,    82,    82,    82,    82,    82,    82,    82,    82,    82,
      82,    82,    82,    82,    82,    82,    83,    84,    85,    85,
      86,    87,    87,    88,    88,    89,    90,    90,    91,    91,
      92,    92,    92,    93,    94,    94,    95,    95,    96,    96,
      97,    97,    97,    98,    98,    99,    99,    99,    99,    99,
      99,   100,   101,   101,   101,   102,   103,   104,   104,   104,
     104,   104,   104,   104,   105,   106,   106,   106,   106,   106,
     106,   106,   106,   106,   106,   106,   107,   109,   108,   110,
     110,   111,   111,   113,   112,   114,   114,   115,   116,   116,
     117,   117,   118,   118,   119,   119,   119,   119,   120,   120,
     121,   121,   122,   122,   123,   123,   123,   124,   125,   126,
     126,   126,   126,   127,   128,   127
};

  /* YYR2YYN -- Number of symbols on the right hand side of rule YYN.  */
static const yytype_int8 yyr2[] =
{
       0,     2,     1,     0,     2,     1,     1,     1,     1,     1,
       1,     1,     1,     3,     0,     2,     3,     3,     3,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     0,     2,
       8,     0,     1,     0,     2,     2,     0,     1,     1,     3,
       2,     2,     1,     1,     0,     1,     0,     1,     1,     3,
       0,     1,     3,     1,     3,     1,     1,     1,     1,     3,
       1,     4,     0,     1,     3,     2,     4,     0,     3,     3,
       3,     3,     2,     2,     4,     0,     3,     3,     3,     3,
       2,     2,     2,     2,     2,     2,     1,     0,     9,     0,
       1,     1,     3,     0,     5,     0,     1,     2,     0,     2,
       1,     1,     1,     1,     0,     2,     2,     2,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     0,     0,     3
};


enum { YYENOMEM = -2 };

#define yyerrok         (yyerrstatus = 0)
#define yyclearin       (yychar = JITTERC_EMPTY)

#define YYACCEPT        goto yyacceptlab
#define YYABORT         goto yyabortlab
#define YYERROR         goto yyerrorlab


#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)                                    \
  do                                                              \
    if (yychar == JITTERC_EMPTY)                                        \
      {                                                           \
        yychar = (Token);                                         \
        yylval = (Value);                                         \
        YYPOPSTACK (yylen);                                       \
        yystate = *yyssp;                                         \
        goto yybackup;                                            \
      }                                                           \
    else                                                          \
      {                                                           \
        yyerror (&yylloc, vm, jitterc_scanner, YY_("syntax error: cannot back up")); \
        YYERROR;                                                  \
      }                                                           \
  while (0)

/* Backward compatibility with an undocumented macro.
   Use JITTERC_error or JITTERC_UNDEF. */
#define YYERRCODE JITTERC_UNDEF

/* YYLLOC_DEFAULT -- Set CURRENT to span from RHS[1] to RHS[N].
   If N is 0, then set CURRENT to the empty location which ends
   the previous symbol: RHS[0] (always defined).  */

#ifndef YYLLOC_DEFAULT
# define YYLLOC_DEFAULT(Current, Rhs, N)                                \
    do                                                                  \
      if (N)                                                            \
        {                                                               \
          (Current).first_line   = YYRHSLOC (Rhs, 1).first_line;        \
          (Current).first_column = YYRHSLOC (Rhs, 1).first_column;      \
          (Current).last_line    = YYRHSLOC (Rhs, N).last_line;         \
          (Current).last_column  = YYRHSLOC (Rhs, N).last_column;       \
        }                                                               \
      else                                                              \
        {                                                               \
          (Current).first_line   = (Current).last_line   =              \
            YYRHSLOC (Rhs, 0).last_line;                                \
          (Current).first_column = (Current).last_column =              \
            YYRHSLOC (Rhs, 0).last_column;                              \
        }                                                               \
    while (0)
#endif

#define YYRHSLOC(Rhs, K) ((Rhs)[K])


/* Enable debugging if requested.  */
#if JITTERC_DEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)                        \
do {                                            \
  if (yydebug)                                  \
    YYFPRINTF Args;                             \
} while (0)


/* YY_LOCATION_PRINT -- Print the location on the stream.
   This macro was not mandated originally: define only if we know
   we won't break user code: when these are the locations we know.  */

# ifndef YY_LOCATION_PRINT
#  if defined JITTERC_LTYPE_IS_TRIVIAL && JITTERC_LTYPE_IS_TRIVIAL

/* Print *YYLOCP on YYO.  Private, do not rely on its existence. */

YY_ATTRIBUTE_UNUSED
static int
yy_location_print_ (FILE *yyo, YYLTYPE const * const yylocp)
{
  int res = 0;
  int end_col = 0 != yylocp->last_column ? yylocp->last_column - 1 : 0;
  if (0 <= yylocp->first_line)
    {
      res += YYFPRINTF (yyo, "%d", yylocp->first_line);
      if (0 <= yylocp->first_column)
        res += YYFPRINTF (yyo, ".%d", yylocp->first_column);
    }
  if (0 <= yylocp->last_line)
    {
      if (yylocp->first_line < yylocp->last_line)
        {
          res += YYFPRINTF (yyo, "-%d", yylocp->last_line);
          if (0 <= end_col)
            res += YYFPRINTF (yyo, ".%d", end_col);
        }
      else if (0 <= end_col && yylocp->first_column < end_col)
        res += YYFPRINTF (yyo, "-%d", end_col);
    }
  return res;
 }

#   define YY_LOCATION_PRINT(File, Loc)          \
  yy_location_print_ (File, &(Loc))

#  else
#   define YY_LOCATION_PRINT(File, Loc) ((void) 0)
#  endif
# endif /* !defined YY_LOCATION_PRINT */


# define YY_SYMBOL_PRINT(Title, Kind, Value, Location)                    \
do {                                                                      \
  if (yydebug)                                                            \
    {                                                                     \
      YYFPRINTF (stderr, "%s ", Title);                                   \
      yy_symbol_print (stderr,                                            \
                  Kind, Value, Location, vm, jitterc_scanner); \
      YYFPRINTF (stderr, "\n");                                           \
    }                                                                     \
} while (0)


/*-----------------------------------.
| Print this symbol's value on YYO.  |
`-----------------------------------*/

static void
yy_symbol_value_print (FILE *yyo,
                       yysymbol_kind_t yykind, YYSTYPE const * const yyvaluep, YYLTYPE const * const yylocationp, struct jitterc_vm *vm, void* jitterc_scanner)
{
  FILE *yyoutput = yyo;
  YYUSE (yyoutput);
  YYUSE (yylocationp);
  YYUSE (vm);
  YYUSE (jitterc_scanner);
  if (!yyvaluep)
    return;
# ifdef YYPRINT
  if (yykind < YYNTOKENS)
    YYPRINT (yyo, yytoknum[yykind], *yyvaluep);
# endif
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YYUSE (yykind);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}


/*---------------------------.
| Print this symbol on YYO.  |
`---------------------------*/

static void
yy_symbol_print (FILE *yyo,
                 yysymbol_kind_t yykind, YYSTYPE const * const yyvaluep, YYLTYPE const * const yylocationp, struct jitterc_vm *vm, void* jitterc_scanner)
{
  YYFPRINTF (yyo, "%s %s (",
             yykind < YYNTOKENS ? "token" : "nterm", yysymbol_name (yykind));

  YY_LOCATION_PRINT (yyo, *yylocationp);
  YYFPRINTF (yyo, ": ");
  yy_symbol_value_print (yyo, yykind, yyvaluep, yylocationp, vm, jitterc_scanner);
  YYFPRINTF (yyo, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

static void
yy_stack_print (yy_state_t *yybottom, yy_state_t *yytop)
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)                            \
do {                                                            \
  if (yydebug)                                                  \
    yy_stack_print ((Bottom), (Top));                           \
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

static void
yy_reduce_print (yy_state_t *yyssp, YYSTYPE *yyvsp, YYLTYPE *yylsp,
                 int yyrule, struct jitterc_vm *vm, void* jitterc_scanner)
{
  int yylno = yyrline[yyrule];
  int yynrhs = yyr2[yyrule];
  int yyi;
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %d):\n",
             yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr,
                       YY_ACCESSING_SYMBOL (+yyssp[yyi + 1 - yynrhs]),
                       &yyvsp[(yyi + 1) - (yynrhs)],
                       &(yylsp[(yyi + 1) - (yynrhs)]), vm, jitterc_scanner);
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)          \
do {                                    \
  if (yydebug)                          \
    yy_reduce_print (yyssp, yyvsp, yylsp, Rule, vm, jitterc_scanner); \
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !JITTERC_DEBUG */
# define YYDPRINTF(Args) ((void) 0)
# define YY_SYMBOL_PRINT(Title, Kind, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !JITTERC_DEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif






/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

static void
yydestruct (const char *yymsg,
            yysymbol_kind_t yykind, YYSTYPE *yyvaluep, YYLTYPE *yylocationp, struct jitterc_vm *vm, void* jitterc_scanner)
{
  YYUSE (yyvaluep);
  YYUSE (yylocationp);
  YYUSE (vm);
  YYUSE (jitterc_scanner);
  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yykind, yyvaluep, yylocationp);

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YYUSE (yykind);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}






/*----------.
| yyparse.  |
`----------*/

int
yyparse (struct jitterc_vm *vm, void* jitterc_scanner)
{
/* The lookahead symbol.  */
int yychar;


/* The semantic value of the lookahead symbol.  */
/* Default value used for initialization, for pacifying older GCCs
   or non-GCC compilers.  */
YY_INITIAL_VALUE (static YYSTYPE yyval_default;)
YYSTYPE yylval YY_INITIAL_VALUE (= yyval_default);

/* Location data for the lookahead symbol.  */
static YYLTYPE yyloc_default
# if defined JITTERC_LTYPE_IS_TRIVIAL && JITTERC_LTYPE_IS_TRIVIAL
  = { 1, 1, 1, 1 }
# endif
;
YYLTYPE yylloc = yyloc_default;

    /* Number of syntax errors so far.  */
    int yynerrs;

    yy_state_fast_t yystate;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus;

    /* The stacks and their tools:
       'yyss': related to states.
       'yyvs': related to semantic values.
       'yyls': related to locations.

       Refer to the stacks through separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* Their size.  */
    YYPTRDIFF_T yystacksize;

    /* The state stack.  */
    yy_state_t yyssa[YYINITDEPTH];
    yy_state_t *yyss;
    yy_state_t *yyssp;

    /* The semantic value stack.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs;
    YYSTYPE *yyvsp;

    /* The location stack.  */
    YYLTYPE yylsa[YYINITDEPTH];
    YYLTYPE *yyls;
    YYLTYPE *yylsp;

  int yyn;
  /* The return value of yyparse.  */
  int yyresult;
  /* Lookahead token as an internal (translated) token number.  */
  yysymbol_kind_t yytoken = YYSYMBOL_YYEMPTY;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;
  YYLTYPE yyloc;

  /* The locations where the error started and ended.  */
  YYLTYPE yyerror_range[3];



#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N), yylsp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  yynerrs = 0;
  yystate = 0;
  yyerrstatus = 0;

  yystacksize = YYINITDEPTH;
  yyssp = yyss = yyssa;
  yyvsp = yyvs = yyvsa;
  yylsp = yyls = yylsa;


  YYDPRINTF ((stderr, "Starting parse\n"));

  yychar = JITTERC_EMPTY; /* Cause a token to be read.  */
  yylsp[0] = yylloc;
  goto yysetstate;


/*------------------------------------------------------------.
| yynewstate -- push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;


/*--------------------------------------------------------------------.
| yysetstate -- set current state (the top of the stack) to yystate.  |
`--------------------------------------------------------------------*/
yysetstate:
  YYDPRINTF ((stderr, "Entering state %d\n", yystate));
  YY_ASSERT (0 <= yystate && yystate < YYNSTATES);
  YY_IGNORE_USELESS_CAST_BEGIN
  *yyssp = YY_CAST (yy_state_t, yystate);
  YY_IGNORE_USELESS_CAST_END
  YY_STACK_PRINT (yyss, yyssp);

  if (yyss + yystacksize - 1 <= yyssp)
#if !defined yyoverflow && !defined YYSTACK_RELOCATE
    goto yyexhaustedlab;
#else
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYPTRDIFF_T yysize = yyssp - yyss + 1;

# if defined yyoverflow
      {
        /* Give user a chance to reallocate the stack.  Use copies of
           these so that the &'s don't force the real ones into
           memory.  */
        yy_state_t *yyss1 = yyss;
        YYSTYPE *yyvs1 = yyvs;
        YYLTYPE *yyls1 = yyls;

        /* Each stack pointer address is followed by the size of the
           data in use in that stack, in bytes.  This used to be a
           conditional around just the two extra args, but that might
           be undefined if yyoverflow is a macro.  */
        yyoverflow (YY_("memory exhausted"),
                    &yyss1, yysize * YYSIZEOF (*yyssp),
                    &yyvs1, yysize * YYSIZEOF (*yyvsp),
                    &yyls1, yysize * YYSIZEOF (*yylsp),
                    &yystacksize);
        yyss = yyss1;
        yyvs = yyvs1;
        yyls = yyls1;
      }
# else /* defined YYSTACK_RELOCATE */
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
        goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
        yystacksize = YYMAXDEPTH;

      {
        yy_state_t *yyss1 = yyss;
        union yyalloc *yyptr =
          YY_CAST (union yyalloc *,
                   YYSTACK_ALLOC (YY_CAST (YYSIZE_T, YYSTACK_BYTES (yystacksize))));
        if (! yyptr)
          goto yyexhaustedlab;
        YYSTACK_RELOCATE (yyss_alloc, yyss);
        YYSTACK_RELOCATE (yyvs_alloc, yyvs);
        YYSTACK_RELOCATE (yyls_alloc, yyls);
#  undef YYSTACK_RELOCATE
        if (yyss1 != yyssa)
          YYSTACK_FREE (yyss1);
      }
# endif

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;
      yylsp = yyls + yysize - 1;

      YY_IGNORE_USELESS_CAST_BEGIN
      YYDPRINTF ((stderr, "Stack size increased to %ld\n",
                  YY_CAST (long, yystacksize)));
      YY_IGNORE_USELESS_CAST_END

      if (yyss + yystacksize - 1 <= yyssp)
        YYABORT;
    }
#endif /* !defined yyoverflow && !defined YYSTACK_RELOCATE */

  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;


/*-----------.
| yybackup.  |
`-----------*/
yybackup:
  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yypact_value_is_default (yyn))
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either empty, or end-of-input, or a valid lookahead.  */
  if (yychar == JITTERC_EMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token\n"));
      yychar = yylex (&yylval, &yylloc, jitterc_scanner);
    }

  if (yychar <= JITTERC_EOF)
    {
      yychar = JITTERC_EOF;
      yytoken = YYSYMBOL_YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else if (yychar == JITTERC_error)
    {
      /* The scanner already issued an error message, process directly
         to error recovery.  But do not keep the error token as
         lookahead, it is too special and may lead us to an endless
         loop in error recovery. */
      yychar = JITTERC_UNDEF;
      yytoken = YYSYMBOL_YYerror;
      yyerror_range[1] = yylloc;
      goto yyerrlab1;
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yytable_value_is_error (yyn))
        goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);
  yystate = yyn;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END
  *++yylsp = yylloc;

  /* Discard the shifted token.  */
  yychar = JITTERC_EMPTY;
  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     '$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];

  /* Default location. */
  YYLLOC_DEFAULT (yyloc, (yylsp - yylen), yylen);
  yyerror_range[1] = yyloc;
  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
  case 16:
#line 342 "../../jitter/jitterc/jitterc.y"
                         { jitterc_vm_add_setting (vm, (yyvsp[-1].string), (yyvsp[0].string));
                           free ((yyvsp[-1].string)); }
#line 1914 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 17:
#line 348 "../../jitter/jitterc/jitterc.y"
    { JITTERC_APPEND_CODE(vm->initial_header_c_code, & (yyvsp[-1].code_block)); }
#line 1920 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 18:
#line 350 "../../jitter/jitterc/jitterc.y"
    { JITTERC_APPEND_CODE(vm->initial_vm1_c_code, & (yyvsp[-1].code_block)); }
#line 1926 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 19:
#line 352 "../../jitter/jitterc/jitterc.y"
    { JITTERC_APPEND_CODE(vm->initial_vm2_c_code, & (yyvsp[-1].code_block)); }
#line 1932 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 20:
#line 354 "../../jitter/jitterc/jitterc.y"
    { JITTERC_APPEND_CODE(vm->initial_vm_main_c_code, & (yyvsp[-1].code_block)); }
#line 1938 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 21:
#line 356 "../../jitter/jitterc/jitterc.y"
    { JITTERC_APPEND_CODE(vm->early_header_c_code, & (yyvsp[-1].code_block)); }
#line 1944 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 22:
#line 358 "../../jitter/jitterc/jitterc.y"
    { JITTERC_APPEND_CODE(vm->late_header_c_code, & (yyvsp[-1].code_block)); }
#line 1950 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 23:
#line 360 "../../jitter/jitterc/jitterc.y"
    { JITTERC_APPEND_CODE(vm->printer_c_code, & (yyvsp[-1].code_block)); }
#line 1956 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 24:
#line 362 "../../jitter/jitterc/jitterc.y"
    { JITTERC_APPEND_CODE(vm->rewriter_c_code, & (yyvsp[-1].code_block)); }
#line 1962 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 25:
#line 364 "../../jitter/jitterc/jitterc.y"
    { JITTERC_APPEND_CODE(vm->early_c_code, & (yyvsp[-1].code_block)); }
#line 1968 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 26:
#line 366 "../../jitter/jitterc/jitterc.y"
    { JITTERC_APPEND_CODE(vm->before_main_c_code, & (yyvsp[-1].code_block)); }
#line 1974 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 27:
#line 368 "../../jitter/jitterc/jitterc.y"
    { JITTERC_APPEND_CODE(vm->initialization_c_code, & (yyvsp[-1].code_block)); }
#line 1980 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 28:
#line 370 "../../jitter/jitterc/jitterc.y"
    { JITTERC_APPEND_CODE(vm->finalization_c_code, & (yyvsp[-1].code_block)); }
#line 1986 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 29:
#line 372 "../../jitter/jitterc/jitterc.y"
    { JITTERC_APPEND_CODE(vm->state_early_c_code, & (yyvsp[-1].code_block)); }
#line 1992 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 30:
#line 374 "../../jitter/jitterc/jitterc.y"
    { JITTERC_APPEND_CODE(vm->state_backing_struct_c_code, & (yyvsp[-1].code_block)); }
#line 1998 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 31:
#line 376 "../../jitter/jitterc/jitterc.y"
    { JITTERC_APPEND_CODE(vm->state_runtime_struct_c_code, & (yyvsp[-1].code_block)); }
#line 2004 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 32:
#line 378 "../../jitter/jitterc/jitterc.y"
    { JITTERC_APPEND_CODE(vm->state_initialization_c_code, & (yyvsp[-1].code_block)); }
#line 2010 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 33:
#line 380 "../../jitter/jitterc/jitterc.y"
    { JITTERC_APPEND_CODE(vm->state_finalization_c_code, & (yyvsp[-1].code_block)); }
#line 2016 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 34:
#line 382 "../../jitter/jitterc/jitterc.y"
    { JITTERC_APPEND_CODE(vm->instruction_beginning_c_code, & (yyvsp[-1].code_block)); }
#line 2022 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 35:
#line 384 "../../jitter/jitterc/jitterc.y"
    { JITTERC_APPEND_CODE(vm->instruction_end_c_code, & (yyvsp[-1].code_block)); }
#line 2028 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 36:
#line 389 "../../jitter/jitterc/jitterc.y"
  { jitterc_clone_list_from (vm->wrapped_functions, (yyvsp[-1].string_list)); }
#line 2034 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 37:
#line 394 "../../jitter/jitterc/jitterc.y"
  { jitterc_clone_list_from (vm->wrapped_globals, (yyvsp[-1].string_list)); }
#line 2040 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 38:
#line 398 "../../jitter/jitterc/jitterc.y"
                          { (yyval.string_list) = gl_list_nx_create_empty (GL_ARRAY_LIST,
                                                          NULL, NULL, NULL,
                                                          true); }
#line 2048 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 39:
#line 401 "../../jitter/jitterc/jitterc.y"
                          { gl_list_add_last ((yyvsp[0].string_list), (yyvsp[-1].string));
                            (yyval.string_list) = (yyvsp[0].string_list); }
#line 2055 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 40:
#line 411 "../../jitter/jitterc/jitterc.y"
  { struct jitterc_rule *rule
      = jitterc_make_rule ((yyvsp[-4].instruction_patterns),
                           (yyvsp[-2].instruction_templates),
                           (yyvsp[-1].template_expression),
                           ((yyvsp[-6].string) != NULL
                            ? (yyvsp[-6].string)
                            : jitter_clone_string ("unnamed")),
                           JITTERC_LINENO);
    jitterc_add_rule (vm, rule); }
#line 2069 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 41:
#line 423 "../../jitter/jitterc/jitterc.y"
                 { (yyval.string) = NULL; }
#line 2075 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 42:
#line 424 "../../jitter/jitterc/jitterc.y"
                 { (yyval.string) = (yyvsp[0].string); }
#line 2081 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 43:
#line 428 "../../jitter/jitterc/jitterc.y"
  { (yyval.template_expression) = jitterc_make_template_expression_boolean (true, JITTERC_LINENO); }
#line 2087 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 44:
#line 430 "../../jitter/jitterc/jitterc.y"
  { (yyval.template_expression) = (yyvsp[0].template_expression); }
#line 2093 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 45:
#line 435 "../../jitter/jitterc/jitterc.y"
  { (yyval.instruction_pattern) = jitterc_make_instruction_pattern ((yyvsp[-1].string), (yyvsp[0].argument_patterns), JITTERC_LINENO); }
#line 2099 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 46:
#line 440 "../../jitter/jitterc/jitterc.y"
  { (yyval.instruction_patterns) = jitterc_make_empty_list (); }
#line 2105 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 47:
#line 442 "../../jitter/jitterc/jitterc.y"
  { (yyval.instruction_patterns) = (yyvsp[0].instruction_patterns); }
#line 2111 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 48:
#line 447 "../../jitter/jitterc/jitterc.y"
  { (yyval.instruction_patterns) = jitterc_make_empty_list ();
    gl_list_add_last ((yyval.instruction_patterns), (yyvsp[0].instruction_pattern)); }
#line 2118 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 49:
#line 450 "../../jitter/jitterc/jitterc.y"
  { (yyval.instruction_patterns) = (yyvsp[0].instruction_patterns);
    gl_list_add_first ((yyval.instruction_patterns), (yyvsp[-2].instruction_pattern)); }
#line 2125 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 50:
#line 456 "../../jitter/jitterc/jitterc.y"
  {
    union jitter_word irrelevant;
    (yyval.argument_pattern) = jitterc_make_argument_pattern ((yyvsp[-1].bare_argument).kind,
                                        false,
                                        irrelevant,
                                        (yyvsp[0].string),
                                        JITTERC_LINENO);
  }
#line 2138 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 51:
#line 465 "../../jitter/jitterc/jitterc.y"
  {
    union jitter_word literal = { .fixnum = (yyvsp[-1].fixnum) };
    enum jitterc_instruction_argument_kind literal_kind
      = jitterc_instruction_argument_kind_literal;
    (yyval.argument_pattern) = jitterc_make_argument_pattern (literal_kind,
                                        true,
                                        literal,
                                        (yyvsp[0].string),
                                        JITTERC_LINENO);
  }
#line 2153 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 52:
#line 476 "../../jitter/jitterc/jitterc.y"
  {
    union jitter_word irrelevant;
    enum jitterc_instruction_argument_kind any_kind
      = jitterc_instruction_argument_kind_unspecified;
    (yyval.argument_pattern) = jitterc_make_argument_pattern (any_kind,
                                        false,
                                        irrelevant,
                                        (yyvsp[0].string),
                                        JITTERC_LINENO);
  }
#line 2168 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 53:
#line 489 "../../jitter/jitterc/jitterc.y"
                    { /* Strip away the prefix. */
                      (yyval.string) = jitter_clone_string (JITTERC_TEXT + 1); }
#line 2175 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 54:
#line 494 "../../jitter/jitterc/jitterc.y"
                 { (yyval.string) = NULL; }
#line 2181 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 55:
#line 495 "../../jitter/jitterc/jitterc.y"
                 { (yyval.string) = (yyvsp[0].string); }
#line 2187 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 56:
#line 500 "../../jitter/jitterc/jitterc.y"
  { (yyval.argument_patterns) = jitterc_make_empty_list (); }
#line 2193 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 57:
#line 502 "../../jitter/jitterc/jitterc.y"
  { (yyval.argument_patterns) = (yyvsp[0].argument_patterns); }
#line 2199 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 58:
#line 507 "../../jitter/jitterc/jitterc.y"
  { (yyval.argument_patterns) = jitterc_make_empty_list ();
    gl_list_add_last ((yyval.argument_patterns), (yyvsp[0].argument_pattern)); }
#line 2206 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 59:
#line 510 "../../jitter/jitterc/jitterc.y"
  { (yyval.argument_patterns) = (yyvsp[0].argument_patterns);
    gl_list_add_first ((yyval.argument_patterns), (yyvsp[-2].argument_pattern)); }
#line 2213 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 60:
#line 516 "../../jitter/jitterc/jitterc.y"
  { (yyval.template_expressions) = jitterc_make_empty_list (); }
#line 2219 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 61:
#line 518 "../../jitter/jitterc/jitterc.y"
  { (yyval.template_expressions) = jitterc_make_empty_list ();
    gl_list_add_last ((yyval.template_expressions), (yyvsp[0].template_expression)); }
#line 2226 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 62:
#line 521 "../../jitter/jitterc/jitterc.y"
  { (yyval.template_expressions) = (yyvsp[0].template_expressions);
    gl_list_add_first ((yyval.template_expressions), (yyvsp[-2].template_expression)); }
#line 2233 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 63:
#line 527 "../../jitter/jitterc/jitterc.y"
  { (yyval.template_expressions) = jitterc_make_empty_list ();
    gl_list_add_last ((yyval.template_expressions), (yyvsp[0].template_expression)); }
#line 2240 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 64:
#line 530 "../../jitter/jitterc/jitterc.y"
  { (yyval.template_expressions) = (yyvsp[0].template_expressions);
    gl_list_add_first ((yyval.template_expressions), (yyvsp[-2].template_expression)); }
#line 2247 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 65:
#line 536 "../../jitter/jitterc/jitterc.y"
  { (yyval.template_expression) = jitterc_make_template_expression_boolean (true, JITTERC_LINENO); }
#line 2253 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 66:
#line 538 "../../jitter/jitterc/jitterc.y"
  { (yyval.template_expression) = jitterc_make_template_expression_boolean (false, JITTERC_LINENO); }
#line 2259 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 67:
#line 540 "../../jitter/jitterc/jitterc.y"
  { union jitter_word w = { .fixnum = (yyvsp[0].fixnum) };
    (yyval.template_expression) = jitterc_make_template_expression_fixnum (w, JITTERC_LINENO); }
#line 2266 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 68:
#line 543 "../../jitter/jitterc/jitterc.y"
  { (yyval.template_expression) = jitterc_make_template_expression_placeholder ((yyvsp[0].string), JITTERC_LINENO); }
#line 2272 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 69:
#line 545 "../../jitter/jitterc/jitterc.y"
  { (yyval.template_expression) = (yyvsp[-1].template_expression); }
#line 2278 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 70:
#line 547 "../../jitter/jitterc/jitterc.y"
  { (yyval.template_expression) = (yyvsp[0].template_expression); }
#line 2284 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 71:
#line 552 "../../jitter/jitterc/jitterc.y"
  { (yyval.template_expression) = jitterc_make_template_expression_operation ((yyvsp[-3].string), (yyvsp[-1].template_expressions), JITTERC_LINENO); }
#line 2290 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 72:
#line 557 "../../jitter/jitterc/jitterc.y"
  { (yyval.instruction_templates) = jitterc_make_empty_list (); }
#line 2296 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 73:
#line 559 "../../jitter/jitterc/jitterc.y"
  { (yyval.instruction_templates) = jitterc_make_empty_list ();
    gl_list_add_last ((yyval.instruction_templates), (yyvsp[0].instruction_template)); }
#line 2303 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 74:
#line 564 "../../jitter/jitterc/jitterc.y"
  { (yyval.instruction_templates) = (yyvsp[0].instruction_templates);
    gl_list_add_first ((yyval.instruction_templates), (yyvsp[-2].instruction_template)); }
#line 2310 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 75:
#line 570 "../../jitter/jitterc/jitterc.y"
  { (yyval.instruction_template) = jitterc_make_instruction_template ((yyvsp[-1].string), (yyvsp[0].template_expressions), JITTERC_LINENO); }
#line 2316 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 76:
#line 575 "../../jitter/jitterc/jitterc.y"
    { jitterc_vm_register_class_set_letter ((yyvsp[-1].register_class), (yyvsp[-2].character));
      jitterc_vm_add_register_class (vm, (yyvsp[-1].register_class)); }
#line 2323 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 77:
#line 581 "../../jitter/jitterc/jitterc.y"
    { (yyval.register_class) = jitterc_make_register_class (); }
#line 2329 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 78:
#line 583 "../../jitter/jitterc/jitterc.y"
    { jitterc_vm_register_class_set_long_name ((yyvsp[0].register_class), (yyvsp[-1].string));
      (yyval.register_class) = (yyvsp[0].register_class); }
#line 2336 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 79:
#line 586 "../../jitter/jitterc/jitterc.y"
    { jitterc_vm_register_class_set_c_type ((yyvsp[0].register_class), (yyvsp[-1].string));
      (yyval.register_class) = (yyvsp[0].register_class); }
#line 2343 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 80:
#line 589 "../../jitter/jitterc/jitterc.y"
    { jitterc_vm_register_class_set_c_initial_value ((yyvsp[0].register_class), (yyvsp[-1].string));
      (yyval.register_class) = (yyvsp[0].register_class); }
#line 2350 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 81:
#line 592 "../../jitter/jitterc/jitterc.y"
    { jitterc_vm_register_class_set_fast_register_no ((yyvsp[0].register_class), (yyvsp[-1].fixnum));
      (yyval.register_class) = (yyvsp[0].register_class); }
#line 2357 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 82:
#line 595 "../../jitter/jitterc/jitterc.y"
    { jitterc_vm_register_class_set_use_slow_registers ((yyvsp[0].register_class), 0);
      (yyval.register_class) = (yyvsp[0].register_class); }
#line 2364 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 83:
#line 598 "../../jitter/jitterc/jitterc.y"
    { jitterc_vm_register_class_set_use_slow_registers ((yyvsp[0].register_class), 1);
      (yyval.register_class) = (yyvsp[0].register_class); }
#line 2371 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 84:
#line 604 "../../jitter/jitterc/jitterc.y"
    { jitterc_vm_stack_set_letter ((yyvsp[-1].stack), (yyvsp[-2].character));
      jitterc_vm_add_stack (vm, (yyvsp[-1].stack)); }
#line 2378 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 85:
#line 610 "../../jitter/jitterc/jitterc.y"
    { (yyval.stack) = jitterc_vm_make_stack (); }
#line 2384 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 86:
#line 612 "../../jitter/jitterc/jitterc.y"
    { jitterc_vm_stack_set_long_name ((yyvsp[0].stack), (yyvsp[-1].string));
      (yyval.stack) = (yyvsp[0].stack); }
#line 2391 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 87:
#line 615 "../../jitter/jitterc/jitterc.y"
    { jitterc_vm_stack_set_c_element_type ((yyvsp[0].stack), (yyvsp[-1].string));
      (yyval.stack) = (yyvsp[0].stack); }
#line 2398 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 88:
#line 618 "../../jitter/jitterc/jitterc.y"
    { jitterc_vm_stack_set_element_no ((yyvsp[0].stack), (yyvsp[-1].fixnum));
      (yyval.stack) = (yyvsp[0].stack); }
#line 2405 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 89:
#line 621 "../../jitter/jitterc/jitterc.y"
    { jitterc_vm_stack_set_c_initial_value ((yyvsp[0].stack), (yyvsp[-1].string));
      (yyval.stack) = (yyvsp[0].stack); }
#line 2412 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 90:
#line 624 "../../jitter/jitterc/jitterc.y"
    { jitterc_vm_stack_set_guard_underflow ((yyvsp[0].stack), 0);
      (yyval.stack) = (yyvsp[0].stack); }
#line 2419 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 91:
#line 627 "../../jitter/jitterc/jitterc.y"
    { jitterc_vm_stack_set_guard_overflow ((yyvsp[0].stack), 0);
      (yyval.stack) = (yyvsp[0].stack); }
#line 2426 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 92:
#line 630 "../../jitter/jitterc/jitterc.y"
    { jitterc_vm_stack_set_guard_underflow ((yyvsp[0].stack), 1);
      (yyval.stack) = (yyvsp[0].stack); }
#line 2433 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 93:
#line 633 "../../jitter/jitterc/jitterc.y"
    { jitterc_vm_stack_set_guard_overflow ((yyvsp[0].stack), 1);
      (yyval.stack) = (yyvsp[0].stack); }
#line 2440 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 94:
#line 636 "../../jitter/jitterc/jitterc.y"
    { jitterc_vm_stack_set_implementation ((yyvsp[0].stack), jitterc_stack_implementation_tos);
      (yyval.stack) = (yyvsp[0].stack); }
#line 2447 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 95:
#line 639 "../../jitter/jitterc/jitterc.y"
    { jitterc_vm_stack_set_implementation ((yyvsp[0].stack),
                                           jitterc_stack_implementation_no_tos);
      (yyval.stack) = (yyvsp[0].stack); }
#line 2455 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 96:
#line 645 "../../jitter/jitterc/jitterc.y"
                           { (yyval.character) = JITTERC_TEXT [0]; }
#line 2461 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 97:
#line 650 "../../jitter/jitterc/jitterc.y"
  { jitterc_vm_append_instruction (vm, jitterc_make_instruction ()); }
#line 2467 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 98:
#line 652 "../../jitter/jitterc/jitterc.y"
  { /* Make an instruction, and initialize its fields. */
    struct jitterc_instruction *ins = jitterc_vm_last_instruction (vm);
    ins->name = (yyvsp[-6].string);
    ins->mangled_name = jitterc_mangle (ins->name);
    /* The arguments have already been added one by one by the argument rule. */
    if (ins->hotness == jitterc_hotness_unspecified)
      ins->hotness = jitterc_hotness_hot;
    if (ins->relocatability == jitterc_relocatability_unspecified)
      ins->relocatability = jitterc_relocatability_relocatable;
    if (ins->callerness == jitterc_callerness_unspecified)
      ins->callerness = jitterc_callerness_non_caller;
    if (ins->calleeness == jitterc_calleeness_unspecified)
      ins->calleeness = jitterc_calleeness_non_callee;
    if (   ins->has_fast_labels
        && ins->relocatability == jitterc_relocatability_non_relocatable)
      JITTERC_PARSE_ERROR("a non-relocatable instruction has fast labels");
    if (   ins->callerness == jitterc_callerness_caller
        && ins->relocatability == jitterc_relocatability_non_relocatable)
      JITTERC_PARSE_ERROR("non-relocatable instructions cannot (currently) be callers");
    ins->code = (yyvsp[-1].code_block).code; }
#line 2492 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 103:
#line 686 "../../jitter/jitterc/jitterc.y"
    { struct jitterc_instruction_argument *arg
        = jitterc_make_instruction_argument ();
      arg->mode = (yyvsp[-1].mode);
      arg->kind = (yyvsp[0].bare_argument).kind;
      if (arg->kind & jitterc_instruction_argument_kind_register)
        arg->register_class_character = (yyvsp[0].bare_argument).register_class_letter;
      if (arg->kind & jitterc_instruction_argument_kind_literal)
        {
          if (arg->mode & jitterc_instruction_argument_mode_out)
            JITTERC_PARSE_ERROR("a literal cannot be an output");

          /* FIXME: this might need to be generalized or cleaned up
             in the future. */
          arg->literal_type = jitterc_literal_type_fixnum;
        }
      if (arg->kind & jitterc_instruction_argument_kind_label)
        {
          if (arg->mode & jitterc_instruction_argument_mode_out)
            JITTERC_PARSE_ERROR("a label cannot be an output");
        }
      if (arg->kind & jitterc_instruction_argument_kind_fast_label)
        {
          jitterc_vm_last_instruction (vm)->has_fast_labels = true;
          if (arg->mode & jitterc_instruction_argument_mode_out)
            JITTERC_PARSE_ERROR("a fast label cannot be an output");
          if (arg->kind != jitterc_instruction_argument_kind_fast_label)
            JITTERC_PARSE_ERROR("a fast label must be the only kind");
        }
      jitterc_vm_append_argument (vm, arg);
    }
#line 2527 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 104:
#line 717 "../../jitter/jitterc/jitterc.y"
    {
      struct jitterc_instruction_argument *arg
        = jitterc_vm_last_argument (vm);
      if (   ! (arg->kind & jitterc_instruction_argument_kind_literal)
             && (yyvsp[-1].boolean))
        JITTERC_PARSE_ERROR("literals for a non-literal argument");
      if (   ! (arg->kind & jitterc_instruction_argument_kind_literal)
          && ((yyvsp[0].string) != NULL))
        JITTERC_PARSE_ERROR("a non-literal argument cannot have a printer");
      arg->c_literal_printer_name = (yyvsp[0].string);
    }
#line 2543 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 105:
#line 731 "../../jitter/jitterc/jitterc.y"
                 { (yyval.string) = NULL; }
#line 2549 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 106:
#line 732 "../../jitter/jitterc/jitterc.y"
                 { (yyval.string) = (yyvsp[0].string); }
#line 2555 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 107:
#line 737 "../../jitter/jitterc/jitterc.y"
    { if ((yyvsp[-1].mode) & (yyvsp[0].mode))
        JITTERC_PARSE_ERROR("duplicate mode");
      (yyval.mode) = (yyvsp[-1].mode) | (yyvsp[0].mode); }
#line 2563 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 108:
#line 744 "../../jitter/jitterc/jitterc.y"
    { (yyval.mode) = jitterc_instruction_argument_mode_unspecified; }
#line 2569 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 109:
#line 746 "../../jitter/jitterc/jitterc.y"
    { if ((yyvsp[-1].mode) & (yyvsp[0].mode))
        JITTERC_PARSE_ERROR("duplicate mode");
      (yyval.mode) = (yyvsp[-1].mode) | (yyvsp[0].mode); }
#line 2577 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 110:
#line 752 "../../jitter/jitterc/jitterc.y"
       { (yyval.mode) = jitterc_instruction_argument_mode_in; }
#line 2583 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 111:
#line 753 "../../jitter/jitterc/jitterc.y"
       { (yyval.mode) = jitterc_instruction_argument_mode_out; }
#line 2589 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 112:
#line 761 "../../jitter/jitterc/jitterc.y"
  {
    enum jitterc_instruction_argument_kind k
      = jitterc_instruction_argument_kind_unspecified;
    char c = JITTERC_TEXT [0];
    switch (c)
      {
      KIND_CASE('n', literal)
      KIND_CASE('l', label)
      KIND_CASE('f', fast_label)
      KIND_CASE_DEFAULT((yyval.bare_argument), c)
      }
    (yyval.bare_argument).kind = k;
  }
#line 2607 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 113:
#line 775 "../../jitter/jitterc/jitterc.y"
  {
    char *text = JITTERC_TEXT;
    enum jitterc_instruction_argument_kind k
      = jitterc_instruction_argument_kind_unspecified;
    int i;
    for (i = 0; text [i] != '\0'; i ++)
      switch (text [i])
        {
        KIND_CASE('n', literal)
        KIND_CASE('l', label)
        KIND_CASE('f', fast_label)
        KIND_CASE_DEFAULT((yyval.bare_argument), text [i])
        }
    (yyval.bare_argument).kind = k;
  }
#line 2627 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 118:
#line 800 "../../jitter/jitterc/jitterc.y"
        { JITTERC_SET_PROPERTY(hotness, hot); }
#line 2633 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 119:
#line 801 "../../jitter/jitterc/jitterc.y"
        { JITTERC_SET_PROPERTY(hotness, cold); }
#line 2639 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 120:
#line 805 "../../jitter/jitterc/jitterc.y"
                  { JITTERC_SET_PROPERTY(relocatability, relocatable); }
#line 2645 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 121:
#line 806 "../../jitter/jitterc/jitterc.y"
                  { JITTERC_SET_PROPERTY(relocatability, non_relocatable); }
#line 2651 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 122:
#line 810 "../../jitter/jitterc/jitterc.y"
           { JITTERC_SET_PROPERTY(callerness, caller); }
#line 2657 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 123:
#line 811 "../../jitter/jitterc/jitterc.y"
           { JITTERC_SET_PROPERTY(calleeness, callee);
             const struct jitterc_instruction *ins = jitterc_vm_last_instruction (vm);
             if (gl_list_size (ins->arguments) > 0)
               JITTERC_PARSE_ERROR("a callee instruction has arguments"); }
#line 2666 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 124:
#line 821 "../../jitter/jitterc/jitterc.y"
                            { (yyval.string) = JITTERC_TEXT_COPY; }
#line 2672 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 125:
#line 822 "../../jitter/jitterc/jitterc.y"
                            { (yyval.string) = JITTERC_TEXT_COPY; }
#line 2678 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 126:
#line 823 "../../jitter/jitterc/jitterc.y"
                            { (yyval.string) = JITTERC_TEXT_COPY; }
#line 2684 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 127:
#line 827 "../../jitter/jitterc/jitterc.y"
             { /* FIXME: unescape properly. */
               char *text = JITTERC_TEXT;
               text [strlen (text) - 1] = '\0'; text ++;
               (yyval.string) = jitter_clone_string (text); }
#line 2693 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 128:
#line 835 "../../jitter/jitterc/jitterc.y"
  { char *s = xmalloc (1);
    strcpy (s, "");
    int newline_no = 0;
    char *text = JITTERC_TEXT;
    int i;
    for (i = 0; text [i] != '\0'; i++)
      if (text [i] == '\n')
        newline_no ++;
    (yyval.code_block).line_number = JITTERC_LINENO - newline_no - 1;
    (yyval.code_block).code = JITTERC_TEXT_COPY;
    JITTERC_APPEND_CODE(s, & (yyval.code_block));
    (yyval.code_block).code = s;
  }
#line 2711 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 129:
#line 851 "../../jitter/jitterc/jitterc.y"
                  { /* Since the string has been matched by the scanner
                       the conversion is actually safe in this case. */
                    (yyval.fixnum) = jitter_string_to_long_long_unsafe (JITTERC_TEXT); }
#line 2719 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 130:
#line 854 "../../jitter/jitterc/jitterc.y"
                  { (yyval.fixnum) = JITTER_BITS_PER_WORD; }
#line 2725 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 131:
#line 855 "../../jitter/jitterc/jitterc.y"
                  { (yyval.fixnum) = JITTER_BYTES_PER_WORD; }
#line 2731 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 132:
#line 856 "../../jitter/jitterc/jitterc.y"
                  { (yyval.fixnum) = JITTER_LG_BYTES_PER_WORD; }
#line 2737 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 133:
#line 860 "../../jitter/jitterc/jitterc.y"
           { (yyval.boolean) = false; }
#line 2743 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 134:
#line 861 "../../jitter/jitterc/jitterc.y"
           { struct jitterc_instruction_argument *arg
               = jitterc_vm_last_argument (vm);
             /* FIXME: this will need generalization later on. */
             union jitterc_literal_value literal_value
               = {.fixnum = (yyvsp[0].fixnum)};
             struct jitterc_literal *literal
               = jitterc_make_literal (jitterc_literal_type_fixnum,
                                         literal_value);
             gl_list_add_last (arg->literals, literal); }
#line 2757 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 135:
#line 870 "../../jitter/jitterc/jitterc.y"
           { (yyval.boolean) = true; }
#line 2763 "../../jitter/jitterc/jitterc-parser.c"
    break;


#line 2767 "../../jitter/jitterc/jitterc-parser.c"

      default: break;
    }
  /* User semantic actions sometimes alter yychar, and that requires
     that yytoken be updated with the new translation.  We take the
     approach of translating immediately before every use of yytoken.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     YYABORT, YYACCEPT, or YYERROR immediately after altering yychar or
     if it invokes YYBACKUP.  In the case of YYABORT or YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of YYERROR or YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  YY_SYMBOL_PRINT ("-> $$ =", YY_CAST (yysymbol_kind_t, yyr1[yyn]), &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;

  *++yyvsp = yyval;
  *++yylsp = yyloc;

  /* Now 'shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */
  {
    const int yylhs = yyr1[yyn] - YYNTOKENS;
    const int yyi = yypgoto[yylhs] + *yyssp;
    yystate = (0 <= yyi && yyi <= YYLAST && yycheck[yyi] == *yyssp
               ? yytable[yyi]
               : yydefgoto[yylhs]);
  }

  goto yynewstate;


/*--------------------------------------.
| yyerrlab -- here on detecting error.  |
`--------------------------------------*/
yyerrlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  yytoken = yychar == JITTERC_EMPTY ? YYSYMBOL_YYEMPTY : YYTRANSLATE (yychar);
  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
      yyerror (&yylloc, vm, jitterc_scanner, YY_("syntax error"));
    }

  yyerror_range[1] = yylloc;
  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
         error, discard it.  */

      if (yychar <= JITTERC_EOF)
        {
          /* Return failure if at end of input.  */
          if (yychar == JITTERC_EOF)
            YYABORT;
        }
      else
        {
          yydestruct ("Error: discarding",
                      yytoken, &yylval, &yylloc, vm, jitterc_scanner);
          yychar = JITTERC_EMPTY;
        }
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:
  /* Pacify compilers when the user code never invokes YYERROR and the
     label yyerrorlab therefore never appears in user code.  */
  if (0)
    YYERROR;

  /* Do not reclaim the symbols of the rule whose action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;      /* Each real token shifted decrements this.  */

  /* Pop stack until we find a state that shifts the error token.  */
  for (;;)
    {
      yyn = yypact[yystate];
      if (!yypact_value_is_default (yyn))
        {
          yyn += YYSYMBOL_YYerror;
          if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYSYMBOL_YYerror)
            {
              yyn = yytable[yyn];
              if (0 < yyn)
                break;
            }
        }

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
        YYABORT;

      yyerror_range[1] = *yylsp;
      yydestruct ("Error: popping",
                  YY_ACCESSING_SYMBOL (yystate), yyvsp, yylsp, vm, jitterc_scanner);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END

  yyerror_range[2] = yylloc;
  ++yylsp;
  YYLLOC_DEFAULT (*yylsp, yyerror_range, 2);

  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", YY_ACCESSING_SYMBOL (yyn), yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;


/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;


#if !defined yyoverflow
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (&yylloc, vm, jitterc_scanner, YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif


/*-----------------------------------------------------.
| yyreturn -- parsing is finished, return the result.  |
`-----------------------------------------------------*/
yyreturn:
  if (yychar != JITTERC_EMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      yytoken = YYTRANSLATE (yychar);
      yydestruct ("Cleanup: discarding lookahead",
                  yytoken, &yylval, &yylloc, vm, jitterc_scanner);
    }
  /* Do not reclaim the symbols of the rule whose action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
                  YY_ACCESSING_SYMBOL (+*yyssp), yyvsp, yylsp, vm, jitterc_scanner);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif

  return yyresult;
}

#line 872 "../../jitter/jitterc/jitterc.y"


void
jitterc_error (YYLTYPE *locp, struct jitterc_vm *vm, yyscan_t jitterc_scanner,
                 char *message)
{
  printf ("%s:%i: %s near \"%s\".\n",
          (vm != NULL) ? vm->source_file_name : "<INPUT>",
          jitterc_get_lineno (jitterc_scanner), message, JITTERC_TEXT);
  exit (EXIT_FAILURE);
}

void
jitterc_scan_error (void *jitterc_scanner)
{
  struct jitterc_vm *vm = NULL; /* A little hack to have vm in scope. */
  JITTERC_PARSE_ERROR("scan error");
}

static struct jitterc_vm *
jitterc_parse_file_star_with_name (FILE *input_file, const char *file_name,
                                   bool generate_line)
{
  yyscan_t scanner;
  jitterc_lex_init (&scanner);
  jitterc_set_in (input_file, scanner);

  struct jitterc_vm *res = jitterc_make_vm ();
  res->source_file_name = jitter_clone_string (file_name);

  /* Set res->generate_line now, before the parsing phase actually starts.  This
     way the code generated at parsing time will be affected. */
  res->generate_line = generate_line;

  /* FIXME: if I ever make parsing errors non-fatal, call jitterc_lex_destroy before
     returning, and finalize the program -- which might be incomplete! */
  if (jitterc_parse (res, scanner))
    jitterc_error (jitterc_get_lloc (scanner), res, scanner, "parse error");
  jitterc_set_in (NULL, scanner);
  jitterc_lex_destroy (scanner);

  /* Now that we have all the unspecialized instructions and all the rules we
     can analyze the VM. */
  jitterc_analyze_vm (res);

  return res;
}

struct jitterc_vm *
jitterc_parse_file_star (FILE *input_file, bool generate_line)
{
  return jitterc_parse_file_star_with_name (input_file, "<stdin>",
                                            generate_line);
}

struct jitterc_vm *
jitterc_parse_file (const char *input_file_name, bool generate_line)
{
  FILE *f;
  if ((f = fopen (input_file_name, "r")) == NULL)
    jitter_fatal ("failed opening file %s", input_file_name);

  /* FIXME: if I ever make parse errors non-fatal, I'll need to close the file
     before returning. */
  struct jitterc_vm *res
    = jitterc_parse_file_star_with_name (f, input_file_name, generate_line);
  fclose (f);
  return res;
}
