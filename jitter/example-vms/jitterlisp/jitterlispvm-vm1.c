/* This code is machine-generated.  See its source for license
   information. This software is derived from software
   distributed under the GNU GPL version 3 or later. */

/* User-specified code, initial vm1 part: beginning. */

/* User-specified code, initial vm1 part: end */

/* VM library: main VM C file template.

   Copyright (C) 2016, 2017, 2018, 2019, 2020 Luca Saiu
   Written by Luca Saiu

   This file is part of Jitter.

   Jitter is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Jitter is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Jitter.  If not, see <http://www.gnu.org/licenses/>. */


/* Generated file warning.
 * ************************************************************************** */

/* Unless this file is named exactly "vm1.c" , without any prefix, you are
   looking at a machine-generated derived file.  The original source is the vm.c
   template from Jitter, with added code implementing the jitterlispvm VM. */




#include <assert.h>
#include <string.h>

#include <jitter/jitter.h>

#if defined (JITTER_PROFILE_SAMPLE)
#include <sys/time.h>
#endif // #if defined (JITTER_PROFILE_SAMPLE)

#include <jitter/jitter-hash.h>
#include <jitter/jitter-instruction.h>
#include <jitter/jitter-mmap.h>
#include <jitter/jitter-mutable-routine.h>
#include <jitter/jitter-print.h>
#include <jitter/jitter-rewrite.h>
#include <jitter/jitter-routine.h>
#include <jitter/jitter-routine-parser.h>
#include <jitter/jitter-specialize.h>
#include <jitter/jitter-defect.h>
#include <jitter/jitter-patch-in.h>

/* I don't need to include <jitter/jitter-executor.h> here, nor to define
   JITTER_THIS_CAN_INCLUDE_JITTER_EXECUTOR_H ; doing so carelessly might
   lead to subtle bugs, that it is better to prevent.
   Of course I can reconsider this decision in the future. */

#include <jitter/jitter-data-locations.h>

#include "jitterlispvm-vm.h"
//#include "jitterlispvm-specialized-instructions.h"
//#include "jitterlispvm-meta-instructions.h"
#include <jitter/jitter-fatal.h>




/* Check requirements for particular features.
 * ************************************************************************** */

/* VM sample-profiling is only supported with GCC.  Do not bother activating it
   with other compilers, if the numbers are unreliable in the end. */
#if  defined (JITTER_PROFILE_SAMPLE)        \
     && ! defined (JITTER_HAVE_ACTUAL_GCC)
# error "Sample-profiling is only reliable with GCC: it requires (machine-independent)"
# error "GNU C extended asm, and it is not worth supporting other compilers if"
# error "the numbers turn out to be unreliable in the end."
#endif




/* Machine-generated data structures.
 * ************************************************************************** */

/* Machine-generated data structures defining this VM.  Initializing a static
   struct is problematic, as it requires constant expressions for each field --
   and const pointers don't qualify.  This is why we initialize the struct
   fields below in jitterlispvm_initialize. */
static struct jitter_vm
the_jitterlispvm_vm;

struct jitter_vm * const
jitterlispvm_vm = & the_jitterlispvm_vm;

struct jitter_list_header * const
jitterlispvm_states = & the_jitterlispvm_vm.states;

/* It is convenient to have this initialised at start up, even before calling
   any initialisation function.  This makes it reliable to read this when, for
   example, handling --version . */
static const struct jitter_vm_configuration
jitterlispvm_vm_the_configuration
  = {
      JITTERLISPVM_LOWER_CASE_PREFIX /* lower_case_prefix */,
      JITTERLISPVM_UPPER_CASE_PREFIX /* upper_case_prefix */,
      JITTERLISPVM_MAX_FAST_REGISTER_NO_PER_CLASS
        /* max_fast_register_no_per_class */,
      JITTERLISPVM_MAX_NONRESIDUAL_LITERAL_NO /* max_nonresidual_literal_no */,
      JITTERLISPVM_DISPATCH_HUMAN_READABLE /* dispatch_human_readable */,
      /* The instrumentation field can be seen as a bit map.  See the comment
         in jitter/jitter-vm.h . */
      (jitter_vm_instrumentation_none
#if defined (JITTER_PROFILE_COUNT)
       | jitter_vm_instrumentation_count
#endif
#if defined (JITTER_PROFILE_SAMPLE)
       | jitter_vm_instrumentation_sample
#endif
       ) /* instrumentation */
    };

const struct jitter_vm_configuration * const
jitterlispvm_vm_configuration
  = & jitterlispvm_vm_the_configuration;




/* Initialization and finalization: internal functions, not for the user.
 * ************************************************************************** */

/* Initialize threads.  This only needs to be called once at initialization, and
   the user doesn't need to bother with it.  Defined along with the executor. */
void
jitterlispvm_initialize_threads (void);

/* Check that the encodings in enum jitter_specialized_instruction_opcode (as
   used in the specializer) are coherent with machine-generated code.  Making a
   mistake here would introduce subtle bugs, so it's better to be defensive. */
static void
jitterlispvm_check_specialized_instruction_opcode_once (void)
{
  static bool already_checked = false;
  if (already_checked)
    return;

  assert (((enum jitter_specialized_instruction_opcode)
           jitterlispvm_specialized_instruction_opcode__eINVALID)
          == jitter_specialized_instruction_opcode_INVALID);
  assert (((enum jitter_specialized_instruction_opcode)
           jitterlispvm_specialized_instruction_opcode__eBEGINBASICBLOCK)
          == jitter_specialized_instruction_opcode_BEGINBASICBLOCK);
  assert (((enum jitter_specialized_instruction_opcode)
           jitterlispvm_specialized_instruction_opcode__eEXITVM)
          == jitter_specialized_instruction_opcode_EXITVM);
  assert (((enum jitter_specialized_instruction_opcode)
           jitterlispvm_specialized_instruction_opcode__eDATALOCATIONS)
          == jitter_specialized_instruction_opcode_DATALOCATIONS);
  assert (((enum jitter_specialized_instruction_opcode)
           jitterlispvm_specialized_instruction_opcode__eNOP)
          == jitter_specialized_instruction_opcode_NOP);
  assert (((enum jitter_specialized_instruction_opcode)
           jitterlispvm_specialized_instruction_opcode__eUNREACHABLE0)
          == jitter_specialized_instruction_opcode_UNREACHABLE0);
  assert (((enum jitter_specialized_instruction_opcode)
           jitterlispvm_specialized_instruction_opcode__eUNREACHABLE1)
          == jitter_specialized_instruction_opcode_UNREACHABLE1);
  assert (((enum jitter_specialized_instruction_opcode)
           jitterlispvm_specialized_instruction_opcode__eUNREACHABLE2)
          == jitter_specialized_instruction_opcode_UNREACHABLE2);

  already_checked = true;
}

/* A prototype for a machine-generated function not needing a public
   declaration, only called thru a pointer within struct jitter_vm . */
int
jitterlispvm_specialize_instruction (struct jitter_mutable_routine *p,
                                 const struct jitter_instruction *ins);

/* Initialize the pointed special-purpose data structure. */
static void
jitterlispvm_initialize_special_purpose_data
   (volatile struct jitter_special_purpose_state_data *d)
{
  d->pending_notifications = 0;
  jitter_initialize_pending_signal_notifications
     (& d->pending_signal_notifications);

  /* Initialise profiling fields. */
  jitter_profile_runtime_initialize (jitterlispvm_vm,
                                     (struct jitter_profile_runtime *)
                                     & d->profile_runtime);
}

/* Finalize the pointed special-purpose data structure. */
static void
jitterlispvm_finalize_special_purpose_data
   (volatile struct jitter_special_purpose_state_data *d)
{
  jitter_finalize_pending_signal_notifications
     (d->pending_signal_notifications);

  jitter_profile_runtime_finalize (jitterlispvm_vm,
                                   (struct jitter_profile_runtime *)
                                   & d->profile_runtime);
}




/* Check that we link with the correct Jitter library.
 * ************************************************************************** */

/* It is possible to make a mistake at link time, and link a VM compiled with
   some threading model with the Jitter runtime for a different model.  That
   would cause crashes, that is better to prevent.  This is a way to detect such
   mistakes very early, by causing a link-time failure in case of mismatch. */
extern volatile const bool
JITTER_DISPATCH_DEPENDENT_GLOBAL_NAME;




/* Low-level debugging features relying on assembly: data locations.
 * ************************************************************************** */

#if defined (JITTER_HAVE_KNOWN_BINARY_FORMAT) && ! defined (JITTER_DISPATCH_SWITCH)
/* A declaration for data locations, as visible from C.  The global is defined in
   assembly in its own separate section thru the machinery in
   jitter/jitter-sections.h . */
extern const char
JITTER_DATA_LOCATION_NAME(jitterlispvm) [];
#endif // #if ...

void
jitterlispvm_dump_data_locations (jitter_print_context output)
{
#ifndef JITTER_DISPATCH_SWITCH
  jitter_dump_data_locations (output, & the_jitterlispvm_vm);
#else
  jitter_print_char_star (output,
                          "VM data location information unavailable\n");
#endif // #ifndef JITTER_DISPATCH_SWITCH
}




/* Initialization and finalization.
 * ************************************************************************** */

#ifdef JITTER_HAVE_PATCH_IN
JITTER_DEFECT_DESCRIPTOR_DECLARATIONS_(jitterlispvm)
JITTER_PATCH_IN_DESCRIPTOR_DECLARATIONS_(jitterlispvm)
#endif // #ifdef JITTER_HAVE_PATCH_IN

#ifndef JITTER_DISPATCH_SWITCH
/* True iff thread sizes are all non-negative and non-huge.  We refuse to
   disassemble otherwise, and when replication is enabled we refuse to run
   altogether.  See the comment right below. */
static bool
jitterlispvm_threads_validated = false;
#endif // #ifndef JITTER_DISPATCH_SWITCH

/* Omit jitterlispvm_validate_thread_sizes_once for switch-dispatching, as threads
   don't exist at all in that case.*/
#ifndef JITTER_DISPATCH_SWITCH
/* Check that VM instruction sizes are all non-negative, and that no thread
   starts before the end of the previous one.  Even one violation of such
   conditions is a symptom that the code has not been compiled with
   -fno-reorder-blocks , which would have disastrous effects with replication.
   It's better to validate threads at startup and fail immediately than to crash
   at run time.

   If even one thread appears to be wrong then refuse to disassemble when
   replication is disabled, and refuse to run altogether if replication is
   enabled. */
static void
jitterlispvm_validate_threads_once (void)
{
  /* Return if this is not the first time we got here. */
  static bool already_validated = false;
  if (already_validated)
    return;

#ifdef JITTER_REPLICATE
# define JITTER_FAIL(error_text)                                             \
    do                                                                       \
      {                                                                      \
        fprintf (stderr,                                                     \
                 "About specialized instruction %i (%s) at %p, size %liB\n", \
                 i, jitterlispvm_specialized_instruction_names [i],              \
                 jitterlispvm_threads [i],                                       \
                 jitterlispvm_thread_sizes [i]);                                 \
        jitter_fatal ("%s: you are not compiling with -fno-reorder-blocks",  \
                      error_text);                                           \
      }                                                                      \
    while (false)
#else
# define JITTER_FAIL(ignored_error_text)  \
    do                                    \
      {                                   \
        everything_valid = false;         \
        goto out;                         \
      }                                   \
    while (false)
#endif // #ifdef JITTER_REPLICATE

  /* The minimum address the next instruction code has to start at.

     This relies on NULL being zero, or in general lower in magnitude than any
     valid pointer.  It is not worth the trouble to be pedantic, as this will be
     true on every architecture where I can afford low-level tricks. */
  jitter_thread lower_bound = NULL;

  /* Check every thread.  We rely on the order here, following specialized
     instruction opcodes. */
  int i;
  bool everything_valid = true;
  for (i = 0; i < JITTERLISPVM_SPECIALIZED_INSTRUCTION_NO; i ++)
    {
      jitter_thread thread = jitterlispvm_threads [i];
      long size = jitterlispvm_thread_sizes [i];

      /* Check that the current thread has non-negative non-huge size and
         doesn't start before the end of the previous one.  If this is true for
         all threads we can conclude that they are non-overlapping as well. */
      if (__builtin_expect (size < 0, false))
        JITTER_FAIL("a specialized instruction has negative code size");
      if (__builtin_expect (size > (1 << 24), false))
        JITTER_FAIL("a specialized instruction has huge code size");
      if (__builtin_expect (lower_bound > thread, false))
        JITTER_FAIL("non-sequential thread");

      /* The next thread cannot start before the end of the current one. */
      lower_bound = ((char*) thread) + size;
    }

#undef JITTER_FAIL

#ifndef JITTER_REPLICATE
 out:
#endif // #ifndef JITTER_REPLICATE

  /* If we have validated every thread size then disassembling appears safe. */
  if (everything_valid)
    jitterlispvm_threads_validated = true;

  /* We have checked the thread sizes, once and for all.  If this function gets
     called again, thru a second jitterlispvm initialization, it will immediately
     return. */
  already_validated = true;
}
#endif // #ifndef JITTER_DISPATCH_SWITCH

#ifdef JITTER_HAVE_PATCH_IN
/* The actual defect table.  We only need it when patch-ins are in use. */
jitter_uint
jitterlispvm_defect_table [JITTERLISPVM_SPECIALIZED_INSTRUCTION_NO];
#endif // #ifdef JITTER_HAVE_PATCH_IN

void
jitterlispvm_initialize (void)
{
  /* Check that the Jitter library we linked is the right one.  This check
     actually only useful to force the global to be used.  I prefer not to use
     an assert, because assertions can be disabled. */
  if (! JITTER_DISPATCH_DEPENDENT_GLOBAL_NAME)
    jitter_fatal ("impossible to reach: the thing should fail at link time");

#ifdef JITTER_REPLICATE
  /* Initialize the executable-memory subsystem. */
  jitter_initialize_executable ();
#endif // #ifdef JITTER_REPLICATE

  /* Initialise the print-context machinery. */
  jitter_print_initialize ();

  /* Perform some sanity checks which only need to be run once. */
  jitterlispvm_check_specialized_instruction_opcode_once ();

  /* We have to initialize threads before jitterlispvm_threads , since the struct
     needs threads. */
  jitterlispvm_initialize_threads ();

#ifndef JITTER_DISPATCH_SWITCH
  /* Validate threads, to make sure the generated code was not compiled with
     incorrect options.  This only needs to be done once. */
  jitterlispvm_validate_threads_once ();
#endif // ifndef JITTER_DISPATCH_SWITCH

  /* Initialize the object pointed by jitterlispvm_vm (see the comment above as to
     why we do it here).  Before actually setting the fields to valid data, fill
     the whole struct with a -- hopefully -- invalid pattern, just to catch
     bugs. */
  static bool vm_struct_initialized = false;
  if (! vm_struct_initialized)
    {
      memset (& the_jitterlispvm_vm, 0xff, sizeof (struct jitter_vm));

      /* Make the configuration struct reachable from the VM struct. */
      the_jitterlispvm_vm.configuration = jitterlispvm_vm_configuration;
      //jitterlispvm_print_vm_configuration (stdout, & the_jitterlispvm_vm.configuration);

      /* Initialize meta-instruction pointers for implicit instructions.
         VM-independent program specialization relies on those, so they have to
         be accessible to the Jitter library, out of generated code.  Since
         meta-instructions are sorted alphabetically in the array, the index
         is not fixed. */
      the_jitterlispvm_vm.exitvm_meta_instruction
        = (jitterlispvm_meta_instructions + jitterlispvm_meta_instruction_id_exitvm);
      the_jitterlispvm_vm.unreachable_meta_instruction
        = (jitterlispvm_meta_instructions
           + jitterlispvm_meta_instruction_id_unreachable);

      /* Threads or pointers to native code blocks of course don't exist with
   switch-dispatching. */
#ifndef JITTER_DISPATCH_SWITCH
      the_jitterlispvm_vm.threads = (jitter_thread *)jitterlispvm_threads;
      the_jitterlispvm_vm.thread_sizes = (long *) jitterlispvm_thread_sizes;
      the_jitterlispvm_vm.threads_validated = jitterlispvm_threads_validated;
#if defined (JITTER_HAVE_KNOWN_BINARY_FORMAT)
      the_jitterlispvm_vm.data_locations = JITTER_DATA_LOCATION_NAME(jitterlispvm);
#else
      the_jitterlispvm_vm.data_locations = NULL;
#endif // #if defined (JITTER_HAVE_KNOWN_BINARY_FORMAT)
#endif // #ifndef JITTER_DISPATCH_SWITCH

      the_jitterlispvm_vm.specialized_instruction_residual_arities
        = jitterlispvm_specialized_instruction_residual_arities;
      the_jitterlispvm_vm.specialized_instruction_label_bitmasks
        = jitterlispvm_specialized_instruction_label_bitmasks;
#ifdef JITTER_HAVE_PATCH_IN
      the_jitterlispvm_vm.specialized_instruction_fast_label_bitmasks
        = jitterlispvm_specialized_instruction_fast_label_bitmasks;
      the_jitterlispvm_vm.patch_in_descriptors =
        JITTER_PATCH_IN_DESCRIPTORS_NAME(jitterlispvm);
      const size_t patch_in_descriptor_size
        = sizeof (struct jitter_patch_in_descriptor);
      the_jitterlispvm_vm.patch_in_descriptor_no
        = (JITTER_PATCH_IN_DESCRIPTORS_SIZE_IN_BYTES_NAME(jitterlispvm)
           / patch_in_descriptor_size);
      /* Cheap sanity check: if the size in bytes is not a multiple of
         the element size, we're doing something very wrong. */
      if (JITTER_PATCH_IN_DESCRIPTORS_SIZE_IN_BYTES_NAME(jitterlispvm)
          % patch_in_descriptor_size != 0)
        jitter_fatal ("patch-in descriptors total size not a multiple "
                      "of the element size");
      /* Initialize the patch-in table for this VM. */
      the_jitterlispvm_vm.patch_in_table
        = jitter_make_patch_in_table (the_jitterlispvm_vm.patch_in_descriptors,
                                      the_jitterlispvm_vm.patch_in_descriptor_no,
                                      JITTERLISPVM_SPECIALIZED_INSTRUCTION_NO);
#else
      the_jitterlispvm_vm.specialized_instruction_fast_label_bitmasks = NULL;
#endif // #ifdef JITTER_HAVE_PATCH_IN

      /* FIXME: I might want to conditionalize this. */
      the_jitterlispvm_vm.specialized_instruction_relocatables
        = jitterlispvm_specialized_instruction_relocatables;

      the_jitterlispvm_vm.specialized_instruction_callers
        = jitterlispvm_specialized_instruction_callers;
      the_jitterlispvm_vm.specialized_instruction_callees
        = jitterlispvm_specialized_instruction_callees;

      the_jitterlispvm_vm.specialized_instruction_names
        = jitterlispvm_specialized_instruction_names;
      the_jitterlispvm_vm.specialized_instruction_no
        = JITTERLISPVM_SPECIALIZED_INSTRUCTION_NO;

      the_jitterlispvm_vm.meta_instruction_string_hash
        = & jitterlispvm_meta_instruction_hash;
      the_jitterlispvm_vm.meta_instructions
        = (struct jitter_meta_instruction *) jitterlispvm_meta_instructions;
      the_jitterlispvm_vm.meta_instruction_no = JITTERLISPVM_META_INSTRUCTION_NO;
      the_jitterlispvm_vm.max_meta_instruction_name_length
        = JITTERLISPVM_MAX_META_INSTRUCTION_NAME_LENGTH;
      the_jitterlispvm_vm.specialized_instruction_to_unspecialized_instruction
        = jitterlispvm_specialized_instruction_to_unspecialized_instruction;
      the_jitterlispvm_vm.register_class_character_to_register_class
        = jitterlispvm_register_class_character_to_register_class;
      the_jitterlispvm_vm.specialize_instruction = jitterlispvm_specialize_instruction;
      the_jitterlispvm_vm.rewrite = jitterlispvm_rewrite;

#ifdef JITTER_HAVE_PATCH_IN
      /* Fill the defect table.  Since the array in question is a global with a
         fixed size, this needs to be done only once. */
      jitter_fill_defect_table (jitterlispvm_defect_table,
                                & the_jitterlispvm_vm,
                                jitterlispvm_worst_case_defect_table,
                                JITTER_DEFECT_DESCRIPTORS_NAME (jitterlispvm),
                                (JITTER_DEFECT_DESCRIPTORS_SIZE_IN_BYTES_NAME
                                    (jitterlispvm)
                                 / sizeof (struct jitter_defect_descriptor)));
#endif // #ifdef JITTER_HAVE_PATCH_IN

      /* Initialize the empty list of states. */
      JITTER_LIST_INITIALIZE_HEADER (& the_jitterlispvm_vm.states);

      vm_struct_initialized = true;
    }

  jitter_initialize_meta_instructions (& jitterlispvm_meta_instruction_hash,
                                         jitterlispvm_meta_instructions,
                                         JITTERLISPVM_META_INSTRUCTION_NO);

#ifdef JITTER_HAVE_PATCH_IN
  jitter_dump_defect_table (stderr, jitterlispvm_defect_table, & the_jitterlispvm_vm);
#endif // #ifdef JITTER_HAVE_PATCH_IN
}

void
jitterlispvm_finalize (void)
{
  /* There's no need to touch the_jitterlispvm_vm ; we can keep it as it is, as it
     contains no dynamically-allocated fields. */
  /* Threads need no finalization. */
  jitter_finalize_meta_instructions (& jitterlispvm_meta_instruction_hash);

#ifdef JITTER_HAVE_PATCH_IN
  /* Destroy the patch-in table for this VM. */
  jitter_destroy_patch_in_table (the_jitterlispvm_vm.patch_in_table,
                                 JITTERLISPVM_SPECIALIZED_INSTRUCTION_NO);
#endif // #ifdef JITTER_HAVE_PATCH_IN

#ifdef JITTER_REPLICATE
  /* Finalize the executable-memory subsystem. */
  jitter_finalize_executable ();
#endif // #ifdef JITTER_REPLICATE

  /* Finalize the state list.  If it is not empty then something has gone
     wrong earlier. */
  if (the_jitterlispvm_vm.states.first != NULL
      || the_jitterlispvm_vm.states.last != NULL)
    jitter_fatal ("not every state structure was destroyed before JITTERLISPVM "
                  "finalisation.");
}




/* VM-dependant mutable routine initialization.
 * ************************************************************************** */

struct jitter_mutable_routine*
jitterlispvm_make_mutable_routine (void)
{
  return jitter_make_mutable_routine (jitterlispvm_vm);
}




/* Sample profiling: internal API.
 * ************************************************************************** */

#if defined (JITTER_PROFILE_SAMPLE)

/* Sample profiling depends on some system features: fail immediately if they
   are not available */
#if ! defined (JITTER_HAVE_SIGACTION) || ! defined (JITTER_HAVE_SETITIMER)
# jitter_fatal "sample-profiling depends on sigaction and setitimer"
#endif

static struct itimerval
jitterlispvm_timer_interval;

static struct itimerval
jitterlispvm_timer_disabled_interval;

/* The sampling data, currently global.  The current implementation does not
   play well with threads, but it can be changed later keeping the same user
   API. */
struct jitterlispvm_sample_profile_state
{
  /* The state currently sample-profiling.  Since such a state can be only one
     right now this field is useful for printing error messages in case the user
     sets up sample-profiling from two states at the same time by mistake.
     This field is also useful for temporarily suspending and then reenabling
     sampling, when The Array is being resized: if the signal handler sees that
     this field is NULL it will not touch the fields. */
  struct jitterlispvm_state *state_p;

  /* A pointer to the counts field within the sample_profile_runtime struct. */
  uint32_t *counts;

  /* A pointer to the current specialised instruction opcode within the
     sample_profile_runtime struct. */
  volatile jitter_int * specialized_opcode_p;

  /* A pointer to the field counting the number of samples, again within the
     sample_profile_runtime struct. */
  unsigned int *sample_no_p;
};

/* The (currently) one and only global state for sample-profiling. */
static struct jitterlispvm_sample_profile_state
jitterlispvm_sample_profile_state;

static void
jitterlispvm_sigprof_handler (int signal)
{
#if 0
  assert (jitterlispvm_sample_profile_state.state_p != NULL);
#endif

  jitter_int specialized_opcode
    = * jitterlispvm_sample_profile_state.specialized_opcode_p;
  if (__builtin_expect ((specialized_opcode >= 0
                         && (specialized_opcode
                             < JITTERLISPVM_SPECIALIZED_INSTRUCTION_NO)),
                        true))
    jitterlispvm_sample_profile_state.counts [specialized_opcode] ++;

  (* jitterlispvm_sample_profile_state.sample_no_p) ++;
}

void
jitterlispvm_profile_sample_initialize (void)
{
  /* Perform a sanity check over the sampling period. */
  if (JITTER_PROFILE_SAMPLE_PERIOD_IN_MILLISECONDS <= 0 ||
      JITTER_PROFILE_SAMPLE_PERIOD_IN_MILLISECONDS >= 1000)
    jitter_fatal ("invalid JITTER_PROFILE_SAMPLE_PERIOD_IN_MILLISECONDS: %f",
                  (double) JITTER_PROFILE_SAMPLE_PERIOD_IN_MILLISECONDS);
  struct sigaction action;
  sigaction (SIGPROF, NULL, & action);
  action.sa_handler = jitterlispvm_sigprof_handler;
  sigaction (SIGPROF, & action, NULL);

  long microseconds
    = (long) (JITTER_PROFILE_SAMPLE_PERIOD_IN_MILLISECONDS * 1000);
  jitterlispvm_timer_interval.it_interval.tv_sec = 0;
  jitterlispvm_timer_interval.it_interval.tv_usec = microseconds;
  jitterlispvm_timer_interval.it_value = jitterlispvm_timer_interval.it_interval;

  jitterlispvm_sample_profile_state.state_p = NULL;
  jitterlispvm_timer_disabled_interval.it_interval.tv_sec = 0;
  jitterlispvm_timer_disabled_interval.it_interval.tv_usec = 0;
  jitterlispvm_timer_disabled_interval.it_value
    = jitterlispvm_timer_disabled_interval.it_interval;
}

void
jitterlispvm_profile_sample_start (struct jitterlispvm_state *state_p)
{
  struct jitter_sample_profile_runtime *spr
    = ((struct jitter_sample_profile_runtime *)
       & JITTERLISPVM_STATE_TO_SPECIAL_PURPOSE_STATE_DATA (state_p)
           ->profile_runtime.sample_profile_runtime);

  if (jitterlispvm_sample_profile_state.state_p != NULL)
    {
      if (state_p != jitterlispvm_sample_profile_state.state_p)
        jitter_fatal ("currently it is only possible to sample-profile from "
                      "one state at the time: trying to sample-profile from "
                      "the state %p when already sample-profiling from the "
                      "state %p",
                      state_p, jitterlispvm_sample_profile_state.state_p);
      else
        {
          /* This situation is a symptom of a bug, but does not need to lead
             to a fatal error. */
          printf ("WARNING: starting profile on the state %p when profiling "
                  "was already active in the same state.\n"
                  "Did you call longjmp from VM code?", state_p);
          fflush (stdout);
        }
    }
  jitterlispvm_sample_profile_state.state_p = state_p;
  jitterlispvm_sample_profile_state.sample_no_p = & spr->sample_no;
  jitterlispvm_sample_profile_state.counts = spr->counts;
  jitterlispvm_sample_profile_state.specialized_opcode_p
    = & spr->current_specialized_instruction_opcode;
  //fprintf (stderr, "SAMPLE START\n"); fflush (NULL);
  if (setitimer (ITIMER_PROF, & jitterlispvm_timer_interval, NULL) != 0)
    jitter_fatal ("setitimer failed when establishing a timer");
}

void
jitterlispvm_profile_sample_stop (void)
{
  if (setitimer (ITIMER_PROF, & jitterlispvm_timer_disabled_interval, NULL) != 0)
    jitter_fatal ("setitimer failed when disabling a timer");

  jitterlispvm_sample_profile_state.state_p = NULL;

  /* The rest is just for defenisveness' sake. */
  * jitterlispvm_sample_profile_state.specialized_opcode_p = -1;
  jitterlispvm_sample_profile_state.sample_no_p = NULL;
  jitterlispvm_sample_profile_state.counts = NULL;
  jitterlispvm_sample_profile_state.specialized_opcode_p = NULL;
}
#endif // #if defined (JITTER_PROFILE_SAMPLE)




/* Array re-allocation.
 * ************************************************************************** */

char *
jitterlispvm_make_place_for_slow_registers (struct jitterlispvm_state *s,
                                        jitter_int new_slow_register_no_per_class)
{
  if (new_slow_register_no_per_class < 0)
    jitter_fatal ("jitterlispvm_make_place_for_slow_registers: negative slow "
                  "register number");
  jitter_int old_slow_register_no_per_class
    = s->jitterlispvm_state_backing.jitter_slow_register_no_per_class;
  /* Change nothing if we already have enough space for the required number of
     slow registers.  The no-change case will be the most common one, and
     this function might be worth optimizing. */
  if (__builtin_expect (new_slow_register_no_per_class
                        > old_slow_register_no_per_class,
                        false))
    {
#if defined (JITTER_PROFILE_SAMPLE)
      /* If sample-profiling is currently in progress on this state suspend it
         temporarily. */
      bool suspending_sample_profiling
        = (jitterlispvm_sample_profile_state.state_p == s);
      if (suspending_sample_profiling)
        jitterlispvm_profile_sample_stop ();
#endif // #if defined (JITTER_PROFILE_SAMPLE)

#if 0
      printf ("Increasing slow register-no (per class) from %li to %li\n", (long) old_slow_register_no_per_class, (long)new_slow_register_no_per_class);
      printf ("Array size %li -> %li\n", (long) JITTERLISPVM_ARRAY_SIZE(old_slow_register_no_per_class), (long) JITTERLISPVM_ARRAY_SIZE(new_slow_register_no_per_class));
#endif
      /* Save the new value for new_slow_register_no_per_class in the state
         structure; reallocate the Array. */
      s->jitterlispvm_state_backing.jitter_slow_register_no_per_class
        = new_slow_register_no_per_class;
      s->jitterlispvm_state_backing.jitter_array
        = jitter_xrealloc ((void *) s->jitterlispvm_state_backing.jitter_array,
                           JITTERLISPVM_ARRAY_SIZE(new_slow_register_no_per_class));

      /* Initialise the slow registers we have just added, for every class. */
      union jitterlispvm_any_register *first_slow_register
        = ((union jitterlispvm_any_register *)
           ((char *) s->jitterlispvm_state_backing.jitter_array
            + JITTERLISPVM_FIRST_SLOW_REGISTER_UNBIASED_OFFSET));
      jitter_int i;
      for (i = old_slow_register_no_per_class;
           i < new_slow_register_no_per_class;
           i ++)
        {
          /* A pointer to the i-th rank of slow registers.  Every register
             in the rank is new and in general (according to its class) may
             need initialisation. */
          union jitterlispvm_any_register *rank
            = first_slow_register + (i * JITTERLISPVM_REGISTER_CLASS_NO);
          JITTERLISPVM_INITIALIZE_SLOW_REGISTER_RANK (rank);
        }
#if defined (JITTER_PROFILE_SAMPLE)
      /* Now we can resume sample-profiling on this state if we suspended it. */
      if (suspending_sample_profiling)
        jitterlispvm_profile_sample_start (s);
#endif // #if defined (JITTER_PROFILE_SAMPLE)
#if 0
      printf ("Done resizing The Array\n");
#endif
    }

  /* Return the new (or unchanged) base, by simply adding the bias to the
     Array as it is now. */
  return s->jitterlispvm_state_backing.jitter_array + JITTER_ARRAY_BIAS;
}

void
jitterlispvm_ensure_enough_slow_registers_for_executable_routine
   (const struct jitter_executable_routine *er, struct jitterlispvm_state *s)
{
  jitterlispvm_make_place_for_slow_registers (s, er->slow_register_per_class_no);
}




/* Program text frontend.
 * ************************************************************************** */

void
jitterlispvm_parse_mutable_routine_from_file_star (FILE *input_file,
                                               struct jitter_mutable_routine *p)
{
  jitter_parse_mutable_routine_from_file_star (input_file, p, jitterlispvm_vm);
}

void
jitterlispvm_parse_mutable_routine_from_file (const char *input_file_name,
                                          struct jitter_mutable_routine *p)
{
  jitter_parse_mutable_routine_from_file (input_file_name, p, jitterlispvm_vm);
}

void
jitterlispvm_parse_mutable_routine_from_string (const char *string,
                                            struct jitter_mutable_routine *p)
{
  jitter_parse_mutable_routine_from_string (string, p, jitterlispvm_vm);
}




/* Executing code: unified routine API.
 * ************************************************************************** */

void
jitterlispvm_ensure_enough_slow_registers_for_routine
   (jitter_routine r, struct jitterlispvm_state *s)
{
  struct jitter_executable_routine *e
    = jitter_routine_make_executable_if_needed (r);
  jitterlispvm_ensure_enough_slow_registers_for_executable_routine (e, s);
}

void
jitterlispvm_execute_routine (jitter_routine r,
                          struct jitterlispvm_state *s)
{
  struct jitter_executable_routine *e
    = jitter_routine_make_executable_if_needed (r);
  jitterlispvm_execute_executable_routine (e, s);
}




/* Profiling: user API.
 * ************************************************************************** */

/* These functions are all trivial wrappers around the functionality declared
   in jitter/jitter-profile.h, hiding the VM pointer. */

struct jitterlispvm_profile_runtime *
jitterlispvm_state_profile_runtime (struct jitterlispvm_state *s)
{
  volatile struct jitter_special_purpose_state_data *spd
    = JITTERLISPVM_ARRAY_TO_SPECIAL_PURPOSE_STATE_DATA
        (s->jitterlispvm_state_backing.jitter_array);
  return (struct jitterlispvm_profile_runtime *) & spd->profile_runtime;
}

struct jitterlispvm_profile_runtime *
jitterlispvm_profile_runtime_make (void)
{
  return jitter_profile_runtime_make (jitterlispvm_vm);
}

void
jitterlispvm_profile_runtime_clear (struct jitterlispvm_profile_runtime * p)
{
  jitter_profile_runtime_clear (jitterlispvm_vm, p);
}

void
jitterlispvm_profile_runtime_merge_from (struct jitterlispvm_profile_runtime *to,
                                     const struct jitterlispvm_profile_runtime *from)
{
  jitter_profile_runtime_merge_from (jitterlispvm_vm, to, from);
}

void
jitterlispvm_profile_runtime_merge_from_state (struct jitterlispvm_profile_runtime *to,
                                           const struct jitterlispvm_state *from_state)
{
  const struct jitterlispvm_profile_runtime* from
    = jitterlispvm_state_profile_runtime ((struct jitterlispvm_state *) from_state);
  jitter_profile_runtime_merge_from (jitterlispvm_vm, to, from);
}

void
jitterlispvm_profile_runtime_print_unspecialized
   (jitter_print_context ct,
    const struct jitterlispvm_profile_runtime *p)
{
  jitter_profile_runtime_print_unspecialized (ct, jitterlispvm_vm, p);
}

void
jitterlispvm_profile_runtime_print_specialized (jitter_print_context ct,
                                            const struct jitterlispvm_profile_runtime
                                            *p)
{
  jitter_profile_runtime_print_specialized (ct, jitterlispvm_vm, p);
}

struct jitterlispvm_profile *
jitterlispvm_profile_unspecialized_from_runtime
   (const struct jitterlispvm_profile_runtime *p)
{
  return jitter_profile_unspecialized_from_runtime (jitterlispvm_vm, p);
}

struct jitterlispvm_profile *
jitterlispvm_profile_specialized_from_runtime (const struct jitterlispvm_profile_runtime
                                           *p)
{
  return jitter_profile_specialized_from_runtime (jitterlispvm_vm, p);
}




/* Evrything following this point is machine-generated.
 * ************************************************************************** */

/* What follows could be conceptually split into several generated C files, but
   having too many of them would be inconvenient for the user to compile and
   link.  For this reason we currently generate just three files: one is this,
   which also contains the specializer, another is for the executor, and then a
   header -- a main module is optional.  The executor will be potentially very
   large, so it is best compiled separately.  The specializer might be large as
   well at this stage, even if its compilation is usually much less
   expensive. */
/* These two macros are convenient for making VM-specific identifiers
   using VM-independent macros from a public header, without polluting
   the global namespace. */
#define JITTER_VM_PREFIX_LOWER_CASE jitterlispvm
#define JITTER_VM_PREFIX_UPPER_CASE JITTERLISPVM

/* User-specified code, printer part: beginning. */

    /* Not really needed.  The printer is jitterlisp_print. */
  
/* User-specified code, printer part: end */

//#include <stdbool.h>

//#include <jitter/jitter.h>
//#include <jitter/jitter-instruction.h>

//#include "jitterlispvm-meta-instructions.h"

// FIXME: comment.
struct jitter_hash_table
jitterlispvm_meta_instruction_hash;


static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_at_mdepth_mto_mregister_meta_instruction_parameter_types [2] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, jitter_default_literal_parameter_printer }, { jitter_meta_instruction_parameter_kind_register, & jitterlispvm_register_class_r, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_branch_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_branch_mif_mfalse_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_branch_mif_mnot_mless_meta_instruction_parameter_types [2] =
  { { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer }, { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_branch_mif_mnot_mnull_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_branch_mif_mnull_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_branch_mif_mregister_mnon_mzero_meta_instruction_parameter_types [3] =
  { { jitter_meta_instruction_parameter_kind_register, & jitterlispvm_register_class_r, jitter_default_literal_parameter_printer }, { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer }, { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_branch_mif_mtrue_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_call_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_call_mcompiled_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_check_mclosure_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_check_mglobal_mdefined_meta_instruction_parameter_types [2] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, jitterlisp_print }, { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_check_min_marity_meta_instruction_parameter_types [2] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, jitter_default_literal_parameter_printer }, { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_check_min_marity_m_malt_meta_instruction_parameter_types [2] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, jitter_default_literal_parameter_printer }, { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_copy_mfrom_mliteral_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, jitterlisp_print } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_copy_mfrom_mregister_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_register, & jitterlispvm_register_class_r, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_copy_mto_mregister_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_register, & jitterlispvm_register_class_r, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_gc_mif_mneeded_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_heap_mallocate_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_literal_mto_mregister_meta_instruction_parameter_types [2] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, jitterlisp_print }, { jitter_meta_instruction_parameter_kind_register, & jitterlispvm_register_class_r, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_nip_mpush_mliteral_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_nip_mpush_mregister_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_register, & jitterlispvm_register_class_r, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_pop_mto_mglobal_meta_instruction_parameter_types [2] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, jitterlisp_print }, { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_pop_mto_mglobal_mdefined_meta_instruction_parameter_types [2] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, jitterlisp_print }, { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_pop_mto_mregister_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_register, & jitterlispvm_register_class_r, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_primitive_meta_instruction_parameter_types [3] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, jitter_default_literal_parameter_printer }, { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, jitter_default_literal_parameter_printer }, { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_primitive_mbox_mget_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_primitive_mbox_msetb_mspecial_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_primitive_mcar_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_primitive_mcdr_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_primitive_mfixnum_meqp_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_primitive_mfixnum_mnot_meqp_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_primitive_mgreaterp_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_primitive_mlessp_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_primitive_mnegate_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_primitive_mnegativep_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_primitive_mnon_mnegativep_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_primitive_mnon_mpositivep_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_primitive_mnon_mzerop_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_primitive_mnot_mgreaterp_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_primitive_mnot_mlessp_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_primitive_mone_mminus_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_primitive_mone_mplus_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_primitive_mpositivep_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_primitive_mprimordial_mdivided_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_primitive_mprimordial_mdivided_munsafe_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_primitive_mprimordial_mminus_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_primitive_mprimordial_mplus_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_primitive_mprimordial_mtimes_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_primitive_mquotient_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_primitive_mquotient_munsafe_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_primitive_mremainder_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_primitive_mremainder_munsafe_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_primitive_mset_mcarb_mspecial_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_primitive_mset_mcdrb_mspecial_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_primitive_mtwo_mdivided_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_primitive_mtwo_mquotient_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_primitive_mtwo_mremainder_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_primitive_mtwo_mtimes_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_primitive_mzerop_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_push_mglobal_meta_instruction_parameter_types [2] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, jitterlisp_print }, { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_push_mliteral_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, jitterlisp_print } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_push_mregister_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_register, & jitterlispvm_register_class_r, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_register_mto_mregister_meta_instruction_parameter_types [2] =
  { { jitter_meta_instruction_parameter_kind_register, & jitterlispvm_register_class_r, jitter_default_literal_parameter_printer }, { jitter_meta_instruction_parameter_kind_register, & jitterlispvm_register_class_r, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_restore_mregister_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_register, & jitterlispvm_register_class_r, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_save_mregister_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_register, & jitterlispvm_register_class_r, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_tail_mcall_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_tail_mcall_mcompiled_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, jitter_default_literal_parameter_printer } };


const struct jitter_meta_instruction
jitterlispvm_meta_instructions [JITTERLISPVM_META_INSTRUCTION_NO]
  = {
      { 0, "at-depth-to-register", 2, false, false, true /* FIXME: this may be wrong with replacements. */, jitterlispvm_at_mdepth_mto_mregister_meta_instruction_parameter_types },
      { 1, "branch", 1, false, false, true /* FIXME: this may be wrong with replacements. */, jitterlispvm_branch_meta_instruction_parameter_types },
      { 2, "branch-if-false", 1, false, false, true /* FIXME: this may be wrong with replacements. */, jitterlispvm_branch_mif_mfalse_meta_instruction_parameter_types },
      { 3, "branch-if-not-less", 2, false, false, true /* FIXME: this may be wrong with replacements. */, jitterlispvm_branch_mif_mnot_mless_meta_instruction_parameter_types },
      { 4, "branch-if-not-null", 1, false, false, true /* FIXME: this may be wrong with replacements. */, jitterlispvm_branch_mif_mnot_mnull_meta_instruction_parameter_types },
      { 5, "branch-if-null", 1, false, false, true /* FIXME: this may be wrong with replacements. */, jitterlispvm_branch_mif_mnull_meta_instruction_parameter_types },
      { 6, "branch-if-register-non-zero", 3, false, false, true /* FIXME: this may be wrong with replacements. */, jitterlispvm_branch_mif_mregister_mnon_mzero_meta_instruction_parameter_types },
      { 7, "branch-if-true", 1, false, false, true /* FIXME: this may be wrong with replacements. */, jitterlispvm_branch_mif_mtrue_meta_instruction_parameter_types },
      { 8, "call", 1, true, false, true /* FIXME: this may be wrong with replacements. */, jitterlispvm_call_meta_instruction_parameter_types },
      { 9, "call-compiled", 1, true, false, true /* FIXME: this may be wrong with replacements. */, jitterlispvm_call_mcompiled_meta_instruction_parameter_types },
      { 10, "call-from-c", 0, true, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 11, "canonicalize-boolean", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 12, "check-closure", 1, false, false, true /* FIXME: this may be wrong with replacements. */, jitterlispvm_check_mclosure_meta_instruction_parameter_types },
      { 13, "check-global-defined", 2, false, false, true /* FIXME: this may be wrong with replacements. */, jitterlispvm_check_mglobal_mdefined_meta_instruction_parameter_types },
      { 14, "check-in-arity", 2, false, false, true /* FIXME: this may be wrong with replacements. */, jitterlispvm_check_min_marity_meta_instruction_parameter_types },
      { 15, "check-in-arity--alt", 2, false, false, true /* FIXME: this may be wrong with replacements. */, jitterlispvm_check_min_marity_m_malt_meta_instruction_parameter_types },
      { 16, "copy-from-literal", 1, false, false, true /* FIXME: this may be wrong with replacements. */, jitterlispvm_copy_mfrom_mliteral_meta_instruction_parameter_types },
      { 17, "copy-from-register", 1, false, false, true /* FIXME: this may be wrong with replacements. */, jitterlispvm_copy_mfrom_mregister_meta_instruction_parameter_types },
      { 18, "copy-to-register", 1, false, false, true /* FIXME: this may be wrong with replacements. */, jitterlispvm_copy_mto_mregister_meta_instruction_parameter_types },
      { 19, "drop", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 20, "drop-nip", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 21, "dup", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 22, "exitvm", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 23, "fail", 0, false, false, false  /* FIXME: this may be wrong with replacements. */, NULL },
      { 24, "gc-if-needed", 1, false, false, true /* FIXME: this may be wrong with replacements. */, jitterlispvm_gc_mif_mneeded_meta_instruction_parameter_types },
      { 25, "heap-allocate", 1, false, false, true /* FIXME: this may be wrong with replacements. */, jitterlispvm_heap_mallocate_meta_instruction_parameter_types },
      { 26, "literal-to-register", 2, false, false, true /* FIXME: this may be wrong with replacements. */, jitterlispvm_literal_mto_mregister_meta_instruction_parameter_types },
      { 27, "nip", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 28, "nip-drop", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 29, "nip-five", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 30, "nip-five-drop", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 31, "nip-four", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 32, "nip-four-drop", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 33, "nip-push-literal", 1, false, false, true /* FIXME: this may be wrong with replacements. */, jitterlispvm_nip_mpush_mliteral_meta_instruction_parameter_types },
      { 34, "nip-push-register", 1, false, false, true /* FIXME: this may be wrong with replacements. */, jitterlispvm_nip_mpush_mregister_meta_instruction_parameter_types },
      { 35, "nip-six", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 36, "nip-six-drop", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 37, "nip-three", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 38, "nip-three-drop", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 39, "nip-two", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 40, "nip-two-drop", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 41, "nop", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 42, "pop-to-global", 2, false, false, true /* FIXME: this may be wrong with replacements. */, jitterlispvm_pop_mto_mglobal_meta_instruction_parameter_types },
      { 43, "pop-to-global-defined", 2, false, false, true /* FIXME: this may be wrong with replacements. */, jitterlispvm_pop_mto_mglobal_mdefined_meta_instruction_parameter_types },
      { 44, "pop-to-register", 1, false, false, true /* FIXME: this may be wrong with replacements. */, jitterlispvm_pop_mto_mregister_meta_instruction_parameter_types },
      { 45, "primitive", 3, false, false, true /* FIXME: this may be wrong with replacements. */, jitterlispvm_primitive_meta_instruction_parameter_types },
      { 46, "primitive-boolean-canonicalize", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 47, "primitive-box", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 48, "primitive-box-get", 1, false, false, true /* FIXME: this may be wrong with replacements. */, jitterlispvm_primitive_mbox_mget_meta_instruction_parameter_types },
      { 49, "primitive-box-setb-special", 1, false, false, true /* FIXME: this may be wrong with replacements. */, jitterlispvm_primitive_mbox_msetb_mspecial_meta_instruction_parameter_types },
      { 50, "primitive-car", 1, false, false, true /* FIXME: this may be wrong with replacements. */, jitterlispvm_primitive_mcar_meta_instruction_parameter_types },
      { 51, "primitive-cdr", 1, false, false, true /* FIXME: this may be wrong with replacements. */, jitterlispvm_primitive_mcdr_meta_instruction_parameter_types },
      { 52, "primitive-characterp", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 53, "primitive-cons-special", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 54, "primitive-consp", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 55, "primitive-eqp", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 56, "primitive-fixnum-eqp", 1, false, false, true /* FIXME: this may be wrong with replacements. */, jitterlispvm_primitive_mfixnum_meqp_meta_instruction_parameter_types },
      { 57, "primitive-fixnum-not-eqp", 1, false, false, true /* FIXME: this may be wrong with replacements. */, jitterlispvm_primitive_mfixnum_mnot_meqp_meta_instruction_parameter_types },
      { 58, "primitive-fixnump", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 59, "primitive-greaterp", 1, false, false, true /* FIXME: this may be wrong with replacements. */, jitterlispvm_primitive_mgreaterp_meta_instruction_parameter_types },
      { 60, "primitive-lessp", 1, false, false, true /* FIXME: this may be wrong with replacements. */, jitterlispvm_primitive_mlessp_meta_instruction_parameter_types },
      { 61, "primitive-negate", 1, false, false, true /* FIXME: this may be wrong with replacements. */, jitterlispvm_primitive_mnegate_meta_instruction_parameter_types },
      { 62, "primitive-negativep", 1, false, false, true /* FIXME: this may be wrong with replacements. */, jitterlispvm_primitive_mnegativep_meta_instruction_parameter_types },
      { 63, "primitive-non-consp", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 64, "primitive-non-negativep", 1, false, false, true /* FIXME: this may be wrong with replacements. */, jitterlispvm_primitive_mnon_mnegativep_meta_instruction_parameter_types },
      { 65, "primitive-non-nullp", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 66, "primitive-non-positivep", 1, false, false, true /* FIXME: this may be wrong with replacements. */, jitterlispvm_primitive_mnon_mpositivep_meta_instruction_parameter_types },
      { 67, "primitive-non-symbolp", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 68, "primitive-non-zerop", 1, false, false, true /* FIXME: this may be wrong with replacements. */, jitterlispvm_primitive_mnon_mzerop_meta_instruction_parameter_types },
      { 69, "primitive-not", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 70, "primitive-not-eqp", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 71, "primitive-not-greaterp", 1, false, false, true /* FIXME: this may be wrong with replacements. */, jitterlispvm_primitive_mnot_mgreaterp_meta_instruction_parameter_types },
      { 72, "primitive-not-lessp", 1, false, false, true /* FIXME: this may be wrong with replacements. */, jitterlispvm_primitive_mnot_mlessp_meta_instruction_parameter_types },
      { 73, "primitive-nothingp", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 74, "primitive-nullp", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 75, "primitive-one-minus", 1, false, false, true /* FIXME: this may be wrong with replacements. */, jitterlispvm_primitive_mone_mminus_meta_instruction_parameter_types },
      { 76, "primitive-one-plus", 1, false, false, true /* FIXME: this may be wrong with replacements. */, jitterlispvm_primitive_mone_mplus_meta_instruction_parameter_types },
      { 77, "primitive-positivep", 1, false, false, true /* FIXME: this may be wrong with replacements. */, jitterlispvm_primitive_mpositivep_meta_instruction_parameter_types },
      { 78, "primitive-primordial-divided", 1, false, false, true /* FIXME: this may be wrong with replacements. */, jitterlispvm_primitive_mprimordial_mdivided_meta_instruction_parameter_types },
      { 79, "primitive-primordial-divided-unsafe", 1, false, false, true /* FIXME: this may be wrong with replacements. */, jitterlispvm_primitive_mprimordial_mdivided_munsafe_meta_instruction_parameter_types },
      { 80, "primitive-primordial-minus", 1, false, false, true /* FIXME: this may be wrong with replacements. */, jitterlispvm_primitive_mprimordial_mminus_meta_instruction_parameter_types },
      { 81, "primitive-primordial-plus", 1, false, false, true /* FIXME: this may be wrong with replacements. */, jitterlispvm_primitive_mprimordial_mplus_meta_instruction_parameter_types },
      { 82, "primitive-primordial-times", 1, false, false, true /* FIXME: this may be wrong with replacements. */, jitterlispvm_primitive_mprimordial_mtimes_meta_instruction_parameter_types },
      { 83, "primitive-quotient", 1, false, false, true /* FIXME: this may be wrong with replacements. */, jitterlispvm_primitive_mquotient_meta_instruction_parameter_types },
      { 84, "primitive-quotient-unsafe", 1, false, false, true /* FIXME: this may be wrong with replacements. */, jitterlispvm_primitive_mquotient_munsafe_meta_instruction_parameter_types },
      { 85, "primitive-remainder", 1, false, false, true /* FIXME: this may be wrong with replacements. */, jitterlispvm_primitive_mremainder_meta_instruction_parameter_types },
      { 86, "primitive-remainder-unsafe", 1, false, false, true /* FIXME: this may be wrong with replacements. */, jitterlispvm_primitive_mremainder_munsafe_meta_instruction_parameter_types },
      { 87, "primitive-set-carb-special", 1, false, false, true /* FIXME: this may be wrong with replacements. */, jitterlispvm_primitive_mset_mcarb_mspecial_meta_instruction_parameter_types },
      { 88, "primitive-set-cdrb-special", 1, false, false, true /* FIXME: this may be wrong with replacements. */, jitterlispvm_primitive_mset_mcdrb_mspecial_meta_instruction_parameter_types },
      { 89, "primitive-symbolp", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 90, "primitive-two-divided", 1, false, false, true /* FIXME: this may be wrong with replacements. */, jitterlispvm_primitive_mtwo_mdivided_meta_instruction_parameter_types },
      { 91, "primitive-two-quotient", 1, false, false, true /* FIXME: this may be wrong with replacements. */, jitterlispvm_primitive_mtwo_mquotient_meta_instruction_parameter_types },
      { 92, "primitive-two-remainder", 1, false, false, true /* FIXME: this may be wrong with replacements. */, jitterlispvm_primitive_mtwo_mremainder_meta_instruction_parameter_types },
      { 93, "primitive-two-times", 1, false, false, true /* FIXME: this may be wrong with replacements. */, jitterlispvm_primitive_mtwo_mtimes_meta_instruction_parameter_types },
      { 94, "primitive-uniquep", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 95, "primitive-zerop", 1, false, false, true /* FIXME: this may be wrong with replacements. */, jitterlispvm_primitive_mzerop_meta_instruction_parameter_types },
      { 96, "procedure-prolog", 0, false, true, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 97, "push-false", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 98, "push-global", 2, false, false, true /* FIXME: this may be wrong with replacements. */, jitterlispvm_push_mglobal_meta_instruction_parameter_types },
      { 99, "push-literal", 1, false, false, true /* FIXME: this may be wrong with replacements. */, jitterlispvm_push_mliteral_meta_instruction_parameter_types },
      { 100, "push-nil", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 101, "push-nothing", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 102, "push-one", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 103, "push-register", 1, false, false, true /* FIXME: this may be wrong with replacements. */, jitterlispvm_push_mregister_meta_instruction_parameter_types },
      { 104, "push-unspecified", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 105, "push-zero", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 106, "register-to-register", 2, false, false, true /* FIXME: this may be wrong with replacements. */, jitterlispvm_register_mto_mregister_meta_instruction_parameter_types },
      { 107, "restore-register", 1, false, false, true /* FIXME: this may be wrong with replacements. */, jitterlispvm_restore_mregister_meta_instruction_parameter_types },
      { 108, "return", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 109, "save-register", 1, false, false, true /* FIXME: this may be wrong with replacements. */, jitterlispvm_save_mregister_meta_instruction_parameter_types },
      { 110, "tail-call", 1, false, false, true /* FIXME: this may be wrong with replacements. */, jitterlispvm_tail_mcall_meta_instruction_parameter_types },
      { 111, "tail-call-compiled", 1, false, false, true /* FIXME: this may be wrong with replacements. */, jitterlispvm_tail_mcall_mcompiled_meta_instruction_parameter_types },
      { 112, "unreachable", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL }
    };

/* The register class descriptor for r registers. */
const struct jitter_register_class
jitterlispvm_register_class_r
  = {
      jitterlispvm_register_class_id_r,
      'r',
      "register_class_r",
      "REGISTER_CLASS_R",
      JITTERLISPVM_REGISTER_r_FAST_REGISTER_NO,
      1 /* Use slow registers */
    };


/* A pointer to every existing register class descriptor. */
const struct jitter_register_class * const
jitterlispvm_regiter_classes []
  = {
      & jitterlispvm_register_class_r
    };

const struct jitter_register_class *
jitterlispvm_register_class_character_to_register_class (char c)
{
  switch (c)
    {
    case 'r': return & jitterlispvm_register_class_r;
    default:  return NULL;
    }
}

//#include "jitterlispvm-specialized-instructions.h"

const char * const
jitterlispvm_specialized_instruction_names [JITTERLISPVM_SPECIALIZED_INSTRUCTION_NO]
  = {
      "!INVALID",
      "!BEGINBASICBLOCK",
      "!EXITVM",
      "!DATALOCATIONS",
      "!NOP",
      "!UNREACHABLE0",
      "!UNREACHABLE1",
      "!UNREACHABLE2",
      "at-depth-to-register/n1/%rR",
      "at-depth-to-register/n2/%rR",
      "at-depth-to-register/n3/%rR",
      "at-depth-to-register/n4/%rR",
      "at-depth-to-register/n5/%rR",
      "at-depth-to-register/n6/%rR",
      "at-depth-to-register/n7/%rR",
      "at-depth-to-register/n8/%rR",
      "at-depth-to-register/n9/%rR",
      "at-depth-to-register/n10/%rR",
      "at-depth-to-register/nR/%rR",
      "branch/fR",
      "branch-if-false/fR",
      "branch-if-not-less/fR/fR",
      "branch-if-not-null/fR",
      "branch-if-null/fR",
      "branch-if-register-non-zero/%rR/fR/fR",
      "branch-if-true/fR",
      "call/n0/retR",
      "call/n1/retR",
      "call/n2/retR",
      "call/n3/retR",
      "call/n4/retR",
      "call/n5/retR",
      "call/n6/retR",
      "call/n7/retR",
      "call/n8/retR",
      "call/n9/retR",
      "call/n10/retR",
      "call/nR/retR",
      "call-compiled/n0/retR",
      "call-compiled/n1/retR",
      "call-compiled/n2/retR",
      "call-compiled/n3/retR",
      "call-compiled/n4/retR",
      "call-compiled/n5/retR",
      "call-compiled/n6/retR",
      "call-compiled/n7/retR",
      "call-compiled/n8/retR",
      "call-compiled/n9/retR",
      "call-compiled/n10/retR",
      "call-compiled/nR/retR",
      "call-from-c/retR",
      "canonicalize-boolean",
      "check-closure/fR",
      "check-global-defined/nR/fR",
      "check-in-arity/n0/fR",
      "check-in-arity/n1/fR",
      "check-in-arity/n2/fR",
      "check-in-arity/n3/fR",
      "check-in-arity/n4/fR",
      "check-in-arity/n5/fR",
      "check-in-arity/n6/fR",
      "check-in-arity/n7/fR",
      "check-in-arity/n8/fR",
      "check-in-arity/n9/fR",
      "check-in-arity/n10/fR",
      "check-in-arity/nR/fR",
      "check-in-arity--alt/n0/fR",
      "check-in-arity--alt/n1/fR",
      "check-in-arity--alt/n2/fR",
      "check-in-arity--alt/n3/fR",
      "check-in-arity--alt/n4/fR",
      "check-in-arity--alt/n5/fR",
      "check-in-arity--alt/n6/fR",
      "check-in-arity--alt/n7/fR",
      "check-in-arity--alt/n8/fR",
      "check-in-arity--alt/n9/fR",
      "check-in-arity--alt/n10/fR",
      "check-in-arity--alt/nR/fR",
      "copy-from-literal/nR",
      "copy-from-register/%rR",
      "copy-to-register/%rR",
      "drop",
      "drop-nip",
      "dup",
      "exitvm",
      "fail/retR",
      "gc-if-needed/fR",
      "heap-allocate/n4",
      "heap-allocate/n8",
      "heap-allocate/n12",
      "heap-allocate/n16",
      "heap-allocate/n24",
      "heap-allocate/n32",
      "heap-allocate/n36",
      "heap-allocate/n48",
      "heap-allocate/n52",
      "heap-allocate/n64",
      "heap-allocate/nR",
      "literal-to-register/nR/%rR",
      "nip",
      "nip-drop",
      "nip-five",
      "nip-five-drop",
      "nip-four",
      "nip-four-drop",
      "nip-push-literal/nR",
      "nip-push-register/%rR",
      "nip-six",
      "nip-six-drop",
      "nip-three",
      "nip-three-drop",
      "nip-two",
      "nip-two-drop",
      "nop",
      "pop-to-global/nR/fR",
      "pop-to-global-defined/nR/fR",
      "pop-to-register/%rR",
      "primitive/nR/n0/fR",
      "primitive/nR/n1/fR",
      "primitive/nR/n2/fR",
      "primitive/nR/n3/fR",
      "primitive/nR/n4/fR",
      "primitive/nR/nR/fR",
      "primitive-boolean-canonicalize",
      "primitive-box",
      "primitive-box-get/fR",
      "primitive-box-setb-special/fR",
      "primitive-car/fR",
      "primitive-cdr/fR",
      "primitive-characterp",
      "primitive-cons-special",
      "primitive-consp",
      "primitive-eqp",
      "primitive-fixnum-eqp/fR",
      "primitive-fixnum-not-eqp/fR",
      "primitive-fixnump",
      "primitive-greaterp/fR",
      "primitive-lessp/fR",
      "primitive-negate/fR",
      "primitive-negativep/fR",
      "primitive-non-consp",
      "primitive-non-negativep/fR",
      "primitive-non-nullp",
      "primitive-non-positivep/fR",
      "primitive-non-symbolp",
      "primitive-non-zerop/fR",
      "primitive-not",
      "primitive-not-eqp",
      "primitive-not-greaterp/fR",
      "primitive-not-lessp/fR",
      "primitive-nothingp",
      "primitive-nullp",
      "primitive-one-minus/fR",
      "primitive-one-plus/fR",
      "primitive-positivep/fR",
      "primitive-primordial-divided/fR",
      "primitive-primordial-divided-unsafe/fR",
      "primitive-primordial-minus/fR",
      "primitive-primordial-plus/fR",
      "primitive-primordial-times/fR",
      "primitive-quotient/fR",
      "primitive-quotient-unsafe/fR",
      "primitive-remainder/fR",
      "primitive-remainder-unsafe/fR",
      "primitive-set-carb-special/fR",
      "primitive-set-cdrb-special/fR",
      "primitive-symbolp",
      "primitive-two-divided/fR",
      "primitive-two-quotient/fR",
      "primitive-two-remainder/fR",
      "primitive-two-times/fR",
      "primitive-uniquep",
      "primitive-zerop/fR",
      "procedure-prolog",
      "push-false",
      "push-global/nR/fR",
      "push-literal/nR",
      "push-nil",
      "push-nothing",
      "push-one",
      "push-register/%rR",
      "push-unspecified",
      "push-zero",
      "register-to-register/%rR/%rR",
      "restore-register/%rR",
      "return",
      "save-register/%rR",
      "tail-call/n0",
      "tail-call/n1",
      "tail-call/n2",
      "tail-call/n3",
      "tail-call/n4",
      "tail-call/n5",
      "tail-call/n6",
      "tail-call/n7",
      "tail-call/n8",
      "tail-call/n9",
      "tail-call/n10",
      "tail-call/nR",
      "tail-call-compiled/n0",
      "tail-call-compiled/n1",
      "tail-call-compiled/n2",
      "tail-call-compiled/n3",
      "tail-call-compiled/n4",
      "tail-call-compiled/n5",
      "tail-call-compiled/n6",
      "tail-call-compiled/n7",
      "tail-call-compiled/n8",
      "tail-call-compiled/n9",
      "tail-call-compiled/n10",
      "tail-call-compiled/nR",
      "unreachable",
      "*branch/fR*-no-fast-branches",
      "*branch-if-false/fR*-no-fast-branches",
      "*branch-if-not-less/fR/fR*-no-fast-branches",
      "*branch-if-not-null/fR*-no-fast-branches",
      "*branch-if-null/fR*-no-fast-branches",
      "*branch-if-register-non-zero/%rR/fR/fR*-no-fast-branches",
      "*branch-if-true/fR*-no-fast-branches",
      "*check-closure/fR*-no-fast-branches",
      "*check-global-defined/nR/fR*-no-fast-branches",
      "*check-in-arity/n0/fR*-no-fast-branches",
      "*check-in-arity/n1/fR*-no-fast-branches",
      "*check-in-arity/n2/fR*-no-fast-branches",
      "*check-in-arity/n3/fR*-no-fast-branches",
      "*check-in-arity/n4/fR*-no-fast-branches",
      "*check-in-arity/n5/fR*-no-fast-branches",
      "*check-in-arity/n6/fR*-no-fast-branches",
      "*check-in-arity/n7/fR*-no-fast-branches",
      "*check-in-arity/n8/fR*-no-fast-branches",
      "*check-in-arity/n9/fR*-no-fast-branches",
      "*check-in-arity/n10/fR*-no-fast-branches",
      "*check-in-arity/nR/fR*-no-fast-branches",
      "*check-in-arity--alt/n0/fR*-no-fast-branches",
      "*check-in-arity--alt/n1/fR*-no-fast-branches",
      "*check-in-arity--alt/n2/fR*-no-fast-branches",
      "*check-in-arity--alt/n3/fR*-no-fast-branches",
      "*check-in-arity--alt/n4/fR*-no-fast-branches",
      "*check-in-arity--alt/n5/fR*-no-fast-branches",
      "*check-in-arity--alt/n6/fR*-no-fast-branches",
      "*check-in-arity--alt/n7/fR*-no-fast-branches",
      "*check-in-arity--alt/n8/fR*-no-fast-branches",
      "*check-in-arity--alt/n9/fR*-no-fast-branches",
      "*check-in-arity--alt/n10/fR*-no-fast-branches",
      "*check-in-arity--alt/nR/fR*-no-fast-branches",
      "*gc-if-needed/fR*-no-fast-branches",
      "*pop-to-global/nR/fR*-no-fast-branches",
      "*pop-to-global-defined/nR/fR*-no-fast-branches",
      "*primitive/nR/n0/fR*-no-fast-branches",
      "*primitive/nR/n1/fR*-no-fast-branches",
      "*primitive/nR/n2/fR*-no-fast-branches",
      "*primitive/nR/n3/fR*-no-fast-branches",
      "*primitive/nR/n4/fR*-no-fast-branches",
      "*primitive/nR/nR/fR*-no-fast-branches",
      "*primitive-box-get/fR*-no-fast-branches",
      "*primitive-box-setb-special/fR*-no-fast-branches",
      "*primitive-car/fR*-no-fast-branches",
      "*primitive-cdr/fR*-no-fast-branches",
      "*primitive-fixnum-eqp/fR*-no-fast-branches",
      "*primitive-fixnum-not-eqp/fR*-no-fast-branches",
      "*primitive-greaterp/fR*-no-fast-branches",
      "*primitive-lessp/fR*-no-fast-branches",
      "*primitive-negate/fR*-no-fast-branches",
      "*primitive-negativep/fR*-no-fast-branches",
      "*primitive-non-negativep/fR*-no-fast-branches",
      "*primitive-non-positivep/fR*-no-fast-branches",
      "*primitive-non-zerop/fR*-no-fast-branches",
      "*primitive-not-greaterp/fR*-no-fast-branches",
      "*primitive-not-lessp/fR*-no-fast-branches",
      "*primitive-one-minus/fR*-no-fast-branches",
      "*primitive-one-plus/fR*-no-fast-branches",
      "*primitive-positivep/fR*-no-fast-branches",
      "*primitive-primordial-divided/fR*-no-fast-branches",
      "*primitive-primordial-divided-unsafe/fR*-no-fast-branches",
      "*primitive-primordial-minus/fR*-no-fast-branches",
      "*primitive-primordial-plus/fR*-no-fast-branches",
      "*primitive-primordial-times/fR*-no-fast-branches",
      "*primitive-quotient/fR*-no-fast-branches",
      "*primitive-quotient-unsafe/fR*-no-fast-branches",
      "*primitive-remainder/fR*-no-fast-branches",
      "*primitive-remainder-unsafe/fR*-no-fast-branches",
      "*primitive-set-carb-special/fR*-no-fast-branches",
      "*primitive-set-cdrb-special/fR*-no-fast-branches",
      "*primitive-two-divided/fR*-no-fast-branches",
      "*primitive-two-quotient/fR*-no-fast-branches",
      "*primitive-two-remainder/fR*-no-fast-branches",
      "*primitive-two-times/fR*-no-fast-branches",
      "*primitive-zerop/fR*-no-fast-branches",
      "*push-global/nR/fR*-no-fast-branches"
    };
// #include <stdlib.h>

// #include "jitterlispvm-specialized-instructions.h"
const size_t
jitterlispvm_specialized_instruction_residual_arities [JITTERLISPVM_SPECIALIZED_INSTRUCTION_NO]
  = {
      0, /* !INVALID */
      1, /* !BEGINBASICBLOCK */
      0, /* !EXITVM */
      0, /* !DATALOCATIONS */
      0, /* !NOP */
      0, /* !UNREACHABLE0 */
      0, /* !UNREACHABLE1 */
      0, /* !UNREACHABLE2 */
      1, /* at-depth-to-register/n1/%rR */
      1, /* at-depth-to-register/n2/%rR */
      1, /* at-depth-to-register/n3/%rR */
      1, /* at-depth-to-register/n4/%rR */
      1, /* at-depth-to-register/n5/%rR */
      1, /* at-depth-to-register/n6/%rR */
      1, /* at-depth-to-register/n7/%rR */
      1, /* at-depth-to-register/n8/%rR */
      1, /* at-depth-to-register/n9/%rR */
      1, /* at-depth-to-register/n10/%rR */
      2, /* at-depth-to-register/nR/%rR */
      1, /* branch/fR */
      1, /* branch-if-false/fR */
      2, /* branch-if-not-less/fR/fR */
      1, /* branch-if-not-null/fR */
      1, /* branch-if-null/fR */
      3, /* branch-if-register-non-zero/%rR/fR/fR */
      1, /* branch-if-true/fR */
      1, /* call/n0/retR */
      1, /* call/n1/retR */
      1, /* call/n2/retR */
      1, /* call/n3/retR */
      1, /* call/n4/retR */
      1, /* call/n5/retR */
      1, /* call/n6/retR */
      1, /* call/n7/retR */
      1, /* call/n8/retR */
      1, /* call/n9/retR */
      1, /* call/n10/retR */
      2, /* call/nR/retR */
      1, /* call-compiled/n0/retR */
      1, /* call-compiled/n1/retR */
      1, /* call-compiled/n2/retR */
      1, /* call-compiled/n3/retR */
      1, /* call-compiled/n4/retR */
      1, /* call-compiled/n5/retR */
      1, /* call-compiled/n6/retR */
      1, /* call-compiled/n7/retR */
      1, /* call-compiled/n8/retR */
      1, /* call-compiled/n9/retR */
      1, /* call-compiled/n10/retR */
      2, /* call-compiled/nR/retR */
      1, /* call-from-c/retR */
      0, /* canonicalize-boolean */
      1, /* check-closure/fR */
      2, /* check-global-defined/nR/fR */
      1, /* check-in-arity/n0/fR */
      1, /* check-in-arity/n1/fR */
      1, /* check-in-arity/n2/fR */
      1, /* check-in-arity/n3/fR */
      1, /* check-in-arity/n4/fR */
      1, /* check-in-arity/n5/fR */
      1, /* check-in-arity/n6/fR */
      1, /* check-in-arity/n7/fR */
      1, /* check-in-arity/n8/fR */
      1, /* check-in-arity/n9/fR */
      1, /* check-in-arity/n10/fR */
      2, /* check-in-arity/nR/fR */
      1, /* check-in-arity--alt/n0/fR */
      1, /* check-in-arity--alt/n1/fR */
      1, /* check-in-arity--alt/n2/fR */
      1, /* check-in-arity--alt/n3/fR */
      1, /* check-in-arity--alt/n4/fR */
      1, /* check-in-arity--alt/n5/fR */
      1, /* check-in-arity--alt/n6/fR */
      1, /* check-in-arity--alt/n7/fR */
      1, /* check-in-arity--alt/n8/fR */
      1, /* check-in-arity--alt/n9/fR */
      1, /* check-in-arity--alt/n10/fR */
      2, /* check-in-arity--alt/nR/fR */
      1, /* copy-from-literal/nR */
      1, /* copy-from-register/%rR */
      1, /* copy-to-register/%rR */
      0, /* drop */
      0, /* drop-nip */
      0, /* dup */
      0, /* exitvm */
      1, /* fail/retR */
      1, /* gc-if-needed/fR */
      0, /* heap-allocate/n4 */
      0, /* heap-allocate/n8 */
      0, /* heap-allocate/n12 */
      0, /* heap-allocate/n16 */
      0, /* heap-allocate/n24 */
      0, /* heap-allocate/n32 */
      0, /* heap-allocate/n36 */
      0, /* heap-allocate/n48 */
      0, /* heap-allocate/n52 */
      0, /* heap-allocate/n64 */
      1, /* heap-allocate/nR */
      2, /* literal-to-register/nR/%rR */
      0, /* nip */
      0, /* nip-drop */
      0, /* nip-five */
      0, /* nip-five-drop */
      0, /* nip-four */
      0, /* nip-four-drop */
      1, /* nip-push-literal/nR */
      1, /* nip-push-register/%rR */
      0, /* nip-six */
      0, /* nip-six-drop */
      0, /* nip-three */
      0, /* nip-three-drop */
      0, /* nip-two */
      0, /* nip-two-drop */
      0, /* nop */
      2, /* pop-to-global/nR/fR */
      2, /* pop-to-global-defined/nR/fR */
      1, /* pop-to-register/%rR */
      2, /* primitive/nR/n0/fR */
      2, /* primitive/nR/n1/fR */
      2, /* primitive/nR/n2/fR */
      2, /* primitive/nR/n3/fR */
      2, /* primitive/nR/n4/fR */
      3, /* primitive/nR/nR/fR */
      0, /* primitive-boolean-canonicalize */
      0, /* primitive-box */
      1, /* primitive-box-get/fR */
      1, /* primitive-box-setb-special/fR */
      1, /* primitive-car/fR */
      1, /* primitive-cdr/fR */
      0, /* primitive-characterp */
      0, /* primitive-cons-special */
      0, /* primitive-consp */
      0, /* primitive-eqp */
      1, /* primitive-fixnum-eqp/fR */
      1, /* primitive-fixnum-not-eqp/fR */
      0, /* primitive-fixnump */
      1, /* primitive-greaterp/fR */
      1, /* primitive-lessp/fR */
      1, /* primitive-negate/fR */
      1, /* primitive-negativep/fR */
      0, /* primitive-non-consp */
      1, /* primitive-non-negativep/fR */
      0, /* primitive-non-nullp */
      1, /* primitive-non-positivep/fR */
      0, /* primitive-non-symbolp */
      1, /* primitive-non-zerop/fR */
      0, /* primitive-not */
      0, /* primitive-not-eqp */
      1, /* primitive-not-greaterp/fR */
      1, /* primitive-not-lessp/fR */
      0, /* primitive-nothingp */
      0, /* primitive-nullp */
      1, /* primitive-one-minus/fR */
      1, /* primitive-one-plus/fR */
      1, /* primitive-positivep/fR */
      1, /* primitive-primordial-divided/fR */
      1, /* primitive-primordial-divided-unsafe/fR */
      1, /* primitive-primordial-minus/fR */
      1, /* primitive-primordial-plus/fR */
      1, /* primitive-primordial-times/fR */
      1, /* primitive-quotient/fR */
      1, /* primitive-quotient-unsafe/fR */
      1, /* primitive-remainder/fR */
      1, /* primitive-remainder-unsafe/fR */
      1, /* primitive-set-carb-special/fR */
      1, /* primitive-set-cdrb-special/fR */
      0, /* primitive-symbolp */
      1, /* primitive-two-divided/fR */
      1, /* primitive-two-quotient/fR */
      1, /* primitive-two-remainder/fR */
      1, /* primitive-two-times/fR */
      0, /* primitive-uniquep */
      1, /* primitive-zerop/fR */
      0, /* procedure-prolog */
      0, /* push-false */
      2, /* push-global/nR/fR */
      1, /* push-literal/nR */
      0, /* push-nil */
      0, /* push-nothing */
      0, /* push-one */
      1, /* push-register/%rR */
      0, /* push-unspecified */
      0, /* push-zero */
      2, /* register-to-register/%rR/%rR */
      1, /* restore-register/%rR */
      0, /* return */
      1, /* save-register/%rR */
      0, /* tail-call/n0 */
      0, /* tail-call/n1 */
      0, /* tail-call/n2 */
      0, /* tail-call/n3 */
      0, /* tail-call/n4 */
      0, /* tail-call/n5 */
      0, /* tail-call/n6 */
      0, /* tail-call/n7 */
      0, /* tail-call/n8 */
      0, /* tail-call/n9 */
      0, /* tail-call/n10 */
      1, /* tail-call/nR */
      0, /* tail-call-compiled/n0 */
      0, /* tail-call-compiled/n1 */
      0, /* tail-call-compiled/n2 */
      0, /* tail-call-compiled/n3 */
      0, /* tail-call-compiled/n4 */
      0, /* tail-call-compiled/n5 */
      0, /* tail-call-compiled/n6 */
      0, /* tail-call-compiled/n7 */
      0, /* tail-call-compiled/n8 */
      0, /* tail-call-compiled/n9 */
      0, /* tail-call-compiled/n10 */
      1, /* tail-call-compiled/nR */
      0, /* unreachable */
      1, /* *branch/fR*-no-fast-branches */
      1, /* *branch-if-false/fR*-no-fast-branches */
      2, /* *branch-if-not-less/fR/fR*-no-fast-branches */
      1, /* *branch-if-not-null/fR*-no-fast-branches */
      1, /* *branch-if-null/fR*-no-fast-branches */
      3, /* *branch-if-register-non-zero/%rR/fR/fR*-no-fast-branches */
      1, /* *branch-if-true/fR*-no-fast-branches */
      1, /* *check-closure/fR*-no-fast-branches */
      2, /* *check-global-defined/nR/fR*-no-fast-branches */
      1, /* *check-in-arity/n0/fR*-no-fast-branches */
      1, /* *check-in-arity/n1/fR*-no-fast-branches */
      1, /* *check-in-arity/n2/fR*-no-fast-branches */
      1, /* *check-in-arity/n3/fR*-no-fast-branches */
      1, /* *check-in-arity/n4/fR*-no-fast-branches */
      1, /* *check-in-arity/n5/fR*-no-fast-branches */
      1, /* *check-in-arity/n6/fR*-no-fast-branches */
      1, /* *check-in-arity/n7/fR*-no-fast-branches */
      1, /* *check-in-arity/n8/fR*-no-fast-branches */
      1, /* *check-in-arity/n9/fR*-no-fast-branches */
      1, /* *check-in-arity/n10/fR*-no-fast-branches */
      2, /* *check-in-arity/nR/fR*-no-fast-branches */
      1, /* *check-in-arity--alt/n0/fR*-no-fast-branches */
      1, /* *check-in-arity--alt/n1/fR*-no-fast-branches */
      1, /* *check-in-arity--alt/n2/fR*-no-fast-branches */
      1, /* *check-in-arity--alt/n3/fR*-no-fast-branches */
      1, /* *check-in-arity--alt/n4/fR*-no-fast-branches */
      1, /* *check-in-arity--alt/n5/fR*-no-fast-branches */
      1, /* *check-in-arity--alt/n6/fR*-no-fast-branches */
      1, /* *check-in-arity--alt/n7/fR*-no-fast-branches */
      1, /* *check-in-arity--alt/n8/fR*-no-fast-branches */
      1, /* *check-in-arity--alt/n9/fR*-no-fast-branches */
      1, /* *check-in-arity--alt/n10/fR*-no-fast-branches */
      2, /* *check-in-arity--alt/nR/fR*-no-fast-branches */
      1, /* *gc-if-needed/fR*-no-fast-branches */
      2, /* *pop-to-global/nR/fR*-no-fast-branches */
      2, /* *pop-to-global-defined/nR/fR*-no-fast-branches */
      2, /* *primitive/nR/n0/fR*-no-fast-branches */
      2, /* *primitive/nR/n1/fR*-no-fast-branches */
      2, /* *primitive/nR/n2/fR*-no-fast-branches */
      2, /* *primitive/nR/n3/fR*-no-fast-branches */
      2, /* *primitive/nR/n4/fR*-no-fast-branches */
      3, /* *primitive/nR/nR/fR*-no-fast-branches */
      1, /* *primitive-box-get/fR*-no-fast-branches */
      1, /* *primitive-box-setb-special/fR*-no-fast-branches */
      1, /* *primitive-car/fR*-no-fast-branches */
      1, /* *primitive-cdr/fR*-no-fast-branches */
      1, /* *primitive-fixnum-eqp/fR*-no-fast-branches */
      1, /* *primitive-fixnum-not-eqp/fR*-no-fast-branches */
      1, /* *primitive-greaterp/fR*-no-fast-branches */
      1, /* *primitive-lessp/fR*-no-fast-branches */
      1, /* *primitive-negate/fR*-no-fast-branches */
      1, /* *primitive-negativep/fR*-no-fast-branches */
      1, /* *primitive-non-negativep/fR*-no-fast-branches */
      1, /* *primitive-non-positivep/fR*-no-fast-branches */
      1, /* *primitive-non-zerop/fR*-no-fast-branches */
      1, /* *primitive-not-greaterp/fR*-no-fast-branches */
      1, /* *primitive-not-lessp/fR*-no-fast-branches */
      1, /* *primitive-one-minus/fR*-no-fast-branches */
      1, /* *primitive-one-plus/fR*-no-fast-branches */
      1, /* *primitive-positivep/fR*-no-fast-branches */
      1, /* *primitive-primordial-divided/fR*-no-fast-branches */
      1, /* *primitive-primordial-divided-unsafe/fR*-no-fast-branches */
      1, /* *primitive-primordial-minus/fR*-no-fast-branches */
      1, /* *primitive-primordial-plus/fR*-no-fast-branches */
      1, /* *primitive-primordial-times/fR*-no-fast-branches */
      1, /* *primitive-quotient/fR*-no-fast-branches */
      1, /* *primitive-quotient-unsafe/fR*-no-fast-branches */
      1, /* *primitive-remainder/fR*-no-fast-branches */
      1, /* *primitive-remainder-unsafe/fR*-no-fast-branches */
      1, /* *primitive-set-carb-special/fR*-no-fast-branches */
      1, /* *primitive-set-cdrb-special/fR*-no-fast-branches */
      1, /* *primitive-two-divided/fR*-no-fast-branches */
      1, /* *primitive-two-quotient/fR*-no-fast-branches */
      1, /* *primitive-two-remainder/fR*-no-fast-branches */
      1, /* *primitive-two-times/fR*-no-fast-branches */
      1, /* *primitive-zerop/fR*-no-fast-branches */
      2 /* *push-global/nR/fR*-no-fast-branches */
    };
const unsigned long // FIXME: shall I use a shorter type when possible?
jitterlispvm_specialized_instruction_label_bitmasks [JITTERLISPVM_SPECIALIZED_INSTRUCTION_NO]
  = {
      /* It's important that !BEGINBASICBLOCK has a zero here: it does not need residual patching. */
      0, /* !INVALID */
      0, /* !BEGINBASICBLOCK */
      0, /* !EXITVM */
      0, /* !DATALOCATIONS */
      0, /* !NOP */
      0, /* !UNREACHABLE0 */
      0, /* !UNREACHABLE1 */
      0, /* !UNREACHABLE2 */
      0, /* at-depth-to-register/n1/%rR */
      0, /* at-depth-to-register/n2/%rR */
      0, /* at-depth-to-register/n3/%rR */
      0, /* at-depth-to-register/n4/%rR */
      0, /* at-depth-to-register/n5/%rR */
      0, /* at-depth-to-register/n6/%rR */
      0, /* at-depth-to-register/n7/%rR */
      0, /* at-depth-to-register/n8/%rR */
      0, /* at-depth-to-register/n9/%rR */
      0, /* at-depth-to-register/n10/%rR */
      0, /* at-depth-to-register/nR/%rR */
      0 | (1UL << 0), /* branch/fR */
      0 | (1UL << 0), /* branch-if-false/fR */
      0 | (1UL << 0) | (1UL << 1), /* branch-if-not-less/fR/fR */
      0 | (1UL << 0), /* branch-if-not-null/fR */
      0 | (1UL << 0), /* branch-if-null/fR */
      0 | (1UL << 1) | (1UL << 2), /* branch-if-register-non-zero/%rR/fR/fR */
      0 | (1UL << 0), /* branch-if-true/fR */
      0, /* call/n0/retR */
      0, /* call/n1/retR */
      0, /* call/n2/retR */
      0, /* call/n3/retR */
      0, /* call/n4/retR */
      0, /* call/n5/retR */
      0, /* call/n6/retR */
      0, /* call/n7/retR */
      0, /* call/n8/retR */
      0, /* call/n9/retR */
      0, /* call/n10/retR */
      0, /* call/nR/retR */
      0, /* call-compiled/n0/retR */
      0, /* call-compiled/n1/retR */
      0, /* call-compiled/n2/retR */
      0, /* call-compiled/n3/retR */
      0, /* call-compiled/n4/retR */
      0, /* call-compiled/n5/retR */
      0, /* call-compiled/n6/retR */
      0, /* call-compiled/n7/retR */
      0, /* call-compiled/n8/retR */
      0, /* call-compiled/n9/retR */
      0, /* call-compiled/n10/retR */
      0, /* call-compiled/nR/retR */
      0, /* call-from-c/retR */
      0, /* canonicalize-boolean */
      0 | (1UL << 0), /* check-closure/fR */
      0 | (1UL << 1), /* check-global-defined/nR/fR */
      0 | (1UL << 0), /* check-in-arity/n0/fR */
      0 | (1UL << 0), /* check-in-arity/n1/fR */
      0 | (1UL << 0), /* check-in-arity/n2/fR */
      0 | (1UL << 0), /* check-in-arity/n3/fR */
      0 | (1UL << 0), /* check-in-arity/n4/fR */
      0 | (1UL << 0), /* check-in-arity/n5/fR */
      0 | (1UL << 0), /* check-in-arity/n6/fR */
      0 | (1UL << 0), /* check-in-arity/n7/fR */
      0 | (1UL << 0), /* check-in-arity/n8/fR */
      0 | (1UL << 0), /* check-in-arity/n9/fR */
      0 | (1UL << 0), /* check-in-arity/n10/fR */
      0 | (1UL << 1), /* check-in-arity/nR/fR */
      0 | (1UL << 0), /* check-in-arity--alt/n0/fR */
      0 | (1UL << 0), /* check-in-arity--alt/n1/fR */
      0 | (1UL << 0), /* check-in-arity--alt/n2/fR */
      0 | (1UL << 0), /* check-in-arity--alt/n3/fR */
      0 | (1UL << 0), /* check-in-arity--alt/n4/fR */
      0 | (1UL << 0), /* check-in-arity--alt/n5/fR */
      0 | (1UL << 0), /* check-in-arity--alt/n6/fR */
      0 | (1UL << 0), /* check-in-arity--alt/n7/fR */
      0 | (1UL << 0), /* check-in-arity--alt/n8/fR */
      0 | (1UL << 0), /* check-in-arity--alt/n9/fR */
      0 | (1UL << 0), /* check-in-arity--alt/n10/fR */
      0 | (1UL << 1), /* check-in-arity--alt/nR/fR */
      0, /* copy-from-literal/nR */
      0, /* copy-from-register/%rR */
      0, /* copy-to-register/%rR */
      0, /* drop */
      0, /* drop-nip */
      0, /* dup */
      0, /* exitvm */
      0, /* fail/retR */
      0 | (1UL << 0), /* gc-if-needed/fR */
      0, /* heap-allocate/n4 */
      0, /* heap-allocate/n8 */
      0, /* heap-allocate/n12 */
      0, /* heap-allocate/n16 */
      0, /* heap-allocate/n24 */
      0, /* heap-allocate/n32 */
      0, /* heap-allocate/n36 */
      0, /* heap-allocate/n48 */
      0, /* heap-allocate/n52 */
      0, /* heap-allocate/n64 */
      0, /* heap-allocate/nR */
      0, /* literal-to-register/nR/%rR */
      0, /* nip */
      0, /* nip-drop */
      0, /* nip-five */
      0, /* nip-five-drop */
      0, /* nip-four */
      0, /* nip-four-drop */
      0, /* nip-push-literal/nR */
      0, /* nip-push-register/%rR */
      0, /* nip-six */
      0, /* nip-six-drop */
      0, /* nip-three */
      0, /* nip-three-drop */
      0, /* nip-two */
      0, /* nip-two-drop */
      0, /* nop */
      0 | (1UL << 1), /* pop-to-global/nR/fR */
      0 | (1UL << 1), /* pop-to-global-defined/nR/fR */
      0, /* pop-to-register/%rR */
      0 | (1UL << 1), /* primitive/nR/n0/fR */
      0 | (1UL << 1), /* primitive/nR/n1/fR */
      0 | (1UL << 1), /* primitive/nR/n2/fR */
      0 | (1UL << 1), /* primitive/nR/n3/fR */
      0 | (1UL << 1), /* primitive/nR/n4/fR */
      0 | (1UL << 2), /* primitive/nR/nR/fR */
      0, /* primitive-boolean-canonicalize */
      0, /* primitive-box */
      0 | (1UL << 0), /* primitive-box-get/fR */
      0 | (1UL << 0), /* primitive-box-setb-special/fR */
      0 | (1UL << 0), /* primitive-car/fR */
      0 | (1UL << 0), /* primitive-cdr/fR */
      0, /* primitive-characterp */
      0, /* primitive-cons-special */
      0, /* primitive-consp */
      0, /* primitive-eqp */
      0 | (1UL << 0), /* primitive-fixnum-eqp/fR */
      0 | (1UL << 0), /* primitive-fixnum-not-eqp/fR */
      0, /* primitive-fixnump */
      0 | (1UL << 0), /* primitive-greaterp/fR */
      0 | (1UL << 0), /* primitive-lessp/fR */
      0 | (1UL << 0), /* primitive-negate/fR */
      0 | (1UL << 0), /* primitive-negativep/fR */
      0, /* primitive-non-consp */
      0 | (1UL << 0), /* primitive-non-negativep/fR */
      0, /* primitive-non-nullp */
      0 | (1UL << 0), /* primitive-non-positivep/fR */
      0, /* primitive-non-symbolp */
      0 | (1UL << 0), /* primitive-non-zerop/fR */
      0, /* primitive-not */
      0, /* primitive-not-eqp */
      0 | (1UL << 0), /* primitive-not-greaterp/fR */
      0 | (1UL << 0), /* primitive-not-lessp/fR */
      0, /* primitive-nothingp */
      0, /* primitive-nullp */
      0 | (1UL << 0), /* primitive-one-minus/fR */
      0 | (1UL << 0), /* primitive-one-plus/fR */
      0 | (1UL << 0), /* primitive-positivep/fR */
      0 | (1UL << 0), /* primitive-primordial-divided/fR */
      0 | (1UL << 0), /* primitive-primordial-divided-unsafe/fR */
      0 | (1UL << 0), /* primitive-primordial-minus/fR */
      0 | (1UL << 0), /* primitive-primordial-plus/fR */
      0 | (1UL << 0), /* primitive-primordial-times/fR */
      0 | (1UL << 0), /* primitive-quotient/fR */
      0 | (1UL << 0), /* primitive-quotient-unsafe/fR */
      0 | (1UL << 0), /* primitive-remainder/fR */
      0 | (1UL << 0), /* primitive-remainder-unsafe/fR */
      0 | (1UL << 0), /* primitive-set-carb-special/fR */
      0 | (1UL << 0), /* primitive-set-cdrb-special/fR */
      0, /* primitive-symbolp */
      0 | (1UL << 0), /* primitive-two-divided/fR */
      0 | (1UL << 0), /* primitive-two-quotient/fR */
      0 | (1UL << 0), /* primitive-two-remainder/fR */
      0 | (1UL << 0), /* primitive-two-times/fR */
      0, /* primitive-uniquep */
      0 | (1UL << 0), /* primitive-zerop/fR */
      0, /* procedure-prolog */
      0, /* push-false */
      0 | (1UL << 1), /* push-global/nR/fR */
      0, /* push-literal/nR */
      0, /* push-nil */
      0, /* push-nothing */
      0, /* push-one */
      0, /* push-register/%rR */
      0, /* push-unspecified */
      0, /* push-zero */
      0, /* register-to-register/%rR/%rR */
      0, /* restore-register/%rR */
      0, /* return */
      0, /* save-register/%rR */
      0, /* tail-call/n0 */
      0, /* tail-call/n1 */
      0, /* tail-call/n2 */
      0, /* tail-call/n3 */
      0, /* tail-call/n4 */
      0, /* tail-call/n5 */
      0, /* tail-call/n6 */
      0, /* tail-call/n7 */
      0, /* tail-call/n8 */
      0, /* tail-call/n9 */
      0, /* tail-call/n10 */
      0, /* tail-call/nR */
      0, /* tail-call-compiled/n0 */
      0, /* tail-call-compiled/n1 */
      0, /* tail-call-compiled/n2 */
      0, /* tail-call-compiled/n3 */
      0, /* tail-call-compiled/n4 */
      0, /* tail-call-compiled/n5 */
      0, /* tail-call-compiled/n6 */
      0, /* tail-call-compiled/n7 */
      0, /* tail-call-compiled/n8 */
      0, /* tail-call-compiled/n9 */
      0, /* tail-call-compiled/n10 */
      0, /* tail-call-compiled/nR */
      0, /* unreachable */
      0 | (1UL << 0), /* *branch/fR*-no-fast-branches */
      0 | (1UL << 0), /* *branch-if-false/fR*-no-fast-branches */
      0 | (1UL << 0) | (1UL << 1), /* *branch-if-not-less/fR/fR*-no-fast-branches */
      0 | (1UL << 0), /* *branch-if-not-null/fR*-no-fast-branches */
      0 | (1UL << 0), /* *branch-if-null/fR*-no-fast-branches */
      0 | (1UL << 1) | (1UL << 2), /* *branch-if-register-non-zero/%rR/fR/fR*-no-fast-branches */
      0 | (1UL << 0), /* *branch-if-true/fR*-no-fast-branches */
      0 | (1UL << 0), /* *check-closure/fR*-no-fast-branches */
      0 | (1UL << 1), /* *check-global-defined/nR/fR*-no-fast-branches */
      0 | (1UL << 0), /* *check-in-arity/n0/fR*-no-fast-branches */
      0 | (1UL << 0), /* *check-in-arity/n1/fR*-no-fast-branches */
      0 | (1UL << 0), /* *check-in-arity/n2/fR*-no-fast-branches */
      0 | (1UL << 0), /* *check-in-arity/n3/fR*-no-fast-branches */
      0 | (1UL << 0), /* *check-in-arity/n4/fR*-no-fast-branches */
      0 | (1UL << 0), /* *check-in-arity/n5/fR*-no-fast-branches */
      0 | (1UL << 0), /* *check-in-arity/n6/fR*-no-fast-branches */
      0 | (1UL << 0), /* *check-in-arity/n7/fR*-no-fast-branches */
      0 | (1UL << 0), /* *check-in-arity/n8/fR*-no-fast-branches */
      0 | (1UL << 0), /* *check-in-arity/n9/fR*-no-fast-branches */
      0 | (1UL << 0), /* *check-in-arity/n10/fR*-no-fast-branches */
      0 | (1UL << 1), /* *check-in-arity/nR/fR*-no-fast-branches */
      0 | (1UL << 0), /* *check-in-arity--alt/n0/fR*-no-fast-branches */
      0 | (1UL << 0), /* *check-in-arity--alt/n1/fR*-no-fast-branches */
      0 | (1UL << 0), /* *check-in-arity--alt/n2/fR*-no-fast-branches */
      0 | (1UL << 0), /* *check-in-arity--alt/n3/fR*-no-fast-branches */
      0 | (1UL << 0), /* *check-in-arity--alt/n4/fR*-no-fast-branches */
      0 | (1UL << 0), /* *check-in-arity--alt/n5/fR*-no-fast-branches */
      0 | (1UL << 0), /* *check-in-arity--alt/n6/fR*-no-fast-branches */
      0 | (1UL << 0), /* *check-in-arity--alt/n7/fR*-no-fast-branches */
      0 | (1UL << 0), /* *check-in-arity--alt/n8/fR*-no-fast-branches */
      0 | (1UL << 0), /* *check-in-arity--alt/n9/fR*-no-fast-branches */
      0 | (1UL << 0), /* *check-in-arity--alt/n10/fR*-no-fast-branches */
      0 | (1UL << 1), /* *check-in-arity--alt/nR/fR*-no-fast-branches */
      0 | (1UL << 0), /* *gc-if-needed/fR*-no-fast-branches */
      0 | (1UL << 1), /* *pop-to-global/nR/fR*-no-fast-branches */
      0 | (1UL << 1), /* *pop-to-global-defined/nR/fR*-no-fast-branches */
      0 | (1UL << 1), /* *primitive/nR/n0/fR*-no-fast-branches */
      0 | (1UL << 1), /* *primitive/nR/n1/fR*-no-fast-branches */
      0 | (1UL << 1), /* *primitive/nR/n2/fR*-no-fast-branches */
      0 | (1UL << 1), /* *primitive/nR/n3/fR*-no-fast-branches */
      0 | (1UL << 1), /* *primitive/nR/n4/fR*-no-fast-branches */
      0 | (1UL << 2), /* *primitive/nR/nR/fR*-no-fast-branches */
      0 | (1UL << 0), /* *primitive-box-get/fR*-no-fast-branches */
      0 | (1UL << 0), /* *primitive-box-setb-special/fR*-no-fast-branches */
      0 | (1UL << 0), /* *primitive-car/fR*-no-fast-branches */
      0 | (1UL << 0), /* *primitive-cdr/fR*-no-fast-branches */
      0 | (1UL << 0), /* *primitive-fixnum-eqp/fR*-no-fast-branches */
      0 | (1UL << 0), /* *primitive-fixnum-not-eqp/fR*-no-fast-branches */
      0 | (1UL << 0), /* *primitive-greaterp/fR*-no-fast-branches */
      0 | (1UL << 0), /* *primitive-lessp/fR*-no-fast-branches */
      0 | (1UL << 0), /* *primitive-negate/fR*-no-fast-branches */
      0 | (1UL << 0), /* *primitive-negativep/fR*-no-fast-branches */
      0 | (1UL << 0), /* *primitive-non-negativep/fR*-no-fast-branches */
      0 | (1UL << 0), /* *primitive-non-positivep/fR*-no-fast-branches */
      0 | (1UL << 0), /* *primitive-non-zerop/fR*-no-fast-branches */
      0 | (1UL << 0), /* *primitive-not-greaterp/fR*-no-fast-branches */
      0 | (1UL << 0), /* *primitive-not-lessp/fR*-no-fast-branches */
      0 | (1UL << 0), /* *primitive-one-minus/fR*-no-fast-branches */
      0 | (1UL << 0), /* *primitive-one-plus/fR*-no-fast-branches */
      0 | (1UL << 0), /* *primitive-positivep/fR*-no-fast-branches */
      0 | (1UL << 0), /* *primitive-primordial-divided/fR*-no-fast-branches */
      0 | (1UL << 0), /* *primitive-primordial-divided-unsafe/fR*-no-fast-branches */
      0 | (1UL << 0), /* *primitive-primordial-minus/fR*-no-fast-branches */
      0 | (1UL << 0), /* *primitive-primordial-plus/fR*-no-fast-branches */
      0 | (1UL << 0), /* *primitive-primordial-times/fR*-no-fast-branches */
      0 | (1UL << 0), /* *primitive-quotient/fR*-no-fast-branches */
      0 | (1UL << 0), /* *primitive-quotient-unsafe/fR*-no-fast-branches */
      0 | (1UL << 0), /* *primitive-remainder/fR*-no-fast-branches */
      0 | (1UL << 0), /* *primitive-remainder-unsafe/fR*-no-fast-branches */
      0 | (1UL << 0), /* *primitive-set-carb-special/fR*-no-fast-branches */
      0 | (1UL << 0), /* *primitive-set-cdrb-special/fR*-no-fast-branches */
      0 | (1UL << 0), /* *primitive-two-divided/fR*-no-fast-branches */
      0 | (1UL << 0), /* *primitive-two-quotient/fR*-no-fast-branches */
      0 | (1UL << 0), /* *primitive-two-remainder/fR*-no-fast-branches */
      0 | (1UL << 0), /* *primitive-two-times/fR*-no-fast-branches */
      0 | (1UL << 0), /* *primitive-zerop/fR*-no-fast-branches */
      0 | (1UL << 1) /* *push-global/nR/fR*-no-fast-branches */
    };
#ifdef JITTER_HAVE_PATCH_IN
const unsigned long // FIXME: shall I use a shorter type when possible?
jitterlispvm_specialized_instruction_fast_label_bitmasks [JITTERLISPVM_SPECIALIZED_INSTRUCTION_NO]
  = {
      /* It's important that !BEGINBASICBLOCK has a zero here: it does not need residual patching. */
      0, /* !INVALID */
      0, /* !BEGINBASICBLOCK */
      0, /* !EXITVM */
      0, /* !DATALOCATIONS */
      0, /* !NOP */
      0, /* !UNREACHABLE0 */
      0, /* !UNREACHABLE1 */
      0, /* !UNREACHABLE2 */
      0, /* at-depth-to-register/n1/%rR */
      0, /* at-depth-to-register/n2/%rR */
      0, /* at-depth-to-register/n3/%rR */
      0, /* at-depth-to-register/n4/%rR */
      0, /* at-depth-to-register/n5/%rR */
      0, /* at-depth-to-register/n6/%rR */
      0, /* at-depth-to-register/n7/%rR */
      0, /* at-depth-to-register/n8/%rR */
      0, /* at-depth-to-register/n9/%rR */
      0, /* at-depth-to-register/n10/%rR */
      0, /* at-depth-to-register/nR/%rR */
      0 | (1UL << 0), /* branch/fR */
      0 | (1UL << 0), /* branch-if-false/fR */
      0 | (1UL << 0) | (1UL << 1), /* branch-if-not-less/fR/fR */
      0 | (1UL << 0), /* branch-if-not-null/fR */
      0 | (1UL << 0), /* branch-if-null/fR */
      0 | (1UL << 1) | (1UL << 2), /* branch-if-register-non-zero/%rR/fR/fR */
      0 | (1UL << 0), /* branch-if-true/fR */
      0, /* call/n0/retR */
      0, /* call/n1/retR */
      0, /* call/n2/retR */
      0, /* call/n3/retR */
      0, /* call/n4/retR */
      0, /* call/n5/retR */
      0, /* call/n6/retR */
      0, /* call/n7/retR */
      0, /* call/n8/retR */
      0, /* call/n9/retR */
      0, /* call/n10/retR */
      0, /* call/nR/retR */
      0, /* call-compiled/n0/retR */
      0, /* call-compiled/n1/retR */
      0, /* call-compiled/n2/retR */
      0, /* call-compiled/n3/retR */
      0, /* call-compiled/n4/retR */
      0, /* call-compiled/n5/retR */
      0, /* call-compiled/n6/retR */
      0, /* call-compiled/n7/retR */
      0, /* call-compiled/n8/retR */
      0, /* call-compiled/n9/retR */
      0, /* call-compiled/n10/retR */
      0, /* call-compiled/nR/retR */
      0, /* call-from-c/retR */
      0, /* canonicalize-boolean */
      0 | (1UL << 0), /* check-closure/fR */
      0 | (1UL << 1), /* check-global-defined/nR/fR */
      0 | (1UL << 0), /* check-in-arity/n0/fR */
      0 | (1UL << 0), /* check-in-arity/n1/fR */
      0 | (1UL << 0), /* check-in-arity/n2/fR */
      0 | (1UL << 0), /* check-in-arity/n3/fR */
      0 | (1UL << 0), /* check-in-arity/n4/fR */
      0 | (1UL << 0), /* check-in-arity/n5/fR */
      0 | (1UL << 0), /* check-in-arity/n6/fR */
      0 | (1UL << 0), /* check-in-arity/n7/fR */
      0 | (1UL << 0), /* check-in-arity/n8/fR */
      0 | (1UL << 0), /* check-in-arity/n9/fR */
      0 | (1UL << 0), /* check-in-arity/n10/fR */
      0 | (1UL << 1), /* check-in-arity/nR/fR */
      0 | (1UL << 0), /* check-in-arity--alt/n0/fR */
      0 | (1UL << 0), /* check-in-arity--alt/n1/fR */
      0 | (1UL << 0), /* check-in-arity--alt/n2/fR */
      0 | (1UL << 0), /* check-in-arity--alt/n3/fR */
      0 | (1UL << 0), /* check-in-arity--alt/n4/fR */
      0 | (1UL << 0), /* check-in-arity--alt/n5/fR */
      0 | (1UL << 0), /* check-in-arity--alt/n6/fR */
      0 | (1UL << 0), /* check-in-arity--alt/n7/fR */
      0 | (1UL << 0), /* check-in-arity--alt/n8/fR */
      0 | (1UL << 0), /* check-in-arity--alt/n9/fR */
      0 | (1UL << 0), /* check-in-arity--alt/n10/fR */
      0 | (1UL << 1), /* check-in-arity--alt/nR/fR */
      0, /* copy-from-literal/nR */
      0, /* copy-from-register/%rR */
      0, /* copy-to-register/%rR */
      0, /* drop */
      0, /* drop-nip */
      0, /* dup */
      0, /* exitvm */
      0, /* fail/retR */
      0 | (1UL << 0), /* gc-if-needed/fR */
      0, /* heap-allocate/n4 */
      0, /* heap-allocate/n8 */
      0, /* heap-allocate/n12 */
      0, /* heap-allocate/n16 */
      0, /* heap-allocate/n24 */
      0, /* heap-allocate/n32 */
      0, /* heap-allocate/n36 */
      0, /* heap-allocate/n48 */
      0, /* heap-allocate/n52 */
      0, /* heap-allocate/n64 */
      0, /* heap-allocate/nR */
      0, /* literal-to-register/nR/%rR */
      0, /* nip */
      0, /* nip-drop */
      0, /* nip-five */
      0, /* nip-five-drop */
      0, /* nip-four */
      0, /* nip-four-drop */
      0, /* nip-push-literal/nR */
      0, /* nip-push-register/%rR */
      0, /* nip-six */
      0, /* nip-six-drop */
      0, /* nip-three */
      0, /* nip-three-drop */
      0, /* nip-two */
      0, /* nip-two-drop */
      0, /* nop */
      0 | (1UL << 1), /* pop-to-global/nR/fR */
      0 | (1UL << 1), /* pop-to-global-defined/nR/fR */
      0, /* pop-to-register/%rR */
      0 | (1UL << 1), /* primitive/nR/n0/fR */
      0 | (1UL << 1), /* primitive/nR/n1/fR */
      0 | (1UL << 1), /* primitive/nR/n2/fR */
      0 | (1UL << 1), /* primitive/nR/n3/fR */
      0 | (1UL << 1), /* primitive/nR/n4/fR */
      0 | (1UL << 2), /* primitive/nR/nR/fR */
      0, /* primitive-boolean-canonicalize */
      0, /* primitive-box */
      0 | (1UL << 0), /* primitive-box-get/fR */
      0 | (1UL << 0), /* primitive-box-setb-special/fR */
      0 | (1UL << 0), /* primitive-car/fR */
      0 | (1UL << 0), /* primitive-cdr/fR */
      0, /* primitive-characterp */
      0, /* primitive-cons-special */
      0, /* primitive-consp */
      0, /* primitive-eqp */
      0 | (1UL << 0), /* primitive-fixnum-eqp/fR */
      0 | (1UL << 0), /* primitive-fixnum-not-eqp/fR */
      0, /* primitive-fixnump */
      0 | (1UL << 0), /* primitive-greaterp/fR */
      0 | (1UL << 0), /* primitive-lessp/fR */
      0 | (1UL << 0), /* primitive-negate/fR */
      0 | (1UL << 0), /* primitive-negativep/fR */
      0, /* primitive-non-consp */
      0 | (1UL << 0), /* primitive-non-negativep/fR */
      0, /* primitive-non-nullp */
      0 | (1UL << 0), /* primitive-non-positivep/fR */
      0, /* primitive-non-symbolp */
      0 | (1UL << 0), /* primitive-non-zerop/fR */
      0, /* primitive-not */
      0, /* primitive-not-eqp */
      0 | (1UL << 0), /* primitive-not-greaterp/fR */
      0 | (1UL << 0), /* primitive-not-lessp/fR */
      0, /* primitive-nothingp */
      0, /* primitive-nullp */
      0 | (1UL << 0), /* primitive-one-minus/fR */
      0 | (1UL << 0), /* primitive-one-plus/fR */
      0 | (1UL << 0), /* primitive-positivep/fR */
      0 | (1UL << 0), /* primitive-primordial-divided/fR */
      0 | (1UL << 0), /* primitive-primordial-divided-unsafe/fR */
      0 | (1UL << 0), /* primitive-primordial-minus/fR */
      0 | (1UL << 0), /* primitive-primordial-plus/fR */
      0 | (1UL << 0), /* primitive-primordial-times/fR */
      0 | (1UL << 0), /* primitive-quotient/fR */
      0 | (1UL << 0), /* primitive-quotient-unsafe/fR */
      0 | (1UL << 0), /* primitive-remainder/fR */
      0 | (1UL << 0), /* primitive-remainder-unsafe/fR */
      0 | (1UL << 0), /* primitive-set-carb-special/fR */
      0 | (1UL << 0), /* primitive-set-cdrb-special/fR */
      0, /* primitive-symbolp */
      0 | (1UL << 0), /* primitive-two-divided/fR */
      0 | (1UL << 0), /* primitive-two-quotient/fR */
      0 | (1UL << 0), /* primitive-two-remainder/fR */
      0 | (1UL << 0), /* primitive-two-times/fR */
      0, /* primitive-uniquep */
      0 | (1UL << 0), /* primitive-zerop/fR */
      0, /* procedure-prolog */
      0, /* push-false */
      0 | (1UL << 1), /* push-global/nR/fR */
      0, /* push-literal/nR */
      0, /* push-nil */
      0, /* push-nothing */
      0, /* push-one */
      0, /* push-register/%rR */
      0, /* push-unspecified */
      0, /* push-zero */
      0, /* register-to-register/%rR/%rR */
      0, /* restore-register/%rR */
      0, /* return */
      0, /* save-register/%rR */
      0, /* tail-call/n0 */
      0, /* tail-call/n1 */
      0, /* tail-call/n2 */
      0, /* tail-call/n3 */
      0, /* tail-call/n4 */
      0, /* tail-call/n5 */
      0, /* tail-call/n6 */
      0, /* tail-call/n7 */
      0, /* tail-call/n8 */
      0, /* tail-call/n9 */
      0, /* tail-call/n10 */
      0, /* tail-call/nR */
      0, /* tail-call-compiled/n0 */
      0, /* tail-call-compiled/n1 */
      0, /* tail-call-compiled/n2 */
      0, /* tail-call-compiled/n3 */
      0, /* tail-call-compiled/n4 */
      0, /* tail-call-compiled/n5 */
      0, /* tail-call-compiled/n6 */
      0, /* tail-call-compiled/n7 */
      0, /* tail-call-compiled/n8 */
      0, /* tail-call-compiled/n9 */
      0, /* tail-call-compiled/n10 */
      0, /* tail-call-compiled/nR */
      0, /* unreachable */
      0, /* *branch/fR*-no-fast-branches */
      0, /* *branch-if-false/fR*-no-fast-branches */
      0, /* *branch-if-not-less/fR/fR*-no-fast-branches */
      0, /* *branch-if-not-null/fR*-no-fast-branches */
      0, /* *branch-if-null/fR*-no-fast-branches */
      0, /* *branch-if-register-non-zero/%rR/fR/fR*-no-fast-branches */
      0, /* *branch-if-true/fR*-no-fast-branches */
      0, /* *check-closure/fR*-no-fast-branches */
      0, /* *check-global-defined/nR/fR*-no-fast-branches */
      0, /* *check-in-arity/n0/fR*-no-fast-branches */
      0, /* *check-in-arity/n1/fR*-no-fast-branches */
      0, /* *check-in-arity/n2/fR*-no-fast-branches */
      0, /* *check-in-arity/n3/fR*-no-fast-branches */
      0, /* *check-in-arity/n4/fR*-no-fast-branches */
      0, /* *check-in-arity/n5/fR*-no-fast-branches */
      0, /* *check-in-arity/n6/fR*-no-fast-branches */
      0, /* *check-in-arity/n7/fR*-no-fast-branches */
      0, /* *check-in-arity/n8/fR*-no-fast-branches */
      0, /* *check-in-arity/n9/fR*-no-fast-branches */
      0, /* *check-in-arity/n10/fR*-no-fast-branches */
      0, /* *check-in-arity/nR/fR*-no-fast-branches */
      0, /* *check-in-arity--alt/n0/fR*-no-fast-branches */
      0, /* *check-in-arity--alt/n1/fR*-no-fast-branches */
      0, /* *check-in-arity--alt/n2/fR*-no-fast-branches */
      0, /* *check-in-arity--alt/n3/fR*-no-fast-branches */
      0, /* *check-in-arity--alt/n4/fR*-no-fast-branches */
      0, /* *check-in-arity--alt/n5/fR*-no-fast-branches */
      0, /* *check-in-arity--alt/n6/fR*-no-fast-branches */
      0, /* *check-in-arity--alt/n7/fR*-no-fast-branches */
      0, /* *check-in-arity--alt/n8/fR*-no-fast-branches */
      0, /* *check-in-arity--alt/n9/fR*-no-fast-branches */
      0, /* *check-in-arity--alt/n10/fR*-no-fast-branches */
      0, /* *check-in-arity--alt/nR/fR*-no-fast-branches */
      0, /* *gc-if-needed/fR*-no-fast-branches */
      0, /* *pop-to-global/nR/fR*-no-fast-branches */
      0, /* *pop-to-global-defined/nR/fR*-no-fast-branches */
      0, /* *primitive/nR/n0/fR*-no-fast-branches */
      0, /* *primitive/nR/n1/fR*-no-fast-branches */
      0, /* *primitive/nR/n2/fR*-no-fast-branches */
      0, /* *primitive/nR/n3/fR*-no-fast-branches */
      0, /* *primitive/nR/n4/fR*-no-fast-branches */
      0, /* *primitive/nR/nR/fR*-no-fast-branches */
      0, /* *primitive-box-get/fR*-no-fast-branches */
      0, /* *primitive-box-setb-special/fR*-no-fast-branches */
      0, /* *primitive-car/fR*-no-fast-branches */
      0, /* *primitive-cdr/fR*-no-fast-branches */
      0, /* *primitive-fixnum-eqp/fR*-no-fast-branches */
      0, /* *primitive-fixnum-not-eqp/fR*-no-fast-branches */
      0, /* *primitive-greaterp/fR*-no-fast-branches */
      0, /* *primitive-lessp/fR*-no-fast-branches */
      0, /* *primitive-negate/fR*-no-fast-branches */
      0, /* *primitive-negativep/fR*-no-fast-branches */
      0, /* *primitive-non-negativep/fR*-no-fast-branches */
      0, /* *primitive-non-positivep/fR*-no-fast-branches */
      0, /* *primitive-non-zerop/fR*-no-fast-branches */
      0, /* *primitive-not-greaterp/fR*-no-fast-branches */
      0, /* *primitive-not-lessp/fR*-no-fast-branches */
      0, /* *primitive-one-minus/fR*-no-fast-branches */
      0, /* *primitive-one-plus/fR*-no-fast-branches */
      0, /* *primitive-positivep/fR*-no-fast-branches */
      0, /* *primitive-primordial-divided/fR*-no-fast-branches */
      0, /* *primitive-primordial-divided-unsafe/fR*-no-fast-branches */
      0, /* *primitive-primordial-minus/fR*-no-fast-branches */
      0, /* *primitive-primordial-plus/fR*-no-fast-branches */
      0, /* *primitive-primordial-times/fR*-no-fast-branches */
      0, /* *primitive-quotient/fR*-no-fast-branches */
      0, /* *primitive-quotient-unsafe/fR*-no-fast-branches */
      0, /* *primitive-remainder/fR*-no-fast-branches */
      0, /* *primitive-remainder-unsafe/fR*-no-fast-branches */
      0, /* *primitive-set-carb-special/fR*-no-fast-branches */
      0, /* *primitive-set-cdrb-special/fR*-no-fast-branches */
      0, /* *primitive-two-divided/fR*-no-fast-branches */
      0, /* *primitive-two-quotient/fR*-no-fast-branches */
      0, /* *primitive-two-remainder/fR*-no-fast-branches */
      0, /* *primitive-two-times/fR*-no-fast-branches */
      0, /* *primitive-zerop/fR*-no-fast-branches */
      0 /* *push-global/nR/fR*-no-fast-branches */
    };
#endif // #ifdef JITTER_HAVE_PATCH_IN

// FIXME: I may want to conditionalize this.
const bool
jitterlispvm_specialized_instruction_relocatables [JITTERLISPVM_SPECIALIZED_INSTRUCTION_NO]
  = {
      true, // !INVALID
      true, // !BEGINBASICBLOCK
      true, // !EXITVM
      true, // !DATALOCATIONS
      true, // !NOP
      true, // !UNREACHABLE0
      true, // !UNREACHABLE1
      true, // !UNREACHABLE2
      true, // at-depth-to-register/n1/%rR
      true, // at-depth-to-register/n2/%rR
      true, // at-depth-to-register/n3/%rR
      true, // at-depth-to-register/n4/%rR
      true, // at-depth-to-register/n5/%rR
      true, // at-depth-to-register/n6/%rR
      true, // at-depth-to-register/n7/%rR
      true, // at-depth-to-register/n8/%rR
      true, // at-depth-to-register/n9/%rR
      true, // at-depth-to-register/n10/%rR
      true, // at-depth-to-register/nR/%rR
      true, // branch/fR
      true, // branch-if-false/fR
      true, // branch-if-not-less/fR/fR
      true, // branch-if-not-null/fR
      true, // branch-if-null/fR
      true, // branch-if-register-non-zero/%rR/fR/fR
      true, // branch-if-true/fR
      true, // call/n0/retR
      true, // call/n1/retR
      true, // call/n2/retR
      true, // call/n3/retR
      true, // call/n4/retR
      true, // call/n5/retR
      true, // call/n6/retR
      true, // call/n7/retR
      true, // call/n8/retR
      true, // call/n9/retR
      true, // call/n10/retR
      true, // call/nR/retR
      true, // call-compiled/n0/retR
      true, // call-compiled/n1/retR
      true, // call-compiled/n2/retR
      true, // call-compiled/n3/retR
      true, // call-compiled/n4/retR
      true, // call-compiled/n5/retR
      true, // call-compiled/n6/retR
      true, // call-compiled/n7/retR
      true, // call-compiled/n8/retR
      true, // call-compiled/n9/retR
      true, // call-compiled/n10/retR
      true, // call-compiled/nR/retR
      true, // call-from-c/retR
      true, // canonicalize-boolean
      true, // check-closure/fR
      true, // check-global-defined/nR/fR
      true, // check-in-arity/n0/fR
      true, // check-in-arity/n1/fR
      true, // check-in-arity/n2/fR
      true, // check-in-arity/n3/fR
      true, // check-in-arity/n4/fR
      true, // check-in-arity/n5/fR
      true, // check-in-arity/n6/fR
      true, // check-in-arity/n7/fR
      true, // check-in-arity/n8/fR
      true, // check-in-arity/n9/fR
      true, // check-in-arity/n10/fR
      true, // check-in-arity/nR/fR
      true, // check-in-arity--alt/n0/fR
      true, // check-in-arity--alt/n1/fR
      true, // check-in-arity--alt/n2/fR
      true, // check-in-arity--alt/n3/fR
      true, // check-in-arity--alt/n4/fR
      true, // check-in-arity--alt/n5/fR
      true, // check-in-arity--alt/n6/fR
      true, // check-in-arity--alt/n7/fR
      true, // check-in-arity--alt/n8/fR
      true, // check-in-arity--alt/n9/fR
      true, // check-in-arity--alt/n10/fR
      true, // check-in-arity--alt/nR/fR
      true, // copy-from-literal/nR
      true, // copy-from-register/%rR
      true, // copy-to-register/%rR
      true, // drop
      true, // drop-nip
      true, // dup
      true, // exitvm
      false, // fail/retR
      true, // gc-if-needed/fR
      true, // heap-allocate/n4
      true, // heap-allocate/n8
      true, // heap-allocate/n12
      true, // heap-allocate/n16
      true, // heap-allocate/n24
      true, // heap-allocate/n32
      true, // heap-allocate/n36
      true, // heap-allocate/n48
      true, // heap-allocate/n52
      true, // heap-allocate/n64
      true, // heap-allocate/nR
      true, // literal-to-register/nR/%rR
      true, // nip
      true, // nip-drop
      true, // nip-five
      true, // nip-five-drop
      true, // nip-four
      true, // nip-four-drop
      true, // nip-push-literal/nR
      true, // nip-push-register/%rR
      true, // nip-six
      true, // nip-six-drop
      true, // nip-three
      true, // nip-three-drop
      true, // nip-two
      true, // nip-two-drop
      true, // nop
      true, // pop-to-global/nR/fR
      true, // pop-to-global-defined/nR/fR
      true, // pop-to-register/%rR
      true, // primitive/nR/n0/fR
      true, // primitive/nR/n1/fR
      true, // primitive/nR/n2/fR
      true, // primitive/nR/n3/fR
      true, // primitive/nR/n4/fR
      true, // primitive/nR/nR/fR
      true, // primitive-boolean-canonicalize
      true, // primitive-box
      true, // primitive-box-get/fR
      true, // primitive-box-setb-special/fR
      true, // primitive-car/fR
      true, // primitive-cdr/fR
      true, // primitive-characterp
      true, // primitive-cons-special
      true, // primitive-consp
      true, // primitive-eqp
      true, // primitive-fixnum-eqp/fR
      true, // primitive-fixnum-not-eqp/fR
      true, // primitive-fixnump
      true, // primitive-greaterp/fR
      true, // primitive-lessp/fR
      true, // primitive-negate/fR
      true, // primitive-negativep/fR
      true, // primitive-non-consp
      true, // primitive-non-negativep/fR
      true, // primitive-non-nullp
      true, // primitive-non-positivep/fR
      true, // primitive-non-symbolp
      true, // primitive-non-zerop/fR
      true, // primitive-not
      true, // primitive-not-eqp
      true, // primitive-not-greaterp/fR
      true, // primitive-not-lessp/fR
      true, // primitive-nothingp
      true, // primitive-nullp
      true, // primitive-one-minus/fR
      true, // primitive-one-plus/fR
      true, // primitive-positivep/fR
      true, // primitive-primordial-divided/fR
      true, // primitive-primordial-divided-unsafe/fR
      true, // primitive-primordial-minus/fR
      true, // primitive-primordial-plus/fR
      true, // primitive-primordial-times/fR
      true, // primitive-quotient/fR
      true, // primitive-quotient-unsafe/fR
      true, // primitive-remainder/fR
      true, // primitive-remainder-unsafe/fR
      true, // primitive-set-carb-special/fR
      true, // primitive-set-cdrb-special/fR
      true, // primitive-symbolp
      true, // primitive-two-divided/fR
      true, // primitive-two-quotient/fR
      true, // primitive-two-remainder/fR
      true, // primitive-two-times/fR
      true, // primitive-uniquep
      true, // primitive-zerop/fR
      true, // procedure-prolog
      true, // push-false
      true, // push-global/nR/fR
      true, // push-literal/nR
      true, // push-nil
      true, // push-nothing
      true, // push-one
      true, // push-register/%rR
      true, // push-unspecified
      true, // push-zero
      true, // register-to-register/%rR/%rR
      true, // restore-register/%rR
      true, // return
      true, // save-register/%rR
      true, // tail-call/n0
      true, // tail-call/n1
      true, // tail-call/n2
      true, // tail-call/n3
      true, // tail-call/n4
      true, // tail-call/n5
      true, // tail-call/n6
      true, // tail-call/n7
      true, // tail-call/n8
      true, // tail-call/n9
      true, // tail-call/n10
      true, // tail-call/nR
      true, // tail-call-compiled/n0
      true, // tail-call-compiled/n1
      true, // tail-call-compiled/n2
      true, // tail-call-compiled/n3
      true, // tail-call-compiled/n4
      true, // tail-call-compiled/n5
      true, // tail-call-compiled/n6
      true, // tail-call-compiled/n7
      true, // tail-call-compiled/n8
      true, // tail-call-compiled/n9
      true, // tail-call-compiled/n10
      true, // tail-call-compiled/nR
      true, // unreachable
      false, // *branch/fR*-no-fast-branches
      false, // *branch-if-false/fR*-no-fast-branches
      false, // *branch-if-not-less/fR/fR*-no-fast-branches
      false, // *branch-if-not-null/fR*-no-fast-branches
      false, // *branch-if-null/fR*-no-fast-branches
      false, // *branch-if-register-non-zero/%rR/fR/fR*-no-fast-branches
      false, // *branch-if-true/fR*-no-fast-branches
      false, // *check-closure/fR*-no-fast-branches
      false, // *check-global-defined/nR/fR*-no-fast-branches
      false, // *check-in-arity/n0/fR*-no-fast-branches
      false, // *check-in-arity/n1/fR*-no-fast-branches
      false, // *check-in-arity/n2/fR*-no-fast-branches
      false, // *check-in-arity/n3/fR*-no-fast-branches
      false, // *check-in-arity/n4/fR*-no-fast-branches
      false, // *check-in-arity/n5/fR*-no-fast-branches
      false, // *check-in-arity/n6/fR*-no-fast-branches
      false, // *check-in-arity/n7/fR*-no-fast-branches
      false, // *check-in-arity/n8/fR*-no-fast-branches
      false, // *check-in-arity/n9/fR*-no-fast-branches
      false, // *check-in-arity/n10/fR*-no-fast-branches
      false, // *check-in-arity/nR/fR*-no-fast-branches
      false, // *check-in-arity--alt/n0/fR*-no-fast-branches
      false, // *check-in-arity--alt/n1/fR*-no-fast-branches
      false, // *check-in-arity--alt/n2/fR*-no-fast-branches
      false, // *check-in-arity--alt/n3/fR*-no-fast-branches
      false, // *check-in-arity--alt/n4/fR*-no-fast-branches
      false, // *check-in-arity--alt/n5/fR*-no-fast-branches
      false, // *check-in-arity--alt/n6/fR*-no-fast-branches
      false, // *check-in-arity--alt/n7/fR*-no-fast-branches
      false, // *check-in-arity--alt/n8/fR*-no-fast-branches
      false, // *check-in-arity--alt/n9/fR*-no-fast-branches
      false, // *check-in-arity--alt/n10/fR*-no-fast-branches
      false, // *check-in-arity--alt/nR/fR*-no-fast-branches
      false, // *gc-if-needed/fR*-no-fast-branches
      false, // *pop-to-global/nR/fR*-no-fast-branches
      false, // *pop-to-global-defined/nR/fR*-no-fast-branches
      false, // *primitive/nR/n0/fR*-no-fast-branches
      false, // *primitive/nR/n1/fR*-no-fast-branches
      false, // *primitive/nR/n2/fR*-no-fast-branches
      false, // *primitive/nR/n3/fR*-no-fast-branches
      false, // *primitive/nR/n4/fR*-no-fast-branches
      false, // *primitive/nR/nR/fR*-no-fast-branches
      false, // *primitive-box-get/fR*-no-fast-branches
      false, // *primitive-box-setb-special/fR*-no-fast-branches
      false, // *primitive-car/fR*-no-fast-branches
      false, // *primitive-cdr/fR*-no-fast-branches
      false, // *primitive-fixnum-eqp/fR*-no-fast-branches
      false, // *primitive-fixnum-not-eqp/fR*-no-fast-branches
      false, // *primitive-greaterp/fR*-no-fast-branches
      false, // *primitive-lessp/fR*-no-fast-branches
      false, // *primitive-negate/fR*-no-fast-branches
      false, // *primitive-negativep/fR*-no-fast-branches
      false, // *primitive-non-negativep/fR*-no-fast-branches
      false, // *primitive-non-positivep/fR*-no-fast-branches
      false, // *primitive-non-zerop/fR*-no-fast-branches
      false, // *primitive-not-greaterp/fR*-no-fast-branches
      false, // *primitive-not-lessp/fR*-no-fast-branches
      false, // *primitive-one-minus/fR*-no-fast-branches
      false, // *primitive-one-plus/fR*-no-fast-branches
      false, // *primitive-positivep/fR*-no-fast-branches
      false, // *primitive-primordial-divided/fR*-no-fast-branches
      false, // *primitive-primordial-divided-unsafe/fR*-no-fast-branches
      false, // *primitive-primordial-minus/fR*-no-fast-branches
      false, // *primitive-primordial-plus/fR*-no-fast-branches
      false, // *primitive-primordial-times/fR*-no-fast-branches
      false, // *primitive-quotient/fR*-no-fast-branches
      false, // *primitive-quotient-unsafe/fR*-no-fast-branches
      false, // *primitive-remainder/fR*-no-fast-branches
      false, // *primitive-remainder-unsafe/fR*-no-fast-branches
      false, // *primitive-set-carb-special/fR*-no-fast-branches
      false, // *primitive-set-cdrb-special/fR*-no-fast-branches
      false, // *primitive-two-divided/fR*-no-fast-branches
      false, // *primitive-two-quotient/fR*-no-fast-branches
      false, // *primitive-two-remainder/fR*-no-fast-branches
      false, // *primitive-two-times/fR*-no-fast-branches
      false, // *primitive-zerop/fR*-no-fast-branches
      false // *push-global/nR/fR*-no-fast-branches
    };

// FIXME: this is not currently accessed, and in fact may be useless.
const bool
jitterlispvm_specialized_instruction_callers [JITTERLISPVM_SPECIALIZED_INSTRUCTION_NO]
  = {
      false, // !INVALID
      false, // !BEGINBASICBLOCK
      false, // !EXITVM
      false, // !DATALOCATIONS
      false, // !NOP
      false, // !UNREACHABLE0
      false, // !UNREACHABLE1
      false, // !UNREACHABLE2
      false, // at-depth-to-register/n1/%rR
      false, // at-depth-to-register/n2/%rR
      false, // at-depth-to-register/n3/%rR
      false, // at-depth-to-register/n4/%rR
      false, // at-depth-to-register/n5/%rR
      false, // at-depth-to-register/n6/%rR
      false, // at-depth-to-register/n7/%rR
      false, // at-depth-to-register/n8/%rR
      false, // at-depth-to-register/n9/%rR
      false, // at-depth-to-register/n10/%rR
      false, // at-depth-to-register/nR/%rR
      false, // branch/fR
      false, // branch-if-false/fR
      false, // branch-if-not-less/fR/fR
      false, // branch-if-not-null/fR
      false, // branch-if-null/fR
      false, // branch-if-register-non-zero/%rR/fR/fR
      false, // branch-if-true/fR
      true, // call/n0/retR
      true, // call/n1/retR
      true, // call/n2/retR
      true, // call/n3/retR
      true, // call/n4/retR
      true, // call/n5/retR
      true, // call/n6/retR
      true, // call/n7/retR
      true, // call/n8/retR
      true, // call/n9/retR
      true, // call/n10/retR
      true, // call/nR/retR
      true, // call-compiled/n0/retR
      true, // call-compiled/n1/retR
      true, // call-compiled/n2/retR
      true, // call-compiled/n3/retR
      true, // call-compiled/n4/retR
      true, // call-compiled/n5/retR
      true, // call-compiled/n6/retR
      true, // call-compiled/n7/retR
      true, // call-compiled/n8/retR
      true, // call-compiled/n9/retR
      true, // call-compiled/n10/retR
      true, // call-compiled/nR/retR
      true, // call-from-c/retR
      false, // canonicalize-boolean
      false, // check-closure/fR
      false, // check-global-defined/nR/fR
      false, // check-in-arity/n0/fR
      false, // check-in-arity/n1/fR
      false, // check-in-arity/n2/fR
      false, // check-in-arity/n3/fR
      false, // check-in-arity/n4/fR
      false, // check-in-arity/n5/fR
      false, // check-in-arity/n6/fR
      false, // check-in-arity/n7/fR
      false, // check-in-arity/n8/fR
      false, // check-in-arity/n9/fR
      false, // check-in-arity/n10/fR
      false, // check-in-arity/nR/fR
      false, // check-in-arity--alt/n0/fR
      false, // check-in-arity--alt/n1/fR
      false, // check-in-arity--alt/n2/fR
      false, // check-in-arity--alt/n3/fR
      false, // check-in-arity--alt/n4/fR
      false, // check-in-arity--alt/n5/fR
      false, // check-in-arity--alt/n6/fR
      false, // check-in-arity--alt/n7/fR
      false, // check-in-arity--alt/n8/fR
      false, // check-in-arity--alt/n9/fR
      false, // check-in-arity--alt/n10/fR
      false, // check-in-arity--alt/nR/fR
      false, // copy-from-literal/nR
      false, // copy-from-register/%rR
      false, // copy-to-register/%rR
      false, // drop
      false, // drop-nip
      false, // dup
      false, // exitvm
      false, // fail/retR
      false, // gc-if-needed/fR
      false, // heap-allocate/n4
      false, // heap-allocate/n8
      false, // heap-allocate/n12
      false, // heap-allocate/n16
      false, // heap-allocate/n24
      false, // heap-allocate/n32
      false, // heap-allocate/n36
      false, // heap-allocate/n48
      false, // heap-allocate/n52
      false, // heap-allocate/n64
      false, // heap-allocate/nR
      false, // literal-to-register/nR/%rR
      false, // nip
      false, // nip-drop
      false, // nip-five
      false, // nip-five-drop
      false, // nip-four
      false, // nip-four-drop
      false, // nip-push-literal/nR
      false, // nip-push-register/%rR
      false, // nip-six
      false, // nip-six-drop
      false, // nip-three
      false, // nip-three-drop
      false, // nip-two
      false, // nip-two-drop
      false, // nop
      false, // pop-to-global/nR/fR
      false, // pop-to-global-defined/nR/fR
      false, // pop-to-register/%rR
      false, // primitive/nR/n0/fR
      false, // primitive/nR/n1/fR
      false, // primitive/nR/n2/fR
      false, // primitive/nR/n3/fR
      false, // primitive/nR/n4/fR
      false, // primitive/nR/nR/fR
      false, // primitive-boolean-canonicalize
      false, // primitive-box
      false, // primitive-box-get/fR
      false, // primitive-box-setb-special/fR
      false, // primitive-car/fR
      false, // primitive-cdr/fR
      false, // primitive-characterp
      false, // primitive-cons-special
      false, // primitive-consp
      false, // primitive-eqp
      false, // primitive-fixnum-eqp/fR
      false, // primitive-fixnum-not-eqp/fR
      false, // primitive-fixnump
      false, // primitive-greaterp/fR
      false, // primitive-lessp/fR
      false, // primitive-negate/fR
      false, // primitive-negativep/fR
      false, // primitive-non-consp
      false, // primitive-non-negativep/fR
      false, // primitive-non-nullp
      false, // primitive-non-positivep/fR
      false, // primitive-non-symbolp
      false, // primitive-non-zerop/fR
      false, // primitive-not
      false, // primitive-not-eqp
      false, // primitive-not-greaterp/fR
      false, // primitive-not-lessp/fR
      false, // primitive-nothingp
      false, // primitive-nullp
      false, // primitive-one-minus/fR
      false, // primitive-one-plus/fR
      false, // primitive-positivep/fR
      false, // primitive-primordial-divided/fR
      false, // primitive-primordial-divided-unsafe/fR
      false, // primitive-primordial-minus/fR
      false, // primitive-primordial-plus/fR
      false, // primitive-primordial-times/fR
      false, // primitive-quotient/fR
      false, // primitive-quotient-unsafe/fR
      false, // primitive-remainder/fR
      false, // primitive-remainder-unsafe/fR
      false, // primitive-set-carb-special/fR
      false, // primitive-set-cdrb-special/fR
      false, // primitive-symbolp
      false, // primitive-two-divided/fR
      false, // primitive-two-quotient/fR
      false, // primitive-two-remainder/fR
      false, // primitive-two-times/fR
      false, // primitive-uniquep
      false, // primitive-zerop/fR
      false, // procedure-prolog
      false, // push-false
      false, // push-global/nR/fR
      false, // push-literal/nR
      false, // push-nil
      false, // push-nothing
      false, // push-one
      false, // push-register/%rR
      false, // push-unspecified
      false, // push-zero
      false, // register-to-register/%rR/%rR
      false, // restore-register/%rR
      false, // return
      false, // save-register/%rR
      false, // tail-call/n0
      false, // tail-call/n1
      false, // tail-call/n2
      false, // tail-call/n3
      false, // tail-call/n4
      false, // tail-call/n5
      false, // tail-call/n6
      false, // tail-call/n7
      false, // tail-call/n8
      false, // tail-call/n9
      false, // tail-call/n10
      false, // tail-call/nR
      false, // tail-call-compiled/n0
      false, // tail-call-compiled/n1
      false, // tail-call-compiled/n2
      false, // tail-call-compiled/n3
      false, // tail-call-compiled/n4
      false, // tail-call-compiled/n5
      false, // tail-call-compiled/n6
      false, // tail-call-compiled/n7
      false, // tail-call-compiled/n8
      false, // tail-call-compiled/n9
      false, // tail-call-compiled/n10
      false, // tail-call-compiled/nR
      false, // unreachable
      false, // *branch/fR*-no-fast-branches
      false, // *branch-if-false/fR*-no-fast-branches
      false, // *branch-if-not-less/fR/fR*-no-fast-branches
      false, // *branch-if-not-null/fR*-no-fast-branches
      false, // *branch-if-null/fR*-no-fast-branches
      false, // *branch-if-register-non-zero/%rR/fR/fR*-no-fast-branches
      false, // *branch-if-true/fR*-no-fast-branches
      false, // *check-closure/fR*-no-fast-branches
      false, // *check-global-defined/nR/fR*-no-fast-branches
      false, // *check-in-arity/n0/fR*-no-fast-branches
      false, // *check-in-arity/n1/fR*-no-fast-branches
      false, // *check-in-arity/n2/fR*-no-fast-branches
      false, // *check-in-arity/n3/fR*-no-fast-branches
      false, // *check-in-arity/n4/fR*-no-fast-branches
      false, // *check-in-arity/n5/fR*-no-fast-branches
      false, // *check-in-arity/n6/fR*-no-fast-branches
      false, // *check-in-arity/n7/fR*-no-fast-branches
      false, // *check-in-arity/n8/fR*-no-fast-branches
      false, // *check-in-arity/n9/fR*-no-fast-branches
      false, // *check-in-arity/n10/fR*-no-fast-branches
      false, // *check-in-arity/nR/fR*-no-fast-branches
      false, // *check-in-arity--alt/n0/fR*-no-fast-branches
      false, // *check-in-arity--alt/n1/fR*-no-fast-branches
      false, // *check-in-arity--alt/n2/fR*-no-fast-branches
      false, // *check-in-arity--alt/n3/fR*-no-fast-branches
      false, // *check-in-arity--alt/n4/fR*-no-fast-branches
      false, // *check-in-arity--alt/n5/fR*-no-fast-branches
      false, // *check-in-arity--alt/n6/fR*-no-fast-branches
      false, // *check-in-arity--alt/n7/fR*-no-fast-branches
      false, // *check-in-arity--alt/n8/fR*-no-fast-branches
      false, // *check-in-arity--alt/n9/fR*-no-fast-branches
      false, // *check-in-arity--alt/n10/fR*-no-fast-branches
      false, // *check-in-arity--alt/nR/fR*-no-fast-branches
      false, // *gc-if-needed/fR*-no-fast-branches
      false, // *pop-to-global/nR/fR*-no-fast-branches
      false, // *pop-to-global-defined/nR/fR*-no-fast-branches
      false, // *primitive/nR/n0/fR*-no-fast-branches
      false, // *primitive/nR/n1/fR*-no-fast-branches
      false, // *primitive/nR/n2/fR*-no-fast-branches
      false, // *primitive/nR/n3/fR*-no-fast-branches
      false, // *primitive/nR/n4/fR*-no-fast-branches
      false, // *primitive/nR/nR/fR*-no-fast-branches
      false, // *primitive-box-get/fR*-no-fast-branches
      false, // *primitive-box-setb-special/fR*-no-fast-branches
      false, // *primitive-car/fR*-no-fast-branches
      false, // *primitive-cdr/fR*-no-fast-branches
      false, // *primitive-fixnum-eqp/fR*-no-fast-branches
      false, // *primitive-fixnum-not-eqp/fR*-no-fast-branches
      false, // *primitive-greaterp/fR*-no-fast-branches
      false, // *primitive-lessp/fR*-no-fast-branches
      false, // *primitive-negate/fR*-no-fast-branches
      false, // *primitive-negativep/fR*-no-fast-branches
      false, // *primitive-non-negativep/fR*-no-fast-branches
      false, // *primitive-non-positivep/fR*-no-fast-branches
      false, // *primitive-non-zerop/fR*-no-fast-branches
      false, // *primitive-not-greaterp/fR*-no-fast-branches
      false, // *primitive-not-lessp/fR*-no-fast-branches
      false, // *primitive-one-minus/fR*-no-fast-branches
      false, // *primitive-one-plus/fR*-no-fast-branches
      false, // *primitive-positivep/fR*-no-fast-branches
      false, // *primitive-primordial-divided/fR*-no-fast-branches
      false, // *primitive-primordial-divided-unsafe/fR*-no-fast-branches
      false, // *primitive-primordial-minus/fR*-no-fast-branches
      false, // *primitive-primordial-plus/fR*-no-fast-branches
      false, // *primitive-primordial-times/fR*-no-fast-branches
      false, // *primitive-quotient/fR*-no-fast-branches
      false, // *primitive-quotient-unsafe/fR*-no-fast-branches
      false, // *primitive-remainder/fR*-no-fast-branches
      false, // *primitive-remainder-unsafe/fR*-no-fast-branches
      false, // *primitive-set-carb-special/fR*-no-fast-branches
      false, // *primitive-set-cdrb-special/fR*-no-fast-branches
      false, // *primitive-two-divided/fR*-no-fast-branches
      false, // *primitive-two-quotient/fR*-no-fast-branches
      false, // *primitive-two-remainder/fR*-no-fast-branches
      false, // *primitive-two-times/fR*-no-fast-branches
      false, // *primitive-zerop/fR*-no-fast-branches
      false // *push-global/nR/fR*-no-fast-branches
    };

// FIXME: this is not currently accessed, and in fact may be useless.
const bool
jitterlispvm_specialized_instruction_callees [JITTERLISPVM_SPECIALIZED_INSTRUCTION_NO]
  = {
      false, // !INVALID
      false, // !BEGINBASICBLOCK
      false, // !EXITVM
      false, // !DATALOCATIONS
      false, // !NOP
      false, // !UNREACHABLE0
      false, // !UNREACHABLE1
      false, // !UNREACHABLE2
      false, // at-depth-to-register/n1/%rR
      false, // at-depth-to-register/n2/%rR
      false, // at-depth-to-register/n3/%rR
      false, // at-depth-to-register/n4/%rR
      false, // at-depth-to-register/n5/%rR
      false, // at-depth-to-register/n6/%rR
      false, // at-depth-to-register/n7/%rR
      false, // at-depth-to-register/n8/%rR
      false, // at-depth-to-register/n9/%rR
      false, // at-depth-to-register/n10/%rR
      false, // at-depth-to-register/nR/%rR
      false, // branch/fR
      false, // branch-if-false/fR
      false, // branch-if-not-less/fR/fR
      false, // branch-if-not-null/fR
      false, // branch-if-null/fR
      false, // branch-if-register-non-zero/%rR/fR/fR
      false, // branch-if-true/fR
      false, // call/n0/retR
      false, // call/n1/retR
      false, // call/n2/retR
      false, // call/n3/retR
      false, // call/n4/retR
      false, // call/n5/retR
      false, // call/n6/retR
      false, // call/n7/retR
      false, // call/n8/retR
      false, // call/n9/retR
      false, // call/n10/retR
      false, // call/nR/retR
      false, // call-compiled/n0/retR
      false, // call-compiled/n1/retR
      false, // call-compiled/n2/retR
      false, // call-compiled/n3/retR
      false, // call-compiled/n4/retR
      false, // call-compiled/n5/retR
      false, // call-compiled/n6/retR
      false, // call-compiled/n7/retR
      false, // call-compiled/n8/retR
      false, // call-compiled/n9/retR
      false, // call-compiled/n10/retR
      false, // call-compiled/nR/retR
      false, // call-from-c/retR
      false, // canonicalize-boolean
      false, // check-closure/fR
      false, // check-global-defined/nR/fR
      false, // check-in-arity/n0/fR
      false, // check-in-arity/n1/fR
      false, // check-in-arity/n2/fR
      false, // check-in-arity/n3/fR
      false, // check-in-arity/n4/fR
      false, // check-in-arity/n5/fR
      false, // check-in-arity/n6/fR
      false, // check-in-arity/n7/fR
      false, // check-in-arity/n8/fR
      false, // check-in-arity/n9/fR
      false, // check-in-arity/n10/fR
      false, // check-in-arity/nR/fR
      false, // check-in-arity--alt/n0/fR
      false, // check-in-arity--alt/n1/fR
      false, // check-in-arity--alt/n2/fR
      false, // check-in-arity--alt/n3/fR
      false, // check-in-arity--alt/n4/fR
      false, // check-in-arity--alt/n5/fR
      false, // check-in-arity--alt/n6/fR
      false, // check-in-arity--alt/n7/fR
      false, // check-in-arity--alt/n8/fR
      false, // check-in-arity--alt/n9/fR
      false, // check-in-arity--alt/n10/fR
      false, // check-in-arity--alt/nR/fR
      false, // copy-from-literal/nR
      false, // copy-from-register/%rR
      false, // copy-to-register/%rR
      false, // drop
      false, // drop-nip
      false, // dup
      false, // exitvm
      false, // fail/retR
      false, // gc-if-needed/fR
      false, // heap-allocate/n4
      false, // heap-allocate/n8
      false, // heap-allocate/n12
      false, // heap-allocate/n16
      false, // heap-allocate/n24
      false, // heap-allocate/n32
      false, // heap-allocate/n36
      false, // heap-allocate/n48
      false, // heap-allocate/n52
      false, // heap-allocate/n64
      false, // heap-allocate/nR
      false, // literal-to-register/nR/%rR
      false, // nip
      false, // nip-drop
      false, // nip-five
      false, // nip-five-drop
      false, // nip-four
      false, // nip-four-drop
      false, // nip-push-literal/nR
      false, // nip-push-register/%rR
      false, // nip-six
      false, // nip-six-drop
      false, // nip-three
      false, // nip-three-drop
      false, // nip-two
      false, // nip-two-drop
      false, // nop
      false, // pop-to-global/nR/fR
      false, // pop-to-global-defined/nR/fR
      false, // pop-to-register/%rR
      false, // primitive/nR/n0/fR
      false, // primitive/nR/n1/fR
      false, // primitive/nR/n2/fR
      false, // primitive/nR/n3/fR
      false, // primitive/nR/n4/fR
      false, // primitive/nR/nR/fR
      false, // primitive-boolean-canonicalize
      false, // primitive-box
      false, // primitive-box-get/fR
      false, // primitive-box-setb-special/fR
      false, // primitive-car/fR
      false, // primitive-cdr/fR
      false, // primitive-characterp
      false, // primitive-cons-special
      false, // primitive-consp
      false, // primitive-eqp
      false, // primitive-fixnum-eqp/fR
      false, // primitive-fixnum-not-eqp/fR
      false, // primitive-fixnump
      false, // primitive-greaterp/fR
      false, // primitive-lessp/fR
      false, // primitive-negate/fR
      false, // primitive-negativep/fR
      false, // primitive-non-consp
      false, // primitive-non-negativep/fR
      false, // primitive-non-nullp
      false, // primitive-non-positivep/fR
      false, // primitive-non-symbolp
      false, // primitive-non-zerop/fR
      false, // primitive-not
      false, // primitive-not-eqp
      false, // primitive-not-greaterp/fR
      false, // primitive-not-lessp/fR
      false, // primitive-nothingp
      false, // primitive-nullp
      false, // primitive-one-minus/fR
      false, // primitive-one-plus/fR
      false, // primitive-positivep/fR
      false, // primitive-primordial-divided/fR
      false, // primitive-primordial-divided-unsafe/fR
      false, // primitive-primordial-minus/fR
      false, // primitive-primordial-plus/fR
      false, // primitive-primordial-times/fR
      false, // primitive-quotient/fR
      false, // primitive-quotient-unsafe/fR
      false, // primitive-remainder/fR
      false, // primitive-remainder-unsafe/fR
      false, // primitive-set-carb-special/fR
      false, // primitive-set-cdrb-special/fR
      false, // primitive-symbolp
      false, // primitive-two-divided/fR
      false, // primitive-two-quotient/fR
      false, // primitive-two-remainder/fR
      false, // primitive-two-times/fR
      false, // primitive-uniquep
      false, // primitive-zerop/fR
      true, // procedure-prolog
      false, // push-false
      false, // push-global/nR/fR
      false, // push-literal/nR
      false, // push-nil
      false, // push-nothing
      false, // push-one
      false, // push-register/%rR
      false, // push-unspecified
      false, // push-zero
      false, // register-to-register/%rR/%rR
      false, // restore-register/%rR
      false, // return
      false, // save-register/%rR
      false, // tail-call/n0
      false, // tail-call/n1
      false, // tail-call/n2
      false, // tail-call/n3
      false, // tail-call/n4
      false, // tail-call/n5
      false, // tail-call/n6
      false, // tail-call/n7
      false, // tail-call/n8
      false, // tail-call/n9
      false, // tail-call/n10
      false, // tail-call/nR
      false, // tail-call-compiled/n0
      false, // tail-call-compiled/n1
      false, // tail-call-compiled/n2
      false, // tail-call-compiled/n3
      false, // tail-call-compiled/n4
      false, // tail-call-compiled/n5
      false, // tail-call-compiled/n6
      false, // tail-call-compiled/n7
      false, // tail-call-compiled/n8
      false, // tail-call-compiled/n9
      false, // tail-call-compiled/n10
      false, // tail-call-compiled/nR
      false, // unreachable
      false, // *branch/fR*-no-fast-branches
      false, // *branch-if-false/fR*-no-fast-branches
      false, // *branch-if-not-less/fR/fR*-no-fast-branches
      false, // *branch-if-not-null/fR*-no-fast-branches
      false, // *branch-if-null/fR*-no-fast-branches
      false, // *branch-if-register-non-zero/%rR/fR/fR*-no-fast-branches
      false, // *branch-if-true/fR*-no-fast-branches
      false, // *check-closure/fR*-no-fast-branches
      false, // *check-global-defined/nR/fR*-no-fast-branches
      false, // *check-in-arity/n0/fR*-no-fast-branches
      false, // *check-in-arity/n1/fR*-no-fast-branches
      false, // *check-in-arity/n2/fR*-no-fast-branches
      false, // *check-in-arity/n3/fR*-no-fast-branches
      false, // *check-in-arity/n4/fR*-no-fast-branches
      false, // *check-in-arity/n5/fR*-no-fast-branches
      false, // *check-in-arity/n6/fR*-no-fast-branches
      false, // *check-in-arity/n7/fR*-no-fast-branches
      false, // *check-in-arity/n8/fR*-no-fast-branches
      false, // *check-in-arity/n9/fR*-no-fast-branches
      false, // *check-in-arity/n10/fR*-no-fast-branches
      false, // *check-in-arity/nR/fR*-no-fast-branches
      false, // *check-in-arity--alt/n0/fR*-no-fast-branches
      false, // *check-in-arity--alt/n1/fR*-no-fast-branches
      false, // *check-in-arity--alt/n2/fR*-no-fast-branches
      false, // *check-in-arity--alt/n3/fR*-no-fast-branches
      false, // *check-in-arity--alt/n4/fR*-no-fast-branches
      false, // *check-in-arity--alt/n5/fR*-no-fast-branches
      false, // *check-in-arity--alt/n6/fR*-no-fast-branches
      false, // *check-in-arity--alt/n7/fR*-no-fast-branches
      false, // *check-in-arity--alt/n8/fR*-no-fast-branches
      false, // *check-in-arity--alt/n9/fR*-no-fast-branches
      false, // *check-in-arity--alt/n10/fR*-no-fast-branches
      false, // *check-in-arity--alt/nR/fR*-no-fast-branches
      false, // *gc-if-needed/fR*-no-fast-branches
      false, // *pop-to-global/nR/fR*-no-fast-branches
      false, // *pop-to-global-defined/nR/fR*-no-fast-branches
      false, // *primitive/nR/n0/fR*-no-fast-branches
      false, // *primitive/nR/n1/fR*-no-fast-branches
      false, // *primitive/nR/n2/fR*-no-fast-branches
      false, // *primitive/nR/n3/fR*-no-fast-branches
      false, // *primitive/nR/n4/fR*-no-fast-branches
      false, // *primitive/nR/nR/fR*-no-fast-branches
      false, // *primitive-box-get/fR*-no-fast-branches
      false, // *primitive-box-setb-special/fR*-no-fast-branches
      false, // *primitive-car/fR*-no-fast-branches
      false, // *primitive-cdr/fR*-no-fast-branches
      false, // *primitive-fixnum-eqp/fR*-no-fast-branches
      false, // *primitive-fixnum-not-eqp/fR*-no-fast-branches
      false, // *primitive-greaterp/fR*-no-fast-branches
      false, // *primitive-lessp/fR*-no-fast-branches
      false, // *primitive-negate/fR*-no-fast-branches
      false, // *primitive-negativep/fR*-no-fast-branches
      false, // *primitive-non-negativep/fR*-no-fast-branches
      false, // *primitive-non-positivep/fR*-no-fast-branches
      false, // *primitive-non-zerop/fR*-no-fast-branches
      false, // *primitive-not-greaterp/fR*-no-fast-branches
      false, // *primitive-not-lessp/fR*-no-fast-branches
      false, // *primitive-one-minus/fR*-no-fast-branches
      false, // *primitive-one-plus/fR*-no-fast-branches
      false, // *primitive-positivep/fR*-no-fast-branches
      false, // *primitive-primordial-divided/fR*-no-fast-branches
      false, // *primitive-primordial-divided-unsafe/fR*-no-fast-branches
      false, // *primitive-primordial-minus/fR*-no-fast-branches
      false, // *primitive-primordial-plus/fR*-no-fast-branches
      false, // *primitive-primordial-times/fR*-no-fast-branches
      false, // *primitive-quotient/fR*-no-fast-branches
      false, // *primitive-quotient-unsafe/fR*-no-fast-branches
      false, // *primitive-remainder/fR*-no-fast-branches
      false, // *primitive-remainder-unsafe/fR*-no-fast-branches
      false, // *primitive-set-carb-special/fR*-no-fast-branches
      false, // *primitive-set-cdrb-special/fR*-no-fast-branches
      false, // *primitive-two-divided/fR*-no-fast-branches
      false, // *primitive-two-quotient/fR*-no-fast-branches
      false, // *primitive-two-remainder/fR*-no-fast-branches
      false, // *primitive-two-times/fR*-no-fast-branches
      false, // *primitive-zerop/fR*-no-fast-branches
      false // *push-global/nR/fR*-no-fast-branches
    };

/* An array whose indices are specialised instruction opcodes, and
   whose elements are the corresponding unspecialised instructions
   opcodes -- or -1 when there is no mapping mapping having */
const int
jitterlispvm_specialized_instruction_to_unspecialized_instruction
   [JITTERLISPVM_SPECIALIZED_INSTRUCTION_NO]
  = {
    -1, /* !INVALID */
    -1, /* !BEGINBASICBLOCK */
    -1, /* !EXITVM */
    -1, /* !DATALOCATIONS */
    -1, /* !NOP */
    -1, /* !UNREACHABLE0 */
    -1, /* !UNREACHABLE1 */
    -1, /* !UNREACHABLE2 */
    jitterlispvm_meta_instruction_id_at_mdepth_mto_mregister, /* at-depth-to-register/n1/%rR */
    jitterlispvm_meta_instruction_id_at_mdepth_mto_mregister, /* at-depth-to-register/n2/%rR */
    jitterlispvm_meta_instruction_id_at_mdepth_mto_mregister, /* at-depth-to-register/n3/%rR */
    jitterlispvm_meta_instruction_id_at_mdepth_mto_mregister, /* at-depth-to-register/n4/%rR */
    jitterlispvm_meta_instruction_id_at_mdepth_mto_mregister, /* at-depth-to-register/n5/%rR */
    jitterlispvm_meta_instruction_id_at_mdepth_mto_mregister, /* at-depth-to-register/n6/%rR */
    jitterlispvm_meta_instruction_id_at_mdepth_mto_mregister, /* at-depth-to-register/n7/%rR */
    jitterlispvm_meta_instruction_id_at_mdepth_mto_mregister, /* at-depth-to-register/n8/%rR */
    jitterlispvm_meta_instruction_id_at_mdepth_mto_mregister, /* at-depth-to-register/n9/%rR */
    jitterlispvm_meta_instruction_id_at_mdepth_mto_mregister, /* at-depth-to-register/n10/%rR */
    jitterlispvm_meta_instruction_id_at_mdepth_mto_mregister, /* at-depth-to-register/nR/%rR */
    jitterlispvm_meta_instruction_id_branch, /* branch/fR */
    jitterlispvm_meta_instruction_id_branch_mif_mfalse, /* branch-if-false/fR */
    jitterlispvm_meta_instruction_id_branch_mif_mnot_mless, /* branch-if-not-less/fR/fR */
    jitterlispvm_meta_instruction_id_branch_mif_mnot_mnull, /* branch-if-not-null/fR */
    jitterlispvm_meta_instruction_id_branch_mif_mnull, /* branch-if-null/fR */
    jitterlispvm_meta_instruction_id_branch_mif_mregister_mnon_mzero, /* branch-if-register-non-zero/%rR/fR/fR */
    jitterlispvm_meta_instruction_id_branch_mif_mtrue, /* branch-if-true/fR */
    jitterlispvm_meta_instruction_id_call, /* call/n0/retR */
    jitterlispvm_meta_instruction_id_call, /* call/n1/retR */
    jitterlispvm_meta_instruction_id_call, /* call/n2/retR */
    jitterlispvm_meta_instruction_id_call, /* call/n3/retR */
    jitterlispvm_meta_instruction_id_call, /* call/n4/retR */
    jitterlispvm_meta_instruction_id_call, /* call/n5/retR */
    jitterlispvm_meta_instruction_id_call, /* call/n6/retR */
    jitterlispvm_meta_instruction_id_call, /* call/n7/retR */
    jitterlispvm_meta_instruction_id_call, /* call/n8/retR */
    jitterlispvm_meta_instruction_id_call, /* call/n9/retR */
    jitterlispvm_meta_instruction_id_call, /* call/n10/retR */
    jitterlispvm_meta_instruction_id_call, /* call/nR/retR */
    jitterlispvm_meta_instruction_id_call_mcompiled, /* call-compiled/n0/retR */
    jitterlispvm_meta_instruction_id_call_mcompiled, /* call-compiled/n1/retR */
    jitterlispvm_meta_instruction_id_call_mcompiled, /* call-compiled/n2/retR */
    jitterlispvm_meta_instruction_id_call_mcompiled, /* call-compiled/n3/retR */
    jitterlispvm_meta_instruction_id_call_mcompiled, /* call-compiled/n4/retR */
    jitterlispvm_meta_instruction_id_call_mcompiled, /* call-compiled/n5/retR */
    jitterlispvm_meta_instruction_id_call_mcompiled, /* call-compiled/n6/retR */
    jitterlispvm_meta_instruction_id_call_mcompiled, /* call-compiled/n7/retR */
    jitterlispvm_meta_instruction_id_call_mcompiled, /* call-compiled/n8/retR */
    jitterlispvm_meta_instruction_id_call_mcompiled, /* call-compiled/n9/retR */
    jitterlispvm_meta_instruction_id_call_mcompiled, /* call-compiled/n10/retR */
    jitterlispvm_meta_instruction_id_call_mcompiled, /* call-compiled/nR/retR */
    jitterlispvm_meta_instruction_id_call_mfrom_mc, /* call-from-c/retR */
    jitterlispvm_meta_instruction_id_canonicalize_mboolean, /* canonicalize-boolean */
    jitterlispvm_meta_instruction_id_check_mclosure, /* check-closure/fR */
    jitterlispvm_meta_instruction_id_check_mglobal_mdefined, /* check-global-defined/nR/fR */
    jitterlispvm_meta_instruction_id_check_min_marity, /* check-in-arity/n0/fR */
    jitterlispvm_meta_instruction_id_check_min_marity, /* check-in-arity/n1/fR */
    jitterlispvm_meta_instruction_id_check_min_marity, /* check-in-arity/n2/fR */
    jitterlispvm_meta_instruction_id_check_min_marity, /* check-in-arity/n3/fR */
    jitterlispvm_meta_instruction_id_check_min_marity, /* check-in-arity/n4/fR */
    jitterlispvm_meta_instruction_id_check_min_marity, /* check-in-arity/n5/fR */
    jitterlispvm_meta_instruction_id_check_min_marity, /* check-in-arity/n6/fR */
    jitterlispvm_meta_instruction_id_check_min_marity, /* check-in-arity/n7/fR */
    jitterlispvm_meta_instruction_id_check_min_marity, /* check-in-arity/n8/fR */
    jitterlispvm_meta_instruction_id_check_min_marity, /* check-in-arity/n9/fR */
    jitterlispvm_meta_instruction_id_check_min_marity, /* check-in-arity/n10/fR */
    jitterlispvm_meta_instruction_id_check_min_marity, /* check-in-arity/nR/fR */
    jitterlispvm_meta_instruction_id_check_min_marity_m_malt, /* check-in-arity--alt/n0/fR */
    jitterlispvm_meta_instruction_id_check_min_marity_m_malt, /* check-in-arity--alt/n1/fR */
    jitterlispvm_meta_instruction_id_check_min_marity_m_malt, /* check-in-arity--alt/n2/fR */
    jitterlispvm_meta_instruction_id_check_min_marity_m_malt, /* check-in-arity--alt/n3/fR */
    jitterlispvm_meta_instruction_id_check_min_marity_m_malt, /* check-in-arity--alt/n4/fR */
    jitterlispvm_meta_instruction_id_check_min_marity_m_malt, /* check-in-arity--alt/n5/fR */
    jitterlispvm_meta_instruction_id_check_min_marity_m_malt, /* check-in-arity--alt/n6/fR */
    jitterlispvm_meta_instruction_id_check_min_marity_m_malt, /* check-in-arity--alt/n7/fR */
    jitterlispvm_meta_instruction_id_check_min_marity_m_malt, /* check-in-arity--alt/n8/fR */
    jitterlispvm_meta_instruction_id_check_min_marity_m_malt, /* check-in-arity--alt/n9/fR */
    jitterlispvm_meta_instruction_id_check_min_marity_m_malt, /* check-in-arity--alt/n10/fR */
    jitterlispvm_meta_instruction_id_check_min_marity_m_malt, /* check-in-arity--alt/nR/fR */
    jitterlispvm_meta_instruction_id_copy_mfrom_mliteral, /* copy-from-literal/nR */
    jitterlispvm_meta_instruction_id_copy_mfrom_mregister, /* copy-from-register/%rR */
    jitterlispvm_meta_instruction_id_copy_mto_mregister, /* copy-to-register/%rR */
    jitterlispvm_meta_instruction_id_drop, /* drop */
    jitterlispvm_meta_instruction_id_drop_mnip, /* drop-nip */
    jitterlispvm_meta_instruction_id_dup, /* dup */
    jitterlispvm_meta_instruction_id_exitvm, /* exitvm */
    jitterlispvm_meta_instruction_id_fail, /* fail/retR */
    jitterlispvm_meta_instruction_id_gc_mif_mneeded, /* gc-if-needed/fR */
    jitterlispvm_meta_instruction_id_heap_mallocate, /* heap-allocate/n4 */
    jitterlispvm_meta_instruction_id_heap_mallocate, /* heap-allocate/n8 */
    jitterlispvm_meta_instruction_id_heap_mallocate, /* heap-allocate/n12 */
    jitterlispvm_meta_instruction_id_heap_mallocate, /* heap-allocate/n16 */
    jitterlispvm_meta_instruction_id_heap_mallocate, /* heap-allocate/n24 */
    jitterlispvm_meta_instruction_id_heap_mallocate, /* heap-allocate/n32 */
    jitterlispvm_meta_instruction_id_heap_mallocate, /* heap-allocate/n36 */
    jitterlispvm_meta_instruction_id_heap_mallocate, /* heap-allocate/n48 */
    jitterlispvm_meta_instruction_id_heap_mallocate, /* heap-allocate/n52 */
    jitterlispvm_meta_instruction_id_heap_mallocate, /* heap-allocate/n64 */
    jitterlispvm_meta_instruction_id_heap_mallocate, /* heap-allocate/nR */
    jitterlispvm_meta_instruction_id_literal_mto_mregister, /* literal-to-register/nR/%rR */
    jitterlispvm_meta_instruction_id_nip, /* nip */
    jitterlispvm_meta_instruction_id_nip_mdrop, /* nip-drop */
    jitterlispvm_meta_instruction_id_nip_mfive, /* nip-five */
    jitterlispvm_meta_instruction_id_nip_mfive_mdrop, /* nip-five-drop */
    jitterlispvm_meta_instruction_id_nip_mfour, /* nip-four */
    jitterlispvm_meta_instruction_id_nip_mfour_mdrop, /* nip-four-drop */
    jitterlispvm_meta_instruction_id_nip_mpush_mliteral, /* nip-push-literal/nR */
    jitterlispvm_meta_instruction_id_nip_mpush_mregister, /* nip-push-register/%rR */
    jitterlispvm_meta_instruction_id_nip_msix, /* nip-six */
    jitterlispvm_meta_instruction_id_nip_msix_mdrop, /* nip-six-drop */
    jitterlispvm_meta_instruction_id_nip_mthree, /* nip-three */
    jitterlispvm_meta_instruction_id_nip_mthree_mdrop, /* nip-three-drop */
    jitterlispvm_meta_instruction_id_nip_mtwo, /* nip-two */
    jitterlispvm_meta_instruction_id_nip_mtwo_mdrop, /* nip-two-drop */
    jitterlispvm_meta_instruction_id_nop, /* nop */
    jitterlispvm_meta_instruction_id_pop_mto_mglobal, /* pop-to-global/nR/fR */
    jitterlispvm_meta_instruction_id_pop_mto_mglobal_mdefined, /* pop-to-global-defined/nR/fR */
    jitterlispvm_meta_instruction_id_pop_mto_mregister, /* pop-to-register/%rR */
    jitterlispvm_meta_instruction_id_primitive, /* primitive/nR/n0/fR */
    jitterlispvm_meta_instruction_id_primitive, /* primitive/nR/n1/fR */
    jitterlispvm_meta_instruction_id_primitive, /* primitive/nR/n2/fR */
    jitterlispvm_meta_instruction_id_primitive, /* primitive/nR/n3/fR */
    jitterlispvm_meta_instruction_id_primitive, /* primitive/nR/n4/fR */
    jitterlispvm_meta_instruction_id_primitive, /* primitive/nR/nR/fR */
    jitterlispvm_meta_instruction_id_primitive_mboolean_mcanonicalize, /* primitive-boolean-canonicalize */
    jitterlispvm_meta_instruction_id_primitive_mbox, /* primitive-box */
    jitterlispvm_meta_instruction_id_primitive_mbox_mget, /* primitive-box-get/fR */
    jitterlispvm_meta_instruction_id_primitive_mbox_msetb_mspecial, /* primitive-box-setb-special/fR */
    jitterlispvm_meta_instruction_id_primitive_mcar, /* primitive-car/fR */
    jitterlispvm_meta_instruction_id_primitive_mcdr, /* primitive-cdr/fR */
    jitterlispvm_meta_instruction_id_primitive_mcharacterp, /* primitive-characterp */
    jitterlispvm_meta_instruction_id_primitive_mcons_mspecial, /* primitive-cons-special */
    jitterlispvm_meta_instruction_id_primitive_mconsp, /* primitive-consp */
    jitterlispvm_meta_instruction_id_primitive_meqp, /* primitive-eqp */
    jitterlispvm_meta_instruction_id_primitive_mfixnum_meqp, /* primitive-fixnum-eqp/fR */
    jitterlispvm_meta_instruction_id_primitive_mfixnum_mnot_meqp, /* primitive-fixnum-not-eqp/fR */
    jitterlispvm_meta_instruction_id_primitive_mfixnump, /* primitive-fixnump */
    jitterlispvm_meta_instruction_id_primitive_mgreaterp, /* primitive-greaterp/fR */
    jitterlispvm_meta_instruction_id_primitive_mlessp, /* primitive-lessp/fR */
    jitterlispvm_meta_instruction_id_primitive_mnegate, /* primitive-negate/fR */
    jitterlispvm_meta_instruction_id_primitive_mnegativep, /* primitive-negativep/fR */
    jitterlispvm_meta_instruction_id_primitive_mnon_mconsp, /* primitive-non-consp */
    jitterlispvm_meta_instruction_id_primitive_mnon_mnegativep, /* primitive-non-negativep/fR */
    jitterlispvm_meta_instruction_id_primitive_mnon_mnullp, /* primitive-non-nullp */
    jitterlispvm_meta_instruction_id_primitive_mnon_mpositivep, /* primitive-non-positivep/fR */
    jitterlispvm_meta_instruction_id_primitive_mnon_msymbolp, /* primitive-non-symbolp */
    jitterlispvm_meta_instruction_id_primitive_mnon_mzerop, /* primitive-non-zerop/fR */
    jitterlispvm_meta_instruction_id_primitive_mnot, /* primitive-not */
    jitterlispvm_meta_instruction_id_primitive_mnot_meqp, /* primitive-not-eqp */
    jitterlispvm_meta_instruction_id_primitive_mnot_mgreaterp, /* primitive-not-greaterp/fR */
    jitterlispvm_meta_instruction_id_primitive_mnot_mlessp, /* primitive-not-lessp/fR */
    jitterlispvm_meta_instruction_id_primitive_mnothingp, /* primitive-nothingp */
    jitterlispvm_meta_instruction_id_primitive_mnullp, /* primitive-nullp */
    jitterlispvm_meta_instruction_id_primitive_mone_mminus, /* primitive-one-minus/fR */
    jitterlispvm_meta_instruction_id_primitive_mone_mplus, /* primitive-one-plus/fR */
    jitterlispvm_meta_instruction_id_primitive_mpositivep, /* primitive-positivep/fR */
    jitterlispvm_meta_instruction_id_primitive_mprimordial_mdivided, /* primitive-primordial-divided/fR */
    jitterlispvm_meta_instruction_id_primitive_mprimordial_mdivided_munsafe, /* primitive-primordial-divided-unsafe/fR */
    jitterlispvm_meta_instruction_id_primitive_mprimordial_mminus, /* primitive-primordial-minus/fR */
    jitterlispvm_meta_instruction_id_primitive_mprimordial_mplus, /* primitive-primordial-plus/fR */
    jitterlispvm_meta_instruction_id_primitive_mprimordial_mtimes, /* primitive-primordial-times/fR */
    jitterlispvm_meta_instruction_id_primitive_mquotient, /* primitive-quotient/fR */
    jitterlispvm_meta_instruction_id_primitive_mquotient_munsafe, /* primitive-quotient-unsafe/fR */
    jitterlispvm_meta_instruction_id_primitive_mremainder, /* primitive-remainder/fR */
    jitterlispvm_meta_instruction_id_primitive_mremainder_munsafe, /* primitive-remainder-unsafe/fR */
    jitterlispvm_meta_instruction_id_primitive_mset_mcarb_mspecial, /* primitive-set-carb-special/fR */
    jitterlispvm_meta_instruction_id_primitive_mset_mcdrb_mspecial, /* primitive-set-cdrb-special/fR */
    jitterlispvm_meta_instruction_id_primitive_msymbolp, /* primitive-symbolp */
    jitterlispvm_meta_instruction_id_primitive_mtwo_mdivided, /* primitive-two-divided/fR */
    jitterlispvm_meta_instruction_id_primitive_mtwo_mquotient, /* primitive-two-quotient/fR */
    jitterlispvm_meta_instruction_id_primitive_mtwo_mremainder, /* primitive-two-remainder/fR */
    jitterlispvm_meta_instruction_id_primitive_mtwo_mtimes, /* primitive-two-times/fR */
    jitterlispvm_meta_instruction_id_primitive_muniquep, /* primitive-uniquep */
    jitterlispvm_meta_instruction_id_primitive_mzerop, /* primitive-zerop/fR */
    jitterlispvm_meta_instruction_id_procedure_mprolog, /* procedure-prolog */
    jitterlispvm_meta_instruction_id_push_mfalse, /* push-false */
    jitterlispvm_meta_instruction_id_push_mglobal, /* push-global/nR/fR */
    jitterlispvm_meta_instruction_id_push_mliteral, /* push-literal/nR */
    jitterlispvm_meta_instruction_id_push_mnil, /* push-nil */
    jitterlispvm_meta_instruction_id_push_mnothing, /* push-nothing */
    jitterlispvm_meta_instruction_id_push_mone, /* push-one */
    jitterlispvm_meta_instruction_id_push_mregister, /* push-register/%rR */
    jitterlispvm_meta_instruction_id_push_munspecified, /* push-unspecified */
    jitterlispvm_meta_instruction_id_push_mzero, /* push-zero */
    jitterlispvm_meta_instruction_id_register_mto_mregister, /* register-to-register/%rR/%rR */
    jitterlispvm_meta_instruction_id_restore_mregister, /* restore-register/%rR */
    jitterlispvm_meta_instruction_id_return, /* return */
    jitterlispvm_meta_instruction_id_save_mregister, /* save-register/%rR */
    jitterlispvm_meta_instruction_id_tail_mcall, /* tail-call/n0 */
    jitterlispvm_meta_instruction_id_tail_mcall, /* tail-call/n1 */
    jitterlispvm_meta_instruction_id_tail_mcall, /* tail-call/n2 */
    jitterlispvm_meta_instruction_id_tail_mcall, /* tail-call/n3 */
    jitterlispvm_meta_instruction_id_tail_mcall, /* tail-call/n4 */
    jitterlispvm_meta_instruction_id_tail_mcall, /* tail-call/n5 */
    jitterlispvm_meta_instruction_id_tail_mcall, /* tail-call/n6 */
    jitterlispvm_meta_instruction_id_tail_mcall, /* tail-call/n7 */
    jitterlispvm_meta_instruction_id_tail_mcall, /* tail-call/n8 */
    jitterlispvm_meta_instruction_id_tail_mcall, /* tail-call/n9 */
    jitterlispvm_meta_instruction_id_tail_mcall, /* tail-call/n10 */
    jitterlispvm_meta_instruction_id_tail_mcall, /* tail-call/nR */
    jitterlispvm_meta_instruction_id_tail_mcall_mcompiled, /* tail-call-compiled/n0 */
    jitterlispvm_meta_instruction_id_tail_mcall_mcompiled, /* tail-call-compiled/n1 */
    jitterlispvm_meta_instruction_id_tail_mcall_mcompiled, /* tail-call-compiled/n2 */
    jitterlispvm_meta_instruction_id_tail_mcall_mcompiled, /* tail-call-compiled/n3 */
    jitterlispvm_meta_instruction_id_tail_mcall_mcompiled, /* tail-call-compiled/n4 */
    jitterlispvm_meta_instruction_id_tail_mcall_mcompiled, /* tail-call-compiled/n5 */
    jitterlispvm_meta_instruction_id_tail_mcall_mcompiled, /* tail-call-compiled/n6 */
    jitterlispvm_meta_instruction_id_tail_mcall_mcompiled, /* tail-call-compiled/n7 */
    jitterlispvm_meta_instruction_id_tail_mcall_mcompiled, /* tail-call-compiled/n8 */
    jitterlispvm_meta_instruction_id_tail_mcall_mcompiled, /* tail-call-compiled/n9 */
    jitterlispvm_meta_instruction_id_tail_mcall_mcompiled, /* tail-call-compiled/n10 */
    jitterlispvm_meta_instruction_id_tail_mcall_mcompiled, /* tail-call-compiled/nR */
    jitterlispvm_meta_instruction_id_unreachable, /* unreachable */
    jitterlispvm_meta_instruction_id_branch, /* *branch/fR*-no-fast-branches */
    jitterlispvm_meta_instruction_id_branch_mif_mfalse, /* *branch-if-false/fR*-no-fast-branches */
    jitterlispvm_meta_instruction_id_branch_mif_mnot_mless, /* *branch-if-not-less/fR/fR*-no-fast-branches */
    jitterlispvm_meta_instruction_id_branch_mif_mnot_mnull, /* *branch-if-not-null/fR*-no-fast-branches */
    jitterlispvm_meta_instruction_id_branch_mif_mnull, /* *branch-if-null/fR*-no-fast-branches */
    jitterlispvm_meta_instruction_id_branch_mif_mregister_mnon_mzero, /* *branch-if-register-non-zero/%rR/fR/fR*-no-fast-branches */
    jitterlispvm_meta_instruction_id_branch_mif_mtrue, /* *branch-if-true/fR*-no-fast-branches */
    jitterlispvm_meta_instruction_id_check_mclosure, /* *check-closure/fR*-no-fast-branches */
    jitterlispvm_meta_instruction_id_check_mglobal_mdefined, /* *check-global-defined/nR/fR*-no-fast-branches */
    jitterlispvm_meta_instruction_id_check_min_marity, /* *check-in-arity/n0/fR*-no-fast-branches */
    jitterlispvm_meta_instruction_id_check_min_marity, /* *check-in-arity/n1/fR*-no-fast-branches */
    jitterlispvm_meta_instruction_id_check_min_marity, /* *check-in-arity/n2/fR*-no-fast-branches */
    jitterlispvm_meta_instruction_id_check_min_marity, /* *check-in-arity/n3/fR*-no-fast-branches */
    jitterlispvm_meta_instruction_id_check_min_marity, /* *check-in-arity/n4/fR*-no-fast-branches */
    jitterlispvm_meta_instruction_id_check_min_marity, /* *check-in-arity/n5/fR*-no-fast-branches */
    jitterlispvm_meta_instruction_id_check_min_marity, /* *check-in-arity/n6/fR*-no-fast-branches */
    jitterlispvm_meta_instruction_id_check_min_marity, /* *check-in-arity/n7/fR*-no-fast-branches */
    jitterlispvm_meta_instruction_id_check_min_marity, /* *check-in-arity/n8/fR*-no-fast-branches */
    jitterlispvm_meta_instruction_id_check_min_marity, /* *check-in-arity/n9/fR*-no-fast-branches */
    jitterlispvm_meta_instruction_id_check_min_marity, /* *check-in-arity/n10/fR*-no-fast-branches */
    jitterlispvm_meta_instruction_id_check_min_marity, /* *check-in-arity/nR/fR*-no-fast-branches */
    jitterlispvm_meta_instruction_id_check_min_marity_m_malt, /* *check-in-arity--alt/n0/fR*-no-fast-branches */
    jitterlispvm_meta_instruction_id_check_min_marity_m_malt, /* *check-in-arity--alt/n1/fR*-no-fast-branches */
    jitterlispvm_meta_instruction_id_check_min_marity_m_malt, /* *check-in-arity--alt/n2/fR*-no-fast-branches */
    jitterlispvm_meta_instruction_id_check_min_marity_m_malt, /* *check-in-arity--alt/n3/fR*-no-fast-branches */
    jitterlispvm_meta_instruction_id_check_min_marity_m_malt, /* *check-in-arity--alt/n4/fR*-no-fast-branches */
    jitterlispvm_meta_instruction_id_check_min_marity_m_malt, /* *check-in-arity--alt/n5/fR*-no-fast-branches */
    jitterlispvm_meta_instruction_id_check_min_marity_m_malt, /* *check-in-arity--alt/n6/fR*-no-fast-branches */
    jitterlispvm_meta_instruction_id_check_min_marity_m_malt, /* *check-in-arity--alt/n7/fR*-no-fast-branches */
    jitterlispvm_meta_instruction_id_check_min_marity_m_malt, /* *check-in-arity--alt/n8/fR*-no-fast-branches */
    jitterlispvm_meta_instruction_id_check_min_marity_m_malt, /* *check-in-arity--alt/n9/fR*-no-fast-branches */
    jitterlispvm_meta_instruction_id_check_min_marity_m_malt, /* *check-in-arity--alt/n10/fR*-no-fast-branches */
    jitterlispvm_meta_instruction_id_check_min_marity_m_malt, /* *check-in-arity--alt/nR/fR*-no-fast-branches */
    jitterlispvm_meta_instruction_id_gc_mif_mneeded, /* *gc-if-needed/fR*-no-fast-branches */
    jitterlispvm_meta_instruction_id_pop_mto_mglobal, /* *pop-to-global/nR/fR*-no-fast-branches */
    jitterlispvm_meta_instruction_id_pop_mto_mglobal_mdefined, /* *pop-to-global-defined/nR/fR*-no-fast-branches */
    jitterlispvm_meta_instruction_id_primitive, /* *primitive/nR/n0/fR*-no-fast-branches */
    jitterlispvm_meta_instruction_id_primitive, /* *primitive/nR/n1/fR*-no-fast-branches */
    jitterlispvm_meta_instruction_id_primitive, /* *primitive/nR/n2/fR*-no-fast-branches */
    jitterlispvm_meta_instruction_id_primitive, /* *primitive/nR/n3/fR*-no-fast-branches */
    jitterlispvm_meta_instruction_id_primitive, /* *primitive/nR/n4/fR*-no-fast-branches */
    jitterlispvm_meta_instruction_id_primitive, /* *primitive/nR/nR/fR*-no-fast-branches */
    jitterlispvm_meta_instruction_id_primitive_mbox_mget, /* *primitive-box-get/fR*-no-fast-branches */
    jitterlispvm_meta_instruction_id_primitive_mbox_msetb_mspecial, /* *primitive-box-setb-special/fR*-no-fast-branches */
    jitterlispvm_meta_instruction_id_primitive_mcar, /* *primitive-car/fR*-no-fast-branches */
    jitterlispvm_meta_instruction_id_primitive_mcdr, /* *primitive-cdr/fR*-no-fast-branches */
    jitterlispvm_meta_instruction_id_primitive_mfixnum_meqp, /* *primitive-fixnum-eqp/fR*-no-fast-branches */
    jitterlispvm_meta_instruction_id_primitive_mfixnum_mnot_meqp, /* *primitive-fixnum-not-eqp/fR*-no-fast-branches */
    jitterlispvm_meta_instruction_id_primitive_mgreaterp, /* *primitive-greaterp/fR*-no-fast-branches */
    jitterlispvm_meta_instruction_id_primitive_mlessp, /* *primitive-lessp/fR*-no-fast-branches */
    jitterlispvm_meta_instruction_id_primitive_mnegate, /* *primitive-negate/fR*-no-fast-branches */
    jitterlispvm_meta_instruction_id_primitive_mnegativep, /* *primitive-negativep/fR*-no-fast-branches */
    jitterlispvm_meta_instruction_id_primitive_mnon_mnegativep, /* *primitive-non-negativep/fR*-no-fast-branches */
    jitterlispvm_meta_instruction_id_primitive_mnon_mpositivep, /* *primitive-non-positivep/fR*-no-fast-branches */
    jitterlispvm_meta_instruction_id_primitive_mnon_mzerop, /* *primitive-non-zerop/fR*-no-fast-branches */
    jitterlispvm_meta_instruction_id_primitive_mnot_mgreaterp, /* *primitive-not-greaterp/fR*-no-fast-branches */
    jitterlispvm_meta_instruction_id_primitive_mnot_mlessp, /* *primitive-not-lessp/fR*-no-fast-branches */
    jitterlispvm_meta_instruction_id_primitive_mone_mminus, /* *primitive-one-minus/fR*-no-fast-branches */
    jitterlispvm_meta_instruction_id_primitive_mone_mplus, /* *primitive-one-plus/fR*-no-fast-branches */
    jitterlispvm_meta_instruction_id_primitive_mpositivep, /* *primitive-positivep/fR*-no-fast-branches */
    jitterlispvm_meta_instruction_id_primitive_mprimordial_mdivided, /* *primitive-primordial-divided/fR*-no-fast-branches */
    jitterlispvm_meta_instruction_id_primitive_mprimordial_mdivided_munsafe, /* *primitive-primordial-divided-unsafe/fR*-no-fast-branches */
    jitterlispvm_meta_instruction_id_primitive_mprimordial_mminus, /* *primitive-primordial-minus/fR*-no-fast-branches */
    jitterlispvm_meta_instruction_id_primitive_mprimordial_mplus, /* *primitive-primordial-plus/fR*-no-fast-branches */
    jitterlispvm_meta_instruction_id_primitive_mprimordial_mtimes, /* *primitive-primordial-times/fR*-no-fast-branches */
    jitterlispvm_meta_instruction_id_primitive_mquotient, /* *primitive-quotient/fR*-no-fast-branches */
    jitterlispvm_meta_instruction_id_primitive_mquotient_munsafe, /* *primitive-quotient-unsafe/fR*-no-fast-branches */
    jitterlispvm_meta_instruction_id_primitive_mremainder, /* *primitive-remainder/fR*-no-fast-branches */
    jitterlispvm_meta_instruction_id_primitive_mremainder_munsafe, /* *primitive-remainder-unsafe/fR*-no-fast-branches */
    jitterlispvm_meta_instruction_id_primitive_mset_mcarb_mspecial, /* *primitive-set-carb-special/fR*-no-fast-branches */
    jitterlispvm_meta_instruction_id_primitive_mset_mcdrb_mspecial, /* *primitive-set-cdrb-special/fR*-no-fast-branches */
    jitterlispvm_meta_instruction_id_primitive_mtwo_mdivided, /* *primitive-two-divided/fR*-no-fast-branches */
    jitterlispvm_meta_instruction_id_primitive_mtwo_mquotient, /* *primitive-two-quotient/fR*-no-fast-branches */
    jitterlispvm_meta_instruction_id_primitive_mtwo_mremainder, /* *primitive-two-remainder/fR*-no-fast-branches */
    jitterlispvm_meta_instruction_id_primitive_mtwo_mtimes, /* *primitive-two-times/fR*-no-fast-branches */
    jitterlispvm_meta_instruction_id_primitive_mzerop, /* *primitive-zerop/fR*-no-fast-branches */
    jitterlispvm_meta_instruction_id_push_mglobal /* *push-global/nR/fR*-no-fast-branches */
    };

#ifdef JITTER_HAVE_PATCH_IN
/* Worst-case defect table. */
const jitter_uint
jitterlispvm_worst_case_defect_table [] =
  {
    jitterlispvm_specialized_instruction_opcode__eINVALID, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eBEGINBASICBLOCK, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eEXITVM, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eDATALOCATIONS, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eNOP, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eUNREACHABLE0, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eUNREACHABLE1, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eUNREACHABLE2, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_at_mdepth_mto_mregister__n1___rrR, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_at_mdepth_mto_mregister__n2___rrR, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_at_mdepth_mto_mregister__n3___rrR, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_at_mdepth_mto_mregister__n4___rrR, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_at_mdepth_mto_mregister__n5___rrR, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_at_mdepth_mto_mregister__n6___rrR, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_at_mdepth_mto_mregister__n7___rrR, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_at_mdepth_mto_mregister__n8___rrR, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_at_mdepth_mto_mregister__n9___rrR, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_at_mdepth_mto_mregister__n10___rrR, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_at_mdepth_mto_mregister__nR___rrR, /* NOT potentially defective. */
    /*jitterlispvm_specialized_instruction_opcode__eINVALID*/jitterlispvm_specialized_instruction_opcode__Abranch__fR_A_mno_mfast_mbranches, /* POTENTIALLY DEFECTIVE. */
    /*jitterlispvm_specialized_instruction_opcode__eINVALID*/jitterlispvm_specialized_instruction_opcode__Abranch_mif_mfalse__fR_A_mno_mfast_mbranches, /* POTENTIALLY DEFECTIVE. */
    /*jitterlispvm_specialized_instruction_opcode__eINVALID*/jitterlispvm_specialized_instruction_opcode__Abranch_mif_mnot_mless__fR__fR_A_mno_mfast_mbranches, /* POTENTIALLY DEFECTIVE. */
    /*jitterlispvm_specialized_instruction_opcode__eINVALID*/jitterlispvm_specialized_instruction_opcode__Abranch_mif_mnot_mnull__fR_A_mno_mfast_mbranches, /* POTENTIALLY DEFECTIVE. */
    /*jitterlispvm_specialized_instruction_opcode__eINVALID*/jitterlispvm_specialized_instruction_opcode__Abranch_mif_mnull__fR_A_mno_mfast_mbranches, /* POTENTIALLY DEFECTIVE. */
    /*jitterlispvm_specialized_instruction_opcode__eINVALID*/jitterlispvm_specialized_instruction_opcode__Abranch_mif_mregister_mnon_mzero___rrR__fR__fR_A_mno_mfast_mbranches, /* POTENTIALLY DEFECTIVE. */
    /*jitterlispvm_specialized_instruction_opcode__eINVALID*/jitterlispvm_specialized_instruction_opcode__Abranch_mif_mtrue__fR_A_mno_mfast_mbranches, /* POTENTIALLY DEFECTIVE. */
    jitterlispvm_specialized_instruction_opcode_call__n0__retR, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_call__n1__retR, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_call__n2__retR, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_call__n3__retR, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_call__n4__retR, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_call__n5__retR, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_call__n6__retR, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_call__n7__retR, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_call__n8__retR, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_call__n9__retR, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_call__n10__retR, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_call__nR__retR, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_call_mcompiled__n0__retR, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_call_mcompiled__n1__retR, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_call_mcompiled__n2__retR, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_call_mcompiled__n3__retR, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_call_mcompiled__n4__retR, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_call_mcompiled__n5__retR, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_call_mcompiled__n6__retR, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_call_mcompiled__n7__retR, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_call_mcompiled__n8__retR, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_call_mcompiled__n9__retR, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_call_mcompiled__n10__retR, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_call_mcompiled__nR__retR, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_call_mfrom_mc__retR, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_canonicalize_mboolean, /* NOT potentially defective. */
    /*jitterlispvm_specialized_instruction_opcode__eINVALID*/jitterlispvm_specialized_instruction_opcode__Acheck_mclosure__fR_A_mno_mfast_mbranches, /* POTENTIALLY DEFECTIVE. */
    /*jitterlispvm_specialized_instruction_opcode__eINVALID*/jitterlispvm_specialized_instruction_opcode__Acheck_mglobal_mdefined__nR__fR_A_mno_mfast_mbranches, /* POTENTIALLY DEFECTIVE. */
    /*jitterlispvm_specialized_instruction_opcode__eINVALID*/jitterlispvm_specialized_instruction_opcode__Acheck_min_marity__n0__fR_A_mno_mfast_mbranches, /* POTENTIALLY DEFECTIVE. */
    /*jitterlispvm_specialized_instruction_opcode__eINVALID*/jitterlispvm_specialized_instruction_opcode__Acheck_min_marity__n1__fR_A_mno_mfast_mbranches, /* POTENTIALLY DEFECTIVE. */
    /*jitterlispvm_specialized_instruction_opcode__eINVALID*/jitterlispvm_specialized_instruction_opcode__Acheck_min_marity__n2__fR_A_mno_mfast_mbranches, /* POTENTIALLY DEFECTIVE. */
    /*jitterlispvm_specialized_instruction_opcode__eINVALID*/jitterlispvm_specialized_instruction_opcode__Acheck_min_marity__n3__fR_A_mno_mfast_mbranches, /* POTENTIALLY DEFECTIVE. */
    /*jitterlispvm_specialized_instruction_opcode__eINVALID*/jitterlispvm_specialized_instruction_opcode__Acheck_min_marity__n4__fR_A_mno_mfast_mbranches, /* POTENTIALLY DEFECTIVE. */
    /*jitterlispvm_specialized_instruction_opcode__eINVALID*/jitterlispvm_specialized_instruction_opcode__Acheck_min_marity__n5__fR_A_mno_mfast_mbranches, /* POTENTIALLY DEFECTIVE. */
    /*jitterlispvm_specialized_instruction_opcode__eINVALID*/jitterlispvm_specialized_instruction_opcode__Acheck_min_marity__n6__fR_A_mno_mfast_mbranches, /* POTENTIALLY DEFECTIVE. */
    /*jitterlispvm_specialized_instruction_opcode__eINVALID*/jitterlispvm_specialized_instruction_opcode__Acheck_min_marity__n7__fR_A_mno_mfast_mbranches, /* POTENTIALLY DEFECTIVE. */
    /*jitterlispvm_specialized_instruction_opcode__eINVALID*/jitterlispvm_specialized_instruction_opcode__Acheck_min_marity__n8__fR_A_mno_mfast_mbranches, /* POTENTIALLY DEFECTIVE. */
    /*jitterlispvm_specialized_instruction_opcode__eINVALID*/jitterlispvm_specialized_instruction_opcode__Acheck_min_marity__n9__fR_A_mno_mfast_mbranches, /* POTENTIALLY DEFECTIVE. */
    /*jitterlispvm_specialized_instruction_opcode__eINVALID*/jitterlispvm_specialized_instruction_opcode__Acheck_min_marity__n10__fR_A_mno_mfast_mbranches, /* POTENTIALLY DEFECTIVE. */
    /*jitterlispvm_specialized_instruction_opcode__eINVALID*/jitterlispvm_specialized_instruction_opcode__Acheck_min_marity__nR__fR_A_mno_mfast_mbranches, /* POTENTIALLY DEFECTIVE. */
    /*jitterlispvm_specialized_instruction_opcode__eINVALID*/jitterlispvm_specialized_instruction_opcode__Acheck_min_marity_m_malt__n0__fR_A_mno_mfast_mbranches, /* POTENTIALLY DEFECTIVE. */
    /*jitterlispvm_specialized_instruction_opcode__eINVALID*/jitterlispvm_specialized_instruction_opcode__Acheck_min_marity_m_malt__n1__fR_A_mno_mfast_mbranches, /* POTENTIALLY DEFECTIVE. */
    /*jitterlispvm_specialized_instruction_opcode__eINVALID*/jitterlispvm_specialized_instruction_opcode__Acheck_min_marity_m_malt__n2__fR_A_mno_mfast_mbranches, /* POTENTIALLY DEFECTIVE. */
    /*jitterlispvm_specialized_instruction_opcode__eINVALID*/jitterlispvm_specialized_instruction_opcode__Acheck_min_marity_m_malt__n3__fR_A_mno_mfast_mbranches, /* POTENTIALLY DEFECTIVE. */
    /*jitterlispvm_specialized_instruction_opcode__eINVALID*/jitterlispvm_specialized_instruction_opcode__Acheck_min_marity_m_malt__n4__fR_A_mno_mfast_mbranches, /* POTENTIALLY DEFECTIVE. */
    /*jitterlispvm_specialized_instruction_opcode__eINVALID*/jitterlispvm_specialized_instruction_opcode__Acheck_min_marity_m_malt__n5__fR_A_mno_mfast_mbranches, /* POTENTIALLY DEFECTIVE. */
    /*jitterlispvm_specialized_instruction_opcode__eINVALID*/jitterlispvm_specialized_instruction_opcode__Acheck_min_marity_m_malt__n6__fR_A_mno_mfast_mbranches, /* POTENTIALLY DEFECTIVE. */
    /*jitterlispvm_specialized_instruction_opcode__eINVALID*/jitterlispvm_specialized_instruction_opcode__Acheck_min_marity_m_malt__n7__fR_A_mno_mfast_mbranches, /* POTENTIALLY DEFECTIVE. */
    /*jitterlispvm_specialized_instruction_opcode__eINVALID*/jitterlispvm_specialized_instruction_opcode__Acheck_min_marity_m_malt__n8__fR_A_mno_mfast_mbranches, /* POTENTIALLY DEFECTIVE. */
    /*jitterlispvm_specialized_instruction_opcode__eINVALID*/jitterlispvm_specialized_instruction_opcode__Acheck_min_marity_m_malt__n9__fR_A_mno_mfast_mbranches, /* POTENTIALLY DEFECTIVE. */
    /*jitterlispvm_specialized_instruction_opcode__eINVALID*/jitterlispvm_specialized_instruction_opcode__Acheck_min_marity_m_malt__n10__fR_A_mno_mfast_mbranches, /* POTENTIALLY DEFECTIVE. */
    /*jitterlispvm_specialized_instruction_opcode__eINVALID*/jitterlispvm_specialized_instruction_opcode__Acheck_min_marity_m_malt__nR__fR_A_mno_mfast_mbranches, /* POTENTIALLY DEFECTIVE. */
    jitterlispvm_specialized_instruction_opcode_copy_mfrom_mliteral__nR, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_copy_mfrom_mregister___rrR, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_copy_mto_mregister___rrR, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_drop, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_drop_mnip, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_dup, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_exitvm, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_fail__retR, /* NOT potentially defective. */
    /*jitterlispvm_specialized_instruction_opcode__eINVALID*/jitterlispvm_specialized_instruction_opcode__Agc_mif_mneeded__fR_A_mno_mfast_mbranches, /* POTENTIALLY DEFECTIVE. */
    jitterlispvm_specialized_instruction_opcode_heap_mallocate__n4, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_heap_mallocate__n8, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_heap_mallocate__n12, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_heap_mallocate__n16, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_heap_mallocate__n24, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_heap_mallocate__n32, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_heap_mallocate__n36, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_heap_mallocate__n48, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_heap_mallocate__n52, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_heap_mallocate__n64, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_heap_mallocate__nR, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_literal_mto_mregister__nR___rrR, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_nip, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_nip_mdrop, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_nip_mfive, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_nip_mfive_mdrop, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_nip_mfour, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_nip_mfour_mdrop, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_nip_mpush_mliteral__nR, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_nip_mpush_mregister___rrR, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_nip_msix, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_nip_msix_mdrop, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_nip_mthree, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_nip_mthree_mdrop, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_nip_mtwo, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_nip_mtwo_mdrop, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_nop, /* NOT potentially defective. */
    /*jitterlispvm_specialized_instruction_opcode__eINVALID*/jitterlispvm_specialized_instruction_opcode__Apop_mto_mglobal__nR__fR_A_mno_mfast_mbranches, /* POTENTIALLY DEFECTIVE. */
    /*jitterlispvm_specialized_instruction_opcode__eINVALID*/jitterlispvm_specialized_instruction_opcode__Apop_mto_mglobal_mdefined__nR__fR_A_mno_mfast_mbranches, /* POTENTIALLY DEFECTIVE. */
    jitterlispvm_specialized_instruction_opcode_pop_mto_mregister___rrR, /* NOT potentially defective. */
    /*jitterlispvm_specialized_instruction_opcode__eINVALID*/jitterlispvm_specialized_instruction_opcode__Aprimitive__nR__n0__fR_A_mno_mfast_mbranches, /* POTENTIALLY DEFECTIVE. */
    /*jitterlispvm_specialized_instruction_opcode__eINVALID*/jitterlispvm_specialized_instruction_opcode__Aprimitive__nR__n1__fR_A_mno_mfast_mbranches, /* POTENTIALLY DEFECTIVE. */
    /*jitterlispvm_specialized_instruction_opcode__eINVALID*/jitterlispvm_specialized_instruction_opcode__Aprimitive__nR__n2__fR_A_mno_mfast_mbranches, /* POTENTIALLY DEFECTIVE. */
    /*jitterlispvm_specialized_instruction_opcode__eINVALID*/jitterlispvm_specialized_instruction_opcode__Aprimitive__nR__n3__fR_A_mno_mfast_mbranches, /* POTENTIALLY DEFECTIVE. */
    /*jitterlispvm_specialized_instruction_opcode__eINVALID*/jitterlispvm_specialized_instruction_opcode__Aprimitive__nR__n4__fR_A_mno_mfast_mbranches, /* POTENTIALLY DEFECTIVE. */
    /*jitterlispvm_specialized_instruction_opcode__eINVALID*/jitterlispvm_specialized_instruction_opcode__Aprimitive__nR__nR__fR_A_mno_mfast_mbranches, /* POTENTIALLY DEFECTIVE. */
    jitterlispvm_specialized_instruction_opcode_primitive_mboolean_mcanonicalize, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_primitive_mbox, /* NOT potentially defective. */
    /*jitterlispvm_specialized_instruction_opcode__eINVALID*/jitterlispvm_specialized_instruction_opcode__Aprimitive_mbox_mget__fR_A_mno_mfast_mbranches, /* POTENTIALLY DEFECTIVE. */
    /*jitterlispvm_specialized_instruction_opcode__eINVALID*/jitterlispvm_specialized_instruction_opcode__Aprimitive_mbox_msetb_mspecial__fR_A_mno_mfast_mbranches, /* POTENTIALLY DEFECTIVE. */
    /*jitterlispvm_specialized_instruction_opcode__eINVALID*/jitterlispvm_specialized_instruction_opcode__Aprimitive_mcar__fR_A_mno_mfast_mbranches, /* POTENTIALLY DEFECTIVE. */
    /*jitterlispvm_specialized_instruction_opcode__eINVALID*/jitterlispvm_specialized_instruction_opcode__Aprimitive_mcdr__fR_A_mno_mfast_mbranches, /* POTENTIALLY DEFECTIVE. */
    jitterlispvm_specialized_instruction_opcode_primitive_mcharacterp, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_primitive_mcons_mspecial, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_primitive_mconsp, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_primitive_meqp, /* NOT potentially defective. */
    /*jitterlispvm_specialized_instruction_opcode__eINVALID*/jitterlispvm_specialized_instruction_opcode__Aprimitive_mfixnum_meqp__fR_A_mno_mfast_mbranches, /* POTENTIALLY DEFECTIVE. */
    /*jitterlispvm_specialized_instruction_opcode__eINVALID*/jitterlispvm_specialized_instruction_opcode__Aprimitive_mfixnum_mnot_meqp__fR_A_mno_mfast_mbranches, /* POTENTIALLY DEFECTIVE. */
    jitterlispvm_specialized_instruction_opcode_primitive_mfixnump, /* NOT potentially defective. */
    /*jitterlispvm_specialized_instruction_opcode__eINVALID*/jitterlispvm_specialized_instruction_opcode__Aprimitive_mgreaterp__fR_A_mno_mfast_mbranches, /* POTENTIALLY DEFECTIVE. */
    /*jitterlispvm_specialized_instruction_opcode__eINVALID*/jitterlispvm_specialized_instruction_opcode__Aprimitive_mlessp__fR_A_mno_mfast_mbranches, /* POTENTIALLY DEFECTIVE. */
    /*jitterlispvm_specialized_instruction_opcode__eINVALID*/jitterlispvm_specialized_instruction_opcode__Aprimitive_mnegate__fR_A_mno_mfast_mbranches, /* POTENTIALLY DEFECTIVE. */
    /*jitterlispvm_specialized_instruction_opcode__eINVALID*/jitterlispvm_specialized_instruction_opcode__Aprimitive_mnegativep__fR_A_mno_mfast_mbranches, /* POTENTIALLY DEFECTIVE. */
    jitterlispvm_specialized_instruction_opcode_primitive_mnon_mconsp, /* NOT potentially defective. */
    /*jitterlispvm_specialized_instruction_opcode__eINVALID*/jitterlispvm_specialized_instruction_opcode__Aprimitive_mnon_mnegativep__fR_A_mno_mfast_mbranches, /* POTENTIALLY DEFECTIVE. */
    jitterlispvm_specialized_instruction_opcode_primitive_mnon_mnullp, /* NOT potentially defective. */
    /*jitterlispvm_specialized_instruction_opcode__eINVALID*/jitterlispvm_specialized_instruction_opcode__Aprimitive_mnon_mpositivep__fR_A_mno_mfast_mbranches, /* POTENTIALLY DEFECTIVE. */
    jitterlispvm_specialized_instruction_opcode_primitive_mnon_msymbolp, /* NOT potentially defective. */
    /*jitterlispvm_specialized_instruction_opcode__eINVALID*/jitterlispvm_specialized_instruction_opcode__Aprimitive_mnon_mzerop__fR_A_mno_mfast_mbranches, /* POTENTIALLY DEFECTIVE. */
    jitterlispvm_specialized_instruction_opcode_primitive_mnot, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_primitive_mnot_meqp, /* NOT potentially defective. */
    /*jitterlispvm_specialized_instruction_opcode__eINVALID*/jitterlispvm_specialized_instruction_opcode__Aprimitive_mnot_mgreaterp__fR_A_mno_mfast_mbranches, /* POTENTIALLY DEFECTIVE. */
    /*jitterlispvm_specialized_instruction_opcode__eINVALID*/jitterlispvm_specialized_instruction_opcode__Aprimitive_mnot_mlessp__fR_A_mno_mfast_mbranches, /* POTENTIALLY DEFECTIVE. */
    jitterlispvm_specialized_instruction_opcode_primitive_mnothingp, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_primitive_mnullp, /* NOT potentially defective. */
    /*jitterlispvm_specialized_instruction_opcode__eINVALID*/jitterlispvm_specialized_instruction_opcode__Aprimitive_mone_mminus__fR_A_mno_mfast_mbranches, /* POTENTIALLY DEFECTIVE. */
    /*jitterlispvm_specialized_instruction_opcode__eINVALID*/jitterlispvm_specialized_instruction_opcode__Aprimitive_mone_mplus__fR_A_mno_mfast_mbranches, /* POTENTIALLY DEFECTIVE. */
    /*jitterlispvm_specialized_instruction_opcode__eINVALID*/jitterlispvm_specialized_instruction_opcode__Aprimitive_mpositivep__fR_A_mno_mfast_mbranches, /* POTENTIALLY DEFECTIVE. */
    /*jitterlispvm_specialized_instruction_opcode__eINVALID*/jitterlispvm_specialized_instruction_opcode__Aprimitive_mprimordial_mdivided__fR_A_mno_mfast_mbranches, /* POTENTIALLY DEFECTIVE. */
    /*jitterlispvm_specialized_instruction_opcode__eINVALID*/jitterlispvm_specialized_instruction_opcode__Aprimitive_mprimordial_mdivided_munsafe__fR_A_mno_mfast_mbranches, /* POTENTIALLY DEFECTIVE. */
    /*jitterlispvm_specialized_instruction_opcode__eINVALID*/jitterlispvm_specialized_instruction_opcode__Aprimitive_mprimordial_mminus__fR_A_mno_mfast_mbranches, /* POTENTIALLY DEFECTIVE. */
    /*jitterlispvm_specialized_instruction_opcode__eINVALID*/jitterlispvm_specialized_instruction_opcode__Aprimitive_mprimordial_mplus__fR_A_mno_mfast_mbranches, /* POTENTIALLY DEFECTIVE. */
    /*jitterlispvm_specialized_instruction_opcode__eINVALID*/jitterlispvm_specialized_instruction_opcode__Aprimitive_mprimordial_mtimes__fR_A_mno_mfast_mbranches, /* POTENTIALLY DEFECTIVE. */
    /*jitterlispvm_specialized_instruction_opcode__eINVALID*/jitterlispvm_specialized_instruction_opcode__Aprimitive_mquotient__fR_A_mno_mfast_mbranches, /* POTENTIALLY DEFECTIVE. */
    /*jitterlispvm_specialized_instruction_opcode__eINVALID*/jitterlispvm_specialized_instruction_opcode__Aprimitive_mquotient_munsafe__fR_A_mno_mfast_mbranches, /* POTENTIALLY DEFECTIVE. */
    /*jitterlispvm_specialized_instruction_opcode__eINVALID*/jitterlispvm_specialized_instruction_opcode__Aprimitive_mremainder__fR_A_mno_mfast_mbranches, /* POTENTIALLY DEFECTIVE. */
    /*jitterlispvm_specialized_instruction_opcode__eINVALID*/jitterlispvm_specialized_instruction_opcode__Aprimitive_mremainder_munsafe__fR_A_mno_mfast_mbranches, /* POTENTIALLY DEFECTIVE. */
    /*jitterlispvm_specialized_instruction_opcode__eINVALID*/jitterlispvm_specialized_instruction_opcode__Aprimitive_mset_mcarb_mspecial__fR_A_mno_mfast_mbranches, /* POTENTIALLY DEFECTIVE. */
    /*jitterlispvm_specialized_instruction_opcode__eINVALID*/jitterlispvm_specialized_instruction_opcode__Aprimitive_mset_mcdrb_mspecial__fR_A_mno_mfast_mbranches, /* POTENTIALLY DEFECTIVE. */
    jitterlispvm_specialized_instruction_opcode_primitive_msymbolp, /* NOT potentially defective. */
    /*jitterlispvm_specialized_instruction_opcode__eINVALID*/jitterlispvm_specialized_instruction_opcode__Aprimitive_mtwo_mdivided__fR_A_mno_mfast_mbranches, /* POTENTIALLY DEFECTIVE. */
    /*jitterlispvm_specialized_instruction_opcode__eINVALID*/jitterlispvm_specialized_instruction_opcode__Aprimitive_mtwo_mquotient__fR_A_mno_mfast_mbranches, /* POTENTIALLY DEFECTIVE. */
    /*jitterlispvm_specialized_instruction_opcode__eINVALID*/jitterlispvm_specialized_instruction_opcode__Aprimitive_mtwo_mremainder__fR_A_mno_mfast_mbranches, /* POTENTIALLY DEFECTIVE. */
    /*jitterlispvm_specialized_instruction_opcode__eINVALID*/jitterlispvm_specialized_instruction_opcode__Aprimitive_mtwo_mtimes__fR_A_mno_mfast_mbranches, /* POTENTIALLY DEFECTIVE. */
    jitterlispvm_specialized_instruction_opcode_primitive_muniquep, /* NOT potentially defective. */
    /*jitterlispvm_specialized_instruction_opcode__eINVALID*/jitterlispvm_specialized_instruction_opcode__Aprimitive_mzerop__fR_A_mno_mfast_mbranches, /* POTENTIALLY DEFECTIVE. */
    jitterlispvm_specialized_instruction_opcode_procedure_mprolog, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_push_mfalse, /* NOT potentially defective. */
    /*jitterlispvm_specialized_instruction_opcode__eINVALID*/jitterlispvm_specialized_instruction_opcode__Apush_mglobal__nR__fR_A_mno_mfast_mbranches, /* POTENTIALLY DEFECTIVE. */
    jitterlispvm_specialized_instruction_opcode_push_mliteral__nR, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_push_mnil, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_push_mnothing, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_push_mone, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_push_mregister___rrR, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_push_munspecified, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_push_mzero, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_register_mto_mregister___rrR___rrR, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_restore_mregister___rrR, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_return, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_save_mregister___rrR, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_tail_mcall__n0, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_tail_mcall__n1, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_tail_mcall__n2, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_tail_mcall__n3, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_tail_mcall__n4, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_tail_mcall__n5, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_tail_mcall__n6, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_tail_mcall__n7, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_tail_mcall__n8, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_tail_mcall__n9, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_tail_mcall__n10, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_tail_mcall__nR, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_tail_mcall_mcompiled__n0, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_tail_mcall_mcompiled__n1, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_tail_mcall_mcompiled__n2, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_tail_mcall_mcompiled__n3, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_tail_mcall_mcompiled__n4, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_tail_mcall_mcompiled__n5, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_tail_mcall_mcompiled__n6, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_tail_mcall_mcompiled__n7, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_tail_mcall_mcompiled__n8, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_tail_mcall_mcompiled__n9, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_tail_mcall_mcompiled__n10, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_tail_mcall_mcompiled__nR, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_unreachable, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__Abranch__fR_A_mno_mfast_mbranches, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__Abranch_mif_mfalse__fR_A_mno_mfast_mbranches, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__Abranch_mif_mnot_mless__fR__fR_A_mno_mfast_mbranches, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__Abranch_mif_mnot_mnull__fR_A_mno_mfast_mbranches, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__Abranch_mif_mnull__fR_A_mno_mfast_mbranches, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__Abranch_mif_mregister_mnon_mzero___rrR__fR__fR_A_mno_mfast_mbranches, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__Abranch_mif_mtrue__fR_A_mno_mfast_mbranches, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__Acheck_mclosure__fR_A_mno_mfast_mbranches, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__Acheck_mglobal_mdefined__nR__fR_A_mno_mfast_mbranches, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__Acheck_min_marity__n0__fR_A_mno_mfast_mbranches, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__Acheck_min_marity__n1__fR_A_mno_mfast_mbranches, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__Acheck_min_marity__n2__fR_A_mno_mfast_mbranches, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__Acheck_min_marity__n3__fR_A_mno_mfast_mbranches, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__Acheck_min_marity__n4__fR_A_mno_mfast_mbranches, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__Acheck_min_marity__n5__fR_A_mno_mfast_mbranches, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__Acheck_min_marity__n6__fR_A_mno_mfast_mbranches, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__Acheck_min_marity__n7__fR_A_mno_mfast_mbranches, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__Acheck_min_marity__n8__fR_A_mno_mfast_mbranches, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__Acheck_min_marity__n9__fR_A_mno_mfast_mbranches, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__Acheck_min_marity__n10__fR_A_mno_mfast_mbranches, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__Acheck_min_marity__nR__fR_A_mno_mfast_mbranches, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__Acheck_min_marity_m_malt__n0__fR_A_mno_mfast_mbranches, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__Acheck_min_marity_m_malt__n1__fR_A_mno_mfast_mbranches, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__Acheck_min_marity_m_malt__n2__fR_A_mno_mfast_mbranches, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__Acheck_min_marity_m_malt__n3__fR_A_mno_mfast_mbranches, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__Acheck_min_marity_m_malt__n4__fR_A_mno_mfast_mbranches, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__Acheck_min_marity_m_malt__n5__fR_A_mno_mfast_mbranches, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__Acheck_min_marity_m_malt__n6__fR_A_mno_mfast_mbranches, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__Acheck_min_marity_m_malt__n7__fR_A_mno_mfast_mbranches, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__Acheck_min_marity_m_malt__n8__fR_A_mno_mfast_mbranches, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__Acheck_min_marity_m_malt__n9__fR_A_mno_mfast_mbranches, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__Acheck_min_marity_m_malt__n10__fR_A_mno_mfast_mbranches, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__Acheck_min_marity_m_malt__nR__fR_A_mno_mfast_mbranches, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__Agc_mif_mneeded__fR_A_mno_mfast_mbranches, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__Apop_mto_mglobal__nR__fR_A_mno_mfast_mbranches, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__Apop_mto_mglobal_mdefined__nR__fR_A_mno_mfast_mbranches, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__Aprimitive__nR__n0__fR_A_mno_mfast_mbranches, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__Aprimitive__nR__n1__fR_A_mno_mfast_mbranches, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__Aprimitive__nR__n2__fR_A_mno_mfast_mbranches, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__Aprimitive__nR__n3__fR_A_mno_mfast_mbranches, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__Aprimitive__nR__n4__fR_A_mno_mfast_mbranches, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__Aprimitive__nR__nR__fR_A_mno_mfast_mbranches, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__Aprimitive_mbox_mget__fR_A_mno_mfast_mbranches, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__Aprimitive_mbox_msetb_mspecial__fR_A_mno_mfast_mbranches, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__Aprimitive_mcar__fR_A_mno_mfast_mbranches, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__Aprimitive_mcdr__fR_A_mno_mfast_mbranches, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__Aprimitive_mfixnum_meqp__fR_A_mno_mfast_mbranches, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__Aprimitive_mfixnum_mnot_meqp__fR_A_mno_mfast_mbranches, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__Aprimitive_mgreaterp__fR_A_mno_mfast_mbranches, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__Aprimitive_mlessp__fR_A_mno_mfast_mbranches, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__Aprimitive_mnegate__fR_A_mno_mfast_mbranches, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__Aprimitive_mnegativep__fR_A_mno_mfast_mbranches, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__Aprimitive_mnon_mnegativep__fR_A_mno_mfast_mbranches, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__Aprimitive_mnon_mpositivep__fR_A_mno_mfast_mbranches, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__Aprimitive_mnon_mzerop__fR_A_mno_mfast_mbranches, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__Aprimitive_mnot_mgreaterp__fR_A_mno_mfast_mbranches, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__Aprimitive_mnot_mlessp__fR_A_mno_mfast_mbranches, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__Aprimitive_mone_mminus__fR_A_mno_mfast_mbranches, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__Aprimitive_mone_mplus__fR_A_mno_mfast_mbranches, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__Aprimitive_mpositivep__fR_A_mno_mfast_mbranches, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__Aprimitive_mprimordial_mdivided__fR_A_mno_mfast_mbranches, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__Aprimitive_mprimordial_mdivided_munsafe__fR_A_mno_mfast_mbranches, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__Aprimitive_mprimordial_mminus__fR_A_mno_mfast_mbranches, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__Aprimitive_mprimordial_mplus__fR_A_mno_mfast_mbranches, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__Aprimitive_mprimordial_mtimes__fR_A_mno_mfast_mbranches, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__Aprimitive_mquotient__fR_A_mno_mfast_mbranches, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__Aprimitive_mquotient_munsafe__fR_A_mno_mfast_mbranches, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__Aprimitive_mremainder__fR_A_mno_mfast_mbranches, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__Aprimitive_mremainder_munsafe__fR_A_mno_mfast_mbranches, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__Aprimitive_mset_mcarb_mspecial__fR_A_mno_mfast_mbranches, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__Aprimitive_mset_mcdrb_mspecial__fR_A_mno_mfast_mbranches, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__Aprimitive_mtwo_mdivided__fR_A_mno_mfast_mbranches, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__Aprimitive_mtwo_mquotient__fR_A_mno_mfast_mbranches, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__Aprimitive_mtwo_mremainder__fR_A_mno_mfast_mbranches, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__Aprimitive_mtwo_mtimes__fR_A_mno_mfast_mbranches, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__Aprimitive_mzerop__fR_A_mno_mfast_mbranches, /* NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__Apush_mglobal__nR__fR_A_mno_mfast_mbranches /* NOT potentially defective. */
  };
#endif // #ifdef JITTER_HAVE_PATCH_IN


void
jitterlispvm_rewrite (struct jitter_mutable_routine *jitter_mutable_routine_p)
{
  JITTTER_REWRITE_FUNCTION_PROLOG_;

/* User-specified code, rewriter part: beginning. */

/* User-specified code, rewriter part: end */


//asm volatile ("\n# checking pop-to-register-push-register");
//fprintf (stderr, "Trying rule 1 of 66, \"pop-to-register-push-register\" (line 1563)\n");
/* Rewrite rule "pop-to-register-push-register" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
    JITTER_RULE_DECLARE_PLACEHOLDER_(b);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, pop_mto_mregister)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, push_mregister)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(0, 0, a)
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(1, 0, b)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
    JITTER_RULE_CLONE_PLACEHOLDER_(b);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule pop-to-register-push-register (line 1563) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction copy-to-register\n");
    JITTER_RULE_APPEND_INSTRUCTION_(copy_mto_mregister);
    //fprintf (stderr, "    instantiating the 0-th argument of copy-to-register\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(a)
                                 );
    //fprintf (stderr, "    rewrite: adding instruction copy-from-register\n");
    JITTER_RULE_APPEND_INSTRUCTION_(copy_mfrom_mregister);
    //fprintf (stderr, "    instantiating the 0-th argument of copy-from-register\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(b)
                                 );
    //fprintf (stderr, "  ...End of the rule pop-to-register-push-register\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
    JITTER_RULE_DESTROY_PLACEHOLDER_(b);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking pop-to-register-push-literal");
//fprintf (stderr, "Trying rule 2 of 66, \"pop-to-register-push-literal\" (line 1568)\n");
/* Rewrite rule "pop-to-register-push-literal" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
    JITTER_RULE_DECLARE_PLACEHOLDER_(b);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, pop_mto_mregister)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, push_mliteral)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(0, 0, a)
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(1, 0, b)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
    JITTER_RULE_CLONE_PLACEHOLDER_(b);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule pop-to-register-push-literal (line 1568) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction copy-to-register\n");
    JITTER_RULE_APPEND_INSTRUCTION_(copy_mto_mregister);
    //fprintf (stderr, "    instantiating the 0-th argument of copy-to-register\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(a)
                                 );
    //fprintf (stderr, "    rewrite: adding instruction copy-from-literal\n");
    JITTER_RULE_APPEND_INSTRUCTION_(copy_mfrom_mliteral);
    //fprintf (stderr, "    instantiating the 0-th argument of copy-from-literal\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(b)
                                 );
    //fprintf (stderr, "  ...End of the rule pop-to-register-push-literal\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
    JITTER_RULE_DESTROY_PLACEHOLDER_(b);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking push-literal-pop-to-register");
//fprintf (stderr, "Trying rule 3 of 66, \"push-literal-pop-to-register\" (line 1574)\n");
/* Rewrite rule "push-literal-pop-to-register" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
    JITTER_RULE_DECLARE_PLACEHOLDER_(b);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, push_mliteral)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, pop_mto_mregister)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(0, 0, a)
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(1, 0, b)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
    JITTER_RULE_CLONE_PLACEHOLDER_(b);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule push-literal-pop-to-register (line 1574) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction literal-to-register\n");
    JITTER_RULE_APPEND_INSTRUCTION_(literal_mto_mregister);
    //fprintf (stderr, "    instantiating the 0-th argument of literal-to-register\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(a)
                                 );
    //fprintf (stderr, "    instantiating the 1-th argument of literal-to-register\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(b)
                                 );
    //fprintf (stderr, "  ...End of the rule push-literal-pop-to-register\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
    JITTER_RULE_DESTROY_PLACEHOLDER_(b);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking drop-push-register");
//fprintf (stderr, "Trying rule 4 of 66, \"drop-push-register\" (line 1580)\n");
/* Rewrite rule "drop-push-register" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, drop)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, push_mregister)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(1, 0, a)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule drop-push-register (line 1580) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction copy-from-register\n");
    JITTER_RULE_APPEND_INSTRUCTION_(copy_mfrom_mregister);
    //fprintf (stderr, "    instantiating the 0-th argument of copy-from-register\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(a)
                                 );
    //fprintf (stderr, "  ...End of the rule drop-push-register\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking drop-push-literal");
//fprintf (stderr, "Trying rule 5 of 66, \"drop-push-literal\" (line 1585)\n");
/* Rewrite rule "drop-push-literal" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, drop)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, push_mliteral)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(1, 0, a)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule drop-push-literal (line 1585) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction copy-from-literal\n");
    JITTER_RULE_APPEND_INSTRUCTION_(copy_mfrom_mliteral);
    //fprintf (stderr, "    instantiating the 0-th argument of copy-from-literal\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(a)
                                 );
    //fprintf (stderr, "  ...End of the rule drop-push-literal\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking drop-push-unspecified");
//fprintf (stderr, "Trying rule 6 of 66, \"drop-push-unspecified\" (line 1590)\n");
/* Rewrite rule "drop-push-unspecified" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, drop)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, push_munspecified)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule drop-push-unspecified (line 1590) fires...\n");
    //fprintf (stderr, "  ...End of the rule drop-push-unspecified\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking push-register-pop-to-register");
//fprintf (stderr, "Trying rule 7 of 66, \"push-register-pop-to-register\" (line 1596)\n");
/* Rewrite rule "push-register-pop-to-register" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
    JITTER_RULE_DECLARE_PLACEHOLDER_(b);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, push_mregister)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, pop_mto_mregister)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(0, 0, a)
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(1, 0, b)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
    JITTER_RULE_CLONE_PLACEHOLDER_(b);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule push-register-pop-to-register (line 1596) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction register-to-register\n");
    JITTER_RULE_APPEND_INSTRUCTION_(register_mto_mregister);
    //fprintf (stderr, "    instantiating the 0-th argument of register-to-register\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(a)
                                 );
    //fprintf (stderr, "    instantiating the 1-th argument of register-to-register\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(b)
                                 );
    //fprintf (stderr, "  ...End of the rule push-register-pop-to-register\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
    JITTER_RULE_DESTROY_PLACEHOLDER_(b);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking copy-from-register-pop-to-register");
//fprintf (stderr, "Trying rule 8 of 66, \"copy-from-register-pop-to-register\" (line 1602)\n");
/* Rewrite rule "copy-from-register-pop-to-register" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
    JITTER_RULE_DECLARE_PLACEHOLDER_(b);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, copy_mfrom_mregister)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, pop_mto_mregister)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(0, 0, a)
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(1, 0, b)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
    JITTER_RULE_CLONE_PLACEHOLDER_(b);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule copy-from-register-pop-to-register (line 1602) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction register-to-register\n");
    JITTER_RULE_APPEND_INSTRUCTION_(register_mto_mregister);
    //fprintf (stderr, "    instantiating the 0-th argument of register-to-register\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(a)
                                 );
    //fprintf (stderr, "    instantiating the 1-th argument of register-to-register\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(b)
                                 );
    //fprintf (stderr, "    rewrite: adding instruction drop\n");
    JITTER_RULE_APPEND_INSTRUCTION_(drop);
    //fprintf (stderr, "  ...End of the rule copy-from-register-pop-to-register\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
    JITTER_RULE_DESTROY_PLACEHOLDER_(b);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking copy-from-literal-pop-to-register");
//fprintf (stderr, "Trying rule 9 of 66, \"copy-from-literal-pop-to-register\" (line 1608)\n");
/* Rewrite rule "copy-from-literal-pop-to-register" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
    JITTER_RULE_DECLARE_PLACEHOLDER_(b);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, copy_mfrom_mliteral)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, pop_mto_mregister)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(0, 0, a)
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(1, 0, b)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
    JITTER_RULE_CLONE_PLACEHOLDER_(b);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule copy-from-literal-pop-to-register (line 1608) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction literal-to-register\n");
    JITTER_RULE_APPEND_INSTRUCTION_(literal_mto_mregister);
    //fprintf (stderr, "    instantiating the 0-th argument of literal-to-register\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(a)
                                 );
    //fprintf (stderr, "    instantiating the 1-th argument of literal-to-register\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(b)
                                 );
    //fprintf (stderr, "    rewrite: adding instruction drop\n");
    JITTER_RULE_APPEND_INSTRUCTION_(drop);
    //fprintf (stderr, "  ...End of the rule copy-from-literal-pop-to-register\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
    JITTER_RULE_DESTROY_PLACEHOLDER_(b);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking push-register-push-register");
//fprintf (stderr, "Trying rule 10 of 66, \"push-register-push-register\" (line 1614)\n");
/* Rewrite rule "push-register-push-register" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, push_mregister)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, push_mregister)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(0, 0, a)
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(1, 0, a)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule push-register-push-register (line 1614) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction push-register\n");
    JITTER_RULE_APPEND_INSTRUCTION_(push_mregister);
    //fprintf (stderr, "    instantiating the 0-th argument of push-register\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(a)
                                 );
    //fprintf (stderr, "    rewrite: adding instruction dup\n");
    JITTER_RULE_APPEND_INSTRUCTION_(dup);
    //fprintf (stderr, "  ...End of the rule push-register-push-register\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking copy-from-register-push-register");
//fprintf (stderr, "Trying rule 11 of 66, \"copy-from-register-push-register\" (line 1620)\n");
/* Rewrite rule "copy-from-register-push-register" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, copy_mfrom_mregister)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, push_mregister)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(0, 0, a)
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(1, 0, a)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule copy-from-register-push-register (line 1620) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction copy-from-register\n");
    JITTER_RULE_APPEND_INSTRUCTION_(copy_mfrom_mregister);
    //fprintf (stderr, "    instantiating the 0-th argument of copy-from-register\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(a)
                                 );
    //fprintf (stderr, "    rewrite: adding instruction dup\n");
    JITTER_RULE_APPEND_INSTRUCTION_(dup);
    //fprintf (stderr, "  ...End of the rule copy-from-register-push-register\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking copy-to-register-and-push-the-same-register");
//fprintf (stderr, "Trying rule 12 of 66, \"copy-to-register-and-push-the-same-register\" (line 1626)\n");
/* Rewrite rule "copy-to-register-and-push-the-same-register" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, copy_mto_mregister)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, push_mregister)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(0, 0, a)
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(1, 0, a)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule copy-to-register-and-push-the-same-register (line 1626) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction copy-to-register\n");
    JITTER_RULE_APPEND_INSTRUCTION_(copy_mto_mregister);
    //fprintf (stderr, "    instantiating the 0-th argument of copy-to-register\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(a)
                                 );
    //fprintf (stderr, "    rewrite: adding instruction dup\n");
    JITTER_RULE_APPEND_INSTRUCTION_(dup);
    //fprintf (stderr, "  ...End of the rule copy-to-register-and-push-the-same-register\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking copy-to-and-from-the-same-register");
//fprintf (stderr, "Trying rule 13 of 66, \"copy-to-and-from-the-same-register\" (line 1632)\n");
/* Rewrite rule "copy-to-and-from-the-same-register" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, copy_mto_mregister)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, copy_mfrom_mregister)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(0, 0, a)
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(1, 0, a)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule copy-to-and-from-the-same-register (line 1632) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction copy-to-register\n");
    JITTER_RULE_APPEND_INSTRUCTION_(copy_mto_mregister);
    //fprintf (stderr, "    instantiating the 0-th argument of copy-to-register\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(a)
                                 );
    //fprintf (stderr, "  ...End of the rule copy-to-and-from-the-same-register\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking push-the-same-literal-twice");
//fprintf (stderr, "Trying rule 14 of 66, \"push-the-same-literal-twice\" (line 1638)\n");
/* Rewrite rule "push-the-same-literal-twice" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, push_mliteral)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, push_mliteral)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(0, 0, a)
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(1, 0, a)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule push-the-same-literal-twice (line 1638) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction push-literal\n");
    JITTER_RULE_APPEND_INSTRUCTION_(push_mliteral);
    //fprintf (stderr, "    instantiating the 0-th argument of push-literal\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(a)
                                 );
    //fprintf (stderr, "    rewrite: adding instruction dup\n");
    JITTER_RULE_APPEND_INSTRUCTION_(dup);
    //fprintf (stderr, "  ...End of the rule push-the-same-literal-twice\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking copy-from-literal-and-push-the-same-literal");
//fprintf (stderr, "Trying rule 15 of 66, \"copy-from-literal-and-push-the-same-literal\" (line 1644)\n");
/* Rewrite rule "copy-from-literal-and-push-the-same-literal" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, copy_mfrom_mliteral)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, push_mliteral)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(0, 0, a)
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(1, 0, a)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule copy-from-literal-and-push-the-same-literal (line 1644) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction copy-from-literal\n");
    JITTER_RULE_APPEND_INSTRUCTION_(copy_mfrom_mliteral);
    //fprintf (stderr, "    instantiating the 0-th argument of copy-from-literal\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(a)
                                 );
    //fprintf (stderr, "    rewrite: adding instruction dup\n");
    JITTER_RULE_APPEND_INSTRUCTION_(dup);
    //fprintf (stderr, "  ...End of the rule copy-from-literal-and-push-the-same-literal\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking copy-from-literal-then-from-another-literal");
//fprintf (stderr, "Trying rule 16 of 66, \"copy-from-literal-then-from-another-literal\" (line 1652)\n");
/* Rewrite rule "copy-from-literal-then-from-another-literal" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
    JITTER_RULE_DECLARE_PLACEHOLDER_(b);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, copy_mfrom_mliteral)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, copy_mfrom_mliteral)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(0, 0, a)
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(1, 0, b)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
    JITTER_RULE_CLONE_PLACEHOLDER_(b);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule copy-from-literal-then-from-another-literal (line 1652) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction copy-from-literal\n");
    JITTER_RULE_APPEND_INSTRUCTION_(copy_mfrom_mliteral);
    //fprintf (stderr, "    instantiating the 0-th argument of copy-from-literal\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(b)
                                 );
    //fprintf (stderr, "  ...End of the rule copy-from-literal-then-from-another-literal\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
    JITTER_RULE_DESTROY_PLACEHOLDER_(b);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking copy-from-literal-then-from-a-register");
//fprintf (stderr, "Trying rule 17 of 66, \"copy-from-literal-then-from-a-register\" (line 1657)\n");
/* Rewrite rule "copy-from-literal-then-from-a-register" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
    JITTER_RULE_DECLARE_PLACEHOLDER_(b);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, copy_mfrom_mliteral)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, copy_mfrom_mregister)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(0, 0, a)
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(1, 0, b)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
    JITTER_RULE_CLONE_PLACEHOLDER_(b);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule copy-from-literal-then-from-a-register (line 1657) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction copy-from-register\n");
    JITTER_RULE_APPEND_INSTRUCTION_(copy_mfrom_mregister);
    //fprintf (stderr, "    instantiating the 0-th argument of copy-from-register\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(b)
                                 );
    //fprintf (stderr, "  ...End of the rule copy-from-literal-then-from-a-register\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
    JITTER_RULE_DESTROY_PLACEHOLDER_(b);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking copy-from-a-register-then-from-literal");
//fprintf (stderr, "Trying rule 18 of 66, \"copy-from-a-register-then-from-literal\" (line 1662)\n");
/* Rewrite rule "copy-from-a-register-then-from-literal" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
    JITTER_RULE_DECLARE_PLACEHOLDER_(b);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, copy_mfrom_mregister)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, copy_mfrom_mliteral)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(0, 0, a)
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(1, 0, b)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
    JITTER_RULE_CLONE_PLACEHOLDER_(b);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule copy-from-a-register-then-from-literal (line 1662) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction copy-from-literal\n");
    JITTER_RULE_APPEND_INSTRUCTION_(copy_mfrom_mliteral);
    //fprintf (stderr, "    instantiating the 0-th argument of copy-from-literal\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(b)
                                 );
    //fprintf (stderr, "  ...End of the rule copy-from-a-register-then-from-literal\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
    JITTER_RULE_DESTROY_PLACEHOLDER_(b);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking copy-from-a-register-then-from-another-register");
//fprintf (stderr, "Trying rule 19 of 66, \"copy-from-a-register-then-from-another-register\" (line 1667)\n");
/* Rewrite rule "copy-from-a-register-then-from-another-register" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
    JITTER_RULE_DECLARE_PLACEHOLDER_(b);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, copy_mfrom_mregister)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, copy_mfrom_mregister)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(0, 0, a)
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(1, 0, b)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
    JITTER_RULE_CLONE_PLACEHOLDER_(b);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule copy-from-a-register-then-from-another-register (line 1667) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction copy-from-register\n");
    JITTER_RULE_APPEND_INSTRUCTION_(copy_mfrom_mregister);
    //fprintf (stderr, "    instantiating the 0-th argument of copy-from-register\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(b)
                                 );
    //fprintf (stderr, "  ...End of the rule copy-from-a-register-then-from-another-register\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
    JITTER_RULE_DESTROY_PLACEHOLDER_(b);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking useless-copy-from-literal-elimination");
//fprintf (stderr, "Trying rule 20 of 66, \"useless-copy-from-literal-elimination\" (line 1674)\n");
/* Rewrite rule "useless-copy-from-literal-elimination" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, copy_mfrom_mliteral)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, drop)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(0, 0, a)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule useless-copy-from-literal-elimination (line 1674) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction drop\n");
    JITTER_RULE_APPEND_INSTRUCTION_(drop);
    //fprintf (stderr, "  ...End of the rule useless-copy-from-literal-elimination\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking useless-copy-from-register-elimination");
//fprintf (stderr, "Trying rule 21 of 66, \"useless-copy-from-register-elimination\" (line 1679)\n");
/* Rewrite rule "useless-copy-from-register-elimination" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, copy_mfrom_mregister)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, drop)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(0, 0, a)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule useless-copy-from-register-elimination (line 1679) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction drop\n");
    JITTER_RULE_APPEND_INSTRUCTION_(drop);
    //fprintf (stderr, "  ...End of the rule useless-copy-from-register-elimination\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking push-register-drop");
//fprintf (stderr, "Trying rule 22 of 66, \"push-register-drop\" (line 1685)\n");
/* Rewrite rule "push-register-drop" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, push_mregister)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, drop)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(0, 0, a)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule push-register-drop (line 1685) fires...\n");
    //fprintf (stderr, "  ...End of the rule push-register-drop\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking push-literal-drop");
//fprintf (stderr, "Trying rule 23 of 66, \"push-literal-drop\" (line 1690)\n");
/* Rewrite rule "push-literal-drop" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, push_mliteral)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, drop)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(0, 0, a)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule push-literal-drop (line 1690) fires...\n");
    //fprintf (stderr, "  ...End of the rule push-literal-drop\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking pop-to-register-copy-from-register");
//fprintf (stderr, "Trying rule 24 of 66, \"pop-to-register-copy-from-register\" (line 1696)\n");
/* Rewrite rule "pop-to-register-copy-from-register" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, pop_mto_mregister)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, copy_mfrom_mregister)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(0, 0, a)
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(1, 0, a)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule pop-to-register-copy-from-register (line 1696) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction copy-to-register\n");
    JITTER_RULE_APPEND_INSTRUCTION_(copy_mto_mregister);
    //fprintf (stderr, "    instantiating the 0-th argument of copy-to-register\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(a)
                                 );
    //fprintf (stderr, "    rewrite: adding instruction nip\n");
    JITTER_RULE_APPEND_INSTRUCTION_(nip);
    //fprintf (stderr, "  ...End of the rule pop-to-register-copy-from-register\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking pop-to-register-drop");
//fprintf (stderr, "Trying rule 25 of 66, \"pop-to-register-drop\" (line 1701)\n");
/* Rewrite rule "pop-to-register-drop" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, pop_mto_mregister)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, drop)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(0, 0, a)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule pop-to-register-drop (line 1701) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction nip\n");
    JITTER_RULE_APPEND_INSTRUCTION_(nip);
    //fprintf (stderr, "    rewrite: adding instruction pop-to-register\n");
    JITTER_RULE_APPEND_INSTRUCTION_(pop_mto_mregister);
    //fprintf (stderr, "    instantiating the 0-th argument of pop-to-register\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(a)
                                 );
    //fprintf (stderr, "  ...End of the rule pop-to-register-drop\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking pop-to-register-nip");
//fprintf (stderr, "Trying rule 26 of 66, \"pop-to-register-nip\" (line 1707)\n");
/* Rewrite rule "pop-to-register-nip" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, pop_mto_mregister)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, nip)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(0, 0, a)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule pop-to-register-nip (line 1707) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction copy-to-register\n");
    JITTER_RULE_APPEND_INSTRUCTION_(copy_mto_mregister);
    //fprintf (stderr, "    instantiating the 0-th argument of copy-to-register\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(a)
                                 );
    //fprintf (stderr, "    rewrite: adding instruction drop-nip\n");
    JITTER_RULE_APPEND_INSTRUCTION_(drop_mnip);
    //fprintf (stderr, "  ...End of the rule pop-to-register-nip\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking drop-nip");
//fprintf (stderr, "Trying rule 27 of 66, \"drop-nip\" (line 1713)\n");
/* Rewrite rule "drop-nip" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, drop)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, nip)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule drop-nip (line 1713) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction drop-nip\n");
    JITTER_RULE_APPEND_INSTRUCTION_(drop_mnip);
    //fprintf (stderr, "  ...End of the rule drop-nip\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking nip-drop");
//fprintf (stderr, "Trying rule 28 of 66, \"nip-drop\" (line 1719)\n");
/* Rewrite rule "nip-drop" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, nip)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, drop)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule nip-drop (line 1719) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction nip-drop\n");
    JITTER_RULE_APPEND_INSTRUCTION_(nip_mdrop);
    //fprintf (stderr, "  ...End of the rule nip-drop\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking drop-drop");
//fprintf (stderr, "Trying rule 29 of 66, \"drop-drop\" (line 1725)\n");
/* Rewrite rule "drop-drop" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, drop)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, drop)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule drop-drop (line 1725) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction nip-drop\n");
    JITTER_RULE_APPEND_INSTRUCTION_(nip_mdrop);
    //fprintf (stderr, "  ...End of the rule drop-drop\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking nip-drop-drop");
//fprintf (stderr, "Trying rule 30 of 66, \"nip-drop-drop\" (line 1731)\n");
/* Rewrite rule "nip-drop-drop" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, nip_mdrop)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, drop)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule nip-drop-drop (line 1731) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction nip-two-drop\n");
    JITTER_RULE_APPEND_INSTRUCTION_(nip_mtwo_mdrop);
    //fprintf (stderr, "  ...End of the rule nip-drop-drop\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking nip-two-drop-drop");
//fprintf (stderr, "Trying rule 31 of 66, \"nip-two-drop-drop\" (line 1736)\n");
/* Rewrite rule "nip-two-drop-drop" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, nip_mtwo_mdrop)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, drop)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule nip-two-drop-drop (line 1736) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction nip-three-drop\n");
    JITTER_RULE_APPEND_INSTRUCTION_(nip_mthree_mdrop);
    //fprintf (stderr, "  ...End of the rule nip-two-drop-drop\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking nip-three-drop-drop");
//fprintf (stderr, "Trying rule 32 of 66, \"nip-three-drop-drop\" (line 1741)\n");
/* Rewrite rule "nip-three-drop-drop" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, nip_mthree_mdrop)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, drop)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule nip-three-drop-drop (line 1741) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction nip-four-drop\n");
    JITTER_RULE_APPEND_INSTRUCTION_(nip_mfour_mdrop);
    //fprintf (stderr, "  ...End of the rule nip-three-drop-drop\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking nip-four-drop-drop");
//fprintf (stderr, "Trying rule 33 of 66, \"nip-four-drop-drop\" (line 1746)\n");
/* Rewrite rule "nip-four-drop-drop" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, nip_mfour_mdrop)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, drop)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule nip-four-drop-drop (line 1746) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction nip-five-drop\n");
    JITTER_RULE_APPEND_INSTRUCTION_(nip_mfive_mdrop);
    //fprintf (stderr, "  ...End of the rule nip-four-drop-drop\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking nip-five-drop-drop");
//fprintf (stderr, "Trying rule 34 of 66, \"nip-five-drop-drop\" (line 1751)\n");
/* Rewrite rule "nip-five-drop-drop" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, nip_mfive_mdrop)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, drop)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule nip-five-drop-drop (line 1751) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction nip-six-drop\n");
    JITTER_RULE_APPEND_INSTRUCTION_(nip_msix_mdrop);
    //fprintf (stderr, "  ...End of the rule nip-five-drop-drop\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking nip-nip");
//fprintf (stderr, "Trying rule 35 of 66, \"nip-nip\" (line 1760)\n");
/* Rewrite rule "nip-nip" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, nip)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, nip)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule nip-nip (line 1760) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction nip-two\n");
    JITTER_RULE_APPEND_INSTRUCTION_(nip_mtwo);
    //fprintf (stderr, "  ...End of the rule nip-nip\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking nip-two-nip");
//fprintf (stderr, "Trying rule 36 of 66, \"nip-two-nip\" (line 1765)\n");
/* Rewrite rule "nip-two-nip" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, nip_mtwo)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, nip)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule nip-two-nip (line 1765) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction nip-three\n");
    JITTER_RULE_APPEND_INSTRUCTION_(nip_mthree);
    //fprintf (stderr, "  ...End of the rule nip-two-nip\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking nip-three-nip");
//fprintf (stderr, "Trying rule 37 of 66, \"nip-three-nip\" (line 1770)\n");
/* Rewrite rule "nip-three-nip" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, nip_mthree)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, nip)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule nip-three-nip (line 1770) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction nip-four\n");
    JITTER_RULE_APPEND_INSTRUCTION_(nip_mfour);
    //fprintf (stderr, "  ...End of the rule nip-three-nip\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking nip-four-nip");
//fprintf (stderr, "Trying rule 38 of 66, \"nip-four-nip\" (line 1775)\n");
/* Rewrite rule "nip-four-nip" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, nip_mfour)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, nip)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule nip-four-nip (line 1775) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction nip-five\n");
    JITTER_RULE_APPEND_INSTRUCTION_(nip_mfive);
    //fprintf (stderr, "  ...End of the rule nip-four-nip\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking nip-five-nip");
//fprintf (stderr, "Trying rule 39 of 66, \"nip-five-nip\" (line 1780)\n");
/* Rewrite rule "nip-five-nip" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, nip_mfive)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, nip)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule nip-five-nip (line 1780) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction nip-six\n");
    JITTER_RULE_APPEND_INSTRUCTION_(nip_msix);
    //fprintf (stderr, "  ...End of the rule nip-five-nip\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking nip-before-copy-from-register");
//fprintf (stderr, "Trying rule 40 of 66, \"nip-before-copy-from-register\" (line 1788)\n");
/* Rewrite rule "nip-before-copy-from-register" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, copy_mfrom_mregister)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, nip)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(0, 0, a)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule nip-before-copy-from-register (line 1788) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction nip\n");
    JITTER_RULE_APPEND_INSTRUCTION_(nip);
    //fprintf (stderr, "    rewrite: adding instruction copy-from-register\n");
    JITTER_RULE_APPEND_INSTRUCTION_(copy_mfrom_mregister);
    //fprintf (stderr, "    instantiating the 0-th argument of copy-from-register\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(a)
                                 );
    //fprintf (stderr, "  ...End of the rule nip-before-copy-from-register\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking nip-before-copy-from-literal");
//fprintf (stderr, "Trying rule 41 of 66, \"nip-before-copy-from-literal\" (line 1793)\n");
/* Rewrite rule "nip-before-copy-from-literal" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, copy_mfrom_mliteral)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, nip)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(0, 0, a)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule nip-before-copy-from-literal (line 1793) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction nip\n");
    JITTER_RULE_APPEND_INSTRUCTION_(nip);
    //fprintf (stderr, "    rewrite: adding instruction copy-from-literal\n");
    JITTER_RULE_APPEND_INSTRUCTION_(copy_mfrom_mliteral);
    //fprintf (stderr, "    instantiating the 0-th argument of copy-from-literal\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(a)
                                 );
    //fprintf (stderr, "  ...End of the rule nip-before-copy-from-literal\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking copy-to-register-nip");
//fprintf (stderr, "Trying rule 42 of 66, \"copy-to-register-nip\" (line 1802)\n");
/* Rewrite rule "copy-to-register-nip" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, copy_mto_mregister)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, nip)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(0, 0, a)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule copy-to-register-nip (line 1802) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction nip\n");
    JITTER_RULE_APPEND_INSTRUCTION_(nip);
    //fprintf (stderr, "    rewrite: adding instruction copy-to-register\n");
    JITTER_RULE_APPEND_INSTRUCTION_(copy_mto_mregister);
    //fprintf (stderr, "    instantiating the 0-th argument of copy-to-register\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(a)
                                 );
    //fprintf (stderr, "  ...End of the rule copy-to-register-nip\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking pop-to-register-return");
//fprintf (stderr, "Trying rule 43 of 66, \"pop-to-register-return\" (line 1808)\n");
/* Rewrite rule "pop-to-register-return" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, pop_mto_mregister)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, return)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(0, 0, a)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule pop-to-register-return (line 1808) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction drop\n");
    JITTER_RULE_APPEND_INSTRUCTION_(drop);
    //fprintf (stderr, "    rewrite: adding instruction return\n");
    JITTER_RULE_APPEND_INSTRUCTION_(return);
    //fprintf (stderr, "  ...End of the rule pop-to-register-return\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking copy-to-register-return");
//fprintf (stderr, "Trying rule 44 of 66, \"copy-to-register-return\" (line 1814)\n");
/* Rewrite rule "copy-to-register-return" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, copy_mto_mregister)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, return)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(0, 0, a)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule copy-to-register-return (line 1814) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction return\n");
    JITTER_RULE_APPEND_INSTRUCTION_(return);
    //fprintf (stderr, "  ...End of the rule copy-to-register-return\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking not-branch-if-true");
//fprintf (stderr, "Trying rule 45 of 66, \"not-branch-if-true\" (line 1827)\n");
/* Rewrite rule "not-branch-if-true" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(b);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, primitive_mnot)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, branch_mif_mtrue)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(1, 0, b)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(b);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule not-branch-if-true (line 1827) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction branch-if-false\n");
    JITTER_RULE_APPEND_INSTRUCTION_(branch_mif_mfalse);
    //fprintf (stderr, "    instantiating the 0-th argument of branch-if-false\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(b)
                                 );
    //fprintf (stderr, "  ...End of the rule not-branch-if-true\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(b);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking nullp-branch-if-true");
//fprintf (stderr, "Trying rule 46 of 66, \"nullp-branch-if-true\" (line 1833)\n");
/* Rewrite rule "nullp-branch-if-true" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, primitive_mnullp)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, branch_mif_mtrue)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(1, 0, a)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule nullp-branch-if-true (line 1833) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction branch-if-null\n");
    JITTER_RULE_APPEND_INSTRUCTION_(branch_mif_mnull);
    //fprintf (stderr, "    instantiating the 0-th argument of branch-if-null\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(a)
                                 );
    //fprintf (stderr, "  ...End of the rule nullp-branch-if-true\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking nullp-branch-if-false");
//fprintf (stderr, "Trying rule 47 of 66, \"nullp-branch-if-false\" (line 1838)\n");
/* Rewrite rule "nullp-branch-if-false" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, primitive_mnullp)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, branch_mif_mfalse)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(1, 0, a)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule nullp-branch-if-false (line 1838) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction branch-if-not-null\n");
    JITTER_RULE_APPEND_INSTRUCTION_(branch_mif_mnot_mnull);
    //fprintf (stderr, "    instantiating the 0-th argument of branch-if-not-null\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(a)
                                 );
    //fprintf (stderr, "  ...End of the rule nullp-branch-if-false\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking non-nullp-branch-if-true");
//fprintf (stderr, "Trying rule 48 of 66, \"non-nullp-branch-if-true\" (line 1844)\n");
/* Rewrite rule "non-nullp-branch-if-true" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, primitive_mnon_mnullp)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, branch_mif_mtrue)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(1, 0, a)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule non-nullp-branch-if-true (line 1844) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction branch-if-not-null\n");
    JITTER_RULE_APPEND_INSTRUCTION_(branch_mif_mnot_mnull);
    //fprintf (stderr, "    instantiating the 0-th argument of branch-if-not-null\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(a)
                                 );
    //fprintf (stderr, "  ...End of the rule non-nullp-branch-if-true\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking non-nullp-branch-if-false");
//fprintf (stderr, "Trying rule 49 of 66, \"non-nullp-branch-if-false\" (line 1849)\n");
/* Rewrite rule "non-nullp-branch-if-false" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, primitive_mnon_mnullp)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, branch_mif_mfalse)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(1, 0, a)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule non-nullp-branch-if-false (line 1849) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction branch-if-null\n");
    JITTER_RULE_APPEND_INSTRUCTION_(branch_mif_mnull);
    //fprintf (stderr, "    instantiating the 0-th argument of branch-if-null\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(a)
                                 );
    //fprintf (stderr, "  ...End of the rule non-nullp-branch-if-false\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking restore-register-then-save-the-same-register");
//fprintf (stderr, "Trying rule 50 of 66, \"restore-register-then-save-the-same-register\" (line 1869)\n");
/* Rewrite rule "restore-register-then-save-the-same-register" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, restore_mregister)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, save_mregister)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(0, 0, a)
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(1, 0, a)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule restore-register-then-save-the-same-register (line 1869) fires...\n");
    //fprintf (stderr, "  ...End of the rule restore-register-then-save-the-same-register\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking scratch");
//fprintf (stderr, "Trying rule 51 of 66, \"scratch\" (line 1905)\n");
/* Rewrite rule "scratch" */
JITTER_RULE_BEGIN(3)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
    JITTER_RULE_DECLARE_PLACEHOLDER_(b);
    JITTER_RULE_DECLARE_PLACEHOLDER_(c);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, push_mregister)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, primitive_mnon_mzerop)
    JITTER_RULE_CONDITION_MATCH_OPCODE(2, branch_mif_mtrue)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(0, 0, a)
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(1, 0, b)
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(2, 0, c)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
    JITTER_RULE_CLONE_PLACEHOLDER_(b);
    JITTER_RULE_CLONE_PLACEHOLDER_(c);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule scratch (line 1905) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction branch-if-register-non-zero\n");
    JITTER_RULE_APPEND_INSTRUCTION_(branch_mif_mregister_mnon_mzero);
    //fprintf (stderr, "    instantiating the 0-th argument of branch-if-register-non-zero\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(a)
                                 );
    //fprintf (stderr, "    instantiating the 1-th argument of branch-if-register-non-zero\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(c)
                                 );
    //fprintf (stderr, "    instantiating the 2-th argument of branch-if-register-non-zero\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(b)
                                 );
    //fprintf (stderr, "  ...End of the rule scratch\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
    JITTER_RULE_DESTROY_PLACEHOLDER_(b);
    JITTER_RULE_DESTROY_PLACEHOLDER_(c);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking nip-drop-push-literal");
//fprintf (stderr, "Trying rule 52 of 66, \"nip-drop-push-literal\" (line 1928)\n");
/* Rewrite rule "nip-drop-push-literal" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, nip_mdrop)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, push_mliteral)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(1, 0, a)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule nip-drop-push-literal (line 1928) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction nip\n");
    JITTER_RULE_APPEND_INSTRUCTION_(nip);
    //fprintf (stderr, "    rewrite: adding instruction copy-from-literal\n");
    JITTER_RULE_APPEND_INSTRUCTION_(copy_mfrom_mliteral);
    //fprintf (stderr, "    instantiating the 0-th argument of copy-from-literal\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(a)
                                 );
    //fprintf (stderr, "  ...End of the rule nip-drop-push-literal\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking nip-drop-push-register");
//fprintf (stderr, "Trying rule 53 of 66, \"nip-drop-push-register\" (line 1933)\n");
/* Rewrite rule "nip-drop-push-register" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, nip_mdrop)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, push_mregister)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(1, 0, a)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule nip-drop-push-register (line 1933) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction nip\n");
    JITTER_RULE_APPEND_INSTRUCTION_(nip);
    //fprintf (stderr, "    rewrite: adding instruction copy-from-register\n");
    JITTER_RULE_APPEND_INSTRUCTION_(copy_mfrom_mregister);
    //fprintf (stderr, "    instantiating the 0-th argument of copy-from-register\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(a)
                                 );
    //fprintf (stderr, "  ...End of the rule nip-drop-push-register\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking nip-two-drop-push-literal");
//fprintf (stderr, "Trying rule 54 of 66, \"nip-two-drop-push-literal\" (line 1938)\n");
/* Rewrite rule "nip-two-drop-push-literal" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, nip_mtwo_mdrop)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, push_mliteral)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(1, 0, a)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule nip-two-drop-push-literal (line 1938) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction nip-two\n");
    JITTER_RULE_APPEND_INSTRUCTION_(nip_mtwo);
    //fprintf (stderr, "    rewrite: adding instruction copy-from-literal\n");
    JITTER_RULE_APPEND_INSTRUCTION_(copy_mfrom_mliteral);
    //fprintf (stderr, "    instantiating the 0-th argument of copy-from-literal\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(a)
                                 );
    //fprintf (stderr, "  ...End of the rule nip-two-drop-push-literal\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking nip-two-drop-push-register");
//fprintf (stderr, "Trying rule 55 of 66, \"nip-two-drop-push-register\" (line 1943)\n");
/* Rewrite rule "nip-two-drop-push-register" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, nip_mtwo_mdrop)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, push_mregister)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(1, 0, a)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule nip-two-drop-push-register (line 1943) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction nip-two\n");
    JITTER_RULE_APPEND_INSTRUCTION_(nip_mtwo);
    //fprintf (stderr, "    rewrite: adding instruction copy-from-register\n");
    JITTER_RULE_APPEND_INSTRUCTION_(copy_mfrom_mregister);
    //fprintf (stderr, "    instantiating the 0-th argument of copy-from-register\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(a)
                                 );
    //fprintf (stderr, "  ...End of the rule nip-two-drop-push-register\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking nip-three-drop-push-literal");
//fprintf (stderr, "Trying rule 56 of 66, \"nip-three-drop-push-literal\" (line 1948)\n");
/* Rewrite rule "nip-three-drop-push-literal" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, nip_mthree_mdrop)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, push_mliteral)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(1, 0, a)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule nip-three-drop-push-literal (line 1948) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction nip-three\n");
    JITTER_RULE_APPEND_INSTRUCTION_(nip_mthree);
    //fprintf (stderr, "    rewrite: adding instruction copy-from-literal\n");
    JITTER_RULE_APPEND_INSTRUCTION_(copy_mfrom_mliteral);
    //fprintf (stderr, "    instantiating the 0-th argument of copy-from-literal\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(a)
                                 );
    //fprintf (stderr, "  ...End of the rule nip-three-drop-push-literal\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking nip-three-drop-push-register");
//fprintf (stderr, "Trying rule 57 of 66, \"nip-three-drop-push-register\" (line 1953)\n");
/* Rewrite rule "nip-three-drop-push-register" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, nip_mthree_mdrop)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, push_mregister)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(1, 0, a)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule nip-three-drop-push-register (line 1953) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction nip-three\n");
    JITTER_RULE_APPEND_INSTRUCTION_(nip_mthree);
    //fprintf (stderr, "    rewrite: adding instruction copy-from-register\n");
    JITTER_RULE_APPEND_INSTRUCTION_(copy_mfrom_mregister);
    //fprintf (stderr, "    instantiating the 0-th argument of copy-from-register\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(a)
                                 );
    //fprintf (stderr, "  ...End of the rule nip-three-drop-push-register\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking nip-four-drop-push-literal");
//fprintf (stderr, "Trying rule 58 of 66, \"nip-four-drop-push-literal\" (line 1958)\n");
/* Rewrite rule "nip-four-drop-push-literal" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, nip_mfour_mdrop)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, push_mliteral)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(1, 0, a)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule nip-four-drop-push-literal (line 1958) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction nip-four\n");
    JITTER_RULE_APPEND_INSTRUCTION_(nip_mfour);
    //fprintf (stderr, "    rewrite: adding instruction copy-from-literal\n");
    JITTER_RULE_APPEND_INSTRUCTION_(copy_mfrom_mliteral);
    //fprintf (stderr, "    instantiating the 0-th argument of copy-from-literal\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(a)
                                 );
    //fprintf (stderr, "  ...End of the rule nip-four-drop-push-literal\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking nip-four-drop-push-register");
//fprintf (stderr, "Trying rule 59 of 66, \"nip-four-drop-push-register\" (line 1963)\n");
/* Rewrite rule "nip-four-drop-push-register" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, nip_mfour_mdrop)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, push_mregister)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(1, 0, a)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule nip-four-drop-push-register (line 1963) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction nip-four\n");
    JITTER_RULE_APPEND_INSTRUCTION_(nip_mfour);
    //fprintf (stderr, "    rewrite: adding instruction copy-from-register\n");
    JITTER_RULE_APPEND_INSTRUCTION_(copy_mfrom_mregister);
    //fprintf (stderr, "    instantiating the 0-th argument of copy-from-register\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(a)
                                 );
    //fprintf (stderr, "  ...End of the rule nip-four-drop-push-register\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking nip-five-drop-push-literal");
//fprintf (stderr, "Trying rule 60 of 66, \"nip-five-drop-push-literal\" (line 1968)\n");
/* Rewrite rule "nip-five-drop-push-literal" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, nip_mfive_mdrop)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, push_mliteral)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(1, 0, a)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule nip-five-drop-push-literal (line 1968) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction nip-five\n");
    JITTER_RULE_APPEND_INSTRUCTION_(nip_mfive);
    //fprintf (stderr, "    rewrite: adding instruction copy-from-literal\n");
    JITTER_RULE_APPEND_INSTRUCTION_(copy_mfrom_mliteral);
    //fprintf (stderr, "    instantiating the 0-th argument of copy-from-literal\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(a)
                                 );
    //fprintf (stderr, "  ...End of the rule nip-five-drop-push-literal\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking nip-five-drop-push-register");
//fprintf (stderr, "Trying rule 61 of 66, \"nip-five-drop-push-register\" (line 1973)\n");
/* Rewrite rule "nip-five-drop-push-register" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, nip_mfive_mdrop)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, push_mregister)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(1, 0, a)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule nip-five-drop-push-register (line 1973) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction nip-five\n");
    JITTER_RULE_APPEND_INSTRUCTION_(nip_mfive);
    //fprintf (stderr, "    rewrite: adding instruction copy-from-register\n");
    JITTER_RULE_APPEND_INSTRUCTION_(copy_mfrom_mregister);
    //fprintf (stderr, "    instantiating the 0-th argument of copy-from-register\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(a)
                                 );
    //fprintf (stderr, "  ...End of the rule nip-five-drop-push-register\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking nip-six-drop-push-literal");
//fprintf (stderr, "Trying rule 62 of 66, \"nip-six-drop-push-literal\" (line 1978)\n");
/* Rewrite rule "nip-six-drop-push-literal" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, nip_msix_mdrop)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, push_mliteral)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(1, 0, a)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule nip-six-drop-push-literal (line 1978) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction nip-six\n");
    JITTER_RULE_APPEND_INSTRUCTION_(nip_msix);
    //fprintf (stderr, "    rewrite: adding instruction copy-from-literal\n");
    JITTER_RULE_APPEND_INSTRUCTION_(copy_mfrom_mliteral);
    //fprintf (stderr, "    instantiating the 0-th argument of copy-from-literal\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(a)
                                 );
    //fprintf (stderr, "  ...End of the rule nip-six-drop-push-literal\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking nip-six-drop-push-register");
//fprintf (stderr, "Trying rule 63 of 66, \"nip-six-drop-push-register\" (line 1983)\n");
/* Rewrite rule "nip-six-drop-push-register" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, nip_msix_mdrop)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, push_mregister)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(1, 0, a)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule nip-six-drop-push-register (line 1983) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction nip-six\n");
    JITTER_RULE_APPEND_INSTRUCTION_(nip_msix);
    //fprintf (stderr, "    rewrite: adding instruction copy-from-register\n");
    JITTER_RULE_APPEND_INSTRUCTION_(copy_mfrom_mregister);
    //fprintf (stderr, "    instantiating the 0-th argument of copy-from-register\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(a)
                                 );
    //fprintf (stderr, "  ...End of the rule nip-six-drop-push-register\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking unnamed");
//fprintf (stderr, "Trying rule 64 of 66, \"unnamed\" (line 2005)\n");
/* Rewrite rule "unnamed" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, nip)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, push_mliteral)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(1, 0, a)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule unnamed (line 2005) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction nip-push-literal\n");
    JITTER_RULE_APPEND_INSTRUCTION_(nip_mpush_mliteral);
    //fprintf (stderr, "    instantiating the 0-th argument of nip-push-literal\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(a)
                                 );
    //fprintf (stderr, "  ...End of the rule unnamed\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking unnamed");
//fprintf (stderr, "Trying rule 65 of 66, \"unnamed\" (line 2010)\n");
/* Rewrite rule "unnamed" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, nip)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, push_mregister)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(1, 0, a)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule unnamed (line 2010) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction nip-push-register\n");
    JITTER_RULE_APPEND_INSTRUCTION_(nip_mpush_mregister);
    //fprintf (stderr, "    instantiating the 0-th argument of nip-push-register\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(a)
                                 );
    //fprintf (stderr, "  ...End of the rule unnamed\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking useless-cons-elimination");
//fprintf (stderr, "Trying rule 66 of 66, \"useless-cons-elimination\" (line 2021)\n");
/* Rewrite rule "useless-cons-elimination" */
JITTER_RULE_BEGIN(4)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
    JITTER_RULE_DECLARE_PLACEHOLDER_(f);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, heap_mallocate)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, gc_mif_mneeded)
    JITTER_RULE_CONDITION_MATCH_OPCODE(2, primitive_mcons_mspecial)
    JITTER_RULE_CONDITION_MATCH_OPCODE(3, nip_mdrop)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(0, 0, a)
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(1, 0, f)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
    JITTER_RULE_CLONE_PLACEHOLDER_(f);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule useless-cons-elimination (line 2021) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction drop\n");
    JITTER_RULE_APPEND_INSTRUCTION_(drop);
    //fprintf (stderr, "    rewrite: adding instruction drop\n");
    JITTER_RULE_APPEND_INSTRUCTION_(drop);
    //fprintf (stderr, "  ...End of the rule useless-cons-elimination\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
    JITTER_RULE_DESTROY_PLACEHOLDER_(f);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//fprintf (stderr, "No more rules to try\n");
}


//#include <jitter/jitter-fatal.h>

//#include <jitter/jitter.h>
//#include <jitter/jitter-instruction.h>
//#include <jitter/jitter-specialize.h>

//#include "jitterlispvm-vm.h"
//#include "jitterlispvm-meta-instructions.h"
//#include "jitterlispvm-specialized-instructions.h"


/* Recognizer function prototypes. */
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__n1 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__n1___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__n2 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__n2___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__n3 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__n3___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__n4 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__n4___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__n5 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__n5___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__n6 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__n6___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__n7 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__n7___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__n8 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__n8___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__n9 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__n9___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__n10 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__n10___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__nR___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_branch (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_branch__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_branch_mif_mfalse (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_branch_mif_mfalse__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_branch_mif_mnot_mless (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_branch_mif_mnot_mless__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_branch_mif_mnot_mless__fR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_branch_mif_mnot_mnull (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_branch_mif_mnot_mnull__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_branch_mif_mnull (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_branch_mif_mnull__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_branch_mif_mregister_mnon_mzero (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_branch_mif_mregister_mnon_mzero___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_branch_mif_mregister_mnon_mzero___rrR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_branch_mif_mregister_mnon_mzero___rrR__fR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_branch_mif_mtrue (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_branch_mif_mtrue__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_call (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_call__n0 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_call__n1 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_call__n2 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_call__n3 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_call__n4 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_call__n5 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_call__n6 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_call__n7 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_call__n8 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_call__n9 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_call__n10 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_call__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_call_mcompiled (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_call_mcompiled__n0 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_call_mcompiled__n1 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_call_mcompiled__n2 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_call_mcompiled__n3 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_call_mcompiled__n4 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_call_mcompiled__n5 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_call_mcompiled__n6 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_call_mcompiled__n7 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_call_mcompiled__n8 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_call_mcompiled__n9 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_call_mcompiled__n10 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_call_mcompiled__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_call_mfrom_mc (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_canonicalize_mboolean (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_mclosure (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_mclosure__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_mglobal_mdefined (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_mglobal_mdefined__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_mglobal_mdefined__nR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity__n0 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity__n0__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity__n1 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity__n1__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity__n2 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity__n2__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity__n3 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity__n3__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity__n4 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity__n4__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity__n5 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity__n5__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity__n6 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity__n6__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity__n7 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity__n7__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity__n8 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity__n8__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity__n9 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity__n9__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity__n10 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity__n10__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity__nR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n0 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n0__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n1 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n1__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n2 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n2__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n3 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n3__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n4 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n4__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n5 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n5__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n6 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n6__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n7 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n7__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n8 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n8__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n9 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n9__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n10 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n10__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__nR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_copy_mfrom_mliteral (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_copy_mfrom_mliteral__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_copy_mfrom_mregister (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_copy_mfrom_mregister___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_copy_mto_mregister (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_copy_mto_mregister___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_drop (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_drop_mnip (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_dup (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_exitvm (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_fail (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_gc_mif_mneeded (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_gc_mif_mneeded__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_heap_mallocate (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_heap_mallocate__n4 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_heap_mallocate__n8 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_heap_mallocate__n12 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_heap_mallocate__n16 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_heap_mallocate__n24 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_heap_mallocate__n32 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_heap_mallocate__n36 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_heap_mallocate__n48 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_heap_mallocate__n52 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_heap_mallocate__n64 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_heap_mallocate__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_literal_mto_mregister (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_literal_mto_mregister__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_literal_mto_mregister__nR___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_nip (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_nip_mdrop (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_nip_mfive (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_nip_mfive_mdrop (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_nip_mfour (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_nip_mfour_mdrop (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_nip_mpush_mliteral (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_nip_mpush_mliteral__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_nip_mpush_mregister (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_nip_mpush_mregister___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_nip_msix (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_nip_msix_mdrop (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_nip_mthree (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_nip_mthree_mdrop (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_nip_mtwo (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_nip_mtwo_mdrop (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_nop (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_pop_mto_mglobal (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_pop_mto_mglobal__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_pop_mto_mglobal__nR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_pop_mto_mglobal_mdefined (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_pop_mto_mglobal_mdefined__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_pop_mto_mglobal_mdefined__nR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_pop_mto_mregister (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_pop_mto_mregister___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive__nR__n0 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive__nR__n0__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive__nR__n1 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive__nR__n1__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive__nR__n2 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive__nR__n2__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive__nR__n3 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive__nR__n3__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive__nR__n4 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive__nR__n4__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive__nR__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive__nR__nR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mboolean_mcanonicalize (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mbox (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mbox_mget (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mbox_mget__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mbox_msetb_mspecial (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mbox_msetb_mspecial__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mcar (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mcar__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mcdr (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mcdr__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mcharacterp (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mcons_mspecial (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mconsp (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_meqp (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mfixnum_meqp (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mfixnum_meqp__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mfixnum_mnot_meqp (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mfixnum_mnot_meqp__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mfixnump (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mgreaterp (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mgreaterp__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mlessp (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mlessp__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mnegate (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mnegate__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mnegativep (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mnegativep__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mnon_mconsp (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mnon_mnegativep (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mnon_mnegativep__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mnon_mnullp (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mnon_mpositivep (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mnon_mpositivep__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mnon_msymbolp (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mnon_mzerop (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mnon_mzerop__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mnot (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mnot_meqp (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mnot_mgreaterp (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mnot_mgreaterp__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mnot_mlessp (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mnot_mlessp__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mnothingp (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mnullp (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mone_mminus (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mone_mminus__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mone_mplus (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mone_mplus__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mpositivep (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mpositivep__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mprimordial_mdivided (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mprimordial_mdivided__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mprimordial_mdivided_munsafe (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mprimordial_mdivided_munsafe__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mprimordial_mminus (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mprimordial_mminus__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mprimordial_mplus (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mprimordial_mplus__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mprimordial_mtimes (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mprimordial_mtimes__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mquotient (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mquotient__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mquotient_munsafe (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mquotient_munsafe__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mremainder (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mremainder__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mremainder_munsafe (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mremainder_munsafe__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mset_mcarb_mspecial (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mset_mcarb_mspecial__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mset_mcdrb_mspecial (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mset_mcdrb_mspecial__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_msymbolp (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mtwo_mdivided (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mtwo_mdivided__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mtwo_mquotient (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mtwo_mquotient__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mtwo_mremainder (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mtwo_mremainder__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mtwo_mtimes (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mtwo_mtimes__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_muniquep (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mzerop (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mzerop__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_procedure_mprolog (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_push_mfalse (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_push_mglobal (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_push_mglobal__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_push_mglobal__nR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_push_mliteral (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_push_mliteral__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_push_mnil (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_push_mnothing (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_push_mone (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_push_mregister (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_push_mregister___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_push_munspecified (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_push_mzero (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_register_mto_mregister (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_register_mto_mregister___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_register_mto_mregister___rrR___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_restore_mregister (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_restore_mregister___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_return (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_save_mregister (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_save_mregister___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_tail_mcall (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_tail_mcall__n0 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_tail_mcall__n1 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_tail_mcall__n2 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_tail_mcall__n3 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_tail_mcall__n4 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_tail_mcall__n5 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_tail_mcall__n6 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_tail_mcall__n7 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_tail_mcall__n8 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_tail_mcall__n9 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_tail_mcall__n10 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_tail_mcall__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_tail_mcall_mcompiled (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_tail_mcall_mcompiled__n0 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_tail_mcall_mcompiled__n1 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_tail_mcall_mcompiled__n2 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_tail_mcall_mcompiled__n3 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_tail_mcall_mcompiled__n4 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_tail_mcall_mcompiled__n5 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_tail_mcall_mcompiled__n6 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_tail_mcall_mcompiled__n7 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_tail_mcall_mcompiled__n8 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_tail_mcall_mcompiled__n9 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_tail_mcall_mcompiled__n10 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_tail_mcall_mcompiled__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_unreachable (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));


/* Recognizer function definitions. */
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 1 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__n1 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 2 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__n2 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 3 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__n3 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 4 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__n4 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 5 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__n5 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 6 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__n6 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 7 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__n7 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 8 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__n8 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 9 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__n9 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 10 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__n10 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__n1 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__n1___rrR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__n1___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_at_mdepth_mto_mregister__n1___rrR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__n2 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__n2___rrR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__n2___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_at_mdepth_mto_mregister__n2___rrR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__n3 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__n3___rrR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__n3___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_at_mdepth_mto_mregister__n3___rrR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__n4 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__n4___rrR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__n4___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_at_mdepth_mto_mregister__n4___rrR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__n5 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__n5___rrR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__n5___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_at_mdepth_mto_mregister__n5___rrR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__n6 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__n6___rrR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__n6___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_at_mdepth_mto_mregister__n6___rrR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__n7 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__n7___rrR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__n7___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_at_mdepth_mto_mregister__n7___rrR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__n8 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__n8___rrR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__n8___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_at_mdepth_mto_mregister__n8___rrR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__n9 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__n9___rrR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__n9___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_at_mdepth_mto_mregister__n9___rrR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__n10 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__n10___rrR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__n10___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_at_mdepth_mto_mregister__n10___rrR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__nR___rrR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__nR___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_at_mdepth_mto_mregister__nR___rrR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_branch (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_branch__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_branch__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_branch__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_branch_mif_mfalse (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_branch_mif_mfalse__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_branch_mif_mfalse__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_branch_mif_mfalse__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_branch_mif_mnot_mless (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_branch_mif_mnot_mless__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_branch_mif_mnot_mless__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_branch_mif_mnot_mless__fR__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_branch_mif_mnot_mless__fR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_branch_mif_mnot_mless__fR__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_branch_mif_mnot_mnull (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_branch_mif_mnot_mnull__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_branch_mif_mnot_mnull__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_branch_mif_mnot_mnull__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_branch_mif_mnull (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_branch_mif_mnull__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_branch_mif_mnull__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_branch_mif_mnull__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_branch_mif_mregister_mnon_mzero (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = jitterlispvm_recognize_specialized_instruction_branch_mif_mregister_mnon_mzero___rrR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_branch_mif_mregister_mnon_mzero___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_branch_mif_mregister_mnon_mzero___rrR__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_branch_mif_mregister_mnon_mzero___rrR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_branch_mif_mregister_mnon_mzero___rrR__fR__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_branch_mif_mregister_mnon_mzero___rrR__fR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_branch_mif_mregister_mnon_mzero___rrR__fR__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_branch_mif_mtrue (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_branch_mif_mtrue__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_branch_mif_mtrue__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_branch_mif_mtrue__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_call (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 0 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_call__n0 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 1 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_call__n1 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 2 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_call__n2 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 3 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_call__n3 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 4 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_call__n4 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 5 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_call__n5 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 6 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_call__n6 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 7 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_call__n7 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 8 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_call__n8 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 9 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_call__n9 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 10 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_call__n10 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = jitterlispvm_recognize_specialized_instruction_call__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_call__n0 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_call__n0__retR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_call__n1 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_call__n1__retR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_call__n2 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_call__n2__retR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_call__n3 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_call__n3__retR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_call__n4 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_call__n4__retR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_call__n5 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_call__n5__retR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_call__n6 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_call__n6__retR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_call__n7 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_call__n7__retR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_call__n8 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_call__n8__retR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_call__n9 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_call__n9__retR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_call__n10 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_call__n10__retR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_call__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_call__nR__retR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_call_mcompiled (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 0 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_call_mcompiled__n0 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 1 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_call_mcompiled__n1 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 2 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_call_mcompiled__n2 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 3 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_call_mcompiled__n3 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 4 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_call_mcompiled__n4 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 5 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_call_mcompiled__n5 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 6 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_call_mcompiled__n6 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 7 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_call_mcompiled__n7 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 8 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_call_mcompiled__n8 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 9 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_call_mcompiled__n9 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 10 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_call_mcompiled__n10 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = jitterlispvm_recognize_specialized_instruction_call_mcompiled__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_call_mcompiled__n0 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_call_mcompiled__n0__retR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_call_mcompiled__n1 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_call_mcompiled__n1__retR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_call_mcompiled__n2 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_call_mcompiled__n2__retR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_call_mcompiled__n3 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_call_mcompiled__n3__retR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_call_mcompiled__n4 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_call_mcompiled__n4__retR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_call_mcompiled__n5 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_call_mcompiled__n5__retR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_call_mcompiled__n6 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_call_mcompiled__n6__retR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_call_mcompiled__n7 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_call_mcompiled__n7__retR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_call_mcompiled__n8 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_call_mcompiled__n8__retR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_call_mcompiled__n9 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_call_mcompiled__n9__retR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_call_mcompiled__n10 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_call_mcompiled__n10__retR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_call_mcompiled__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_call_mcompiled__nR__retR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_call_mfrom_mc (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_call_mfrom_mc__retR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_canonicalize_mboolean (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_canonicalize_mboolean;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_mclosure (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_check_mclosure__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_mclosure__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_check_mclosure__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_mglobal_mdefined (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = jitterlispvm_recognize_specialized_instruction_check_mglobal_mdefined__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_mglobal_mdefined__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_check_mglobal_mdefined__nR__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_mglobal_mdefined__nR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_check_mglobal_mdefined__nR__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 0 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_check_min_marity__n0 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 1 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_check_min_marity__n1 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 2 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_check_min_marity__n2 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 3 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_check_min_marity__n3 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 4 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_check_min_marity__n4 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 5 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_check_min_marity__n5 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 6 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_check_min_marity__n6 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 7 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_check_min_marity__n7 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 8 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_check_min_marity__n8 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 9 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_check_min_marity__n9 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 10 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_check_min_marity__n10 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = jitterlispvm_recognize_specialized_instruction_check_min_marity__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity__n0 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_check_min_marity__n0__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity__n0__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_check_min_marity__n0__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity__n1 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_check_min_marity__n1__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity__n1__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_check_min_marity__n1__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity__n2 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_check_min_marity__n2__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity__n2__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_check_min_marity__n2__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity__n3 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_check_min_marity__n3__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity__n3__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_check_min_marity__n3__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity__n4 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_check_min_marity__n4__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity__n4__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_check_min_marity__n4__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity__n5 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_check_min_marity__n5__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity__n5__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_check_min_marity__n5__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity__n6 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_check_min_marity__n6__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity__n6__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_check_min_marity__n6__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity__n7 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_check_min_marity__n7__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity__n7__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_check_min_marity__n7__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity__n8 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_check_min_marity__n8__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity__n8__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_check_min_marity__n8__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity__n9 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_check_min_marity__n9__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity__n9__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_check_min_marity__n9__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity__n10 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_check_min_marity__n10__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity__n10__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_check_min_marity__n10__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_check_min_marity__nR__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity__nR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_check_min_marity__nR__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 0 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n0 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 1 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n1 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 2 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n2 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 3 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n3 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 4 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n4 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 5 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n5 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 6 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n6 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 7 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n7 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 8 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n8 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 9 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n9 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 10 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n10 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n0 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n0__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n0__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_check_min_marity_m_malt__n0__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n1 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n1__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n1__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_check_min_marity_m_malt__n1__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n2 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n2__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n2__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_check_min_marity_m_malt__n2__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n3 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n3__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n3__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_check_min_marity_m_malt__n3__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n4 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n4__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n4__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_check_min_marity_m_malt__n4__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n5 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n5__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n5__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_check_min_marity_m_malt__n5__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n6 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n6__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n6__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_check_min_marity_m_malt__n6__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n7 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n7__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n7__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_check_min_marity_m_malt__n7__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n8 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n8__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n8__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_check_min_marity_m_malt__n8__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n9 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n9__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n9__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_check_min_marity_m_malt__n9__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n10 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n10__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n10__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_check_min_marity_m_malt__n10__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__nR__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__nR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_check_min_marity_m_malt__nR__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_copy_mfrom_mliteral (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = jitterlispvm_recognize_specialized_instruction_copy_mfrom_mliteral__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_copy_mfrom_mliteral__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_copy_mfrom_mliteral__nR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_copy_mfrom_mregister (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = jitterlispvm_recognize_specialized_instruction_copy_mfrom_mregister___rrR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_copy_mfrom_mregister___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_copy_mfrom_mregister___rrR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_copy_mto_mregister (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = jitterlispvm_recognize_specialized_instruction_copy_mto_mregister___rrR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_copy_mto_mregister___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_copy_mto_mregister___rrR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_drop (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_drop;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_drop_mnip (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_drop_mnip;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_dup (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_dup;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_exitvm (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_exitvm;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_fail (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_fail__retR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_gc_mif_mneeded (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_gc_mif_mneeded__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_gc_mif_mneeded__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_gc_mif_mneeded__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_heap_mallocate (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 4 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_heap_mallocate__n4 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 8 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_heap_mallocate__n8 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 12 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_heap_mallocate__n12 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 16 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_heap_mallocate__n16 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 24 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_heap_mallocate__n24 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 32 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_heap_mallocate__n32 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 36 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_heap_mallocate__n36 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 48 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_heap_mallocate__n48 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 52 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_heap_mallocate__n52 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 64 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_heap_mallocate__n64 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = jitterlispvm_recognize_specialized_instruction_heap_mallocate__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_heap_mallocate__n4 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_heap_mallocate__n4;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_heap_mallocate__n8 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_heap_mallocate__n8;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_heap_mallocate__n12 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_heap_mallocate__n12;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_heap_mallocate__n16 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_heap_mallocate__n16;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_heap_mallocate__n24 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_heap_mallocate__n24;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_heap_mallocate__n32 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_heap_mallocate__n32;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_heap_mallocate__n36 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_heap_mallocate__n36;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_heap_mallocate__n48 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_heap_mallocate__n48;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_heap_mallocate__n52 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_heap_mallocate__n52;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_heap_mallocate__n64 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_heap_mallocate__n64;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_heap_mallocate__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_heap_mallocate__nR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_literal_mto_mregister (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = jitterlispvm_recognize_specialized_instruction_literal_mto_mregister__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_literal_mto_mregister__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = jitterlispvm_recognize_specialized_instruction_literal_mto_mregister__nR___rrR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_literal_mto_mregister__nR___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_literal_mto_mregister__nR___rrR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_nip (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_nip;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_nip_mdrop (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_nip_mdrop;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_nip_mfive (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_nip_mfive;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_nip_mfive_mdrop (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_nip_mfive_mdrop;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_nip_mfour (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_nip_mfour;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_nip_mfour_mdrop (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_nip_mfour_mdrop;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_nip_mpush_mliteral (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = jitterlispvm_recognize_specialized_instruction_nip_mpush_mliteral__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_nip_mpush_mliteral__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_nip_mpush_mliteral__nR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_nip_mpush_mregister (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = jitterlispvm_recognize_specialized_instruction_nip_mpush_mregister___rrR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_nip_mpush_mregister___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_nip_mpush_mregister___rrR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_nip_msix (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_nip_msix;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_nip_msix_mdrop (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_nip_msix_mdrop;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_nip_mthree (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_nip_mthree;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_nip_mthree_mdrop (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_nip_mthree_mdrop;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_nip_mtwo (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_nip_mtwo;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_nip_mtwo_mdrop (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_nip_mtwo_mdrop;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_nop (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_nop;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_pop_mto_mglobal (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = jitterlispvm_recognize_specialized_instruction_pop_mto_mglobal__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_pop_mto_mglobal__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_pop_mto_mglobal__nR__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_pop_mto_mglobal__nR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_pop_mto_mglobal__nR__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_pop_mto_mglobal_mdefined (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = jitterlispvm_recognize_specialized_instruction_pop_mto_mglobal_mdefined__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_pop_mto_mglobal_mdefined__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_pop_mto_mglobal_mdefined__nR__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_pop_mto_mglobal_mdefined__nR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_pop_mto_mglobal_mdefined__nR__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_pop_mto_mregister (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = jitterlispvm_recognize_specialized_instruction_pop_mto_mregister___rrR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_pop_mto_mregister___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_pop_mto_mregister___rrR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = jitterlispvm_recognize_specialized_instruction_primitive__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 0 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_primitive__nR__n0 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 1 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_primitive__nR__n1 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 2 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_primitive__nR__n2 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 3 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_primitive__nR__n3 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 4 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_primitive__nR__n4 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = jitterlispvm_recognize_specialized_instruction_primitive__nR__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive__nR__n0 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_primitive__nR__n0__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive__nR__n0__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_primitive__nR__n0__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive__nR__n1 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_primitive__nR__n1__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive__nR__n1__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_primitive__nR__n1__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive__nR__n2 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_primitive__nR__n2__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive__nR__n2__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_primitive__nR__n2__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive__nR__n3 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_primitive__nR__n3__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive__nR__n3__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_primitive__nR__n3__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive__nR__n4 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_primitive__nR__n4__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive__nR__n4__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_primitive__nR__n4__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive__nR__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_primitive__nR__nR__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive__nR__nR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_primitive__nR__nR__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mboolean_mcanonicalize (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_primitive_mboolean_mcanonicalize;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mbox (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_primitive_mbox;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mbox_mget (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_primitive_mbox_mget__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mbox_mget__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_primitive_mbox_mget__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mbox_msetb_mspecial (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_primitive_mbox_msetb_mspecial__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mbox_msetb_mspecial__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_primitive_mbox_msetb_mspecial__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mcar (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_primitive_mcar__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mcar__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_primitive_mcar__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mcdr (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_primitive_mcdr__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mcdr__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_primitive_mcdr__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mcharacterp (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_primitive_mcharacterp;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mcons_mspecial (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_primitive_mcons_mspecial;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mconsp (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_primitive_mconsp;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_meqp (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_primitive_meqp;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mfixnum_meqp (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_primitive_mfixnum_meqp__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mfixnum_meqp__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_primitive_mfixnum_meqp__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mfixnum_mnot_meqp (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_primitive_mfixnum_mnot_meqp__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mfixnum_mnot_meqp__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_primitive_mfixnum_mnot_meqp__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mfixnump (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_primitive_mfixnump;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mgreaterp (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_primitive_mgreaterp__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mgreaterp__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_primitive_mgreaterp__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mlessp (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_primitive_mlessp__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mlessp__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_primitive_mlessp__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mnegate (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_primitive_mnegate__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mnegate__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_primitive_mnegate__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mnegativep (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_primitive_mnegativep__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mnegativep__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_primitive_mnegativep__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mnon_mconsp (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_primitive_mnon_mconsp;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mnon_mnegativep (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_primitive_mnon_mnegativep__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mnon_mnegativep__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_primitive_mnon_mnegativep__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mnon_mnullp (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_primitive_mnon_mnullp;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mnon_mpositivep (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_primitive_mnon_mpositivep__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mnon_mpositivep__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_primitive_mnon_mpositivep__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mnon_msymbolp (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_primitive_mnon_msymbolp;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mnon_mzerop (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_primitive_mnon_mzerop__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mnon_mzerop__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_primitive_mnon_mzerop__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mnot (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_primitive_mnot;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mnot_meqp (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_primitive_mnot_meqp;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mnot_mgreaterp (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_primitive_mnot_mgreaterp__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mnot_mgreaterp__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_primitive_mnot_mgreaterp__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mnot_mlessp (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_primitive_mnot_mlessp__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mnot_mlessp__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_primitive_mnot_mlessp__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mnothingp (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_primitive_mnothingp;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mnullp (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_primitive_mnullp;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mone_mminus (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_primitive_mone_mminus__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mone_mminus__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_primitive_mone_mminus__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mone_mplus (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_primitive_mone_mplus__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mone_mplus__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_primitive_mone_mplus__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mpositivep (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_primitive_mpositivep__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mpositivep__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_primitive_mpositivep__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mprimordial_mdivided (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_primitive_mprimordial_mdivided__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mprimordial_mdivided__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_primitive_mprimordial_mdivided__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mprimordial_mdivided_munsafe (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_primitive_mprimordial_mdivided_munsafe__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mprimordial_mdivided_munsafe__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_primitive_mprimordial_mdivided_munsafe__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mprimordial_mminus (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_primitive_mprimordial_mminus__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mprimordial_mminus__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_primitive_mprimordial_mminus__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mprimordial_mplus (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_primitive_mprimordial_mplus__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mprimordial_mplus__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_primitive_mprimordial_mplus__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mprimordial_mtimes (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_primitive_mprimordial_mtimes__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mprimordial_mtimes__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_primitive_mprimordial_mtimes__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mquotient (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_primitive_mquotient__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mquotient__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_primitive_mquotient__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mquotient_munsafe (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_primitive_mquotient_munsafe__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mquotient_munsafe__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_primitive_mquotient_munsafe__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mremainder (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_primitive_mremainder__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mremainder__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_primitive_mremainder__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mremainder_munsafe (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_primitive_mremainder_munsafe__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mremainder_munsafe__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_primitive_mremainder_munsafe__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mset_mcarb_mspecial (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_primitive_mset_mcarb_mspecial__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mset_mcarb_mspecial__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_primitive_mset_mcarb_mspecial__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mset_mcdrb_mspecial (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_primitive_mset_mcdrb_mspecial__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mset_mcdrb_mspecial__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_primitive_mset_mcdrb_mspecial__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_msymbolp (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_primitive_msymbolp;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mtwo_mdivided (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_primitive_mtwo_mdivided__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mtwo_mdivided__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_primitive_mtwo_mdivided__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mtwo_mquotient (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_primitive_mtwo_mquotient__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mtwo_mquotient__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_primitive_mtwo_mquotient__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mtwo_mremainder (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_primitive_mtwo_mremainder__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mtwo_mremainder__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_primitive_mtwo_mremainder__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mtwo_mtimes (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_primitive_mtwo_mtimes__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mtwo_mtimes__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_primitive_mtwo_mtimes__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_muniquep (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_primitive_muniquep;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mzerop (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_primitive_mzerop__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mzerop__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_primitive_mzerop__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_procedure_mprolog (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_procedure_mprolog;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_push_mfalse (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_push_mfalse;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_push_mglobal (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = jitterlispvm_recognize_specialized_instruction_push_mglobal__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_push_mglobal__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_push_mglobal__nR__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_push_mglobal__nR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_push_mglobal__nR__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_push_mliteral (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = jitterlispvm_recognize_specialized_instruction_push_mliteral__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_push_mliteral__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_push_mliteral__nR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_push_mnil (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_push_mnil;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_push_mnothing (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_push_mnothing;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_push_mone (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_push_mone;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_push_mregister (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = jitterlispvm_recognize_specialized_instruction_push_mregister___rrR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_push_mregister___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_push_mregister___rrR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_push_munspecified (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_push_munspecified;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_push_mzero (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_push_mzero;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_register_mto_mregister (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = jitterlispvm_recognize_specialized_instruction_register_mto_mregister___rrR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_register_mto_mregister___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = jitterlispvm_recognize_specialized_instruction_register_mto_mregister___rrR___rrR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_register_mto_mregister___rrR___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_register_mto_mregister___rrR___rrR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_restore_mregister (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = jitterlispvm_recognize_specialized_instruction_restore_mregister___rrR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_restore_mregister___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_restore_mregister___rrR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_return (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_return;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_save_mregister (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = jitterlispvm_recognize_specialized_instruction_save_mregister___rrR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_save_mregister___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_save_mregister___rrR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_tail_mcall (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 0 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_tail_mcall__n0 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 1 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_tail_mcall__n1 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 2 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_tail_mcall__n2 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 3 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_tail_mcall__n3 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 4 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_tail_mcall__n4 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 5 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_tail_mcall__n5 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 6 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_tail_mcall__n6 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 7 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_tail_mcall__n7 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 8 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_tail_mcall__n8 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 9 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_tail_mcall__n9 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 10 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_tail_mcall__n10 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = jitterlispvm_recognize_specialized_instruction_tail_mcall__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_tail_mcall__n0 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_tail_mcall__n0;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_tail_mcall__n1 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_tail_mcall__n1;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_tail_mcall__n2 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_tail_mcall__n2;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_tail_mcall__n3 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_tail_mcall__n3;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_tail_mcall__n4 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_tail_mcall__n4;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_tail_mcall__n5 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_tail_mcall__n5;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_tail_mcall__n6 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_tail_mcall__n6;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_tail_mcall__n7 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_tail_mcall__n7;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_tail_mcall__n8 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_tail_mcall__n8;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_tail_mcall__n9 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_tail_mcall__n9;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_tail_mcall__n10 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_tail_mcall__n10;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_tail_mcall__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_tail_mcall__nR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_tail_mcall_mcompiled (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 0 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_tail_mcall_mcompiled__n0 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 1 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_tail_mcall_mcompiled__n1 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 2 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_tail_mcall_mcompiled__n2 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 3 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_tail_mcall_mcompiled__n3 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 4 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_tail_mcall_mcompiled__n4 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 5 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_tail_mcall_mcompiled__n5 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 6 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_tail_mcall_mcompiled__n6 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 7 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_tail_mcall_mcompiled__n7 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 8 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_tail_mcall_mcompiled__n8 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 9 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_tail_mcall_mcompiled__n9 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 10 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_tail_mcall_mcompiled__n10 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = jitterlispvm_recognize_specialized_instruction_tail_mcall_mcompiled__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_tail_mcall_mcompiled__n0 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_tail_mcall_mcompiled__n0;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_tail_mcall_mcompiled__n1 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_tail_mcall_mcompiled__n1;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_tail_mcall_mcompiled__n2 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_tail_mcall_mcompiled__n2;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_tail_mcall_mcompiled__n3 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_tail_mcall_mcompiled__n3;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_tail_mcall_mcompiled__n4 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_tail_mcall_mcompiled__n4;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_tail_mcall_mcompiled__n5 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_tail_mcall_mcompiled__n5;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_tail_mcall_mcompiled__n6 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_tail_mcall_mcompiled__n6;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_tail_mcall_mcompiled__n7 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_tail_mcall_mcompiled__n7;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_tail_mcall_mcompiled__n8 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_tail_mcall_mcompiled__n8;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_tail_mcall_mcompiled__n9 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_tail_mcall_mcompiled__n9;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_tail_mcall_mcompiled__n10 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_tail_mcall_mcompiled__n10;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_tail_mcall_mcompiled__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_tail_mcall_mcompiled__nR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_unreachable (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_unreachable;
}



/* Recognizer entry point. */
static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction (struct jitter_mutable_routine *p,
                                            const struct jitter_instruction *ins)
{
  bool fl = ! p->options.slow_literals_only;
  const struct jitter_meta_instruction *mi = ins->meta_instruction;
  switch (mi->id)
    {
    case jitterlispvm_meta_instruction_id_at_mdepth_mto_mregister:
      return jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_branch:
      return jitterlispvm_recognize_specialized_instruction_branch (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_branch_mif_mfalse:
      return jitterlispvm_recognize_specialized_instruction_branch_mif_mfalse (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_branch_mif_mnot_mless:
      return jitterlispvm_recognize_specialized_instruction_branch_mif_mnot_mless (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_branch_mif_mnot_mnull:
      return jitterlispvm_recognize_specialized_instruction_branch_mif_mnot_mnull (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_branch_mif_mnull:
      return jitterlispvm_recognize_specialized_instruction_branch_mif_mnull (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_branch_mif_mregister_mnon_mzero:
      return jitterlispvm_recognize_specialized_instruction_branch_mif_mregister_mnon_mzero (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_branch_mif_mtrue:
      return jitterlispvm_recognize_specialized_instruction_branch_mif_mtrue (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_call:
      return jitterlispvm_recognize_specialized_instruction_call (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_call_mcompiled:
      return jitterlispvm_recognize_specialized_instruction_call_mcompiled (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_call_mfrom_mc:
      return jitterlispvm_recognize_specialized_instruction_call_mfrom_mc (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_canonicalize_mboolean:
      return jitterlispvm_recognize_specialized_instruction_canonicalize_mboolean (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_check_mclosure:
      return jitterlispvm_recognize_specialized_instruction_check_mclosure (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_check_mglobal_mdefined:
      return jitterlispvm_recognize_specialized_instruction_check_mglobal_mdefined (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_check_min_marity:
      return jitterlispvm_recognize_specialized_instruction_check_min_marity (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_check_min_marity_m_malt:
      return jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_copy_mfrom_mliteral:
      return jitterlispvm_recognize_specialized_instruction_copy_mfrom_mliteral (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_copy_mfrom_mregister:
      return jitterlispvm_recognize_specialized_instruction_copy_mfrom_mregister (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_copy_mto_mregister:
      return jitterlispvm_recognize_specialized_instruction_copy_mto_mregister (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_drop:
      return jitterlispvm_recognize_specialized_instruction_drop (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_drop_mnip:
      return jitterlispvm_recognize_specialized_instruction_drop_mnip (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_dup:
      return jitterlispvm_recognize_specialized_instruction_dup (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_exitvm:
      return jitterlispvm_recognize_specialized_instruction_exitvm (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_fail:
      return jitterlispvm_recognize_specialized_instruction_fail (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_gc_mif_mneeded:
      return jitterlispvm_recognize_specialized_instruction_gc_mif_mneeded (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_heap_mallocate:
      return jitterlispvm_recognize_specialized_instruction_heap_mallocate (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_literal_mto_mregister:
      return jitterlispvm_recognize_specialized_instruction_literal_mto_mregister (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_nip:
      return jitterlispvm_recognize_specialized_instruction_nip (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_nip_mdrop:
      return jitterlispvm_recognize_specialized_instruction_nip_mdrop (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_nip_mfive:
      return jitterlispvm_recognize_specialized_instruction_nip_mfive (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_nip_mfive_mdrop:
      return jitterlispvm_recognize_specialized_instruction_nip_mfive_mdrop (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_nip_mfour:
      return jitterlispvm_recognize_specialized_instruction_nip_mfour (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_nip_mfour_mdrop:
      return jitterlispvm_recognize_specialized_instruction_nip_mfour_mdrop (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_nip_mpush_mliteral:
      return jitterlispvm_recognize_specialized_instruction_nip_mpush_mliteral (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_nip_mpush_mregister:
      return jitterlispvm_recognize_specialized_instruction_nip_mpush_mregister (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_nip_msix:
      return jitterlispvm_recognize_specialized_instruction_nip_msix (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_nip_msix_mdrop:
      return jitterlispvm_recognize_specialized_instruction_nip_msix_mdrop (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_nip_mthree:
      return jitterlispvm_recognize_specialized_instruction_nip_mthree (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_nip_mthree_mdrop:
      return jitterlispvm_recognize_specialized_instruction_nip_mthree_mdrop (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_nip_mtwo:
      return jitterlispvm_recognize_specialized_instruction_nip_mtwo (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_nip_mtwo_mdrop:
      return jitterlispvm_recognize_specialized_instruction_nip_mtwo_mdrop (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_nop:
      return jitterlispvm_recognize_specialized_instruction_nop (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_pop_mto_mglobal:
      return jitterlispvm_recognize_specialized_instruction_pop_mto_mglobal (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_pop_mto_mglobal_mdefined:
      return jitterlispvm_recognize_specialized_instruction_pop_mto_mglobal_mdefined (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_pop_mto_mregister:
      return jitterlispvm_recognize_specialized_instruction_pop_mto_mregister (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_primitive:
      return jitterlispvm_recognize_specialized_instruction_primitive (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_primitive_mboolean_mcanonicalize:
      return jitterlispvm_recognize_specialized_instruction_primitive_mboolean_mcanonicalize (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_primitive_mbox:
      return jitterlispvm_recognize_specialized_instruction_primitive_mbox (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_primitive_mbox_mget:
      return jitterlispvm_recognize_specialized_instruction_primitive_mbox_mget (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_primitive_mbox_msetb_mspecial:
      return jitterlispvm_recognize_specialized_instruction_primitive_mbox_msetb_mspecial (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_primitive_mcar:
      return jitterlispvm_recognize_specialized_instruction_primitive_mcar (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_primitive_mcdr:
      return jitterlispvm_recognize_specialized_instruction_primitive_mcdr (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_primitive_mcharacterp:
      return jitterlispvm_recognize_specialized_instruction_primitive_mcharacterp (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_primitive_mcons_mspecial:
      return jitterlispvm_recognize_specialized_instruction_primitive_mcons_mspecial (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_primitive_mconsp:
      return jitterlispvm_recognize_specialized_instruction_primitive_mconsp (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_primitive_meqp:
      return jitterlispvm_recognize_specialized_instruction_primitive_meqp (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_primitive_mfixnum_meqp:
      return jitterlispvm_recognize_specialized_instruction_primitive_mfixnum_meqp (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_primitive_mfixnum_mnot_meqp:
      return jitterlispvm_recognize_specialized_instruction_primitive_mfixnum_mnot_meqp (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_primitive_mfixnump:
      return jitterlispvm_recognize_specialized_instruction_primitive_mfixnump (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_primitive_mgreaterp:
      return jitterlispvm_recognize_specialized_instruction_primitive_mgreaterp (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_primitive_mlessp:
      return jitterlispvm_recognize_specialized_instruction_primitive_mlessp (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_primitive_mnegate:
      return jitterlispvm_recognize_specialized_instruction_primitive_mnegate (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_primitive_mnegativep:
      return jitterlispvm_recognize_specialized_instruction_primitive_mnegativep (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_primitive_mnon_mconsp:
      return jitterlispvm_recognize_specialized_instruction_primitive_mnon_mconsp (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_primitive_mnon_mnegativep:
      return jitterlispvm_recognize_specialized_instruction_primitive_mnon_mnegativep (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_primitive_mnon_mnullp:
      return jitterlispvm_recognize_specialized_instruction_primitive_mnon_mnullp (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_primitive_mnon_mpositivep:
      return jitterlispvm_recognize_specialized_instruction_primitive_mnon_mpositivep (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_primitive_mnon_msymbolp:
      return jitterlispvm_recognize_specialized_instruction_primitive_mnon_msymbolp (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_primitive_mnon_mzerop:
      return jitterlispvm_recognize_specialized_instruction_primitive_mnon_mzerop (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_primitive_mnot:
      return jitterlispvm_recognize_specialized_instruction_primitive_mnot (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_primitive_mnot_meqp:
      return jitterlispvm_recognize_specialized_instruction_primitive_mnot_meqp (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_primitive_mnot_mgreaterp:
      return jitterlispvm_recognize_specialized_instruction_primitive_mnot_mgreaterp (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_primitive_mnot_mlessp:
      return jitterlispvm_recognize_specialized_instruction_primitive_mnot_mlessp (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_primitive_mnothingp:
      return jitterlispvm_recognize_specialized_instruction_primitive_mnothingp (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_primitive_mnullp:
      return jitterlispvm_recognize_specialized_instruction_primitive_mnullp (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_primitive_mone_mminus:
      return jitterlispvm_recognize_specialized_instruction_primitive_mone_mminus (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_primitive_mone_mplus:
      return jitterlispvm_recognize_specialized_instruction_primitive_mone_mplus (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_primitive_mpositivep:
      return jitterlispvm_recognize_specialized_instruction_primitive_mpositivep (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_primitive_mprimordial_mdivided:
      return jitterlispvm_recognize_specialized_instruction_primitive_mprimordial_mdivided (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_primitive_mprimordial_mdivided_munsafe:
      return jitterlispvm_recognize_specialized_instruction_primitive_mprimordial_mdivided_munsafe (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_primitive_mprimordial_mminus:
      return jitterlispvm_recognize_specialized_instruction_primitive_mprimordial_mminus (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_primitive_mprimordial_mplus:
      return jitterlispvm_recognize_specialized_instruction_primitive_mprimordial_mplus (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_primitive_mprimordial_mtimes:
      return jitterlispvm_recognize_specialized_instruction_primitive_mprimordial_mtimes (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_primitive_mquotient:
      return jitterlispvm_recognize_specialized_instruction_primitive_mquotient (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_primitive_mquotient_munsafe:
      return jitterlispvm_recognize_specialized_instruction_primitive_mquotient_munsafe (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_primitive_mremainder:
      return jitterlispvm_recognize_specialized_instruction_primitive_mremainder (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_primitive_mremainder_munsafe:
      return jitterlispvm_recognize_specialized_instruction_primitive_mremainder_munsafe (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_primitive_mset_mcarb_mspecial:
      return jitterlispvm_recognize_specialized_instruction_primitive_mset_mcarb_mspecial (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_primitive_mset_mcdrb_mspecial:
      return jitterlispvm_recognize_specialized_instruction_primitive_mset_mcdrb_mspecial (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_primitive_msymbolp:
      return jitterlispvm_recognize_specialized_instruction_primitive_msymbolp (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_primitive_mtwo_mdivided:
      return jitterlispvm_recognize_specialized_instruction_primitive_mtwo_mdivided (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_primitive_mtwo_mquotient:
      return jitterlispvm_recognize_specialized_instruction_primitive_mtwo_mquotient (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_primitive_mtwo_mremainder:
      return jitterlispvm_recognize_specialized_instruction_primitive_mtwo_mremainder (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_primitive_mtwo_mtimes:
      return jitterlispvm_recognize_specialized_instruction_primitive_mtwo_mtimes (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_primitive_muniquep:
      return jitterlispvm_recognize_specialized_instruction_primitive_muniquep (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_primitive_mzerop:
      return jitterlispvm_recognize_specialized_instruction_primitive_mzerop (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_procedure_mprolog:
      return jitterlispvm_recognize_specialized_instruction_procedure_mprolog (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_push_mfalse:
      return jitterlispvm_recognize_specialized_instruction_push_mfalse (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_push_mglobal:
      return jitterlispvm_recognize_specialized_instruction_push_mglobal (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_push_mliteral:
      return jitterlispvm_recognize_specialized_instruction_push_mliteral (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_push_mnil:
      return jitterlispvm_recognize_specialized_instruction_push_mnil (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_push_mnothing:
      return jitterlispvm_recognize_specialized_instruction_push_mnothing (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_push_mone:
      return jitterlispvm_recognize_specialized_instruction_push_mone (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_push_mregister:
      return jitterlispvm_recognize_specialized_instruction_push_mregister (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_push_munspecified:
      return jitterlispvm_recognize_specialized_instruction_push_munspecified (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_push_mzero:
      return jitterlispvm_recognize_specialized_instruction_push_mzero (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_register_mto_mregister:
      return jitterlispvm_recognize_specialized_instruction_register_mto_mregister (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_restore_mregister:
      return jitterlispvm_recognize_specialized_instruction_restore_mregister (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_return:
      return jitterlispvm_recognize_specialized_instruction_return (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_save_mregister:
      return jitterlispvm_recognize_specialized_instruction_save_mregister (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_tail_mcall:
      return jitterlispvm_recognize_specialized_instruction_tail_mcall (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_tail_mcall_mcompiled:
      return jitterlispvm_recognize_specialized_instruction_tail_mcall_mcompiled (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_unreachable:
      return jitterlispvm_recognize_specialized_instruction_unreachable (ins->parameters, fl);
    default:
      jitter_fatal ("invalid meta-instruction id %i", (int)mi->id);
    }
  __builtin_unreachable ();
}

/* Specializer entry point: the only non-static function here. */
int
jitterlispvm_specialize_instruction (struct jitter_mutable_routine *p,
                                 const struct jitter_instruction *ins)
{
  enum jitterlispvm_specialized_instruction_opcode opcode
    = jitterlispvm_recognize_specialized_instruction (p, ins);
  if (opcode == jitterlispvm_specialized_instruction_opcode__eINVALID)
    jitter_fatal ("specialization failed: %s", ins->meta_instruction->name);

#ifdef JITTER_HAVE_PATCH_IN
  /* Replace the opcode with its non-defective counterpart. */
  opcode = jitterlispvm_defect_table [opcode];
#endif // #ifdef JITTER_HAVE_PATCH_IN

  jitter_add_specialized_instruction_opcode (p, opcode);


  /* FIXME: in the old shell-based generator I grouped specialized instructions by
     their "residual parameter map", yielding a switch with a lot of different
     specialized instructions mapping to the same case.  I should redo that here. */
  switch (opcode)
    {
    case jitterlispvm_specialized_instruction_opcode__eINVALID:
      break;

    case jitterlispvm_specialized_instruction_opcode__eBEGINBASICBLOCK:
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      break;

    case jitterlispvm_specialized_instruction_opcode__eEXITVM:
      break;

    case jitterlispvm_specialized_instruction_opcode__eDATALOCATIONS:
      break;

    case jitterlispvm_specialized_instruction_opcode__eNOP:
      break;

    case jitterlispvm_specialized_instruction_opcode__eUNREACHABLE0:
      break;

    case jitterlispvm_specialized_instruction_opcode__eUNREACHABLE1:
      break;

    case jitterlispvm_specialized_instruction_opcode__eUNREACHABLE2:
      break;

    case jitterlispvm_specialized_instruction_opcode_at_mdepth_mto_mregister__n1___rrR:
      /* A slow register is passed as a residual literal offset. */      jitter_add_specialized_instruction_literal (p, JITTERLISPVM_SLOW_REGISTER_OFFSET(r, ins->parameters[1]->register_index));
      break;

    case jitterlispvm_specialized_instruction_opcode_at_mdepth_mto_mregister__n2___rrR:
      /* A slow register is passed as a residual literal offset. */      jitter_add_specialized_instruction_literal (p, JITTERLISPVM_SLOW_REGISTER_OFFSET(r, ins->parameters[1]->register_index));
      break;

    case jitterlispvm_specialized_instruction_opcode_at_mdepth_mto_mregister__n3___rrR:
      /* A slow register is passed as a residual literal offset. */      jitter_add_specialized_instruction_literal (p, JITTERLISPVM_SLOW_REGISTER_OFFSET(r, ins->parameters[1]->register_index));
      break;

    case jitterlispvm_specialized_instruction_opcode_at_mdepth_mto_mregister__n4___rrR:
      /* A slow register is passed as a residual literal offset. */      jitter_add_specialized_instruction_literal (p, JITTERLISPVM_SLOW_REGISTER_OFFSET(r, ins->parameters[1]->register_index));
      break;

    case jitterlispvm_specialized_instruction_opcode_at_mdepth_mto_mregister__n5___rrR:
      /* A slow register is passed as a residual literal offset. */      jitter_add_specialized_instruction_literal (p, JITTERLISPVM_SLOW_REGISTER_OFFSET(r, ins->parameters[1]->register_index));
      break;

    case jitterlispvm_specialized_instruction_opcode_at_mdepth_mto_mregister__n6___rrR:
      /* A slow register is passed as a residual literal offset. */      jitter_add_specialized_instruction_literal (p, JITTERLISPVM_SLOW_REGISTER_OFFSET(r, ins->parameters[1]->register_index));
      break;

    case jitterlispvm_specialized_instruction_opcode_at_mdepth_mto_mregister__n7___rrR:
      /* A slow register is passed as a residual literal offset. */      jitter_add_specialized_instruction_literal (p, JITTERLISPVM_SLOW_REGISTER_OFFSET(r, ins->parameters[1]->register_index));
      break;

    case jitterlispvm_specialized_instruction_opcode_at_mdepth_mto_mregister__n8___rrR:
      /* A slow register is passed as a residual literal offset. */      jitter_add_specialized_instruction_literal (p, JITTERLISPVM_SLOW_REGISTER_OFFSET(r, ins->parameters[1]->register_index));
      break;

    case jitterlispvm_specialized_instruction_opcode_at_mdepth_mto_mregister__n9___rrR:
      /* A slow register is passed as a residual literal offset. */      jitter_add_specialized_instruction_literal (p, JITTERLISPVM_SLOW_REGISTER_OFFSET(r, ins->parameters[1]->register_index));
      break;

    case jitterlispvm_specialized_instruction_opcode_at_mdepth_mto_mregister__n10___rrR:
      /* A slow register is passed as a residual literal offset. */      jitter_add_specialized_instruction_literal (p, JITTERLISPVM_SLOW_REGISTER_OFFSET(r, ins->parameters[1]->register_index));
      break;

    case jitterlispvm_specialized_instruction_opcode_at_mdepth_mto_mregister__nR___rrR:
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* A slow register is passed as a residual literal offset. */      jitter_add_specialized_instruction_literal (p, JITTERLISPVM_SLOW_REGISTER_OFFSET(r, ins->parameters[1]->register_index));
      break;

    case jitterlispvm_specialized_instruction_opcode_branch__fR:
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      break;

    case jitterlispvm_specialized_instruction_opcode_branch_mif_mfalse__fR:
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      break;

    case jitterlispvm_specialized_instruction_opcode_branch_mif_mnot_mless__fR__fR:
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      break;

    case jitterlispvm_specialized_instruction_opcode_branch_mif_mnot_mnull__fR:
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      break;

    case jitterlispvm_specialized_instruction_opcode_branch_mif_mnull__fR:
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      break;

    case jitterlispvm_specialized_instruction_opcode_branch_mif_mregister_mnon_mzero___rrR__fR__fR:
      /* A slow register is passed as a residual literal offset. */      jitter_add_specialized_instruction_literal (p, JITTERLISPVM_SLOW_REGISTER_OFFSET(r, ins->parameters[0]->register_index));
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      break;

    case jitterlispvm_specialized_instruction_opcode_branch_mif_mtrue__fR:
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      break;

    case jitterlispvm_specialized_instruction_opcode_call__n0__retR:
      /* Caller instruction: make place for the return address,
         whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    case jitterlispvm_specialized_instruction_opcode_call__n1__retR:
      /* Caller instruction: make place for the return address,
         whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    case jitterlispvm_specialized_instruction_opcode_call__n2__retR:
      /* Caller instruction: make place for the return address,
         whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    case jitterlispvm_specialized_instruction_opcode_call__n3__retR:
      /* Caller instruction: make place for the return address,
         whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    case jitterlispvm_specialized_instruction_opcode_call__n4__retR:
      /* Caller instruction: make place for the return address,
         whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    case jitterlispvm_specialized_instruction_opcode_call__n5__retR:
      /* Caller instruction: make place for the return address,
         whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    case jitterlispvm_specialized_instruction_opcode_call__n6__retR:
      /* Caller instruction: make place for the return address,
         whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    case jitterlispvm_specialized_instruction_opcode_call__n7__retR:
      /* Caller instruction: make place for the return address,
         whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    case jitterlispvm_specialized_instruction_opcode_call__n8__retR:
      /* Caller instruction: make place for the return address,
         whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    case jitterlispvm_specialized_instruction_opcode_call__n9__retR:
      /* Caller instruction: make place for the return address,
         whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    case jitterlispvm_specialized_instruction_opcode_call__n10__retR:
      /* Caller instruction: make place for the return address,
         whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    case jitterlispvm_specialized_instruction_opcode_call__nR__retR:
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* Caller instruction: make place for the return address,
         whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    case jitterlispvm_specialized_instruction_opcode_call_mcompiled__n0__retR:
      /* Caller instruction: make place for the return address,
         whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    case jitterlispvm_specialized_instruction_opcode_call_mcompiled__n1__retR:
      /* Caller instruction: make place for the return address,
         whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    case jitterlispvm_specialized_instruction_opcode_call_mcompiled__n2__retR:
      /* Caller instruction: make place for the return address,
         whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    case jitterlispvm_specialized_instruction_opcode_call_mcompiled__n3__retR:
      /* Caller instruction: make place for the return address,
         whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    case jitterlispvm_specialized_instruction_opcode_call_mcompiled__n4__retR:
      /* Caller instruction: make place for the return address,
         whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    case jitterlispvm_specialized_instruction_opcode_call_mcompiled__n5__retR:
      /* Caller instruction: make place for the return address,
         whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    case jitterlispvm_specialized_instruction_opcode_call_mcompiled__n6__retR:
      /* Caller instruction: make place for the return address,
         whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    case jitterlispvm_specialized_instruction_opcode_call_mcompiled__n7__retR:
      /* Caller instruction: make place for the return address,
         whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    case jitterlispvm_specialized_instruction_opcode_call_mcompiled__n8__retR:
      /* Caller instruction: make place for the return address,
         whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    case jitterlispvm_specialized_instruction_opcode_call_mcompiled__n9__retR:
      /* Caller instruction: make place for the return address,
         whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    case jitterlispvm_specialized_instruction_opcode_call_mcompiled__n10__retR:
      /* Caller instruction: make place for the return address,
         whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    case jitterlispvm_specialized_instruction_opcode_call_mcompiled__nR__retR:
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* Caller instruction: make place for the return address,
         whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    case jitterlispvm_specialized_instruction_opcode_call_mfrom_mc__retR:
      /* Caller instruction: make place for the return address,
         whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    case jitterlispvm_specialized_instruction_opcode_canonicalize_mboolean:
      break;

    case jitterlispvm_specialized_instruction_opcode_check_mclosure__fR:
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      break;

    case jitterlispvm_specialized_instruction_opcode_check_mglobal_mdefined__nR__fR:
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      break;

    case jitterlispvm_specialized_instruction_opcode_check_min_marity__n0__fR:
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      break;

    case jitterlispvm_specialized_instruction_opcode_check_min_marity__n1__fR:
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      break;

    case jitterlispvm_specialized_instruction_opcode_check_min_marity__n2__fR:
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      break;

    case jitterlispvm_specialized_instruction_opcode_check_min_marity__n3__fR:
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      break;

    case jitterlispvm_specialized_instruction_opcode_check_min_marity__n4__fR:
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      break;

    case jitterlispvm_specialized_instruction_opcode_check_min_marity__n5__fR:
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      break;

    case jitterlispvm_specialized_instruction_opcode_check_min_marity__n6__fR:
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      break;

    case jitterlispvm_specialized_instruction_opcode_check_min_marity__n7__fR:
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      break;

    case jitterlispvm_specialized_instruction_opcode_check_min_marity__n8__fR:
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      break;

    case jitterlispvm_specialized_instruction_opcode_check_min_marity__n9__fR:
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      break;

    case jitterlispvm_specialized_instruction_opcode_check_min_marity__n10__fR:
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      break;

    case jitterlispvm_specialized_instruction_opcode_check_min_marity__nR__fR:
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      break;

    case jitterlispvm_specialized_instruction_opcode_check_min_marity_m_malt__n0__fR:
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      break;

    case jitterlispvm_specialized_instruction_opcode_check_min_marity_m_malt__n1__fR:
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      break;

    case jitterlispvm_specialized_instruction_opcode_check_min_marity_m_malt__n2__fR:
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      break;

    case jitterlispvm_specialized_instruction_opcode_check_min_marity_m_malt__n3__fR:
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      break;

    case jitterlispvm_specialized_instruction_opcode_check_min_marity_m_malt__n4__fR:
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      break;

    case jitterlispvm_specialized_instruction_opcode_check_min_marity_m_malt__n5__fR:
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      break;

    case jitterlispvm_specialized_instruction_opcode_check_min_marity_m_malt__n6__fR:
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      break;

    case jitterlispvm_specialized_instruction_opcode_check_min_marity_m_malt__n7__fR:
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      break;

    case jitterlispvm_specialized_instruction_opcode_check_min_marity_m_malt__n8__fR:
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      break;

    case jitterlispvm_specialized_instruction_opcode_check_min_marity_m_malt__n9__fR:
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      break;

    case jitterlispvm_specialized_instruction_opcode_check_min_marity_m_malt__n10__fR:
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      break;

    case jitterlispvm_specialized_instruction_opcode_check_min_marity_m_malt__nR__fR:
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      break;

    case jitterlispvm_specialized_instruction_opcode_copy_mfrom_mliteral__nR:
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      break;

    case jitterlispvm_specialized_instruction_opcode_copy_mfrom_mregister___rrR:
      /* A slow register is passed as a residual literal offset. */      jitter_add_specialized_instruction_literal (p, JITTERLISPVM_SLOW_REGISTER_OFFSET(r, ins->parameters[0]->register_index));
      break;

    case jitterlispvm_specialized_instruction_opcode_copy_mto_mregister___rrR:
      /* A slow register is passed as a residual literal offset. */      jitter_add_specialized_instruction_literal (p, JITTERLISPVM_SLOW_REGISTER_OFFSET(r, ins->parameters[0]->register_index));
      break;

    case jitterlispvm_specialized_instruction_opcode_drop:
      break;

    case jitterlispvm_specialized_instruction_opcode_drop_mnip:
      break;

    case jitterlispvm_specialized_instruction_opcode_dup:
      break;

    case jitterlispvm_specialized_instruction_opcode_exitvm:
      break;

    case jitterlispvm_specialized_instruction_opcode_fail__retR:
      /* Non-relocatable instruction: make place for the return label,
         whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    case jitterlispvm_specialized_instruction_opcode_gc_mif_mneeded__fR:
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      break;

    case jitterlispvm_specialized_instruction_opcode_heap_mallocate__n4:
      break;

    case jitterlispvm_specialized_instruction_opcode_heap_mallocate__n8:
      break;

    case jitterlispvm_specialized_instruction_opcode_heap_mallocate__n12:
      break;

    case jitterlispvm_specialized_instruction_opcode_heap_mallocate__n16:
      break;

    case jitterlispvm_specialized_instruction_opcode_heap_mallocate__n24:
      break;

    case jitterlispvm_specialized_instruction_opcode_heap_mallocate__n32:
      break;

    case jitterlispvm_specialized_instruction_opcode_heap_mallocate__n36:
      break;

    case jitterlispvm_specialized_instruction_opcode_heap_mallocate__n48:
      break;

    case jitterlispvm_specialized_instruction_opcode_heap_mallocate__n52:
      break;

    case jitterlispvm_specialized_instruction_opcode_heap_mallocate__n64:
      break;

    case jitterlispvm_specialized_instruction_opcode_heap_mallocate__nR:
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      break;

    case jitterlispvm_specialized_instruction_opcode_literal_mto_mregister__nR___rrR:
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* A slow register is passed as a residual literal offset. */      jitter_add_specialized_instruction_literal (p, JITTERLISPVM_SLOW_REGISTER_OFFSET(r, ins->parameters[1]->register_index));
      break;

    case jitterlispvm_specialized_instruction_opcode_nip:
      break;

    case jitterlispvm_specialized_instruction_opcode_nip_mdrop:
      break;

    case jitterlispvm_specialized_instruction_opcode_nip_mfive:
      break;

    case jitterlispvm_specialized_instruction_opcode_nip_mfive_mdrop:
      break;

    case jitterlispvm_specialized_instruction_opcode_nip_mfour:
      break;

    case jitterlispvm_specialized_instruction_opcode_nip_mfour_mdrop:
      break;

    case jitterlispvm_specialized_instruction_opcode_nip_mpush_mliteral__nR:
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      break;

    case jitterlispvm_specialized_instruction_opcode_nip_mpush_mregister___rrR:
      /* A slow register is passed as a residual literal offset. */      jitter_add_specialized_instruction_literal (p, JITTERLISPVM_SLOW_REGISTER_OFFSET(r, ins->parameters[0]->register_index));
      break;

    case jitterlispvm_specialized_instruction_opcode_nip_msix:
      break;

    case jitterlispvm_specialized_instruction_opcode_nip_msix_mdrop:
      break;

    case jitterlispvm_specialized_instruction_opcode_nip_mthree:
      break;

    case jitterlispvm_specialized_instruction_opcode_nip_mthree_mdrop:
      break;

    case jitterlispvm_specialized_instruction_opcode_nip_mtwo:
      break;

    case jitterlispvm_specialized_instruction_opcode_nip_mtwo_mdrop:
      break;

    case jitterlispvm_specialized_instruction_opcode_nop:
      break;

    case jitterlispvm_specialized_instruction_opcode_pop_mto_mglobal__nR__fR:
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      break;

    case jitterlispvm_specialized_instruction_opcode_pop_mto_mglobal_mdefined__nR__fR:
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      break;

    case jitterlispvm_specialized_instruction_opcode_pop_mto_mregister___rrR:
      /* A slow register is passed as a residual literal offset. */      jitter_add_specialized_instruction_literal (p, JITTERLISPVM_SLOW_REGISTER_OFFSET(r, ins->parameters[0]->register_index));
      break;

    case jitterlispvm_specialized_instruction_opcode_primitive__nR__n0__fR:
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      break;

    case jitterlispvm_specialized_instruction_opcode_primitive__nR__n1__fR:
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      break;

    case jitterlispvm_specialized_instruction_opcode_primitive__nR__n2__fR:
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      break;

    case jitterlispvm_specialized_instruction_opcode_primitive__nR__n3__fR:
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      break;

    case jitterlispvm_specialized_instruction_opcode_primitive__nR__n4__fR:
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      break;

    case jitterlispvm_specialized_instruction_opcode_primitive__nR__nR__fR:
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      jitter_add_specialized_instruction_literal (p, ins->parameters[1]->literal.ufixnum);
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      break;

    case jitterlispvm_specialized_instruction_opcode_primitive_mboolean_mcanonicalize:
      break;

    case jitterlispvm_specialized_instruction_opcode_primitive_mbox:
      break;

    case jitterlispvm_specialized_instruction_opcode_primitive_mbox_mget__fR:
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      break;

    case jitterlispvm_specialized_instruction_opcode_primitive_mbox_msetb_mspecial__fR:
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      break;

    case jitterlispvm_specialized_instruction_opcode_primitive_mcar__fR:
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      break;

    case jitterlispvm_specialized_instruction_opcode_primitive_mcdr__fR:
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      break;

    case jitterlispvm_specialized_instruction_opcode_primitive_mcharacterp:
      break;

    case jitterlispvm_specialized_instruction_opcode_primitive_mcons_mspecial:
      break;

    case jitterlispvm_specialized_instruction_opcode_primitive_mconsp:
      break;

    case jitterlispvm_specialized_instruction_opcode_primitive_meqp:
      break;

    case jitterlispvm_specialized_instruction_opcode_primitive_mfixnum_meqp__fR:
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      break;

    case jitterlispvm_specialized_instruction_opcode_primitive_mfixnum_mnot_meqp__fR:
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      break;

    case jitterlispvm_specialized_instruction_opcode_primitive_mfixnump:
      break;

    case jitterlispvm_specialized_instruction_opcode_primitive_mgreaterp__fR:
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      break;

    case jitterlispvm_specialized_instruction_opcode_primitive_mlessp__fR:
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      break;

    case jitterlispvm_specialized_instruction_opcode_primitive_mnegate__fR:
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      break;

    case jitterlispvm_specialized_instruction_opcode_primitive_mnegativep__fR:
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      break;

    case jitterlispvm_specialized_instruction_opcode_primitive_mnon_mconsp:
      break;

    case jitterlispvm_specialized_instruction_opcode_primitive_mnon_mnegativep__fR:
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      break;

    case jitterlispvm_specialized_instruction_opcode_primitive_mnon_mnullp:
      break;

    case jitterlispvm_specialized_instruction_opcode_primitive_mnon_mpositivep__fR:
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      break;

    case jitterlispvm_specialized_instruction_opcode_primitive_mnon_msymbolp:
      break;

    case jitterlispvm_specialized_instruction_opcode_primitive_mnon_mzerop__fR:
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      break;

    case jitterlispvm_specialized_instruction_opcode_primitive_mnot:
      break;

    case jitterlispvm_specialized_instruction_opcode_primitive_mnot_meqp:
      break;

    case jitterlispvm_specialized_instruction_opcode_primitive_mnot_mgreaterp__fR:
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      break;

    case jitterlispvm_specialized_instruction_opcode_primitive_mnot_mlessp__fR:
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      break;

    case jitterlispvm_specialized_instruction_opcode_primitive_mnothingp:
      break;

    case jitterlispvm_specialized_instruction_opcode_primitive_mnullp:
      break;

    case jitterlispvm_specialized_instruction_opcode_primitive_mone_mminus__fR:
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      break;

    case jitterlispvm_specialized_instruction_opcode_primitive_mone_mplus__fR:
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      break;

    case jitterlispvm_specialized_instruction_opcode_primitive_mpositivep__fR:
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      break;

    case jitterlispvm_specialized_instruction_opcode_primitive_mprimordial_mdivided__fR:
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      break;

    case jitterlispvm_specialized_instruction_opcode_primitive_mprimordial_mdivided_munsafe__fR:
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      break;

    case jitterlispvm_specialized_instruction_opcode_primitive_mprimordial_mminus__fR:
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      break;

    case jitterlispvm_specialized_instruction_opcode_primitive_mprimordial_mplus__fR:
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      break;

    case jitterlispvm_specialized_instruction_opcode_primitive_mprimordial_mtimes__fR:
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      break;

    case jitterlispvm_specialized_instruction_opcode_primitive_mquotient__fR:
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      break;

    case jitterlispvm_specialized_instruction_opcode_primitive_mquotient_munsafe__fR:
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      break;

    case jitterlispvm_specialized_instruction_opcode_primitive_mremainder__fR:
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      break;

    case jitterlispvm_specialized_instruction_opcode_primitive_mremainder_munsafe__fR:
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      break;

    case jitterlispvm_specialized_instruction_opcode_primitive_mset_mcarb_mspecial__fR:
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      break;

    case jitterlispvm_specialized_instruction_opcode_primitive_mset_mcdrb_mspecial__fR:
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      break;

    case jitterlispvm_specialized_instruction_opcode_primitive_msymbolp:
      break;

    case jitterlispvm_specialized_instruction_opcode_primitive_mtwo_mdivided__fR:
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      break;

    case jitterlispvm_specialized_instruction_opcode_primitive_mtwo_mquotient__fR:
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      break;

    case jitterlispvm_specialized_instruction_opcode_primitive_mtwo_mremainder__fR:
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      break;

    case jitterlispvm_specialized_instruction_opcode_primitive_mtwo_mtimes__fR:
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      break;

    case jitterlispvm_specialized_instruction_opcode_primitive_muniquep:
      break;

    case jitterlispvm_specialized_instruction_opcode_primitive_mzerop__fR:
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      break;

    case jitterlispvm_specialized_instruction_opcode_procedure_mprolog:
      break;

    case jitterlispvm_specialized_instruction_opcode_push_mfalse:
      break;

    case jitterlispvm_specialized_instruction_opcode_push_mglobal__nR__fR:
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      break;

    case jitterlispvm_specialized_instruction_opcode_push_mliteral__nR:
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      break;

    case jitterlispvm_specialized_instruction_opcode_push_mnil:
      break;

    case jitterlispvm_specialized_instruction_opcode_push_mnothing:
      break;

    case jitterlispvm_specialized_instruction_opcode_push_mone:
      break;

    case jitterlispvm_specialized_instruction_opcode_push_mregister___rrR:
      /* A slow register is passed as a residual literal offset. */      jitter_add_specialized_instruction_literal (p, JITTERLISPVM_SLOW_REGISTER_OFFSET(r, ins->parameters[0]->register_index));
      break;

    case jitterlispvm_specialized_instruction_opcode_push_munspecified:
      break;

    case jitterlispvm_specialized_instruction_opcode_push_mzero:
      break;

    case jitterlispvm_specialized_instruction_opcode_register_mto_mregister___rrR___rrR:
      /* A slow register is passed as a residual literal offset. */      jitter_add_specialized_instruction_literal (p, JITTERLISPVM_SLOW_REGISTER_OFFSET(r, ins->parameters[0]->register_index));
      /* A slow register is passed as a residual literal offset. */      jitter_add_specialized_instruction_literal (p, JITTERLISPVM_SLOW_REGISTER_OFFSET(r, ins->parameters[1]->register_index));
      break;

    case jitterlispvm_specialized_instruction_opcode_restore_mregister___rrR:
      /* A slow register is passed as a residual literal offset. */      jitter_add_specialized_instruction_literal (p, JITTERLISPVM_SLOW_REGISTER_OFFSET(r, ins->parameters[0]->register_index));
      break;

    case jitterlispvm_specialized_instruction_opcode_return:
      break;

    case jitterlispvm_specialized_instruction_opcode_save_mregister___rrR:
      /* A slow register is passed as a residual literal offset. */      jitter_add_specialized_instruction_literal (p, JITTERLISPVM_SLOW_REGISTER_OFFSET(r, ins->parameters[0]->register_index));
      break;

    case jitterlispvm_specialized_instruction_opcode_tail_mcall__n0:
      break;

    case jitterlispvm_specialized_instruction_opcode_tail_mcall__n1:
      break;

    case jitterlispvm_specialized_instruction_opcode_tail_mcall__n2:
      break;

    case jitterlispvm_specialized_instruction_opcode_tail_mcall__n3:
      break;

    case jitterlispvm_specialized_instruction_opcode_tail_mcall__n4:
      break;

    case jitterlispvm_specialized_instruction_opcode_tail_mcall__n5:
      break;

    case jitterlispvm_specialized_instruction_opcode_tail_mcall__n6:
      break;

    case jitterlispvm_specialized_instruction_opcode_tail_mcall__n7:
      break;

    case jitterlispvm_specialized_instruction_opcode_tail_mcall__n8:
      break;

    case jitterlispvm_specialized_instruction_opcode_tail_mcall__n9:
      break;

    case jitterlispvm_specialized_instruction_opcode_tail_mcall__n10:
      break;

    case jitterlispvm_specialized_instruction_opcode_tail_mcall__nR:
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      break;

    case jitterlispvm_specialized_instruction_opcode_tail_mcall_mcompiled__n0:
      break;

    case jitterlispvm_specialized_instruction_opcode_tail_mcall_mcompiled__n1:
      break;

    case jitterlispvm_specialized_instruction_opcode_tail_mcall_mcompiled__n2:
      break;

    case jitterlispvm_specialized_instruction_opcode_tail_mcall_mcompiled__n3:
      break;

    case jitterlispvm_specialized_instruction_opcode_tail_mcall_mcompiled__n4:
      break;

    case jitterlispvm_specialized_instruction_opcode_tail_mcall_mcompiled__n5:
      break;

    case jitterlispvm_specialized_instruction_opcode_tail_mcall_mcompiled__n6:
      break;

    case jitterlispvm_specialized_instruction_opcode_tail_mcall_mcompiled__n7:
      break;

    case jitterlispvm_specialized_instruction_opcode_tail_mcall_mcompiled__n8:
      break;

    case jitterlispvm_specialized_instruction_opcode_tail_mcall_mcompiled__n9:
      break;

    case jitterlispvm_specialized_instruction_opcode_tail_mcall_mcompiled__n10:
      break;

    case jitterlispvm_specialized_instruction_opcode_tail_mcall_mcompiled__nR:
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      break;

    case jitterlispvm_specialized_instruction_opcode_unreachable:
      break;

    case jitterlispvm_specialized_instruction_opcode__Abranch__fR_A_mno_mfast_mbranches:
      /* Non-relocatable instruction: make place for the return label,
         whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    case jitterlispvm_specialized_instruction_opcode__Abranch_mif_mfalse__fR_A_mno_mfast_mbranches:
      /* Non-relocatable instruction: make place for the return label,
         whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    case jitterlispvm_specialized_instruction_opcode__Abranch_mif_mnot_mless__fR__fR_A_mno_mfast_mbranches:
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      /* Non-relocatable instruction: make place for the return label,
         whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    case jitterlispvm_specialized_instruction_opcode__Abranch_mif_mnot_mnull__fR_A_mno_mfast_mbranches:
      /* Non-relocatable instruction: make place for the return label,
         whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    case jitterlispvm_specialized_instruction_opcode__Abranch_mif_mnull__fR_A_mno_mfast_mbranches:
      /* Non-relocatable instruction: make place for the return label,
         whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    case jitterlispvm_specialized_instruction_opcode__Abranch_mif_mregister_mnon_mzero___rrR__fR__fR_A_mno_mfast_mbranches:
      /* A slow register is passed as a residual literal offset. */      jitter_add_specialized_instruction_literal (p, JITTERLISPVM_SLOW_REGISTER_OFFSET(r, ins->parameters[0]->register_index));
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      /* Non-relocatable instruction: make place for the return label,
         whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    case jitterlispvm_specialized_instruction_opcode__Abranch_mif_mtrue__fR_A_mno_mfast_mbranches:
      /* Non-relocatable instruction: make place for the return label,
         whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    case jitterlispvm_specialized_instruction_opcode__Acheck_mclosure__fR_A_mno_mfast_mbranches:
      /* Non-relocatable instruction: make place for the return label,
         whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    case jitterlispvm_specialized_instruction_opcode__Acheck_mglobal_mdefined__nR__fR_A_mno_mfast_mbranches:
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* Non-relocatable instruction: make place for the return label,
         whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    case jitterlispvm_specialized_instruction_opcode__Acheck_min_marity__n0__fR_A_mno_mfast_mbranches:
      /* Non-relocatable instruction: make place for the return label,
         whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    case jitterlispvm_specialized_instruction_opcode__Acheck_min_marity__n1__fR_A_mno_mfast_mbranches:
      /* Non-relocatable instruction: make place for the return label,
         whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    case jitterlispvm_specialized_instruction_opcode__Acheck_min_marity__n2__fR_A_mno_mfast_mbranches:
      /* Non-relocatable instruction: make place for the return label,
         whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    case jitterlispvm_specialized_instruction_opcode__Acheck_min_marity__n3__fR_A_mno_mfast_mbranches:
      /* Non-relocatable instruction: make place for the return label,
         whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    case jitterlispvm_specialized_instruction_opcode__Acheck_min_marity__n4__fR_A_mno_mfast_mbranches:
      /* Non-relocatable instruction: make place for the return label,
         whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    case jitterlispvm_specialized_instruction_opcode__Acheck_min_marity__n5__fR_A_mno_mfast_mbranches:
      /* Non-relocatable instruction: make place for the return label,
         whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    case jitterlispvm_specialized_instruction_opcode__Acheck_min_marity__n6__fR_A_mno_mfast_mbranches:
      /* Non-relocatable instruction: make place for the return label,
         whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    case jitterlispvm_specialized_instruction_opcode__Acheck_min_marity__n7__fR_A_mno_mfast_mbranches:
      /* Non-relocatable instruction: make place for the return label,
         whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    case jitterlispvm_specialized_instruction_opcode__Acheck_min_marity__n8__fR_A_mno_mfast_mbranches:
      /* Non-relocatable instruction: make place for the return label,
         whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    case jitterlispvm_specialized_instruction_opcode__Acheck_min_marity__n9__fR_A_mno_mfast_mbranches:
      /* Non-relocatable instruction: make place for the return label,
         whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    case jitterlispvm_specialized_instruction_opcode__Acheck_min_marity__n10__fR_A_mno_mfast_mbranches:
      /* Non-relocatable instruction: make place for the return label,
         whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    case jitterlispvm_specialized_instruction_opcode__Acheck_min_marity__nR__fR_A_mno_mfast_mbranches:
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* Non-relocatable instruction: make place for the return label,
         whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    case jitterlispvm_specialized_instruction_opcode__Acheck_min_marity_m_malt__n0__fR_A_mno_mfast_mbranches:
      /* Non-relocatable instruction: make place for the return label,
         whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    case jitterlispvm_specialized_instruction_opcode__Acheck_min_marity_m_malt__n1__fR_A_mno_mfast_mbranches:
      /* Non-relocatable instruction: make place for the return label,
         whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    case jitterlispvm_specialized_instruction_opcode__Acheck_min_marity_m_malt__n2__fR_A_mno_mfast_mbranches:
      /* Non-relocatable instruction: make place for the return label,
         whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    case jitterlispvm_specialized_instruction_opcode__Acheck_min_marity_m_malt__n3__fR_A_mno_mfast_mbranches:
      /* Non-relocatable instruction: make place for the return label,
         whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    case jitterlispvm_specialized_instruction_opcode__Acheck_min_marity_m_malt__n4__fR_A_mno_mfast_mbranches:
      /* Non-relocatable instruction: make place for the return label,
         whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    case jitterlispvm_specialized_instruction_opcode__Acheck_min_marity_m_malt__n5__fR_A_mno_mfast_mbranches:
      /* Non-relocatable instruction: make place for the return label,
         whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    case jitterlispvm_specialized_instruction_opcode__Acheck_min_marity_m_malt__n6__fR_A_mno_mfast_mbranches:
      /* Non-relocatable instruction: make place for the return label,
         whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    case jitterlispvm_specialized_instruction_opcode__Acheck_min_marity_m_malt__n7__fR_A_mno_mfast_mbranches:
      /* Non-relocatable instruction: make place for the return label,
         whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    case jitterlispvm_specialized_instruction_opcode__Acheck_min_marity_m_malt__n8__fR_A_mno_mfast_mbranches:
      /* Non-relocatable instruction: make place for the return label,
         whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    case jitterlispvm_specialized_instruction_opcode__Acheck_min_marity_m_malt__n9__fR_A_mno_mfast_mbranches:
      /* Non-relocatable instruction: make place for the return label,
         whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    case jitterlispvm_specialized_instruction_opcode__Acheck_min_marity_m_malt__n10__fR_A_mno_mfast_mbranches:
      /* Non-relocatable instruction: make place for the return label,
         whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    case jitterlispvm_specialized_instruction_opcode__Acheck_min_marity_m_malt__nR__fR_A_mno_mfast_mbranches:
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* Non-relocatable instruction: make place for the return label,
         whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    case jitterlispvm_specialized_instruction_opcode__Agc_mif_mneeded__fR_A_mno_mfast_mbranches:
      /* Non-relocatable instruction: make place for the return label,
         whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    case jitterlispvm_specialized_instruction_opcode__Apop_mto_mglobal__nR__fR_A_mno_mfast_mbranches:
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* Non-relocatable instruction: make place for the return label,
         whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    case jitterlispvm_specialized_instruction_opcode__Apop_mto_mglobal_mdefined__nR__fR_A_mno_mfast_mbranches:
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* Non-relocatable instruction: make place for the return label,
         whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    case jitterlispvm_specialized_instruction_opcode__Aprimitive__nR__n0__fR_A_mno_mfast_mbranches:
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* Non-relocatable instruction: make place for the return label,
         whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    case jitterlispvm_specialized_instruction_opcode__Aprimitive__nR__n1__fR_A_mno_mfast_mbranches:
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* Non-relocatable instruction: make place for the return label,
         whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    case jitterlispvm_specialized_instruction_opcode__Aprimitive__nR__n2__fR_A_mno_mfast_mbranches:
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* Non-relocatable instruction: make place for the return label,
         whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    case jitterlispvm_specialized_instruction_opcode__Aprimitive__nR__n3__fR_A_mno_mfast_mbranches:
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* Non-relocatable instruction: make place for the return label,
         whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    case jitterlispvm_specialized_instruction_opcode__Aprimitive__nR__n4__fR_A_mno_mfast_mbranches:
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* Non-relocatable instruction: make place for the return label,
         whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    case jitterlispvm_specialized_instruction_opcode__Aprimitive__nR__nR__fR_A_mno_mfast_mbranches:
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      jitter_add_specialized_instruction_literal (p, ins->parameters[1]->literal.ufixnum);
      /* Non-relocatable instruction: make place for the return label,
         whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    case jitterlispvm_specialized_instruction_opcode__Aprimitive_mbox_mget__fR_A_mno_mfast_mbranches:
      /* Non-relocatable instruction: make place for the return label,
         whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    case jitterlispvm_specialized_instruction_opcode__Aprimitive_mbox_msetb_mspecial__fR_A_mno_mfast_mbranches:
      /* Non-relocatable instruction: make place for the return label,
         whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    case jitterlispvm_specialized_instruction_opcode__Aprimitive_mcar__fR_A_mno_mfast_mbranches:
      /* Non-relocatable instruction: make place for the return label,
         whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    case jitterlispvm_specialized_instruction_opcode__Aprimitive_mcdr__fR_A_mno_mfast_mbranches:
      /* Non-relocatable instruction: make place for the return label,
         whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    case jitterlispvm_specialized_instruction_opcode__Aprimitive_mfixnum_meqp__fR_A_mno_mfast_mbranches:
      /* Non-relocatable instruction: make place for the return label,
         whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    case jitterlispvm_specialized_instruction_opcode__Aprimitive_mfixnum_mnot_meqp__fR_A_mno_mfast_mbranches:
      /* Non-relocatable instruction: make place for the return label,
         whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    case jitterlispvm_specialized_instruction_opcode__Aprimitive_mgreaterp__fR_A_mno_mfast_mbranches:
      /* Non-relocatable instruction: make place for the return label,
         whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    case jitterlispvm_specialized_instruction_opcode__Aprimitive_mlessp__fR_A_mno_mfast_mbranches:
      /* Non-relocatable instruction: make place for the return label,
         whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    case jitterlispvm_specialized_instruction_opcode__Aprimitive_mnegate__fR_A_mno_mfast_mbranches:
      /* Non-relocatable instruction: make place for the return label,
         whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    case jitterlispvm_specialized_instruction_opcode__Aprimitive_mnegativep__fR_A_mno_mfast_mbranches:
      /* Non-relocatable instruction: make place for the return label,
         whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    case jitterlispvm_specialized_instruction_opcode__Aprimitive_mnon_mnegativep__fR_A_mno_mfast_mbranches:
      /* Non-relocatable instruction: make place for the return label,
         whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    case jitterlispvm_specialized_instruction_opcode__Aprimitive_mnon_mpositivep__fR_A_mno_mfast_mbranches:
      /* Non-relocatable instruction: make place for the return label,
         whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    case jitterlispvm_specialized_instruction_opcode__Aprimitive_mnon_mzerop__fR_A_mno_mfast_mbranches:
      /* Non-relocatable instruction: make place for the return label,
         whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    case jitterlispvm_specialized_instruction_opcode__Aprimitive_mnot_mgreaterp__fR_A_mno_mfast_mbranches:
      /* Non-relocatable instruction: make place for the return label,
         whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    case jitterlispvm_specialized_instruction_opcode__Aprimitive_mnot_mlessp__fR_A_mno_mfast_mbranches:
      /* Non-relocatable instruction: make place for the return label,
         whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    case jitterlispvm_specialized_instruction_opcode__Aprimitive_mone_mminus__fR_A_mno_mfast_mbranches:
      /* Non-relocatable instruction: make place for the return label,
         whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    case jitterlispvm_specialized_instruction_opcode__Aprimitive_mone_mplus__fR_A_mno_mfast_mbranches:
      /* Non-relocatable instruction: make place for the return label,
         whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    case jitterlispvm_specialized_instruction_opcode__Aprimitive_mpositivep__fR_A_mno_mfast_mbranches:
      /* Non-relocatable instruction: make place for the return label,
         whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    case jitterlispvm_specialized_instruction_opcode__Aprimitive_mprimordial_mdivided__fR_A_mno_mfast_mbranches:
      /* Non-relocatable instruction: make place for the return label,
         whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    case jitterlispvm_specialized_instruction_opcode__Aprimitive_mprimordial_mdivided_munsafe__fR_A_mno_mfast_mbranches:
      /* Non-relocatable instruction: make place for the return label,
         whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    case jitterlispvm_specialized_instruction_opcode__Aprimitive_mprimordial_mminus__fR_A_mno_mfast_mbranches:
      /* Non-relocatable instruction: make place for the return label,
         whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    case jitterlispvm_specialized_instruction_opcode__Aprimitive_mprimordial_mplus__fR_A_mno_mfast_mbranches:
      /* Non-relocatable instruction: make place for the return label,
         whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    case jitterlispvm_specialized_instruction_opcode__Aprimitive_mprimordial_mtimes__fR_A_mno_mfast_mbranches:
      /* Non-relocatable instruction: make place for the return label,
         whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    case jitterlispvm_specialized_instruction_opcode__Aprimitive_mquotient__fR_A_mno_mfast_mbranches:
      /* Non-relocatable instruction: make place for the return label,
         whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    case jitterlispvm_specialized_instruction_opcode__Aprimitive_mquotient_munsafe__fR_A_mno_mfast_mbranches:
      /* Non-relocatable instruction: make place for the return label,
         whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    case jitterlispvm_specialized_instruction_opcode__Aprimitive_mremainder__fR_A_mno_mfast_mbranches:
      /* Non-relocatable instruction: make place for the return label,
         whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    case jitterlispvm_specialized_instruction_opcode__Aprimitive_mremainder_munsafe__fR_A_mno_mfast_mbranches:
      /* Non-relocatable instruction: make place for the return label,
         whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    case jitterlispvm_specialized_instruction_opcode__Aprimitive_mset_mcarb_mspecial__fR_A_mno_mfast_mbranches:
      /* Non-relocatable instruction: make place for the return label,
         whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    case jitterlispvm_specialized_instruction_opcode__Aprimitive_mset_mcdrb_mspecial__fR_A_mno_mfast_mbranches:
      /* Non-relocatable instruction: make place for the return label,
         whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    case jitterlispvm_specialized_instruction_opcode__Aprimitive_mtwo_mdivided__fR_A_mno_mfast_mbranches:
      /* Non-relocatable instruction: make place for the return label,
         whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    case jitterlispvm_specialized_instruction_opcode__Aprimitive_mtwo_mquotient__fR_A_mno_mfast_mbranches:
      /* Non-relocatable instruction: make place for the return label,
         whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    case jitterlispvm_specialized_instruction_opcode__Aprimitive_mtwo_mremainder__fR_A_mno_mfast_mbranches:
      /* Non-relocatable instruction: make place for the return label,
         whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    case jitterlispvm_specialized_instruction_opcode__Aprimitive_mtwo_mtimes__fR_A_mno_mfast_mbranches:
      /* Non-relocatable instruction: make place for the return label,
         whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    case jitterlispvm_specialized_instruction_opcode__Aprimitive_mzerop__fR_A_mno_mfast_mbranches:
      /* Non-relocatable instruction: make place for the return label,
         whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    case jitterlispvm_specialized_instruction_opcode__Apush_mglobal__nR__fR_A_mno_mfast_mbranches:
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* Non-relocatable instruction: make place for the return label,
         whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    default:
      jitter_fatal ("invalid specialized instruction opcode %i", (int)opcode);
    }
  return 1; // FIXME: I should rethink this return value.
}

void
jitterlispvm_state_initialize (struct jitterlispvm_state *jitter_state)
{
  struct jitterlispvm_state_backing * const jitter_state_backing
    __attribute__ ((unused))
    = & jitter_state->jitterlispvm_state_backing;
  struct jitterlispvm_state_runtime * const jitter_state_runtime
    __attribute__ ((unused))
    = & jitter_state->jitterlispvm_state_runtime;

  /* Initialize the Array. */
  jitter_state_backing->jitter_slow_register_no_per_class = 0; // FIXME: raise?
  jitter_state_backing->jitter_array
    = jitter_xmalloc (JITTERLISPVM_ARRAY_SIZE(jitter_state_backing
                         ->jitter_slow_register_no_per_class));

  /* Initialize special-purpose data. */
  jitterlispvm_initialize_special_purpose_data (JITTERLISPVM_ARRAY_TO_SPECIAL_PURPOSE_STATE_DATA (jitter_state_backing->jitter_array));

  /* Initialize stack backings and stack runtime data structures, pointing
     to memory from the backings. */
  jitterlisp_object jitter_stack_m_initial_element = (jitterlisp_object) (JITTERLISP_UNDEFINED);
  jitter_stack_initialize_tos_backing(& jitter_state_backing->jitter_stack_mainstack_backing,
                                      sizeof (jitterlisp_object),
                                      8192,
                                      (char *) & jitter_stack_m_initial_element,
                                      1,
                                      1);
  JITTER_STACK_TOS_INITIALIZE(jitterlisp_object, jitter_state_runtime-> ,
                              mainstack, jitter_state_backing->jitter_stack_mainstack_backing);
  jitterlisp_object jitter_stack_t_initial_element = (jitterlisp_object) (JITTERLISP_UNDEFINED);
  jitter_stack_initialize_ntos_backing(& jitter_state_backing->jitter_stack_returnstack_backing,
                                      sizeof (jitterlisp_object),
                                      8192,
                                      (char *) & jitter_stack_t_initial_element,
                                      1,
                                      1);
  JITTER_STACK_NTOS_INITIALIZE(jitterlisp_object, jitter_state_runtime-> ,
                              returnstack, jitter_state_backing->jitter_stack_returnstack_backing);

  /* Initialise the link register, if present. */
#if    defined(JITTER_DISPATCH_SWITCH)                   \
    || defined(JITTER_DISPATCH_DIRECT_THREADING)         \
    || defined(JITTER_DISPATCH_MINIMAL_THREADING)        \
    || (   defined(JITTER_DISPATCH_NO_THREADING)         \
        && ! defined(JITTER_MACHINE_SUPPORTS_PROCEDURE))
  jitter_state_runtime->_jitter_link = NULL;
#endif

  /* Initialise r-class fast registers. */


  /* User code for state initialization. */

#ifdef JITTER_GC_STUB
  /* Initialize the next pointer and the limit pointer to refer to a fixed-size
     nursery.  There is no real GC yet, so when next hits limit there will be
     a failure; still, allocation should work up to that point. */
  size_t nursery_size = 1024 * 1024 * 10;
  char *nursery = jitter_xmalloc (nursery_size);
  jitter_state_runtime->allocation_next = nursery;
  jitter_state_runtime->allocation_limit = nursery + nursery_size;
#endif // #ifdef JITTER_GC_STUB
  
  /* End of the user code for state initialization. */

  /* Link this new state to the list of states. */
  JITTER_LIST_LINK_LAST (jitterlispvm_state, links, & jitterlispvm_vm->states, jitter_state);

}

void
jitterlispvm_state_finalize (struct jitterlispvm_state *jitter_state)
{
  /* Unlink this new state from the list of states. */
  JITTER_LIST_UNLINK (jitterlispvm_state, links, & jitterlispvm_vm->states, jitter_state);

  struct jitterlispvm_state_backing * const jitter_state_backing
    __attribute__ ((unused))
    = & jitter_state->jitterlispvm_state_backing;
  struct jitterlispvm_state_runtime * const jitter_state_runtime
    __attribute__ ((unused))
    = & jitter_state->jitterlispvm_state_runtime;

  /* Finalize special-purpose data. */
  jitterlispvm_finalize_special_purpose_data (JITTERLISPVM_ARRAY_TO_SPECIAL_PURPOSE_STATE_DATA (jitter_state_backing->jitter_array));

  /* Finalize stack backings -- There is no need to finalize the stack
     runtime data structures, as they hold no heap data of their own. */
  jitter_stack_finalize_backing (& jitter_state_backing->jitter_stack_mainstack_backing);
  jitter_stack_finalize_backing (& jitter_state_backing->jitter_stack_returnstack_backing);


  /* User code for state finalization. */

  /* End of the user code for state finalization. */

  /* Finalize the Array. */
  free ((void *) jitter_state_backing->jitter_array);

}

