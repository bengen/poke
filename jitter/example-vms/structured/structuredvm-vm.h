/* This code is machine-generated.  See its source for license
   information. This software is derived from software
   distributed under the GNU GPL version 3 or later. */

/* User-specified code, initial header part: beginning. */

/* User-specified code, initial header part: end */

/* VM library: main header file.

   Copyright (C) 2016, 2017, 2018, 2019, 2020 Luca Saiu
   Written by Luca Saiu

   This file is part of Jitter.

   Jitter is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Jitter is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Jitter.  If not, see <http://www.gnu.org/licenses/>. */


/* Generated file warning.
 * ************************************************************************** */

/* Unless this file is named exactly "vm.h" , without any prefix, you are
   looking at a machine-generated derived file.  The original source is the vm.h
   template from Jitter, with added code implementing the structuredvm VM. */




/* This multiple-inclusion guard is opened here in the template, and will be
   closed at the end of the generated code.  It is normal to find no matching
   #endif in the template file.  */
#ifndef STRUCTUREDVM_VM_H_
#define STRUCTUREDVM_VM_H_


/* This is the main VM header to use from hand-written code.
 * ************************************************************************** */

#include <stdio.h>
#include <stdbool.h>

#include <jitter/jitter.h>
#include <jitter/jitter-hash.h>
#include <jitter/jitter-stack.h>
#include <jitter/jitter-instruction.h>
#include <jitter/jitter-mutable-routine.h>
#include <jitter/jitter-print.h>
#include <jitter/jitter-routine.h>
//#include <jitter/jitter-specialize.h> // FIXME: what about only declaring jitter_specialize in another header, and not including this?
#include <jitter/jitter-disassemble.h>
#include <jitter/jitter-vm.h>
#include <jitter/jitter-profile.h>
#include <jitter/jitter-data-locations.h>
#include <jitter/jitter-arithmetic.h>
#include <jitter/jitter-bitwise.h>
#include <jitter/jitter-signals.h>
#include <jitter/jitter-list.h>




/* Initialization and finalization.
 * ************************************************************************** */

/* Initialize the runtime state for the structuredvm VM.  This needs to be called
   before using VM routines or VM states in any way. */
void
structuredvm_initialize (void);

/* Finalize the runtime state, freeing some resources.  After calling this no
   use of VM routines or states is allowed.  It is possible to re-initialize
   after finalizing; these later re-initializations might be more efficient than
   the first initialization. */
void
structuredvm_finalize (void);




/* State data structure initialization and finalization.
 * ************************************************************************** */

/* The machine state is separated into the backing and the more compact runtime
   data structures, to be allocated in registers as far as possible.  These are
   just a forward-declarations: the actual definitions are machine-generated. */
struct structuredvm_state_backing;
struct structuredvm_state_runtime;

/* A data structure containing both the backing and the runtime state.  This is
   a forward-declaration: the actual definition will come after both are
   defined. */
struct structuredvm_state;

/* Initialize the pointed VM state data structure, or fail fatally.  The
   function definition is machine-generated, even if it may include user code.
   The state backing and runtime are initialized at the same time, and in fact
   the distinction between them is invisible to the VM user. */
void
structuredvm_state_initialize (struct structuredvm_state *state)
  __attribute__ ((nonnull (1)));

/* Finalize the pointed VM state data structure, or fail fatally.  The function
   definition is machine-generated, even if it may include user code.  The state
   backing and runtime are finalized at the same time. */
void
structuredvm_state_finalize (struct structuredvm_state *state)
  __attribute__ ((nonnull (1)));




/* State data structure: iteration.
 * ************************************************************************** */

/* The header of a doubly-linked list linking every state for the structuredvm VM
   together.  This global is automatically wrapped, and therefore also
   accessible from VM instruction code. */
extern struct jitter_list_header * const
structuredvm_states;

/* A pointer to the current state, only accessible from VM code.  This is usable
   for pointer comparison when iterating over states. */
#define STRUCTUREDVM_OWN_STATE                           \
  ((struct structuredvm_state *) jitter_original_state)

/* Given an l-value of type struct structuredvm_state * (usually a variable name)
   expand to a for loop statement iterating over every existing structuredvm state
   using the l-value as iteration variable.  The expansion will execute the
   statement immediately following the macro call with the l-value in scope;
   in order words the loop body is not a macro argument, but follows the macro
   use.
   The l-value may be evaluated an unspecified number of times.
   This macro is safe to use within VM instruction code.
   For example:
     struct structuredvm_state *s;
     STRUCTUREDVM_FOR_EACH_STATE (s)
       printf ("This is a state: %p\n", s); // (but printf unsafe in VM code) */
#define STRUCTUREDVM_FOR_EACH_STATE(jitter_state_iteration_lvalue)     \
  for ((jitter_state_iteration_lvalue)                             \
          = structuredvm_states->first;                                \
       (jitter_state_iteration_lvalue)                             \
          != NULL;                                                 \
       (jitter_state_iteration_lvalue)                             \
         = (jitter_state_iteration_lvalue)->links.next)            \
    /* Here comes the body supplied by the user: no semicolon. */




/* Mutable routine initialization.
 * ************************************************************************** */

/* Return a freshly-allocated empty mutable routine for the structuredvm VM. */
struct jitter_mutable_routine*
structuredvm_make_mutable_routine (void)
  __attribute__ ((returns_nonnull));

/* Mutable routine finalization is actually VM-independent, but a definition of
   structuredvm_destroy_mutable_routine is provided below as a macro, for cosmetic
   reasons. */


/* Mutable routines: code generation C API.
 * ************************************************************************** */

/* This is the preferred way of adding a new VM instruction to a pointed
   routine, more efficient than structuredvm_mutable_routine_append_instruction_name
   even if only usable when the VM instruction opcode is known at compile time.
   The unspecialized instruction name must be explicitly mangled by the user as
   per the rules in jitterc_mangle.c .  For example an instruction named foo_bar
   can be added to the routine pointed by p with any one of
     structuredvm_mutable_routine_append_instruction_name (p, "foo_bar");
   ,
     STRUCTUREDVM_MUTABLE_ROUTINE_APPEND_INSTRUCTION (p, foo_ubar);
   , and
     STRUCTUREDVM_MUTABLE_ROUTINE_APPEND_INSTRUCTION_ID 
        (p, structuredvm_meta_instruction_id_foo_ubar);
   .
   The string "foo_bar" is not mangled, but the token foo_ubar is. */
#define STRUCTUREDVM_MUTABLE_ROUTINE_APPEND_INSTRUCTION(                 \
          routine_p, instruction_mangled_name_root)                  \
  do                                                                 \
    {                                                                \
      jitter_mutable_routine_append_meta_instruction                 \
         ((routine_p),                                               \
          structuredvm_meta_instructions                                 \
          + JITTER_CONCATENATE_TWO(structuredvm_meta_instruction_id_,    \
                                   instruction_mangled_name_root));  \
    }                                                                \
  while (false)

/* Append the unspecialized instruction whose id is given to the pointed routine.
   The id must be a case of enum structuredvm_meta_instruction_id ; such cases have
   a name starting with structuredvm_meta_instruction_id_ .
   This is slightly less convenient to use than STRUCTUREDVM_MUTABLE_ROUTINE_APPEND_INSTRUCTION
   but more general, as the instruction id is allowed to be a non-constant C
   expression. */
#define STRUCTUREDVM_MUTABLE_ROUTINE_APPEND_INSTRUCTION_ID(_jitter_routine_p,       \
                                                       _jitter_instruction_id)  \
  do                                                                            \
    {                                                                           \
      jitter_mutable_routine_append_instruction_id                              \
         ((_jitter_routine_p),                                                  \
          structuredvm_meta_instructions,                                           \
          STRUCTUREDVM_META_INSTRUCTION_NO,                                         \
          (_jitter_instruction_id));                                            \
    }                                                                           \
  while (false)

/* This is the preferred way of appending a register argument to the instruction
   being added to the pointed routine, more convenient than directly using
   structuredvm_mutable_routine_append_register_id_parameter , even if only usable
   when the register class is known at compile time.  Here the register class is
   only provided as a letter, but both the routine pointer and the register
   index are arbitrary C expressions.
   For example, in
     STRUCTUREDVM_MUTABLE_ROUTINE_APPEND_REGISTER_PARAMETER (p, r,
                                                         variable_to_index (x));
   the second macro argument "r" represents the register class named "r", and
   not the value of a variable named r. */
#define STRUCTUREDVM_MUTABLE_ROUTINE_APPEND_REGISTER_PARAMETER(routine_p,     \
                                                           class_letter,  \
                                                           index)         \
  do                                                                      \
    {                                                                     \
      structuredvm_mutable_routine_append_register_parameter                  \
         ((routine_p),                                                    \
          & JITTER_CONCATENATE_TWO(structuredvm_register_class_,              \
                                   class_letter),                         \
          (index));                                                       \
    }                                                                     \
  while (false)




/* Routine unified API: initialization.
 * ************************************************************************** */

/* See the comments above in "Mutable routines: initialization", and the
   implementation of the unified routine API in <jitter/jitter-routine.h> . */

#define structuredvm_make_routine structuredvm_make_mutable_routine




/* Routine unified API: code generation C API.
 * ************************************************************************** */

/* See the comments above in "Mutable routines: code generation C API". */

#define STRUCTUREDVM_ROUTINE_APPEND_INSTRUCTION  \
  STRUCTUREDVM_MUTABLE_ROUTINE_APPEND_INSTRUCTION
#define STRUCTUREDVM_ROUTINE_APPEND_INSTRUCTION_ID  \
  STRUCTUREDVM_MUTABLE_ROUTINE_APPEND_INSTRUCTION_ID
#define STRUCTUREDVM_ROUTINE_APPEND_REGISTER_PARAMETER  \
  STRUCTUREDVM_MUTABLE_ROUTINE_APPEND_REGISTER_PARAMETER




/* Array: special-purpose data.
 * ************************************************************************** */

/* The Array is a convenient place to store special-purpose data, accessible in
   an efficient way from a VM routine.
   Every item in special-purpose data is thread-local. */

/* The special-purpose data struct.  Every Array contains one of these at unbiased
   offset STRUCTUREDVM_SPECIAL_PURPOSE_STATE_DATA_UNBIASED_OFFSET from the unbiased
   beginning of the array.
   This entire struct is aligned to at least sizeof (jitter_int) bytes.  The
   entire struct is meant to be always accessed through a pointer-to-volatile,
   as its content may be altered from signal handlers and from different
   threads.  In particualar the user should use the macro
     STRUCTUREDVM_ARRAY_TO_SPECIAL_PURPOSE_STATE_DATA
   defined below and the macros defined from it as accessors.
   VM code accessing special-purpose data for its own state should use
     STRUCTUREDVM_SPECIAL_PURPOSE_STATE_DATA
   and the macros defined from it. */
struct jitter_special_purpose_state_data
{
  /* Notification fields.
   * ***************************************************************** */

  /* This is a Boolean flag, held as a word-sized datum so as to ensure
     atomicity in access.  It is also aligned to at least sizeof (jitter_int)
     bytes.
     Non-zero means that there is at least one notification pending, zero means
     that there are no notifications.  The flag specifies no other details: it
     is meant to be fast to check, with detailed information about each pending
     notification available elsewhere.
     It is the receiver's responsibility to periodically poll for notifications
     in application-specific "safe-points":
     A check can be inserted, for example, in all of these program points:
     a) at every backward branch;
     b) at every procedure entry;
     c) right after a call to each blocking primitive (as long as primitives
       can be interrupted).
     Safe-point checks are designed to be short and fast in the common case.  In
     the common case no action is required, and the VM routine should simply
     fall through.  If an action is required then control should branch off to a
     handler, where the user may implement the required behavior.
     It is mandatory that, as long as notifications can arrive, this field
     is reset to zero (when handling pending notifications) only by a thread
     running VM code in the state containing this struct.
     Other threads are allowed to set this to non-zero, in order to send a
     notification.  */
  jitter_int pending_notifications;

  /* Information about pending signal notifications.  If any signal is pending
     then pending_notifications must also be set, so that a notification check
     can always just quickly check pending_notifications, and then look at more
     details (including in pending_signal_notifications) only in the rare case
     of pending_notifications being true. */
  struct jitter_signal_notification *pending_signal_notifications;


  /* Profiling instrumentation fields.
   * ***************************************************************** */
  struct jitter_profile_runtime profile_runtime;
};




/* The Array and volatility.
 * ************************************************************************** */

/* Some fields of The Array, seen from VM code, are meant to be volatile, since
   they can be set by signal handlers or by other threads.  However it is
   acceptable to not see such changes immediately after they occur (notifications
   will get delayed, but not lost) and always accessing such data through a
   volatile struct is suboptimal.

   Non-VM code does need a volatile qualifier.

   Advanced dispatches already need a trick using inline assembly to make the
   base pointer (a biased pointer to The Array beginning) appear to
   spontaneously change beween instruction.  That is sufficient to express the
   degree of volatility required for this purpose.
   Simple dispatches, on targets where inline assembly may not be available at
   all, will use an actual volatile qualifier. */
#if defined (JITTER_DISPATCH_SWITCH)               \
    || defined (JITTER_DISPATCH_DIRECT_THREADING)
# define STRUCTUREDVM_ARRAY_VOLATILE_QUALIFIER volatile
#elif defined (JITTER_DISPATCH_MINIMAL_THREADING)  \
      || defined (JITTER_DISPATCH_NO_THREADING)
# define STRUCTUREDVM_ARRAY_VOLATILE_QUALIFIER /* nothing */
#else
# error "unknown dispatch: this should not happen"
#endif /* dispatch conditional */




/* Array element access: residuals, transfers, slow registers, and more.
 * ************************************************************************** */

/* In order to cover a wider range of addresses with simple base + register
   addressing the base does not necessarily point to the beginning of the Array;
   instead the base points to the beginning of the Array plus JITTER_ARRAY_BIAS
   bytes.
   FIXME: define the bias as a value appropriate to each architecture.  I think
   I should just move the definition to jitter-machine.h and provide a default
   here, in case the definition is missing on some architecture. */

/* FIXME: Horrible, horrible, horrible temporary workaround!

   This is a temporary workaround, very ugly and fragile, to compensate
   a limitation in jitter-specialize.c , which I will need to rewrite anyway.
   The problem is that jitter-specialize.c patches snippets to load non-label
   residuals in a VM-independent way based only on slow-register/memory residual
   indices, which is incorrect.  By using this particular bias I am cancelling
   that error.
   Test case, on a machine having only one register residual and a VM having just
     one fast register:
     [luca@moore ~/repos/jitter/_build/native-gcc-9]$ Q=bin/uninspired--no-threading; make $Q && echo 'mov 2, %r1' | libtool --mode=execute valgrind $Q --disassemble - --print-locations
   If this bias is wrong the slow-register accesses in mov/nR/%rR will use two
   different offsets, one for reading and another for writing.  With this
   workaround they will be the same.
   Good, with workadound (biased offset 0x0 from the base in %rbx):
    # 0x4a43d38: mov/nR/%rR 0x2, 0x20 (21 bytes):
        0x0000000004effb30 41 bc 02 00 00 00    	movl   $0x2,%r12d
        0x0000000004effb36 48 c7 43 00 20 00 00 00 	movq   $0x20,0x0(%rbx)
        0x0000000004effb3e 48 8b 13             	movq   (%rbx),%rdx
        0x0000000004effb41 4c 89 24 13          	movq   %r12,(%rbx,%rdx,1)
   Bad, with JITTER_ARRAY_BIAS defined as zero: first write at 0x0(%rbx)
                                                then read at 0x10(%rbx):
    # 0x4a43d38: mov/nR/%rR 0x2, 0x30 (22 bytes):
        0x0000000004effb30 41 bc 02 00 00 00    	movl   $0x2,%r12d
        0x0000000004effb36 48 c7 43 00 30 00 00 00 	movq   $0x30,0x0(%rbx)
        0x0000000004effb3e 48 8b 53 10          	movq   0x10(%rbx),%rdx
        0x0000000004effb42 4c 89 24 13          	movq   %r12,(%rbx,%rdx,1) */
#define JITTER_ARRAY_BIAS \
  (sizeof (struct jitter_special_purpose_state_data))
//#define JITTER_ARRAY_BIAS //0//(((jitter_int) 1 << 15))//(((jitter_int) 1 << 31))//0//0//16//0

/* Array-based globals are not implemented yet.  For the purpose of computing
   Array offsets I will say they are zero. */
#define STRUCTUREDVM_GLOBAL_NO 0

/* Transfer registers are not implemented yet.  For the purpose of computing
   Array offsets I will say they are zero. */
#define STRUCTUREDVM_TRANSFER_REGISTER_NO 0

/* Define macros holding offsets in bytes for the first global, memory residual
   and transfer register, from an initial Array pointer.
   In general we have to keep into account:
   - globals (word-sized);
   - special-purpose state data;
   - memory residuals (word-sized);
   - transfer registers (word-sized);
   - slow registers (structuredvm_any_register-sized and aligned).
   Notice that memory
   residuals (meaning residuals stored in The Array) are zero on dispatching
   modes different from no-threading.  This relies on
   STRUCTUREDVM_MAX_MEMORY_RESIDUAL_ARITY , defined below, which in its turn depends
   on STRUCTUREDVM_MAX_RESIDUAL_ARITY, which is machine-generated. */
#define STRUCTUREDVM_FIRST_GLOBAL_UNBIASED_OFFSET  \
  0
#define STRUCTUREDVM_SPECIAL_PURPOSE_STATE_DATA_UNBIASED_OFFSET  \
  (STRUCTUREDVM_FIRST_GLOBAL_UNBIASED_OFFSET                     \
   + sizeof (jitter_int) * STRUCTUREDVM_GLOBAL_NO)
#define STRUCTUREDVM_FIRST_MEMORY_RESIDUAL_UNBIASED_OFFSET   \
  (STRUCTUREDVM_SPECIAL_PURPOSE_STATE_DATA_UNBIASED_OFFSET   \
   + sizeof (struct jitter_special_purpose_state_data))
#define STRUCTUREDVM_FIRST_TRANSFER_REGISTER_UNBIASED_OFFSET        \
  (STRUCTUREDVM_FIRST_MEMORY_RESIDUAL_UNBIASED_OFFSET               \
   + sizeof (jitter_int) * STRUCTUREDVM_MAX_MEMORY_RESIDUAL_ARITY)
#define STRUCTUREDVM_FIRST_SLOW_REGISTER_UNBIASED_OFFSET          \
  JITTER_NEXT_MULTIPLE_OF_POSITIVE                            \
     (STRUCTUREDVM_FIRST_TRANSFER_REGISTER_UNBIASED_OFFSET        \
      + sizeof (jitter_int) * STRUCTUREDVM_TRANSFER_REGISTER_NO,  \
      sizeof (union structuredvm_any_register))

/* Expand to the offset of the special-purpose data struct from the Array
   biased beginning. */
#define STRUCTUREDVM_SPECIAL_PURPOSE_STATE_DATA_OFFSET       \
  (STRUCTUREDVM_SPECIAL_PURPOSE_STATE_DATA_UNBIASED_OFFSET   \
   - JITTER_ARRAY_BIAS)

/* Given an expression evaluating to the Array unbiased beginning, expand to
   an expression evaluating to a pointer to its special-purpose data.
   This is convenient for accessing special-purpose data from outside the
   state -- for example, to set the pending notification flag for another
   thread.
   There are two versions of this feature:
     STRUCTUREDVM_ARRAY_TO_SPECIAL_PURPOSE_STATE_DATA
   is meant to be used to access state data for some other thread, or in
   general out of VM code.
     STRUCTUREDVM_OWN_SPECIAL_PURPOSE_STATE_DATA
   is for VM code accessing its own special-purpose data. */
#define STRUCTUREDVM_ARRAY_TO_SPECIAL_PURPOSE_STATE_DATA_PRIVATE(qualifier,      \
                                                             array_address)  \
  ((qualifier struct jitter_special_purpose_state_data *)                    \
   (((char *) (array_address))                                               \
    + STRUCTUREDVM_SPECIAL_PURPOSE_STATE_DATA_UNBIASED_OFFSET))
#define STRUCTUREDVM_ARRAY_TO_SPECIAL_PURPOSE_STATE_DATA(array_address)       \
  STRUCTUREDVM_ARRAY_TO_SPECIAL_PURPOSE_STATE_DATA_PRIVATE (volatile,         \
                                                        (array_address))
#define STRUCTUREDVM_OWN_SPECIAL_PURPOSE_STATE_DATA          \
  STRUCTUREDVM_ARRAY_TO_SPECIAL_PURPOSE_STATE_DATA_PRIVATE   \
     (STRUCTUREDVM_ARRAY_VOLATILE_QUALIFIER,                 \
      ((char *) jitter_array_base) - JITTER_ARRAY_BIAS)

/* Given a state pointer, expand to an expression evaluating to a pointer to
   the state's special-purpose data.  This is meant for threads accessing
   other threads' special-purpose data, typically to set notifications. */
#define STRUCTUREDVM_STATE_TO_SPECIAL_PURPOSE_STATE_DATA(state_p)  \
  (STRUCTUREDVM_ARRAY_TO_SPECIAL_PURPOSE_STATE_DATA                \
     ((state_p)->structuredvm_state_backing.jitter_array))

/* Given a state pointer, expand to an expression evaluating to the
   pending_notification field for the state as an l-value.  This is meant for
   threads sending notifications to other threads. */
#define STRUCTUREDVM_STATE_TO_PENDING_NOTIFICATIONS(state_p)   \
  (STRUCTUREDVM_STATE_TO_SPECIAL_PURPOSE_STATE_DATA (state_p)  \
     ->pending_notifications)

/* Given a state pointer and a signal, expand to an l-value evaluating to a the
   pending field of the struct jitter_signal_notification element for the given
   signal in the pointed state.  This is meant for threads sending signal
   notifications to other threads and for C handler function. */
#define STRUCTUREDVM_STATE_AND_SIGNAL_TO_PENDING_SIGNAL_NOTIFICATION(state_p,    \
                                                                 signal_id)  \
  (((STRUCTUREDVM_STATE_TO_SPECIAL_PURPOSE_STATE_DATA (state_p)                   \
       ->pending_signal_notifications)                                        \
    + (signal_id))->pending)


/* Expand to the offset of the i-th register of class c in bytes from the Array
   beginning.
   The c argument must be a literal C (one-character) identifier.
   The i argument should always be a compile-time constant for performance, and
   it is in generated code.
   The i-th c-class register must be slow, otherwise the offset will be
   incorrect -- in fact fast registers are, hopefully, not in memory at all.

   Slow registers come in the Array ordered first by index, then by class.  For
   example if there are three classes "r" with 4 fast registers, "f" with 7 fast
   registers and "q" with 2 fast registers, slow registers can be accessed in
   this order:
     r4, f7, q2, r5, r8, q3, r6, r9, q4, and so on.
   Each contiguous group of slow registers spanning every class and starting
   from the first class (here for example <r5, r6, q3>) is called a "rank".
   This organization is convenient since changing the number of slow registers
   doesn't invalidate any offset computed in the past: the Array can simply be
   resized and its base pointer updated, without changing the code accessing it.

   This relies on macro such as STRUCTUREDVM_REGISTER_CLASS_NO and
   STRUCTUREDVM_REGISTER_?_FAST_REGISTER_NO and , defined below in machine-generated
   code. */
#define STRUCTUREDVM_SLOW_REGISTER_UNBIASED_OFFSET(c, i)                     \
  (STRUCTUREDVM_FIRST_SLOW_REGISTER_UNBIASED_OFFSET                          \
   + (sizeof (union structuredvm_any_register)                               \
      * (STRUCTUREDVM_REGISTER_CLASS_NO                                      \
         * ((i) - JITTER_CONCATENATE_THREE(STRUCTUREDVM_REGISTER_, c,        \
                                           _FAST_REGISTER_NO))           \
         + JITTER_CONCATENATE_THREE(STRUCTUREDVM_REGISTER_, c, _CLASS_ID))))

/* Expand to the offset of the i-th register of class c in bytes from the base,
   keeping the bias into account. */
#define STRUCTUREDVM_SLOW_REGISTER_OFFSET(c, i)                              \
  (STRUCTUREDVM_SLOW_REGISTER_UNBIASED_OFFSET(c, i) - JITTER_ARRAY_BIAS)

/* Expand to the Array size in bytes, assuming the given number of slow
   registers per class.  This is an allocation size, ignoring the bias. */
#define STRUCTUREDVM_ARRAY_SIZE(slow_register_per_class_no)                  \
  (STRUCTUREDVM_FIRST_SLOW_REGISTER_UNBIASED_OFFSET                          \
   + (sizeof (union structuredvm_any_register)                               \
      * STRUCTUREDVM_REGISTER_CLASS_NO                                       \
      * (slow_register_per_class_no)))




/* Residual access.
 * ************************************************************************** */

/* How many residuals we can have at most in memory, which is to say,
   without counting residuals kept in reserved registers.

   Implementation note: it would be wrong here to use a CPP conditional based on
   the value of STRUCTUREDVM_MAX_RESIDUAL_ARITY , as I was doing in a preliminary
   version.  That lead to a tricky bug, since STRUCTUREDVM_MAX_RESIDUAL_ARITY ,
   which is defined below but is not yet available here, simply counted as 0
   for the purposes of evaluating the CPP condititional. */
#ifdef JITTER_DISPATCH_NO_THREADING
  /* We are using no-threading dispatch.  If there are no more residuals
     than reserved residual registers then we never need to keep any in
     memory.  Otherwise we need to keep as many residuals in memory as the
     total number of residuals minus how many registers are reserved for
     them. */
# define STRUCTUREDVM_MAX_MEMORY_RESIDUAL_ARITY                          \
    ((STRUCTUREDVM_MAX_RESIDUAL_ARITY <= JITTER_RESIDUAL_REGISTER_NO)    \
     ? 0                                                             \
     : (STRUCTUREDVM_MAX_RESIDUAL_ARITY - JITTER_RESIDUAL_REGISTER_NO))
#else // Not no-threading.
  /* No registers are reserved for residuals in this dispatching mode; even if
     in fact all residuals are memory residuals they don't count here, since
     residuals are not held in The Array in this dispatching mode. */
# define STRUCTUREDVM_MAX_MEMORY_RESIDUAL_ARITY  \
  0
#endif // #ifdef JITTER_DISPATCH_NO_THREADING

#ifdef JITTER_DISPATCH_NO_THREADING
/* Expand to the offset from the base, in bytes, of the i-th residual.  The
   given index must be greater than or equal to JITTER_RESIDUAL_REGISTER_NO;
   residuals with indices lower than that number are not stored in The Array
   at all.
   This is not useful with any of the other dispatching modes, where residuals
   directly follow each VM instruction opcode or thread.  For good performance i
   should always be a compile-time constant, as it is in machine-generated
   code.
   Residuals always have the size of a jitter word, even if some register class
   may be wider. */
/* FIXME: if later I use a different policy than simply checking
   JITTER_RESIDUAL_REGISTER_NO to decide how many residuals to keep in
   registers, then I have to change this or meet very nasty bugs. */
# define STRUCTUREDVM_RESIDUAL_UNBIASED_OFFSET(i)                      \
    (STRUCTUREDVM_FIRST_MEMORY_RESIDUAL_UNBIASED_OFFSET                \
     + (sizeof (jitter_int) * (i - JITTER_RESIDUAL_REGISTER_NO)))
# define STRUCTUREDVM_RESIDUAL_OFFSET(i)  \
    (STRUCTUREDVM_RESIDUAL_UNBIASED_OFFSET(i) - JITTER_ARRAY_BIAS)
#endif // #ifdef JITTER_DISPATCH_NO_THREADING



/* Mutable routine text frontend.
 * ************************************************************************** */

/* Parse VM code from the given file or string into the pointed VM routine,
   which is allowed but not required to be empty.
   These are simple wrappers around functions implemented in the Bison file. */
void
structuredvm_parse_mutable_routine_from_file_star (FILE *input_file,
                                               struct jitter_mutable_routine *p)
  __attribute__ ((nonnull (1, 2)));
void
structuredvm_parse_mutable_routine_from_file (const char *input_file_name,
                                          struct jitter_mutable_routine *p)
  __attribute__ ((nonnull (1, 2)));
void
structuredvm_parse_mutable_routine_from_string (const char *string,
                                            struct jitter_mutable_routine *p)
  __attribute__ ((nonnull (1, 2)));




/* Unified routine text frontend.
 * ************************************************************************** */

/* The C wrappers for the ordinary API can be reused for the unified API, since
   it internally works with mutable routines. */
#define structuredvm_parse_routine_from_file_star  \
  structuredvm_parse_mutable_routine_from_file_star
#define structuredvm_parse_routine_from_file  \
  structuredvm_parse_mutable_routine_from_file
#define structuredvm_parse_routine_from_string  \
  structuredvm_parse_mutable_routine_from_string




/* Machine-generated data structures.
 * ************************************************************************** */

/* Declare a few machine-generated data structures, which together define a VM. */

/* Threads or pointers to native code blocks of course don't exist with
   switch-dispatching. */
#ifndef JITTER_DISPATCH_SWITCH
/* Every possible thread, indexed by enum jitter_specialized_instruction_opcode .
   This is used at specialization time, and the user shouldn't need to touch
   it. */
extern const jitter_thread *
structuredvm_threads;

/* VM instruction end label.  These are not all reachable at run time, but
   having them in a global array might prevent older GCCs from being too clever
   in reordering blocks. */
extern const jitter_thread *
structuredvm_thread_ends;

/* The size, in chars, of each thread's native code.  The elements are in the
   same order of structuredvm_threads.  Sizes could conceptually be of type size_t ,
   but in order to be defensive I'm storing pointer differences as signed
   values, so that we may catch compilation problems: if any VM instruction end
   *precedes* its VM instruction beginning, then the compiler has reordered
   labels, which would have disastrous effects with replicated code. */
extern const long *
structuredvm_thread_sizes;
#endif // #ifndef JITTER_DISPATCH_SWITCH

/* This is defined in the machine-generated vm/meta-instructions.c . */
extern struct jitter_hash_table
structuredvm_meta_instruction_hash;

/* An array specifying every existing meta-instruction, defined in the order of
   enum structuredvm_meta_instruction_id .  This is defined in vm/meta-instructions.c ,
   which is machine-generated. */
extern const struct jitter_meta_instruction
structuredvm_meta_instructions [];

/* An array whose indices are specialised instruction opcodes, and
   whose elements are the corresponding unspecialised instructions
   opcodes -- or -1 when there is no mapping mapping having */
extern const int
structuredvm_specialized_instruction_to_unspecialized_instruction [];

/* How many residual parameters each specialized instruction has.  The
   actual array definition is machine-generated. */
extern const size_t
structuredvm_specialized_instruction_residual_arities [];

/* An array of bitmasks, one per specialized instruction.  Each bitmask holds
   one bit per residual argument, counting from the least significant (the first
   residual arg maps to element & (1 << 0), the second to element & (1 << 1),
   and so on).
   Each bit is 1 if and only if the corresponding residual argument is a label
   or a fast label.
   Only residual arguments are counted: for example a specialized instruction
   foo_n1_lR_r2 would have a mask with the *first* bit set. */
extern const unsigned long // FIXME: possibly use a shorter type when possible
structuredvm_specialized_instruction_label_bitmasks [];

/* Like structuredvm_specialized_instruction_label_bitmasks , but for fast labels
   only.
   The actual definition is conditionalized so as to appear only when
   needed according to the dispatching model. */
extern const unsigned long // FIXME: possibly use a shorter type when possible
structuredvm_specialized_instruction_fast_label_bitmasks [];

/* An array of booleans in which each element is true iff the specialized
   instruction whose opcode is the index is relocatable. */
extern const bool
structuredvm_specialized_instruction_relocatables [];

/* An array of booleans in which each element is true iff the specialized
   instruction whose opcode is the index is a caller. */
extern const bool
structuredvm_specialized_instruction_callers [];

/* An array of booleans in which each element is true iff the specialized
   instruction whose opcode is the index is a callee. */
extern const bool
structuredvm_specialized_instruction_callees [];

/* This big array of strings contains the name of each specialized instruction,
   in the order of enum structuredvm_specialized_instruction_opcode . */
extern const char* const
structuredvm_specialized_instruction_names [];


/* A pointer to a struct containing const pointers to the structures above, plus
   sizes; there will be only one instance of this per VM, machine-generated.
   Each program data structure contains a pointer to that instance, so that
   VM-independent functions, given a program, will have everything needed to
   work.  The one instance of struct jitter_vm for the structuredvm VM. */
extern struct jitter_vm * const
structuredvm_vm;

/* A pointer to a struct containing VM-specific parameters set in part when
   calling jitterc and in part when compiling the generated C code, such as the
   dispatching model and the number of fast registers.  The data is fully
   initialized only after a call to structuredvm_initialize . */
extern const
struct jitter_vm_configuration * const
structuredvm_vm_configuration;




/* Compatibility macros.
 * ************************************************************************** */

/* It is convenient, for future extensibility, to expose an interface in which
   some VM-independent functions and data structures actually look as if they
   were specific to the user VM. */

/* What the user refers to as struct structuredvm_mutable_routine is actually a
   struct jitter_mutable_routine , whose definition is VM-independent. */
#define structuredvm_mutable_routine jitter_mutable_routine

/* Same for executable routines. */
#define structuredvm_executable_routine jitter_executable_routine

/* Same for unified routines. */
#define structuredvm_routine jitter_routine

/* Destroy a non-executable routine (routine initialization is actually
   VM-specific). */
#define structuredvm_destroy_mutable_routine jitter_destroy_mutable_routine

/* Destroy a unified routine. */
#define structuredvm_destroy_routine jitter_destroy_routine

/* Pin a unified routine. */
#define structuredvm_pin_routine jitter_pin_routine

/* Unpin a unified routine. */
#define structuredvm_unpin_routine jitter_unpin_routine

/* Print VM configuration. */
#define structuredvm_print_vm_configuration jitter_print_vm_configuration

/* Generic routine construction API. */
#define structuredvm_label \
  jitter_label
#define structuredvm_fresh_label \
  jitter_fresh_label

/* Mutable routine option API. */
#define structuredvm_set_mutable_routine_option_slow_literals_only \
  jitter_set_mutable_routine_option_slow_literals_only
#define structuredvm_set_mutable_routine_option_slow_registers_only \
  jitter_set_mutable_routine_option_slow_registers_only
#define structuredvm_set_mutable_routine_option_slow_literals_and_registers_only \
  jitter_set_mutable_routine_option_slow_literals_and_registers_only
#define structuredvm_set_mutable_routine_option_add_final_exitvm \
  jitter_set_mutable_routine_option_add_final_exitvm
#define structuredvm_set_mutable_routine_option_optimization_rewriting \
  jitter_set_mutable_routine_option_optimization_rewriting

/* Printing and disassembling: ordinary API. */
#define structuredvm_mutable_routine_print \
  jitter_mutable_routine_print
#define structuredvm_executable_routine_disassemble \
  jitter_executable_routine_disassemble

/* Mutable routine construction API. */
#define structuredvm_mutable_routine_append_instruction_name \
  jitter_mutable_routine_append_instruction_name
#define structuredvm_mutable_routine_append_meta_instruction \
  jitter_mutable_routine_append_meta_instruction
#define structuredvm_mutable_routine_append_label \
  jitter_mutable_routine_append_label
#define structuredvm_mutable_routine_append_symbolic_label \
  jitter_mutable_routine_append_symbolic_label
#define structuredvm_mutable_routine_append_register_parameter \
  jitter_mutable_routine_append_register_parameter
#define structuredvm_mutable_routine_append_literal_parameter \
  jitter_mutable_routine_append_literal_parameter
#define structuredvm_mutable_routine_append_signed_literal_parameter \
  jitter_mutable_routine_append_signed_literal_parameter
#define structuredvm_mutable_routine_append_unsigned_literal_parameter \
  jitter_mutable_routine_append_unsigned_literal_parameter
#define structuredvm_mutable_routine_append_pointer_literal_parameter \
  jitter_mutable_routine_append_pointer_literal_parameter
#define structuredvm_mutable_routine_append_label_parameter \
  jitter_mutable_routine_append_label_parameter
#define structuredvm_mutable_routine_append_symbolic_label_parameter \
  jitter_mutable_routine_append_symbolic_label_parameter

/* Mutable routine destruction. */
#define structuredvm_destroy_executable_routine \
  jitter_destroy_executable_routine

/* Making executable routines from mutable routines. */
#define structuredvm_make_executable_routine \
  jitter_make_executable_routine

/* Unified routine option API. */
#define structuredvm_set_routine_option_slow_literals_only \
  jitter_set_mutable_routine_option_slow_literals_only
#define structuredvm_set_routine_option_slow_registers_only \
  jitter_set_mutable_routine_option_slow_registers_only
#define structuredvm_set_routine_option_slow_literals_and_registers_only \
  jitter_set_mutable_routine_option_slow_literals_and_registers_only
#define structuredvm_set_routine_option_add_final_exitvm \
  jitter_set_mutable_routine_option_add_final_exitvm
#define structuredvm_set_routine_option_optimization_rewriting \
  jitter_set_mutable_routine_option_optimization_rewriting

/* Printing and disassembling: unified API.  These do not follow the pattern of
   the rest: wrapped identifiers here are the names of C functions specific to
   the unified API */
#define structuredvm_routine_print \
  jitter_routine_print
#define structuredvm_routine_disassemble \
  jitter_routine_disassemble

/* Unified routine construction API. */
#define structuredvm_routine_append_instruction_name \
  jitter_mutable_routine_append_instruction_name
#define structuredvm_routine_append_meta_instruction \
  jitter_mutable_routine_append_meta_instruction
#define structuredvm_routine_append_label \
  jitter_mutable_routine_append_label
#define structuredvm_routine_append_symbolic_label \
  jitter_mutable_routine_append_symbolic_label
#define structuredvm_routine_append_register_parameter \
  jitter_mutable_routine_append_register_parameter
#define structuredvm_routine_append_literal_parameter \
  jitter_mutable_routine_append_literal_parameter
#define structuredvm_routine_append_signed_literal_parameter \
  jitter_mutable_routine_append_signed_literal_parameter
#define structuredvm_routine_append_unsigned_literal_parameter \
  jitter_mutable_routine_append_unsigned_literal_parameter
#define structuredvm_routine_append_pointer_literal_parameter \
  jitter_mutable_routine_append_pointer_literal_parameter
#define structuredvm_routine_append_label_parameter \
  jitter_mutable_routine_append_label_parameter
#define structuredvm_routine_append_symbolic_label_parameter \
  jitter_mutable_routine_append_symbolic_label_parameter

/* Mutable routine destruction. */
#define structuredvm_destroy_routine                                           \
  /* This does not follow the pattern of the rest: the wrapped identifier  \
     here is the name of a C function specific to the unified API. */      \
  jitter_destroy_routine

/* The unified API has no facility to explicitly make executable routines: their
   very existence is hidden.  For this reason some of the macros above, such
   structuredvm_make_executable_routine, have no unified counterpart here. */

/* Profiling.  Apart from structuredvm_state_profile, which returns a pointer to
   the profile within a pointed state structure, everything else here has the
   same API as the functionality in jitter/jitter-profile.h , without the VM
   pointer.
   Notice that this API does nothing useful onless one of the CPP macros
   JITTER_PROFILE_COUNT or JITTER_PROFILE_SAMPLE is defined. */
#define structuredvm_profile_runtime  \
  jitter_profile_runtime /* the struct name */
#define structuredvm_profile  \
  jitter_profile /* the struct name */
// FIXME: no: distinguish between struct jitter_profile_runtime and its user-friendly variant
struct jitter_profile_runtime *
structuredvm_state_profile_runtime (struct structuredvm_state *s)
  __attribute__ ((returns_nonnull, nonnull (1)));
struct structuredvm_profile_runtime*
structuredvm_profile_runtime_make (void)
  __attribute__ ((returns_nonnull));
#define structuredvm_profile_destroy jitter_profile_destroy
void
structuredvm_profile_runtime_clear (struct structuredvm_profile_runtime *p)
  __attribute__ ((nonnull (1)));
void
structuredvm_profile_runtime_merge_from (struct structuredvm_profile_runtime *to,
                                     const struct structuredvm_profile_runtime *from)
  __attribute__ ((nonnull (1, 2)));
void
structuredvm_profile_runtime_merge_from_state (struct structuredvm_profile_runtime *to,
                                   const struct structuredvm_state *from_state)
  __attribute__ ((nonnull (1, 2)));
struct structuredvm_profile *
structuredvm_profile_unspecialized_from_runtime
   (const struct structuredvm_profile_runtime *p)
  __attribute__ ((returns_nonnull, nonnull (1)));
struct structuredvm_profile *
structuredvm_profile_specialized_from_runtime (const struct structuredvm_profile_runtime
                                           *p)
  __attribute__ ((returns_nonnull, nonnull (1)));
void
structuredvm_profile_runtime_print_unspecialized
   (jitter_print_context ct,
    const struct structuredvm_profile_runtime *p)
  __attribute__ ((nonnull (1, 2)));
void
structuredvm_profile_runtime_print_specialized (jitter_print_context ct,
                                            const struct structuredvm_profile_runtime
                                            *p)
  __attribute__ ((nonnull (1, 2)));




/* Register class types.
 * ************************************************************************** */

/* Return a pointer to a statically allocated register class descriptor, given
   the register class character, or NULL if the character does not represent a
   valid register class.

   A constant array indexed by a character would have been more efficient, but
   relying on character ordering is not portable, at least in theory.  A
   non-constant array could be initialized in a portable way, but that would
   probably not be worth the trouble. */
const struct jitter_register_class *
structuredvm_register_class_character_to_register_class (char c)
  __attribute__ ((pure));


/* A constant array of constant pointers to every existing register class
   descriptor, ordered by class id; each pointer within the array refers the
   only existing class descriptor for its class.  The number of elements is
   STRUCTUREDVM_REGISTER_CLASS_NO , but that is not declared because the definition
   of STRUCTUREDVM_REGISTER_CLASS_NO comes later in generated code.

   This is useful when the user code enumerates every existing register class,
   particularly for debugging. */
extern const struct jitter_register_class * const
structuredvm_regiter_classes [];




/* Array re-allocation.
 * ************************************************************************** */

/* Make the Array in the pointed state large enough to accommodate the given
   number of slow reigsters per class, adjusting the Array pointer as needed
   and recording information about the new size in the state; change nothing
   if the array is already large enough.  Return the new base.
   For example passing 3 as the value of slow_register_no would make
   place for three slow registers per register class: if the current VM had two
   classes 'r' and 'f' than the function would ensure that the Array can hold
   three 'r' and three 'f' slow registers, independently from the number
   of fast 'r' or 'f' registers.
   Any new elements allocated in the Array are left uninitialized, but its old
   content remains valid. */
char *
structuredvm_make_place_for_slow_registers (struct structuredvm_state *s,
                                        jitter_int slow_register_no_per_class)
  __attribute__ ((noinline));




/* **************************************************************************
 * Evrything following this point is for internal use only.
 * ************************************************************************** */




/* Defect tables.
 * ************************************************************************** */

/* It is harmless to declare these unconditionally, even if they only used when
   patch-ins are available.  See jitter/jitter-defect.h .*/

/* The worst-case defect table.  This is a global constant array, having one
   element per specialized instruction. */
extern const jitter_uint
structuredvm_worst_case_defect_table [];

/* The actual defect table, to be filled at initialization time. */
extern jitter_uint
structuredvm_defect_table [];




/* Instruction rewriter.
 * ************************************************************************** */

/* Try to apply each rewrite rule in order and run the first one that matches,
   if any, on the pointed program.  When a rule fires the following ones are not
   checked but if a rule, after removing the last few instructions, adds another
   one, the addition will trigger another rewrite in its turn, and so on until
   no more rewriting is possible.  The rewriting process is inherently
   recursive.

   The implementation of this function is machine-generated, but the user can
   add her own code in the rewriter-c block, which ends up near the beginning of
   this function body, right after JITTTER_REWRITE_FUNCTION_PROLOG_ .  The
   formal argument seen from the body is named jitter_mutable_routine_p .

   Rationale: the argument is named differently in the body in order to keep
   the namespace conventions and, more importantly, to encourage the user to
   read this comment.

   The user must *not* append labels to the VM routines during rewriting: that
   would break it.  The user is responsible for destroying any instruction she
   removes, including their arguments.  The user can assume that
   jitter_rewritable_instruction_no is strictly greater than zero. */
void
structuredvm_rewrite (struct jitter_mutable_routine *jitter_mutable_routine_p);




/* Program points at run time in executable routines.
 * ************************************************************************** */

/* Provide a nice name for a program point type which looks VM-dependent. */
typedef jitter_program_point
structuredvm_program_point;

/* Again, provide a VM-dependent alias for an actually VM-independent macro. */
#define STRUCTUREDVM_EXECUTABLE_ROUTINE_BEGINNING(_jitter_executable_routine_ptr)  \
  JITTER_EXECUTABLE_ROUTINE_BEGINNING(_jitter_executable_routine_ptr)




/* Program points at run time in routines: unified routine API.
 * ************************************************************************** */

/* Like STRUCTUREDVM_EXECUTABLE_ROUTINE_BEGINNING for the unified routine API. */
#define STRUCTUREDVM_ROUTINE_BEGINNING(_jitter_routine)                \
  JITTER_EXECUTABLE_ROUTINE_BEGINNING                              \
     (jitter_routine_make_executable_if_needed (_jitter_routine))



/* Executing code from an executable routine.
 * ************************************************************************** */

/* Make sure that the pointed state has enough slow registers to run the pointed
   executable routine; if that is not the case, allocate more slow registers. */
void
structuredvm_ensure_enough_slow_registers_for_executable_routine
   (const struct jitter_executable_routine *er, struct structuredvm_state *s)
  __attribute__ ((nonnull (1, 2)));

/* Run VM code starting from the given program point (which must belong to some
   executable routine), in the pointed VM state.

   Since no executable routine is given this cannot automatically guarantee that
   the slow registers in the pointed state are in sufficient number; it is the
   user's responsibility to check, if needed.

   This function is also usable with the unified routine API. */
void
structuredvm_branch_to_program_point (structuredvm_program_point p,
                                  struct structuredvm_state *s)
  __attribute__ ((nonnull (1, 2)));

/* Run VM code starting from the beginning of the pointed executable routine,
   in the pointed VM state.  This does ensure that the slow registers in
   the pointed state are in sufficient number, by calling
   structuredvm_ensure_enough_slow_registers_for .
   This function is slightly less efficient than
   structuredvm_branch_to_program_point , and structuredvm_branch_to_program_point
   should be preferred in contexts where C code repeatedly calls VM code. */
void
structuredvm_execute_executable_routine (const struct jitter_executable_routine *er,
                                     struct structuredvm_state *s)
  __attribute__ ((nonnull (1, 2)));




/* Executing code: unified routine API.
 * ************************************************************************** */

/* Like structuredvm_ensure_enough_slow_registers_for_executable_routine , with the
   unified API. */
void
structuredvm_ensure_enough_slow_registers_for_routine
   (jitter_routine r, struct structuredvm_state *s)
  __attribute__ ((nonnull (1, 2)));

/* structuredvm_branch_to_program_point , declared above, is also usable with the
   unified routine API. */

/* Like structuredvm_execute_executable_routine, for a unified routine. */
void
structuredvm_execute_routine (jitter_routine r,
                          struct structuredvm_state *s)
  __attribute__ ((nonnull (1, 2)));




/* Low-level debugging features relying on assembly: data locations.
 * ************************************************************************** */

/* Dump human-readable information about data locations to the given print
   context.
   This is a trivial VM-dependent wrapper around jitter_dump_data_locations,
   which does not require a struct jitter_vm pointer as input. */
void
structuredvm_dump_data_locations (jitter_print_context output)
  __attribute__ ((nonnull (1)));




/* Sample profiling: internal API.
 * ************************************************************************** */

/* The functions in this sections are used internally by vm2.c, only when
   sample-profiling is enabled.  In fact these functions are not defined at all
   otherwise. */

/* Initialise global sampling-related structures. */
// FIXME: no: distinguish struct jitter_profile_runtime and struct jitter_profile
void
structuredvm_profile_sample_initialize (void);

/* Begin sampling. */
void
structuredvm_profile_sample_start (struct structuredvm_state *state)
  __attribute__ ((nonnull (1)));

/* Stop sampling. */
void
structuredvm_profile_sample_stop (void);




/* Machine-generated code.
 * ************************************************************************** */

/* What follows could be conceptually split into several generated header files,
   but having too many files would be inconvenient for the user to compile and
   link.  For this reason we generate a single header. */

/* User-specified code, early header part: beginning. */

/* User-specified code, early header part: end */

/* Configuration data for struct jitter_vm_configuration. */
#define STRUCTUREDVM_VM_NAME JITTER_STRINGIFY(Structuredvm)
#define STRUCTUREDVM_LOWER_CASE_PREFIX "structuredvm"
#define STRUCTUREDVM_UPPER_CASE_PREFIX "STRUCTUREDVM"
#define STRUCTUREDVM_DISPATCH_HUMAN_READABLE \
  JITTER_DISPATCH_NAME_STRING
#define STRUCTUREDVM_MAX_FAST_REGISTER_NO_PER_CLASS 5
#define STRUCTUREDVM_MAX_NONRESIDUAL_LITERAL_NO -1


/* For each register class define the register type, a unique index, and the
   number of fast registers.  Indices are useful for computing slow register
   offsets.  For each register class declare a global register class
   descriptor, convenient to use when generating unspecialized instructions
   from the C API.*/
typedef
union jitter_word structuredvm_register_r;
#define STRUCTUREDVM_REGISTER_r_CLASS_ID 0
#define STRUCTUREDVM_REGISTER_r_FAST_REGISTER_NO 2
extern const struct jitter_register_class
structuredvm_register_class_r;

/* How many register classes we have. */
#define STRUCTUREDVM_REGISTER_CLASS_NO  1

/* A union large enough to hold a register of any class, or a machine word. */
union structuredvm_any_register
{
  /* In any case the union must be at least as large as a machine word. */
  jitter_int jitter_unused_field;

  structuredvm_register_r r /* A r-class register */;
};

/* An enumeration of all structuredvm register classes. */
enum structuredvm_register_class_id
  {
    structuredvm_register_class_id_r = STRUCTUREDVM_REGISTER_r_CLASS_ID,

    /* The number of register class ids, not valid as a class id itself. */
    structuredvm_register_class_id_no = STRUCTUREDVM_REGISTER_CLASS_NO
  };

/* A macro expanding to a statement initialising a rank of slow
   registers.  The argument has type union structuredvm_any_register *
   and points to the first register in a rank. */
#define STRUCTUREDVM_INITIALIZE_SLOW_REGISTER_RANK(rank) \
  do \
    { \
      union structuredvm_any_register *_jitter_rank __attribute__ ((unused)) \
        = (rank); \
      _jitter_rank [0].r = (union jitter_word) ((union jitter_word) {.fixnum = 0}); \
    } \
  while (false)


#ifndef STRUCTUREDVM_STATE_H_
#define STRUCTUREDVM_STATE_H_

//#include <jitter/jitter.h>

/* Early C code from the user for the state definition. */
/* End of the early C code from the user for the state definition. */

/* The VM state backing. */
struct structuredvm_state_backing
{
  /* The Array.  This initial pointer is kept in the backing, since it is
     not normally needed at run time.  By subtracting JITTER_ARRAY_BIAS from
     it (as a pointer to char) we get the base pointer. */
  char *jitter_array;

  /* How many slow registers per class the Array can hold, without being
     reallocated.  This number is always the same for evey class. */
  jitter_int jitter_slow_register_no_per_class;

  /* Stack backing data structures. */
  struct jitter_stack_backing jitter_stack_mainstack_backing;

  /* State backing fields added in C by the user. */

  /* End of the state backing fields added in C by the user. */
};

/* The VM state runtime data structure, using memory from the VM state backing. */
struct structuredvm_state_runtime
{
#if    defined(JITTER_DISPATCH_SWITCH)                   \
    || defined(JITTER_DISPATCH_DIRECT_THREADING)         \
    || defined(JITTER_DISPATCH_MINIMAL_THREADING)        \
    || (   defined(JITTER_DISPATCH_NO_THREADING)         \
        && ! defined(JITTER_MACHINE_SUPPORTS_PROCEDURE))
  /* A link register for branch-and-link operations.  This field must *not*
     be accessed from user code, as it may not exist on all dispatching
     models.  It is only used internally for JITTER_PROCEDURE_PROLOG. */
  const union jitter_word *_jitter_link;
#endif

  /* With recent GCC versions (as of Summer 2017) the *last* declared fields
     are the most likely to be allocated in registers; this is why VM registers
     are in reverse order here.  The first few fast registers will be the "fastest"
     ones, allocated in hardware registers; they may be followed by other fast
     fast allocated on the stack at known offsets, with intermediate performance; then
     come the slow registers.  In critical code the users should prefer a register with as
     small an index as possible for best performance. */
  structuredvm_register_r jitter_fast_register_r_1;
  structuredvm_register_r jitter_fast_register_r_0;

  /* Stack runtime data structures. */
  JITTER_STACK_TOS_DECLARATION(jitter_int, mainstack);

  /* State runtime fields added in C by the user. */

  /* End of the state runtime fields added in C by the user. */
};

/* A struct holding both the backing and the runtime part of the VM state. */
struct structuredvm_state
{
  /* Pointers to the previous and next VM state for this VM. */
  struct jitter_list_links links;

  /* Each state data structure contains its backing. */
  struct structuredvm_state_backing structuredvm_state_backing;

  /* Each state data structure contains its runtime data structures,
     to be allocated to registers as long as possible, and using
     memory from the backing. */
  struct structuredvm_state_runtime structuredvm_state_runtime;
};
#endif // #ifndef STRUCTUREDVM_STATE_H_
#ifndef STRUCTUREDVM_META_INSTRUCTIONS_H_
#define STRUCTUREDVM_META_INSTRUCTIONS_H_

enum structuredvm_meta_instruction_id
  {
    structuredvm_meta_instruction_id_b = 0,
    structuredvm_meta_instruction_id_be = 1,
    structuredvm_meta_instruction_id_beqi_mstack = 2,
    structuredvm_meta_instruction_id_beqr_mstack = 3,
    structuredvm_meta_instruction_id_bf_mstack = 4,
    structuredvm_meta_instruction_id_bg = 5,
    structuredvm_meta_instruction_id_bge = 6,
    structuredvm_meta_instruction_id_bger_mstack = 7,
    structuredvm_meta_instruction_id_bl = 8,
    structuredvm_meta_instruction_id_ble = 9,
    structuredvm_meta_instruction_id_bne = 10,
    structuredvm_meta_instruction_id_bneqi_mstack = 11,
    structuredvm_meta_instruction_id_bneqr_mstack = 12,
    structuredvm_meta_instruction_id_bt_mstack = 13,
    structuredvm_meta_instruction_id_copy_mto_mr_mstack = 14,
    structuredvm_meta_instruction_id_different_mstack = 15,
    structuredvm_meta_instruction_id_differenti_mstack = 16,
    structuredvm_meta_instruction_id_divided = 17,
    structuredvm_meta_instruction_id_divided_mstack = 18,
    structuredvm_meta_instruction_id_drop_mstack = 19,
    structuredvm_meta_instruction_id_dup_mstack = 20,
    structuredvm_meta_instruction_id_equal_mstack = 21,
    structuredvm_meta_instruction_id_equali_mstack = 22,
    structuredvm_meta_instruction_id_exitvm = 23,
    structuredvm_meta_instruction_id_greater_mstack = 24,
    structuredvm_meta_instruction_id_greaterorequal_mstack = 25,
    structuredvm_meta_instruction_id_input = 26,
    structuredvm_meta_instruction_id_input_mstack = 27,
    structuredvm_meta_instruction_id_isnonzero_mstack = 28,
    structuredvm_meta_instruction_id_less_mstack = 29,
    structuredvm_meta_instruction_id_lessorequal_mstack = 30,
    structuredvm_meta_instruction_id_logicaland_mstack = 31,
    structuredvm_meta_instruction_id_logicalnot_mstack = 32,
    structuredvm_meta_instruction_id_logicalor_mstack = 33,
    structuredvm_meta_instruction_id_minus = 34,
    structuredvm_meta_instruction_id_minus_mstack = 35,
    structuredvm_meta_instruction_id_minusi_mstack = 36,
    structuredvm_meta_instruction_id_minusr_mstack = 37,
    structuredvm_meta_instruction_id_mov = 38,
    structuredvm_meta_instruction_id_plus = 39,
    structuredvm_meta_instruction_id_plus_mstack = 40,
    structuredvm_meta_instruction_id_plusi_mstack = 41,
    structuredvm_meta_instruction_id_pop_mstack = 42,
    structuredvm_meta_instruction_id_print = 43,
    structuredvm_meta_instruction_id_print_mstack = 44,
    structuredvm_meta_instruction_id_push_mstack = 45,
    structuredvm_meta_instruction_id_pushr_mbeqr_mstack = 46,
    structuredvm_meta_instruction_id_pushr_mbger_mstack = 47,
    structuredvm_meta_instruction_id_pushr_mminusr_mpop_mstack = 48,
    structuredvm_meta_instruction_id_remainder = 49,
    structuredvm_meta_instruction_id_remainder_mstack = 50,
    structuredvm_meta_instruction_id_times = 51,
    structuredvm_meta_instruction_id_times_mstack = 52,
    structuredvm_meta_instruction_id_uminus = 53,
    structuredvm_meta_instruction_id_uminus_mstack = 54,
    structuredvm_meta_instruction_id_unreachable = 55
  };

#define STRUCTUREDVM_META_INSTRUCTION_NO 56

/* The longest meta-instruction name length, not mangled, without
   counting the final '\0' character. */
#define STRUCTUREDVM_MAX_META_INSTRUCTION_NAME_LENGTH 22

#endif // #ifndef STRUCTUREDVM_META_INSTRUCTIONS_H_
#ifndef STRUCTUREDVM_SPECIALIZED_INSTRUCTIONS_H_
#define STRUCTUREDVM_SPECIALIZED_INSTRUCTIONS_H_

enum structuredvm_specialized_instruction_opcode
  {
    structuredvm_specialized_instruction_opcode__eINVALID = 0,
    structuredvm_specialized_instruction_opcode__eBEGINBASICBLOCK = 1,
    structuredvm_specialized_instruction_opcode__eEXITVM = 2,
    structuredvm_specialized_instruction_opcode__eDATALOCATIONS = 3,
    structuredvm_specialized_instruction_opcode__eNOP = 4,
    structuredvm_specialized_instruction_opcode__eUNREACHABLE0 = 5,
    structuredvm_specialized_instruction_opcode__eUNREACHABLE1 = 6,
    structuredvm_specialized_instruction_opcode__eUNREACHABLE2 = 7,
    structuredvm_specialized_instruction_opcode_b__fR = 8,
    structuredvm_specialized_instruction_opcode_be___rr0___rr0__fR = 9,
    structuredvm_specialized_instruction_opcode_be___rr0___rr1__fR = 10,
    structuredvm_specialized_instruction_opcode_be___rr0___rrR__fR = 11,
    structuredvm_specialized_instruction_opcode_be___rr0__n0__fR = 12,
    structuredvm_specialized_instruction_opcode_be___rr0__nR__fR = 13,
    structuredvm_specialized_instruction_opcode_be___rr1___rr0__fR = 14,
    structuredvm_specialized_instruction_opcode_be___rr1___rr1__fR = 15,
    structuredvm_specialized_instruction_opcode_be___rr1___rrR__fR = 16,
    structuredvm_specialized_instruction_opcode_be___rr1__n0__fR = 17,
    structuredvm_specialized_instruction_opcode_be___rr1__nR__fR = 18,
    structuredvm_specialized_instruction_opcode_be___rrR___rr0__fR = 19,
    structuredvm_specialized_instruction_opcode_be___rrR___rr1__fR = 20,
    structuredvm_specialized_instruction_opcode_be___rrR___rrR__fR = 21,
    structuredvm_specialized_instruction_opcode_be___rrR__n0__fR = 22,
    structuredvm_specialized_instruction_opcode_be___rrR__nR__fR = 23,
    structuredvm_specialized_instruction_opcode_be__n0___rr0__fR = 24,
    structuredvm_specialized_instruction_opcode_be__n0___rr1__fR = 25,
    structuredvm_specialized_instruction_opcode_be__n0___rrR__fR = 26,
    structuredvm_specialized_instruction_opcode_be__n0__n0__fR = 27,
    structuredvm_specialized_instruction_opcode_be__n0__nR__fR = 28,
    structuredvm_specialized_instruction_opcode_be__nR___rr0__fR = 29,
    structuredvm_specialized_instruction_opcode_be__nR___rr1__fR = 30,
    structuredvm_specialized_instruction_opcode_be__nR___rrR__fR = 31,
    structuredvm_specialized_instruction_opcode_be__nR__n0__fR = 32,
    structuredvm_specialized_instruction_opcode_be__nR__nR__fR = 33,
    structuredvm_specialized_instruction_opcode_beqi_mstack__n_m1__fR = 34,
    structuredvm_specialized_instruction_opcode_beqi_mstack__n0__fR = 35,
    structuredvm_specialized_instruction_opcode_beqi_mstack__n1__fR = 36,
    structuredvm_specialized_instruction_opcode_beqi_mstack__n2__fR = 37,
    structuredvm_specialized_instruction_opcode_beqi_mstack__nR__fR = 38,
    structuredvm_specialized_instruction_opcode_beqr_mstack___rr0__fR = 39,
    structuredvm_specialized_instruction_opcode_beqr_mstack___rr1__fR = 40,
    structuredvm_specialized_instruction_opcode_beqr_mstack___rrR__fR = 41,
    structuredvm_specialized_instruction_opcode_bf_mstack__fR = 42,
    structuredvm_specialized_instruction_opcode_bg___rr0___rr0__fR = 43,
    structuredvm_specialized_instruction_opcode_bg___rr0___rr1__fR = 44,
    structuredvm_specialized_instruction_opcode_bg___rr0___rrR__fR = 45,
    structuredvm_specialized_instruction_opcode_bg___rr0__n0__fR = 46,
    structuredvm_specialized_instruction_opcode_bg___rr0__nR__fR = 47,
    structuredvm_specialized_instruction_opcode_bg___rr1___rr0__fR = 48,
    structuredvm_specialized_instruction_opcode_bg___rr1___rr1__fR = 49,
    structuredvm_specialized_instruction_opcode_bg___rr1___rrR__fR = 50,
    structuredvm_specialized_instruction_opcode_bg___rr1__n0__fR = 51,
    structuredvm_specialized_instruction_opcode_bg___rr1__nR__fR = 52,
    structuredvm_specialized_instruction_opcode_bg___rrR___rr0__fR = 53,
    structuredvm_specialized_instruction_opcode_bg___rrR___rr1__fR = 54,
    structuredvm_specialized_instruction_opcode_bg___rrR___rrR__fR = 55,
    structuredvm_specialized_instruction_opcode_bg___rrR__n0__fR = 56,
    structuredvm_specialized_instruction_opcode_bg___rrR__nR__fR = 57,
    structuredvm_specialized_instruction_opcode_bg__n0___rr0__fR = 58,
    structuredvm_specialized_instruction_opcode_bg__n0___rr1__fR = 59,
    structuredvm_specialized_instruction_opcode_bg__n0___rrR__fR = 60,
    structuredvm_specialized_instruction_opcode_bg__n0__n0__fR = 61,
    structuredvm_specialized_instruction_opcode_bg__n0__nR__fR = 62,
    structuredvm_specialized_instruction_opcode_bg__nR___rr0__fR = 63,
    structuredvm_specialized_instruction_opcode_bg__nR___rr1__fR = 64,
    structuredvm_specialized_instruction_opcode_bg__nR___rrR__fR = 65,
    structuredvm_specialized_instruction_opcode_bg__nR__n0__fR = 66,
    structuredvm_specialized_instruction_opcode_bg__nR__nR__fR = 67,
    structuredvm_specialized_instruction_opcode_bge___rr0___rr0__fR = 68,
    structuredvm_specialized_instruction_opcode_bge___rr0___rr1__fR = 69,
    structuredvm_specialized_instruction_opcode_bge___rr0___rrR__fR = 70,
    structuredvm_specialized_instruction_opcode_bge___rr0__n0__fR = 71,
    structuredvm_specialized_instruction_opcode_bge___rr0__nR__fR = 72,
    structuredvm_specialized_instruction_opcode_bge___rr1___rr0__fR = 73,
    structuredvm_specialized_instruction_opcode_bge___rr1___rr1__fR = 74,
    structuredvm_specialized_instruction_opcode_bge___rr1___rrR__fR = 75,
    structuredvm_specialized_instruction_opcode_bge___rr1__n0__fR = 76,
    structuredvm_specialized_instruction_opcode_bge___rr1__nR__fR = 77,
    structuredvm_specialized_instruction_opcode_bge___rrR___rr0__fR = 78,
    structuredvm_specialized_instruction_opcode_bge___rrR___rr1__fR = 79,
    structuredvm_specialized_instruction_opcode_bge___rrR___rrR__fR = 80,
    structuredvm_specialized_instruction_opcode_bge___rrR__n0__fR = 81,
    structuredvm_specialized_instruction_opcode_bge___rrR__nR__fR = 82,
    structuredvm_specialized_instruction_opcode_bge__n0___rr0__fR = 83,
    structuredvm_specialized_instruction_opcode_bge__n0___rr1__fR = 84,
    structuredvm_specialized_instruction_opcode_bge__n0___rrR__fR = 85,
    structuredvm_specialized_instruction_opcode_bge__n0__n0__fR = 86,
    structuredvm_specialized_instruction_opcode_bge__n0__nR__fR = 87,
    structuredvm_specialized_instruction_opcode_bge__nR___rr0__fR = 88,
    structuredvm_specialized_instruction_opcode_bge__nR___rr1__fR = 89,
    structuredvm_specialized_instruction_opcode_bge__nR___rrR__fR = 90,
    structuredvm_specialized_instruction_opcode_bge__nR__n0__fR = 91,
    structuredvm_specialized_instruction_opcode_bge__nR__nR__fR = 92,
    structuredvm_specialized_instruction_opcode_bger_mstack___rr0__fR = 93,
    structuredvm_specialized_instruction_opcode_bger_mstack___rr1__fR = 94,
    structuredvm_specialized_instruction_opcode_bger_mstack___rrR__fR = 95,
    structuredvm_specialized_instruction_opcode_bl___rr0___rr0__fR = 96,
    structuredvm_specialized_instruction_opcode_bl___rr0___rr1__fR = 97,
    structuredvm_specialized_instruction_opcode_bl___rr0___rrR__fR = 98,
    structuredvm_specialized_instruction_opcode_bl___rr0__n0__fR = 99,
    structuredvm_specialized_instruction_opcode_bl___rr0__nR__fR = 100,
    structuredvm_specialized_instruction_opcode_bl___rr1___rr0__fR = 101,
    structuredvm_specialized_instruction_opcode_bl___rr1___rr1__fR = 102,
    structuredvm_specialized_instruction_opcode_bl___rr1___rrR__fR = 103,
    structuredvm_specialized_instruction_opcode_bl___rr1__n0__fR = 104,
    structuredvm_specialized_instruction_opcode_bl___rr1__nR__fR = 105,
    structuredvm_specialized_instruction_opcode_bl___rrR___rr0__fR = 106,
    structuredvm_specialized_instruction_opcode_bl___rrR___rr1__fR = 107,
    structuredvm_specialized_instruction_opcode_bl___rrR___rrR__fR = 108,
    structuredvm_specialized_instruction_opcode_bl___rrR__n0__fR = 109,
    structuredvm_specialized_instruction_opcode_bl___rrR__nR__fR = 110,
    structuredvm_specialized_instruction_opcode_bl__n0___rr0__fR = 111,
    structuredvm_specialized_instruction_opcode_bl__n0___rr1__fR = 112,
    structuredvm_specialized_instruction_opcode_bl__n0___rrR__fR = 113,
    structuredvm_specialized_instruction_opcode_bl__n0__n0__fR = 114,
    structuredvm_specialized_instruction_opcode_bl__n0__nR__fR = 115,
    structuredvm_specialized_instruction_opcode_bl__nR___rr0__fR = 116,
    structuredvm_specialized_instruction_opcode_bl__nR___rr1__fR = 117,
    structuredvm_specialized_instruction_opcode_bl__nR___rrR__fR = 118,
    structuredvm_specialized_instruction_opcode_bl__nR__n0__fR = 119,
    structuredvm_specialized_instruction_opcode_bl__nR__nR__fR = 120,
    structuredvm_specialized_instruction_opcode_ble___rr0___rr0__fR = 121,
    structuredvm_specialized_instruction_opcode_ble___rr0___rr1__fR = 122,
    structuredvm_specialized_instruction_opcode_ble___rr0___rrR__fR = 123,
    structuredvm_specialized_instruction_opcode_ble___rr0__n0__fR = 124,
    structuredvm_specialized_instruction_opcode_ble___rr0__nR__fR = 125,
    structuredvm_specialized_instruction_opcode_ble___rr1___rr0__fR = 126,
    structuredvm_specialized_instruction_opcode_ble___rr1___rr1__fR = 127,
    structuredvm_specialized_instruction_opcode_ble___rr1___rrR__fR = 128,
    structuredvm_specialized_instruction_opcode_ble___rr1__n0__fR = 129,
    structuredvm_specialized_instruction_opcode_ble___rr1__nR__fR = 130,
    structuredvm_specialized_instruction_opcode_ble___rrR___rr0__fR = 131,
    structuredvm_specialized_instruction_opcode_ble___rrR___rr1__fR = 132,
    structuredvm_specialized_instruction_opcode_ble___rrR___rrR__fR = 133,
    structuredvm_specialized_instruction_opcode_ble___rrR__n0__fR = 134,
    structuredvm_specialized_instruction_opcode_ble___rrR__nR__fR = 135,
    structuredvm_specialized_instruction_opcode_ble__n0___rr0__fR = 136,
    structuredvm_specialized_instruction_opcode_ble__n0___rr1__fR = 137,
    structuredvm_specialized_instruction_opcode_ble__n0___rrR__fR = 138,
    structuredvm_specialized_instruction_opcode_ble__n0__n0__fR = 139,
    structuredvm_specialized_instruction_opcode_ble__n0__nR__fR = 140,
    structuredvm_specialized_instruction_opcode_ble__nR___rr0__fR = 141,
    structuredvm_specialized_instruction_opcode_ble__nR___rr1__fR = 142,
    structuredvm_specialized_instruction_opcode_ble__nR___rrR__fR = 143,
    structuredvm_specialized_instruction_opcode_ble__nR__n0__fR = 144,
    structuredvm_specialized_instruction_opcode_ble__nR__nR__fR = 145,
    structuredvm_specialized_instruction_opcode_bne___rr0___rr0__fR = 146,
    structuredvm_specialized_instruction_opcode_bne___rr0___rr1__fR = 147,
    structuredvm_specialized_instruction_opcode_bne___rr0___rrR__fR = 148,
    structuredvm_specialized_instruction_opcode_bne___rr0__n0__fR = 149,
    structuredvm_specialized_instruction_opcode_bne___rr0__nR__fR = 150,
    structuredvm_specialized_instruction_opcode_bne___rr1___rr0__fR = 151,
    structuredvm_specialized_instruction_opcode_bne___rr1___rr1__fR = 152,
    structuredvm_specialized_instruction_opcode_bne___rr1___rrR__fR = 153,
    structuredvm_specialized_instruction_opcode_bne___rr1__n0__fR = 154,
    structuredvm_specialized_instruction_opcode_bne___rr1__nR__fR = 155,
    structuredvm_specialized_instruction_opcode_bne___rrR___rr0__fR = 156,
    structuredvm_specialized_instruction_opcode_bne___rrR___rr1__fR = 157,
    structuredvm_specialized_instruction_opcode_bne___rrR___rrR__fR = 158,
    structuredvm_specialized_instruction_opcode_bne___rrR__n0__fR = 159,
    structuredvm_specialized_instruction_opcode_bne___rrR__nR__fR = 160,
    structuredvm_specialized_instruction_opcode_bne__n0___rr0__fR = 161,
    structuredvm_specialized_instruction_opcode_bne__n0___rr1__fR = 162,
    structuredvm_specialized_instruction_opcode_bne__n0___rrR__fR = 163,
    structuredvm_specialized_instruction_opcode_bne__n0__n0__fR = 164,
    structuredvm_specialized_instruction_opcode_bne__n0__nR__fR = 165,
    structuredvm_specialized_instruction_opcode_bne__nR___rr0__fR = 166,
    structuredvm_specialized_instruction_opcode_bne__nR___rr1__fR = 167,
    structuredvm_specialized_instruction_opcode_bne__nR___rrR__fR = 168,
    structuredvm_specialized_instruction_opcode_bne__nR__n0__fR = 169,
    structuredvm_specialized_instruction_opcode_bne__nR__nR__fR = 170,
    structuredvm_specialized_instruction_opcode_bneqi_mstack__n_m1__fR = 171,
    structuredvm_specialized_instruction_opcode_bneqi_mstack__n0__fR = 172,
    structuredvm_specialized_instruction_opcode_bneqi_mstack__n1__fR = 173,
    structuredvm_specialized_instruction_opcode_bneqi_mstack__n2__fR = 174,
    structuredvm_specialized_instruction_opcode_bneqi_mstack__nR__fR = 175,
    structuredvm_specialized_instruction_opcode_bneqr_mstack___rr0__fR = 176,
    structuredvm_specialized_instruction_opcode_bneqr_mstack___rr1__fR = 177,
    structuredvm_specialized_instruction_opcode_bneqr_mstack___rrR__fR = 178,
    structuredvm_specialized_instruction_opcode_bt_mstack__fR = 179,
    structuredvm_specialized_instruction_opcode_copy_mto_mr_mstack___rr0 = 180,
    structuredvm_specialized_instruction_opcode_copy_mto_mr_mstack___rr1 = 181,
    structuredvm_specialized_instruction_opcode_copy_mto_mr_mstack___rrR = 182,
    structuredvm_specialized_instruction_opcode_different_mstack = 183,
    structuredvm_specialized_instruction_opcode_differenti_mstack__n0 = 184,
    structuredvm_specialized_instruction_opcode_differenti_mstack__n1 = 185,
    structuredvm_specialized_instruction_opcode_differenti_mstack__n2 = 186,
    structuredvm_specialized_instruction_opcode_differenti_mstack__nR = 187,
    structuredvm_specialized_instruction_opcode_divided___rr0___rr0___rr0 = 188,
    structuredvm_specialized_instruction_opcode_divided___rr0___rr0___rr1 = 189,
    structuredvm_specialized_instruction_opcode_divided___rr0___rr0___rrR = 190,
    structuredvm_specialized_instruction_opcode_divided___rr0___rr1___rr0 = 191,
    structuredvm_specialized_instruction_opcode_divided___rr0___rr1___rr1 = 192,
    structuredvm_specialized_instruction_opcode_divided___rr0___rr1___rrR = 193,
    structuredvm_specialized_instruction_opcode_divided___rr0___rrR___rr0 = 194,
    structuredvm_specialized_instruction_opcode_divided___rr0___rrR___rr1 = 195,
    structuredvm_specialized_instruction_opcode_divided___rr0___rrR___rrR = 196,
    structuredvm_specialized_instruction_opcode_divided___rr0__n2___rr0 = 197,
    structuredvm_specialized_instruction_opcode_divided___rr0__n2___rr1 = 198,
    structuredvm_specialized_instruction_opcode_divided___rr0__n2___rrR = 199,
    structuredvm_specialized_instruction_opcode_divided___rr0__nR___rr0 = 200,
    structuredvm_specialized_instruction_opcode_divided___rr0__nR___rr1 = 201,
    structuredvm_specialized_instruction_opcode_divided___rr0__nR___rrR = 202,
    structuredvm_specialized_instruction_opcode_divided___rr1___rr0___rr0 = 203,
    structuredvm_specialized_instruction_opcode_divided___rr1___rr0___rr1 = 204,
    structuredvm_specialized_instruction_opcode_divided___rr1___rr0___rrR = 205,
    structuredvm_specialized_instruction_opcode_divided___rr1___rr1___rr0 = 206,
    structuredvm_specialized_instruction_opcode_divided___rr1___rr1___rr1 = 207,
    structuredvm_specialized_instruction_opcode_divided___rr1___rr1___rrR = 208,
    structuredvm_specialized_instruction_opcode_divided___rr1___rrR___rr0 = 209,
    structuredvm_specialized_instruction_opcode_divided___rr1___rrR___rr1 = 210,
    structuredvm_specialized_instruction_opcode_divided___rr1___rrR___rrR = 211,
    structuredvm_specialized_instruction_opcode_divided___rr1__n2___rr0 = 212,
    structuredvm_specialized_instruction_opcode_divided___rr1__n2___rr1 = 213,
    structuredvm_specialized_instruction_opcode_divided___rr1__n2___rrR = 214,
    structuredvm_specialized_instruction_opcode_divided___rr1__nR___rr0 = 215,
    structuredvm_specialized_instruction_opcode_divided___rr1__nR___rr1 = 216,
    structuredvm_specialized_instruction_opcode_divided___rr1__nR___rrR = 217,
    structuredvm_specialized_instruction_opcode_divided___rrR___rr0___rr0 = 218,
    structuredvm_specialized_instruction_opcode_divided___rrR___rr0___rr1 = 219,
    structuredvm_specialized_instruction_opcode_divided___rrR___rr0___rrR = 220,
    structuredvm_specialized_instruction_opcode_divided___rrR___rr1___rr0 = 221,
    structuredvm_specialized_instruction_opcode_divided___rrR___rr1___rr1 = 222,
    structuredvm_specialized_instruction_opcode_divided___rrR___rr1___rrR = 223,
    structuredvm_specialized_instruction_opcode_divided___rrR___rrR___rr0 = 224,
    structuredvm_specialized_instruction_opcode_divided___rrR___rrR___rr1 = 225,
    structuredvm_specialized_instruction_opcode_divided___rrR___rrR___rrR = 226,
    structuredvm_specialized_instruction_opcode_divided___rrR__n2___rr0 = 227,
    structuredvm_specialized_instruction_opcode_divided___rrR__n2___rr1 = 228,
    structuredvm_specialized_instruction_opcode_divided___rrR__n2___rrR = 229,
    structuredvm_specialized_instruction_opcode_divided___rrR__nR___rr0 = 230,
    structuredvm_specialized_instruction_opcode_divided___rrR__nR___rr1 = 231,
    structuredvm_specialized_instruction_opcode_divided___rrR__nR___rrR = 232,
    structuredvm_specialized_instruction_opcode_divided__nR___rr0___rr0 = 233,
    structuredvm_specialized_instruction_opcode_divided__nR___rr0___rr1 = 234,
    structuredvm_specialized_instruction_opcode_divided__nR___rr0___rrR = 235,
    structuredvm_specialized_instruction_opcode_divided__nR___rr1___rr0 = 236,
    structuredvm_specialized_instruction_opcode_divided__nR___rr1___rr1 = 237,
    structuredvm_specialized_instruction_opcode_divided__nR___rr1___rrR = 238,
    structuredvm_specialized_instruction_opcode_divided__nR___rrR___rr0 = 239,
    structuredvm_specialized_instruction_opcode_divided__nR___rrR___rr1 = 240,
    structuredvm_specialized_instruction_opcode_divided__nR___rrR___rrR = 241,
    structuredvm_specialized_instruction_opcode_divided__nR__n2___rr0 = 242,
    structuredvm_specialized_instruction_opcode_divided__nR__n2___rr1 = 243,
    structuredvm_specialized_instruction_opcode_divided__nR__n2___rrR = 244,
    structuredvm_specialized_instruction_opcode_divided__nR__nR___rr0 = 245,
    structuredvm_specialized_instruction_opcode_divided__nR__nR___rr1 = 246,
    structuredvm_specialized_instruction_opcode_divided__nR__nR___rrR = 247,
    structuredvm_specialized_instruction_opcode_divided_mstack = 248,
    structuredvm_specialized_instruction_opcode_drop_mstack = 249,
    structuredvm_specialized_instruction_opcode_dup_mstack = 250,
    structuredvm_specialized_instruction_opcode_equal_mstack = 251,
    structuredvm_specialized_instruction_opcode_equali_mstack__n0 = 252,
    structuredvm_specialized_instruction_opcode_equali_mstack__n1 = 253,
    structuredvm_specialized_instruction_opcode_equali_mstack__n2 = 254,
    structuredvm_specialized_instruction_opcode_equali_mstack__nR = 255,
    structuredvm_specialized_instruction_opcode_exitvm = 256,
    structuredvm_specialized_instruction_opcode_greater_mstack = 257,
    structuredvm_specialized_instruction_opcode_greaterorequal_mstack = 258,
    structuredvm_specialized_instruction_opcode_input___rr0__retR = 259,
    structuredvm_specialized_instruction_opcode_input___rr1__retR = 260,
    structuredvm_specialized_instruction_opcode_input___rrR__retR = 261,
    structuredvm_specialized_instruction_opcode_input_mstack__retR = 262,
    structuredvm_specialized_instruction_opcode_isnonzero_mstack = 263,
    structuredvm_specialized_instruction_opcode_less_mstack = 264,
    structuredvm_specialized_instruction_opcode_lessorequal_mstack = 265,
    structuredvm_specialized_instruction_opcode_logicaland_mstack = 266,
    structuredvm_specialized_instruction_opcode_logicalnot_mstack = 267,
    structuredvm_specialized_instruction_opcode_logicalor_mstack = 268,
    structuredvm_specialized_instruction_opcode_minus___rr0___rr0___rr0 = 269,
    structuredvm_specialized_instruction_opcode_minus___rr0___rr0___rr1 = 270,
    structuredvm_specialized_instruction_opcode_minus___rr0___rr0___rrR = 271,
    structuredvm_specialized_instruction_opcode_minus___rr0___rr1___rr0 = 272,
    structuredvm_specialized_instruction_opcode_minus___rr0___rr1___rr1 = 273,
    structuredvm_specialized_instruction_opcode_minus___rr0___rr1___rrR = 274,
    structuredvm_specialized_instruction_opcode_minus___rr0___rrR___rr0 = 275,
    structuredvm_specialized_instruction_opcode_minus___rr0___rrR___rr1 = 276,
    structuredvm_specialized_instruction_opcode_minus___rr0___rrR___rrR = 277,
    structuredvm_specialized_instruction_opcode_minus___rr0__n1___rr0 = 278,
    structuredvm_specialized_instruction_opcode_minus___rr0__n1___rr1 = 279,
    structuredvm_specialized_instruction_opcode_minus___rr0__n1___rrR = 280,
    structuredvm_specialized_instruction_opcode_minus___rr0__n2___rr0 = 281,
    structuredvm_specialized_instruction_opcode_minus___rr0__n2___rr1 = 282,
    structuredvm_specialized_instruction_opcode_minus___rr0__n2___rrR = 283,
    structuredvm_specialized_instruction_opcode_minus___rr0__nR___rr0 = 284,
    structuredvm_specialized_instruction_opcode_minus___rr0__nR___rr1 = 285,
    structuredvm_specialized_instruction_opcode_minus___rr0__nR___rrR = 286,
    structuredvm_specialized_instruction_opcode_minus___rr1___rr0___rr0 = 287,
    structuredvm_specialized_instruction_opcode_minus___rr1___rr0___rr1 = 288,
    structuredvm_specialized_instruction_opcode_minus___rr1___rr0___rrR = 289,
    structuredvm_specialized_instruction_opcode_minus___rr1___rr1___rr0 = 290,
    structuredvm_specialized_instruction_opcode_minus___rr1___rr1___rr1 = 291,
    structuredvm_specialized_instruction_opcode_minus___rr1___rr1___rrR = 292,
    structuredvm_specialized_instruction_opcode_minus___rr1___rrR___rr0 = 293,
    structuredvm_specialized_instruction_opcode_minus___rr1___rrR___rr1 = 294,
    structuredvm_specialized_instruction_opcode_minus___rr1___rrR___rrR = 295,
    structuredvm_specialized_instruction_opcode_minus___rr1__n1___rr0 = 296,
    structuredvm_specialized_instruction_opcode_minus___rr1__n1___rr1 = 297,
    structuredvm_specialized_instruction_opcode_minus___rr1__n1___rrR = 298,
    structuredvm_specialized_instruction_opcode_minus___rr1__n2___rr0 = 299,
    structuredvm_specialized_instruction_opcode_minus___rr1__n2___rr1 = 300,
    structuredvm_specialized_instruction_opcode_minus___rr1__n2___rrR = 301,
    structuredvm_specialized_instruction_opcode_minus___rr1__nR___rr0 = 302,
    structuredvm_specialized_instruction_opcode_minus___rr1__nR___rr1 = 303,
    structuredvm_specialized_instruction_opcode_minus___rr1__nR___rrR = 304,
    structuredvm_specialized_instruction_opcode_minus___rrR___rr0___rr0 = 305,
    structuredvm_specialized_instruction_opcode_minus___rrR___rr0___rr1 = 306,
    structuredvm_specialized_instruction_opcode_minus___rrR___rr0___rrR = 307,
    structuredvm_specialized_instruction_opcode_minus___rrR___rr1___rr0 = 308,
    structuredvm_specialized_instruction_opcode_minus___rrR___rr1___rr1 = 309,
    structuredvm_specialized_instruction_opcode_minus___rrR___rr1___rrR = 310,
    structuredvm_specialized_instruction_opcode_minus___rrR___rrR___rr0 = 311,
    structuredvm_specialized_instruction_opcode_minus___rrR___rrR___rr1 = 312,
    structuredvm_specialized_instruction_opcode_minus___rrR___rrR___rrR = 313,
    structuredvm_specialized_instruction_opcode_minus___rrR__n1___rr0 = 314,
    structuredvm_specialized_instruction_opcode_minus___rrR__n1___rr1 = 315,
    structuredvm_specialized_instruction_opcode_minus___rrR__n1___rrR = 316,
    structuredvm_specialized_instruction_opcode_minus___rrR__n2___rr0 = 317,
    structuredvm_specialized_instruction_opcode_minus___rrR__n2___rr1 = 318,
    structuredvm_specialized_instruction_opcode_minus___rrR__n2___rrR = 319,
    structuredvm_specialized_instruction_opcode_minus___rrR__nR___rr0 = 320,
    structuredvm_specialized_instruction_opcode_minus___rrR__nR___rr1 = 321,
    structuredvm_specialized_instruction_opcode_minus___rrR__nR___rrR = 322,
    structuredvm_specialized_instruction_opcode_minus__nR___rr0___rr0 = 323,
    structuredvm_specialized_instruction_opcode_minus__nR___rr0___rr1 = 324,
    structuredvm_specialized_instruction_opcode_minus__nR___rr0___rrR = 325,
    structuredvm_specialized_instruction_opcode_minus__nR___rr1___rr0 = 326,
    structuredvm_specialized_instruction_opcode_minus__nR___rr1___rr1 = 327,
    structuredvm_specialized_instruction_opcode_minus__nR___rr1___rrR = 328,
    structuredvm_specialized_instruction_opcode_minus__nR___rrR___rr0 = 329,
    structuredvm_specialized_instruction_opcode_minus__nR___rrR___rr1 = 330,
    structuredvm_specialized_instruction_opcode_minus__nR___rrR___rrR = 331,
    structuredvm_specialized_instruction_opcode_minus__nR__n1___rr0 = 332,
    structuredvm_specialized_instruction_opcode_minus__nR__n1___rr1 = 333,
    structuredvm_specialized_instruction_opcode_minus__nR__n1___rrR = 334,
    structuredvm_specialized_instruction_opcode_minus__nR__n2___rr0 = 335,
    structuredvm_specialized_instruction_opcode_minus__nR__n2___rr1 = 336,
    structuredvm_specialized_instruction_opcode_minus__nR__n2___rrR = 337,
    structuredvm_specialized_instruction_opcode_minus__nR__nR___rr0 = 338,
    structuredvm_specialized_instruction_opcode_minus__nR__nR___rr1 = 339,
    structuredvm_specialized_instruction_opcode_minus__nR__nR___rrR = 340,
    structuredvm_specialized_instruction_opcode_minus_mstack = 341,
    structuredvm_specialized_instruction_opcode_minusi_mstack__n1 = 342,
    structuredvm_specialized_instruction_opcode_minusi_mstack__n2 = 343,
    structuredvm_specialized_instruction_opcode_minusi_mstack__nR = 344,
    structuredvm_specialized_instruction_opcode_minusr_mstack___rr0 = 345,
    structuredvm_specialized_instruction_opcode_minusr_mstack___rr1 = 346,
    structuredvm_specialized_instruction_opcode_minusr_mstack___rrR = 347,
    structuredvm_specialized_instruction_opcode_mov___rr0___rr0 = 348,
    structuredvm_specialized_instruction_opcode_mov___rr0___rr1 = 349,
    structuredvm_specialized_instruction_opcode_mov___rr0___rrR = 350,
    structuredvm_specialized_instruction_opcode_mov___rr1___rr0 = 351,
    structuredvm_specialized_instruction_opcode_mov___rr1___rr1 = 352,
    structuredvm_specialized_instruction_opcode_mov___rr1___rrR = 353,
    structuredvm_specialized_instruction_opcode_mov___rrR___rr0 = 354,
    structuredvm_specialized_instruction_opcode_mov___rrR___rr1 = 355,
    structuredvm_specialized_instruction_opcode_mov___rrR___rrR = 356,
    structuredvm_specialized_instruction_opcode_mov__n0___rr0 = 357,
    structuredvm_specialized_instruction_opcode_mov__n0___rr1 = 358,
    structuredvm_specialized_instruction_opcode_mov__n0___rrR = 359,
    structuredvm_specialized_instruction_opcode_mov__n1___rr0 = 360,
    structuredvm_specialized_instruction_opcode_mov__n1___rr1 = 361,
    structuredvm_specialized_instruction_opcode_mov__n1___rrR = 362,
    structuredvm_specialized_instruction_opcode_mov__n_m1___rr0 = 363,
    structuredvm_specialized_instruction_opcode_mov__n_m1___rr1 = 364,
    structuredvm_specialized_instruction_opcode_mov__n_m1___rrR = 365,
    structuredvm_specialized_instruction_opcode_mov__n2___rr0 = 366,
    structuredvm_specialized_instruction_opcode_mov__n2___rr1 = 367,
    structuredvm_specialized_instruction_opcode_mov__n2___rrR = 368,
    structuredvm_specialized_instruction_opcode_mov__nR___rr0 = 369,
    structuredvm_specialized_instruction_opcode_mov__nR___rr1 = 370,
    structuredvm_specialized_instruction_opcode_mov__nR___rrR = 371,
    structuredvm_specialized_instruction_opcode_plus___rr0___rr0___rr0 = 372,
    structuredvm_specialized_instruction_opcode_plus___rr0___rr0___rr1 = 373,
    structuredvm_specialized_instruction_opcode_plus___rr0___rr0___rrR = 374,
    structuredvm_specialized_instruction_opcode_plus___rr0___rr1___rr0 = 375,
    structuredvm_specialized_instruction_opcode_plus___rr0___rr1___rr1 = 376,
    structuredvm_specialized_instruction_opcode_plus___rr0___rr1___rrR = 377,
    structuredvm_specialized_instruction_opcode_plus___rr0___rrR___rr0 = 378,
    structuredvm_specialized_instruction_opcode_plus___rr0___rrR___rr1 = 379,
    structuredvm_specialized_instruction_opcode_plus___rr0___rrR___rrR = 380,
    structuredvm_specialized_instruction_opcode_plus___rr0__n1___rr0 = 381,
    structuredvm_specialized_instruction_opcode_plus___rr0__n1___rr1 = 382,
    structuredvm_specialized_instruction_opcode_plus___rr0__n1___rrR = 383,
    structuredvm_specialized_instruction_opcode_plus___rr0__n2___rr0 = 384,
    structuredvm_specialized_instruction_opcode_plus___rr0__n2___rr1 = 385,
    structuredvm_specialized_instruction_opcode_plus___rr0__n2___rrR = 386,
    structuredvm_specialized_instruction_opcode_plus___rr0__nR___rr0 = 387,
    structuredvm_specialized_instruction_opcode_plus___rr0__nR___rr1 = 388,
    structuredvm_specialized_instruction_opcode_plus___rr0__nR___rrR = 389,
    structuredvm_specialized_instruction_opcode_plus___rr1___rr0___rr0 = 390,
    structuredvm_specialized_instruction_opcode_plus___rr1___rr0___rr1 = 391,
    structuredvm_specialized_instruction_opcode_plus___rr1___rr0___rrR = 392,
    structuredvm_specialized_instruction_opcode_plus___rr1___rr1___rr0 = 393,
    structuredvm_specialized_instruction_opcode_plus___rr1___rr1___rr1 = 394,
    structuredvm_specialized_instruction_opcode_plus___rr1___rr1___rrR = 395,
    structuredvm_specialized_instruction_opcode_plus___rr1___rrR___rr0 = 396,
    structuredvm_specialized_instruction_opcode_plus___rr1___rrR___rr1 = 397,
    structuredvm_specialized_instruction_opcode_plus___rr1___rrR___rrR = 398,
    structuredvm_specialized_instruction_opcode_plus___rr1__n1___rr0 = 399,
    structuredvm_specialized_instruction_opcode_plus___rr1__n1___rr1 = 400,
    structuredvm_specialized_instruction_opcode_plus___rr1__n1___rrR = 401,
    structuredvm_specialized_instruction_opcode_plus___rr1__n2___rr0 = 402,
    structuredvm_specialized_instruction_opcode_plus___rr1__n2___rr1 = 403,
    structuredvm_specialized_instruction_opcode_plus___rr1__n2___rrR = 404,
    structuredvm_specialized_instruction_opcode_plus___rr1__nR___rr0 = 405,
    structuredvm_specialized_instruction_opcode_plus___rr1__nR___rr1 = 406,
    structuredvm_specialized_instruction_opcode_plus___rr1__nR___rrR = 407,
    structuredvm_specialized_instruction_opcode_plus___rrR___rr0___rr0 = 408,
    structuredvm_specialized_instruction_opcode_plus___rrR___rr0___rr1 = 409,
    structuredvm_specialized_instruction_opcode_plus___rrR___rr0___rrR = 410,
    structuredvm_specialized_instruction_opcode_plus___rrR___rr1___rr0 = 411,
    structuredvm_specialized_instruction_opcode_plus___rrR___rr1___rr1 = 412,
    structuredvm_specialized_instruction_opcode_plus___rrR___rr1___rrR = 413,
    structuredvm_specialized_instruction_opcode_plus___rrR___rrR___rr0 = 414,
    structuredvm_specialized_instruction_opcode_plus___rrR___rrR___rr1 = 415,
    structuredvm_specialized_instruction_opcode_plus___rrR___rrR___rrR = 416,
    structuredvm_specialized_instruction_opcode_plus___rrR__n1___rr0 = 417,
    structuredvm_specialized_instruction_opcode_plus___rrR__n1___rr1 = 418,
    structuredvm_specialized_instruction_opcode_plus___rrR__n1___rrR = 419,
    structuredvm_specialized_instruction_opcode_plus___rrR__n2___rr0 = 420,
    structuredvm_specialized_instruction_opcode_plus___rrR__n2___rr1 = 421,
    structuredvm_specialized_instruction_opcode_plus___rrR__n2___rrR = 422,
    structuredvm_specialized_instruction_opcode_plus___rrR__nR___rr0 = 423,
    structuredvm_specialized_instruction_opcode_plus___rrR__nR___rr1 = 424,
    structuredvm_specialized_instruction_opcode_plus___rrR__nR___rrR = 425,
    structuredvm_specialized_instruction_opcode_plus__nR___rr0___rr0 = 426,
    structuredvm_specialized_instruction_opcode_plus__nR___rr0___rr1 = 427,
    structuredvm_specialized_instruction_opcode_plus__nR___rr0___rrR = 428,
    structuredvm_specialized_instruction_opcode_plus__nR___rr1___rr0 = 429,
    structuredvm_specialized_instruction_opcode_plus__nR___rr1___rr1 = 430,
    structuredvm_specialized_instruction_opcode_plus__nR___rr1___rrR = 431,
    structuredvm_specialized_instruction_opcode_plus__nR___rrR___rr0 = 432,
    structuredvm_specialized_instruction_opcode_plus__nR___rrR___rr1 = 433,
    structuredvm_specialized_instruction_opcode_plus__nR___rrR___rrR = 434,
    structuredvm_specialized_instruction_opcode_plus__nR__n1___rr0 = 435,
    structuredvm_specialized_instruction_opcode_plus__nR__n1___rr1 = 436,
    structuredvm_specialized_instruction_opcode_plus__nR__n1___rrR = 437,
    structuredvm_specialized_instruction_opcode_plus__nR__n2___rr0 = 438,
    structuredvm_specialized_instruction_opcode_plus__nR__n2___rr1 = 439,
    structuredvm_specialized_instruction_opcode_plus__nR__n2___rrR = 440,
    structuredvm_specialized_instruction_opcode_plus__nR__nR___rr0 = 441,
    structuredvm_specialized_instruction_opcode_plus__nR__nR___rr1 = 442,
    structuredvm_specialized_instruction_opcode_plus__nR__nR___rrR = 443,
    structuredvm_specialized_instruction_opcode_plus_mstack = 444,
    structuredvm_specialized_instruction_opcode_plusi_mstack__n_m1 = 445,
    structuredvm_specialized_instruction_opcode_plusi_mstack__n1 = 446,
    structuredvm_specialized_instruction_opcode_plusi_mstack__n2 = 447,
    structuredvm_specialized_instruction_opcode_plusi_mstack__nR = 448,
    structuredvm_specialized_instruction_opcode_pop_mstack___rr0 = 449,
    structuredvm_specialized_instruction_opcode_pop_mstack___rr1 = 450,
    structuredvm_specialized_instruction_opcode_pop_mstack___rrR = 451,
    structuredvm_specialized_instruction_opcode_print___rr0__retR = 452,
    structuredvm_specialized_instruction_opcode_print___rr1__retR = 453,
    structuredvm_specialized_instruction_opcode_print___rrR__retR = 454,
    structuredvm_specialized_instruction_opcode_print__nR__retR = 455,
    structuredvm_specialized_instruction_opcode_print_mstack__retR = 456,
    structuredvm_specialized_instruction_opcode_push_mstack___rr0 = 457,
    structuredvm_specialized_instruction_opcode_push_mstack___rr1 = 458,
    structuredvm_specialized_instruction_opcode_push_mstack___rrR = 459,
    structuredvm_specialized_instruction_opcode_push_mstack__n0 = 460,
    structuredvm_specialized_instruction_opcode_push_mstack__n1 = 461,
    structuredvm_specialized_instruction_opcode_push_mstack__n_m1 = 462,
    structuredvm_specialized_instruction_opcode_push_mstack__n2 = 463,
    structuredvm_specialized_instruction_opcode_push_mstack__nR = 464,
    structuredvm_specialized_instruction_opcode_push_mstack__lR = 465,
    structuredvm_specialized_instruction_opcode_pushr_mbeqr_mstack___rr0___rr0__fR = 466,
    structuredvm_specialized_instruction_opcode_pushr_mbeqr_mstack___rr0___rr1__fR = 467,
    structuredvm_specialized_instruction_opcode_pushr_mbeqr_mstack___rr0___rrR__fR = 468,
    structuredvm_specialized_instruction_opcode_pushr_mbeqr_mstack___rr1___rr0__fR = 469,
    structuredvm_specialized_instruction_opcode_pushr_mbeqr_mstack___rr1___rr1__fR = 470,
    structuredvm_specialized_instruction_opcode_pushr_mbeqr_mstack___rr1___rrR__fR = 471,
    structuredvm_specialized_instruction_opcode_pushr_mbeqr_mstack___rrR___rr0__fR = 472,
    structuredvm_specialized_instruction_opcode_pushr_mbeqr_mstack___rrR___rr1__fR = 473,
    structuredvm_specialized_instruction_opcode_pushr_mbeqr_mstack___rrR___rrR__fR = 474,
    structuredvm_specialized_instruction_opcode_pushr_mbger_mstack___rr0___rr0__fR = 475,
    structuredvm_specialized_instruction_opcode_pushr_mbger_mstack___rr0___rr1__fR = 476,
    structuredvm_specialized_instruction_opcode_pushr_mbger_mstack___rr0___rrR__fR = 477,
    structuredvm_specialized_instruction_opcode_pushr_mbger_mstack___rr1___rr0__fR = 478,
    structuredvm_specialized_instruction_opcode_pushr_mbger_mstack___rr1___rr1__fR = 479,
    structuredvm_specialized_instruction_opcode_pushr_mbger_mstack___rr1___rrR__fR = 480,
    structuredvm_specialized_instruction_opcode_pushr_mbger_mstack___rrR___rr0__fR = 481,
    structuredvm_specialized_instruction_opcode_pushr_mbger_mstack___rrR___rr1__fR = 482,
    structuredvm_specialized_instruction_opcode_pushr_mbger_mstack___rrR___rrR__fR = 483,
    structuredvm_specialized_instruction_opcode_pushr_mminusr_mpop_mstack___rr0___rr0___rr0 = 484,
    structuredvm_specialized_instruction_opcode_pushr_mminusr_mpop_mstack___rr0___rr0___rr1 = 485,
    structuredvm_specialized_instruction_opcode_pushr_mminusr_mpop_mstack___rr0___rr0___rrR = 486,
    structuredvm_specialized_instruction_opcode_pushr_mminusr_mpop_mstack___rr0___rr1___rr0 = 487,
    structuredvm_specialized_instruction_opcode_pushr_mminusr_mpop_mstack___rr0___rr1___rr1 = 488,
    structuredvm_specialized_instruction_opcode_pushr_mminusr_mpop_mstack___rr0___rr1___rrR = 489,
    structuredvm_specialized_instruction_opcode_pushr_mminusr_mpop_mstack___rr0___rrR___rr0 = 490,
    structuredvm_specialized_instruction_opcode_pushr_mminusr_mpop_mstack___rr0___rrR___rr1 = 491,
    structuredvm_specialized_instruction_opcode_pushr_mminusr_mpop_mstack___rr0___rrR___rrR = 492,
    structuredvm_specialized_instruction_opcode_pushr_mminusr_mpop_mstack___rr1___rr0___rr0 = 493,
    structuredvm_specialized_instruction_opcode_pushr_mminusr_mpop_mstack___rr1___rr0___rr1 = 494,
    structuredvm_specialized_instruction_opcode_pushr_mminusr_mpop_mstack___rr1___rr0___rrR = 495,
    structuredvm_specialized_instruction_opcode_pushr_mminusr_mpop_mstack___rr1___rr1___rr0 = 496,
    structuredvm_specialized_instruction_opcode_pushr_mminusr_mpop_mstack___rr1___rr1___rr1 = 497,
    structuredvm_specialized_instruction_opcode_pushr_mminusr_mpop_mstack___rr1___rr1___rrR = 498,
    structuredvm_specialized_instruction_opcode_pushr_mminusr_mpop_mstack___rr1___rrR___rr0 = 499,
    structuredvm_specialized_instruction_opcode_pushr_mminusr_mpop_mstack___rr1___rrR___rr1 = 500,
    structuredvm_specialized_instruction_opcode_pushr_mminusr_mpop_mstack___rr1___rrR___rrR = 501,
    structuredvm_specialized_instruction_opcode_pushr_mminusr_mpop_mstack___rrR___rr0___rr0 = 502,
    structuredvm_specialized_instruction_opcode_pushr_mminusr_mpop_mstack___rrR___rr0___rr1 = 503,
    structuredvm_specialized_instruction_opcode_pushr_mminusr_mpop_mstack___rrR___rr0___rrR = 504,
    structuredvm_specialized_instruction_opcode_pushr_mminusr_mpop_mstack___rrR___rr1___rr0 = 505,
    structuredvm_specialized_instruction_opcode_pushr_mminusr_mpop_mstack___rrR___rr1___rr1 = 506,
    structuredvm_specialized_instruction_opcode_pushr_mminusr_mpop_mstack___rrR___rr1___rrR = 507,
    structuredvm_specialized_instruction_opcode_pushr_mminusr_mpop_mstack___rrR___rrR___rr0 = 508,
    structuredvm_specialized_instruction_opcode_pushr_mminusr_mpop_mstack___rrR___rrR___rr1 = 509,
    structuredvm_specialized_instruction_opcode_pushr_mminusr_mpop_mstack___rrR___rrR___rrR = 510,
    structuredvm_specialized_instruction_opcode_remainder___rr0___rr0___rr0 = 511,
    structuredvm_specialized_instruction_opcode_remainder___rr0___rr0___rr1 = 512,
    structuredvm_specialized_instruction_opcode_remainder___rr0___rr0___rrR = 513,
    structuredvm_specialized_instruction_opcode_remainder___rr0___rr1___rr0 = 514,
    structuredvm_specialized_instruction_opcode_remainder___rr0___rr1___rr1 = 515,
    structuredvm_specialized_instruction_opcode_remainder___rr0___rr1___rrR = 516,
    structuredvm_specialized_instruction_opcode_remainder___rr0___rrR___rr0 = 517,
    structuredvm_specialized_instruction_opcode_remainder___rr0___rrR___rr1 = 518,
    structuredvm_specialized_instruction_opcode_remainder___rr0___rrR___rrR = 519,
    structuredvm_specialized_instruction_opcode_remainder___rr0__n2___rr0 = 520,
    structuredvm_specialized_instruction_opcode_remainder___rr0__n2___rr1 = 521,
    structuredvm_specialized_instruction_opcode_remainder___rr0__n2___rrR = 522,
    structuredvm_specialized_instruction_opcode_remainder___rr0__nR___rr0 = 523,
    structuredvm_specialized_instruction_opcode_remainder___rr0__nR___rr1 = 524,
    structuredvm_specialized_instruction_opcode_remainder___rr0__nR___rrR = 525,
    structuredvm_specialized_instruction_opcode_remainder___rr1___rr0___rr0 = 526,
    structuredvm_specialized_instruction_opcode_remainder___rr1___rr0___rr1 = 527,
    structuredvm_specialized_instruction_opcode_remainder___rr1___rr0___rrR = 528,
    structuredvm_specialized_instruction_opcode_remainder___rr1___rr1___rr0 = 529,
    structuredvm_specialized_instruction_opcode_remainder___rr1___rr1___rr1 = 530,
    structuredvm_specialized_instruction_opcode_remainder___rr1___rr1___rrR = 531,
    structuredvm_specialized_instruction_opcode_remainder___rr1___rrR___rr0 = 532,
    structuredvm_specialized_instruction_opcode_remainder___rr1___rrR___rr1 = 533,
    structuredvm_specialized_instruction_opcode_remainder___rr1___rrR___rrR = 534,
    structuredvm_specialized_instruction_opcode_remainder___rr1__n2___rr0 = 535,
    structuredvm_specialized_instruction_opcode_remainder___rr1__n2___rr1 = 536,
    structuredvm_specialized_instruction_opcode_remainder___rr1__n2___rrR = 537,
    structuredvm_specialized_instruction_opcode_remainder___rr1__nR___rr0 = 538,
    structuredvm_specialized_instruction_opcode_remainder___rr1__nR___rr1 = 539,
    structuredvm_specialized_instruction_opcode_remainder___rr1__nR___rrR = 540,
    structuredvm_specialized_instruction_opcode_remainder___rrR___rr0___rr0 = 541,
    structuredvm_specialized_instruction_opcode_remainder___rrR___rr0___rr1 = 542,
    structuredvm_specialized_instruction_opcode_remainder___rrR___rr0___rrR = 543,
    structuredvm_specialized_instruction_opcode_remainder___rrR___rr1___rr0 = 544,
    structuredvm_specialized_instruction_opcode_remainder___rrR___rr1___rr1 = 545,
    structuredvm_specialized_instruction_opcode_remainder___rrR___rr1___rrR = 546,
    structuredvm_specialized_instruction_opcode_remainder___rrR___rrR___rr0 = 547,
    structuredvm_specialized_instruction_opcode_remainder___rrR___rrR___rr1 = 548,
    structuredvm_specialized_instruction_opcode_remainder___rrR___rrR___rrR = 549,
    structuredvm_specialized_instruction_opcode_remainder___rrR__n2___rr0 = 550,
    structuredvm_specialized_instruction_opcode_remainder___rrR__n2___rr1 = 551,
    structuredvm_specialized_instruction_opcode_remainder___rrR__n2___rrR = 552,
    structuredvm_specialized_instruction_opcode_remainder___rrR__nR___rr0 = 553,
    structuredvm_specialized_instruction_opcode_remainder___rrR__nR___rr1 = 554,
    structuredvm_specialized_instruction_opcode_remainder___rrR__nR___rrR = 555,
    structuredvm_specialized_instruction_opcode_remainder__nR___rr0___rr0 = 556,
    structuredvm_specialized_instruction_opcode_remainder__nR___rr0___rr1 = 557,
    structuredvm_specialized_instruction_opcode_remainder__nR___rr0___rrR = 558,
    structuredvm_specialized_instruction_opcode_remainder__nR___rr1___rr0 = 559,
    structuredvm_specialized_instruction_opcode_remainder__nR___rr1___rr1 = 560,
    structuredvm_specialized_instruction_opcode_remainder__nR___rr1___rrR = 561,
    structuredvm_specialized_instruction_opcode_remainder__nR___rrR___rr0 = 562,
    structuredvm_specialized_instruction_opcode_remainder__nR___rrR___rr1 = 563,
    structuredvm_specialized_instruction_opcode_remainder__nR___rrR___rrR = 564,
    structuredvm_specialized_instruction_opcode_remainder__nR__n2___rr0 = 565,
    structuredvm_specialized_instruction_opcode_remainder__nR__n2___rr1 = 566,
    structuredvm_specialized_instruction_opcode_remainder__nR__n2___rrR = 567,
    structuredvm_specialized_instruction_opcode_remainder__nR__nR___rr0 = 568,
    structuredvm_specialized_instruction_opcode_remainder__nR__nR___rr1 = 569,
    structuredvm_specialized_instruction_opcode_remainder__nR__nR___rrR = 570,
    structuredvm_specialized_instruction_opcode_remainder_mstack = 571,
    structuredvm_specialized_instruction_opcode_times___rr0___rr0___rr0 = 572,
    structuredvm_specialized_instruction_opcode_times___rr0___rr0___rr1 = 573,
    structuredvm_specialized_instruction_opcode_times___rr0___rr0___rrR = 574,
    structuredvm_specialized_instruction_opcode_times___rr0___rr1___rr0 = 575,
    structuredvm_specialized_instruction_opcode_times___rr0___rr1___rr1 = 576,
    structuredvm_specialized_instruction_opcode_times___rr0___rr1___rrR = 577,
    structuredvm_specialized_instruction_opcode_times___rr0___rrR___rr0 = 578,
    structuredvm_specialized_instruction_opcode_times___rr0___rrR___rr1 = 579,
    structuredvm_specialized_instruction_opcode_times___rr0___rrR___rrR = 580,
    structuredvm_specialized_instruction_opcode_times___rr0__n2___rr0 = 581,
    structuredvm_specialized_instruction_opcode_times___rr0__n2___rr1 = 582,
    structuredvm_specialized_instruction_opcode_times___rr0__n2___rrR = 583,
    structuredvm_specialized_instruction_opcode_times___rr0__nR___rr0 = 584,
    structuredvm_specialized_instruction_opcode_times___rr0__nR___rr1 = 585,
    structuredvm_specialized_instruction_opcode_times___rr0__nR___rrR = 586,
    structuredvm_specialized_instruction_opcode_times___rr1___rr0___rr0 = 587,
    structuredvm_specialized_instruction_opcode_times___rr1___rr0___rr1 = 588,
    structuredvm_specialized_instruction_opcode_times___rr1___rr0___rrR = 589,
    structuredvm_specialized_instruction_opcode_times___rr1___rr1___rr0 = 590,
    structuredvm_specialized_instruction_opcode_times___rr1___rr1___rr1 = 591,
    structuredvm_specialized_instruction_opcode_times___rr1___rr1___rrR = 592,
    structuredvm_specialized_instruction_opcode_times___rr1___rrR___rr0 = 593,
    structuredvm_specialized_instruction_opcode_times___rr1___rrR___rr1 = 594,
    structuredvm_specialized_instruction_opcode_times___rr1___rrR___rrR = 595,
    structuredvm_specialized_instruction_opcode_times___rr1__n2___rr0 = 596,
    structuredvm_specialized_instruction_opcode_times___rr1__n2___rr1 = 597,
    structuredvm_specialized_instruction_opcode_times___rr1__n2___rrR = 598,
    structuredvm_specialized_instruction_opcode_times___rr1__nR___rr0 = 599,
    structuredvm_specialized_instruction_opcode_times___rr1__nR___rr1 = 600,
    structuredvm_specialized_instruction_opcode_times___rr1__nR___rrR = 601,
    structuredvm_specialized_instruction_opcode_times___rrR___rr0___rr0 = 602,
    structuredvm_specialized_instruction_opcode_times___rrR___rr0___rr1 = 603,
    structuredvm_specialized_instruction_opcode_times___rrR___rr0___rrR = 604,
    structuredvm_specialized_instruction_opcode_times___rrR___rr1___rr0 = 605,
    structuredvm_specialized_instruction_opcode_times___rrR___rr1___rr1 = 606,
    structuredvm_specialized_instruction_opcode_times___rrR___rr1___rrR = 607,
    structuredvm_specialized_instruction_opcode_times___rrR___rrR___rr0 = 608,
    structuredvm_specialized_instruction_opcode_times___rrR___rrR___rr1 = 609,
    structuredvm_specialized_instruction_opcode_times___rrR___rrR___rrR = 610,
    structuredvm_specialized_instruction_opcode_times___rrR__n2___rr0 = 611,
    structuredvm_specialized_instruction_opcode_times___rrR__n2___rr1 = 612,
    structuredvm_specialized_instruction_opcode_times___rrR__n2___rrR = 613,
    structuredvm_specialized_instruction_opcode_times___rrR__nR___rr0 = 614,
    structuredvm_specialized_instruction_opcode_times___rrR__nR___rr1 = 615,
    structuredvm_specialized_instruction_opcode_times___rrR__nR___rrR = 616,
    structuredvm_specialized_instruction_opcode_times__nR___rr0___rr0 = 617,
    structuredvm_specialized_instruction_opcode_times__nR___rr0___rr1 = 618,
    structuredvm_specialized_instruction_opcode_times__nR___rr0___rrR = 619,
    structuredvm_specialized_instruction_opcode_times__nR___rr1___rr0 = 620,
    structuredvm_specialized_instruction_opcode_times__nR___rr1___rr1 = 621,
    structuredvm_specialized_instruction_opcode_times__nR___rr1___rrR = 622,
    structuredvm_specialized_instruction_opcode_times__nR___rrR___rr0 = 623,
    structuredvm_specialized_instruction_opcode_times__nR___rrR___rr1 = 624,
    structuredvm_specialized_instruction_opcode_times__nR___rrR___rrR = 625,
    structuredvm_specialized_instruction_opcode_times__nR__n2___rr0 = 626,
    structuredvm_specialized_instruction_opcode_times__nR__n2___rr1 = 627,
    structuredvm_specialized_instruction_opcode_times__nR__n2___rrR = 628,
    structuredvm_specialized_instruction_opcode_times__nR__nR___rr0 = 629,
    structuredvm_specialized_instruction_opcode_times__nR__nR___rr1 = 630,
    structuredvm_specialized_instruction_opcode_times__nR__nR___rrR = 631,
    structuredvm_specialized_instruction_opcode_times_mstack = 632,
    structuredvm_specialized_instruction_opcode_uminus___rr0___rr0 = 633,
    structuredvm_specialized_instruction_opcode_uminus___rr0___rr1 = 634,
    structuredvm_specialized_instruction_opcode_uminus___rr0___rrR = 635,
    structuredvm_specialized_instruction_opcode_uminus___rr1___rr0 = 636,
    structuredvm_specialized_instruction_opcode_uminus___rr1___rr1 = 637,
    structuredvm_specialized_instruction_opcode_uminus___rr1___rrR = 638,
    structuredvm_specialized_instruction_opcode_uminus___rrR___rr0 = 639,
    structuredvm_specialized_instruction_opcode_uminus___rrR___rr1 = 640,
    structuredvm_specialized_instruction_opcode_uminus___rrR___rrR = 641,
    structuredvm_specialized_instruction_opcode_uminus__nR___rr0 = 642,
    structuredvm_specialized_instruction_opcode_uminus__nR___rr1 = 643,
    structuredvm_specialized_instruction_opcode_uminus__nR___rrR = 644,
    structuredvm_specialized_instruction_opcode_uminus_mstack = 645,
    structuredvm_specialized_instruction_opcode_unreachable = 646,
    structuredvm_specialized_instruction_opcode__Ab__fR_A_mno_mfast_mbranches = 647,
    structuredvm_specialized_instruction_opcode__Abe___rr0___rr0__fR_A_mno_mfast_mbranches = 648,
    structuredvm_specialized_instruction_opcode__Abe___rr0___rr1__fR_A_mno_mfast_mbranches = 649,
    structuredvm_specialized_instruction_opcode__Abe___rr0___rrR__fR_A_mno_mfast_mbranches = 650,
    structuredvm_specialized_instruction_opcode__Abe___rr0__n0__fR_A_mno_mfast_mbranches = 651,
    structuredvm_specialized_instruction_opcode__Abe___rr0__nR__fR_A_mno_mfast_mbranches = 652,
    structuredvm_specialized_instruction_opcode__Abe___rr1___rr0__fR_A_mno_mfast_mbranches = 653,
    structuredvm_specialized_instruction_opcode__Abe___rr1___rr1__fR_A_mno_mfast_mbranches = 654,
    structuredvm_specialized_instruction_opcode__Abe___rr1___rrR__fR_A_mno_mfast_mbranches = 655,
    structuredvm_specialized_instruction_opcode__Abe___rr1__n0__fR_A_mno_mfast_mbranches = 656,
    structuredvm_specialized_instruction_opcode__Abe___rr1__nR__fR_A_mno_mfast_mbranches = 657,
    structuredvm_specialized_instruction_opcode__Abe___rrR___rr0__fR_A_mno_mfast_mbranches = 658,
    structuredvm_specialized_instruction_opcode__Abe___rrR___rr1__fR_A_mno_mfast_mbranches = 659,
    structuredvm_specialized_instruction_opcode__Abe___rrR___rrR__fR_A_mno_mfast_mbranches = 660,
    structuredvm_specialized_instruction_opcode__Abe___rrR__n0__fR_A_mno_mfast_mbranches = 661,
    structuredvm_specialized_instruction_opcode__Abe___rrR__nR__fR_A_mno_mfast_mbranches = 662,
    structuredvm_specialized_instruction_opcode__Abe__n0___rr0__fR_A_mno_mfast_mbranches = 663,
    structuredvm_specialized_instruction_opcode__Abe__n0___rr1__fR_A_mno_mfast_mbranches = 664,
    structuredvm_specialized_instruction_opcode__Abe__n0___rrR__fR_A_mno_mfast_mbranches = 665,
    structuredvm_specialized_instruction_opcode__Abe__n0__n0__fR_A_mno_mfast_mbranches = 666,
    structuredvm_specialized_instruction_opcode__Abe__n0__nR__fR_A_mno_mfast_mbranches = 667,
    structuredvm_specialized_instruction_opcode__Abe__nR___rr0__fR_A_mno_mfast_mbranches = 668,
    structuredvm_specialized_instruction_opcode__Abe__nR___rr1__fR_A_mno_mfast_mbranches = 669,
    structuredvm_specialized_instruction_opcode__Abe__nR___rrR__fR_A_mno_mfast_mbranches = 670,
    structuredvm_specialized_instruction_opcode__Abe__nR__n0__fR_A_mno_mfast_mbranches = 671,
    structuredvm_specialized_instruction_opcode__Abe__nR__nR__fR_A_mno_mfast_mbranches = 672,
    structuredvm_specialized_instruction_opcode__Abeqi_mstack__n_m1__fR_A_mno_mfast_mbranches = 673,
    structuredvm_specialized_instruction_opcode__Abeqi_mstack__n0__fR_A_mno_mfast_mbranches = 674,
    structuredvm_specialized_instruction_opcode__Abeqi_mstack__n1__fR_A_mno_mfast_mbranches = 675,
    structuredvm_specialized_instruction_opcode__Abeqi_mstack__n2__fR_A_mno_mfast_mbranches = 676,
    structuredvm_specialized_instruction_opcode__Abeqi_mstack__nR__fR_A_mno_mfast_mbranches = 677,
    structuredvm_specialized_instruction_opcode__Abeqr_mstack___rr0__fR_A_mno_mfast_mbranches = 678,
    structuredvm_specialized_instruction_opcode__Abeqr_mstack___rr1__fR_A_mno_mfast_mbranches = 679,
    structuredvm_specialized_instruction_opcode__Abeqr_mstack___rrR__fR_A_mno_mfast_mbranches = 680,
    structuredvm_specialized_instruction_opcode__Abf_mstack__fR_A_mno_mfast_mbranches = 681,
    structuredvm_specialized_instruction_opcode__Abg___rr0___rr0__fR_A_mno_mfast_mbranches = 682,
    structuredvm_specialized_instruction_opcode__Abg___rr0___rr1__fR_A_mno_mfast_mbranches = 683,
    structuredvm_specialized_instruction_opcode__Abg___rr0___rrR__fR_A_mno_mfast_mbranches = 684,
    structuredvm_specialized_instruction_opcode__Abg___rr0__n0__fR_A_mno_mfast_mbranches = 685,
    structuredvm_specialized_instruction_opcode__Abg___rr0__nR__fR_A_mno_mfast_mbranches = 686,
    structuredvm_specialized_instruction_opcode__Abg___rr1___rr0__fR_A_mno_mfast_mbranches = 687,
    structuredvm_specialized_instruction_opcode__Abg___rr1___rr1__fR_A_mno_mfast_mbranches = 688,
    structuredvm_specialized_instruction_opcode__Abg___rr1___rrR__fR_A_mno_mfast_mbranches = 689,
    structuredvm_specialized_instruction_opcode__Abg___rr1__n0__fR_A_mno_mfast_mbranches = 690,
    structuredvm_specialized_instruction_opcode__Abg___rr1__nR__fR_A_mno_mfast_mbranches = 691,
    structuredvm_specialized_instruction_opcode__Abg___rrR___rr0__fR_A_mno_mfast_mbranches = 692,
    structuredvm_specialized_instruction_opcode__Abg___rrR___rr1__fR_A_mno_mfast_mbranches = 693,
    structuredvm_specialized_instruction_opcode__Abg___rrR___rrR__fR_A_mno_mfast_mbranches = 694,
    structuredvm_specialized_instruction_opcode__Abg___rrR__n0__fR_A_mno_mfast_mbranches = 695,
    structuredvm_specialized_instruction_opcode__Abg___rrR__nR__fR_A_mno_mfast_mbranches = 696,
    structuredvm_specialized_instruction_opcode__Abg__n0___rr0__fR_A_mno_mfast_mbranches = 697,
    structuredvm_specialized_instruction_opcode__Abg__n0___rr1__fR_A_mno_mfast_mbranches = 698,
    structuredvm_specialized_instruction_opcode__Abg__n0___rrR__fR_A_mno_mfast_mbranches = 699,
    structuredvm_specialized_instruction_opcode__Abg__n0__n0__fR_A_mno_mfast_mbranches = 700,
    structuredvm_specialized_instruction_opcode__Abg__n0__nR__fR_A_mno_mfast_mbranches = 701,
    structuredvm_specialized_instruction_opcode__Abg__nR___rr0__fR_A_mno_mfast_mbranches = 702,
    structuredvm_specialized_instruction_opcode__Abg__nR___rr1__fR_A_mno_mfast_mbranches = 703,
    structuredvm_specialized_instruction_opcode__Abg__nR___rrR__fR_A_mno_mfast_mbranches = 704,
    structuredvm_specialized_instruction_opcode__Abg__nR__n0__fR_A_mno_mfast_mbranches = 705,
    structuredvm_specialized_instruction_opcode__Abg__nR__nR__fR_A_mno_mfast_mbranches = 706,
    structuredvm_specialized_instruction_opcode__Abge___rr0___rr0__fR_A_mno_mfast_mbranches = 707,
    structuredvm_specialized_instruction_opcode__Abge___rr0___rr1__fR_A_mno_mfast_mbranches = 708,
    structuredvm_specialized_instruction_opcode__Abge___rr0___rrR__fR_A_mno_mfast_mbranches = 709,
    structuredvm_specialized_instruction_opcode__Abge___rr0__n0__fR_A_mno_mfast_mbranches = 710,
    structuredvm_specialized_instruction_opcode__Abge___rr0__nR__fR_A_mno_mfast_mbranches = 711,
    structuredvm_specialized_instruction_opcode__Abge___rr1___rr0__fR_A_mno_mfast_mbranches = 712,
    structuredvm_specialized_instruction_opcode__Abge___rr1___rr1__fR_A_mno_mfast_mbranches = 713,
    structuredvm_specialized_instruction_opcode__Abge___rr1___rrR__fR_A_mno_mfast_mbranches = 714,
    structuredvm_specialized_instruction_opcode__Abge___rr1__n0__fR_A_mno_mfast_mbranches = 715,
    structuredvm_specialized_instruction_opcode__Abge___rr1__nR__fR_A_mno_mfast_mbranches = 716,
    structuredvm_specialized_instruction_opcode__Abge___rrR___rr0__fR_A_mno_mfast_mbranches = 717,
    structuredvm_specialized_instruction_opcode__Abge___rrR___rr1__fR_A_mno_mfast_mbranches = 718,
    structuredvm_specialized_instruction_opcode__Abge___rrR___rrR__fR_A_mno_mfast_mbranches = 719,
    structuredvm_specialized_instruction_opcode__Abge___rrR__n0__fR_A_mno_mfast_mbranches = 720,
    structuredvm_specialized_instruction_opcode__Abge___rrR__nR__fR_A_mno_mfast_mbranches = 721,
    structuredvm_specialized_instruction_opcode__Abge__n0___rr0__fR_A_mno_mfast_mbranches = 722,
    structuredvm_specialized_instruction_opcode__Abge__n0___rr1__fR_A_mno_mfast_mbranches = 723,
    structuredvm_specialized_instruction_opcode__Abge__n0___rrR__fR_A_mno_mfast_mbranches = 724,
    structuredvm_specialized_instruction_opcode__Abge__n0__n0__fR_A_mno_mfast_mbranches = 725,
    structuredvm_specialized_instruction_opcode__Abge__n0__nR__fR_A_mno_mfast_mbranches = 726,
    structuredvm_specialized_instruction_opcode__Abge__nR___rr0__fR_A_mno_mfast_mbranches = 727,
    structuredvm_specialized_instruction_opcode__Abge__nR___rr1__fR_A_mno_mfast_mbranches = 728,
    structuredvm_specialized_instruction_opcode__Abge__nR___rrR__fR_A_mno_mfast_mbranches = 729,
    structuredvm_specialized_instruction_opcode__Abge__nR__n0__fR_A_mno_mfast_mbranches = 730,
    structuredvm_specialized_instruction_opcode__Abge__nR__nR__fR_A_mno_mfast_mbranches = 731,
    structuredvm_specialized_instruction_opcode__Abger_mstack___rr0__fR_A_mno_mfast_mbranches = 732,
    structuredvm_specialized_instruction_opcode__Abger_mstack___rr1__fR_A_mno_mfast_mbranches = 733,
    structuredvm_specialized_instruction_opcode__Abger_mstack___rrR__fR_A_mno_mfast_mbranches = 734,
    structuredvm_specialized_instruction_opcode__Abl___rr0___rr0__fR_A_mno_mfast_mbranches = 735,
    structuredvm_specialized_instruction_opcode__Abl___rr0___rr1__fR_A_mno_mfast_mbranches = 736,
    structuredvm_specialized_instruction_opcode__Abl___rr0___rrR__fR_A_mno_mfast_mbranches = 737,
    structuredvm_specialized_instruction_opcode__Abl___rr0__n0__fR_A_mno_mfast_mbranches = 738,
    structuredvm_specialized_instruction_opcode__Abl___rr0__nR__fR_A_mno_mfast_mbranches = 739,
    structuredvm_specialized_instruction_opcode__Abl___rr1___rr0__fR_A_mno_mfast_mbranches = 740,
    structuredvm_specialized_instruction_opcode__Abl___rr1___rr1__fR_A_mno_mfast_mbranches = 741,
    structuredvm_specialized_instruction_opcode__Abl___rr1___rrR__fR_A_mno_mfast_mbranches = 742,
    structuredvm_specialized_instruction_opcode__Abl___rr1__n0__fR_A_mno_mfast_mbranches = 743,
    structuredvm_specialized_instruction_opcode__Abl___rr1__nR__fR_A_mno_mfast_mbranches = 744,
    structuredvm_specialized_instruction_opcode__Abl___rrR___rr0__fR_A_mno_mfast_mbranches = 745,
    structuredvm_specialized_instruction_opcode__Abl___rrR___rr1__fR_A_mno_mfast_mbranches = 746,
    structuredvm_specialized_instruction_opcode__Abl___rrR___rrR__fR_A_mno_mfast_mbranches = 747,
    structuredvm_specialized_instruction_opcode__Abl___rrR__n0__fR_A_mno_mfast_mbranches = 748,
    structuredvm_specialized_instruction_opcode__Abl___rrR__nR__fR_A_mno_mfast_mbranches = 749,
    structuredvm_specialized_instruction_opcode__Abl__n0___rr0__fR_A_mno_mfast_mbranches = 750,
    structuredvm_specialized_instruction_opcode__Abl__n0___rr1__fR_A_mno_mfast_mbranches = 751,
    structuredvm_specialized_instruction_opcode__Abl__n0___rrR__fR_A_mno_mfast_mbranches = 752,
    structuredvm_specialized_instruction_opcode__Abl__n0__n0__fR_A_mno_mfast_mbranches = 753,
    structuredvm_specialized_instruction_opcode__Abl__n0__nR__fR_A_mno_mfast_mbranches = 754,
    structuredvm_specialized_instruction_opcode__Abl__nR___rr0__fR_A_mno_mfast_mbranches = 755,
    structuredvm_specialized_instruction_opcode__Abl__nR___rr1__fR_A_mno_mfast_mbranches = 756,
    structuredvm_specialized_instruction_opcode__Abl__nR___rrR__fR_A_mno_mfast_mbranches = 757,
    structuredvm_specialized_instruction_opcode__Abl__nR__n0__fR_A_mno_mfast_mbranches = 758,
    structuredvm_specialized_instruction_opcode__Abl__nR__nR__fR_A_mno_mfast_mbranches = 759,
    structuredvm_specialized_instruction_opcode__Able___rr0___rr0__fR_A_mno_mfast_mbranches = 760,
    structuredvm_specialized_instruction_opcode__Able___rr0___rr1__fR_A_mno_mfast_mbranches = 761,
    structuredvm_specialized_instruction_opcode__Able___rr0___rrR__fR_A_mno_mfast_mbranches = 762,
    structuredvm_specialized_instruction_opcode__Able___rr0__n0__fR_A_mno_mfast_mbranches = 763,
    structuredvm_specialized_instruction_opcode__Able___rr0__nR__fR_A_mno_mfast_mbranches = 764,
    structuredvm_specialized_instruction_opcode__Able___rr1___rr0__fR_A_mno_mfast_mbranches = 765,
    structuredvm_specialized_instruction_opcode__Able___rr1___rr1__fR_A_mno_mfast_mbranches = 766,
    structuredvm_specialized_instruction_opcode__Able___rr1___rrR__fR_A_mno_mfast_mbranches = 767,
    structuredvm_specialized_instruction_opcode__Able___rr1__n0__fR_A_mno_mfast_mbranches = 768,
    structuredvm_specialized_instruction_opcode__Able___rr1__nR__fR_A_mno_mfast_mbranches = 769,
    structuredvm_specialized_instruction_opcode__Able___rrR___rr0__fR_A_mno_mfast_mbranches = 770,
    structuredvm_specialized_instruction_opcode__Able___rrR___rr1__fR_A_mno_mfast_mbranches = 771,
    structuredvm_specialized_instruction_opcode__Able___rrR___rrR__fR_A_mno_mfast_mbranches = 772,
    structuredvm_specialized_instruction_opcode__Able___rrR__n0__fR_A_mno_mfast_mbranches = 773,
    structuredvm_specialized_instruction_opcode__Able___rrR__nR__fR_A_mno_mfast_mbranches = 774,
    structuredvm_specialized_instruction_opcode__Able__n0___rr0__fR_A_mno_mfast_mbranches = 775,
    structuredvm_specialized_instruction_opcode__Able__n0___rr1__fR_A_mno_mfast_mbranches = 776,
    structuredvm_specialized_instruction_opcode__Able__n0___rrR__fR_A_mno_mfast_mbranches = 777,
    structuredvm_specialized_instruction_opcode__Able__n0__n0__fR_A_mno_mfast_mbranches = 778,
    structuredvm_specialized_instruction_opcode__Able__n0__nR__fR_A_mno_mfast_mbranches = 779,
    structuredvm_specialized_instruction_opcode__Able__nR___rr0__fR_A_mno_mfast_mbranches = 780,
    structuredvm_specialized_instruction_opcode__Able__nR___rr1__fR_A_mno_mfast_mbranches = 781,
    structuredvm_specialized_instruction_opcode__Able__nR___rrR__fR_A_mno_mfast_mbranches = 782,
    structuredvm_specialized_instruction_opcode__Able__nR__n0__fR_A_mno_mfast_mbranches = 783,
    structuredvm_specialized_instruction_opcode__Able__nR__nR__fR_A_mno_mfast_mbranches = 784,
    structuredvm_specialized_instruction_opcode__Abne___rr0___rr0__fR_A_mno_mfast_mbranches = 785,
    structuredvm_specialized_instruction_opcode__Abne___rr0___rr1__fR_A_mno_mfast_mbranches = 786,
    structuredvm_specialized_instruction_opcode__Abne___rr0___rrR__fR_A_mno_mfast_mbranches = 787,
    structuredvm_specialized_instruction_opcode__Abne___rr0__n0__fR_A_mno_mfast_mbranches = 788,
    structuredvm_specialized_instruction_opcode__Abne___rr0__nR__fR_A_mno_mfast_mbranches = 789,
    structuredvm_specialized_instruction_opcode__Abne___rr1___rr0__fR_A_mno_mfast_mbranches = 790,
    structuredvm_specialized_instruction_opcode__Abne___rr1___rr1__fR_A_mno_mfast_mbranches = 791,
    structuredvm_specialized_instruction_opcode__Abne___rr1___rrR__fR_A_mno_mfast_mbranches = 792,
    structuredvm_specialized_instruction_opcode__Abne___rr1__n0__fR_A_mno_mfast_mbranches = 793,
    structuredvm_specialized_instruction_opcode__Abne___rr1__nR__fR_A_mno_mfast_mbranches = 794,
    structuredvm_specialized_instruction_opcode__Abne___rrR___rr0__fR_A_mno_mfast_mbranches = 795,
    structuredvm_specialized_instruction_opcode__Abne___rrR___rr1__fR_A_mno_mfast_mbranches = 796,
    structuredvm_specialized_instruction_opcode__Abne___rrR___rrR__fR_A_mno_mfast_mbranches = 797,
    structuredvm_specialized_instruction_opcode__Abne___rrR__n0__fR_A_mno_mfast_mbranches = 798,
    structuredvm_specialized_instruction_opcode__Abne___rrR__nR__fR_A_mno_mfast_mbranches = 799,
    structuredvm_specialized_instruction_opcode__Abne__n0___rr0__fR_A_mno_mfast_mbranches = 800,
    structuredvm_specialized_instruction_opcode__Abne__n0___rr1__fR_A_mno_mfast_mbranches = 801,
    structuredvm_specialized_instruction_opcode__Abne__n0___rrR__fR_A_mno_mfast_mbranches = 802,
    structuredvm_specialized_instruction_opcode__Abne__n0__n0__fR_A_mno_mfast_mbranches = 803,
    structuredvm_specialized_instruction_opcode__Abne__n0__nR__fR_A_mno_mfast_mbranches = 804,
    structuredvm_specialized_instruction_opcode__Abne__nR___rr0__fR_A_mno_mfast_mbranches = 805,
    structuredvm_specialized_instruction_opcode__Abne__nR___rr1__fR_A_mno_mfast_mbranches = 806,
    structuredvm_specialized_instruction_opcode__Abne__nR___rrR__fR_A_mno_mfast_mbranches = 807,
    structuredvm_specialized_instruction_opcode__Abne__nR__n0__fR_A_mno_mfast_mbranches = 808,
    structuredvm_specialized_instruction_opcode__Abne__nR__nR__fR_A_mno_mfast_mbranches = 809,
    structuredvm_specialized_instruction_opcode__Abneqi_mstack__n_m1__fR_A_mno_mfast_mbranches = 810,
    structuredvm_specialized_instruction_opcode__Abneqi_mstack__n0__fR_A_mno_mfast_mbranches = 811,
    structuredvm_specialized_instruction_opcode__Abneqi_mstack__n1__fR_A_mno_mfast_mbranches = 812,
    structuredvm_specialized_instruction_opcode__Abneqi_mstack__n2__fR_A_mno_mfast_mbranches = 813,
    structuredvm_specialized_instruction_opcode__Abneqi_mstack__nR__fR_A_mno_mfast_mbranches = 814,
    structuredvm_specialized_instruction_opcode__Abneqr_mstack___rr0__fR_A_mno_mfast_mbranches = 815,
    structuredvm_specialized_instruction_opcode__Abneqr_mstack___rr1__fR_A_mno_mfast_mbranches = 816,
    structuredvm_specialized_instruction_opcode__Abneqr_mstack___rrR__fR_A_mno_mfast_mbranches = 817,
    structuredvm_specialized_instruction_opcode__Abt_mstack__fR_A_mno_mfast_mbranches = 818,
    structuredvm_specialized_instruction_opcode__Apushr_mbeqr_mstack___rr0___rr0__fR_A_mno_mfast_mbranches = 819,
    structuredvm_specialized_instruction_opcode__Apushr_mbeqr_mstack___rr0___rr1__fR_A_mno_mfast_mbranches = 820,
    structuredvm_specialized_instruction_opcode__Apushr_mbeqr_mstack___rr0___rrR__fR_A_mno_mfast_mbranches = 821,
    structuredvm_specialized_instruction_opcode__Apushr_mbeqr_mstack___rr1___rr0__fR_A_mno_mfast_mbranches = 822,
    structuredvm_specialized_instruction_opcode__Apushr_mbeqr_mstack___rr1___rr1__fR_A_mno_mfast_mbranches = 823,
    structuredvm_specialized_instruction_opcode__Apushr_mbeqr_mstack___rr1___rrR__fR_A_mno_mfast_mbranches = 824,
    structuredvm_specialized_instruction_opcode__Apushr_mbeqr_mstack___rrR___rr0__fR_A_mno_mfast_mbranches = 825,
    structuredvm_specialized_instruction_opcode__Apushr_mbeqr_mstack___rrR___rr1__fR_A_mno_mfast_mbranches = 826,
    structuredvm_specialized_instruction_opcode__Apushr_mbeqr_mstack___rrR___rrR__fR_A_mno_mfast_mbranches = 827,
    structuredvm_specialized_instruction_opcode__Apushr_mbger_mstack___rr0___rr0__fR_A_mno_mfast_mbranches = 828,
    structuredvm_specialized_instruction_opcode__Apushr_mbger_mstack___rr0___rr1__fR_A_mno_mfast_mbranches = 829,
    structuredvm_specialized_instruction_opcode__Apushr_mbger_mstack___rr0___rrR__fR_A_mno_mfast_mbranches = 830,
    structuredvm_specialized_instruction_opcode__Apushr_mbger_mstack___rr1___rr0__fR_A_mno_mfast_mbranches = 831,
    structuredvm_specialized_instruction_opcode__Apushr_mbger_mstack___rr1___rr1__fR_A_mno_mfast_mbranches = 832,
    structuredvm_specialized_instruction_opcode__Apushr_mbger_mstack___rr1___rrR__fR_A_mno_mfast_mbranches = 833,
    structuredvm_specialized_instruction_opcode__Apushr_mbger_mstack___rrR___rr0__fR_A_mno_mfast_mbranches = 834,
    structuredvm_specialized_instruction_opcode__Apushr_mbger_mstack___rrR___rr1__fR_A_mno_mfast_mbranches = 835,
    structuredvm_specialized_instruction_opcode__Apushr_mbger_mstack___rrR___rrR__fR_A_mno_mfast_mbranches = 836
  };

#define STRUCTUREDVM_SPECIALIZED_INSTRUCTION_NO 837

#endif // #ifndef STRUCTUREDVM_SPECIALIZED_INSTRUCTIONS_H_
/* How many residuals we can have at most.  This, with some dispatching models,
   is needed to compute a slow register offset from the base. */
#define STRUCTUREDVM_MAX_RESIDUAL_ARITY  3

/* User-specified code, late header part: beginning. */

/* User-specified code, late header part: end */


/* Close the multiple-inclusion guard opened in the template. */
#endif // #ifndef STRUCTUREDVM_VM_H_
