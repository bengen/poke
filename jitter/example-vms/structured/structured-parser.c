/* A Bison parser, made by GNU Bison 3.0.4.  */

/* Bison implementation for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015 Free Software Foundation, Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "3.0.4"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 1

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1

/* Substitute the type names.  */
#define YYSTYPE         STRUCTURED_STYPE
#define YYLTYPE         STRUCTURED_LTYPE
/* Substitute the variable and function names.  */
#define yyparse         structured_parse
#define yylex           structured_lex
#define yyerror         structured_error
#define yydebug         structured_debug
#define yynerrs         structured_nerrs


/* Copy the first part of user declarations.  */
#line 24 "../../jitter/example-vms/structured/structured.y" /* yacc.c:339  */

#include <stdio.h>
#include <ctype.h>
#include <jitter/jitter-malloc.h>
#include <jitter/jitter-fatal.h>
#include <jitter/jitter-parse-int.h>
#include <jitter/jitter-string.h>

#include "structured-syntax.h"
#include "structured-parser.h"
#include "structured-scanner.h"

/* This is currently a fatal error.  I could longjmp away instead. */
static void
structured_error (YYLTYPE *locp, struct structured_program *p,
                  yyscan_t scanner, char *message)
  __attribute__ ((noreturn));

#define STRUCTURED_PARSE_ERROR(message)                            \
  do                                                               \
    {                                                              \
      structured_error (structured_get_lloc (structured_scanner),  \
                        p, structured_scanner, message);           \
    }                                                              \
  while (false)

/* What would be yytext in a non-reentrant scanner. */
#define STRUCTURED_TEXT \
  (structured_get_text (structured_scanner))

 /* What would be yylineno in a non-reentrant scanner. */
#define STRUCTURED_LINENO \
  (structured_get_lineno (structured_scanner))

/* A copy of what would be yytext in a non-reentrant scanner. */
#define STRUCTURED_TEXT_COPY \
  (jitter_clone_string (STRUCTURED_TEXT))

/* Return a pointer to a fresh malloc-allocated expression of the given case.
   No field is initialized but case_. */
static struct structured_expression*
structured_make_expression (enum structured_expression_case case_)
{
  struct structured_expression *res
    = jitter_xmalloc (sizeof (struct structured_expression));
  res->case_ = case_;

  return res;
}

/* Return a pointer to a fresh malloc-allocated expression of the primitive
   case, with the given binary primitive and operands.  Every field is
   initalized. */
static struct structured_expression*
structured_make_binary (enum structured_primitive primitive,
                        struct structured_expression *operand_0,
                        struct structured_expression *operand_1)
{
  struct structured_expression *res
    = structured_make_expression (structured_expression_case_primitive);
  res->primitive = primitive;
  res->primitive_operand_0 = operand_0;
  res->primitive_operand_1 = operand_1;
  return res;
}

/* Return a pointer to a fresh malloc-allocated expression of the primitive
   case, with the given nullary primitive.  Every field is initalized. */
static struct structured_expression*
structured_make_nullary (enum structured_primitive primitive)
{
  return structured_make_binary (primitive, NULL, NULL);
}

/* Return a pointer to a fresh malloc-allocated expression of the primitive
   case, with the given unary primitive and operand.  Every field is
   initalized. */
static struct structured_expression*
structured_make_unary (enum structured_primitive primitive,
                       struct structured_expression *operand_0)
{
  return structured_make_binary (primitive, operand_0, NULL);
}

/* Return a pointer to a fresh malloc-allocated statement of the given case.
   No field is initialized but case_. */
static struct structured_statement*
structured_make_statement (enum structured_statement_case case_)
{
  struct structured_statement *res
    = jitter_xmalloc (sizeof (struct structured_statement));
  res->case_ = case_;

  return res;
}

/* If the pointed expressions is non-NULL return a pointer to a fresh
   malloc-allocated statement containing a sequence setting the given variable
   to the pointed expression, and then the pointed statement.  If the expression
   pointer is NULL just return the pointed body without allocating anything
   more. */
static struct structured_statement*
structured_with_optional_initialization (structured_variable v,
                                         struct structured_expression *e,
                                         struct structured_statement *body)
{
  if (e == NULL)
    return body;
  else
    {
      struct structured_statement *sequence
        = structured_make_statement (structured_statement_case_sequence);
      struct structured_statement *assignment
        = structured_make_statement (structured_statement_case_assignment);
      assignment->assignment_variable = v;
      assignment->assignment_expression = e;
      sequence->sequence_statement_0 = assignment;
      sequence->sequence_statement_1 = body;
      return sequence;
    }
}


#line 198 "../../jitter/example-vms/structured/structured-parser.c" /* yacc.c:339  */

# ifndef YY_NULLPTR
#  if defined __cplusplus && 201103L <= __cplusplus
#   define YY_NULLPTR nullptr
#  else
#   define YY_NULLPTR 0
#  endif
# endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 0
#endif

/* In a future release of Bison, this section will be replaced
   by #include "structured-parser.h".  */
#ifndef YY_STRUCTURED_JITTER_EXAMPLE_VMS_STRUCTURED_STRUCTURED_PARSER_H_INCLUDED
# define YY_STRUCTURED_JITTER_EXAMPLE_VMS_STRUCTURED_STRUCTURED_PARSER_H_INCLUDED
/* Debug traces.  */
#ifndef STRUCTURED_DEBUG
# if defined YYDEBUG
#if YYDEBUG
#   define STRUCTURED_DEBUG 1
#  else
#   define STRUCTURED_DEBUG 0
#  endif
# else /* ! defined YYDEBUG */
#  define STRUCTURED_DEBUG 0
# endif /* ! defined YYDEBUG */
#endif  /* ! defined STRUCTURED_DEBUG */
#if STRUCTURED_DEBUG
extern int structured_debug;
#endif
/* "%code requires" blocks.  */
#line 178 "../../jitter/example-vms/structured/structured.y" /* yacc.c:355  */

/* Simplified error-reporting facilities calling structured_error, suitable to be
   called from the scanner and the parser without the complicated and
   irrelevant parameters needed by structured_error . */
void
structured_scan_error (void *structured_scanner) __attribute__ ((noreturn));

struct structured_program *
structured_parse_file_star (FILE *input_file);

struct structured_program *
structured_parse_file (const char *input_file_name);

#line 250 "../../jitter/example-vms/structured/structured-parser.c" /* yacc.c:355  */

/* Token type.  */
#ifndef STRUCTURED_TOKENTYPE
# define STRUCTURED_TOKENTYPE
  enum structured_tokentype
  {
    BEGIN_ = 258,
    END = 259,
    SKIP = 260,
    VAR = 261,
    PRINT = 262,
    INPUT = 263,
    SET_TO = 264,
    SEMICOLON = 265,
    COMMA = 266,
    IF = 267,
    THEN = 268,
    ELSE = 269,
    ELIF = 270,
    WHILE = 271,
    DO = 272,
    REPEAT = 273,
    UNTIL = 274,
    OPEN_PAREN = 275,
    CLOSE_PAREN = 276,
    VARIABLE = 277,
    DECIMAL_LITERAL = 278,
    TRUE = 279,
    FALSE = 280,
    PLUS = 281,
    MINUS = 282,
    TIMES = 283,
    DIVIDED = 284,
    REMAINDER = 285,
    EQUAL = 286,
    DIFFERENT = 287,
    LESS = 288,
    LESS_OR_EQUAL = 289,
    GREATER = 290,
    GREATER_OR_EQUAL = 291,
    LOGICAL_OR = 292,
    LOGICAL_AND = 293,
    LOGICAL_NOT = 294,
    UNARY_MINUS = 295
  };
#endif
/* Tokens.  */
#define BEGIN_ 258
#define END 259
#define SKIP 260
#define VAR 261
#define PRINT 262
#define INPUT 263
#define SET_TO 264
#define SEMICOLON 265
#define COMMA 266
#define IF 267
#define THEN 268
#define ELSE 269
#define ELIF 270
#define WHILE 271
#define DO 272
#define REPEAT 273
#define UNTIL 274
#define OPEN_PAREN 275
#define CLOSE_PAREN 276
#define VARIABLE 277
#define DECIMAL_LITERAL 278
#define TRUE 279
#define FALSE 280
#define PLUS 281
#define MINUS 282
#define TIMES 283
#define DIVIDED 284
#define REMAINDER 285
#define EQUAL 286
#define DIFFERENT 287
#define LESS 288
#define LESS_OR_EQUAL 289
#define GREATER 290
#define GREATER_OR_EQUAL 291
#define LOGICAL_OR 292
#define LOGICAL_AND 293
#define LOGICAL_NOT 294
#define UNARY_MINUS 295

/* Value type.  */
#if ! defined STRUCTURED_STYPE && ! defined STRUCTURED_STYPE_IS_DECLARED

union STRUCTURED_STYPE
{
#line 193 "../../jitter/example-vms/structured/structured.y" /* yacc.c:355  */

  jitter_int literal;
  structured_variable variable;
  struct structured_expression *expression;
  struct structured_statement *statement;

#line 349 "../../jitter/example-vms/structured/structured-parser.c" /* yacc.c:355  */
};

typedef union STRUCTURED_STYPE STRUCTURED_STYPE;
# define STRUCTURED_STYPE_IS_TRIVIAL 1
# define STRUCTURED_STYPE_IS_DECLARED 1
#endif

/* Location type.  */
#if ! defined STRUCTURED_LTYPE && ! defined STRUCTURED_LTYPE_IS_DECLARED
typedef struct STRUCTURED_LTYPE STRUCTURED_LTYPE;
struct STRUCTURED_LTYPE
{
  int first_line;
  int first_column;
  int last_line;
  int last_column;
};
# define STRUCTURED_LTYPE_IS_DECLARED 1
# define STRUCTURED_LTYPE_IS_TRIVIAL 1
#endif



int structured_parse (struct structured_program *p, void* structured_scanner);

#endif /* !YY_STRUCTURED_JITTER_EXAMPLE_VMS_STRUCTURED_STRUCTURED_PARSER_H_INCLUDED  */

/* Copy the second part of user declarations.  */

#line 379 "../../jitter/example-vms/structured/structured-parser.c" /* yacc.c:358  */

#ifdef short
# undef short
#endif

#ifdef YYTYPE_UINT8
typedef YYTYPE_UINT8 yytype_uint8;
#else
typedef unsigned char yytype_uint8;
#endif

#ifdef YYTYPE_INT8
typedef YYTYPE_INT8 yytype_int8;
#else
typedef signed char yytype_int8;
#endif

#ifdef YYTYPE_UINT16
typedef YYTYPE_UINT16 yytype_uint16;
#else
typedef unsigned short int yytype_uint16;
#endif

#ifdef YYTYPE_INT16
typedef YYTYPE_INT16 yytype_int16;
#else
typedef short int yytype_int16;
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif ! defined YYSIZE_T
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned int
# endif
#endif

#define YYSIZE_MAXIMUM ((YYSIZE_T) -1)

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(Msgid) dgettext ("bison-runtime", Msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(Msgid) Msgid
# endif
#endif

#ifndef YY_ATTRIBUTE
# if (defined __GNUC__                                               \
      && (2 < __GNUC__ || (__GNUC__ == 2 && 96 <= __GNUC_MINOR__)))  \
     || defined __SUNPRO_C && 0x5110 <= __SUNPRO_C
#  define YY_ATTRIBUTE(Spec) __attribute__(Spec)
# else
#  define YY_ATTRIBUTE(Spec) /* empty */
# endif
#endif

#ifndef YY_ATTRIBUTE_PURE
# define YY_ATTRIBUTE_PURE   YY_ATTRIBUTE ((__pure__))
#endif

#ifndef YY_ATTRIBUTE_UNUSED
# define YY_ATTRIBUTE_UNUSED YY_ATTRIBUTE ((__unused__))
#endif

#if !defined _Noreturn \
     && (!defined __STDC_VERSION__ || __STDC_VERSION__ < 201112)
# if defined _MSC_VER && 1200 <= _MSC_VER
#  define _Noreturn __declspec (noreturn)
# else
#  define _Noreturn YY_ATTRIBUTE ((__noreturn__))
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(E) ((void) (E))
#else
# define YYUSE(E) /* empty */
#endif

#if defined __GNUC__ && 407 <= __GNUC__ * 100 + __GNUC_MINOR__
/* Suppress an incorrect diagnostic about yylval being uninitialized.  */
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN \
    _Pragma ("GCC diagnostic push") \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")\
    _Pragma ("GCC diagnostic ignored \"-Wmaybe-uninitialized\"")
# define YY_IGNORE_MAYBE_UNINITIALIZED_END \
    _Pragma ("GCC diagnostic pop")
#else
# define YY_INITIAL_VALUE(Value) Value
#endif
#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
# define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif


#if ! defined yyoverflow || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined EXIT_SUCCESS
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
      /* Use EXIT_SUCCESS as a witness for stdlib.h.  */
#     ifndef EXIT_SUCCESS
#      define EXIT_SUCCESS 0
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's 'empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined YYMALLOC || defined malloc) \
             && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined EXIT_SUCCESS
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* ! defined yyoverflow || YYERROR_VERBOSE */


#if (! defined yyoverflow \
     && (! defined __cplusplus \
         || (defined STRUCTURED_LTYPE_IS_TRIVIAL && STRUCTURED_LTYPE_IS_TRIVIAL \
             && defined STRUCTURED_STYPE_IS_TRIVIAL && STRUCTURED_STYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yytype_int16 yyss_alloc;
  YYSTYPE yyvs_alloc;
  YYLTYPE yyls_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (sizeof (yytype_int16) + sizeof (YYSTYPE) + sizeof (YYLTYPE)) \
      + 2 * YYSTACK_GAP_MAXIMUM)

# define YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)                           \
    do                                                                  \
      {                                                                 \
        YYSIZE_T yynewbytes;                                            \
        YYCOPY (&yyptr->Stack_alloc, Stack, yysize);                    \
        Stack = &yyptr->Stack_alloc;                                    \
        yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAXIMUM; \
        yyptr += yynewbytes / sizeof (*yyptr);                          \
      }                                                                 \
    while (0)

#endif

#if defined YYCOPY_NEEDED && YYCOPY_NEEDED
/* Copy COUNT objects from SRC to DST.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(Dst, Src, Count) \
      __builtin_memcpy (Dst, Src, (Count) * sizeof (*(Src)))
#  else
#   define YYCOPY(Dst, Src, Count)              \
      do                                        \
        {                                       \
          YYSIZE_T yyi;                         \
          for (yyi = 0; yyi < (Count); yyi++)   \
            (Dst)[yyi] = (Src)[yyi];            \
        }                                       \
      while (0)
#  endif
# endif
#endif /* !YYCOPY_NEEDED */

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  33
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   396

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  41
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  18
/* YYNRULES -- Number of rules.  */
#define YYNRULES  54
/* YYNSTATES -- Number of states.  */
#define YYNSTATES  112

/* YYTRANSLATE[YYX] -- Symbol number corresponding to YYX as returned
   by yylex, with out-of-bounds checking.  */
#define YYUNDEFTOK  2
#define YYMAXUTOK   295

#define YYTRANSLATE(YYX)                                                \
  ((unsigned int) (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[TOKEN-NUM] -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex, without out-of-bounds checking.  */
static const yytype_uint8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40
};

#if STRUCTURED_DEBUG
  /* YYRLINE[YYN] -- Source line where rule number YYN was defined.  */
static const yytype_uint16 yyrline[] =
{
       0,   241,   241,   246,   248,   252,   255,   257,   259,   274,
     281,   289,   292,   297,   303,   304,   309,   311,   315,   320,
     327,   329,   335,   336,   341,   344,   347,   349,   351,   353,
     355,   357,   359,   361,   363,   365,   367,   369,   371,   373,
     375,   383,   391,   393,   398,   408,   413,   418,   420,   422,
     427,   431,   433,   439,   446
};
#endif

#if STRUCTURED_DEBUG || YYERROR_VERBOSE || 0
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "BEGIN_", "END", "SKIP", "VAR", "PRINT",
  "INPUT", "SET_TO", "SEMICOLON", "COMMA", "IF", "THEN", "ELSE", "ELIF",
  "WHILE", "DO", "REPEAT", "UNTIL", "OPEN_PAREN", "CLOSE_PAREN",
  "VARIABLE", "DECIMAL_LITERAL", "TRUE", "FALSE", "PLUS", "MINUS", "TIMES",
  "DIVIDED", "REMAINDER", "EQUAL", "DIFFERENT", "LESS", "LESS_OR_EQUAL",
  "GREATER", "GREATER_OR_EQUAL", "LOGICAL_OR", "LOGICAL_AND",
  "LOGICAL_NOT", "UNARY_MINUS", "$accept", "program", "statement",
  "if_statement", "if_statement_rest", "statements",
  "one_or_more_statements", "block", "block_rest",
  "optional_initialization", "expression", "if_expression",
  "if_expression_rest", "literal", "variable", "optional_skip", "begin",
  "end", YY_NULLPTR
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[NUM] -- (External) token number corresponding to the
   (internal) symbol number NUM (which must be that of a token).  */
static const yytype_uint16 yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290,   291,   292,   293,   294,
     295
};
# endif

#define YYPACT_NINF -83

#define yypact_value_is_default(Yystate) \
  (!!((Yystate) == (-83)))

#define YYTABLE_NINF -52

#define yytable_value_is_error(Yytable_value) \
  0

  /* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
     STATE-NUM.  */
static const yytype_int16 yypact[] =
{
      90,   -83,   -83,   -14,    10,    10,    10,    90,   -83,    14,
      90,   -83,   -83,     7,    11,    90,   -83,   -12,   -83,    10,
      10,   -83,   -83,   -83,    10,    10,   118,   -83,   -83,   -83,
     186,   311,    -2,   -83,   -83,    10,   -83,    16,    10,     1,
     212,   -83,   345,   -83,   -83,   -83,    10,    10,    10,    10,
      10,    10,    10,    10,    10,    10,    10,    10,    10,    90,
      90,    10,   147,   -83,   -83,   358,   347,   -14,   -83,    10,
     -83,   130,   130,   171,   195,   195,   -33,   -33,   -33,   -33,
     -33,   -33,   -13,   -83,     9,    16,   160,   -83,   -83,   -83,
     289,    90,    10,   -83,   -83,   -83,   -83,    10,    10,   -83,
      16,   238,   105,   264,   -83,    90,   -83,    10,     9,   289,
     -83,   -83
};

  /* YYDEFACT[STATE-NUM] -- Default reduction number in state STATE-NUM.
     Performed when YYTABLE does not specify something else to do.  Zero
     means the default is an error.  */
static const yytype_uint8 yydefact[] =
{
      14,    53,    52,     0,     0,     0,     0,    14,    50,     0,
      16,     2,    15,     0,     0,    14,    18,    22,    43,     0,
       0,    47,    48,    49,     0,     0,     0,    24,    25,     7,
       0,     0,     0,     1,    17,     0,     3,     0,     0,     0,
       0,    27,     0,    30,    42,     5,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,    14,
      14,     0,     0,    54,     6,    23,    51,     0,    19,     0,
      26,    28,    29,    31,    32,    33,    34,    35,    36,    37,
      38,    39,    41,    40,     0,     0,     0,     4,    20,    21,
       0,    14,     0,    10,    11,     8,     9,     0,     0,    44,
       0,     0,     0,     0,    13,    14,    46,     0,     0,     0,
      12,    45
};

  /* YYPGOTO[NTERM-NUM].  */
static const yytype_int8 yypgoto[] =
{
     -83,   -83,   -83,   -83,   -82,    -6,    -4,   -40,   -83,   -83,
      23,   -83,   -78,   -83,     0,   -83,   -83,   -35
};

  /* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int8 yydefgoto[] =
{
      -1,     9,    10,    29,    93,    11,    12,    16,    68,    39,
      26,    41,    99,    27,    28,    14,    15,    94
};

  /* YYTABLE[YYPACT[STATE-NUM]] -- What to do in state STATE-NUM.  If
     positive, shift that token.  If negative, reduce the rule whose
     number is the opposite.  If YYTABLE_NINF, syntax error.  */
static const yytype_int8 yytable[] =
{
      13,    32,    64,    17,    57,    58,    34,    13,     8,    37,
      13,    66,    67,    63,    33,    13,    35,    61,    18,    38,
      63,    36,    19,    91,    92,    58,   110,    89,    30,    31,
      20,   111,     8,    21,    22,    23,     0,    24,     0,     0,
       0,     0,    40,    42,     0,     0,     0,    43,    44,    25,
      95,     0,     0,    84,    85,     0,     0,     0,    62,    13,
      13,    65,    88,     0,     0,   104,    13,    17,     0,    71,
      72,    73,    74,    75,    76,    77,    78,    79,    80,    81,
      82,    83,     0,     0,    86,   100,     0,     0,     0,     0,
       0,    13,    90,     1,     0,     2,     3,     4,     0,   108,
     -51,     0,     5,     0,     0,    13,     6,     0,     7,   106,
       0,     0,     8,     0,     0,   101,     0,     0,     0,     0,
     102,   103,     0,     0,     0,     0,     0,     0,    45,     0,
     109,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    56,    57,    58,    46,    47,    48,    49,    50,    51,
      52,    53,    54,    55,    56,    57,    58,    87,    48,    49,
      50,    51,    52,    53,    54,    55,    56,    57,    58,     0,
      96,     0,     0,    46,    47,    48,    49,    50,    51,    52,
      53,    54,    55,    56,    57,    58,    46,    47,    48,    49,
      50,    51,    52,    53,    54,    55,    56,    57,    58,    59,
      49,    50,    51,    52,    53,    54,    55,    56,    57,    58,
       0,     0,    46,    47,    48,    49,    50,    51,    52,    53,
      54,    55,    56,    57,    58,    69,    51,    52,    53,    54,
      55,    56,    57,    58,     0,     0,     0,     0,    46,    47,
      48,    49,    50,    51,    52,    53,    54,    55,    56,    57,
      58,   105,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,    46,    47,    48,    49,    50,    51,
      52,    53,    54,    55,    56,    57,    58,   107,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
      46,    47,    48,    49,    50,    51,    52,    53,    54,    55,
      56,    57,    58,    97,    98,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,    46,    47,    48,    49,    50,
      51,    52,    53,    54,    55,    56,    57,    58,    60,     0,
       0,     0,     0,     0,     0,     0,     0,    46,    47,    48,
      49,    50,    51,    52,    53,    54,    55,    56,    57,    58,
       1,     0,     2,     3,     4,     0,     0,     0,     0,     5,
       0,     0,     0,     6,     0,     7,    70,     0,     0,     8,
       0,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    56,    57,    58,    46,    47,    48,    49,    50,    51,
      52,    53,    54,    55,    56,    57,    58
};

static const yytype_int8 yycheck[] =
{
       0,     7,    37,     3,    37,    38,    10,     7,    22,    15,
      10,    10,    11,     4,     0,    15,     9,    19,     8,    31,
       4,    10,    12,    14,    15,    38,   108,    67,     5,     6,
      20,   109,    22,    23,    24,    25,    -1,    27,    -1,    -1,
      -1,    -1,    19,    20,    -1,    -1,    -1,    24,    25,    39,
      85,    -1,    -1,    59,    60,    -1,    -1,    -1,    35,    59,
      60,    38,    66,    -1,    -1,   100,    66,    67,    -1,    46,
      47,    48,    49,    50,    51,    52,    53,    54,    55,    56,
      57,    58,    -1,    -1,    61,    91,    -1,    -1,    -1,    -1,
      -1,    91,    69,     3,    -1,     5,     6,     7,    -1,   105,
      10,    -1,    12,    -1,    -1,   105,    16,    -1,    18,     4,
      -1,    -1,    22,    -1,    -1,    92,    -1,    -1,    -1,    -1,
      97,    98,    -1,    -1,    -1,    -1,    -1,    -1,    10,    -1,
     107,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    26,    27,    28,    29,    30,    31,
      32,    33,    34,    35,    36,    37,    38,    10,    28,    29,
      30,    31,    32,    33,    34,    35,    36,    37,    38,    -1,
      10,    -1,    -1,    26,    27,    28,    29,    30,    31,    32,
      33,    34,    35,    36,    37,    38,    26,    27,    28,    29,
      30,    31,    32,    33,    34,    35,    36,    37,    38,    13,
      29,    30,    31,    32,    33,    34,    35,    36,    37,    38,
      -1,    -1,    26,    27,    28,    29,    30,    31,    32,    33,
      34,    35,    36,    37,    38,    13,    31,    32,    33,    34,
      35,    36,    37,    38,    -1,    -1,    -1,    -1,    26,    27,
      28,    29,    30,    31,    32,    33,    34,    35,    36,    37,
      38,    13,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    26,    27,    28,    29,    30,    31,
      32,    33,    34,    35,    36,    37,    38,    13,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      26,    27,    28,    29,    30,    31,    32,    33,    34,    35,
      36,    37,    38,    14,    15,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    26,    27,    28,    29,    30,
      31,    32,    33,    34,    35,    36,    37,    38,    17,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    26,    27,    28,
      29,    30,    31,    32,    33,    34,    35,    36,    37,    38,
       3,    -1,     5,     6,     7,    -1,    -1,    -1,    -1,    12,
      -1,    -1,    -1,    16,    -1,    18,    21,    -1,    -1,    22,
      -1,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    26,    27,    28,    29,    30,    31,
      32,    33,    34,    35,    36,    37,    38
};

  /* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
     symbol of state STATE-NUM.  */
static const yytype_uint8 yystos[] =
{
       0,     3,     5,     6,     7,    12,    16,    18,    22,    42,
      43,    46,    47,    55,    56,    57,    48,    55,     8,    12,
      20,    23,    24,    25,    27,    39,    51,    54,    55,    44,
      51,    51,    46,     0,    47,     9,    10,    46,    31,    50,
      51,    52,    51,    51,    51,    10,    26,    27,    28,    29,
      30,    31,    32,    33,    34,    35,    36,    37,    38,    13,
      17,    19,    51,     4,    58,    51,    10,    11,    49,    13,
      21,    51,    51,    51,    51,    51,    51,    51,    51,    51,
      51,    51,    51,    51,    46,    46,    51,    10,    47,    48,
      51,    14,    15,    45,    58,    58,    10,    14,    15,    53,
      46,    51,    51,    51,    58,    13,     4,    13,    46,    51,
      45,    53
};

  /* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const yytype_uint8 yyr1[] =
{
       0,    41,    42,    43,    43,    43,    43,    43,    43,    43,
      44,    45,    45,    45,    46,    46,    47,    47,    47,    48,
      49,    49,    50,    50,    51,    51,    51,    51,    51,    51,
      51,    51,    51,    51,    51,    51,    51,    51,    51,    51,
      51,    51,    51,    51,    52,    53,    53,    54,    54,    54,
      55,    56,    56,    57,    58
};

  /* YYR2[YYN] -- Number of symbols on the right hand side of rule YYN.  */
static const yytype_uint8 yyr2[] =
{
       0,     2,     1,     2,     4,     3,     3,     2,     5,     5,
       4,     1,     5,     3,     0,     1,     1,     2,     2,     3,
       2,     2,     0,     2,     1,     1,     3,     2,     3,     3,
       2,     3,     3,     3,     3,     3,     3,     3,     3,     3,
       3,     3,     2,     1,     4,     5,     3,     1,     1,     1,
       1,     0,     1,     1,     1
};


#define yyerrok         (yyerrstatus = 0)
#define yyclearin       (yychar = YYEMPTY)
#define YYEMPTY         (-2)
#define YYEOF           0

#define YYACCEPT        goto yyacceptlab
#define YYABORT         goto yyabortlab
#define YYERROR         goto yyerrorlab


#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)                                  \
do                                                              \
  if (yychar == YYEMPTY)                                        \
    {                                                           \
      yychar = (Token);                                         \
      yylval = (Value);                                         \
      YYPOPSTACK (yylen);                                       \
      yystate = *yyssp;                                         \
      goto yybackup;                                            \
    }                                                           \
  else                                                          \
    {                                                           \
      yyerror (&yylloc, p, structured_scanner, YY_("syntax error: cannot back up")); \
      YYERROR;                                                  \
    }                                                           \
while (0)

/* Error token number */
#define YYTERROR        1
#define YYERRCODE       256


/* YYLLOC_DEFAULT -- Set CURRENT to span from RHS[1] to RHS[N].
   If N is 0, then set CURRENT to the empty location which ends
   the previous symbol: RHS[0] (always defined).  */

#ifndef YYLLOC_DEFAULT
# define YYLLOC_DEFAULT(Current, Rhs, N)                                \
    do                                                                  \
      if (N)                                                            \
        {                                                               \
          (Current).first_line   = YYRHSLOC (Rhs, 1).first_line;        \
          (Current).first_column = YYRHSLOC (Rhs, 1).first_column;      \
          (Current).last_line    = YYRHSLOC (Rhs, N).last_line;         \
          (Current).last_column  = YYRHSLOC (Rhs, N).last_column;       \
        }                                                               \
      else                                                              \
        {                                                               \
          (Current).first_line   = (Current).last_line   =              \
            YYRHSLOC (Rhs, 0).last_line;                                \
          (Current).first_column = (Current).last_column =              \
            YYRHSLOC (Rhs, 0).last_column;                              \
        }                                                               \
    while (0)
#endif

#define YYRHSLOC(Rhs, K) ((Rhs)[K])


/* Enable debugging if requested.  */
#if STRUCTURED_DEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)                        \
do {                                            \
  if (yydebug)                                  \
    YYFPRINTF Args;                             \
} while (0)


/* YY_LOCATION_PRINT -- Print the location on the stream.
   This macro was not mandated originally: define only if we know
   we won't break user code: when these are the locations we know.  */

#ifndef YY_LOCATION_PRINT
# if defined STRUCTURED_LTYPE_IS_TRIVIAL && STRUCTURED_LTYPE_IS_TRIVIAL

/* Print *YYLOCP on YYO.  Private, do not rely on its existence. */

YY_ATTRIBUTE_UNUSED
static unsigned
yy_location_print_ (FILE *yyo, YYLTYPE const * const yylocp)
{
  unsigned res = 0;
  int end_col = 0 != yylocp->last_column ? yylocp->last_column - 1 : 0;
  if (0 <= yylocp->first_line)
    {
      res += YYFPRINTF (yyo, "%d", yylocp->first_line);
      if (0 <= yylocp->first_column)
        res += YYFPRINTF (yyo, ".%d", yylocp->first_column);
    }
  if (0 <= yylocp->last_line)
    {
      if (yylocp->first_line < yylocp->last_line)
        {
          res += YYFPRINTF (yyo, "-%d", yylocp->last_line);
          if (0 <= end_col)
            res += YYFPRINTF (yyo, ".%d", end_col);
        }
      else if (0 <= end_col && yylocp->first_column < end_col)
        res += YYFPRINTF (yyo, "-%d", end_col);
    }
  return res;
 }

#  define YY_LOCATION_PRINT(File, Loc)          \
  yy_location_print_ (File, &(Loc))

# else
#  define YY_LOCATION_PRINT(File, Loc) ((void) 0)
# endif
#endif


# define YY_SYMBOL_PRINT(Title, Type, Value, Location)                    \
do {                                                                      \
  if (yydebug)                                                            \
    {                                                                     \
      YYFPRINTF (stderr, "%s ", Title);                                   \
      yy_symbol_print (stderr,                                            \
                  Type, Value, Location, p, structured_scanner); \
      YYFPRINTF (stderr, "\n");                                           \
    }                                                                     \
} while (0)


/*----------------------------------------.
| Print this symbol's value on YYOUTPUT.  |
`----------------------------------------*/

static void
yy_symbol_value_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep, YYLTYPE const * const yylocationp, struct structured_program *p, void* structured_scanner)
{
  FILE *yyo = yyoutput;
  YYUSE (yyo);
  YYUSE (yylocationp);
  YYUSE (p);
  YYUSE (structured_scanner);
  if (!yyvaluep)
    return;
# ifdef YYPRINT
  if (yytype < YYNTOKENS)
    YYPRINT (yyoutput, yytoknum[yytype], *yyvaluep);
# endif
  YYUSE (yytype);
}


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

static void
yy_symbol_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep, YYLTYPE const * const yylocationp, struct structured_program *p, void* structured_scanner)
{
  YYFPRINTF (yyoutput, "%s %s (",
             yytype < YYNTOKENS ? "token" : "nterm", yytname[yytype]);

  YY_LOCATION_PRINT (yyoutput, *yylocationp);
  YYFPRINTF (yyoutput, ": ");
  yy_symbol_value_print (yyoutput, yytype, yyvaluep, yylocationp, p, structured_scanner);
  YYFPRINTF (yyoutput, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

static void
yy_stack_print (yytype_int16 *yybottom, yytype_int16 *yytop)
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)                            \
do {                                                            \
  if (yydebug)                                                  \
    yy_stack_print ((Bottom), (Top));                           \
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

static void
yy_reduce_print (yytype_int16 *yyssp, YYSTYPE *yyvsp, YYLTYPE *yylsp, int yyrule, struct structured_program *p, void* structured_scanner)
{
  unsigned long int yylno = yyrline[yyrule];
  int yynrhs = yyr2[yyrule];
  int yyi;
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %lu):\n",
             yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr,
                       yystos[yyssp[yyi + 1 - yynrhs]],
                       &(yyvsp[(yyi + 1) - (yynrhs)])
                       , &(yylsp[(yyi + 1) - (yynrhs)])                       , p, structured_scanner);
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)          \
do {                                    \
  if (yydebug)                          \
    yy_reduce_print (yyssp, yyvsp, yylsp, Rule, p, structured_scanner); \
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !STRUCTURED_DEBUG */
# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !STRUCTURED_DEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif


#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined __GLIBC__ && defined _STRING_H
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
static YYSIZE_T
yystrlen (const char *yystr)
{
  YYSIZE_T yylen;
  for (yylen = 0; yystr[yylen]; yylen++)
    continue;
  return yylen;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
static char *
yystpcpy (char *yydest, const char *yysrc)
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

# ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYSIZE_T
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      YYSIZE_T yyn = 0;
      char const *yyp = yystr;

      for (;;)
        switch (*++yyp)
          {
          case '\'':
          case ',':
            goto do_not_strip_quotes;

          case '\\':
            if (*++yyp != '\\')
              goto do_not_strip_quotes;
            /* Fall through.  */
          default:
            if (yyres)
              yyres[yyn] = *yyp;
            yyn++;
            break;

          case '"':
            if (yyres)
              yyres[yyn] = '\0';
            return yyn;
          }
    do_not_strip_quotes: ;
    }

  if (! yyres)
    return yystrlen (yystr);

  return yystpcpy (yyres, yystr) - yyres;
}
# endif

/* Copy into *YYMSG, which is of size *YYMSG_ALLOC, an error message
   about the unexpected token YYTOKEN for the state stack whose top is
   YYSSP.

   Return 0 if *YYMSG was successfully written.  Return 1 if *YYMSG is
   not large enough to hold the message.  In that case, also set
   *YYMSG_ALLOC to the required number of bytes.  Return 2 if the
   required number of bytes is too large to store.  */
static int
yysyntax_error (YYSIZE_T *yymsg_alloc, char **yymsg,
                yytype_int16 *yyssp, int yytoken)
{
  YYSIZE_T yysize0 = yytnamerr (YY_NULLPTR, yytname[yytoken]);
  YYSIZE_T yysize = yysize0;
  enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
  /* Internationalized format string. */
  const char *yyformat = YY_NULLPTR;
  /* Arguments of yyformat. */
  char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
  /* Number of reported tokens (one for the "unexpected", one per
     "expected"). */
  int yycount = 0;

  /* There are many possibilities here to consider:
     - If this state is a consistent state with a default action, then
       the only way this function was invoked is if the default action
       is an error action.  In that case, don't check for expected
       tokens because there are none.
     - The only way there can be no lookahead present (in yychar) is if
       this state is a consistent state with a default action.  Thus,
       detecting the absence of a lookahead is sufficient to determine
       that there is no unexpected or expected token to report.  In that
       case, just report a simple "syntax error".
     - Don't assume there isn't a lookahead just because this state is a
       consistent state with a default action.  There might have been a
       previous inconsistent state, consistent state with a non-default
       action, or user semantic action that manipulated yychar.
     - Of course, the expected token list depends on states to have
       correct lookahead information, and it depends on the parser not
       to perform extra reductions after fetching a lookahead from the
       scanner and before detecting a syntax error.  Thus, state merging
       (from LALR or IELR) and default reductions corrupt the expected
       token list.  However, the list is correct for canonical LR with
       one exception: it will still contain any token that will not be
       accepted due to an error action in a later state.
  */
  if (yytoken != YYEMPTY)
    {
      int yyn = yypact[*yyssp];
      yyarg[yycount++] = yytname[yytoken];
      if (!yypact_value_is_default (yyn))
        {
          /* Start YYX at -YYN if negative to avoid negative indexes in
             YYCHECK.  In other words, skip the first -YYN actions for
             this state because they are default actions.  */
          int yyxbegin = yyn < 0 ? -yyn : 0;
          /* Stay within bounds of both yycheck and yytname.  */
          int yychecklim = YYLAST - yyn + 1;
          int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
          int yyx;

          for (yyx = yyxbegin; yyx < yyxend; ++yyx)
            if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR
                && !yytable_value_is_error (yytable[yyx + yyn]))
              {
                if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
                  {
                    yycount = 1;
                    yysize = yysize0;
                    break;
                  }
                yyarg[yycount++] = yytname[yyx];
                {
                  YYSIZE_T yysize1 = yysize + yytnamerr (YY_NULLPTR, yytname[yyx]);
                  if (! (yysize <= yysize1
                         && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
                    return 2;
                  yysize = yysize1;
                }
              }
        }
    }

  switch (yycount)
    {
# define YYCASE_(N, S)                      \
      case N:                               \
        yyformat = S;                       \
      break
      YYCASE_(0, YY_("syntax error"));
      YYCASE_(1, YY_("syntax error, unexpected %s"));
      YYCASE_(2, YY_("syntax error, unexpected %s, expecting %s"));
      YYCASE_(3, YY_("syntax error, unexpected %s, expecting %s or %s"));
      YYCASE_(4, YY_("syntax error, unexpected %s, expecting %s or %s or %s"));
      YYCASE_(5, YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s"));
# undef YYCASE_
    }

  {
    YYSIZE_T yysize1 = yysize + yystrlen (yyformat);
    if (! (yysize <= yysize1 && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
      return 2;
    yysize = yysize1;
  }

  if (*yymsg_alloc < yysize)
    {
      *yymsg_alloc = 2 * yysize;
      if (! (yysize <= *yymsg_alloc
             && *yymsg_alloc <= YYSTACK_ALLOC_MAXIMUM))
        *yymsg_alloc = YYSTACK_ALLOC_MAXIMUM;
      return 1;
    }

  /* Avoid sprintf, as that infringes on the user's name space.
     Don't have undefined behavior even if the translation
     produced a string with the wrong number of "%s"s.  */
  {
    char *yyp = *yymsg;
    int yyi = 0;
    while ((*yyp = *yyformat) != '\0')
      if (*yyp == '%' && yyformat[1] == 's' && yyi < yycount)
        {
          yyp += yytnamerr (yyp, yyarg[yyi++]);
          yyformat += 2;
        }
      else
        {
          yyp++;
          yyformat++;
        }
  }
  return 0;
}
#endif /* YYERROR_VERBOSE */

/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

static void
yydestruct (const char *yymsg, int yytype, YYSTYPE *yyvaluep, YYLTYPE *yylocationp, struct structured_program *p, void* structured_scanner)
{
  YYUSE (yyvaluep);
  YYUSE (yylocationp);
  YYUSE (p);
  YYUSE (structured_scanner);
  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YYUSE (yytype);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}




/*----------.
| yyparse.  |
`----------*/

int
yyparse (struct structured_program *p, void* structured_scanner)
{
/* The lookahead symbol.  */
int yychar;


/* The semantic value of the lookahead symbol.  */
/* Default value used for initialization, for pacifying older GCCs
   or non-GCC compilers.  */
YY_INITIAL_VALUE (static YYSTYPE yyval_default;)
YYSTYPE yylval YY_INITIAL_VALUE (= yyval_default);

/* Location data for the lookahead symbol.  */
static YYLTYPE yyloc_default
# if defined STRUCTURED_LTYPE_IS_TRIVIAL && STRUCTURED_LTYPE_IS_TRIVIAL
  = { 1, 1, 1, 1 }
# endif
;
YYLTYPE yylloc = yyloc_default;

    /* Number of syntax errors so far.  */
    int yynerrs;

    int yystate;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus;

    /* The stacks and their tools:
       'yyss': related to states.
       'yyvs': related to semantic values.
       'yyls': related to locations.

       Refer to the stacks through separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* The state stack.  */
    yytype_int16 yyssa[YYINITDEPTH];
    yytype_int16 *yyss;
    yytype_int16 *yyssp;

    /* The semantic value stack.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs;
    YYSTYPE *yyvsp;

    /* The location stack.  */
    YYLTYPE yylsa[YYINITDEPTH];
    YYLTYPE *yyls;
    YYLTYPE *yylsp;

    /* The locations where the error started and ended.  */
    YYLTYPE yyerror_range[3];

    YYSIZE_T yystacksize;

  int yyn;
  int yyresult;
  /* Lookahead token as an internal (translated) token number.  */
  int yytoken = 0;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;
  YYLTYPE yyloc;

#if YYERROR_VERBOSE
  /* Buffer for error messages, and its allocated size.  */
  char yymsgbuf[128];
  char *yymsg = yymsgbuf;
  YYSIZE_T yymsg_alloc = sizeof yymsgbuf;
#endif

#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N), yylsp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  yyssp = yyss = yyssa;
  yyvsp = yyvs = yyvsa;
  yylsp = yyls = yylsa;
  yystacksize = YYINITDEPTH;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY; /* Cause a token to be read.  */
  yylsp[0] = yylloc;
  goto yysetstate;

/*------------------------------------------------------------.
| yynewstate -- Push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
 yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;

 yysetstate:
  *yyssp = yystate;

  if (yyss + yystacksize - 1 <= yyssp)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = yyssp - yyss + 1;

#ifdef yyoverflow
      {
        /* Give user a chance to reallocate the stack.  Use copies of
           these so that the &'s don't force the real ones into
           memory.  */
        YYSTYPE *yyvs1 = yyvs;
        yytype_int16 *yyss1 = yyss;
        YYLTYPE *yyls1 = yyls;

        /* Each stack pointer address is followed by the size of the
           data in use in that stack, in bytes.  This used to be a
           conditional around just the two extra args, but that might
           be undefined if yyoverflow is a macro.  */
        yyoverflow (YY_("memory exhausted"),
                    &yyss1, yysize * sizeof (*yyssp),
                    &yyvs1, yysize * sizeof (*yyvsp),
                    &yyls1, yysize * sizeof (*yylsp),
                    &yystacksize);

        yyls = yyls1;
        yyss = yyss1;
        yyvs = yyvs1;
      }
#else /* no yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto yyexhaustedlab;
# else
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
        goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
        yystacksize = YYMAXDEPTH;

      {
        yytype_int16 *yyss1 = yyss;
        union yyalloc *yyptr =
          (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
        if (! yyptr)
          goto yyexhaustedlab;
        YYSTACK_RELOCATE (yyss_alloc, yyss);
        YYSTACK_RELOCATE (yyvs_alloc, yyvs);
        YYSTACK_RELOCATE (yyls_alloc, yyls);
#  undef YYSTACK_RELOCATE
        if (yyss1 != yyssa)
          YYSTACK_FREE (yyss1);
      }
# endif
#endif /* no yyoverflow */

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;
      yylsp = yyls + yysize - 1;

      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
                  (unsigned long int) yystacksize));

      if (yyss + yystacksize - 1 <= yyssp)
        YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;

/*-----------.
| yybackup.  |
`-----------*/
yybackup:

  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yypact_value_is_default (yyn))
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid lookahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = yylex (&yylval, &yylloc, structured_scanner);
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yytable_value_is_error (yyn))
        goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);

  /* Discard the shifted token.  */
  yychar = YYEMPTY;

  yystate = yyn;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END
  *++yylsp = yylloc;
  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     '$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];

  /* Default location.  */
  YYLLOC_DEFAULT (yyloc, (yylsp - yylen), yylen);
  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
        case 2:
#line 242 "../../jitter/example-vms/structured/structured.y" /* yacc.c:1646  */
    { p->main_statement = (yyvsp[0].statement); }
#line 1695 "../../jitter/example-vms/structured/structured-parser.c" /* yacc.c:1646  */
    break;

  case 3:
#line 247 "../../jitter/example-vms/structured/structured.y" /* yacc.c:1646  */
    { (yyval.statement) = structured_make_statement (structured_statement_case_skip); }
#line 1701 "../../jitter/example-vms/structured/structured-parser.c" /* yacc.c:1646  */
    break;

  case 4:
#line 249 "../../jitter/example-vms/structured/structured.y" /* yacc.c:1646  */
    { (yyval.statement) = structured_make_statement (structured_statement_case_assignment);
    (yyval.statement)->assignment_variable = (yyvsp[-3].variable);
    (yyval.statement)->assignment_expression = (yyvsp[-1].expression); }
#line 1709 "../../jitter/example-vms/structured/structured-parser.c" /* yacc.c:1646  */
    break;

  case 5:
#line 253 "../../jitter/example-vms/structured/structured.y" /* yacc.c:1646  */
    { (yyval.statement) = structured_make_statement (structured_statement_case_print);
    (yyval.statement)->print_expression = (yyvsp[-1].expression); }
#line 1716 "../../jitter/example-vms/structured/structured-parser.c" /* yacc.c:1646  */
    break;

  case 6:
#line 256 "../../jitter/example-vms/structured/structured.y" /* yacc.c:1646  */
    { (yyval.statement) = (yyvsp[-1].statement); }
#line 1722 "../../jitter/example-vms/structured/structured-parser.c" /* yacc.c:1646  */
    break;

  case 7:
#line 258 "../../jitter/example-vms/structured/structured.y" /* yacc.c:1646  */
    { (yyval.statement) = (yyvsp[0].statement); }
#line 1728 "../../jitter/example-vms/structured/structured-parser.c" /* yacc.c:1646  */
    break;

  case 8:
#line 260 "../../jitter/example-vms/structured/structured.y" /* yacc.c:1646  */
    { /* Parse "while A do B end" as "if A then repeat B until not A else
       skip". */
    struct structured_statement *r
      = structured_make_statement (structured_statement_case_repeat_until);
    r->repeat_until_body = (yyvsp[-1].statement);
    /* FIXME: clone $2 into a separate heap object, if I want to be able to free
       ASTs. */
    r->repeat_until_guard
      = structured_make_unary (structured_primitive_logical_not, (yyvsp[-3].expression));
    (yyval.statement) = structured_make_statement (structured_statement_case_if_then_else);
    (yyval.statement)->if_then_else_condition = (yyvsp[-3].expression);
    (yyval.statement)->if_then_else_then_branch = r;
    (yyval.statement)->if_then_else_else_branch
      = structured_make_statement (structured_statement_case_skip); }
#line 1747 "../../jitter/example-vms/structured/structured-parser.c" /* yacc.c:1646  */
    break;

  case 9:
#line 275 "../../jitter/example-vms/structured/structured.y" /* yacc.c:1646  */
    { (yyval.statement) = structured_make_statement (structured_statement_case_repeat_until);
    (yyval.statement)->repeat_until_body = (yyvsp[-3].statement);
    (yyval.statement)->repeat_until_guard = (yyvsp[-1].expression); }
#line 1755 "../../jitter/example-vms/structured/structured-parser.c" /* yacc.c:1646  */
    break;

  case 10:
#line 282 "../../jitter/example-vms/structured/structured.y" /* yacc.c:1646  */
    { (yyval.statement) = structured_make_statement (structured_statement_case_if_then_else);
    (yyval.statement)->if_then_else_condition = (yyvsp[-3].expression);
    (yyval.statement)->if_then_else_then_branch = (yyvsp[-1].statement);
    (yyval.statement)->if_then_else_else_branch = (yyvsp[0].statement); }
#line 1764 "../../jitter/example-vms/structured/structured-parser.c" /* yacc.c:1646  */
    break;

  case 11:
#line 290 "../../jitter/example-vms/structured/structured.y" /* yacc.c:1646  */
    { /* Parse "if A then B end" as "if A then B else skip end". */
    (yyval.statement) = structured_make_statement (structured_statement_case_skip); }
#line 1771 "../../jitter/example-vms/structured/structured-parser.c" /* yacc.c:1646  */
    break;

  case 12:
#line 293 "../../jitter/example-vms/structured/structured.y" /* yacc.c:1646  */
    { (yyval.statement) = structured_make_statement (structured_statement_case_if_then_else);
    (yyval.statement)->if_then_else_condition = (yyvsp[-3].expression);
    (yyval.statement)->if_then_else_then_branch = (yyvsp[-1].statement);
    (yyval.statement)->if_then_else_else_branch = (yyvsp[0].statement); }
#line 1780 "../../jitter/example-vms/structured/structured-parser.c" /* yacc.c:1646  */
    break;

  case 13:
#line 298 "../../jitter/example-vms/structured/structured.y" /* yacc.c:1646  */
    { (yyval.statement) = (yyvsp[-1].statement); }
#line 1786 "../../jitter/example-vms/structured/structured-parser.c" /* yacc.c:1646  */
    break;

  case 14:
#line 303 "../../jitter/example-vms/structured/structured.y" /* yacc.c:1646  */
    { (yyval.statement) = structured_make_statement (structured_statement_case_skip); }
#line 1792 "../../jitter/example-vms/structured/structured-parser.c" /* yacc.c:1646  */
    break;

  case 15:
#line 305 "../../jitter/example-vms/structured/structured.y" /* yacc.c:1646  */
    { (yyval.statement) = (yyvsp[0].statement); }
#line 1798 "../../jitter/example-vms/structured/structured-parser.c" /* yacc.c:1646  */
    break;

  case 16:
#line 310 "../../jitter/example-vms/structured/structured.y" /* yacc.c:1646  */
    { (yyval.statement) = (yyvsp[0].statement); }
#line 1804 "../../jitter/example-vms/structured/structured-parser.c" /* yacc.c:1646  */
    break;

  case 17:
#line 312 "../../jitter/example-vms/structured/structured.y" /* yacc.c:1646  */
    { (yyval.statement) = structured_make_statement (structured_statement_case_sequence);
    (yyval.statement)->sequence_statement_0 = (yyvsp[-1].statement);
    (yyval.statement)->sequence_statement_1 = (yyvsp[0].statement); }
#line 1812 "../../jitter/example-vms/structured/structured-parser.c" /* yacc.c:1646  */
    break;

  case 18:
#line 316 "../../jitter/example-vms/structured/structured.y" /* yacc.c:1646  */
    { (yyval.statement) = (yyvsp[0].statement); }
#line 1818 "../../jitter/example-vms/structured/structured-parser.c" /* yacc.c:1646  */
    break;

  case 19:
#line 321 "../../jitter/example-vms/structured/structured.y" /* yacc.c:1646  */
    { (yyval.statement) = structured_make_statement (structured_statement_case_block);
    (yyval.statement)->block_variable = (yyvsp[-2].variable);
    (yyval.statement)->block_body = structured_with_optional_initialization ((yyvsp[-2].variable), (yyvsp[-1].expression), (yyvsp[0].statement)); }
#line 1826 "../../jitter/example-vms/structured/structured-parser.c" /* yacc.c:1646  */
    break;

  case 20:
#line 328 "../../jitter/example-vms/structured/structured.y" /* yacc.c:1646  */
    { (yyval.statement) = (yyvsp[0].statement); }
#line 1832 "../../jitter/example-vms/structured/structured-parser.c" /* yacc.c:1646  */
    break;

  case 21:
#line 330 "../../jitter/example-vms/structured/structured.y" /* yacc.c:1646  */
    { (yyval.statement) = (yyvsp[0].statement); }
#line 1838 "../../jitter/example-vms/structured/structured-parser.c" /* yacc.c:1646  */
    break;

  case 22:
#line 335 "../../jitter/example-vms/structured/structured.y" /* yacc.c:1646  */
    { (yyval.expression) = NULL; }
#line 1844 "../../jitter/example-vms/structured/structured-parser.c" /* yacc.c:1646  */
    break;

  case 23:
#line 337 "../../jitter/example-vms/structured/structured.y" /* yacc.c:1646  */
    { (yyval.expression) = (yyvsp[0].expression); }
#line 1850 "../../jitter/example-vms/structured/structured-parser.c" /* yacc.c:1646  */
    break;

  case 24:
#line 342 "../../jitter/example-vms/structured/structured.y" /* yacc.c:1646  */
    { (yyval.expression) = structured_make_expression (structured_expression_case_literal);
    (yyval.expression)->literal = (yyvsp[0].literal); }
#line 1857 "../../jitter/example-vms/structured/structured-parser.c" /* yacc.c:1646  */
    break;

  case 25:
#line 345 "../../jitter/example-vms/structured/structured.y" /* yacc.c:1646  */
    { (yyval.expression) = structured_make_expression (structured_expression_case_variable);
    (yyval.expression)->variable = (yyvsp[0].variable); }
#line 1864 "../../jitter/example-vms/structured/structured-parser.c" /* yacc.c:1646  */
    break;

  case 26:
#line 348 "../../jitter/example-vms/structured/structured.y" /* yacc.c:1646  */
    { (yyval.expression) = (yyvsp[-1].expression); }
#line 1870 "../../jitter/example-vms/structured/structured-parser.c" /* yacc.c:1646  */
    break;

  case 27:
#line 350 "../../jitter/example-vms/structured/structured.y" /* yacc.c:1646  */
    { (yyval.expression) = (yyvsp[0].expression); }
#line 1876 "../../jitter/example-vms/structured/structured-parser.c" /* yacc.c:1646  */
    break;

  case 28:
#line 352 "../../jitter/example-vms/structured/structured.y" /* yacc.c:1646  */
    { (yyval.expression) = structured_make_binary (structured_primitive_plus, (yyvsp[-2].expression), (yyvsp[0].expression)); }
#line 1882 "../../jitter/example-vms/structured/structured-parser.c" /* yacc.c:1646  */
    break;

  case 29:
#line 354 "../../jitter/example-vms/structured/structured.y" /* yacc.c:1646  */
    { (yyval.expression) = structured_make_binary (structured_primitive_minus, (yyvsp[-2].expression), (yyvsp[0].expression)); }
#line 1888 "../../jitter/example-vms/structured/structured-parser.c" /* yacc.c:1646  */
    break;

  case 30:
#line 356 "../../jitter/example-vms/structured/structured.y" /* yacc.c:1646  */
    { (yyval.expression) = structured_make_unary (structured_primitive_unary_minus, (yyvsp[0].expression)); }
#line 1894 "../../jitter/example-vms/structured/structured-parser.c" /* yacc.c:1646  */
    break;

  case 31:
#line 358 "../../jitter/example-vms/structured/structured.y" /* yacc.c:1646  */
    { (yyval.expression) = structured_make_binary (structured_primitive_times, (yyvsp[-2].expression), (yyvsp[0].expression)); }
#line 1900 "../../jitter/example-vms/structured/structured-parser.c" /* yacc.c:1646  */
    break;

  case 32:
#line 360 "../../jitter/example-vms/structured/structured.y" /* yacc.c:1646  */
    { (yyval.expression) = structured_make_binary (structured_primitive_divided, (yyvsp[-2].expression), (yyvsp[0].expression)); }
#line 1906 "../../jitter/example-vms/structured/structured-parser.c" /* yacc.c:1646  */
    break;

  case 33:
#line 362 "../../jitter/example-vms/structured/structured.y" /* yacc.c:1646  */
    { (yyval.expression) = structured_make_binary (structured_primitive_remainder, (yyvsp[-2].expression), (yyvsp[0].expression)); }
#line 1912 "../../jitter/example-vms/structured/structured-parser.c" /* yacc.c:1646  */
    break;

  case 34:
#line 364 "../../jitter/example-vms/structured/structured.y" /* yacc.c:1646  */
    { (yyval.expression) = structured_make_binary (structured_primitive_equal, (yyvsp[-2].expression), (yyvsp[0].expression)); }
#line 1918 "../../jitter/example-vms/structured/structured-parser.c" /* yacc.c:1646  */
    break;

  case 35:
#line 366 "../../jitter/example-vms/structured/structured.y" /* yacc.c:1646  */
    { (yyval.expression) = structured_make_binary (structured_primitive_different, (yyvsp[-2].expression), (yyvsp[0].expression)); }
#line 1924 "../../jitter/example-vms/structured/structured-parser.c" /* yacc.c:1646  */
    break;

  case 36:
#line 368 "../../jitter/example-vms/structured/structured.y" /* yacc.c:1646  */
    { (yyval.expression) = structured_make_binary (structured_primitive_less, (yyvsp[-2].expression), (yyvsp[0].expression)); }
#line 1930 "../../jitter/example-vms/structured/structured-parser.c" /* yacc.c:1646  */
    break;

  case 37:
#line 370 "../../jitter/example-vms/structured/structured.y" /* yacc.c:1646  */
    { (yyval.expression) = structured_make_binary (structured_primitive_less_or_equal, (yyvsp[-2].expression), (yyvsp[0].expression)); }
#line 1936 "../../jitter/example-vms/structured/structured-parser.c" /* yacc.c:1646  */
    break;

  case 38:
#line 372 "../../jitter/example-vms/structured/structured.y" /* yacc.c:1646  */
    { (yyval.expression) = structured_make_binary (structured_primitive_greater, (yyvsp[-2].expression), (yyvsp[0].expression)); }
#line 1942 "../../jitter/example-vms/structured/structured-parser.c" /* yacc.c:1646  */
    break;

  case 39:
#line 374 "../../jitter/example-vms/structured/structured.y" /* yacc.c:1646  */
    { (yyval.expression) = structured_make_binary (structured_primitive_greater_or_equal, (yyvsp[-2].expression), (yyvsp[0].expression)); }
#line 1948 "../../jitter/example-vms/structured/structured-parser.c" /* yacc.c:1646  */
    break;

  case 40:
#line 376 "../../jitter/example-vms/structured/structured.y" /* yacc.c:1646  */
    { /* Parse "A and B" as "if A then B else false end". */
    (yyval.expression) = structured_make_expression (structured_expression_case_if_then_else);
    (yyval.expression)->if_then_else_condition = (yyvsp[-2].expression);
    (yyval.expression)->if_then_else_then_branch = (yyvsp[0].expression);
    (yyval.expression)->if_then_else_else_branch
      = structured_make_expression (structured_expression_case_literal);
    (yyval.expression)->if_then_else_else_branch->literal = 0; }
#line 1960 "../../jitter/example-vms/structured/structured-parser.c" /* yacc.c:1646  */
    break;

  case 41:
#line 384 "../../jitter/example-vms/structured/structured.y" /* yacc.c:1646  */
    { /* Parse "A or B" as "if A then true else B end". */
    (yyval.expression) = structured_make_expression (structured_expression_case_if_then_else);
    (yyval.expression)->if_then_else_condition = (yyvsp[-2].expression);
    (yyval.expression)->if_then_else_then_branch
      = structured_make_expression (structured_expression_case_literal);
    (yyval.expression)->if_then_else_then_branch->literal = 1;
    (yyval.expression)->if_then_else_else_branch = (yyvsp[0].expression); }
#line 1972 "../../jitter/example-vms/structured/structured-parser.c" /* yacc.c:1646  */
    break;

  case 42:
#line 392 "../../jitter/example-vms/structured/structured.y" /* yacc.c:1646  */
    { (yyval.expression) = structured_make_unary (structured_primitive_logical_not, (yyvsp[0].expression)); }
#line 1978 "../../jitter/example-vms/structured/structured-parser.c" /* yacc.c:1646  */
    break;

  case 43:
#line 394 "../../jitter/example-vms/structured/structured.y" /* yacc.c:1646  */
    { (yyval.expression) = structured_make_nullary (structured_primitive_input); }
#line 1984 "../../jitter/example-vms/structured/structured-parser.c" /* yacc.c:1646  */
    break;

  case 44:
#line 399 "../../jitter/example-vms/structured/structured.y" /* yacc.c:1646  */
    { (yyval.expression) = structured_make_expression (structured_expression_case_if_then_else);
    (yyval.expression)->if_then_else_condition = (yyvsp[-3].expression);
    (yyval.expression)->if_then_else_then_branch = (yyvsp[-1].expression);
    (yyval.expression)->if_then_else_else_branch = (yyvsp[0].expression); }
#line 1993 "../../jitter/example-vms/structured/structured-parser.c" /* yacc.c:1646  */
    break;

  case 45:
#line 409 "../../jitter/example-vms/structured/structured.y" /* yacc.c:1646  */
    { (yyval.expression) = structured_make_expression (structured_expression_case_if_then_else);
    (yyval.expression)->if_then_else_condition = (yyvsp[-3].expression);
    (yyval.expression)->if_then_else_then_branch = (yyvsp[-1].expression);
    (yyval.expression)->if_then_else_else_branch = (yyvsp[0].expression); }
#line 2002 "../../jitter/example-vms/structured/structured-parser.c" /* yacc.c:1646  */
    break;

  case 46:
#line 414 "../../jitter/example-vms/structured/structured.y" /* yacc.c:1646  */
    { (yyval.expression) = (yyvsp[-1].expression); }
#line 2008 "../../jitter/example-vms/structured/structured-parser.c" /* yacc.c:1646  */
    break;

  case 47:
#line 419 "../../jitter/example-vms/structured/structured.y" /* yacc.c:1646  */
    { (yyval.literal) = jitter_string_to_long_long_unsafe (STRUCTURED_TEXT); }
#line 2014 "../../jitter/example-vms/structured/structured-parser.c" /* yacc.c:1646  */
    break;

  case 48:
#line 421 "../../jitter/example-vms/structured/structured.y" /* yacc.c:1646  */
    { (yyval.literal) = 1; }
#line 2020 "../../jitter/example-vms/structured/structured-parser.c" /* yacc.c:1646  */
    break;

  case 49:
#line 423 "../../jitter/example-vms/structured/structured.y" /* yacc.c:1646  */
    { (yyval.literal) = 0; }
#line 2026 "../../jitter/example-vms/structured/structured-parser.c" /* yacc.c:1646  */
    break;

  case 50:
#line 428 "../../jitter/example-vms/structured/structured.y" /* yacc.c:1646  */
    { (yyval.variable) = STRUCTURED_TEXT_COPY; }
#line 2032 "../../jitter/example-vms/structured/structured-parser.c" /* yacc.c:1646  */
    break;


#line 2036 "../../jitter/example-vms/structured/structured-parser.c" /* yacc.c:1646  */
      default: break;
    }
  /* User semantic actions sometimes alter yychar, and that requires
     that yytoken be updated with the new translation.  We take the
     approach of translating immediately before every use of yytoken.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     YYABORT, YYACCEPT, or YYERROR immediately after altering yychar or
     if it invokes YYBACKUP.  In the case of YYABORT or YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of YYERROR or YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  YY_SYMBOL_PRINT ("-> $$ =", yyr1[yyn], &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;
  *++yylsp = yyloc;

  /* Now 'shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTOKENS] + *yyssp;
  if (0 <= yystate && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTOKENS];

  goto yynewstate;


/*--------------------------------------.
| yyerrlab -- here on detecting error.  |
`--------------------------------------*/
yyerrlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  yytoken = yychar == YYEMPTY ? YYEMPTY : YYTRANSLATE (yychar);

  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if ! YYERROR_VERBOSE
      yyerror (&yylloc, p, structured_scanner, YY_("syntax error"));
#else
# define YYSYNTAX_ERROR yysyntax_error (&yymsg_alloc, &yymsg, \
                                        yyssp, yytoken)
      {
        char const *yymsgp = YY_("syntax error");
        int yysyntax_error_status;
        yysyntax_error_status = YYSYNTAX_ERROR;
        if (yysyntax_error_status == 0)
          yymsgp = yymsg;
        else if (yysyntax_error_status == 1)
          {
            if (yymsg != yymsgbuf)
              YYSTACK_FREE (yymsg);
            yymsg = (char *) YYSTACK_ALLOC (yymsg_alloc);
            if (!yymsg)
              {
                yymsg = yymsgbuf;
                yymsg_alloc = sizeof yymsgbuf;
                yysyntax_error_status = 2;
              }
            else
              {
                yysyntax_error_status = YYSYNTAX_ERROR;
                yymsgp = yymsg;
              }
          }
        yyerror (&yylloc, p, structured_scanner, yymsgp);
        if (yysyntax_error_status == 2)
          goto yyexhaustedlab;
      }
# undef YYSYNTAX_ERROR
#endif
    }

  yyerror_range[1] = yylloc;

  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
         error, discard it.  */

      if (yychar <= YYEOF)
        {
          /* Return failure if at end of input.  */
          if (yychar == YYEOF)
            YYABORT;
        }
      else
        {
          yydestruct ("Error: discarding",
                      yytoken, &yylval, &yylloc, p, structured_scanner);
          yychar = YYEMPTY;
        }
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:

  /* Pacify compilers like GCC when the user code never invokes
     YYERROR and the label yyerrorlab therefore never appears in user
     code.  */
  if (/*CONSTCOND*/ 0)
     goto yyerrorlab;

  yyerror_range[1] = yylsp[1-yylen];
  /* Do not reclaim the symbols of the rule whose action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;      /* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (!yypact_value_is_default (yyn))
        {
          yyn += YYTERROR;
          if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
            {
              yyn = yytable[yyn];
              if (0 < yyn)
                break;
            }
        }

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
        YYABORT;

      yyerror_range[1] = *yylsp;
      yydestruct ("Error: popping",
                  yystos[yystate], yyvsp, yylsp, p, structured_scanner);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END

  yyerror_range[2] = yylloc;
  /* Using YYLLOC is tempting, but would change the location of
     the lookahead.  YYLOC is available though.  */
  YYLLOC_DEFAULT (yyloc, yyerror_range, 2);
  *++yylsp = yyloc;

  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", yystos[yyn], yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

#if !defined yyoverflow || YYERROR_VERBOSE
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (&yylloc, p, structured_scanner, YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif

yyreturn:
  if (yychar != YYEMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      yytoken = YYTRANSLATE (yychar);
      yydestruct ("Cleanup: discarding lookahead",
                  yytoken, &yylval, &yylloc, p, structured_scanner);
    }
  /* Do not reclaim the symbols of the rule whose action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
                  yystos[*yyssp], yyvsp, yylsp, p, structured_scanner);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
#if YYERROR_VERBOSE
  if (yymsg != yymsgbuf)
    YYSTACK_FREE (yymsg);
#endif
  return yyresult;
}
#line 450 "../../jitter/example-vms/structured/structured.y" /* yacc.c:1906  */


void
structured_error (YYLTYPE *locp, struct structured_program *p, yyscan_t structured_scanner,
                 char *message)
{
  printf ("%s:%i: %s near \"%s\".\n",
          (p != NULL) ? p->source_file_name : "<INPUT>",
          structured_get_lineno (structured_scanner), message, STRUCTURED_TEXT);
  exit (EXIT_FAILURE);
}

void
structured_scan_error (void *structured_scanner)
{
  struct structured_program *p = NULL; /* A little hack to have p in scope. */
  STRUCTURED_PARSE_ERROR("scan error");
}

static struct structured_program *
structured_parse_file_star_with_name (FILE *input_file, const char *file_name)
{
  yyscan_t scanner;
  structured_lex_init (&scanner);
  structured_set_in (input_file, scanner);

  struct structured_program *res
    = jitter_xmalloc (sizeof (struct structured_program));
  res->source_file_name = jitter_clone_string (file_name);
  /* FIXME: if I ever make parsing errors non-fatal, call structured_lex_destroy before
     returning, and finalize the program -- which might be incomplete! */
  if (structured_parse (res, scanner))
    structured_error (structured_get_lloc (scanner), res, scanner, "parse error");
  structured_set_in (NULL, scanner);
  structured_lex_destroy (scanner);

  return res;
}

struct structured_program *
structured_parse_file_star (FILE *input_file)
{
  return structured_parse_file_star_with_name (input_file, "<stdin>");
}

struct structured_program *
structured_parse_file (const char *input_file_name)
{
  FILE *f;
  if ((f = fopen (input_file_name, "r")) == NULL)
    jitter_fatal ("failed opening file %s", input_file_name);

  /* FIXME: if I ever make parse errors non-fatal, I'll need to close the file
     before returning. */
  struct structured_program *res
    = structured_parse_file_star_with_name (f, input_file_name);
  fclose (f);
  return res;
}
