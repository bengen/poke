/* A Bison parser, made by GNU Bison 3.0.4.  */

/* Bison interface for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015 Free Software Foundation, Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

#ifndef YY_STRUCTURED_JITTER_EXAMPLE_VMS_STRUCTURED_STRUCTURED_PARSER_H_INCLUDED
# define YY_STRUCTURED_JITTER_EXAMPLE_VMS_STRUCTURED_STRUCTURED_PARSER_H_INCLUDED
/* Debug traces.  */
#ifndef STRUCTURED_DEBUG
# if defined YYDEBUG
#if YYDEBUG
#   define STRUCTURED_DEBUG 1
#  else
#   define STRUCTURED_DEBUG 0
#  endif
# else /* ! defined YYDEBUG */
#  define STRUCTURED_DEBUG 0
# endif /* ! defined YYDEBUG */
#endif  /* ! defined STRUCTURED_DEBUG */
#if STRUCTURED_DEBUG
extern int structured_debug;
#endif
/* "%code requires" blocks.  */
#line 178 "../../jitter/example-vms/structured/structured.y" /* yacc.c:1909  */

/* Simplified error-reporting facilities calling structured_error, suitable to be
   called from the scanner and the parser without the complicated and
   irrelevant parameters needed by structured_error . */
void
structured_scan_error (void *structured_scanner) __attribute__ ((noreturn));

struct structured_program *
structured_parse_file_star (FILE *input_file);

struct structured_program *
structured_parse_file (const char *input_file_name);

#line 66 "../../jitter/example-vms/structured/structured-parser.h" /* yacc.c:1909  */

/* Token type.  */
#ifndef STRUCTURED_TOKENTYPE
# define STRUCTURED_TOKENTYPE
  enum structured_tokentype
  {
    BEGIN_ = 258,
    END = 259,
    SKIP = 260,
    VAR = 261,
    PRINT = 262,
    INPUT = 263,
    SET_TO = 264,
    SEMICOLON = 265,
    COMMA = 266,
    IF = 267,
    THEN = 268,
    ELSE = 269,
    ELIF = 270,
    WHILE = 271,
    DO = 272,
    REPEAT = 273,
    UNTIL = 274,
    OPEN_PAREN = 275,
    CLOSE_PAREN = 276,
    VARIABLE = 277,
    DECIMAL_LITERAL = 278,
    TRUE = 279,
    FALSE = 280,
    PLUS = 281,
    MINUS = 282,
    TIMES = 283,
    DIVIDED = 284,
    REMAINDER = 285,
    EQUAL = 286,
    DIFFERENT = 287,
    LESS = 288,
    LESS_OR_EQUAL = 289,
    GREATER = 290,
    GREATER_OR_EQUAL = 291,
    LOGICAL_OR = 292,
    LOGICAL_AND = 293,
    LOGICAL_NOT = 294,
    UNARY_MINUS = 295
  };
#endif
/* Tokens.  */
#define BEGIN_ 258
#define END 259
#define SKIP 260
#define VAR 261
#define PRINT 262
#define INPUT 263
#define SET_TO 264
#define SEMICOLON 265
#define COMMA 266
#define IF 267
#define THEN 268
#define ELSE 269
#define ELIF 270
#define WHILE 271
#define DO 272
#define REPEAT 273
#define UNTIL 274
#define OPEN_PAREN 275
#define CLOSE_PAREN 276
#define VARIABLE 277
#define DECIMAL_LITERAL 278
#define TRUE 279
#define FALSE 280
#define PLUS 281
#define MINUS 282
#define TIMES 283
#define DIVIDED 284
#define REMAINDER 285
#define EQUAL 286
#define DIFFERENT 287
#define LESS 288
#define LESS_OR_EQUAL 289
#define GREATER 290
#define GREATER_OR_EQUAL 291
#define LOGICAL_OR 292
#define LOGICAL_AND 293
#define LOGICAL_NOT 294
#define UNARY_MINUS 295

/* Value type.  */
#if ! defined STRUCTURED_STYPE && ! defined STRUCTURED_STYPE_IS_DECLARED

union STRUCTURED_STYPE
{
#line 193 "../../jitter/example-vms/structured/structured.y" /* yacc.c:1909  */

  jitter_int literal;
  structured_variable variable;
  struct structured_expression *expression;
  struct structured_statement *statement;

#line 165 "../../jitter/example-vms/structured/structured-parser.h" /* yacc.c:1909  */
};

typedef union STRUCTURED_STYPE STRUCTURED_STYPE;
# define STRUCTURED_STYPE_IS_TRIVIAL 1
# define STRUCTURED_STYPE_IS_DECLARED 1
#endif

/* Location type.  */
#if ! defined STRUCTURED_LTYPE && ! defined STRUCTURED_LTYPE_IS_DECLARED
typedef struct STRUCTURED_LTYPE STRUCTURED_LTYPE;
struct STRUCTURED_LTYPE
{
  int first_line;
  int first_column;
  int last_line;
  int last_column;
};
# define STRUCTURED_LTYPE_IS_DECLARED 1
# define STRUCTURED_LTYPE_IS_TRIVIAL 1
#endif



int structured_parse (struct structured_program *p, void* structured_scanner);

#endif /* !YY_STRUCTURED_JITTER_EXAMPLE_VMS_STRUCTURED_STRUCTURED_PARSER_H_INCLUDED  */
