/* This code is machine-generated.  See its source for license
   information. This software is derived from software
   distributed under the GNU GPL version 3 or later. */

/* User-specified code, initial header part: beginning. */

/* User-specified code, initial header part: end */

/* VM library: main header file.

   Copyright (C) 2016, 2017, 2018, 2019, 2020 Luca Saiu
   Written by Luca Saiu

   This file is part of Jitter.

   Jitter is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Jitter is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Jitter.  If not, see <http://www.gnu.org/licenses/>. */


/* Generated file warning.
 * ************************************************************************** */

/* Unless this file is named exactly "vm.h" , without any prefix, you are
   looking at a machine-generated derived file.  The original source is the vm.h
   template from Jitter, with added code implementing the uninspired VM. */




/* This multiple-inclusion guard is opened here in the template, and will be
   closed at the end of the generated code.  It is normal to find no matching
   #endif in the template file.  */
#ifndef UNINSPIRED_VM_H_
#define UNINSPIRED_VM_H_


/* This is the main VM header to use from hand-written code.
 * ************************************************************************** */

#include <stdio.h>
#include <stdbool.h>

#include <jitter/jitter.h>
#include <jitter/jitter-hash.h>
#include <jitter/jitter-stack.h>
#include <jitter/jitter-instruction.h>
#include <jitter/jitter-mutable-routine.h>
#include <jitter/jitter-print.h>
#include <jitter/jitter-routine.h>
//#include <jitter/jitter-specialize.h> // FIXME: what about only declaring jitter_specialize in another header, and not including this?
#include <jitter/jitter-disassemble.h>
#include <jitter/jitter-vm.h>
#include <jitter/jitter-profile.h>
#include <jitter/jitter-data-locations.h>
#include <jitter/jitter-arithmetic.h>
#include <jitter/jitter-bitwise.h>
#include <jitter/jitter-signals.h>
#include <jitter/jitter-list.h>




/* Initialization and finalization.
 * ************************************************************************** */

/* Initialize the runtime state for the uninspired VM.  This needs to be called
   before using VM routines or VM states in any way. */
void
uninspired_initialize (void);

/* Finalize the runtime state, freeing some resources.  After calling this no
   use of VM routines or states is allowed.  It is possible to re-initialize
   after finalizing; these later re-initializations might be more efficient than
   the first initialization. */
void
uninspired_finalize (void);




/* State data structure initialization and finalization.
 * ************************************************************************** */

/* The machine state is separated into the backing and the more compact runtime
   data structures, to be allocated in registers as far as possible.  These are
   just a forward-declarations: the actual definitions are machine-generated. */
struct uninspired_state_backing;
struct uninspired_state_runtime;

/* A data structure containing both the backing and the runtime state.  This is
   a forward-declaration: the actual definition will come after both are
   defined. */
struct uninspired_state;

/* Initialize the pointed VM state data structure, or fail fatally.  The
   function definition is machine-generated, even if it may include user code.
   The state backing and runtime are initialized at the same time, and in fact
   the distinction between them is invisible to the VM user. */
void
uninspired_state_initialize (struct uninspired_state *state)
  __attribute__ ((nonnull (1)));

/* Finalize the pointed VM state data structure, or fail fatally.  The function
   definition is machine-generated, even if it may include user code.  The state
   backing and runtime are finalized at the same time. */
void
uninspired_state_finalize (struct uninspired_state *state)
  __attribute__ ((nonnull (1)));




/* State data structure: iteration.
 * ************************************************************************** */

/* The header of a doubly-linked list linking every state for the uninspired VM
   together.  This global is automatically wrapped, and therefore also
   accessible from VM instruction code. */
extern struct jitter_list_header * const
uninspired_states;

/* A pointer to the current state, only accessible from VM code.  This is usable
   for pointer comparison when iterating over states. */
#define UNINSPIRED_OWN_STATE                           \
  ((struct uninspired_state *) jitter_original_state)

/* Given an l-value of type struct uninspired_state * (usually a variable name)
   expand to a for loop statement iterating over every existing uninspired state
   using the l-value as iteration variable.  The expansion will execute the
   statement immediately following the macro call with the l-value in scope;
   in order words the loop body is not a macro argument, but follows the macro
   use.
   The l-value may be evaluated an unspecified number of times.
   This macro is safe to use within VM instruction code.
   For example:
     struct uninspired_state *s;
     UNINSPIRED_FOR_EACH_STATE (s)
       printf ("This is a state: %p\n", s); // (but printf unsafe in VM code) */
#define UNINSPIRED_FOR_EACH_STATE(jitter_state_iteration_lvalue)     \
  for ((jitter_state_iteration_lvalue)                             \
          = uninspired_states->first;                                \
       (jitter_state_iteration_lvalue)                             \
          != NULL;                                                 \
       (jitter_state_iteration_lvalue)                             \
         = (jitter_state_iteration_lvalue)->links.next)            \
    /* Here comes the body supplied by the user: no semicolon. */




/* Mutable routine initialization.
 * ************************************************************************** */

/* Return a freshly-allocated empty mutable routine for the uninspired VM. */
struct jitter_mutable_routine*
uninspired_make_mutable_routine (void)
  __attribute__ ((returns_nonnull));

/* Mutable routine finalization is actually VM-independent, but a definition of
   uninspired_destroy_mutable_routine is provided below as a macro, for cosmetic
   reasons. */


/* Mutable routines: code generation C API.
 * ************************************************************************** */

/* This is the preferred way of adding a new VM instruction to a pointed
   routine, more efficient than uninspired_mutable_routine_append_instruction_name
   even if only usable when the VM instruction opcode is known at compile time.
   The unspecialized instruction name must be explicitly mangled by the user as
   per the rules in jitterc_mangle.c .  For example an instruction named foo_bar
   can be added to the routine pointed by p with any one of
     uninspired_mutable_routine_append_instruction_name (p, "foo_bar");
   ,
     UNINSPIRED_MUTABLE_ROUTINE_APPEND_INSTRUCTION (p, foo_ubar);
   , and
     UNINSPIRED_MUTABLE_ROUTINE_APPEND_INSTRUCTION_ID 
        (p, uninspired_meta_instruction_id_foo_ubar);
   .
   The string "foo_bar" is not mangled, but the token foo_ubar is. */
#define UNINSPIRED_MUTABLE_ROUTINE_APPEND_INSTRUCTION(                 \
          routine_p, instruction_mangled_name_root)                  \
  do                                                                 \
    {                                                                \
      jitter_mutable_routine_append_meta_instruction                 \
         ((routine_p),                                               \
          uninspired_meta_instructions                                 \
          + JITTER_CONCATENATE_TWO(uninspired_meta_instruction_id_,    \
                                   instruction_mangled_name_root));  \
    }                                                                \
  while (false)

/* Append the unspecialized instruction whose id is given to the pointed routine.
   The id must be a case of enum uninspired_meta_instruction_id ; such cases have
   a name starting with uninspired_meta_instruction_id_ .
   This is slightly less convenient to use than UNINSPIRED_MUTABLE_ROUTINE_APPEND_INSTRUCTION
   but more general, as the instruction id is allowed to be a non-constant C
   expression. */
#define UNINSPIRED_MUTABLE_ROUTINE_APPEND_INSTRUCTION_ID(_jitter_routine_p,       \
                                                       _jitter_instruction_id)  \
  do                                                                            \
    {                                                                           \
      jitter_mutable_routine_append_instruction_id                              \
         ((_jitter_routine_p),                                                  \
          uninspired_meta_instructions,                                           \
          UNINSPIRED_META_INSTRUCTION_NO,                                         \
          (_jitter_instruction_id));                                            \
    }                                                                           \
  while (false)

/* This is the preferred way of appending a register argument to the instruction
   being added to the pointed routine, more convenient than directly using
   uninspired_mutable_routine_append_register_id_parameter , even if only usable
   when the register class is known at compile time.  Here the register class is
   only provided as a letter, but both the routine pointer and the register
   index are arbitrary C expressions.
   For example, in
     UNINSPIRED_MUTABLE_ROUTINE_APPEND_REGISTER_PARAMETER (p, r,
                                                         variable_to_index (x));
   the second macro argument "r" represents the register class named "r", and
   not the value of a variable named r. */
#define UNINSPIRED_MUTABLE_ROUTINE_APPEND_REGISTER_PARAMETER(routine_p,     \
                                                           class_letter,  \
                                                           index)         \
  do                                                                      \
    {                                                                     \
      uninspired_mutable_routine_append_register_parameter                  \
         ((routine_p),                                                    \
          & JITTER_CONCATENATE_TWO(uninspired_register_class_,              \
                                   class_letter),                         \
          (index));                                                       \
    }                                                                     \
  while (false)




/* Routine unified API: initialization.
 * ************************************************************************** */

/* See the comments above in "Mutable routines: initialization", and the
   implementation of the unified routine API in <jitter/jitter-routine.h> . */

#define uninspired_make_routine uninspired_make_mutable_routine




/* Routine unified API: code generation C API.
 * ************************************************************************** */

/* See the comments above in "Mutable routines: code generation C API". */

#define UNINSPIRED_ROUTINE_APPEND_INSTRUCTION  \
  UNINSPIRED_MUTABLE_ROUTINE_APPEND_INSTRUCTION
#define UNINSPIRED_ROUTINE_APPEND_INSTRUCTION_ID  \
  UNINSPIRED_MUTABLE_ROUTINE_APPEND_INSTRUCTION_ID
#define UNINSPIRED_ROUTINE_APPEND_REGISTER_PARAMETER  \
  UNINSPIRED_MUTABLE_ROUTINE_APPEND_REGISTER_PARAMETER




/* Array: special-purpose data.
 * ************************************************************************** */

/* The Array is a convenient place to store special-purpose data, accessible in
   an efficient way from a VM routine.
   Every item in special-purpose data is thread-local. */

/* The special-purpose data struct.  Every Array contains one of these at unbiased
   offset UNINSPIRED_SPECIAL_PURPOSE_STATE_DATA_UNBIASED_OFFSET from the unbiased
   beginning of the array.
   This entire struct is aligned to at least sizeof (jitter_int) bytes.  The
   entire struct is meant to be always accessed through a pointer-to-volatile,
   as its content may be altered from signal handlers and from different
   threads.  In particualar the user should use the macro
     UNINSPIRED_ARRAY_TO_SPECIAL_PURPOSE_STATE_DATA
   defined below and the macros defined from it as accessors.
   VM code accessing special-purpose data for its own state should use
     UNINSPIRED_SPECIAL_PURPOSE_STATE_DATA
   and the macros defined from it. */
struct jitter_special_purpose_state_data
{
  /* Notification fields.
   * ***************************************************************** */

  /* This is a Boolean flag, held as a word-sized datum so as to ensure
     atomicity in access.  It is also aligned to at least sizeof (jitter_int)
     bytes.
     Non-zero means that there is at least one notification pending, zero means
     that there are no notifications.  The flag specifies no other details: it
     is meant to be fast to check, with detailed information about each pending
     notification available elsewhere.
     It is the receiver's responsibility to periodically poll for notifications
     in application-specific "safe-points":
     A check can be inserted, for example, in all of these program points:
     a) at every backward branch;
     b) at every procedure entry;
     c) right after a call to each blocking primitive (as long as primitives
       can be interrupted).
     Safe-point checks are designed to be short and fast in the common case.  In
     the common case no action is required, and the VM routine should simply
     fall through.  If an action is required then control should branch off to a
     handler, where the user may implement the required behavior.
     It is mandatory that, as long as notifications can arrive, this field
     is reset to zero (when handling pending notifications) only by a thread
     running VM code in the state containing this struct.
     Other threads are allowed to set this to non-zero, in order to send a
     notification.  */
  jitter_int pending_notifications;

  /* Information about pending signal notifications.  If any signal is pending
     then pending_notifications must also be set, so that a notification check
     can always just quickly check pending_notifications, and then look at more
     details (including in pending_signal_notifications) only in the rare case
     of pending_notifications being true. */
  struct jitter_signal_notification *pending_signal_notifications;


  /* Profiling instrumentation fields.
   * ***************************************************************** */
  struct jitter_profile_runtime profile_runtime;
};




/* The Array and volatility.
 * ************************************************************************** */

/* Some fields of The Array, seen from VM code, are meant to be volatile, since
   they can be set by signal handlers or by other threads.  However it is
   acceptable to not see such changes immediately after they occur (notifications
   will get delayed, but not lost) and always accessing such data through a
   volatile struct is suboptimal.

   Non-VM code does need a volatile qualifier.

   Advanced dispatches already need a trick using inline assembly to make the
   base pointer (a biased pointer to The Array beginning) appear to
   spontaneously change beween instruction.  That is sufficient to express the
   degree of volatility required for this purpose.
   Simple dispatches, on targets where inline assembly may not be available at
   all, will use an actual volatile qualifier. */
#if defined (JITTER_DISPATCH_SWITCH)               \
    || defined (JITTER_DISPATCH_DIRECT_THREADING)
# define UNINSPIRED_ARRAY_VOLATILE_QUALIFIER volatile
#elif defined (JITTER_DISPATCH_MINIMAL_THREADING)  \
      || defined (JITTER_DISPATCH_NO_THREADING)
# define UNINSPIRED_ARRAY_VOLATILE_QUALIFIER /* nothing */
#else
# error "unknown dispatch: this should not happen"
#endif /* dispatch conditional */




/* Array element access: residuals, transfers, slow registers, and more.
 * ************************************************************************** */

/* In order to cover a wider range of addresses with simple base + register
   addressing the base does not necessarily point to the beginning of the Array;
   instead the base points to the beginning of the Array plus JITTER_ARRAY_BIAS
   bytes.
   FIXME: define the bias as a value appropriate to each architecture.  I think
   I should just move the definition to jitter-machine.h and provide a default
   here, in case the definition is missing on some architecture. */

/* FIXME: Horrible, horrible, horrible temporary workaround!

   This is a temporary workaround, very ugly and fragile, to compensate
   a limitation in jitter-specialize.c , which I will need to rewrite anyway.
   The problem is that jitter-specialize.c patches snippets to load non-label
   residuals in a VM-independent way based only on slow-register/memory residual
   indices, which is incorrect.  By using this particular bias I am cancelling
   that error.
   Test case, on a machine having only one register residual and a VM having just
     one fast register:
     [luca@moore ~/repos/jitter/_build/native-gcc-9]$ Q=bin/uninspired--no-threading; make $Q && echo 'mov 2, %r1' | libtool --mode=execute valgrind $Q --disassemble - --print-locations
   If this bias is wrong the slow-register accesses in mov/nR/%rR will use two
   different offsets, one for reading and another for writing.  With this
   workaround they will be the same.
   Good, with workadound (biased offset 0x0 from the base in %rbx):
    # 0x4a43d38: mov/nR/%rR 0x2, 0x20 (21 bytes):
        0x0000000004effb30 41 bc 02 00 00 00    	movl   $0x2,%r12d
        0x0000000004effb36 48 c7 43 00 20 00 00 00 	movq   $0x20,0x0(%rbx)
        0x0000000004effb3e 48 8b 13             	movq   (%rbx),%rdx
        0x0000000004effb41 4c 89 24 13          	movq   %r12,(%rbx,%rdx,1)
   Bad, with JITTER_ARRAY_BIAS defined as zero: first write at 0x0(%rbx)
                                                then read at 0x10(%rbx):
    # 0x4a43d38: mov/nR/%rR 0x2, 0x30 (22 bytes):
        0x0000000004effb30 41 bc 02 00 00 00    	movl   $0x2,%r12d
        0x0000000004effb36 48 c7 43 00 30 00 00 00 	movq   $0x30,0x0(%rbx)
        0x0000000004effb3e 48 8b 53 10          	movq   0x10(%rbx),%rdx
        0x0000000004effb42 4c 89 24 13          	movq   %r12,(%rbx,%rdx,1) */
#define JITTER_ARRAY_BIAS \
  (sizeof (struct jitter_special_purpose_state_data))
//#define JITTER_ARRAY_BIAS //0//(((jitter_int) 1 << 15))//(((jitter_int) 1 << 31))//0//0//16//0

/* Array-based globals are not implemented yet.  For the purpose of computing
   Array offsets I will say they are zero. */
#define UNINSPIRED_GLOBAL_NO 0

/* Transfer registers are not implemented yet.  For the purpose of computing
   Array offsets I will say they are zero. */
#define UNINSPIRED_TRANSFER_REGISTER_NO 0

/* Define macros holding offsets in bytes for the first global, memory residual
   and transfer register, from an initial Array pointer.
   In general we have to keep into account:
   - globals (word-sized);
   - special-purpose state data;
   - memory residuals (word-sized);
   - transfer registers (word-sized);
   - slow registers (uninspired_any_register-sized and aligned).
   Notice that memory
   residuals (meaning residuals stored in The Array) are zero on dispatching
   modes different from no-threading.  This relies on
   UNINSPIRED_MAX_MEMORY_RESIDUAL_ARITY , defined below, which in its turn depends
   on UNINSPIRED_MAX_RESIDUAL_ARITY, which is machine-generated. */
#define UNINSPIRED_FIRST_GLOBAL_UNBIASED_OFFSET  \
  0
#define UNINSPIRED_SPECIAL_PURPOSE_STATE_DATA_UNBIASED_OFFSET  \
  (UNINSPIRED_FIRST_GLOBAL_UNBIASED_OFFSET                     \
   + sizeof (jitter_int) * UNINSPIRED_GLOBAL_NO)
#define UNINSPIRED_FIRST_MEMORY_RESIDUAL_UNBIASED_OFFSET   \
  (UNINSPIRED_SPECIAL_PURPOSE_STATE_DATA_UNBIASED_OFFSET   \
   + sizeof (struct jitter_special_purpose_state_data))
#define UNINSPIRED_FIRST_TRANSFER_REGISTER_UNBIASED_OFFSET        \
  (UNINSPIRED_FIRST_MEMORY_RESIDUAL_UNBIASED_OFFSET               \
   + sizeof (jitter_int) * UNINSPIRED_MAX_MEMORY_RESIDUAL_ARITY)
#define UNINSPIRED_FIRST_SLOW_REGISTER_UNBIASED_OFFSET          \
  JITTER_NEXT_MULTIPLE_OF_POSITIVE                            \
     (UNINSPIRED_FIRST_TRANSFER_REGISTER_UNBIASED_OFFSET        \
      + sizeof (jitter_int) * UNINSPIRED_TRANSFER_REGISTER_NO,  \
      sizeof (union uninspired_any_register))

/* Expand to the offset of the special-purpose data struct from the Array
   biased beginning. */
#define UNINSPIRED_SPECIAL_PURPOSE_STATE_DATA_OFFSET       \
  (UNINSPIRED_SPECIAL_PURPOSE_STATE_DATA_UNBIASED_OFFSET   \
   - JITTER_ARRAY_BIAS)

/* Given an expression evaluating to the Array unbiased beginning, expand to
   an expression evaluating to a pointer to its special-purpose data.
   This is convenient for accessing special-purpose data from outside the
   state -- for example, to set the pending notification flag for another
   thread.
   There are two versions of this feature:
     UNINSPIRED_ARRAY_TO_SPECIAL_PURPOSE_STATE_DATA
   is meant to be used to access state data for some other thread, or in
   general out of VM code.
     UNINSPIRED_OWN_SPECIAL_PURPOSE_STATE_DATA
   is for VM code accessing its own special-purpose data. */
#define UNINSPIRED_ARRAY_TO_SPECIAL_PURPOSE_STATE_DATA_PRIVATE(qualifier,      \
                                                             array_address)  \
  ((qualifier struct jitter_special_purpose_state_data *)                    \
   (((char *) (array_address))                                               \
    + UNINSPIRED_SPECIAL_PURPOSE_STATE_DATA_UNBIASED_OFFSET))
#define UNINSPIRED_ARRAY_TO_SPECIAL_PURPOSE_STATE_DATA(array_address)       \
  UNINSPIRED_ARRAY_TO_SPECIAL_PURPOSE_STATE_DATA_PRIVATE (volatile,         \
                                                        (array_address))
#define UNINSPIRED_OWN_SPECIAL_PURPOSE_STATE_DATA          \
  UNINSPIRED_ARRAY_TO_SPECIAL_PURPOSE_STATE_DATA_PRIVATE   \
     (UNINSPIRED_ARRAY_VOLATILE_QUALIFIER,                 \
      ((char *) jitter_array_base) - JITTER_ARRAY_BIAS)

/* Given a state pointer, expand to an expression evaluating to a pointer to
   the state's special-purpose data.  This is meant for threads accessing
   other threads' special-purpose data, typically to set notifications. */
#define UNINSPIRED_STATE_TO_SPECIAL_PURPOSE_STATE_DATA(state_p)  \
  (UNINSPIRED_ARRAY_TO_SPECIAL_PURPOSE_STATE_DATA                \
     ((state_p)->uninspired_state_backing.jitter_array))

/* Given a state pointer, expand to an expression evaluating to the
   pending_notification field for the state as an l-value.  This is meant for
   threads sending notifications to other threads. */
#define UNINSPIRED_STATE_TO_PENDING_NOTIFICATIONS(state_p)   \
  (UNINSPIRED_STATE_TO_SPECIAL_PURPOSE_STATE_DATA (state_p)  \
     ->pending_notifications)

/* Given a state pointer and a signal, expand to an l-value evaluating to a the
   pending field of the struct jitter_signal_notification element for the given
   signal in the pointed state.  This is meant for threads sending signal
   notifications to other threads and for C handler function. */
#define UNINSPIRED_STATE_AND_SIGNAL_TO_PENDING_SIGNAL_NOTIFICATION(state_p,    \
                                                                 signal_id)  \
  (((UNINSPIRED_STATE_TO_SPECIAL_PURPOSE_STATE_DATA (state_p)                   \
       ->pending_signal_notifications)                                        \
    + (signal_id))->pending)


/* Expand to the offset of the i-th register of class c in bytes from the Array
   beginning.
   The c argument must be a literal C (one-character) identifier.
   The i argument should always be a compile-time constant for performance, and
   it is in generated code.
   The i-th c-class register must be slow, otherwise the offset will be
   incorrect -- in fact fast registers are, hopefully, not in memory at all.

   Slow registers come in the Array ordered first by index, then by class.  For
   example if there are three classes "r" with 4 fast registers, "f" with 7 fast
   registers and "q" with 2 fast registers, slow registers can be accessed in
   this order:
     r4, f7, q2, r5, r8, q3, r6, r9, q4, and so on.
   Each contiguous group of slow registers spanning every class and starting
   from the first class (here for example <r5, r6, q3>) is called a "rank".
   This organization is convenient since changing the number of slow registers
   doesn't invalidate any offset computed in the past: the Array can simply be
   resized and its base pointer updated, without changing the code accessing it.

   This relies on macro such as UNINSPIRED_REGISTER_CLASS_NO and
   UNINSPIRED_REGISTER_?_FAST_REGISTER_NO and , defined below in machine-generated
   code. */
#define UNINSPIRED_SLOW_REGISTER_UNBIASED_OFFSET(c, i)                     \
  (UNINSPIRED_FIRST_SLOW_REGISTER_UNBIASED_OFFSET                          \
   + (sizeof (union uninspired_any_register)                               \
      * (UNINSPIRED_REGISTER_CLASS_NO                                      \
         * ((i) - JITTER_CONCATENATE_THREE(UNINSPIRED_REGISTER_, c,        \
                                           _FAST_REGISTER_NO))           \
         + JITTER_CONCATENATE_THREE(UNINSPIRED_REGISTER_, c, _CLASS_ID))))

/* Expand to the offset of the i-th register of class c in bytes from the base,
   keeping the bias into account. */
#define UNINSPIRED_SLOW_REGISTER_OFFSET(c, i)                              \
  (UNINSPIRED_SLOW_REGISTER_UNBIASED_OFFSET(c, i) - JITTER_ARRAY_BIAS)

/* Expand to the Array size in bytes, assuming the given number of slow
   registers per class.  This is an allocation size, ignoring the bias. */
#define UNINSPIRED_ARRAY_SIZE(slow_register_per_class_no)                  \
  (UNINSPIRED_FIRST_SLOW_REGISTER_UNBIASED_OFFSET                          \
   + (sizeof (union uninspired_any_register)                               \
      * UNINSPIRED_REGISTER_CLASS_NO                                       \
      * (slow_register_per_class_no)))




/* Residual access.
 * ************************************************************************** */

/* How many residuals we can have at most in memory, which is to say,
   without counting residuals kept in reserved registers.

   Implementation note: it would be wrong here to use a CPP conditional based on
   the value of UNINSPIRED_MAX_RESIDUAL_ARITY , as I was doing in a preliminary
   version.  That lead to a tricky bug, since UNINSPIRED_MAX_RESIDUAL_ARITY ,
   which is defined below but is not yet available here, simply counted as 0
   for the purposes of evaluating the CPP condititional. */
#ifdef JITTER_DISPATCH_NO_THREADING
  /* We are using no-threading dispatch.  If there are no more residuals
     than reserved residual registers then we never need to keep any in
     memory.  Otherwise we need to keep as many residuals in memory as the
     total number of residuals minus how many registers are reserved for
     them. */
# define UNINSPIRED_MAX_MEMORY_RESIDUAL_ARITY                          \
    ((UNINSPIRED_MAX_RESIDUAL_ARITY <= JITTER_RESIDUAL_REGISTER_NO)    \
     ? 0                                                             \
     : (UNINSPIRED_MAX_RESIDUAL_ARITY - JITTER_RESIDUAL_REGISTER_NO))
#else // Not no-threading.
  /* No registers are reserved for residuals in this dispatching mode; even if
     in fact all residuals are memory residuals they don't count here, since
     residuals are not held in The Array in this dispatching mode. */
# define UNINSPIRED_MAX_MEMORY_RESIDUAL_ARITY  \
  0
#endif // #ifdef JITTER_DISPATCH_NO_THREADING

#ifdef JITTER_DISPATCH_NO_THREADING
/* Expand to the offset from the base, in bytes, of the i-th residual.  The
   given index must be greater than or equal to JITTER_RESIDUAL_REGISTER_NO;
   residuals with indices lower than that number are not stored in The Array
   at all.
   This is not useful with any of the other dispatching modes, where residuals
   directly follow each VM instruction opcode or thread.  For good performance i
   should always be a compile-time constant, as it is in machine-generated
   code.
   Residuals always have the size of a jitter word, even if some register class
   may be wider. */
/* FIXME: if later I use a different policy than simply checking
   JITTER_RESIDUAL_REGISTER_NO to decide how many residuals to keep in
   registers, then I have to change this or meet very nasty bugs. */
# define UNINSPIRED_RESIDUAL_UNBIASED_OFFSET(i)                      \
    (UNINSPIRED_FIRST_MEMORY_RESIDUAL_UNBIASED_OFFSET                \
     + (sizeof (jitter_int) * (i - JITTER_RESIDUAL_REGISTER_NO)))
# define UNINSPIRED_RESIDUAL_OFFSET(i)  \
    (UNINSPIRED_RESIDUAL_UNBIASED_OFFSET(i) - JITTER_ARRAY_BIAS)
#endif // #ifdef JITTER_DISPATCH_NO_THREADING



/* Mutable routine text frontend.
 * ************************************************************************** */

/* Parse VM code from the given file or string into the pointed VM routine,
   which is allowed but not required to be empty.
   These are simple wrappers around functions implemented in the Bison file. */
void
uninspired_parse_mutable_routine_from_file_star (FILE *input_file,
                                               struct jitter_mutable_routine *p)
  __attribute__ ((nonnull (1, 2)));
void
uninspired_parse_mutable_routine_from_file (const char *input_file_name,
                                          struct jitter_mutable_routine *p)
  __attribute__ ((nonnull (1, 2)));
void
uninspired_parse_mutable_routine_from_string (const char *string,
                                            struct jitter_mutable_routine *p)
  __attribute__ ((nonnull (1, 2)));




/* Unified routine text frontend.
 * ************************************************************************** */

/* The C wrappers for the ordinary API can be reused for the unified API, since
   it internally works with mutable routines. */
#define uninspired_parse_routine_from_file_star  \
  uninspired_parse_mutable_routine_from_file_star
#define uninspired_parse_routine_from_file  \
  uninspired_parse_mutable_routine_from_file
#define uninspired_parse_routine_from_string  \
  uninspired_parse_mutable_routine_from_string




/* Machine-generated data structures.
 * ************************************************************************** */

/* Declare a few machine-generated data structures, which together define a VM. */

/* Threads or pointers to native code blocks of course don't exist with
   switch-dispatching. */
#ifndef JITTER_DISPATCH_SWITCH
/* Every possible thread, indexed by enum jitter_specialized_instruction_opcode .
   This is used at specialization time, and the user shouldn't need to touch
   it. */
extern const jitter_thread *
uninspired_threads;

/* VM instruction end label.  These are not all reachable at run time, but
   having them in a global array might prevent older GCCs from being too clever
   in reordering blocks. */
extern const jitter_thread *
uninspired_thread_ends;

/* The size, in chars, of each thread's native code.  The elements are in the
   same order of uninspired_threads.  Sizes could conceptually be of type size_t ,
   but in order to be defensive I'm storing pointer differences as signed
   values, so that we may catch compilation problems: if any VM instruction end
   *precedes* its VM instruction beginning, then the compiler has reordered
   labels, which would have disastrous effects with replicated code. */
extern const long *
uninspired_thread_sizes;
#endif // #ifndef JITTER_DISPATCH_SWITCH

/* This is defined in the machine-generated vm/meta-instructions.c . */
extern struct jitter_hash_table
uninspired_meta_instruction_hash;

/* An array specifying every existing meta-instruction, defined in the order of
   enum uninspired_meta_instruction_id .  This is defined in vm/meta-instructions.c ,
   which is machine-generated. */
extern const struct jitter_meta_instruction
uninspired_meta_instructions [];

/* An array whose indices are specialised instruction opcodes, and
   whose elements are the corresponding unspecialised instructions
   opcodes -- or -1 when there is no mapping mapping having */
extern const int
uninspired_specialized_instruction_to_unspecialized_instruction [];

/* How many residual parameters each specialized instruction has.  The
   actual array definition is machine-generated. */
extern const size_t
uninspired_specialized_instruction_residual_arities [];

/* An array of bitmasks, one per specialized instruction.  Each bitmask holds
   one bit per residual argument, counting from the least significant (the first
   residual arg maps to element & (1 << 0), the second to element & (1 << 1),
   and so on).
   Each bit is 1 if and only if the corresponding residual argument is a label
   or a fast label.
   Only residual arguments are counted: for example a specialized instruction
   foo_n1_lR_r2 would have a mask with the *first* bit set. */
extern const unsigned long // FIXME: possibly use a shorter type when possible
uninspired_specialized_instruction_label_bitmasks [];

/* Like uninspired_specialized_instruction_label_bitmasks , but for fast labels
   only.
   The actual definition is conditionalized so as to appear only when
   needed according to the dispatching model. */
extern const unsigned long // FIXME: possibly use a shorter type when possible
uninspired_specialized_instruction_fast_label_bitmasks [];

/* An array of booleans in which each element is true iff the specialized
   instruction whose opcode is the index is relocatable. */
extern const bool
uninspired_specialized_instruction_relocatables [];

/* An array of booleans in which each element is true iff the specialized
   instruction whose opcode is the index is a caller. */
extern const bool
uninspired_specialized_instruction_callers [];

/* An array of booleans in which each element is true iff the specialized
   instruction whose opcode is the index is a callee. */
extern const bool
uninspired_specialized_instruction_callees [];

/* This big array of strings contains the name of each specialized instruction,
   in the order of enum uninspired_specialized_instruction_opcode . */
extern const char* const
uninspired_specialized_instruction_names [];


/* A pointer to a struct containing const pointers to the structures above, plus
   sizes; there will be only one instance of this per VM, machine-generated.
   Each program data structure contains a pointer to that instance, so that
   VM-independent functions, given a program, will have everything needed to
   work.  The one instance of struct jitter_vm for the uninspired VM. */
extern struct jitter_vm * const
uninspired_vm;

/* A pointer to a struct containing VM-specific parameters set in part when
   calling jitterc and in part when compiling the generated C code, such as the
   dispatching model and the number of fast registers.  The data is fully
   initialized only after a call to uninspired_initialize . */
extern const
struct jitter_vm_configuration * const
uninspired_vm_configuration;




/* Compatibility macros.
 * ************************************************************************** */

/* It is convenient, for future extensibility, to expose an interface in which
   some VM-independent functions and data structures actually look as if they
   were specific to the user VM. */

/* What the user refers to as struct uninspired_mutable_routine is actually a
   struct jitter_mutable_routine , whose definition is VM-independent. */
#define uninspired_mutable_routine jitter_mutable_routine

/* Same for executable routines. */
#define uninspired_executable_routine jitter_executable_routine

/* Same for unified routines. */
#define uninspired_routine jitter_routine

/* Destroy a non-executable routine (routine initialization is actually
   VM-specific). */
#define uninspired_destroy_mutable_routine jitter_destroy_mutable_routine

/* Destroy a unified routine. */
#define uninspired_destroy_routine jitter_destroy_routine

/* Pin a unified routine. */
#define uninspired_pin_routine jitter_pin_routine

/* Unpin a unified routine. */
#define uninspired_unpin_routine jitter_unpin_routine

/* Print VM configuration. */
#define uninspired_print_vm_configuration jitter_print_vm_configuration

/* Generic routine construction API. */
#define uninspired_label \
  jitter_label
#define uninspired_fresh_label \
  jitter_fresh_label

/* Mutable routine option API. */
#define uninspired_set_mutable_routine_option_slow_literals_only \
  jitter_set_mutable_routine_option_slow_literals_only
#define uninspired_set_mutable_routine_option_slow_registers_only \
  jitter_set_mutable_routine_option_slow_registers_only
#define uninspired_set_mutable_routine_option_slow_literals_and_registers_only \
  jitter_set_mutable_routine_option_slow_literals_and_registers_only
#define uninspired_set_mutable_routine_option_add_final_exitvm \
  jitter_set_mutable_routine_option_add_final_exitvm
#define uninspired_set_mutable_routine_option_optimization_rewriting \
  jitter_set_mutable_routine_option_optimization_rewriting

/* Printing and disassembling: ordinary API. */
#define uninspired_mutable_routine_print \
  jitter_mutable_routine_print
#define uninspired_executable_routine_disassemble \
  jitter_executable_routine_disassemble

/* Mutable routine construction API. */
#define uninspired_mutable_routine_append_instruction_name \
  jitter_mutable_routine_append_instruction_name
#define uninspired_mutable_routine_append_meta_instruction \
  jitter_mutable_routine_append_meta_instruction
#define uninspired_mutable_routine_append_label \
  jitter_mutable_routine_append_label
#define uninspired_mutable_routine_append_symbolic_label \
  jitter_mutable_routine_append_symbolic_label
#define uninspired_mutable_routine_append_register_parameter \
  jitter_mutable_routine_append_register_parameter
#define uninspired_mutable_routine_append_literal_parameter \
  jitter_mutable_routine_append_literal_parameter
#define uninspired_mutable_routine_append_signed_literal_parameter \
  jitter_mutable_routine_append_signed_literal_parameter
#define uninspired_mutable_routine_append_unsigned_literal_parameter \
  jitter_mutable_routine_append_unsigned_literal_parameter
#define uninspired_mutable_routine_append_pointer_literal_parameter \
  jitter_mutable_routine_append_pointer_literal_parameter
#define uninspired_mutable_routine_append_label_parameter \
  jitter_mutable_routine_append_label_parameter
#define uninspired_mutable_routine_append_symbolic_label_parameter \
  jitter_mutable_routine_append_symbolic_label_parameter

/* Mutable routine destruction. */
#define uninspired_destroy_executable_routine \
  jitter_destroy_executable_routine

/* Making executable routines from mutable routines. */
#define uninspired_make_executable_routine \
  jitter_make_executable_routine

/* Unified routine option API. */
#define uninspired_set_routine_option_slow_literals_only \
  jitter_set_mutable_routine_option_slow_literals_only
#define uninspired_set_routine_option_slow_registers_only \
  jitter_set_mutable_routine_option_slow_registers_only
#define uninspired_set_routine_option_slow_literals_and_registers_only \
  jitter_set_mutable_routine_option_slow_literals_and_registers_only
#define uninspired_set_routine_option_add_final_exitvm \
  jitter_set_mutable_routine_option_add_final_exitvm
#define uninspired_set_routine_option_optimization_rewriting \
  jitter_set_mutable_routine_option_optimization_rewriting

/* Printing and disassembling: unified API.  These do not follow the pattern of
   the rest: wrapped identifiers here are the names of C functions specific to
   the unified API */
#define uninspired_routine_print \
  jitter_routine_print
#define uninspired_routine_disassemble \
  jitter_routine_disassemble

/* Unified routine construction API. */
#define uninspired_routine_append_instruction_name \
  jitter_mutable_routine_append_instruction_name
#define uninspired_routine_append_meta_instruction \
  jitter_mutable_routine_append_meta_instruction
#define uninspired_routine_append_label \
  jitter_mutable_routine_append_label
#define uninspired_routine_append_symbolic_label \
  jitter_mutable_routine_append_symbolic_label
#define uninspired_routine_append_register_parameter \
  jitter_mutable_routine_append_register_parameter
#define uninspired_routine_append_literal_parameter \
  jitter_mutable_routine_append_literal_parameter
#define uninspired_routine_append_signed_literal_parameter \
  jitter_mutable_routine_append_signed_literal_parameter
#define uninspired_routine_append_unsigned_literal_parameter \
  jitter_mutable_routine_append_unsigned_literal_parameter
#define uninspired_routine_append_pointer_literal_parameter \
  jitter_mutable_routine_append_pointer_literal_parameter
#define uninspired_routine_append_label_parameter \
  jitter_mutable_routine_append_label_parameter
#define uninspired_routine_append_symbolic_label_parameter \
  jitter_mutable_routine_append_symbolic_label_parameter

/* Mutable routine destruction. */
#define uninspired_destroy_routine                                           \
  /* This does not follow the pattern of the rest: the wrapped identifier  \
     here is the name of a C function specific to the unified API. */      \
  jitter_destroy_routine

/* The unified API has no facility to explicitly make executable routines: their
   very existence is hidden.  For this reason some of the macros above, such
   uninspired_make_executable_routine, have no unified counterpart here. */

/* Profiling.  Apart from uninspired_state_profile, which returns a pointer to
   the profile within a pointed state structure, everything else here has the
   same API as the functionality in jitter/jitter-profile.h , without the VM
   pointer.
   Notice that this API does nothing useful onless one of the CPP macros
   JITTER_PROFILE_COUNT or JITTER_PROFILE_SAMPLE is defined. */
#define uninspired_profile_runtime  \
  jitter_profile_runtime /* the struct name */
#define uninspired_profile  \
  jitter_profile /* the struct name */
// FIXME: no: distinguish between struct jitter_profile_runtime and its user-friendly variant
struct jitter_profile_runtime *
uninspired_state_profile_runtime (struct uninspired_state *s)
  __attribute__ ((returns_nonnull, nonnull (1)));
struct uninspired_profile_runtime*
uninspired_profile_runtime_make (void)
  __attribute__ ((returns_nonnull));
#define uninspired_profile_destroy jitter_profile_destroy
void
uninspired_profile_runtime_clear (struct uninspired_profile_runtime *p)
  __attribute__ ((nonnull (1)));
void
uninspired_profile_runtime_merge_from (struct uninspired_profile_runtime *to,
                                     const struct uninspired_profile_runtime *from)
  __attribute__ ((nonnull (1, 2)));
void
uninspired_profile_runtime_merge_from_state (struct uninspired_profile_runtime *to,
                                   const struct uninspired_state *from_state)
  __attribute__ ((nonnull (1, 2)));
struct uninspired_profile *
uninspired_profile_unspecialized_from_runtime
   (const struct uninspired_profile_runtime *p)
  __attribute__ ((returns_nonnull, nonnull (1)));
struct uninspired_profile *
uninspired_profile_specialized_from_runtime (const struct uninspired_profile_runtime
                                           *p)
  __attribute__ ((returns_nonnull, nonnull (1)));
void
uninspired_profile_runtime_print_unspecialized
   (jitter_print_context ct,
    const struct uninspired_profile_runtime *p)
  __attribute__ ((nonnull (1, 2)));
void
uninspired_profile_runtime_print_specialized (jitter_print_context ct,
                                            const struct uninspired_profile_runtime
                                            *p)
  __attribute__ ((nonnull (1, 2)));




/* Register class types.
 * ************************************************************************** */

/* Return a pointer to a statically allocated register class descriptor, given
   the register class character, or NULL if the character does not represent a
   valid register class.

   A constant array indexed by a character would have been more efficient, but
   relying on character ordering is not portable, at least in theory.  A
   non-constant array could be initialized in a portable way, but that would
   probably not be worth the trouble. */
const struct jitter_register_class *
uninspired_register_class_character_to_register_class (char c)
  __attribute__ ((pure));


/* A constant array of constant pointers to every existing register class
   descriptor, ordered by class id; each pointer within the array refers the
   only existing class descriptor for its class.  The number of elements is
   UNINSPIRED_REGISTER_CLASS_NO , but that is not declared because the definition
   of UNINSPIRED_REGISTER_CLASS_NO comes later in generated code.

   This is useful when the user code enumerates every existing register class,
   particularly for debugging. */
extern const struct jitter_register_class * const
uninspired_regiter_classes [];




/* Array re-allocation.
 * ************************************************************************** */

/* Make the Array in the pointed state large enough to accommodate the given
   number of slow reigsters per class, adjusting the Array pointer as needed
   and recording information about the new size in the state; change nothing
   if the array is already large enough.  Return the new base.
   For example passing 3 as the value of slow_register_no would make
   place for three slow registers per register class: if the current VM had two
   classes 'r' and 'f' than the function would ensure that the Array can hold
   three 'r' and three 'f' slow registers, independently from the number
   of fast 'r' or 'f' registers.
   Any new elements allocated in the Array are left uninitialized, but its old
   content remains valid. */
char *
uninspired_make_place_for_slow_registers (struct uninspired_state *s,
                                        jitter_int slow_register_no_per_class)
  __attribute__ ((noinline));




/* **************************************************************************
 * Evrything following this point is for internal use only.
 * ************************************************************************** */




/* Defect tables.
 * ************************************************************************** */

/* It is harmless to declare these unconditionally, even if they only used when
   patch-ins are available.  See jitter/jitter-defect.h .*/

/* The worst-case defect table.  This is a global constant array, having one
   element per specialized instruction. */
extern const jitter_uint
uninspired_worst_case_defect_table [];

/* The actual defect table, to be filled at initialization time. */
extern jitter_uint
uninspired_defect_table [];




/* Instruction rewriter.
 * ************************************************************************** */

/* Try to apply each rewrite rule in order and run the first one that matches,
   if any, on the pointed program.  When a rule fires the following ones are not
   checked but if a rule, after removing the last few instructions, adds another
   one, the addition will trigger another rewrite in its turn, and so on until
   no more rewriting is possible.  The rewriting process is inherently
   recursive.

   The implementation of this function is machine-generated, but the user can
   add her own code in the rewriter-c block, which ends up near the beginning of
   this function body, right after JITTTER_REWRITE_FUNCTION_PROLOG_ .  The
   formal argument seen from the body is named jitter_mutable_routine_p .

   Rationale: the argument is named differently in the body in order to keep
   the namespace conventions and, more importantly, to encourage the user to
   read this comment.

   The user must *not* append labels to the VM routines during rewriting: that
   would break it.  The user is responsible for destroying any instruction she
   removes, including their arguments.  The user can assume that
   jitter_rewritable_instruction_no is strictly greater than zero. */
void
uninspired_rewrite (struct jitter_mutable_routine *jitter_mutable_routine_p);




/* Program points at run time in executable routines.
 * ************************************************************************** */

/* Provide a nice name for a program point type which looks VM-dependent. */
typedef jitter_program_point
uninspired_program_point;

/* Again, provide a VM-dependent alias for an actually VM-independent macro. */
#define UNINSPIRED_EXECUTABLE_ROUTINE_BEGINNING(_jitter_executable_routine_ptr)  \
  JITTER_EXECUTABLE_ROUTINE_BEGINNING(_jitter_executable_routine_ptr)




/* Program points at run time in routines: unified routine API.
 * ************************************************************************** */

/* Like UNINSPIRED_EXECUTABLE_ROUTINE_BEGINNING for the unified routine API. */
#define UNINSPIRED_ROUTINE_BEGINNING(_jitter_routine)                \
  JITTER_EXECUTABLE_ROUTINE_BEGINNING                              \
     (jitter_routine_make_executable_if_needed (_jitter_routine))



/* Executing code from an executable routine.
 * ************************************************************************** */

/* Make sure that the pointed state has enough slow registers to run the pointed
   executable routine; if that is not the case, allocate more slow registers. */
void
uninspired_ensure_enough_slow_registers_for_executable_routine
   (const struct jitter_executable_routine *er, struct uninspired_state *s)
  __attribute__ ((nonnull (1, 2)));

/* Run VM code starting from the given program point (which must belong to some
   executable routine), in the pointed VM state.

   Since no executable routine is given this cannot automatically guarantee that
   the slow registers in the pointed state are in sufficient number; it is the
   user's responsibility to check, if needed.

   This function is also usable with the unified routine API. */
void
uninspired_branch_to_program_point (uninspired_program_point p,
                                  struct uninspired_state *s)
  __attribute__ ((nonnull (1, 2)));

/* Run VM code starting from the beginning of the pointed executable routine,
   in the pointed VM state.  This does ensure that the slow registers in
   the pointed state are in sufficient number, by calling
   uninspired_ensure_enough_slow_registers_for .
   This function is slightly less efficient than
   uninspired_branch_to_program_point , and uninspired_branch_to_program_point
   should be preferred in contexts where C code repeatedly calls VM code. */
void
uninspired_execute_executable_routine (const struct jitter_executable_routine *er,
                                     struct uninspired_state *s)
  __attribute__ ((nonnull (1, 2)));




/* Executing code: unified routine API.
 * ************************************************************************** */

/* Like uninspired_ensure_enough_slow_registers_for_executable_routine , with the
   unified API. */
void
uninspired_ensure_enough_slow_registers_for_routine
   (jitter_routine r, struct uninspired_state *s)
  __attribute__ ((nonnull (1, 2)));

/* uninspired_branch_to_program_point , declared above, is also usable with the
   unified routine API. */

/* Like uninspired_execute_executable_routine, for a unified routine. */
void
uninspired_execute_routine (jitter_routine r,
                          struct uninspired_state *s)
  __attribute__ ((nonnull (1, 2)));




/* Low-level debugging features relying on assembly: data locations.
 * ************************************************************************** */

/* Dump human-readable information about data locations to the given print
   context.
   This is a trivial VM-dependent wrapper around jitter_dump_data_locations,
   which does not require a struct jitter_vm pointer as input. */
void
uninspired_dump_data_locations (jitter_print_context output)
  __attribute__ ((nonnull (1)));




/* Sample profiling: internal API.
 * ************************************************************************** */

/* The functions in this sections are used internally by vm2.c, only when
   sample-profiling is enabled.  In fact these functions are not defined at all
   otherwise. */

/* Initialise global sampling-related structures. */
// FIXME: no: distinguish struct jitter_profile_runtime and struct jitter_profile
void
uninspired_profile_sample_initialize (void);

/* Begin sampling. */
void
uninspired_profile_sample_start (struct uninspired_state *state)
  __attribute__ ((nonnull (1)));

/* Stop sampling. */
void
uninspired_profile_sample_stop (void);




/* Machine-generated code.
 * ************************************************************************** */

/* What follows could be conceptually split into several generated header files,
   but having too many files would be inconvenient for the user to compile and
   link.  For this reason we generate a single header. */

/* User-specified code, early header part: beginning. */

    struct exception_handler
    {
      jitter_uint exception;
      uninspired_program_point code;
      jitter_stack_height stack_height;
    };
  
  #include <string.h> // FIXME: remove after my tests with memcpy

  union uninspired_currently_unique_kind_register_t
  {
    /* Each of the following fields has the same name and a compatible type (in
       this case exactly the same) as the corresponding fields in union
       jitter_word .  This is useful for meta-instructions whose arguments may be
       either immediates or registers. */
    jitter_int fixnum;
    jitter_uint ufixnum;
    union jitter_word * restrict pointer;
  };

# define VECTOR_ELEMENT_TYPE  float//double//char//int//double //float
# define VECTOR_ELEMENT_NO    2//1024//8//1024//512 //8//512//1024

# define USE_ATTRIBUTE_VECTOR_SIZE

#ifdef USE_ATTRIBUTE_VECTOR_SIZE
  typedef VECTOR_ELEMENT_TYPE
  vector
    __attribute__ ((vector_size (VECTOR_ELEMENT_NO
                                 * sizeof(VECTOR_ELEMENT_TYPE)),
                    aligned));
#else
  typedef VECTOR_ELEMENT_TYPE
  vector [VECTOR_ELEMENT_NO]
    __attribute__ ((aligned));
#endif // #ifdef USE_ATTRIBUTE_VECTOR_SIZE
  
/* User-specified code, early header part: end */

/* Configuration data for struct jitter_vm_configuration. */
#define UNINSPIRED_VM_NAME JITTER_STRINGIFY(Uninspired)
#define UNINSPIRED_LOWER_CASE_PREFIX "uninspired"
#define UNINSPIRED_UPPER_CASE_PREFIX "UNINSPIRED"
#define UNINSPIRED_DISPATCH_HUMAN_READABLE \
  JITTER_DISPATCH_NAME_STRING
#define UNINSPIRED_MAX_FAST_REGISTER_NO_PER_CLASS 1
#define UNINSPIRED_MAX_NONRESIDUAL_LITERAL_NO -1


/* For each register class define the register type, a unique index, and the
   number of fast registers.  Indices are useful for computing slow register
   offsets.  For each register class declare a global register class
   descriptor, convenient to use when generating unspecialized instructions
   from the C API.*/
typedef
union uninspired_currently_unique_kind_register_t uninspired_register_r;
#define UNINSPIRED_REGISTER_r_CLASS_ID 0
#define UNINSPIRED_REGISTER_r_FAST_REGISTER_NO 1
extern const struct jitter_register_class
uninspired_register_class_r;
typedef
VECTOR_ELEMENT_TYPE uninspired_register_f;
#define UNINSPIRED_REGISTER_f_CLASS_ID 1
#define UNINSPIRED_REGISTER_f_FAST_REGISTER_NO 0
extern const struct jitter_register_class
uninspired_register_class_f;
typedef
vector uninspired_register_v;
#define UNINSPIRED_REGISTER_v_CLASS_ID 2
#define UNINSPIRED_REGISTER_v_FAST_REGISTER_NO 0
extern const struct jitter_register_class
uninspired_register_class_v;
typedef
char uninspired_register_c;
#define UNINSPIRED_REGISTER_c_CLASS_ID 3
#define UNINSPIRED_REGISTER_c_FAST_REGISTER_NO 0
extern const struct jitter_register_class
uninspired_register_class_c;

/* How many register classes we have. */
#define UNINSPIRED_REGISTER_CLASS_NO  4

/* A union large enough to hold a register of any class, or a machine word. */
union uninspired_any_register
{
  /* In any case the union must be at least as large as a machine word. */
  jitter_int jitter_unused_field;

  uninspired_register_r r /* A r-class register */;
  uninspired_register_f f /* A f-class register */;
  uninspired_register_v v /* A v-class register */;
  uninspired_register_c c /* A c-class register */;
};

/* An enumeration of all uninspired register classes. */
enum uninspired_register_class_id
  {
    uninspired_register_class_id_r = UNINSPIRED_REGISTER_r_CLASS_ID,
    uninspired_register_class_id_f = UNINSPIRED_REGISTER_f_CLASS_ID,
    uninspired_register_class_id_v = UNINSPIRED_REGISTER_v_CLASS_ID,
    uninspired_register_class_id_c = UNINSPIRED_REGISTER_c_CLASS_ID,

    /* The number of register class ids, not valid as a class id itself. */
    uninspired_register_class_id_no = UNINSPIRED_REGISTER_CLASS_NO
  };

/* A macro expanding to a statement initialising a rank of slow
   registers.  The argument has type union uninspired_any_register *
   and points to the first register in a rank. */
#define UNINSPIRED_INITIALIZE_SLOW_REGISTER_RANK(rank) \
  do \
    { \
      union uninspired_any_register *_jitter_rank __attribute__ ((unused)) \
        = (rank); \
      _jitter_rank [0].r = (union uninspired_currently_unique_kind_register_t) ((union uninspired_currently_unique_kind_register_t) {.ufixnum = 0x11223344}); \
      /* f-class registers need no initialisation. */ \
      /* v-class registers need no initialisation. */ \
      /* c-class registers need no initialisation. */ \
    } \
  while (false)


#ifndef UNINSPIRED_STATE_H_
#define UNINSPIRED_STATE_H_

//#include <jitter/jitter.h>

/* Early C code from the user for the state definition. */
/* End of the early C code from the user for the state definition. */

/* The VM state backing. */
struct uninspired_state_backing
{
  /* The Array.  This initial pointer is kept in the backing, since it is
     not normally needed at run time.  By subtracting JITTER_ARRAY_BIAS from
     it (as a pointer to char) we get the base pointer. */
  char *jitter_array;

  /* How many slow registers per class the Array can hold, without being
     reallocated.  This number is always the same for evey class. */
  jitter_int jitter_slow_register_no_per_class;

  /* Stack backing data structures. */
  struct jitter_stack_backing jitter_stack_stack_backing;
  struct jitter_stack_backing jitter_stack_handlers_backing;

  /* State backing fields added in C by the user. */

  /* End of the state backing fields added in C by the user. */
};

/* The VM state runtime data structure, using memory from the VM state backing. */
struct uninspired_state_runtime
{
#if    defined(JITTER_DISPATCH_SWITCH)                   \
    || defined(JITTER_DISPATCH_DIRECT_THREADING)         \
    || defined(JITTER_DISPATCH_MINIMAL_THREADING)        \
    || (   defined(JITTER_DISPATCH_NO_THREADING)         \
        && ! defined(JITTER_MACHINE_SUPPORTS_PROCEDURE))
  /* A link register for branch-and-link operations.  This field must *not*
     be accessed from user code, as it may not exist on all dispatching
     models.  It is only used internally for JITTER_PROCEDURE_PROLOG. */
  const union jitter_word *_jitter_link;
#endif

  /* With recent GCC versions (as of Summer 2017) the *last* declared fields
     are the most likely to be allocated in registers; this is why VM registers
     are in reverse order here.  The first few fast registers will be the "fastest"
     ones, allocated in hardware registers; they may be followed by other fast
     fast allocated on the stack at known offsets, with intermediate performance; then
     come the slow registers.  In critical code the users should prefer a register with as
     small an index as possible for best performance. */
  uninspired_register_r jitter_fast_register_r_0;

  /* Stack runtime data structures. */
  JITTER_STACK_TOS_DECLARATION(union jitter_word, stack);
  JITTER_STACK_NTOS_DECLARATION(struct exception_handler, handlers);

  /* State runtime fields added in C by the user. */

  
  /* End of the state runtime fields added in C by the user. */
};

/* A struct holding both the backing and the runtime part of the VM state. */
struct uninspired_state
{
  /* Pointers to the previous and next VM state for this VM. */
  struct jitter_list_links links;

  /* Each state data structure contains its backing. */
  struct uninspired_state_backing uninspired_state_backing;

  /* Each state data structure contains its runtime data structures,
     to be allocated to registers as long as possible, and using
     memory from the backing. */
  struct uninspired_state_runtime uninspired_state_runtime;
};
#endif // #ifndef UNINSPIRED_STATE_H_
#ifndef UNINSPIRED_META_INSTRUCTIONS_H_
#define UNINSPIRED_META_INSTRUCTIONS_H_

enum uninspired_meta_instruction_id
  {
    uninspired_meta_instruction_id_add = 0,
    uninspired_meta_instruction_id_addo = 1,
    uninspired_meta_instruction_id_b = 2,
    uninspired_meta_instruction_id_band = 3,
    uninspired_meta_instruction_id_beq = 4,
    uninspired_meta_instruction_id_bge = 5,
    uninspired_meta_instruction_id_bgeu = 6,
    uninspired_meta_instruction_id_bgt = 7,
    uninspired_meta_instruction_id_bgtu = 8,
    uninspired_meta_instruction_id_ble = 9,
    uninspired_meta_instruction_id_bleu = 10,
    uninspired_meta_instruction_id_blt = 11,
    uninspired_meta_instruction_id_bltu = 12,
    uninspired_meta_instruction_id_bne = 13,
    uninspired_meta_instruction_id_bneg = 14,
    uninspired_meta_instruction_id_bnneg = 15,
    uninspired_meta_instruction_id_bnotand = 16,
    uninspired_meta_instruction_id_bnpos = 17,
    uninspired_meta_instruction_id_bnz = 18,
    uninspired_meta_instruction_id_bpos = 19,
    uninspired_meta_instruction_id_br = 20,
    uninspired_meta_instruction_id_bulge = 21,
    uninspired_meta_instruction_id_bulgeforth = 22,
    uninspired_meta_instruction_id_bz = 23,
    uninspired_meta_instruction_id_clear_mpending = 24,
    uninspired_meta_instruction_id_div = 25,
    uninspired_meta_instruction_id_divo = 26,
    uninspired_meta_instruction_id_drop_mhandler = 27,
    uninspired_meta_instruction_id_endvm = 28,
    uninspired_meta_instruction_id_exit = 29,
    uninspired_meta_instruction_id_exitvm = 30,
    uninspired_meta_instruction_id_fadd = 31,
    uninspired_meta_instruction_id_fdiv = 32,
    uninspired_meta_instruction_id_fincr = 33,
    uninspired_meta_instruction_id_fmul = 34,
    uninspired_meta_instruction_id_fprint = 35,
    uninspired_meta_instruction_id_fset = 36,
    uninspired_meta_instruction_id_fsub = 37,
    uninspired_meta_instruction_id_hcf = 38,
    uninspired_meta_instruction_id_install_msignal_mhandler = 39,
    uninspired_meta_instruction_id_loadwithbyteoffset = 40,
    uninspired_meta_instruction_id_loadwithwordoffset = 41,
    uninspired_meta_instruction_id_mallocwords = 42,
    uninspired_meta_instruction_id_mod = 43,
    uninspired_meta_instruction_id_modo = 44,
    uninspired_meta_instruction_id_mov = 45,
    uninspired_meta_instruction_id_mroll = 46,
    uninspired_meta_instruction_id_mrollforth = 47,
    uninspired_meta_instruction_id_mrot = 48,
    uninspired_meta_instruction_id_mul = 49,
    uninspired_meta_instruction_id_mulo = 50,
    uninspired_meta_instruction_id_nop = 51,
    uninspired_meta_instruction_id_print_mpending_msignals = 52,
    uninspired_meta_instruction_id_print_mtopmost = 53,
    uninspired_meta_instruction_id_printfixnum = 54,
    uninspired_meta_instruction_id_procedurecall = 55,
    uninspired_meta_instruction_id_procedurecallr = 56,
    uninspired_meta_instruction_id_procedureprolog = 57,
    uninspired_meta_instruction_id_procedurereturn = 58,
    uninspired_meta_instruction_id_push_mdepths = 59,
    uninspired_meta_instruction_id_push_mhandler = 60,
    uninspired_meta_instruction_id_push_mincreasing = 61,
    uninspired_meta_instruction_id_quake = 62,
    uninspired_meta_instruction_id_raise = 63,
    uninspired_meta_instruction_id_random = 64,
    uninspired_meta_instruction_id_reverse = 65,
    uninspired_meta_instruction_id_roll = 66,
    uninspired_meta_instruction_id_rollforth = 67,
    uninspired_meta_instruction_id_rot = 68,
    uninspired_meta_instruction_id_safe_mpoint = 69,
    uninspired_meta_instruction_id_set_mpending = 70,
    uninspired_meta_instruction_id_slide = 71,
    uninspired_meta_instruction_id_slideforth = 72,
    uninspired_meta_instruction_id_stackdrop = 73,
    uninspired_meta_instruction_id_stackdup = 74,
    uninspired_meta_instruction_id_stackif = 75,
    uninspired_meta_instruction_id_stacknip = 76,
    uninspired_meta_instruction_id_stacknondroppingif = 77,
    uninspired_meta_instruction_id_stacknot = 78,
    uninspired_meta_instruction_id_stackoneminus = 79,
    uninspired_meta_instruction_id_stackoneplus = 80,
    uninspired_meta_instruction_id_stackover = 81,
    uninspired_meta_instruction_id_stackpeek = 82,
    uninspired_meta_instruction_id_stackplus = 83,
    uninspired_meta_instruction_id_stackplusr = 84,
    uninspired_meta_instruction_id_stackpop = 85,
    uninspired_meta_instruction_id_stackprint = 86,
    uninspired_meta_instruction_id_stackpush = 87,
    uninspired_meta_instruction_id_stackpushunspecified = 88,
    uninspired_meta_instruction_id_stackset = 89,
    uninspired_meta_instruction_id_stackswap = 90,
    uninspired_meta_instruction_id_stackswaptop = 91,
    uninspired_meta_instruction_id_stacktimes = 92,
    uninspired_meta_instruction_id_storewithbyteoffset = 93,
    uninspired_meta_instruction_id_storewithwordoffset = 94,
    uninspired_meta_instruction_id_sub = 95,
    uninspired_meta_instruction_id_subo = 96,
    uninspired_meta_instruction_id_tuck = 97,
    uninspired_meta_instruction_id_unreachable = 98,
    uninspired_meta_instruction_id_whirl = 99,
    uninspired_meta_instruction_id_whirlforth = 100
  };

#define UNINSPIRED_META_INSTRUCTION_NO 101

/* The longest meta-instruction name length, not mangled, without
   counting the final '\0' character. */
#define UNINSPIRED_MAX_META_INSTRUCTION_NAME_LENGTH 22

#endif // #ifndef UNINSPIRED_META_INSTRUCTIONS_H_
#ifndef UNINSPIRED_SPECIALIZED_INSTRUCTIONS_H_
#define UNINSPIRED_SPECIALIZED_INSTRUCTIONS_H_

enum uninspired_specialized_instruction_opcode
  {
    uninspired_specialized_instruction_opcode__eINVALID = 0,
    uninspired_specialized_instruction_opcode__eBEGINBASICBLOCK = 1,
    uninspired_specialized_instruction_opcode__eEXITVM = 2,
    uninspired_specialized_instruction_opcode__eDATALOCATIONS = 3,
    uninspired_specialized_instruction_opcode__eNOP = 4,
    uninspired_specialized_instruction_opcode__eUNREACHABLE0 = 5,
    uninspired_specialized_instruction_opcode__eUNREACHABLE1 = 6,
    uninspired_specialized_instruction_opcode__eUNREACHABLE2 = 7,
    uninspired_specialized_instruction_opcode_add___rr0___rr0___rr0 = 8,
    uninspired_specialized_instruction_opcode_add___rr0___rr0___rrR = 9,
    uninspired_specialized_instruction_opcode_add___rr0___rrR___rr0 = 10,
    uninspired_specialized_instruction_opcode_add___rr0___rrR___rrR = 11,
    uninspired_specialized_instruction_opcode_add___rr0__n1___rr0 = 12,
    uninspired_specialized_instruction_opcode_add___rr0__n1___rrR = 13,
    uninspired_specialized_instruction_opcode_add___rr0__n_m1___rr0 = 14,
    uninspired_specialized_instruction_opcode_add___rr0__n_m1___rrR = 15,
    uninspired_specialized_instruction_opcode_add___rr0__nR___rr0 = 16,
    uninspired_specialized_instruction_opcode_add___rr0__nR___rrR = 17,
    uninspired_specialized_instruction_opcode_add___rrR___rr0___rr0 = 18,
    uninspired_specialized_instruction_opcode_add___rrR___rr0___rrR = 19,
    uninspired_specialized_instruction_opcode_add___rrR___rrR___rr0 = 20,
    uninspired_specialized_instruction_opcode_add___rrR___rrR___rrR = 21,
    uninspired_specialized_instruction_opcode_add___rrR__n1___rr0 = 22,
    uninspired_specialized_instruction_opcode_add___rrR__n1___rrR = 23,
    uninspired_specialized_instruction_opcode_add___rrR__n_m1___rr0 = 24,
    uninspired_specialized_instruction_opcode_add___rrR__n_m1___rrR = 25,
    uninspired_specialized_instruction_opcode_add___rrR__nR___rr0 = 26,
    uninspired_specialized_instruction_opcode_add___rrR__nR___rrR = 27,
    uninspired_specialized_instruction_opcode_add__n1___rr0___rr0 = 28,
    uninspired_specialized_instruction_opcode_add__n1___rr0___rrR = 29,
    uninspired_specialized_instruction_opcode_add__n1___rrR___rr0 = 30,
    uninspired_specialized_instruction_opcode_add__n1___rrR___rrR = 31,
    uninspired_specialized_instruction_opcode_add__n1__n1___rr0 = 32,
    uninspired_specialized_instruction_opcode_add__n1__n1___rrR = 33,
    uninspired_specialized_instruction_opcode_add__n1__n_m1___rr0 = 34,
    uninspired_specialized_instruction_opcode_add__n1__n_m1___rrR = 35,
    uninspired_specialized_instruction_opcode_add__n1__nR___rr0 = 36,
    uninspired_specialized_instruction_opcode_add__n1__nR___rrR = 37,
    uninspired_specialized_instruction_opcode_add__n_m1___rr0___rr0 = 38,
    uninspired_specialized_instruction_opcode_add__n_m1___rr0___rrR = 39,
    uninspired_specialized_instruction_opcode_add__n_m1___rrR___rr0 = 40,
    uninspired_specialized_instruction_opcode_add__n_m1___rrR___rrR = 41,
    uninspired_specialized_instruction_opcode_add__n_m1__n1___rr0 = 42,
    uninspired_specialized_instruction_opcode_add__n_m1__n1___rrR = 43,
    uninspired_specialized_instruction_opcode_add__n_m1__n_m1___rr0 = 44,
    uninspired_specialized_instruction_opcode_add__n_m1__n_m1___rrR = 45,
    uninspired_specialized_instruction_opcode_add__n_m1__nR___rr0 = 46,
    uninspired_specialized_instruction_opcode_add__n_m1__nR___rrR = 47,
    uninspired_specialized_instruction_opcode_add__nR___rr0___rr0 = 48,
    uninspired_specialized_instruction_opcode_add__nR___rr0___rrR = 49,
    uninspired_specialized_instruction_opcode_add__nR___rrR___rr0 = 50,
    uninspired_specialized_instruction_opcode_add__nR___rrR___rrR = 51,
    uninspired_specialized_instruction_opcode_add__nR__n1___rr0 = 52,
    uninspired_specialized_instruction_opcode_add__nR__n1___rrR = 53,
    uninspired_specialized_instruction_opcode_add__nR__n_m1___rr0 = 54,
    uninspired_specialized_instruction_opcode_add__nR__n_m1___rrR = 55,
    uninspired_specialized_instruction_opcode_add__nR__nR___rr0 = 56,
    uninspired_specialized_instruction_opcode_add__nR__nR___rrR = 57,
    uninspired_specialized_instruction_opcode_addo___rr0___rr0___rr0__fR = 58,
    uninspired_specialized_instruction_opcode_addo___rr0___rr0___rrR__fR = 59,
    uninspired_specialized_instruction_opcode_addo___rr0___rrR___rr0__fR = 60,
    uninspired_specialized_instruction_opcode_addo___rr0___rrR___rrR__fR = 61,
    uninspired_specialized_instruction_opcode_addo___rr0__n1___rr0__fR = 62,
    uninspired_specialized_instruction_opcode_addo___rr0__n1___rrR__fR = 63,
    uninspired_specialized_instruction_opcode_addo___rr0__n_m1___rr0__fR = 64,
    uninspired_specialized_instruction_opcode_addo___rr0__n_m1___rrR__fR = 65,
    uninspired_specialized_instruction_opcode_addo___rr0__nR___rr0__fR = 66,
    uninspired_specialized_instruction_opcode_addo___rr0__nR___rrR__fR = 67,
    uninspired_specialized_instruction_opcode_addo___rrR___rr0___rr0__fR = 68,
    uninspired_specialized_instruction_opcode_addo___rrR___rr0___rrR__fR = 69,
    uninspired_specialized_instruction_opcode_addo___rrR___rrR___rr0__fR = 70,
    uninspired_specialized_instruction_opcode_addo___rrR___rrR___rrR__fR = 71,
    uninspired_specialized_instruction_opcode_addo___rrR__n1___rr0__fR = 72,
    uninspired_specialized_instruction_opcode_addo___rrR__n1___rrR__fR = 73,
    uninspired_specialized_instruction_opcode_addo___rrR__n_m1___rr0__fR = 74,
    uninspired_specialized_instruction_opcode_addo___rrR__n_m1___rrR__fR = 75,
    uninspired_specialized_instruction_opcode_addo___rrR__nR___rr0__fR = 76,
    uninspired_specialized_instruction_opcode_addo___rrR__nR___rrR__fR = 77,
    uninspired_specialized_instruction_opcode_addo__nR___rr0___rr0__fR = 78,
    uninspired_specialized_instruction_opcode_addo__nR___rr0___rrR__fR = 79,
    uninspired_specialized_instruction_opcode_addo__nR___rrR___rr0__fR = 80,
    uninspired_specialized_instruction_opcode_addo__nR___rrR___rrR__fR = 81,
    uninspired_specialized_instruction_opcode_addo__nR__n1___rr0__fR = 82,
    uninspired_specialized_instruction_opcode_addo__nR__n1___rrR__fR = 83,
    uninspired_specialized_instruction_opcode_addo__nR__n_m1___rr0__fR = 84,
    uninspired_specialized_instruction_opcode_addo__nR__n_m1___rrR__fR = 85,
    uninspired_specialized_instruction_opcode_addo__nR__nR___rr0__fR = 86,
    uninspired_specialized_instruction_opcode_addo__nR__nR___rrR__fR = 87,
    uninspired_specialized_instruction_opcode_b__fR = 88,
    uninspired_specialized_instruction_opcode_band___rr0___rr0__fR = 89,
    uninspired_specialized_instruction_opcode_band___rr0___rrR__fR = 90,
    uninspired_specialized_instruction_opcode_band___rr0__n3__fR = 91,
    uninspired_specialized_instruction_opcode_band___rr0__n7__fR = 92,
    uninspired_specialized_instruction_opcode_band___rr0__nR__fR = 93,
    uninspired_specialized_instruction_opcode_band___rrR___rr0__fR = 94,
    uninspired_specialized_instruction_opcode_band___rrR___rrR__fR = 95,
    uninspired_specialized_instruction_opcode_band___rrR__n3__fR = 96,
    uninspired_specialized_instruction_opcode_band___rrR__n7__fR = 97,
    uninspired_specialized_instruction_opcode_band___rrR__nR__fR = 98,
    uninspired_specialized_instruction_opcode_band__nR___rr0__fR = 99,
    uninspired_specialized_instruction_opcode_band__nR___rrR__fR = 100,
    uninspired_specialized_instruction_opcode_band__nR__n3__fR = 101,
    uninspired_specialized_instruction_opcode_band__nR__n7__fR = 102,
    uninspired_specialized_instruction_opcode_band__nR__nR__fR = 103,
    uninspired_specialized_instruction_opcode_beq___rr0___rr0__fR = 104,
    uninspired_specialized_instruction_opcode_beq___rr0___rrR__fR = 105,
    uninspired_specialized_instruction_opcode_beq___rr0__nR__fR = 106,
    uninspired_specialized_instruction_opcode_beq___rrR___rr0__fR = 107,
    uninspired_specialized_instruction_opcode_beq___rrR___rrR__fR = 108,
    uninspired_specialized_instruction_opcode_beq___rrR__nR__fR = 109,
    uninspired_specialized_instruction_opcode_beq__nR___rr0__fR = 110,
    uninspired_specialized_instruction_opcode_beq__nR___rrR__fR = 111,
    uninspired_specialized_instruction_opcode_beq__nR__nR__fR = 112,
    uninspired_specialized_instruction_opcode_bge___rr0___rr0__fR = 113,
    uninspired_specialized_instruction_opcode_bge___rr0___rrR__fR = 114,
    uninspired_specialized_instruction_opcode_bge___rr0__n0__fR = 115,
    uninspired_specialized_instruction_opcode_bge___rr0__n1__fR = 116,
    uninspired_specialized_instruction_opcode_bge___rr0__nR__fR = 117,
    uninspired_specialized_instruction_opcode_bge___rrR___rr0__fR = 118,
    uninspired_specialized_instruction_opcode_bge___rrR___rrR__fR = 119,
    uninspired_specialized_instruction_opcode_bge___rrR__n0__fR = 120,
    uninspired_specialized_instruction_opcode_bge___rrR__n1__fR = 121,
    uninspired_specialized_instruction_opcode_bge___rrR__nR__fR = 122,
    uninspired_specialized_instruction_opcode_bge__n0___rr0__fR = 123,
    uninspired_specialized_instruction_opcode_bge__n0___rrR__fR = 124,
    uninspired_specialized_instruction_opcode_bge__n0__n0__fR = 125,
    uninspired_specialized_instruction_opcode_bge__n0__n1__fR = 126,
    uninspired_specialized_instruction_opcode_bge__n0__nR__fR = 127,
    uninspired_specialized_instruction_opcode_bge__n1___rr0__fR = 128,
    uninspired_specialized_instruction_opcode_bge__n1___rrR__fR = 129,
    uninspired_specialized_instruction_opcode_bge__n1__n0__fR = 130,
    uninspired_specialized_instruction_opcode_bge__n1__n1__fR = 131,
    uninspired_specialized_instruction_opcode_bge__n1__nR__fR = 132,
    uninspired_specialized_instruction_opcode_bge__nR___rr0__fR = 133,
    uninspired_specialized_instruction_opcode_bge__nR___rrR__fR = 134,
    uninspired_specialized_instruction_opcode_bge__nR__n0__fR = 135,
    uninspired_specialized_instruction_opcode_bge__nR__n1__fR = 136,
    uninspired_specialized_instruction_opcode_bge__nR__nR__fR = 137,
    uninspired_specialized_instruction_opcode_bgeu___rr0___rr0__fR = 138,
    uninspired_specialized_instruction_opcode_bgeu___rr0___rrR__fR = 139,
    uninspired_specialized_instruction_opcode_bgeu___rr0__n0__fR = 140,
    uninspired_specialized_instruction_opcode_bgeu___rr0__n1__fR = 141,
    uninspired_specialized_instruction_opcode_bgeu___rr0__nR__fR = 142,
    uninspired_specialized_instruction_opcode_bgeu___rrR___rr0__fR = 143,
    uninspired_specialized_instruction_opcode_bgeu___rrR___rrR__fR = 144,
    uninspired_specialized_instruction_opcode_bgeu___rrR__n0__fR = 145,
    uninspired_specialized_instruction_opcode_bgeu___rrR__n1__fR = 146,
    uninspired_specialized_instruction_opcode_bgeu___rrR__nR__fR = 147,
    uninspired_specialized_instruction_opcode_bgeu__n0___rr0__fR = 148,
    uninspired_specialized_instruction_opcode_bgeu__n0___rrR__fR = 149,
    uninspired_specialized_instruction_opcode_bgeu__n0__n0__fR = 150,
    uninspired_specialized_instruction_opcode_bgeu__n0__n1__fR = 151,
    uninspired_specialized_instruction_opcode_bgeu__n0__nR__fR = 152,
    uninspired_specialized_instruction_opcode_bgeu__n1___rr0__fR = 153,
    uninspired_specialized_instruction_opcode_bgeu__n1___rrR__fR = 154,
    uninspired_specialized_instruction_opcode_bgeu__n1__n0__fR = 155,
    uninspired_specialized_instruction_opcode_bgeu__n1__n1__fR = 156,
    uninspired_specialized_instruction_opcode_bgeu__n1__nR__fR = 157,
    uninspired_specialized_instruction_opcode_bgeu__nR___rr0__fR = 158,
    uninspired_specialized_instruction_opcode_bgeu__nR___rrR__fR = 159,
    uninspired_specialized_instruction_opcode_bgeu__nR__n0__fR = 160,
    uninspired_specialized_instruction_opcode_bgeu__nR__n1__fR = 161,
    uninspired_specialized_instruction_opcode_bgeu__nR__nR__fR = 162,
    uninspired_specialized_instruction_opcode_bgt___rr0___rr0__fR = 163,
    uninspired_specialized_instruction_opcode_bgt___rr0___rrR__fR = 164,
    uninspired_specialized_instruction_opcode_bgt___rr0__n0__fR = 165,
    uninspired_specialized_instruction_opcode_bgt___rr0__n1__fR = 166,
    uninspired_specialized_instruction_opcode_bgt___rr0__nR__fR = 167,
    uninspired_specialized_instruction_opcode_bgt___rrR___rr0__fR = 168,
    uninspired_specialized_instruction_opcode_bgt___rrR___rrR__fR = 169,
    uninspired_specialized_instruction_opcode_bgt___rrR__n0__fR = 170,
    uninspired_specialized_instruction_opcode_bgt___rrR__n1__fR = 171,
    uninspired_specialized_instruction_opcode_bgt___rrR__nR__fR = 172,
    uninspired_specialized_instruction_opcode_bgt__n0___rr0__fR = 173,
    uninspired_specialized_instruction_opcode_bgt__n0___rrR__fR = 174,
    uninspired_specialized_instruction_opcode_bgt__n0__n0__fR = 175,
    uninspired_specialized_instruction_opcode_bgt__n0__n1__fR = 176,
    uninspired_specialized_instruction_opcode_bgt__n0__nR__fR = 177,
    uninspired_specialized_instruction_opcode_bgt__n1___rr0__fR = 178,
    uninspired_specialized_instruction_opcode_bgt__n1___rrR__fR = 179,
    uninspired_specialized_instruction_opcode_bgt__n1__n0__fR = 180,
    uninspired_specialized_instruction_opcode_bgt__n1__n1__fR = 181,
    uninspired_specialized_instruction_opcode_bgt__n1__nR__fR = 182,
    uninspired_specialized_instruction_opcode_bgt__nR___rr0__fR = 183,
    uninspired_specialized_instruction_opcode_bgt__nR___rrR__fR = 184,
    uninspired_specialized_instruction_opcode_bgt__nR__n0__fR = 185,
    uninspired_specialized_instruction_opcode_bgt__nR__n1__fR = 186,
    uninspired_specialized_instruction_opcode_bgt__nR__nR__fR = 187,
    uninspired_specialized_instruction_opcode_bgtu___rr0___rr0__fR = 188,
    uninspired_specialized_instruction_opcode_bgtu___rr0___rrR__fR = 189,
    uninspired_specialized_instruction_opcode_bgtu___rr0__n0__fR = 190,
    uninspired_specialized_instruction_opcode_bgtu___rr0__n1__fR = 191,
    uninspired_specialized_instruction_opcode_bgtu___rr0__nR__fR = 192,
    uninspired_specialized_instruction_opcode_bgtu___rrR___rr0__fR = 193,
    uninspired_specialized_instruction_opcode_bgtu___rrR___rrR__fR = 194,
    uninspired_specialized_instruction_opcode_bgtu___rrR__n0__fR = 195,
    uninspired_specialized_instruction_opcode_bgtu___rrR__n1__fR = 196,
    uninspired_specialized_instruction_opcode_bgtu___rrR__nR__fR = 197,
    uninspired_specialized_instruction_opcode_bgtu__n0___rr0__fR = 198,
    uninspired_specialized_instruction_opcode_bgtu__n0___rrR__fR = 199,
    uninspired_specialized_instruction_opcode_bgtu__n0__n0__fR = 200,
    uninspired_specialized_instruction_opcode_bgtu__n0__n1__fR = 201,
    uninspired_specialized_instruction_opcode_bgtu__n0__nR__fR = 202,
    uninspired_specialized_instruction_opcode_bgtu__n1___rr0__fR = 203,
    uninspired_specialized_instruction_opcode_bgtu__n1___rrR__fR = 204,
    uninspired_specialized_instruction_opcode_bgtu__n1__n0__fR = 205,
    uninspired_specialized_instruction_opcode_bgtu__n1__n1__fR = 206,
    uninspired_specialized_instruction_opcode_bgtu__n1__nR__fR = 207,
    uninspired_specialized_instruction_opcode_bgtu__nR___rr0__fR = 208,
    uninspired_specialized_instruction_opcode_bgtu__nR___rrR__fR = 209,
    uninspired_specialized_instruction_opcode_bgtu__nR__n0__fR = 210,
    uninspired_specialized_instruction_opcode_bgtu__nR__n1__fR = 211,
    uninspired_specialized_instruction_opcode_bgtu__nR__nR__fR = 212,
    uninspired_specialized_instruction_opcode_ble___rr0___rr0__fR = 213,
    uninspired_specialized_instruction_opcode_ble___rr0___rrR__fR = 214,
    uninspired_specialized_instruction_opcode_ble___rr0__n0__fR = 215,
    uninspired_specialized_instruction_opcode_ble___rr0__n1__fR = 216,
    uninspired_specialized_instruction_opcode_ble___rr0__nR__fR = 217,
    uninspired_specialized_instruction_opcode_ble___rrR___rr0__fR = 218,
    uninspired_specialized_instruction_opcode_ble___rrR___rrR__fR = 219,
    uninspired_specialized_instruction_opcode_ble___rrR__n0__fR = 220,
    uninspired_specialized_instruction_opcode_ble___rrR__n1__fR = 221,
    uninspired_specialized_instruction_opcode_ble___rrR__nR__fR = 222,
    uninspired_specialized_instruction_opcode_ble__n0___rr0__fR = 223,
    uninspired_specialized_instruction_opcode_ble__n0___rrR__fR = 224,
    uninspired_specialized_instruction_opcode_ble__n0__n0__fR = 225,
    uninspired_specialized_instruction_opcode_ble__n0__n1__fR = 226,
    uninspired_specialized_instruction_opcode_ble__n0__nR__fR = 227,
    uninspired_specialized_instruction_opcode_ble__n1___rr0__fR = 228,
    uninspired_specialized_instruction_opcode_ble__n1___rrR__fR = 229,
    uninspired_specialized_instruction_opcode_ble__n1__n0__fR = 230,
    uninspired_specialized_instruction_opcode_ble__n1__n1__fR = 231,
    uninspired_specialized_instruction_opcode_ble__n1__nR__fR = 232,
    uninspired_specialized_instruction_opcode_ble__nR___rr0__fR = 233,
    uninspired_specialized_instruction_opcode_ble__nR___rrR__fR = 234,
    uninspired_specialized_instruction_opcode_ble__nR__n0__fR = 235,
    uninspired_specialized_instruction_opcode_ble__nR__n1__fR = 236,
    uninspired_specialized_instruction_opcode_ble__nR__nR__fR = 237,
    uninspired_specialized_instruction_opcode_bleu___rr0___rr0__fR = 238,
    uninspired_specialized_instruction_opcode_bleu___rr0___rrR__fR = 239,
    uninspired_specialized_instruction_opcode_bleu___rr0__n0__fR = 240,
    uninspired_specialized_instruction_opcode_bleu___rr0__n1__fR = 241,
    uninspired_specialized_instruction_opcode_bleu___rr0__nR__fR = 242,
    uninspired_specialized_instruction_opcode_bleu___rrR___rr0__fR = 243,
    uninspired_specialized_instruction_opcode_bleu___rrR___rrR__fR = 244,
    uninspired_specialized_instruction_opcode_bleu___rrR__n0__fR = 245,
    uninspired_specialized_instruction_opcode_bleu___rrR__n1__fR = 246,
    uninspired_specialized_instruction_opcode_bleu___rrR__nR__fR = 247,
    uninspired_specialized_instruction_opcode_bleu__n0___rr0__fR = 248,
    uninspired_specialized_instruction_opcode_bleu__n0___rrR__fR = 249,
    uninspired_specialized_instruction_opcode_bleu__n0__n0__fR = 250,
    uninspired_specialized_instruction_opcode_bleu__n0__n1__fR = 251,
    uninspired_specialized_instruction_opcode_bleu__n0__nR__fR = 252,
    uninspired_specialized_instruction_opcode_bleu__n1___rr0__fR = 253,
    uninspired_specialized_instruction_opcode_bleu__n1___rrR__fR = 254,
    uninspired_specialized_instruction_opcode_bleu__n1__n0__fR = 255,
    uninspired_specialized_instruction_opcode_bleu__n1__n1__fR = 256,
    uninspired_specialized_instruction_opcode_bleu__n1__nR__fR = 257,
    uninspired_specialized_instruction_opcode_bleu__nR___rr0__fR = 258,
    uninspired_specialized_instruction_opcode_bleu__nR___rrR__fR = 259,
    uninspired_specialized_instruction_opcode_bleu__nR__n0__fR = 260,
    uninspired_specialized_instruction_opcode_bleu__nR__n1__fR = 261,
    uninspired_specialized_instruction_opcode_bleu__nR__nR__fR = 262,
    uninspired_specialized_instruction_opcode_blt___rr0___rr0__fR = 263,
    uninspired_specialized_instruction_opcode_blt___rr0___rrR__fR = 264,
    uninspired_specialized_instruction_opcode_blt___rr0__n0__fR = 265,
    uninspired_specialized_instruction_opcode_blt___rr0__n1__fR = 266,
    uninspired_specialized_instruction_opcode_blt___rr0__nR__fR = 267,
    uninspired_specialized_instruction_opcode_blt___rrR___rr0__fR = 268,
    uninspired_specialized_instruction_opcode_blt___rrR___rrR__fR = 269,
    uninspired_specialized_instruction_opcode_blt___rrR__n0__fR = 270,
    uninspired_specialized_instruction_opcode_blt___rrR__n1__fR = 271,
    uninspired_specialized_instruction_opcode_blt___rrR__nR__fR = 272,
    uninspired_specialized_instruction_opcode_blt__n0___rr0__fR = 273,
    uninspired_specialized_instruction_opcode_blt__n0___rrR__fR = 274,
    uninspired_specialized_instruction_opcode_blt__n0__n0__fR = 275,
    uninspired_specialized_instruction_opcode_blt__n0__n1__fR = 276,
    uninspired_specialized_instruction_opcode_blt__n0__nR__fR = 277,
    uninspired_specialized_instruction_opcode_blt__n1___rr0__fR = 278,
    uninspired_specialized_instruction_opcode_blt__n1___rrR__fR = 279,
    uninspired_specialized_instruction_opcode_blt__n1__n0__fR = 280,
    uninspired_specialized_instruction_opcode_blt__n1__n1__fR = 281,
    uninspired_specialized_instruction_opcode_blt__n1__nR__fR = 282,
    uninspired_specialized_instruction_opcode_blt__nR___rr0__fR = 283,
    uninspired_specialized_instruction_opcode_blt__nR___rrR__fR = 284,
    uninspired_specialized_instruction_opcode_blt__nR__n0__fR = 285,
    uninspired_specialized_instruction_opcode_blt__nR__n1__fR = 286,
    uninspired_specialized_instruction_opcode_blt__nR__nR__fR = 287,
    uninspired_specialized_instruction_opcode_bltu___rr0___rr0__fR = 288,
    uninspired_specialized_instruction_opcode_bltu___rr0___rrR__fR = 289,
    uninspired_specialized_instruction_opcode_bltu___rr0__n0__fR = 290,
    uninspired_specialized_instruction_opcode_bltu___rr0__n1__fR = 291,
    uninspired_specialized_instruction_opcode_bltu___rr0__nR__fR = 292,
    uninspired_specialized_instruction_opcode_bltu___rrR___rr0__fR = 293,
    uninspired_specialized_instruction_opcode_bltu___rrR___rrR__fR = 294,
    uninspired_specialized_instruction_opcode_bltu___rrR__n0__fR = 295,
    uninspired_specialized_instruction_opcode_bltu___rrR__n1__fR = 296,
    uninspired_specialized_instruction_opcode_bltu___rrR__nR__fR = 297,
    uninspired_specialized_instruction_opcode_bltu__n0___rr0__fR = 298,
    uninspired_specialized_instruction_opcode_bltu__n0___rrR__fR = 299,
    uninspired_specialized_instruction_opcode_bltu__n0__n0__fR = 300,
    uninspired_specialized_instruction_opcode_bltu__n0__n1__fR = 301,
    uninspired_specialized_instruction_opcode_bltu__n0__nR__fR = 302,
    uninspired_specialized_instruction_opcode_bltu__n1___rr0__fR = 303,
    uninspired_specialized_instruction_opcode_bltu__n1___rrR__fR = 304,
    uninspired_specialized_instruction_opcode_bltu__n1__n0__fR = 305,
    uninspired_specialized_instruction_opcode_bltu__n1__n1__fR = 306,
    uninspired_specialized_instruction_opcode_bltu__n1__nR__fR = 307,
    uninspired_specialized_instruction_opcode_bltu__nR___rr0__fR = 308,
    uninspired_specialized_instruction_opcode_bltu__nR___rrR__fR = 309,
    uninspired_specialized_instruction_opcode_bltu__nR__n0__fR = 310,
    uninspired_specialized_instruction_opcode_bltu__nR__n1__fR = 311,
    uninspired_specialized_instruction_opcode_bltu__nR__nR__fR = 312,
    uninspired_specialized_instruction_opcode_bne___rr0___rr0__fR = 313,
    uninspired_specialized_instruction_opcode_bne___rr0___rrR__fR = 314,
    uninspired_specialized_instruction_opcode_bne___rr0__n0__fR = 315,
    uninspired_specialized_instruction_opcode_bne___rr0__nR__fR = 316,
    uninspired_specialized_instruction_opcode_bne___rrR___rr0__fR = 317,
    uninspired_specialized_instruction_opcode_bne___rrR___rrR__fR = 318,
    uninspired_specialized_instruction_opcode_bne___rrR__n0__fR = 319,
    uninspired_specialized_instruction_opcode_bne___rrR__nR__fR = 320,
    uninspired_specialized_instruction_opcode_bne__n0___rr0__fR = 321,
    uninspired_specialized_instruction_opcode_bne__n0___rrR__fR = 322,
    uninspired_specialized_instruction_opcode_bne__n0__n0__fR = 323,
    uninspired_specialized_instruction_opcode_bne__n0__nR__fR = 324,
    uninspired_specialized_instruction_opcode_bne__nR___rr0__fR = 325,
    uninspired_specialized_instruction_opcode_bne__nR___rrR__fR = 326,
    uninspired_specialized_instruction_opcode_bne__nR__n0__fR = 327,
    uninspired_specialized_instruction_opcode_bne__nR__nR__fR = 328,
    uninspired_specialized_instruction_opcode_bneg___rr0__fR = 329,
    uninspired_specialized_instruction_opcode_bneg___rrR__fR = 330,
    uninspired_specialized_instruction_opcode_bneg__nR__fR = 331,
    uninspired_specialized_instruction_opcode_bnneg___rr0__fR = 332,
    uninspired_specialized_instruction_opcode_bnneg___rrR__fR = 333,
    uninspired_specialized_instruction_opcode_bnneg__nR__fR = 334,
    uninspired_specialized_instruction_opcode_bnotand___rr0___rr0__fR = 335,
    uninspired_specialized_instruction_opcode_bnotand___rr0___rrR__fR = 336,
    uninspired_specialized_instruction_opcode_bnotand___rr0__n3__fR = 337,
    uninspired_specialized_instruction_opcode_bnotand___rr0__n7__fR = 338,
    uninspired_specialized_instruction_opcode_bnotand___rr0__nR__fR = 339,
    uninspired_specialized_instruction_opcode_bnotand___rrR___rr0__fR = 340,
    uninspired_specialized_instruction_opcode_bnotand___rrR___rrR__fR = 341,
    uninspired_specialized_instruction_opcode_bnotand___rrR__n3__fR = 342,
    uninspired_specialized_instruction_opcode_bnotand___rrR__n7__fR = 343,
    uninspired_specialized_instruction_opcode_bnotand___rrR__nR__fR = 344,
    uninspired_specialized_instruction_opcode_bnotand__nR___rr0__fR = 345,
    uninspired_specialized_instruction_opcode_bnotand__nR___rrR__fR = 346,
    uninspired_specialized_instruction_opcode_bnotand__nR__n3__fR = 347,
    uninspired_specialized_instruction_opcode_bnotand__nR__n7__fR = 348,
    uninspired_specialized_instruction_opcode_bnotand__nR__nR__fR = 349,
    uninspired_specialized_instruction_opcode_bnpos___rr0__fR = 350,
    uninspired_specialized_instruction_opcode_bnpos___rrR__fR = 351,
    uninspired_specialized_instruction_opcode_bnpos__nR__fR = 352,
    uninspired_specialized_instruction_opcode_bnz___rr0__fR = 353,
    uninspired_specialized_instruction_opcode_bnz___rrR__fR = 354,
    uninspired_specialized_instruction_opcode_bnz__nR__fR = 355,
    uninspired_specialized_instruction_opcode_bpos___rr0__fR = 356,
    uninspired_specialized_instruction_opcode_bpos___rrR__fR = 357,
    uninspired_specialized_instruction_opcode_bpos__nR__fR = 358,
    uninspired_specialized_instruction_opcode_br___rr0 = 359,
    uninspired_specialized_instruction_opcode_br___rrR = 360,
    uninspired_specialized_instruction_opcode_bulge__n0 = 361,
    uninspired_specialized_instruction_opcode_bulge__n1 = 362,
    uninspired_specialized_instruction_opcode_bulge__n2 = 363,
    uninspired_specialized_instruction_opcode_bulge__n3 = 364,
    uninspired_specialized_instruction_opcode_bulge__nR = 365,
    uninspired_specialized_instruction_opcode_bulgeforth = 366,
    uninspired_specialized_instruction_opcode_bz___rr0__fR = 367,
    uninspired_specialized_instruction_opcode_bz___rrR__fR = 368,
    uninspired_specialized_instruction_opcode_bz__nR__fR = 369,
    uninspired_specialized_instruction_opcode_clear_mpending__retR = 370,
    uninspired_specialized_instruction_opcode_div___rr0___rr0___rr0__retR = 371,
    uninspired_specialized_instruction_opcode_div___rr0___rr0___rrR__retR = 372,
    uninspired_specialized_instruction_opcode_div___rr0___rrR___rr0__retR = 373,
    uninspired_specialized_instruction_opcode_div___rr0___rrR___rrR__retR = 374,
    uninspired_specialized_instruction_opcode_div___rr0__n2___rr0__retR = 375,
    uninspired_specialized_instruction_opcode_div___rr0__n2___rrR__retR = 376,
    uninspired_specialized_instruction_opcode_div___rr0__nR___rr0__retR = 377,
    uninspired_specialized_instruction_opcode_div___rr0__nR___rrR__retR = 378,
    uninspired_specialized_instruction_opcode_div___rrR___rr0___rr0__retR = 379,
    uninspired_specialized_instruction_opcode_div___rrR___rr0___rrR__retR = 380,
    uninspired_specialized_instruction_opcode_div___rrR___rrR___rr0__retR = 381,
    uninspired_specialized_instruction_opcode_div___rrR___rrR___rrR__retR = 382,
    uninspired_specialized_instruction_opcode_div___rrR__n2___rr0__retR = 383,
    uninspired_specialized_instruction_opcode_div___rrR__n2___rrR__retR = 384,
    uninspired_specialized_instruction_opcode_div___rrR__nR___rr0__retR = 385,
    uninspired_specialized_instruction_opcode_div___rrR__nR___rrR__retR = 386,
    uninspired_specialized_instruction_opcode_div__nR___rr0___rr0__retR = 387,
    uninspired_specialized_instruction_opcode_div__nR___rr0___rrR__retR = 388,
    uninspired_specialized_instruction_opcode_div__nR___rrR___rr0__retR = 389,
    uninspired_specialized_instruction_opcode_div__nR___rrR___rrR__retR = 390,
    uninspired_specialized_instruction_opcode_div__nR__n2___rr0__retR = 391,
    uninspired_specialized_instruction_opcode_div__nR__n2___rrR__retR = 392,
    uninspired_specialized_instruction_opcode_div__nR__nR___rr0__retR = 393,
    uninspired_specialized_instruction_opcode_div__nR__nR___rrR__retR = 394,
    uninspired_specialized_instruction_opcode_divo___rr0___rr0___rr0__fR = 395,
    uninspired_specialized_instruction_opcode_divo___rr0___rr0___rrR__fR = 396,
    uninspired_specialized_instruction_opcode_divo___rr0___rrR___rr0__fR = 397,
    uninspired_specialized_instruction_opcode_divo___rr0___rrR___rrR__fR = 398,
    uninspired_specialized_instruction_opcode_divo___rr0__n2___rr0__fR = 399,
    uninspired_specialized_instruction_opcode_divo___rr0__n2___rrR__fR = 400,
    uninspired_specialized_instruction_opcode_divo___rr0__nR___rr0__fR = 401,
    uninspired_specialized_instruction_opcode_divo___rr0__nR___rrR__fR = 402,
    uninspired_specialized_instruction_opcode_divo___rrR___rr0___rr0__fR = 403,
    uninspired_specialized_instruction_opcode_divo___rrR___rr0___rrR__fR = 404,
    uninspired_specialized_instruction_opcode_divo___rrR___rrR___rr0__fR = 405,
    uninspired_specialized_instruction_opcode_divo___rrR___rrR___rrR__fR = 406,
    uninspired_specialized_instruction_opcode_divo___rrR__n2___rr0__fR = 407,
    uninspired_specialized_instruction_opcode_divo___rrR__n2___rrR__fR = 408,
    uninspired_specialized_instruction_opcode_divo___rrR__nR___rr0__fR = 409,
    uninspired_specialized_instruction_opcode_divo___rrR__nR___rrR__fR = 410,
    uninspired_specialized_instruction_opcode_divo__nR___rr0___rr0__fR = 411,
    uninspired_specialized_instruction_opcode_divo__nR___rr0___rrR__fR = 412,
    uninspired_specialized_instruction_opcode_divo__nR___rrR___rr0__fR = 413,
    uninspired_specialized_instruction_opcode_divo__nR___rrR___rrR__fR = 414,
    uninspired_specialized_instruction_opcode_divo__nR__n2___rr0__fR = 415,
    uninspired_specialized_instruction_opcode_divo__nR__n2___rrR__fR = 416,
    uninspired_specialized_instruction_opcode_divo__nR__nR___rr0__fR = 417,
    uninspired_specialized_instruction_opcode_divo__nR__nR___rrR__fR = 418,
    uninspired_specialized_instruction_opcode_drop_mhandler = 419,
    uninspired_specialized_instruction_opcode_endvm = 420,
    uninspired_specialized_instruction_opcode_exit___rr0__retR = 421,
    uninspired_specialized_instruction_opcode_exit___rrR__retR = 422,
    uninspired_specialized_instruction_opcode_exit__nR__retR = 423,
    uninspired_specialized_instruction_opcode_exitvm = 424,
    uninspired_specialized_instruction_opcode_fadd___rfR___rfR___rfR = 425,
    uninspired_specialized_instruction_opcode_fdiv___rfR___rfR___rfR = 426,
    uninspired_specialized_instruction_opcode_fincr___rfR = 427,
    uninspired_specialized_instruction_opcode_fmul___rfR___rfR___rfR = 428,
    uninspired_specialized_instruction_opcode_fprint___rfR = 429,
    uninspired_specialized_instruction_opcode_fset___rr0___rfR = 430,
    uninspired_specialized_instruction_opcode_fset___rrR___rfR = 431,
    uninspired_specialized_instruction_opcode_fset__nR___rfR = 432,
    uninspired_specialized_instruction_opcode_fsub___rfR___rfR___rfR = 433,
    uninspired_specialized_instruction_opcode_hcf__retR = 434,
    uninspired_specialized_instruction_opcode_install_msignal_mhandler__retR = 435,
    uninspired_specialized_instruction_opcode_loadwithbyteoffset___rr0___rr0___rr0 = 436,
    uninspired_specialized_instruction_opcode_loadwithbyteoffset___rr0___rr0___rrR = 437,
    uninspired_specialized_instruction_opcode_loadwithbyteoffset___rr0___rrR___rr0 = 438,
    uninspired_specialized_instruction_opcode_loadwithbyteoffset___rr0___rrR___rrR = 439,
    uninspired_specialized_instruction_opcode_loadwithbyteoffset___rr0__n0___rr0 = 440,
    uninspired_specialized_instruction_opcode_loadwithbyteoffset___rr0__n0___rrR = 441,
    uninspired_specialized_instruction_opcode_loadwithbyteoffset___rr0__n8___rr0 = 442,
    uninspired_specialized_instruction_opcode_loadwithbyteoffset___rr0__n8___rrR = 443,
    uninspired_specialized_instruction_opcode_loadwithbyteoffset___rr0__nR___rr0 = 444,
    uninspired_specialized_instruction_opcode_loadwithbyteoffset___rr0__nR___rrR = 445,
    uninspired_specialized_instruction_opcode_loadwithbyteoffset___rrR___rr0___rr0 = 446,
    uninspired_specialized_instruction_opcode_loadwithbyteoffset___rrR___rr0___rrR = 447,
    uninspired_specialized_instruction_opcode_loadwithbyteoffset___rrR___rrR___rr0 = 448,
    uninspired_specialized_instruction_opcode_loadwithbyteoffset___rrR___rrR___rrR = 449,
    uninspired_specialized_instruction_opcode_loadwithbyteoffset___rrR__n0___rr0 = 450,
    uninspired_specialized_instruction_opcode_loadwithbyteoffset___rrR__n0___rrR = 451,
    uninspired_specialized_instruction_opcode_loadwithbyteoffset___rrR__n8___rr0 = 452,
    uninspired_specialized_instruction_opcode_loadwithbyteoffset___rrR__n8___rrR = 453,
    uninspired_specialized_instruction_opcode_loadwithbyteoffset___rrR__nR___rr0 = 454,
    uninspired_specialized_instruction_opcode_loadwithbyteoffset___rrR__nR___rrR = 455,
    uninspired_specialized_instruction_opcode_loadwithwordoffset___rr0___rr0___rr0 = 456,
    uninspired_specialized_instruction_opcode_loadwithwordoffset___rr0___rr0___rrR = 457,
    uninspired_specialized_instruction_opcode_loadwithwordoffset___rr0___rrR___rr0 = 458,
    uninspired_specialized_instruction_opcode_loadwithwordoffset___rr0___rrR___rrR = 459,
    uninspired_specialized_instruction_opcode_loadwithwordoffset___rr0__n0___rr0 = 460,
    uninspired_specialized_instruction_opcode_loadwithwordoffset___rr0__n0___rrR = 461,
    uninspired_specialized_instruction_opcode_loadwithwordoffset___rr0__n1___rr0 = 462,
    uninspired_specialized_instruction_opcode_loadwithwordoffset___rr0__n1___rrR = 463,
    uninspired_specialized_instruction_opcode_loadwithwordoffset___rr0__n2___rr0 = 464,
    uninspired_specialized_instruction_opcode_loadwithwordoffset___rr0__n2___rrR = 465,
    uninspired_specialized_instruction_opcode_loadwithwordoffset___rr0__nR___rr0 = 466,
    uninspired_specialized_instruction_opcode_loadwithwordoffset___rr0__nR___rrR = 467,
    uninspired_specialized_instruction_opcode_loadwithwordoffset___rrR___rr0___rr0 = 468,
    uninspired_specialized_instruction_opcode_loadwithwordoffset___rrR___rr0___rrR = 469,
    uninspired_specialized_instruction_opcode_loadwithwordoffset___rrR___rrR___rr0 = 470,
    uninspired_specialized_instruction_opcode_loadwithwordoffset___rrR___rrR___rrR = 471,
    uninspired_specialized_instruction_opcode_loadwithwordoffset___rrR__n0___rr0 = 472,
    uninspired_specialized_instruction_opcode_loadwithwordoffset___rrR__n0___rrR = 473,
    uninspired_specialized_instruction_opcode_loadwithwordoffset___rrR__n1___rr0 = 474,
    uninspired_specialized_instruction_opcode_loadwithwordoffset___rrR__n1___rrR = 475,
    uninspired_specialized_instruction_opcode_loadwithwordoffset___rrR__n2___rr0 = 476,
    uninspired_specialized_instruction_opcode_loadwithwordoffset___rrR__n2___rrR = 477,
    uninspired_specialized_instruction_opcode_loadwithwordoffset___rrR__nR___rr0 = 478,
    uninspired_specialized_instruction_opcode_loadwithwordoffset___rrR__nR___rrR = 479,
    uninspired_specialized_instruction_opcode_mallocwords___rr0___rr0__retR = 480,
    uninspired_specialized_instruction_opcode_mallocwords___rr0___rrR__retR = 481,
    uninspired_specialized_instruction_opcode_mallocwords___rrR___rr0__retR = 482,
    uninspired_specialized_instruction_opcode_mallocwords___rrR___rrR__retR = 483,
    uninspired_specialized_instruction_opcode_mallocwords__nR___rr0__retR = 484,
    uninspired_specialized_instruction_opcode_mallocwords__nR___rrR__retR = 485,
    uninspired_specialized_instruction_opcode_mod___rr0___rr0___rr0__retR = 486,
    uninspired_specialized_instruction_opcode_mod___rr0___rr0___rrR__retR = 487,
    uninspired_specialized_instruction_opcode_mod___rr0___rrR___rr0__retR = 488,
    uninspired_specialized_instruction_opcode_mod___rr0___rrR___rrR__retR = 489,
    uninspired_specialized_instruction_opcode_mod___rr0__n2___rr0__retR = 490,
    uninspired_specialized_instruction_opcode_mod___rr0__n2___rrR__retR = 491,
    uninspired_specialized_instruction_opcode_mod___rr0__nR___rr0__retR = 492,
    uninspired_specialized_instruction_opcode_mod___rr0__nR___rrR__retR = 493,
    uninspired_specialized_instruction_opcode_mod___rrR___rr0___rr0__retR = 494,
    uninspired_specialized_instruction_opcode_mod___rrR___rr0___rrR__retR = 495,
    uninspired_specialized_instruction_opcode_mod___rrR___rrR___rr0__retR = 496,
    uninspired_specialized_instruction_opcode_mod___rrR___rrR___rrR__retR = 497,
    uninspired_specialized_instruction_opcode_mod___rrR__n2___rr0__retR = 498,
    uninspired_specialized_instruction_opcode_mod___rrR__n2___rrR__retR = 499,
    uninspired_specialized_instruction_opcode_mod___rrR__nR___rr0__retR = 500,
    uninspired_specialized_instruction_opcode_mod___rrR__nR___rrR__retR = 501,
    uninspired_specialized_instruction_opcode_mod__nR___rr0___rr0__retR = 502,
    uninspired_specialized_instruction_opcode_mod__nR___rr0___rrR__retR = 503,
    uninspired_specialized_instruction_opcode_mod__nR___rrR___rr0__retR = 504,
    uninspired_specialized_instruction_opcode_mod__nR___rrR___rrR__retR = 505,
    uninspired_specialized_instruction_opcode_mod__nR__n2___rr0__retR = 506,
    uninspired_specialized_instruction_opcode_mod__nR__n2___rrR__retR = 507,
    uninspired_specialized_instruction_opcode_mod__nR__nR___rr0__retR = 508,
    uninspired_specialized_instruction_opcode_mod__nR__nR___rrR__retR = 509,
    uninspired_specialized_instruction_opcode_modo___rr0___rr0___rr0__fR = 510,
    uninspired_specialized_instruction_opcode_modo___rr0___rr0___rrR__fR = 511,
    uninspired_specialized_instruction_opcode_modo___rr0___rrR___rr0__fR = 512,
    uninspired_specialized_instruction_opcode_modo___rr0___rrR___rrR__fR = 513,
    uninspired_specialized_instruction_opcode_modo___rr0__n2___rr0__fR = 514,
    uninspired_specialized_instruction_opcode_modo___rr0__n2___rrR__fR = 515,
    uninspired_specialized_instruction_opcode_modo___rr0__nR___rr0__fR = 516,
    uninspired_specialized_instruction_opcode_modo___rr0__nR___rrR__fR = 517,
    uninspired_specialized_instruction_opcode_modo___rrR___rr0___rr0__fR = 518,
    uninspired_specialized_instruction_opcode_modo___rrR___rr0___rrR__fR = 519,
    uninspired_specialized_instruction_opcode_modo___rrR___rrR___rr0__fR = 520,
    uninspired_specialized_instruction_opcode_modo___rrR___rrR___rrR__fR = 521,
    uninspired_specialized_instruction_opcode_modo___rrR__n2___rr0__fR = 522,
    uninspired_specialized_instruction_opcode_modo___rrR__n2___rrR__fR = 523,
    uninspired_specialized_instruction_opcode_modo___rrR__nR___rr0__fR = 524,
    uninspired_specialized_instruction_opcode_modo___rrR__nR___rrR__fR = 525,
    uninspired_specialized_instruction_opcode_modo__nR___rr0___rr0__fR = 526,
    uninspired_specialized_instruction_opcode_modo__nR___rr0___rrR__fR = 527,
    uninspired_specialized_instruction_opcode_modo__nR___rrR___rr0__fR = 528,
    uninspired_specialized_instruction_opcode_modo__nR___rrR___rrR__fR = 529,
    uninspired_specialized_instruction_opcode_modo__nR__n2___rr0__fR = 530,
    uninspired_specialized_instruction_opcode_modo__nR__n2___rrR__fR = 531,
    uninspired_specialized_instruction_opcode_modo__nR__nR___rr0__fR = 532,
    uninspired_specialized_instruction_opcode_modo__nR__nR___rrR__fR = 533,
    uninspired_specialized_instruction_opcode_mov___rr0___rr0 = 534,
    uninspired_specialized_instruction_opcode_mov___rr0___rrR = 535,
    uninspired_specialized_instruction_opcode_mov___rrR___rr0 = 536,
    uninspired_specialized_instruction_opcode_mov___rrR___rrR = 537,
    uninspired_specialized_instruction_opcode_mov__n0___rr0 = 538,
    uninspired_specialized_instruction_opcode_mov__n0___rrR = 539,
    uninspired_specialized_instruction_opcode_mov__n1___rr0 = 540,
    uninspired_specialized_instruction_opcode_mov__n1___rrR = 541,
    uninspired_specialized_instruction_opcode_mov__nR___rr0 = 542,
    uninspired_specialized_instruction_opcode_mov__nR___rrR = 543,
    uninspired_specialized_instruction_opcode_mov__lR___rr0 = 544,
    uninspired_specialized_instruction_opcode_mov__lR___rrR = 545,
    uninspired_specialized_instruction_opcode_mroll__n0 = 546,
    uninspired_specialized_instruction_opcode_mroll__n1 = 547,
    uninspired_specialized_instruction_opcode_mroll__n2 = 548,
    uninspired_specialized_instruction_opcode_mroll__n3 = 549,
    uninspired_specialized_instruction_opcode_mroll__n4 = 550,
    uninspired_specialized_instruction_opcode_mroll__n5 = 551,
    uninspired_specialized_instruction_opcode_mroll__n6 = 552,
    uninspired_specialized_instruction_opcode_mroll__n7 = 553,
    uninspired_specialized_instruction_opcode_mroll__n8 = 554,
    uninspired_specialized_instruction_opcode_mroll__n9 = 555,
    uninspired_specialized_instruction_opcode_mroll__n10 = 556,
    uninspired_specialized_instruction_opcode_mroll__n11 = 557,
    uninspired_specialized_instruction_opcode_mroll__n12 = 558,
    uninspired_specialized_instruction_opcode_mroll__n13 = 559,
    uninspired_specialized_instruction_opcode_mroll__n14 = 560,
    uninspired_specialized_instruction_opcode_mroll__n15 = 561,
    uninspired_specialized_instruction_opcode_mroll__n16 = 562,
    uninspired_specialized_instruction_opcode_mroll__n17 = 563,
    uninspired_specialized_instruction_opcode_mroll__n18 = 564,
    uninspired_specialized_instruction_opcode_mroll__n19 = 565,
    uninspired_specialized_instruction_opcode_mroll__n20 = 566,
    uninspired_specialized_instruction_opcode_mroll__n21 = 567,
    uninspired_specialized_instruction_opcode_mroll__n22 = 568,
    uninspired_specialized_instruction_opcode_mroll__n23 = 569,
    uninspired_specialized_instruction_opcode_mroll__n24 = 570,
    uninspired_specialized_instruction_opcode_mroll__n25 = 571,
    uninspired_specialized_instruction_opcode_mroll__n26 = 572,
    uninspired_specialized_instruction_opcode_mroll__n27 = 573,
    uninspired_specialized_instruction_opcode_mroll__n28 = 574,
    uninspired_specialized_instruction_opcode_mroll__n29 = 575,
    uninspired_specialized_instruction_opcode_mroll__n30 = 576,
    uninspired_specialized_instruction_opcode_mroll__n31 = 577,
    uninspired_specialized_instruction_opcode_mroll__n32 = 578,
    uninspired_specialized_instruction_opcode_mroll__n33 = 579,
    uninspired_specialized_instruction_opcode_mroll__n34 = 580,
    uninspired_specialized_instruction_opcode_mroll__n35 = 581,
    uninspired_specialized_instruction_opcode_mroll__n36 = 582,
    uninspired_specialized_instruction_opcode_mroll__n37 = 583,
    uninspired_specialized_instruction_opcode_mroll__n38 = 584,
    uninspired_specialized_instruction_opcode_mroll__n39 = 585,
    uninspired_specialized_instruction_opcode_mroll__n40 = 586,
    uninspired_specialized_instruction_opcode_mroll__n41 = 587,
    uninspired_specialized_instruction_opcode_mroll__n42 = 588,
    uninspired_specialized_instruction_opcode_mroll__n43 = 589,
    uninspired_specialized_instruction_opcode_mroll__n44 = 590,
    uninspired_specialized_instruction_opcode_mroll__n45 = 591,
    uninspired_specialized_instruction_opcode_mroll__n46 = 592,
    uninspired_specialized_instruction_opcode_mroll__n47 = 593,
    uninspired_specialized_instruction_opcode_mroll__n48 = 594,
    uninspired_specialized_instruction_opcode_mroll__n49 = 595,
    uninspired_specialized_instruction_opcode_mroll__n50 = 596,
    uninspired_specialized_instruction_opcode_mroll__n51 = 597,
    uninspired_specialized_instruction_opcode_mroll__n52 = 598,
    uninspired_specialized_instruction_opcode_mroll__n53 = 599,
    uninspired_specialized_instruction_opcode_mroll__n54 = 600,
    uninspired_specialized_instruction_opcode_mroll__n55 = 601,
    uninspired_specialized_instruction_opcode_mroll__n56 = 602,
    uninspired_specialized_instruction_opcode_mroll__n57 = 603,
    uninspired_specialized_instruction_opcode_mroll__n58 = 604,
    uninspired_specialized_instruction_opcode_mroll__n59 = 605,
    uninspired_specialized_instruction_opcode_mroll__n60 = 606,
    uninspired_specialized_instruction_opcode_mroll__n61 = 607,
    uninspired_specialized_instruction_opcode_mroll__n62 = 608,
    uninspired_specialized_instruction_opcode_mroll__n63 = 609,
    uninspired_specialized_instruction_opcode_mroll__n64 = 610,
    uninspired_specialized_instruction_opcode_mroll__n65 = 611,
    uninspired_specialized_instruction_opcode_mroll__n66 = 612,
    uninspired_specialized_instruction_opcode_mroll__n67 = 613,
    uninspired_specialized_instruction_opcode_mroll__n68 = 614,
    uninspired_specialized_instruction_opcode_mroll__n69 = 615,
    uninspired_specialized_instruction_opcode_mroll__n70 = 616,
    uninspired_specialized_instruction_opcode_mroll__nR = 617,
    uninspired_specialized_instruction_opcode_mrollforth = 618,
    uninspired_specialized_instruction_opcode_mrot = 619,
    uninspired_specialized_instruction_opcode_mul___rr0___rr0___rr0 = 620,
    uninspired_specialized_instruction_opcode_mul___rr0___rr0___rrR = 621,
    uninspired_specialized_instruction_opcode_mul___rr0___rrR___rr0 = 622,
    uninspired_specialized_instruction_opcode_mul___rr0___rrR___rrR = 623,
    uninspired_specialized_instruction_opcode_mul___rr0__n2___rr0 = 624,
    uninspired_specialized_instruction_opcode_mul___rr0__n2___rrR = 625,
    uninspired_specialized_instruction_opcode_mul___rr0__nR___rr0 = 626,
    uninspired_specialized_instruction_opcode_mul___rr0__nR___rrR = 627,
    uninspired_specialized_instruction_opcode_mul___rrR___rr0___rr0 = 628,
    uninspired_specialized_instruction_opcode_mul___rrR___rr0___rrR = 629,
    uninspired_specialized_instruction_opcode_mul___rrR___rrR___rr0 = 630,
    uninspired_specialized_instruction_opcode_mul___rrR___rrR___rrR = 631,
    uninspired_specialized_instruction_opcode_mul___rrR__n2___rr0 = 632,
    uninspired_specialized_instruction_opcode_mul___rrR__n2___rrR = 633,
    uninspired_specialized_instruction_opcode_mul___rrR__nR___rr0 = 634,
    uninspired_specialized_instruction_opcode_mul___rrR__nR___rrR = 635,
    uninspired_specialized_instruction_opcode_mul__n2___rr0___rr0 = 636,
    uninspired_specialized_instruction_opcode_mul__n2___rr0___rrR = 637,
    uninspired_specialized_instruction_opcode_mul__n2___rrR___rr0 = 638,
    uninspired_specialized_instruction_opcode_mul__n2___rrR___rrR = 639,
    uninspired_specialized_instruction_opcode_mul__n2__n2___rr0 = 640,
    uninspired_specialized_instruction_opcode_mul__n2__n2___rrR = 641,
    uninspired_specialized_instruction_opcode_mul__n2__nR___rr0 = 642,
    uninspired_specialized_instruction_opcode_mul__n2__nR___rrR = 643,
    uninspired_specialized_instruction_opcode_mul__nR___rr0___rr0 = 644,
    uninspired_specialized_instruction_opcode_mul__nR___rr0___rrR = 645,
    uninspired_specialized_instruction_opcode_mul__nR___rrR___rr0 = 646,
    uninspired_specialized_instruction_opcode_mul__nR___rrR___rrR = 647,
    uninspired_specialized_instruction_opcode_mul__nR__n2___rr0 = 648,
    uninspired_specialized_instruction_opcode_mul__nR__n2___rrR = 649,
    uninspired_specialized_instruction_opcode_mul__nR__nR___rr0 = 650,
    uninspired_specialized_instruction_opcode_mul__nR__nR___rrR = 651,
    uninspired_specialized_instruction_opcode_mulo___rr0___rr0___rr0__fR = 652,
    uninspired_specialized_instruction_opcode_mulo___rr0___rr0___rrR__fR = 653,
    uninspired_specialized_instruction_opcode_mulo___rr0___rrR___rr0__fR = 654,
    uninspired_specialized_instruction_opcode_mulo___rr0___rrR___rrR__fR = 655,
    uninspired_specialized_instruction_opcode_mulo___rr0__n2___rr0__fR = 656,
    uninspired_specialized_instruction_opcode_mulo___rr0__n2___rrR__fR = 657,
    uninspired_specialized_instruction_opcode_mulo___rr0__nR___rr0__fR = 658,
    uninspired_specialized_instruction_opcode_mulo___rr0__nR___rrR__fR = 659,
    uninspired_specialized_instruction_opcode_mulo___rrR___rr0___rr0__fR = 660,
    uninspired_specialized_instruction_opcode_mulo___rrR___rr0___rrR__fR = 661,
    uninspired_specialized_instruction_opcode_mulo___rrR___rrR___rr0__fR = 662,
    uninspired_specialized_instruction_opcode_mulo___rrR___rrR___rrR__fR = 663,
    uninspired_specialized_instruction_opcode_mulo___rrR__n2___rr0__fR = 664,
    uninspired_specialized_instruction_opcode_mulo___rrR__n2___rrR__fR = 665,
    uninspired_specialized_instruction_opcode_mulo___rrR__nR___rr0__fR = 666,
    uninspired_specialized_instruction_opcode_mulo___rrR__nR___rrR__fR = 667,
    uninspired_specialized_instruction_opcode_mulo__nR___rr0___rr0__fR = 668,
    uninspired_specialized_instruction_opcode_mulo__nR___rr0___rrR__fR = 669,
    uninspired_specialized_instruction_opcode_mulo__nR___rrR___rr0__fR = 670,
    uninspired_specialized_instruction_opcode_mulo__nR___rrR___rrR__fR = 671,
    uninspired_specialized_instruction_opcode_mulo__nR__n2___rr0__fR = 672,
    uninspired_specialized_instruction_opcode_mulo__nR__n2___rrR__fR = 673,
    uninspired_specialized_instruction_opcode_mulo__nR__nR___rr0__fR = 674,
    uninspired_specialized_instruction_opcode_mulo__nR__nR___rrR__fR = 675,
    uninspired_specialized_instruction_opcode_nop = 676,
    uninspired_specialized_instruction_opcode_print_mpending_msignals__retR = 677,
    uninspired_specialized_instruction_opcode_print_mtopmost__nR__retR = 678,
    uninspired_specialized_instruction_opcode_printfixnum___rr0__retR = 679,
    uninspired_specialized_instruction_opcode_printfixnum___rrR__retR = 680,
    uninspired_specialized_instruction_opcode_printfixnum__nR__retR = 681,
    uninspired_specialized_instruction_opcode_procedurecall__fR__retR = 682,
    uninspired_specialized_instruction_opcode_procedurecallr___rr0__retR = 683,
    uninspired_specialized_instruction_opcode_procedurecallr___rrR__retR = 684,
    uninspired_specialized_instruction_opcode_procedurecallr__lR__retR = 685,
    uninspired_specialized_instruction_opcode_procedureprolog = 686,
    uninspired_specialized_instruction_opcode_procedurereturn = 687,
    uninspired_specialized_instruction_opcode_push_mdepths__nR__retR = 688,
    uninspired_specialized_instruction_opcode_push_mhandler__n0__lR = 689,
    uninspired_specialized_instruction_opcode_push_mhandler__n1__lR = 690,
    uninspired_specialized_instruction_opcode_push_mhandler__n2__lR = 691,
    uninspired_specialized_instruction_opcode_push_mhandler__n3__lR = 692,
    uninspired_specialized_instruction_opcode_push_mhandler__n4__lR = 693,
    uninspired_specialized_instruction_opcode_push_mhandler__n5__lR = 694,
    uninspired_specialized_instruction_opcode_push_mhandler__n6__lR = 695,
    uninspired_specialized_instruction_opcode_push_mhandler__n7__lR = 696,
    uninspired_specialized_instruction_opcode_push_mhandler__n8__lR = 697,
    uninspired_specialized_instruction_opcode_push_mhandler__n9__lR = 698,
    uninspired_specialized_instruction_opcode_push_mhandler__n10__lR = 699,
    uninspired_specialized_instruction_opcode_push_mhandler__nR__lR = 700,
    uninspired_specialized_instruction_opcode_push_mincreasing__nR__retR = 701,
    uninspired_specialized_instruction_opcode_quake = 702,
    uninspired_specialized_instruction_opcode_raise___rr0 = 703,
    uninspired_specialized_instruction_opcode_raise___rrR = 704,
    uninspired_specialized_instruction_opcode_raise__n0 = 705,
    uninspired_specialized_instruction_opcode_raise__n1 = 706,
    uninspired_specialized_instruction_opcode_raise__n2 = 707,
    uninspired_specialized_instruction_opcode_raise__n3 = 708,
    uninspired_specialized_instruction_opcode_raise__n4 = 709,
    uninspired_specialized_instruction_opcode_raise__n5 = 710,
    uninspired_specialized_instruction_opcode_raise__n6 = 711,
    uninspired_specialized_instruction_opcode_raise__n7 = 712,
    uninspired_specialized_instruction_opcode_raise__n8 = 713,
    uninspired_specialized_instruction_opcode_raise__n9 = 714,
    uninspired_specialized_instruction_opcode_raise__n10 = 715,
    uninspired_specialized_instruction_opcode_raise__nR = 716,
    uninspired_specialized_instruction_opcode_random___rr0__retR = 717,
    uninspired_specialized_instruction_opcode_random___rrR__retR = 718,
    uninspired_specialized_instruction_opcode_reverse__n0 = 719,
    uninspired_specialized_instruction_opcode_reverse__n1 = 720,
    uninspired_specialized_instruction_opcode_reverse__n2 = 721,
    uninspired_specialized_instruction_opcode_reverse__n3 = 722,
    uninspired_specialized_instruction_opcode_reverse__n4 = 723,
    uninspired_specialized_instruction_opcode_reverse__n5 = 724,
    uninspired_specialized_instruction_opcode_reverse__n6 = 725,
    uninspired_specialized_instruction_opcode_reverse__n7 = 726,
    uninspired_specialized_instruction_opcode_reverse__n8 = 727,
    uninspired_specialized_instruction_opcode_reverse__n9 = 728,
    uninspired_specialized_instruction_opcode_reverse__n10 = 729,
    uninspired_specialized_instruction_opcode_reverse__n11 = 730,
    uninspired_specialized_instruction_opcode_reverse__n12 = 731,
    uninspired_specialized_instruction_opcode_reverse__n13 = 732,
    uninspired_specialized_instruction_opcode_reverse__n14 = 733,
    uninspired_specialized_instruction_opcode_reverse__n15 = 734,
    uninspired_specialized_instruction_opcode_reverse__n16 = 735,
    uninspired_specialized_instruction_opcode_reverse__n17 = 736,
    uninspired_specialized_instruction_opcode_reverse__n18 = 737,
    uninspired_specialized_instruction_opcode_reverse__n19 = 738,
    uninspired_specialized_instruction_opcode_reverse__n20 = 739,
    uninspired_specialized_instruction_opcode_reverse__n21 = 740,
    uninspired_specialized_instruction_opcode_reverse__n22 = 741,
    uninspired_specialized_instruction_opcode_reverse__n23 = 742,
    uninspired_specialized_instruction_opcode_reverse__n24 = 743,
    uninspired_specialized_instruction_opcode_reverse__n25 = 744,
    uninspired_specialized_instruction_opcode_reverse__n26 = 745,
    uninspired_specialized_instruction_opcode_reverse__n27 = 746,
    uninspired_specialized_instruction_opcode_reverse__n28 = 747,
    uninspired_specialized_instruction_opcode_reverse__n29 = 748,
    uninspired_specialized_instruction_opcode_reverse__n30 = 749,
    uninspired_specialized_instruction_opcode_reverse__n31 = 750,
    uninspired_specialized_instruction_opcode_reverse__n32 = 751,
    uninspired_specialized_instruction_opcode_reverse__n33 = 752,
    uninspired_specialized_instruction_opcode_reverse__n34 = 753,
    uninspired_specialized_instruction_opcode_reverse__n35 = 754,
    uninspired_specialized_instruction_opcode_reverse__n36 = 755,
    uninspired_specialized_instruction_opcode_reverse__n37 = 756,
    uninspired_specialized_instruction_opcode_reverse__n38 = 757,
    uninspired_specialized_instruction_opcode_reverse__n39 = 758,
    uninspired_specialized_instruction_opcode_reverse__n40 = 759,
    uninspired_specialized_instruction_opcode_reverse__n41 = 760,
    uninspired_specialized_instruction_opcode_reverse__n42 = 761,
    uninspired_specialized_instruction_opcode_reverse__n43 = 762,
    uninspired_specialized_instruction_opcode_reverse__n44 = 763,
    uninspired_specialized_instruction_opcode_reverse__n45 = 764,
    uninspired_specialized_instruction_opcode_reverse__n46 = 765,
    uninspired_specialized_instruction_opcode_reverse__n47 = 766,
    uninspired_specialized_instruction_opcode_reverse__n48 = 767,
    uninspired_specialized_instruction_opcode_reverse__n49 = 768,
    uninspired_specialized_instruction_opcode_reverse__n50 = 769,
    uninspired_specialized_instruction_opcode_reverse__n51 = 770,
    uninspired_specialized_instruction_opcode_reverse__n52 = 771,
    uninspired_specialized_instruction_opcode_reverse__n53 = 772,
    uninspired_specialized_instruction_opcode_reverse__n54 = 773,
    uninspired_specialized_instruction_opcode_reverse__n55 = 774,
    uninspired_specialized_instruction_opcode_reverse__n56 = 775,
    uninspired_specialized_instruction_opcode_reverse__n57 = 776,
    uninspired_specialized_instruction_opcode_reverse__n58 = 777,
    uninspired_specialized_instruction_opcode_reverse__n59 = 778,
    uninspired_specialized_instruction_opcode_reverse__n60 = 779,
    uninspired_specialized_instruction_opcode_reverse__n61 = 780,
    uninspired_specialized_instruction_opcode_reverse__n62 = 781,
    uninspired_specialized_instruction_opcode_reverse__n63 = 782,
    uninspired_specialized_instruction_opcode_reverse__n64 = 783,
    uninspired_specialized_instruction_opcode_reverse__n65 = 784,
    uninspired_specialized_instruction_opcode_reverse__n66 = 785,
    uninspired_specialized_instruction_opcode_reverse__n67 = 786,
    uninspired_specialized_instruction_opcode_reverse__n68 = 787,
    uninspired_specialized_instruction_opcode_reverse__n69 = 788,
    uninspired_specialized_instruction_opcode_reverse__n70 = 789,
    uninspired_specialized_instruction_opcode_reverse__nR = 790,
    uninspired_specialized_instruction_opcode_roll__n0 = 791,
    uninspired_specialized_instruction_opcode_roll__n1 = 792,
    uninspired_specialized_instruction_opcode_roll__n2 = 793,
    uninspired_specialized_instruction_opcode_roll__n3 = 794,
    uninspired_specialized_instruction_opcode_roll__n4 = 795,
    uninspired_specialized_instruction_opcode_roll__n5 = 796,
    uninspired_specialized_instruction_opcode_roll__n6 = 797,
    uninspired_specialized_instruction_opcode_roll__n7 = 798,
    uninspired_specialized_instruction_opcode_roll__n8 = 799,
    uninspired_specialized_instruction_opcode_roll__n9 = 800,
    uninspired_specialized_instruction_opcode_roll__n10 = 801,
    uninspired_specialized_instruction_opcode_roll__n11 = 802,
    uninspired_specialized_instruction_opcode_roll__n12 = 803,
    uninspired_specialized_instruction_opcode_roll__n13 = 804,
    uninspired_specialized_instruction_opcode_roll__n14 = 805,
    uninspired_specialized_instruction_opcode_roll__n15 = 806,
    uninspired_specialized_instruction_opcode_roll__n16 = 807,
    uninspired_specialized_instruction_opcode_roll__n17 = 808,
    uninspired_specialized_instruction_opcode_roll__n18 = 809,
    uninspired_specialized_instruction_opcode_roll__n19 = 810,
    uninspired_specialized_instruction_opcode_roll__n20 = 811,
    uninspired_specialized_instruction_opcode_roll__n21 = 812,
    uninspired_specialized_instruction_opcode_roll__n22 = 813,
    uninspired_specialized_instruction_opcode_roll__n23 = 814,
    uninspired_specialized_instruction_opcode_roll__n24 = 815,
    uninspired_specialized_instruction_opcode_roll__n25 = 816,
    uninspired_specialized_instruction_opcode_roll__n26 = 817,
    uninspired_specialized_instruction_opcode_roll__n27 = 818,
    uninspired_specialized_instruction_opcode_roll__n28 = 819,
    uninspired_specialized_instruction_opcode_roll__n29 = 820,
    uninspired_specialized_instruction_opcode_roll__n30 = 821,
    uninspired_specialized_instruction_opcode_roll__n31 = 822,
    uninspired_specialized_instruction_opcode_roll__n32 = 823,
    uninspired_specialized_instruction_opcode_roll__n33 = 824,
    uninspired_specialized_instruction_opcode_roll__n34 = 825,
    uninspired_specialized_instruction_opcode_roll__n35 = 826,
    uninspired_specialized_instruction_opcode_roll__n36 = 827,
    uninspired_specialized_instruction_opcode_roll__n37 = 828,
    uninspired_specialized_instruction_opcode_roll__n38 = 829,
    uninspired_specialized_instruction_opcode_roll__n39 = 830,
    uninspired_specialized_instruction_opcode_roll__n40 = 831,
    uninspired_specialized_instruction_opcode_roll__n41 = 832,
    uninspired_specialized_instruction_opcode_roll__n42 = 833,
    uninspired_specialized_instruction_opcode_roll__n43 = 834,
    uninspired_specialized_instruction_opcode_roll__n44 = 835,
    uninspired_specialized_instruction_opcode_roll__n45 = 836,
    uninspired_specialized_instruction_opcode_roll__n46 = 837,
    uninspired_specialized_instruction_opcode_roll__n47 = 838,
    uninspired_specialized_instruction_opcode_roll__n48 = 839,
    uninspired_specialized_instruction_opcode_roll__n49 = 840,
    uninspired_specialized_instruction_opcode_roll__n50 = 841,
    uninspired_specialized_instruction_opcode_roll__n51 = 842,
    uninspired_specialized_instruction_opcode_roll__n52 = 843,
    uninspired_specialized_instruction_opcode_roll__n53 = 844,
    uninspired_specialized_instruction_opcode_roll__n54 = 845,
    uninspired_specialized_instruction_opcode_roll__n55 = 846,
    uninspired_specialized_instruction_opcode_roll__n56 = 847,
    uninspired_specialized_instruction_opcode_roll__n57 = 848,
    uninspired_specialized_instruction_opcode_roll__n58 = 849,
    uninspired_specialized_instruction_opcode_roll__n59 = 850,
    uninspired_specialized_instruction_opcode_roll__n60 = 851,
    uninspired_specialized_instruction_opcode_roll__n61 = 852,
    uninspired_specialized_instruction_opcode_roll__n62 = 853,
    uninspired_specialized_instruction_opcode_roll__n63 = 854,
    uninspired_specialized_instruction_opcode_roll__n64 = 855,
    uninspired_specialized_instruction_opcode_roll__n65 = 856,
    uninspired_specialized_instruction_opcode_roll__n66 = 857,
    uninspired_specialized_instruction_opcode_roll__n67 = 858,
    uninspired_specialized_instruction_opcode_roll__n68 = 859,
    uninspired_specialized_instruction_opcode_roll__n69 = 860,
    uninspired_specialized_instruction_opcode_roll__n70 = 861,
    uninspired_specialized_instruction_opcode_roll__nR = 862,
    uninspired_specialized_instruction_opcode_rollforth = 863,
    uninspired_specialized_instruction_opcode_rot = 864,
    uninspired_specialized_instruction_opcode_safe_mpoint__fR = 865,
    uninspired_specialized_instruction_opcode_set_mpending = 866,
    uninspired_specialized_instruction_opcode_slide__n1__n1 = 867,
    uninspired_specialized_instruction_opcode_slide__n1__n2 = 868,
    uninspired_specialized_instruction_opcode_slide__n1__n3 = 869,
    uninspired_specialized_instruction_opcode_slide__n1__n4 = 870,
    uninspired_specialized_instruction_opcode_slide__n1__n5 = 871,
    uninspired_specialized_instruction_opcode_slide__n1__nR = 872,
    uninspired_specialized_instruction_opcode_slide__n2__n1 = 873,
    uninspired_specialized_instruction_opcode_slide__n2__n2 = 874,
    uninspired_specialized_instruction_opcode_slide__n2__n3 = 875,
    uninspired_specialized_instruction_opcode_slide__n2__n4 = 876,
    uninspired_specialized_instruction_opcode_slide__n2__n5 = 877,
    uninspired_specialized_instruction_opcode_slide__n2__nR = 878,
    uninspired_specialized_instruction_opcode_slide__n3__n1 = 879,
    uninspired_specialized_instruction_opcode_slide__n3__n2 = 880,
    uninspired_specialized_instruction_opcode_slide__n3__n3 = 881,
    uninspired_specialized_instruction_opcode_slide__n3__n4 = 882,
    uninspired_specialized_instruction_opcode_slide__n3__n5 = 883,
    uninspired_specialized_instruction_opcode_slide__n3__nR = 884,
    uninspired_specialized_instruction_opcode_slide__nR__n1 = 885,
    uninspired_specialized_instruction_opcode_slide__nR__n2 = 886,
    uninspired_specialized_instruction_opcode_slide__nR__n3 = 887,
    uninspired_specialized_instruction_opcode_slide__nR__n4 = 888,
    uninspired_specialized_instruction_opcode_slide__nR__n5 = 889,
    uninspired_specialized_instruction_opcode_slide__nR__nR = 890,
    uninspired_specialized_instruction_opcode_slideforth = 891,
    uninspired_specialized_instruction_opcode_stackdrop = 892,
    uninspired_specialized_instruction_opcode_stackdup = 893,
    uninspired_specialized_instruction_opcode_stackif__fR = 894,
    uninspired_specialized_instruction_opcode_stacknip = 895,
    uninspired_specialized_instruction_opcode_stacknondroppingif__fR = 896,
    uninspired_specialized_instruction_opcode_stacknot = 897,
    uninspired_specialized_instruction_opcode_stackoneminus = 898,
    uninspired_specialized_instruction_opcode_stackoneplus = 899,
    uninspired_specialized_instruction_opcode_stackover = 900,
    uninspired_specialized_instruction_opcode_stackpeek___rr0 = 901,
    uninspired_specialized_instruction_opcode_stackpeek___rrR = 902,
    uninspired_specialized_instruction_opcode_stackplus = 903,
    uninspired_specialized_instruction_opcode_stackplusr___rr0 = 904,
    uninspired_specialized_instruction_opcode_stackplusr___rrR = 905,
    uninspired_specialized_instruction_opcode_stackpop___rr0 = 906,
    uninspired_specialized_instruction_opcode_stackpop___rrR = 907,
    uninspired_specialized_instruction_opcode_stackprint__retR = 908,
    uninspired_specialized_instruction_opcode_stackpush___rr0 = 909,
    uninspired_specialized_instruction_opcode_stackpush___rrR = 910,
    uninspired_specialized_instruction_opcode_stackpush__n0 = 911,
    uninspired_specialized_instruction_opcode_stackpush__n1 = 912,
    uninspired_specialized_instruction_opcode_stackpush__nR = 913,
    uninspired_specialized_instruction_opcode_stackpush__lR = 914,
    uninspired_specialized_instruction_opcode_stackpushunspecified = 915,
    uninspired_specialized_instruction_opcode_stackset___rr0 = 916,
    uninspired_specialized_instruction_opcode_stackset___rrR = 917,
    uninspired_specialized_instruction_opcode_stackset__n0 = 918,
    uninspired_specialized_instruction_opcode_stackset__n1 = 919,
    uninspired_specialized_instruction_opcode_stackset__nR = 920,
    uninspired_specialized_instruction_opcode_stackset__lR = 921,
    uninspired_specialized_instruction_opcode_stackswap = 922,
    uninspired_specialized_instruction_opcode_stackswaptop___rr0 = 923,
    uninspired_specialized_instruction_opcode_stackswaptop___rrR = 924,
    uninspired_specialized_instruction_opcode_stacktimes = 925,
    uninspired_specialized_instruction_opcode_storewithbyteoffset___rr0___rr0___rr0 = 926,
    uninspired_specialized_instruction_opcode_storewithbyteoffset___rr0___rr0___rrR = 927,
    uninspired_specialized_instruction_opcode_storewithbyteoffset___rr0___rr0__n0 = 928,
    uninspired_specialized_instruction_opcode_storewithbyteoffset___rr0___rr0__n8 = 929,
    uninspired_specialized_instruction_opcode_storewithbyteoffset___rr0___rr0__nR = 930,
    uninspired_specialized_instruction_opcode_storewithbyteoffset___rr0___rrR___rr0 = 931,
    uninspired_specialized_instruction_opcode_storewithbyteoffset___rr0___rrR___rrR = 932,
    uninspired_specialized_instruction_opcode_storewithbyteoffset___rr0___rrR__n0 = 933,
    uninspired_specialized_instruction_opcode_storewithbyteoffset___rr0___rrR__n8 = 934,
    uninspired_specialized_instruction_opcode_storewithbyteoffset___rr0___rrR__nR = 935,
    uninspired_specialized_instruction_opcode_storewithbyteoffset___rrR___rr0___rr0 = 936,
    uninspired_specialized_instruction_opcode_storewithbyteoffset___rrR___rr0___rrR = 937,
    uninspired_specialized_instruction_opcode_storewithbyteoffset___rrR___rr0__n0 = 938,
    uninspired_specialized_instruction_opcode_storewithbyteoffset___rrR___rr0__n8 = 939,
    uninspired_specialized_instruction_opcode_storewithbyteoffset___rrR___rr0__nR = 940,
    uninspired_specialized_instruction_opcode_storewithbyteoffset___rrR___rrR___rr0 = 941,
    uninspired_specialized_instruction_opcode_storewithbyteoffset___rrR___rrR___rrR = 942,
    uninspired_specialized_instruction_opcode_storewithbyteoffset___rrR___rrR__n0 = 943,
    uninspired_specialized_instruction_opcode_storewithbyteoffset___rrR___rrR__n8 = 944,
    uninspired_specialized_instruction_opcode_storewithbyteoffset___rrR___rrR__nR = 945,
    uninspired_specialized_instruction_opcode_storewithbyteoffset__nR___rr0___rr0 = 946,
    uninspired_specialized_instruction_opcode_storewithbyteoffset__nR___rr0___rrR = 947,
    uninspired_specialized_instruction_opcode_storewithbyteoffset__nR___rr0__n0 = 948,
    uninspired_specialized_instruction_opcode_storewithbyteoffset__nR___rr0__n8 = 949,
    uninspired_specialized_instruction_opcode_storewithbyteoffset__nR___rr0__nR = 950,
    uninspired_specialized_instruction_opcode_storewithbyteoffset__nR___rrR___rr0 = 951,
    uninspired_specialized_instruction_opcode_storewithbyteoffset__nR___rrR___rrR = 952,
    uninspired_specialized_instruction_opcode_storewithbyteoffset__nR___rrR__n0 = 953,
    uninspired_specialized_instruction_opcode_storewithbyteoffset__nR___rrR__n8 = 954,
    uninspired_specialized_instruction_opcode_storewithbyteoffset__nR___rrR__nR = 955,
    uninspired_specialized_instruction_opcode_storewithwordoffset___rr0___rr0___rr0 = 956,
    uninspired_specialized_instruction_opcode_storewithwordoffset___rr0___rr0___rrR = 957,
    uninspired_specialized_instruction_opcode_storewithwordoffset___rr0___rr0__n0 = 958,
    uninspired_specialized_instruction_opcode_storewithwordoffset___rr0___rr0__n1 = 959,
    uninspired_specialized_instruction_opcode_storewithwordoffset___rr0___rr0__n2 = 960,
    uninspired_specialized_instruction_opcode_storewithwordoffset___rr0___rr0__nR = 961,
    uninspired_specialized_instruction_opcode_storewithwordoffset___rr0___rrR___rr0 = 962,
    uninspired_specialized_instruction_opcode_storewithwordoffset___rr0___rrR___rrR = 963,
    uninspired_specialized_instruction_opcode_storewithwordoffset___rr0___rrR__n0 = 964,
    uninspired_specialized_instruction_opcode_storewithwordoffset___rr0___rrR__n1 = 965,
    uninspired_specialized_instruction_opcode_storewithwordoffset___rr0___rrR__n2 = 966,
    uninspired_specialized_instruction_opcode_storewithwordoffset___rr0___rrR__nR = 967,
    uninspired_specialized_instruction_opcode_storewithwordoffset___rrR___rr0___rr0 = 968,
    uninspired_specialized_instruction_opcode_storewithwordoffset___rrR___rr0___rrR = 969,
    uninspired_specialized_instruction_opcode_storewithwordoffset___rrR___rr0__n0 = 970,
    uninspired_specialized_instruction_opcode_storewithwordoffset___rrR___rr0__n1 = 971,
    uninspired_specialized_instruction_opcode_storewithwordoffset___rrR___rr0__n2 = 972,
    uninspired_specialized_instruction_opcode_storewithwordoffset___rrR___rr0__nR = 973,
    uninspired_specialized_instruction_opcode_storewithwordoffset___rrR___rrR___rr0 = 974,
    uninspired_specialized_instruction_opcode_storewithwordoffset___rrR___rrR___rrR = 975,
    uninspired_specialized_instruction_opcode_storewithwordoffset___rrR___rrR__n0 = 976,
    uninspired_specialized_instruction_opcode_storewithwordoffset___rrR___rrR__n1 = 977,
    uninspired_specialized_instruction_opcode_storewithwordoffset___rrR___rrR__n2 = 978,
    uninspired_specialized_instruction_opcode_storewithwordoffset___rrR___rrR__nR = 979,
    uninspired_specialized_instruction_opcode_storewithwordoffset__nR___rr0___rr0 = 980,
    uninspired_specialized_instruction_opcode_storewithwordoffset__nR___rr0___rrR = 981,
    uninspired_specialized_instruction_opcode_storewithwordoffset__nR___rr0__n0 = 982,
    uninspired_specialized_instruction_opcode_storewithwordoffset__nR___rr0__n1 = 983,
    uninspired_specialized_instruction_opcode_storewithwordoffset__nR___rr0__n2 = 984,
    uninspired_specialized_instruction_opcode_storewithwordoffset__nR___rr0__nR = 985,
    uninspired_specialized_instruction_opcode_storewithwordoffset__nR___rrR___rr0 = 986,
    uninspired_specialized_instruction_opcode_storewithwordoffset__nR___rrR___rrR = 987,
    uninspired_specialized_instruction_opcode_storewithwordoffset__nR___rrR__n0 = 988,
    uninspired_specialized_instruction_opcode_storewithwordoffset__nR___rrR__n1 = 989,
    uninspired_specialized_instruction_opcode_storewithwordoffset__nR___rrR__n2 = 990,
    uninspired_specialized_instruction_opcode_storewithwordoffset__nR___rrR__nR = 991,
    uninspired_specialized_instruction_opcode_sub___rr0___rr0___rr0 = 992,
    uninspired_specialized_instruction_opcode_sub___rr0___rr0___rrR = 993,
    uninspired_specialized_instruction_opcode_sub___rr0___rrR___rr0 = 994,
    uninspired_specialized_instruction_opcode_sub___rr0___rrR___rrR = 995,
    uninspired_specialized_instruction_opcode_sub___rr0__n1___rr0 = 996,
    uninspired_specialized_instruction_opcode_sub___rr0__n1___rrR = 997,
    uninspired_specialized_instruction_opcode_sub___rr0__nR___rr0 = 998,
    uninspired_specialized_instruction_opcode_sub___rr0__nR___rrR = 999,
    uninspired_specialized_instruction_opcode_sub___rrR___rr0___rr0 = 1000,
    uninspired_specialized_instruction_opcode_sub___rrR___rr0___rrR = 1001,
    uninspired_specialized_instruction_opcode_sub___rrR___rrR___rr0 = 1002,
    uninspired_specialized_instruction_opcode_sub___rrR___rrR___rrR = 1003,
    uninspired_specialized_instruction_opcode_sub___rrR__n1___rr0 = 1004,
    uninspired_specialized_instruction_opcode_sub___rrR__n1___rrR = 1005,
    uninspired_specialized_instruction_opcode_sub___rrR__nR___rr0 = 1006,
    uninspired_specialized_instruction_opcode_sub___rrR__nR___rrR = 1007,
    uninspired_specialized_instruction_opcode_sub__n0___rr0___rr0 = 1008,
    uninspired_specialized_instruction_opcode_sub__n0___rr0___rrR = 1009,
    uninspired_specialized_instruction_opcode_sub__n0___rrR___rr0 = 1010,
    uninspired_specialized_instruction_opcode_sub__n0___rrR___rrR = 1011,
    uninspired_specialized_instruction_opcode_sub__n0__n1___rr0 = 1012,
    uninspired_specialized_instruction_opcode_sub__n0__n1___rrR = 1013,
    uninspired_specialized_instruction_opcode_sub__n0__nR___rr0 = 1014,
    uninspired_specialized_instruction_opcode_sub__n0__nR___rrR = 1015,
    uninspired_specialized_instruction_opcode_sub__nR___rr0___rr0 = 1016,
    uninspired_specialized_instruction_opcode_sub__nR___rr0___rrR = 1017,
    uninspired_specialized_instruction_opcode_sub__nR___rrR___rr0 = 1018,
    uninspired_specialized_instruction_opcode_sub__nR___rrR___rrR = 1019,
    uninspired_specialized_instruction_opcode_sub__nR__n1___rr0 = 1020,
    uninspired_specialized_instruction_opcode_sub__nR__n1___rrR = 1021,
    uninspired_specialized_instruction_opcode_sub__nR__nR___rr0 = 1022,
    uninspired_specialized_instruction_opcode_sub__nR__nR___rrR = 1023,
    uninspired_specialized_instruction_opcode_subo___rr0___rr0___rr0__fR = 1024,
    uninspired_specialized_instruction_opcode_subo___rr0___rr0___rrR__fR = 1025,
    uninspired_specialized_instruction_opcode_subo___rr0___rrR___rr0__fR = 1026,
    uninspired_specialized_instruction_opcode_subo___rr0___rrR___rrR__fR = 1027,
    uninspired_specialized_instruction_opcode_subo___rr0__n1___rr0__fR = 1028,
    uninspired_specialized_instruction_opcode_subo___rr0__n1___rrR__fR = 1029,
    uninspired_specialized_instruction_opcode_subo___rr0__nR___rr0__fR = 1030,
    uninspired_specialized_instruction_opcode_subo___rr0__nR___rrR__fR = 1031,
    uninspired_specialized_instruction_opcode_subo___rrR___rr0___rr0__fR = 1032,
    uninspired_specialized_instruction_opcode_subo___rrR___rr0___rrR__fR = 1033,
    uninspired_specialized_instruction_opcode_subo___rrR___rrR___rr0__fR = 1034,
    uninspired_specialized_instruction_opcode_subo___rrR___rrR___rrR__fR = 1035,
    uninspired_specialized_instruction_opcode_subo___rrR__n1___rr0__fR = 1036,
    uninspired_specialized_instruction_opcode_subo___rrR__n1___rrR__fR = 1037,
    uninspired_specialized_instruction_opcode_subo___rrR__nR___rr0__fR = 1038,
    uninspired_specialized_instruction_opcode_subo___rrR__nR___rrR__fR = 1039,
    uninspired_specialized_instruction_opcode_subo__nR___rr0___rr0__fR = 1040,
    uninspired_specialized_instruction_opcode_subo__nR___rr0___rrR__fR = 1041,
    uninspired_specialized_instruction_opcode_subo__nR___rrR___rr0__fR = 1042,
    uninspired_specialized_instruction_opcode_subo__nR___rrR___rrR__fR = 1043,
    uninspired_specialized_instruction_opcode_subo__nR__n1___rr0__fR = 1044,
    uninspired_specialized_instruction_opcode_subo__nR__n1___rrR__fR = 1045,
    uninspired_specialized_instruction_opcode_subo__nR__nR___rr0__fR = 1046,
    uninspired_specialized_instruction_opcode_subo__nR__nR___rrR__fR = 1047,
    uninspired_specialized_instruction_opcode_tuck = 1048,
    uninspired_specialized_instruction_opcode_unreachable = 1049,
    uninspired_specialized_instruction_opcode_whirl__n0 = 1050,
    uninspired_specialized_instruction_opcode_whirl__n1 = 1051,
    uninspired_specialized_instruction_opcode_whirl__n2 = 1052,
    uninspired_specialized_instruction_opcode_whirl__n3 = 1053,
    uninspired_specialized_instruction_opcode_whirl__nR = 1054,
    uninspired_specialized_instruction_opcode_whirlforth = 1055,
    uninspired_specialized_instruction_opcode__Aaddo___rr0___rr0___rr0__fR_A_mno_mfast_mbranches = 1056,
    uninspired_specialized_instruction_opcode__Aaddo___rr0___rr0___rrR__fR_A_mno_mfast_mbranches = 1057,
    uninspired_specialized_instruction_opcode__Aaddo___rr0___rrR___rr0__fR_A_mno_mfast_mbranches = 1058,
    uninspired_specialized_instruction_opcode__Aaddo___rr0___rrR___rrR__fR_A_mno_mfast_mbranches = 1059,
    uninspired_specialized_instruction_opcode__Aaddo___rr0__n1___rr0__fR_A_mno_mfast_mbranches = 1060,
    uninspired_specialized_instruction_opcode__Aaddo___rr0__n1___rrR__fR_A_mno_mfast_mbranches = 1061,
    uninspired_specialized_instruction_opcode__Aaddo___rr0__n_m1___rr0__fR_A_mno_mfast_mbranches = 1062,
    uninspired_specialized_instruction_opcode__Aaddo___rr0__n_m1___rrR__fR_A_mno_mfast_mbranches = 1063,
    uninspired_specialized_instruction_opcode__Aaddo___rr0__nR___rr0__fR_A_mno_mfast_mbranches = 1064,
    uninspired_specialized_instruction_opcode__Aaddo___rr0__nR___rrR__fR_A_mno_mfast_mbranches = 1065,
    uninspired_specialized_instruction_opcode__Aaddo___rrR___rr0___rr0__fR_A_mno_mfast_mbranches = 1066,
    uninspired_specialized_instruction_opcode__Aaddo___rrR___rr0___rrR__fR_A_mno_mfast_mbranches = 1067,
    uninspired_specialized_instruction_opcode__Aaddo___rrR___rrR___rr0__fR_A_mno_mfast_mbranches = 1068,
    uninspired_specialized_instruction_opcode__Aaddo___rrR___rrR___rrR__fR_A_mno_mfast_mbranches = 1069,
    uninspired_specialized_instruction_opcode__Aaddo___rrR__n1___rr0__fR_A_mno_mfast_mbranches = 1070,
    uninspired_specialized_instruction_opcode__Aaddo___rrR__n1___rrR__fR_A_mno_mfast_mbranches = 1071,
    uninspired_specialized_instruction_opcode__Aaddo___rrR__n_m1___rr0__fR_A_mno_mfast_mbranches = 1072,
    uninspired_specialized_instruction_opcode__Aaddo___rrR__n_m1___rrR__fR_A_mno_mfast_mbranches = 1073,
    uninspired_specialized_instruction_opcode__Aaddo___rrR__nR___rr0__fR_A_mno_mfast_mbranches = 1074,
    uninspired_specialized_instruction_opcode__Aaddo___rrR__nR___rrR__fR_A_mno_mfast_mbranches = 1075,
    uninspired_specialized_instruction_opcode__Aaddo__nR___rr0___rr0__fR_A_mno_mfast_mbranches = 1076,
    uninspired_specialized_instruction_opcode__Aaddo__nR___rr0___rrR__fR_A_mno_mfast_mbranches = 1077,
    uninspired_specialized_instruction_opcode__Aaddo__nR___rrR___rr0__fR_A_mno_mfast_mbranches = 1078,
    uninspired_specialized_instruction_opcode__Aaddo__nR___rrR___rrR__fR_A_mno_mfast_mbranches = 1079,
    uninspired_specialized_instruction_opcode__Aaddo__nR__n1___rr0__fR_A_mno_mfast_mbranches = 1080,
    uninspired_specialized_instruction_opcode__Aaddo__nR__n1___rrR__fR_A_mno_mfast_mbranches = 1081,
    uninspired_specialized_instruction_opcode__Aaddo__nR__n_m1___rr0__fR_A_mno_mfast_mbranches = 1082,
    uninspired_specialized_instruction_opcode__Aaddo__nR__n_m1___rrR__fR_A_mno_mfast_mbranches = 1083,
    uninspired_specialized_instruction_opcode__Aaddo__nR__nR___rr0__fR_A_mno_mfast_mbranches = 1084,
    uninspired_specialized_instruction_opcode__Aaddo__nR__nR___rrR__fR_A_mno_mfast_mbranches = 1085,
    uninspired_specialized_instruction_opcode__Ab__fR_A_mno_mfast_mbranches = 1086,
    uninspired_specialized_instruction_opcode__Aband___rr0___rr0__fR_A_mno_mfast_mbranches = 1087,
    uninspired_specialized_instruction_opcode__Aband___rr0___rrR__fR_A_mno_mfast_mbranches = 1088,
    uninspired_specialized_instruction_opcode__Aband___rr0__n3__fR_A_mno_mfast_mbranches = 1089,
    uninspired_specialized_instruction_opcode__Aband___rr0__n7__fR_A_mno_mfast_mbranches = 1090,
    uninspired_specialized_instruction_opcode__Aband___rr0__nR__fR_A_mno_mfast_mbranches = 1091,
    uninspired_specialized_instruction_opcode__Aband___rrR___rr0__fR_A_mno_mfast_mbranches = 1092,
    uninspired_specialized_instruction_opcode__Aband___rrR___rrR__fR_A_mno_mfast_mbranches = 1093,
    uninspired_specialized_instruction_opcode__Aband___rrR__n3__fR_A_mno_mfast_mbranches = 1094,
    uninspired_specialized_instruction_opcode__Aband___rrR__n7__fR_A_mno_mfast_mbranches = 1095,
    uninspired_specialized_instruction_opcode__Aband___rrR__nR__fR_A_mno_mfast_mbranches = 1096,
    uninspired_specialized_instruction_opcode__Aband__nR___rr0__fR_A_mno_mfast_mbranches = 1097,
    uninspired_specialized_instruction_opcode__Aband__nR___rrR__fR_A_mno_mfast_mbranches = 1098,
    uninspired_specialized_instruction_opcode__Aband__nR__n3__fR_A_mno_mfast_mbranches = 1099,
    uninspired_specialized_instruction_opcode__Aband__nR__n7__fR_A_mno_mfast_mbranches = 1100,
    uninspired_specialized_instruction_opcode__Aband__nR__nR__fR_A_mno_mfast_mbranches = 1101,
    uninspired_specialized_instruction_opcode__Abeq___rr0___rr0__fR_A_mno_mfast_mbranches = 1102,
    uninspired_specialized_instruction_opcode__Abeq___rr0___rrR__fR_A_mno_mfast_mbranches = 1103,
    uninspired_specialized_instruction_opcode__Abeq___rr0__nR__fR_A_mno_mfast_mbranches = 1104,
    uninspired_specialized_instruction_opcode__Abeq___rrR___rr0__fR_A_mno_mfast_mbranches = 1105,
    uninspired_specialized_instruction_opcode__Abeq___rrR___rrR__fR_A_mno_mfast_mbranches = 1106,
    uninspired_specialized_instruction_opcode__Abeq___rrR__nR__fR_A_mno_mfast_mbranches = 1107,
    uninspired_specialized_instruction_opcode__Abeq__nR___rr0__fR_A_mno_mfast_mbranches = 1108,
    uninspired_specialized_instruction_opcode__Abeq__nR___rrR__fR_A_mno_mfast_mbranches = 1109,
    uninspired_specialized_instruction_opcode__Abeq__nR__nR__fR_A_mno_mfast_mbranches = 1110,
    uninspired_specialized_instruction_opcode__Abge___rr0___rr0__fR_A_mno_mfast_mbranches = 1111,
    uninspired_specialized_instruction_opcode__Abge___rr0___rrR__fR_A_mno_mfast_mbranches = 1112,
    uninspired_specialized_instruction_opcode__Abge___rr0__n0__fR_A_mno_mfast_mbranches = 1113,
    uninspired_specialized_instruction_opcode__Abge___rr0__n1__fR_A_mno_mfast_mbranches = 1114,
    uninspired_specialized_instruction_opcode__Abge___rr0__nR__fR_A_mno_mfast_mbranches = 1115,
    uninspired_specialized_instruction_opcode__Abge___rrR___rr0__fR_A_mno_mfast_mbranches = 1116,
    uninspired_specialized_instruction_opcode__Abge___rrR___rrR__fR_A_mno_mfast_mbranches = 1117,
    uninspired_specialized_instruction_opcode__Abge___rrR__n0__fR_A_mno_mfast_mbranches = 1118,
    uninspired_specialized_instruction_opcode__Abge___rrR__n1__fR_A_mno_mfast_mbranches = 1119,
    uninspired_specialized_instruction_opcode__Abge___rrR__nR__fR_A_mno_mfast_mbranches = 1120,
    uninspired_specialized_instruction_opcode__Abge__n0___rr0__fR_A_mno_mfast_mbranches = 1121,
    uninspired_specialized_instruction_opcode__Abge__n0___rrR__fR_A_mno_mfast_mbranches = 1122,
    uninspired_specialized_instruction_opcode__Abge__n0__n0__fR_A_mno_mfast_mbranches = 1123,
    uninspired_specialized_instruction_opcode__Abge__n0__n1__fR_A_mno_mfast_mbranches = 1124,
    uninspired_specialized_instruction_opcode__Abge__n0__nR__fR_A_mno_mfast_mbranches = 1125,
    uninspired_specialized_instruction_opcode__Abge__n1___rr0__fR_A_mno_mfast_mbranches = 1126,
    uninspired_specialized_instruction_opcode__Abge__n1___rrR__fR_A_mno_mfast_mbranches = 1127,
    uninspired_specialized_instruction_opcode__Abge__n1__n0__fR_A_mno_mfast_mbranches = 1128,
    uninspired_specialized_instruction_opcode__Abge__n1__n1__fR_A_mno_mfast_mbranches = 1129,
    uninspired_specialized_instruction_opcode__Abge__n1__nR__fR_A_mno_mfast_mbranches = 1130,
    uninspired_specialized_instruction_opcode__Abge__nR___rr0__fR_A_mno_mfast_mbranches = 1131,
    uninspired_specialized_instruction_opcode__Abge__nR___rrR__fR_A_mno_mfast_mbranches = 1132,
    uninspired_specialized_instruction_opcode__Abge__nR__n0__fR_A_mno_mfast_mbranches = 1133,
    uninspired_specialized_instruction_opcode__Abge__nR__n1__fR_A_mno_mfast_mbranches = 1134,
    uninspired_specialized_instruction_opcode__Abge__nR__nR__fR_A_mno_mfast_mbranches = 1135,
    uninspired_specialized_instruction_opcode__Abgeu___rr0___rr0__fR_A_mno_mfast_mbranches = 1136,
    uninspired_specialized_instruction_opcode__Abgeu___rr0___rrR__fR_A_mno_mfast_mbranches = 1137,
    uninspired_specialized_instruction_opcode__Abgeu___rr0__n0__fR_A_mno_mfast_mbranches = 1138,
    uninspired_specialized_instruction_opcode__Abgeu___rr0__n1__fR_A_mno_mfast_mbranches = 1139,
    uninspired_specialized_instruction_opcode__Abgeu___rr0__nR__fR_A_mno_mfast_mbranches = 1140,
    uninspired_specialized_instruction_opcode__Abgeu___rrR___rr0__fR_A_mno_mfast_mbranches = 1141,
    uninspired_specialized_instruction_opcode__Abgeu___rrR___rrR__fR_A_mno_mfast_mbranches = 1142,
    uninspired_specialized_instruction_opcode__Abgeu___rrR__n0__fR_A_mno_mfast_mbranches = 1143,
    uninspired_specialized_instruction_opcode__Abgeu___rrR__n1__fR_A_mno_mfast_mbranches = 1144,
    uninspired_specialized_instruction_opcode__Abgeu___rrR__nR__fR_A_mno_mfast_mbranches = 1145,
    uninspired_specialized_instruction_opcode__Abgeu__n0___rr0__fR_A_mno_mfast_mbranches = 1146,
    uninspired_specialized_instruction_opcode__Abgeu__n0___rrR__fR_A_mno_mfast_mbranches = 1147,
    uninspired_specialized_instruction_opcode__Abgeu__n0__n0__fR_A_mno_mfast_mbranches = 1148,
    uninspired_specialized_instruction_opcode__Abgeu__n0__n1__fR_A_mno_mfast_mbranches = 1149,
    uninspired_specialized_instruction_opcode__Abgeu__n0__nR__fR_A_mno_mfast_mbranches = 1150,
    uninspired_specialized_instruction_opcode__Abgeu__n1___rr0__fR_A_mno_mfast_mbranches = 1151,
    uninspired_specialized_instruction_opcode__Abgeu__n1___rrR__fR_A_mno_mfast_mbranches = 1152,
    uninspired_specialized_instruction_opcode__Abgeu__n1__n0__fR_A_mno_mfast_mbranches = 1153,
    uninspired_specialized_instruction_opcode__Abgeu__n1__n1__fR_A_mno_mfast_mbranches = 1154,
    uninspired_specialized_instruction_opcode__Abgeu__n1__nR__fR_A_mno_mfast_mbranches = 1155,
    uninspired_specialized_instruction_opcode__Abgeu__nR___rr0__fR_A_mno_mfast_mbranches = 1156,
    uninspired_specialized_instruction_opcode__Abgeu__nR___rrR__fR_A_mno_mfast_mbranches = 1157,
    uninspired_specialized_instruction_opcode__Abgeu__nR__n0__fR_A_mno_mfast_mbranches = 1158,
    uninspired_specialized_instruction_opcode__Abgeu__nR__n1__fR_A_mno_mfast_mbranches = 1159,
    uninspired_specialized_instruction_opcode__Abgeu__nR__nR__fR_A_mno_mfast_mbranches = 1160,
    uninspired_specialized_instruction_opcode__Abgt___rr0___rr0__fR_A_mno_mfast_mbranches = 1161,
    uninspired_specialized_instruction_opcode__Abgt___rr0___rrR__fR_A_mno_mfast_mbranches = 1162,
    uninspired_specialized_instruction_opcode__Abgt___rr0__n0__fR_A_mno_mfast_mbranches = 1163,
    uninspired_specialized_instruction_opcode__Abgt___rr0__n1__fR_A_mno_mfast_mbranches = 1164,
    uninspired_specialized_instruction_opcode__Abgt___rr0__nR__fR_A_mno_mfast_mbranches = 1165,
    uninspired_specialized_instruction_opcode__Abgt___rrR___rr0__fR_A_mno_mfast_mbranches = 1166,
    uninspired_specialized_instruction_opcode__Abgt___rrR___rrR__fR_A_mno_mfast_mbranches = 1167,
    uninspired_specialized_instruction_opcode__Abgt___rrR__n0__fR_A_mno_mfast_mbranches = 1168,
    uninspired_specialized_instruction_opcode__Abgt___rrR__n1__fR_A_mno_mfast_mbranches = 1169,
    uninspired_specialized_instruction_opcode__Abgt___rrR__nR__fR_A_mno_mfast_mbranches = 1170,
    uninspired_specialized_instruction_opcode__Abgt__n0___rr0__fR_A_mno_mfast_mbranches = 1171,
    uninspired_specialized_instruction_opcode__Abgt__n0___rrR__fR_A_mno_mfast_mbranches = 1172,
    uninspired_specialized_instruction_opcode__Abgt__n0__n0__fR_A_mno_mfast_mbranches = 1173,
    uninspired_specialized_instruction_opcode__Abgt__n0__n1__fR_A_mno_mfast_mbranches = 1174,
    uninspired_specialized_instruction_opcode__Abgt__n0__nR__fR_A_mno_mfast_mbranches = 1175,
    uninspired_specialized_instruction_opcode__Abgt__n1___rr0__fR_A_mno_mfast_mbranches = 1176,
    uninspired_specialized_instruction_opcode__Abgt__n1___rrR__fR_A_mno_mfast_mbranches = 1177,
    uninspired_specialized_instruction_opcode__Abgt__n1__n0__fR_A_mno_mfast_mbranches = 1178,
    uninspired_specialized_instruction_opcode__Abgt__n1__n1__fR_A_mno_mfast_mbranches = 1179,
    uninspired_specialized_instruction_opcode__Abgt__n1__nR__fR_A_mno_mfast_mbranches = 1180,
    uninspired_specialized_instruction_opcode__Abgt__nR___rr0__fR_A_mno_mfast_mbranches = 1181,
    uninspired_specialized_instruction_opcode__Abgt__nR___rrR__fR_A_mno_mfast_mbranches = 1182,
    uninspired_specialized_instruction_opcode__Abgt__nR__n0__fR_A_mno_mfast_mbranches = 1183,
    uninspired_specialized_instruction_opcode__Abgt__nR__n1__fR_A_mno_mfast_mbranches = 1184,
    uninspired_specialized_instruction_opcode__Abgt__nR__nR__fR_A_mno_mfast_mbranches = 1185,
    uninspired_specialized_instruction_opcode__Abgtu___rr0___rr0__fR_A_mno_mfast_mbranches = 1186,
    uninspired_specialized_instruction_opcode__Abgtu___rr0___rrR__fR_A_mno_mfast_mbranches = 1187,
    uninspired_specialized_instruction_opcode__Abgtu___rr0__n0__fR_A_mno_mfast_mbranches = 1188,
    uninspired_specialized_instruction_opcode__Abgtu___rr0__n1__fR_A_mno_mfast_mbranches = 1189,
    uninspired_specialized_instruction_opcode__Abgtu___rr0__nR__fR_A_mno_mfast_mbranches = 1190,
    uninspired_specialized_instruction_opcode__Abgtu___rrR___rr0__fR_A_mno_mfast_mbranches = 1191,
    uninspired_specialized_instruction_opcode__Abgtu___rrR___rrR__fR_A_mno_mfast_mbranches = 1192,
    uninspired_specialized_instruction_opcode__Abgtu___rrR__n0__fR_A_mno_mfast_mbranches = 1193,
    uninspired_specialized_instruction_opcode__Abgtu___rrR__n1__fR_A_mno_mfast_mbranches = 1194,
    uninspired_specialized_instruction_opcode__Abgtu___rrR__nR__fR_A_mno_mfast_mbranches = 1195,
    uninspired_specialized_instruction_opcode__Abgtu__n0___rr0__fR_A_mno_mfast_mbranches = 1196,
    uninspired_specialized_instruction_opcode__Abgtu__n0___rrR__fR_A_mno_mfast_mbranches = 1197,
    uninspired_specialized_instruction_opcode__Abgtu__n0__n0__fR_A_mno_mfast_mbranches = 1198,
    uninspired_specialized_instruction_opcode__Abgtu__n0__n1__fR_A_mno_mfast_mbranches = 1199,
    uninspired_specialized_instruction_opcode__Abgtu__n0__nR__fR_A_mno_mfast_mbranches = 1200,
    uninspired_specialized_instruction_opcode__Abgtu__n1___rr0__fR_A_mno_mfast_mbranches = 1201,
    uninspired_specialized_instruction_opcode__Abgtu__n1___rrR__fR_A_mno_mfast_mbranches = 1202,
    uninspired_specialized_instruction_opcode__Abgtu__n1__n0__fR_A_mno_mfast_mbranches = 1203,
    uninspired_specialized_instruction_opcode__Abgtu__n1__n1__fR_A_mno_mfast_mbranches = 1204,
    uninspired_specialized_instruction_opcode__Abgtu__n1__nR__fR_A_mno_mfast_mbranches = 1205,
    uninspired_specialized_instruction_opcode__Abgtu__nR___rr0__fR_A_mno_mfast_mbranches = 1206,
    uninspired_specialized_instruction_opcode__Abgtu__nR___rrR__fR_A_mno_mfast_mbranches = 1207,
    uninspired_specialized_instruction_opcode__Abgtu__nR__n0__fR_A_mno_mfast_mbranches = 1208,
    uninspired_specialized_instruction_opcode__Abgtu__nR__n1__fR_A_mno_mfast_mbranches = 1209,
    uninspired_specialized_instruction_opcode__Abgtu__nR__nR__fR_A_mno_mfast_mbranches = 1210,
    uninspired_specialized_instruction_opcode__Able___rr0___rr0__fR_A_mno_mfast_mbranches = 1211,
    uninspired_specialized_instruction_opcode__Able___rr0___rrR__fR_A_mno_mfast_mbranches = 1212,
    uninspired_specialized_instruction_opcode__Able___rr0__n0__fR_A_mno_mfast_mbranches = 1213,
    uninspired_specialized_instruction_opcode__Able___rr0__n1__fR_A_mno_mfast_mbranches = 1214,
    uninspired_specialized_instruction_opcode__Able___rr0__nR__fR_A_mno_mfast_mbranches = 1215,
    uninspired_specialized_instruction_opcode__Able___rrR___rr0__fR_A_mno_mfast_mbranches = 1216,
    uninspired_specialized_instruction_opcode__Able___rrR___rrR__fR_A_mno_mfast_mbranches = 1217,
    uninspired_specialized_instruction_opcode__Able___rrR__n0__fR_A_mno_mfast_mbranches = 1218,
    uninspired_specialized_instruction_opcode__Able___rrR__n1__fR_A_mno_mfast_mbranches = 1219,
    uninspired_specialized_instruction_opcode__Able___rrR__nR__fR_A_mno_mfast_mbranches = 1220,
    uninspired_specialized_instruction_opcode__Able__n0___rr0__fR_A_mno_mfast_mbranches = 1221,
    uninspired_specialized_instruction_opcode__Able__n0___rrR__fR_A_mno_mfast_mbranches = 1222,
    uninspired_specialized_instruction_opcode__Able__n0__n0__fR_A_mno_mfast_mbranches = 1223,
    uninspired_specialized_instruction_opcode__Able__n0__n1__fR_A_mno_mfast_mbranches = 1224,
    uninspired_specialized_instruction_opcode__Able__n0__nR__fR_A_mno_mfast_mbranches = 1225,
    uninspired_specialized_instruction_opcode__Able__n1___rr0__fR_A_mno_mfast_mbranches = 1226,
    uninspired_specialized_instruction_opcode__Able__n1___rrR__fR_A_mno_mfast_mbranches = 1227,
    uninspired_specialized_instruction_opcode__Able__n1__n0__fR_A_mno_mfast_mbranches = 1228,
    uninspired_specialized_instruction_opcode__Able__n1__n1__fR_A_mno_mfast_mbranches = 1229,
    uninspired_specialized_instruction_opcode__Able__n1__nR__fR_A_mno_mfast_mbranches = 1230,
    uninspired_specialized_instruction_opcode__Able__nR___rr0__fR_A_mno_mfast_mbranches = 1231,
    uninspired_specialized_instruction_opcode__Able__nR___rrR__fR_A_mno_mfast_mbranches = 1232,
    uninspired_specialized_instruction_opcode__Able__nR__n0__fR_A_mno_mfast_mbranches = 1233,
    uninspired_specialized_instruction_opcode__Able__nR__n1__fR_A_mno_mfast_mbranches = 1234,
    uninspired_specialized_instruction_opcode__Able__nR__nR__fR_A_mno_mfast_mbranches = 1235,
    uninspired_specialized_instruction_opcode__Ableu___rr0___rr0__fR_A_mno_mfast_mbranches = 1236,
    uninspired_specialized_instruction_opcode__Ableu___rr0___rrR__fR_A_mno_mfast_mbranches = 1237,
    uninspired_specialized_instruction_opcode__Ableu___rr0__n0__fR_A_mno_mfast_mbranches = 1238,
    uninspired_specialized_instruction_opcode__Ableu___rr0__n1__fR_A_mno_mfast_mbranches = 1239,
    uninspired_specialized_instruction_opcode__Ableu___rr0__nR__fR_A_mno_mfast_mbranches = 1240,
    uninspired_specialized_instruction_opcode__Ableu___rrR___rr0__fR_A_mno_mfast_mbranches = 1241,
    uninspired_specialized_instruction_opcode__Ableu___rrR___rrR__fR_A_mno_mfast_mbranches = 1242,
    uninspired_specialized_instruction_opcode__Ableu___rrR__n0__fR_A_mno_mfast_mbranches = 1243,
    uninspired_specialized_instruction_opcode__Ableu___rrR__n1__fR_A_mno_mfast_mbranches = 1244,
    uninspired_specialized_instruction_opcode__Ableu___rrR__nR__fR_A_mno_mfast_mbranches = 1245,
    uninspired_specialized_instruction_opcode__Ableu__n0___rr0__fR_A_mno_mfast_mbranches = 1246,
    uninspired_specialized_instruction_opcode__Ableu__n0___rrR__fR_A_mno_mfast_mbranches = 1247,
    uninspired_specialized_instruction_opcode__Ableu__n0__n0__fR_A_mno_mfast_mbranches = 1248,
    uninspired_specialized_instruction_opcode__Ableu__n0__n1__fR_A_mno_mfast_mbranches = 1249,
    uninspired_specialized_instruction_opcode__Ableu__n0__nR__fR_A_mno_mfast_mbranches = 1250,
    uninspired_specialized_instruction_opcode__Ableu__n1___rr0__fR_A_mno_mfast_mbranches = 1251,
    uninspired_specialized_instruction_opcode__Ableu__n1___rrR__fR_A_mno_mfast_mbranches = 1252,
    uninspired_specialized_instruction_opcode__Ableu__n1__n0__fR_A_mno_mfast_mbranches = 1253,
    uninspired_specialized_instruction_opcode__Ableu__n1__n1__fR_A_mno_mfast_mbranches = 1254,
    uninspired_specialized_instruction_opcode__Ableu__n1__nR__fR_A_mno_mfast_mbranches = 1255,
    uninspired_specialized_instruction_opcode__Ableu__nR___rr0__fR_A_mno_mfast_mbranches = 1256,
    uninspired_specialized_instruction_opcode__Ableu__nR___rrR__fR_A_mno_mfast_mbranches = 1257,
    uninspired_specialized_instruction_opcode__Ableu__nR__n0__fR_A_mno_mfast_mbranches = 1258,
    uninspired_specialized_instruction_opcode__Ableu__nR__n1__fR_A_mno_mfast_mbranches = 1259,
    uninspired_specialized_instruction_opcode__Ableu__nR__nR__fR_A_mno_mfast_mbranches = 1260,
    uninspired_specialized_instruction_opcode__Ablt___rr0___rr0__fR_A_mno_mfast_mbranches = 1261,
    uninspired_specialized_instruction_opcode__Ablt___rr0___rrR__fR_A_mno_mfast_mbranches = 1262,
    uninspired_specialized_instruction_opcode__Ablt___rr0__n0__fR_A_mno_mfast_mbranches = 1263,
    uninspired_specialized_instruction_opcode__Ablt___rr0__n1__fR_A_mno_mfast_mbranches = 1264,
    uninspired_specialized_instruction_opcode__Ablt___rr0__nR__fR_A_mno_mfast_mbranches = 1265,
    uninspired_specialized_instruction_opcode__Ablt___rrR___rr0__fR_A_mno_mfast_mbranches = 1266,
    uninspired_specialized_instruction_opcode__Ablt___rrR___rrR__fR_A_mno_mfast_mbranches = 1267,
    uninspired_specialized_instruction_opcode__Ablt___rrR__n0__fR_A_mno_mfast_mbranches = 1268,
    uninspired_specialized_instruction_opcode__Ablt___rrR__n1__fR_A_mno_mfast_mbranches = 1269,
    uninspired_specialized_instruction_opcode__Ablt___rrR__nR__fR_A_mno_mfast_mbranches = 1270,
    uninspired_specialized_instruction_opcode__Ablt__n0___rr0__fR_A_mno_mfast_mbranches = 1271,
    uninspired_specialized_instruction_opcode__Ablt__n0___rrR__fR_A_mno_mfast_mbranches = 1272,
    uninspired_specialized_instruction_opcode__Ablt__n0__n0__fR_A_mno_mfast_mbranches = 1273,
    uninspired_specialized_instruction_opcode__Ablt__n0__n1__fR_A_mno_mfast_mbranches = 1274,
    uninspired_specialized_instruction_opcode__Ablt__n0__nR__fR_A_mno_mfast_mbranches = 1275,
    uninspired_specialized_instruction_opcode__Ablt__n1___rr0__fR_A_mno_mfast_mbranches = 1276,
    uninspired_specialized_instruction_opcode__Ablt__n1___rrR__fR_A_mno_mfast_mbranches = 1277,
    uninspired_specialized_instruction_opcode__Ablt__n1__n0__fR_A_mno_mfast_mbranches = 1278,
    uninspired_specialized_instruction_opcode__Ablt__n1__n1__fR_A_mno_mfast_mbranches = 1279,
    uninspired_specialized_instruction_opcode__Ablt__n1__nR__fR_A_mno_mfast_mbranches = 1280,
    uninspired_specialized_instruction_opcode__Ablt__nR___rr0__fR_A_mno_mfast_mbranches = 1281,
    uninspired_specialized_instruction_opcode__Ablt__nR___rrR__fR_A_mno_mfast_mbranches = 1282,
    uninspired_specialized_instruction_opcode__Ablt__nR__n0__fR_A_mno_mfast_mbranches = 1283,
    uninspired_specialized_instruction_opcode__Ablt__nR__n1__fR_A_mno_mfast_mbranches = 1284,
    uninspired_specialized_instruction_opcode__Ablt__nR__nR__fR_A_mno_mfast_mbranches = 1285,
    uninspired_specialized_instruction_opcode__Abltu___rr0___rr0__fR_A_mno_mfast_mbranches = 1286,
    uninspired_specialized_instruction_opcode__Abltu___rr0___rrR__fR_A_mno_mfast_mbranches = 1287,
    uninspired_specialized_instruction_opcode__Abltu___rr0__n0__fR_A_mno_mfast_mbranches = 1288,
    uninspired_specialized_instruction_opcode__Abltu___rr0__n1__fR_A_mno_mfast_mbranches = 1289,
    uninspired_specialized_instruction_opcode__Abltu___rr0__nR__fR_A_mno_mfast_mbranches = 1290,
    uninspired_specialized_instruction_opcode__Abltu___rrR___rr0__fR_A_mno_mfast_mbranches = 1291,
    uninspired_specialized_instruction_opcode__Abltu___rrR___rrR__fR_A_mno_mfast_mbranches = 1292,
    uninspired_specialized_instruction_opcode__Abltu___rrR__n0__fR_A_mno_mfast_mbranches = 1293,
    uninspired_specialized_instruction_opcode__Abltu___rrR__n1__fR_A_mno_mfast_mbranches = 1294,
    uninspired_specialized_instruction_opcode__Abltu___rrR__nR__fR_A_mno_mfast_mbranches = 1295,
    uninspired_specialized_instruction_opcode__Abltu__n0___rr0__fR_A_mno_mfast_mbranches = 1296,
    uninspired_specialized_instruction_opcode__Abltu__n0___rrR__fR_A_mno_mfast_mbranches = 1297,
    uninspired_specialized_instruction_opcode__Abltu__n0__n0__fR_A_mno_mfast_mbranches = 1298,
    uninspired_specialized_instruction_opcode__Abltu__n0__n1__fR_A_mno_mfast_mbranches = 1299,
    uninspired_specialized_instruction_opcode__Abltu__n0__nR__fR_A_mno_mfast_mbranches = 1300,
    uninspired_specialized_instruction_opcode__Abltu__n1___rr0__fR_A_mno_mfast_mbranches = 1301,
    uninspired_specialized_instruction_opcode__Abltu__n1___rrR__fR_A_mno_mfast_mbranches = 1302,
    uninspired_specialized_instruction_opcode__Abltu__n1__n0__fR_A_mno_mfast_mbranches = 1303,
    uninspired_specialized_instruction_opcode__Abltu__n1__n1__fR_A_mno_mfast_mbranches = 1304,
    uninspired_specialized_instruction_opcode__Abltu__n1__nR__fR_A_mno_mfast_mbranches = 1305,
    uninspired_specialized_instruction_opcode__Abltu__nR___rr0__fR_A_mno_mfast_mbranches = 1306,
    uninspired_specialized_instruction_opcode__Abltu__nR___rrR__fR_A_mno_mfast_mbranches = 1307,
    uninspired_specialized_instruction_opcode__Abltu__nR__n0__fR_A_mno_mfast_mbranches = 1308,
    uninspired_specialized_instruction_opcode__Abltu__nR__n1__fR_A_mno_mfast_mbranches = 1309,
    uninspired_specialized_instruction_opcode__Abltu__nR__nR__fR_A_mno_mfast_mbranches = 1310,
    uninspired_specialized_instruction_opcode__Abne___rr0___rr0__fR_A_mno_mfast_mbranches = 1311,
    uninspired_specialized_instruction_opcode__Abne___rr0___rrR__fR_A_mno_mfast_mbranches = 1312,
    uninspired_specialized_instruction_opcode__Abne___rr0__n0__fR_A_mno_mfast_mbranches = 1313,
    uninspired_specialized_instruction_opcode__Abne___rr0__nR__fR_A_mno_mfast_mbranches = 1314,
    uninspired_specialized_instruction_opcode__Abne___rrR___rr0__fR_A_mno_mfast_mbranches = 1315,
    uninspired_specialized_instruction_opcode__Abne___rrR___rrR__fR_A_mno_mfast_mbranches = 1316,
    uninspired_specialized_instruction_opcode__Abne___rrR__n0__fR_A_mno_mfast_mbranches = 1317,
    uninspired_specialized_instruction_opcode__Abne___rrR__nR__fR_A_mno_mfast_mbranches = 1318,
    uninspired_specialized_instruction_opcode__Abne__n0___rr0__fR_A_mno_mfast_mbranches = 1319,
    uninspired_specialized_instruction_opcode__Abne__n0___rrR__fR_A_mno_mfast_mbranches = 1320,
    uninspired_specialized_instruction_opcode__Abne__n0__n0__fR_A_mno_mfast_mbranches = 1321,
    uninspired_specialized_instruction_opcode__Abne__n0__nR__fR_A_mno_mfast_mbranches = 1322,
    uninspired_specialized_instruction_opcode__Abne__nR___rr0__fR_A_mno_mfast_mbranches = 1323,
    uninspired_specialized_instruction_opcode__Abne__nR___rrR__fR_A_mno_mfast_mbranches = 1324,
    uninspired_specialized_instruction_opcode__Abne__nR__n0__fR_A_mno_mfast_mbranches = 1325,
    uninspired_specialized_instruction_opcode__Abne__nR__nR__fR_A_mno_mfast_mbranches = 1326,
    uninspired_specialized_instruction_opcode__Abneg___rr0__fR_A_mno_mfast_mbranches = 1327,
    uninspired_specialized_instruction_opcode__Abneg___rrR__fR_A_mno_mfast_mbranches = 1328,
    uninspired_specialized_instruction_opcode__Abneg__nR__fR_A_mno_mfast_mbranches = 1329,
    uninspired_specialized_instruction_opcode__Abnneg___rr0__fR_A_mno_mfast_mbranches = 1330,
    uninspired_specialized_instruction_opcode__Abnneg___rrR__fR_A_mno_mfast_mbranches = 1331,
    uninspired_specialized_instruction_opcode__Abnneg__nR__fR_A_mno_mfast_mbranches = 1332,
    uninspired_specialized_instruction_opcode__Abnotand___rr0___rr0__fR_A_mno_mfast_mbranches = 1333,
    uninspired_specialized_instruction_opcode__Abnotand___rr0___rrR__fR_A_mno_mfast_mbranches = 1334,
    uninspired_specialized_instruction_opcode__Abnotand___rr0__n3__fR_A_mno_mfast_mbranches = 1335,
    uninspired_specialized_instruction_opcode__Abnotand___rr0__n7__fR_A_mno_mfast_mbranches = 1336,
    uninspired_specialized_instruction_opcode__Abnotand___rr0__nR__fR_A_mno_mfast_mbranches = 1337,
    uninspired_specialized_instruction_opcode__Abnotand___rrR___rr0__fR_A_mno_mfast_mbranches = 1338,
    uninspired_specialized_instruction_opcode__Abnotand___rrR___rrR__fR_A_mno_mfast_mbranches = 1339,
    uninspired_specialized_instruction_opcode__Abnotand___rrR__n3__fR_A_mno_mfast_mbranches = 1340,
    uninspired_specialized_instruction_opcode__Abnotand___rrR__n7__fR_A_mno_mfast_mbranches = 1341,
    uninspired_specialized_instruction_opcode__Abnotand___rrR__nR__fR_A_mno_mfast_mbranches = 1342,
    uninspired_specialized_instruction_opcode__Abnotand__nR___rr0__fR_A_mno_mfast_mbranches = 1343,
    uninspired_specialized_instruction_opcode__Abnotand__nR___rrR__fR_A_mno_mfast_mbranches = 1344,
    uninspired_specialized_instruction_opcode__Abnotand__nR__n3__fR_A_mno_mfast_mbranches = 1345,
    uninspired_specialized_instruction_opcode__Abnotand__nR__n7__fR_A_mno_mfast_mbranches = 1346,
    uninspired_specialized_instruction_opcode__Abnotand__nR__nR__fR_A_mno_mfast_mbranches = 1347,
    uninspired_specialized_instruction_opcode__Abnpos___rr0__fR_A_mno_mfast_mbranches = 1348,
    uninspired_specialized_instruction_opcode__Abnpos___rrR__fR_A_mno_mfast_mbranches = 1349,
    uninspired_specialized_instruction_opcode__Abnpos__nR__fR_A_mno_mfast_mbranches = 1350,
    uninspired_specialized_instruction_opcode__Abnz___rr0__fR_A_mno_mfast_mbranches = 1351,
    uninspired_specialized_instruction_opcode__Abnz___rrR__fR_A_mno_mfast_mbranches = 1352,
    uninspired_specialized_instruction_opcode__Abnz__nR__fR_A_mno_mfast_mbranches = 1353,
    uninspired_specialized_instruction_opcode__Abpos___rr0__fR_A_mno_mfast_mbranches = 1354,
    uninspired_specialized_instruction_opcode__Abpos___rrR__fR_A_mno_mfast_mbranches = 1355,
    uninspired_specialized_instruction_opcode__Abpos__nR__fR_A_mno_mfast_mbranches = 1356,
    uninspired_specialized_instruction_opcode__Abz___rr0__fR_A_mno_mfast_mbranches = 1357,
    uninspired_specialized_instruction_opcode__Abz___rrR__fR_A_mno_mfast_mbranches = 1358,
    uninspired_specialized_instruction_opcode__Abz__nR__fR_A_mno_mfast_mbranches = 1359,
    uninspired_specialized_instruction_opcode__Adivo___rr0___rr0___rr0__fR_A_mno_mfast_mbranches = 1360,
    uninspired_specialized_instruction_opcode__Adivo___rr0___rr0___rrR__fR_A_mno_mfast_mbranches = 1361,
    uninspired_specialized_instruction_opcode__Adivo___rr0___rrR___rr0__fR_A_mno_mfast_mbranches = 1362,
    uninspired_specialized_instruction_opcode__Adivo___rr0___rrR___rrR__fR_A_mno_mfast_mbranches = 1363,
    uninspired_specialized_instruction_opcode__Adivo___rr0__n2___rr0__fR_A_mno_mfast_mbranches = 1364,
    uninspired_specialized_instruction_opcode__Adivo___rr0__n2___rrR__fR_A_mno_mfast_mbranches = 1365,
    uninspired_specialized_instruction_opcode__Adivo___rr0__nR___rr0__fR_A_mno_mfast_mbranches = 1366,
    uninspired_specialized_instruction_opcode__Adivo___rr0__nR___rrR__fR_A_mno_mfast_mbranches = 1367,
    uninspired_specialized_instruction_opcode__Adivo___rrR___rr0___rr0__fR_A_mno_mfast_mbranches = 1368,
    uninspired_specialized_instruction_opcode__Adivo___rrR___rr0___rrR__fR_A_mno_mfast_mbranches = 1369,
    uninspired_specialized_instruction_opcode__Adivo___rrR___rrR___rr0__fR_A_mno_mfast_mbranches = 1370,
    uninspired_specialized_instruction_opcode__Adivo___rrR___rrR___rrR__fR_A_mno_mfast_mbranches = 1371,
    uninspired_specialized_instruction_opcode__Adivo___rrR__n2___rr0__fR_A_mno_mfast_mbranches = 1372,
    uninspired_specialized_instruction_opcode__Adivo___rrR__n2___rrR__fR_A_mno_mfast_mbranches = 1373,
    uninspired_specialized_instruction_opcode__Adivo___rrR__nR___rr0__fR_A_mno_mfast_mbranches = 1374,
    uninspired_specialized_instruction_opcode__Adivo___rrR__nR___rrR__fR_A_mno_mfast_mbranches = 1375,
    uninspired_specialized_instruction_opcode__Adivo__nR___rr0___rr0__fR_A_mno_mfast_mbranches = 1376,
    uninspired_specialized_instruction_opcode__Adivo__nR___rr0___rrR__fR_A_mno_mfast_mbranches = 1377,
    uninspired_specialized_instruction_opcode__Adivo__nR___rrR___rr0__fR_A_mno_mfast_mbranches = 1378,
    uninspired_specialized_instruction_opcode__Adivo__nR___rrR___rrR__fR_A_mno_mfast_mbranches = 1379,
    uninspired_specialized_instruction_opcode__Adivo__nR__n2___rr0__fR_A_mno_mfast_mbranches = 1380,
    uninspired_specialized_instruction_opcode__Adivo__nR__n2___rrR__fR_A_mno_mfast_mbranches = 1381,
    uninspired_specialized_instruction_opcode__Adivo__nR__nR___rr0__fR_A_mno_mfast_mbranches = 1382,
    uninspired_specialized_instruction_opcode__Adivo__nR__nR___rrR__fR_A_mno_mfast_mbranches = 1383,
    uninspired_specialized_instruction_opcode__Amodo___rr0___rr0___rr0__fR_A_mno_mfast_mbranches = 1384,
    uninspired_specialized_instruction_opcode__Amodo___rr0___rr0___rrR__fR_A_mno_mfast_mbranches = 1385,
    uninspired_specialized_instruction_opcode__Amodo___rr0___rrR___rr0__fR_A_mno_mfast_mbranches = 1386,
    uninspired_specialized_instruction_opcode__Amodo___rr0___rrR___rrR__fR_A_mno_mfast_mbranches = 1387,
    uninspired_specialized_instruction_opcode__Amodo___rr0__n2___rr0__fR_A_mno_mfast_mbranches = 1388,
    uninspired_specialized_instruction_opcode__Amodo___rr0__n2___rrR__fR_A_mno_mfast_mbranches = 1389,
    uninspired_specialized_instruction_opcode__Amodo___rr0__nR___rr0__fR_A_mno_mfast_mbranches = 1390,
    uninspired_specialized_instruction_opcode__Amodo___rr0__nR___rrR__fR_A_mno_mfast_mbranches = 1391,
    uninspired_specialized_instruction_opcode__Amodo___rrR___rr0___rr0__fR_A_mno_mfast_mbranches = 1392,
    uninspired_specialized_instruction_opcode__Amodo___rrR___rr0___rrR__fR_A_mno_mfast_mbranches = 1393,
    uninspired_specialized_instruction_opcode__Amodo___rrR___rrR___rr0__fR_A_mno_mfast_mbranches = 1394,
    uninspired_specialized_instruction_opcode__Amodo___rrR___rrR___rrR__fR_A_mno_mfast_mbranches = 1395,
    uninspired_specialized_instruction_opcode__Amodo___rrR__n2___rr0__fR_A_mno_mfast_mbranches = 1396,
    uninspired_specialized_instruction_opcode__Amodo___rrR__n2___rrR__fR_A_mno_mfast_mbranches = 1397,
    uninspired_specialized_instruction_opcode__Amodo___rrR__nR___rr0__fR_A_mno_mfast_mbranches = 1398,
    uninspired_specialized_instruction_opcode__Amodo___rrR__nR___rrR__fR_A_mno_mfast_mbranches = 1399,
    uninspired_specialized_instruction_opcode__Amodo__nR___rr0___rr0__fR_A_mno_mfast_mbranches = 1400,
    uninspired_specialized_instruction_opcode__Amodo__nR___rr0___rrR__fR_A_mno_mfast_mbranches = 1401,
    uninspired_specialized_instruction_opcode__Amodo__nR___rrR___rr0__fR_A_mno_mfast_mbranches = 1402,
    uninspired_specialized_instruction_opcode__Amodo__nR___rrR___rrR__fR_A_mno_mfast_mbranches = 1403,
    uninspired_specialized_instruction_opcode__Amodo__nR__n2___rr0__fR_A_mno_mfast_mbranches = 1404,
    uninspired_specialized_instruction_opcode__Amodo__nR__n2___rrR__fR_A_mno_mfast_mbranches = 1405,
    uninspired_specialized_instruction_opcode__Amodo__nR__nR___rr0__fR_A_mno_mfast_mbranches = 1406,
    uninspired_specialized_instruction_opcode__Amodo__nR__nR___rrR__fR_A_mno_mfast_mbranches = 1407,
    uninspired_specialized_instruction_opcode__Amulo___rr0___rr0___rr0__fR_A_mno_mfast_mbranches = 1408,
    uninspired_specialized_instruction_opcode__Amulo___rr0___rr0___rrR__fR_A_mno_mfast_mbranches = 1409,
    uninspired_specialized_instruction_opcode__Amulo___rr0___rrR___rr0__fR_A_mno_mfast_mbranches = 1410,
    uninspired_specialized_instruction_opcode__Amulo___rr0___rrR___rrR__fR_A_mno_mfast_mbranches = 1411,
    uninspired_specialized_instruction_opcode__Amulo___rr0__n2___rr0__fR_A_mno_mfast_mbranches = 1412,
    uninspired_specialized_instruction_opcode__Amulo___rr0__n2___rrR__fR_A_mno_mfast_mbranches = 1413,
    uninspired_specialized_instruction_opcode__Amulo___rr0__nR___rr0__fR_A_mno_mfast_mbranches = 1414,
    uninspired_specialized_instruction_opcode__Amulo___rr0__nR___rrR__fR_A_mno_mfast_mbranches = 1415,
    uninspired_specialized_instruction_opcode__Amulo___rrR___rr0___rr0__fR_A_mno_mfast_mbranches = 1416,
    uninspired_specialized_instruction_opcode__Amulo___rrR___rr0___rrR__fR_A_mno_mfast_mbranches = 1417,
    uninspired_specialized_instruction_opcode__Amulo___rrR___rrR___rr0__fR_A_mno_mfast_mbranches = 1418,
    uninspired_specialized_instruction_opcode__Amulo___rrR___rrR___rrR__fR_A_mno_mfast_mbranches = 1419,
    uninspired_specialized_instruction_opcode__Amulo___rrR__n2___rr0__fR_A_mno_mfast_mbranches = 1420,
    uninspired_specialized_instruction_opcode__Amulo___rrR__n2___rrR__fR_A_mno_mfast_mbranches = 1421,
    uninspired_specialized_instruction_opcode__Amulo___rrR__nR___rr0__fR_A_mno_mfast_mbranches = 1422,
    uninspired_specialized_instruction_opcode__Amulo___rrR__nR___rrR__fR_A_mno_mfast_mbranches = 1423,
    uninspired_specialized_instruction_opcode__Amulo__nR___rr0___rr0__fR_A_mno_mfast_mbranches = 1424,
    uninspired_specialized_instruction_opcode__Amulo__nR___rr0___rrR__fR_A_mno_mfast_mbranches = 1425,
    uninspired_specialized_instruction_opcode__Amulo__nR___rrR___rr0__fR_A_mno_mfast_mbranches = 1426,
    uninspired_specialized_instruction_opcode__Amulo__nR___rrR___rrR__fR_A_mno_mfast_mbranches = 1427,
    uninspired_specialized_instruction_opcode__Amulo__nR__n2___rr0__fR_A_mno_mfast_mbranches = 1428,
    uninspired_specialized_instruction_opcode__Amulo__nR__n2___rrR__fR_A_mno_mfast_mbranches = 1429,
    uninspired_specialized_instruction_opcode__Amulo__nR__nR___rr0__fR_A_mno_mfast_mbranches = 1430,
    uninspired_specialized_instruction_opcode__Amulo__nR__nR___rrR__fR_A_mno_mfast_mbranches = 1431,
    uninspired_specialized_instruction_opcode__Aprocedurecall__fR__retR_A_mno_mfast_mbranches = 1432,
    uninspired_specialized_instruction_opcode__Asafe_mpoint__fR_A_mno_mfast_mbranches = 1433,
    uninspired_specialized_instruction_opcode__Astackif__fR_A_mno_mfast_mbranches = 1434,
    uninspired_specialized_instruction_opcode__Astacknondroppingif__fR_A_mno_mfast_mbranches = 1435,
    uninspired_specialized_instruction_opcode__Asubo___rr0___rr0___rr0__fR_A_mno_mfast_mbranches = 1436,
    uninspired_specialized_instruction_opcode__Asubo___rr0___rr0___rrR__fR_A_mno_mfast_mbranches = 1437,
    uninspired_specialized_instruction_opcode__Asubo___rr0___rrR___rr0__fR_A_mno_mfast_mbranches = 1438,
    uninspired_specialized_instruction_opcode__Asubo___rr0___rrR___rrR__fR_A_mno_mfast_mbranches = 1439,
    uninspired_specialized_instruction_opcode__Asubo___rr0__n1___rr0__fR_A_mno_mfast_mbranches = 1440,
    uninspired_specialized_instruction_opcode__Asubo___rr0__n1___rrR__fR_A_mno_mfast_mbranches = 1441,
    uninspired_specialized_instruction_opcode__Asubo___rr0__nR___rr0__fR_A_mno_mfast_mbranches = 1442,
    uninspired_specialized_instruction_opcode__Asubo___rr0__nR___rrR__fR_A_mno_mfast_mbranches = 1443,
    uninspired_specialized_instruction_opcode__Asubo___rrR___rr0___rr0__fR_A_mno_mfast_mbranches = 1444,
    uninspired_specialized_instruction_opcode__Asubo___rrR___rr0___rrR__fR_A_mno_mfast_mbranches = 1445,
    uninspired_specialized_instruction_opcode__Asubo___rrR___rrR___rr0__fR_A_mno_mfast_mbranches = 1446,
    uninspired_specialized_instruction_opcode__Asubo___rrR___rrR___rrR__fR_A_mno_mfast_mbranches = 1447,
    uninspired_specialized_instruction_opcode__Asubo___rrR__n1___rr0__fR_A_mno_mfast_mbranches = 1448,
    uninspired_specialized_instruction_opcode__Asubo___rrR__n1___rrR__fR_A_mno_mfast_mbranches = 1449,
    uninspired_specialized_instruction_opcode__Asubo___rrR__nR___rr0__fR_A_mno_mfast_mbranches = 1450,
    uninspired_specialized_instruction_opcode__Asubo___rrR__nR___rrR__fR_A_mno_mfast_mbranches = 1451,
    uninspired_specialized_instruction_opcode__Asubo__nR___rr0___rr0__fR_A_mno_mfast_mbranches = 1452,
    uninspired_specialized_instruction_opcode__Asubo__nR___rr0___rrR__fR_A_mno_mfast_mbranches = 1453,
    uninspired_specialized_instruction_opcode__Asubo__nR___rrR___rr0__fR_A_mno_mfast_mbranches = 1454,
    uninspired_specialized_instruction_opcode__Asubo__nR___rrR___rrR__fR_A_mno_mfast_mbranches = 1455,
    uninspired_specialized_instruction_opcode__Asubo__nR__n1___rr0__fR_A_mno_mfast_mbranches = 1456,
    uninspired_specialized_instruction_opcode__Asubo__nR__n1___rrR__fR_A_mno_mfast_mbranches = 1457,
    uninspired_specialized_instruction_opcode__Asubo__nR__nR___rr0__fR_A_mno_mfast_mbranches = 1458,
    uninspired_specialized_instruction_opcode__Asubo__nR__nR___rrR__fR_A_mno_mfast_mbranches = 1459
  };

#define UNINSPIRED_SPECIALIZED_INSTRUCTION_NO 1460

#endif // #ifndef UNINSPIRED_SPECIALIZED_INSTRUCTIONS_H_
/* How many residuals we can have at most.  This, with some dispatching models,
   is needed to compute a slow register offset from the base. */
#define UNINSPIRED_MAX_RESIDUAL_ARITY  4

/* User-specified code, late header part: beginning. */

  
/* User-specified code, late header part: end */


/* Close the multiple-inclusion guard opened in the template. */
#endif // #ifndef UNINSPIRED_VM_H_
