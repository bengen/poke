#!@SHELL@
# Jitter testsuite: VM programs.  This is -*- sh -*-, to be preprocessed.
# Copyright (C) 2017 Luca Saiu
# Updated in 2019 by Luca Saiu
# Written by Luca Saiu

# This file is part of Jitter.

# Jitter is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Jitter is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Jitter.  If not, see <http://www.gnu.org/licenses/>.


# Include my little testsuite library.  Among the rest this sourcing enters the
# directory containing *this* script (not the utility script).
. "@abs_top_builddir@/tests/utility"

# Print a range for test case indices.
echo 1..40

# Test programs doing nothing.
jitter_run_structured_case_all_dispatches "skip" "" ""
jitter_run_structured_case_all_dispatches "begin-end" "" ""

# Run a small program involving a conditional jump.
jitter_run_structured_case_all_dispatches "conditional" "42" ""

# Test a simple structured program computing the Maximum Common Divisor of two
# naturals using Euclid's algorithm.
jitter_run_structured_case_all_dispatches "euclid" "21" ""

# Test a simple structured program computing the first few prime numbers in a
# naïve way.
jitter_run_structured_case_all_dispatches "primes" "2/3/5/7/11/13/17/19/23/29" ""
