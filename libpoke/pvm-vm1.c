/* This code is machine-generated.  See its source for license
   information. This software is derived from software
   distributed under the GNU GPL version 3 or later. */

/* User-specified code, initial vm1 part: beginning. */
#line 157 "../../libpoke/pvm.jitter"
#line 157 "../../libpoke/pvm.jitter"

#   include <config.h>
  
/* User-specified code, initial vm1 part: end */

/* VM library: main VM C file template.

   Copyright (C) 2016, 2017, 2018, 2019, 2020 Luca Saiu
   Written by Luca Saiu

   This file is part of Jitter.

   Jitter is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Jitter is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Jitter.  If not, see <http://www.gnu.org/licenses/>. */


/* Generated file warning.
 * ************************************************************************** */

/* Unless this file is named exactly "vm1.c" , without any prefix, you are
   looking at a machine-generated derived file.  The original source is the vm.c
   template from Jitter, with added code implementing the pvm VM. */




#include <assert.h>
#include <string.h>

#include <jitter/jitter.h>

#if defined (JITTER_PROFILE_SAMPLE)
#include <sys/time.h>
#endif // #if defined (JITTER_PROFILE_SAMPLE)

#include <jitter/jitter-hash.h>
#include <jitter/jitter-instruction.h>
#include <jitter/jitter-mmap.h>
#include <jitter/jitter-mutable-routine.h>
#include <jitter/jitter-print.h>
#include <jitter/jitter-rewrite.h>
#include <jitter/jitter-routine.h>
#include <jitter/jitter-routine-parser.h>
#include <jitter/jitter-specialize.h>
#include <jitter/jitter-defect.h>
#include <jitter/jitter-patch-in.h>

/* I don't need to include <jitter/jitter-executor.h> here, nor to define
   JITTER_THIS_CAN_INCLUDE_JITTER_EXECUTOR_H ; doing so carelessly might
   lead to subtle bugs, that it is better to prevent.
   Of course I can reconsider this decision in the future. */

#include <jitter/jitter-data-locations.h>

#include "pvm-vm.h"
//#include "pvm-specialized-instructions.h"
//#include "pvm-meta-instructions.h"
#include <jitter/jitter-fatal.h>




/* Check requirements for particular features.
 * ************************************************************************** */

/* VM sample-profiling is only supported with GCC.  Do not bother activating it
   with other compilers, if the numbers are unreliable in the end. */
#if  defined (JITTER_PROFILE_SAMPLE)        \
     && ! defined (JITTER_HAVE_ACTUAL_GCC)
# error "Sample-profiling is only reliable with GCC: it requires (machine-independent)"
# error "GNU C extended asm, and it is not worth supporting other compilers if"
# error "the numbers turn out to be unreliable in the end."
#endif




/* Machine-generated data structures.
 * ************************************************************************** */

/* Machine-generated data structures defining this VM.  Initializing a static
   struct is problematic, as it requires constant expressions for each field --
   and const pointers don't qualify.  This is why we initialize the struct
   fields below in pvm_initialize. */
static struct jitter_vm
the_pvm_vm;

struct jitter_vm * const
pvm_vm = & the_pvm_vm;

struct jitter_list_header * const
pvm_states = & the_pvm_vm.states;

/* It is convenient to have this initialised at start up, even before calling
   any initialisation function.  This makes it reliable to read this when, for
   example, handling --version . */
static const struct jitter_vm_configuration
pvm_vm_the_configuration
  = {
      PVM_LOWER_CASE_PREFIX /* lower_case_prefix */,
      PVM_UPPER_CASE_PREFIX /* upper_case_prefix */,
      PVM_MAX_FAST_REGISTER_NO_PER_CLASS
        /* max_fast_register_no_per_class */,
      PVM_MAX_NONRESIDUAL_LITERAL_NO /* max_nonresidual_literal_no */,
      PVM_DISPATCH_HUMAN_READABLE /* dispatch_human_readable */,
      /* The instrumentation field can be seen as a bit map.  See the comment
         in jitter/jitter-vm.h . */
      (jitter_vm_instrumentation_none
#if defined (JITTER_PROFILE_COUNT)
       | jitter_vm_instrumentation_count
#endif
#if defined (JITTER_PROFILE_SAMPLE)
       | jitter_vm_instrumentation_sample
#endif
       ) /* instrumentation */
    };

const struct jitter_vm_configuration * const
pvm_vm_configuration
  = & pvm_vm_the_configuration;




/* Initialization and finalization: internal functions, not for the user.
 * ************************************************************************** */

/* Initialize threads.  This only needs to be called once at initialization, and
   the user doesn't need to bother with it.  Defined along with the executor. */
void
pvm_initialize_threads (void);

/* Check that the encodings in enum jitter_specialized_instruction_opcode (as
   used in the specializer) are coherent with machine-generated code.  Making a
   mistake here would introduce subtle bugs, so it's better to be defensive. */
static void
pvm_check_specialized_instruction_opcode_once (void)
{
  static bool already_checked = false;
  if (already_checked)
    return;

  assert (((enum jitter_specialized_instruction_opcode)
           pvm_specialized_instruction_opcode__eINVALID)
          == jitter_specialized_instruction_opcode_INVALID);
  assert (((enum jitter_specialized_instruction_opcode)
           pvm_specialized_instruction_opcode__eBEGINBASICBLOCK)
          == jitter_specialized_instruction_opcode_BEGINBASICBLOCK);
  assert (((enum jitter_specialized_instruction_opcode)
           pvm_specialized_instruction_opcode__eEXITVM)
          == jitter_specialized_instruction_opcode_EXITVM);
  assert (((enum jitter_specialized_instruction_opcode)
           pvm_specialized_instruction_opcode__eDATALOCATIONS)
          == jitter_specialized_instruction_opcode_DATALOCATIONS);
  assert (((enum jitter_specialized_instruction_opcode)
           pvm_specialized_instruction_opcode__eNOP)
          == jitter_specialized_instruction_opcode_NOP);
  assert (((enum jitter_specialized_instruction_opcode)
           pvm_specialized_instruction_opcode__eUNREACHABLE0)
          == jitter_specialized_instruction_opcode_UNREACHABLE0);
  assert (((enum jitter_specialized_instruction_opcode)
           pvm_specialized_instruction_opcode__eUNREACHABLE1)
          == jitter_specialized_instruction_opcode_UNREACHABLE1);
  assert (((enum jitter_specialized_instruction_opcode)
           pvm_specialized_instruction_opcode__eUNREACHABLE2)
          == jitter_specialized_instruction_opcode_UNREACHABLE2);

  already_checked = true;
}

/* A prototype for a machine-generated function not needing a public
   declaration, only called thru a pointer within struct jitter_vm . */
int
pvm_specialize_instruction (struct jitter_mutable_routine *p,
                                 const struct jitter_instruction *ins);

/* Initialize the pointed special-purpose data structure. */
static void
pvm_initialize_special_purpose_data
   (volatile struct jitter_special_purpose_state_data *d)
{
  d->pending_notifications = 0;
  jitter_initialize_pending_signal_notifications
     (& d->pending_signal_notifications);

  /* Initialise profiling fields. */
  jitter_profile_runtime_initialize (pvm_vm,
                                     (struct jitter_profile_runtime *)
                                     & d->profile_runtime);
}

/* Finalize the pointed special-purpose data structure. */
static void
pvm_finalize_special_purpose_data
   (volatile struct jitter_special_purpose_state_data *d)
{
  jitter_finalize_pending_signal_notifications
     (d->pending_signal_notifications);

  jitter_profile_runtime_finalize (pvm_vm,
                                   (struct jitter_profile_runtime *)
                                   & d->profile_runtime);
}




/* Check that we link with the correct Jitter library.
 * ************************************************************************** */

/* It is possible to make a mistake at link time, and link a VM compiled with
   some threading model with the Jitter runtime for a different model.  That
   would cause crashes, that is better to prevent.  This is a way to detect such
   mistakes very early, by causing a link-time failure in case of mismatch. */
extern volatile const bool
JITTER_DISPATCH_DEPENDENT_GLOBAL_NAME;




/* Low-level debugging features relying on assembly: data locations.
 * ************************************************************************** */

#if defined (JITTER_HAVE_KNOWN_BINARY_FORMAT) && ! defined (JITTER_DISPATCH_SWITCH)
/* A declaration for data locations, as visible from C.  The global is defined in
   assembly in its own separate section thru the machinery in
   jitter/jitter-sections.h . */
extern const char
JITTER_DATA_LOCATION_NAME(pvm) [];
#endif // #if ...

void
pvm_dump_data_locations (jitter_print_context output)
{
#ifndef JITTER_DISPATCH_SWITCH
  jitter_dump_data_locations (output, & the_pvm_vm);
#else
  jitter_print_char_star (output,
                          "VM data location information unavailable\n");
#endif // #ifndef JITTER_DISPATCH_SWITCH
}




/* Initialization and finalization.
 * ************************************************************************** */

#ifdef JITTER_HAVE_PATCH_IN
JITTER_DEFECT_DESCRIPTOR_DECLARATIONS_(pvm)
JITTER_PATCH_IN_DESCRIPTOR_DECLARATIONS_(pvm)
#endif // #ifdef JITTER_HAVE_PATCH_IN

#ifndef JITTER_DISPATCH_SWITCH
/* True iff thread sizes are all non-negative and non-huge.  We refuse to
   disassemble otherwise, and when replication is enabled we refuse to run
   altogether.  See the comment right below. */
static bool
pvm_threads_validated = false;
#endif // #ifndef JITTER_DISPATCH_SWITCH

/* Omit pvm_validate_thread_sizes_once for switch-dispatching, as threads
   don't exist at all in that case.*/
#ifndef JITTER_DISPATCH_SWITCH
/* Check that VM instruction sizes are all non-negative, and that no thread
   starts before the end of the previous one.  Even one violation of such
   conditions is a symptom that the code has not been compiled with
   -fno-reorder-blocks , which would have disastrous effects with replication.
   It's better to validate threads at startup and fail immediately than to crash
   at run time.

   If even one thread appears to be wrong then refuse to disassemble when
   replication is disabled, and refuse to run altogether if replication is
   enabled. */
static void
pvm_validate_threads_once (void)
{
  /* Return if this is not the first time we got here. */
  static bool already_validated = false;
  if (already_validated)
    return;

#ifdef JITTER_REPLICATE
# define JITTER_FAIL(error_text)                                             \
    do                                                                       \
      {                                                                      \
        fprintf (stderr,                                                     \
                 "About specialized instruction %i (%s) at %p, size %liB\n", \
                 i, pvm_specialized_instruction_names [i],              \
                 pvm_threads [i],                                       \
                 pvm_thread_sizes [i]);                                 \
        jitter_fatal ("%s: you are not compiling with -fno-reorder-blocks",  \
                      error_text);                                           \
      }                                                                      \
    while (false)
#else
# define JITTER_FAIL(ignored_error_text)  \
    do                                    \
      {                                   \
        everything_valid = false;         \
        goto out;                         \
      }                                   \
    while (false)
#endif // #ifdef JITTER_REPLICATE

  /* The minimum address the next instruction code has to start at.

     This relies on NULL being zero, or in general lower in magnitude than any
     valid pointer.  It is not worth the trouble to be pedantic, as this will be
     true on every architecture where I can afford low-level tricks. */
  jitter_thread lower_bound = NULL;

  /* Check every thread.  We rely on the order here, following specialized
     instruction opcodes. */
  int i;
  bool everything_valid = true;
  for (i = 0; i < PVM_SPECIALIZED_INSTRUCTION_NO; i ++)
    {
      jitter_thread thread = pvm_threads [i];
      long size = pvm_thread_sizes [i];

      /* Check that the current thread has non-negative non-huge size and
         doesn't start before the end of the previous one.  If this is true for
         all threads we can conclude that they are non-overlapping as well. */
      if (__builtin_expect (size < 0, false))
        JITTER_FAIL("a specialized instruction has negative code size");
      if (__builtin_expect (size > (1 << 24), false))
        JITTER_FAIL("a specialized instruction has huge code size");
      if (__builtin_expect (lower_bound > thread, false))
        JITTER_FAIL("non-sequential thread");

      /* The next thread cannot start before the end of the current one. */
      lower_bound = ((char*) thread) + size;
    }

#undef JITTER_FAIL

#ifndef JITTER_REPLICATE
 out:
#endif // #ifndef JITTER_REPLICATE

  /* If we have validated every thread size then disassembling appears safe. */
  if (everything_valid)
    pvm_threads_validated = true;

  /* We have checked the thread sizes, once and for all.  If this function gets
     called again, thru a second pvm initialization, it will immediately
     return. */
  already_validated = true;
}
#endif // #ifndef JITTER_DISPATCH_SWITCH

#ifdef JITTER_HAVE_PATCH_IN
/* The actual defect table.  We only need it when patch-ins are in use. */
jitter_uint
pvm_defect_table [PVM_SPECIALIZED_INSTRUCTION_NO];
#endif // #ifdef JITTER_HAVE_PATCH_IN

void
pvm_initialize (void)
{
  /* Check that the Jitter library we linked is the right one.  This check
     actually only useful to force the global to be used.  I prefer not to use
     an assert, because assertions can be disabled. */
  if (! JITTER_DISPATCH_DEPENDENT_GLOBAL_NAME)
    jitter_fatal ("impossible to reach: the thing should fail at link time");

#ifdef JITTER_REPLICATE
  /* Initialize the executable-memory subsystem. */
  jitter_initialize_executable ();
#endif // #ifdef JITTER_REPLICATE

  /* Initialise the print-context machinery. */
  jitter_print_initialize ();

  /* Perform some sanity checks which only need to be run once. */
  pvm_check_specialized_instruction_opcode_once ();

  /* We have to initialize threads before pvm_threads , since the struct
     needs threads. */
  pvm_initialize_threads ();

#ifndef JITTER_DISPATCH_SWITCH
  /* Validate threads, to make sure the generated code was not compiled with
     incorrect options.  This only needs to be done once. */
  pvm_validate_threads_once ();
#endif // ifndef JITTER_DISPATCH_SWITCH

  /* Initialize the object pointed by pvm_vm (see the comment above as to
     why we do it here).  Before actually setting the fields to valid data, fill
     the whole struct with a -- hopefully -- invalid pattern, just to catch
     bugs. */
  static bool vm_struct_initialized = false;
  if (! vm_struct_initialized)
    {
      memset (& the_pvm_vm, 0xff, sizeof (struct jitter_vm));

      /* Make the configuration struct reachable from the VM struct. */
      the_pvm_vm.configuration = pvm_vm_configuration;
      //pvm_print_vm_configuration (stdout, & the_pvm_vm.configuration);

      /* Initialize meta-instruction pointers for implicit instructions.
         VM-independent program specialization relies on those, so they have to
         be accessible to the Jitter library, out of generated code.  Since
         meta-instructions are sorted alphabetically in the array, the index
         is not fixed. */
      the_pvm_vm.exitvm_meta_instruction
        = (pvm_meta_instructions + pvm_meta_instruction_id_exitvm);
      the_pvm_vm.unreachable_meta_instruction
        = (pvm_meta_instructions
           + pvm_meta_instruction_id_unreachable);

      /* Threads or pointers to native code blocks of course don't exist with
   switch-dispatching. */
#ifndef JITTER_DISPATCH_SWITCH
      the_pvm_vm.threads = (jitter_thread *)pvm_threads;
      the_pvm_vm.thread_sizes = (long *) pvm_thread_sizes;
      the_pvm_vm.threads_validated = pvm_threads_validated;
#if defined (JITTER_HAVE_KNOWN_BINARY_FORMAT)
      the_pvm_vm.data_locations = JITTER_DATA_LOCATION_NAME(pvm);
#else
      the_pvm_vm.data_locations = NULL;
#endif // #if defined (JITTER_HAVE_KNOWN_BINARY_FORMAT)
#endif // #ifndef JITTER_DISPATCH_SWITCH

      the_pvm_vm.specialized_instruction_residual_arities
        = pvm_specialized_instruction_residual_arities;
      the_pvm_vm.specialized_instruction_label_bitmasks
        = pvm_specialized_instruction_label_bitmasks;
#ifdef JITTER_HAVE_PATCH_IN
      the_pvm_vm.specialized_instruction_fast_label_bitmasks
        = pvm_specialized_instruction_fast_label_bitmasks;
      the_pvm_vm.patch_in_descriptors =
        JITTER_PATCH_IN_DESCRIPTORS_NAME(pvm);
      const size_t patch_in_descriptor_size
        = sizeof (struct jitter_patch_in_descriptor);
      the_pvm_vm.patch_in_descriptor_no
        = (JITTER_PATCH_IN_DESCRIPTORS_SIZE_IN_BYTES_NAME(pvm)
           / patch_in_descriptor_size);
      /* Cheap sanity check: if the size in bytes is not a multiple of
         the element size, we're doing something very wrong. */
      if (JITTER_PATCH_IN_DESCRIPTORS_SIZE_IN_BYTES_NAME(pvm)
          % patch_in_descriptor_size != 0)
        jitter_fatal ("patch-in descriptors total size not a multiple "
                      "of the element size");
      /* Initialize the patch-in table for this VM. */
      the_pvm_vm.patch_in_table
        = jitter_make_patch_in_table (the_pvm_vm.patch_in_descriptors,
                                      the_pvm_vm.patch_in_descriptor_no,
                                      PVM_SPECIALIZED_INSTRUCTION_NO);
#else
      the_pvm_vm.specialized_instruction_fast_label_bitmasks = NULL;
#endif // #ifdef JITTER_HAVE_PATCH_IN

      /* FIXME: I might want to conditionalize this. */
      the_pvm_vm.specialized_instruction_relocatables
        = pvm_specialized_instruction_relocatables;

      the_pvm_vm.specialized_instruction_callers
        = pvm_specialized_instruction_callers;
      the_pvm_vm.specialized_instruction_callees
        = pvm_specialized_instruction_callees;

      the_pvm_vm.specialized_instruction_names
        = pvm_specialized_instruction_names;
      the_pvm_vm.specialized_instruction_no
        = PVM_SPECIALIZED_INSTRUCTION_NO;

      the_pvm_vm.meta_instruction_string_hash
        = & pvm_meta_instruction_hash;
      the_pvm_vm.meta_instructions
        = (struct jitter_meta_instruction *) pvm_meta_instructions;
      the_pvm_vm.meta_instruction_no = PVM_META_INSTRUCTION_NO;
      the_pvm_vm.max_meta_instruction_name_length
        = PVM_MAX_META_INSTRUCTION_NAME_LENGTH;
      the_pvm_vm.specialized_instruction_to_unspecialized_instruction
        = pvm_specialized_instruction_to_unspecialized_instruction;
      the_pvm_vm.register_class_character_to_register_class
        = pvm_register_class_character_to_register_class;
      the_pvm_vm.specialize_instruction = pvm_specialize_instruction;
      the_pvm_vm.rewrite = pvm_rewrite;

#ifdef JITTER_HAVE_PATCH_IN
      /* Fill the defect table.  Since the array in question is a global with a
         fixed size, this needs to be done only once. */
      jitter_fill_defect_table (pvm_defect_table,
                                & the_pvm_vm,
                                pvm_worst_case_defect_table,
                                JITTER_DEFECT_DESCRIPTORS_NAME (pvm),
                                (JITTER_DEFECT_DESCRIPTORS_SIZE_IN_BYTES_NAME
                                    (pvm)
                                 / sizeof (struct jitter_defect_descriptor)));
#endif // #ifdef JITTER_HAVE_PATCH_IN

      /* Initialize the empty list of states. */
      JITTER_LIST_INITIALIZE_HEADER (& the_pvm_vm.states);

      vm_struct_initialized = true;
    }

  jitter_initialize_meta_instructions (& pvm_meta_instruction_hash,
                                         pvm_meta_instructions,
                                         PVM_META_INSTRUCTION_NO);

#ifdef JITTER_HAVE_PATCH_IN
  jitter_dump_defect_table (stderr, pvm_defect_table, & the_pvm_vm);
#endif // #ifdef JITTER_HAVE_PATCH_IN
}

void
pvm_finalize (void)
{
  /* There's no need to touch the_pvm_vm ; we can keep it as it is, as it
     contains no dynamically-allocated fields. */
  /* Threads need no finalization. */
  jitter_finalize_meta_instructions (& pvm_meta_instruction_hash);

#ifdef JITTER_HAVE_PATCH_IN
  /* Destroy the patch-in table for this VM. */
  jitter_destroy_patch_in_table (the_pvm_vm.patch_in_table,
                                 PVM_SPECIALIZED_INSTRUCTION_NO);
#endif // #ifdef JITTER_HAVE_PATCH_IN

#ifdef JITTER_REPLICATE
  /* Finalize the executable-memory subsystem. */
  jitter_finalize_executable ();
#endif // #ifdef JITTER_REPLICATE

  /* Finalize the state list.  If it is not empty then something has gone
     wrong earlier. */
  if (the_pvm_vm.states.first != NULL
      || the_pvm_vm.states.last != NULL)
    jitter_fatal ("not every state structure was destroyed before PVM "
                  "finalisation.");
}




/* VM-dependant mutable routine initialization.
 * ************************************************************************** */

struct jitter_mutable_routine*
pvm_make_mutable_routine (void)
{
  return jitter_make_mutable_routine (pvm_vm);
}




/* Sample profiling: internal API.
 * ************************************************************************** */

#if defined (JITTER_PROFILE_SAMPLE)

/* Sample profiling depends on some system features: fail immediately if they
   are not available */
#if ! defined (JITTER_HAVE_SIGACTION) || ! defined (JITTER_HAVE_SETITIMER)
# jitter_fatal "sample-profiling depends on sigaction and setitimer"
#endif

static struct itimerval
pvm_timer_interval;

static struct itimerval
pvm_timer_disabled_interval;

/* The sampling data, currently global.  The current implementation does not
   play well with threads, but it can be changed later keeping the same user
   API. */
struct pvm_sample_profile_state
{
  /* The state currently sample-profiling.  Since such a state can be only one
     right now this field is useful for printing error messages in case the user
     sets up sample-profiling from two states at the same time by mistake.
     This field is also useful for temporarily suspending and then reenabling
     sampling, when The Array is being resized: if the signal handler sees that
     this field is NULL it will not touch the fields. */
  struct pvm_state *state_p;

  /* A pointer to the counts field within the sample_profile_runtime struct. */
  uint32_t *counts;

  /* A pointer to the current specialised instruction opcode within the
     sample_profile_runtime struct. */
  volatile jitter_int * specialized_opcode_p;

  /* A pointer to the field counting the number of samples, again within the
     sample_profile_runtime struct. */
  unsigned int *sample_no_p;
};

/* The (currently) one and only global state for sample-profiling. */
static struct pvm_sample_profile_state
pvm_sample_profile_state;

static void
pvm_sigprof_handler (int signal)
{
#if 0
  assert (pvm_sample_profile_state.state_p != NULL);
#endif

  jitter_int specialized_opcode
    = * pvm_sample_profile_state.specialized_opcode_p;
  if (__builtin_expect ((specialized_opcode >= 0
                         && (specialized_opcode
                             < PVM_SPECIALIZED_INSTRUCTION_NO)),
                        true))
    pvm_sample_profile_state.counts [specialized_opcode] ++;

  (* pvm_sample_profile_state.sample_no_p) ++;
}

void
pvm_profile_sample_initialize (void)
{
  /* Perform a sanity check over the sampling period. */
  if (JITTER_PROFILE_SAMPLE_PERIOD_IN_MILLISECONDS <= 0 ||
      JITTER_PROFILE_SAMPLE_PERIOD_IN_MILLISECONDS >= 1000)
    jitter_fatal ("invalid JITTER_PROFILE_SAMPLE_PERIOD_IN_MILLISECONDS: %f",
                  (double) JITTER_PROFILE_SAMPLE_PERIOD_IN_MILLISECONDS);
  struct sigaction action;
  sigaction (SIGPROF, NULL, & action);
  action.sa_handler = pvm_sigprof_handler;
  sigaction (SIGPROF, & action, NULL);

  long microseconds
    = (long) (JITTER_PROFILE_SAMPLE_PERIOD_IN_MILLISECONDS * 1000);
  pvm_timer_interval.it_interval.tv_sec = 0;
  pvm_timer_interval.it_interval.tv_usec = microseconds;
  pvm_timer_interval.it_value = pvm_timer_interval.it_interval;

  pvm_sample_profile_state.state_p = NULL;
  pvm_timer_disabled_interval.it_interval.tv_sec = 0;
  pvm_timer_disabled_interval.it_interval.tv_usec = 0;
  pvm_timer_disabled_interval.it_value
    = pvm_timer_disabled_interval.it_interval;
}

void
pvm_profile_sample_start (struct pvm_state *state_p)
{
  struct jitter_sample_profile_runtime *spr
    = ((struct jitter_sample_profile_runtime *)
       & PVM_STATE_TO_SPECIAL_PURPOSE_STATE_DATA (state_p)
           ->profile_runtime.sample_profile_runtime);

  if (pvm_sample_profile_state.state_p != NULL)
    {
      if (state_p != pvm_sample_profile_state.state_p)
        jitter_fatal ("currently it is only possible to sample-profile from "
                      "one state at the time: trying to sample-profile from "
                      "the state %p when already sample-profiling from the "
                      "state %p",
                      state_p, pvm_sample_profile_state.state_p);
      else
        {
          /* This situation is a symptom of a bug, but does not need to lead
             to a fatal error. */
          printf ("WARNING: starting profile on the state %p when profiling "
                  "was already active in the same state.\n"
                  "Did you call longjmp from VM code?", state_p);
          fflush (stdout);
        }
    }
  pvm_sample_profile_state.state_p = state_p;
  pvm_sample_profile_state.sample_no_p = & spr->sample_no;
  pvm_sample_profile_state.counts = spr->counts;
  pvm_sample_profile_state.specialized_opcode_p
    = & spr->current_specialized_instruction_opcode;
  //fprintf (stderr, "SAMPLE START\n"); fflush (NULL);
  if (setitimer (ITIMER_PROF, & pvm_timer_interval, NULL) != 0)
    jitter_fatal ("setitimer failed when establishing a timer");
}

void
pvm_profile_sample_stop (void)
{
  if (setitimer (ITIMER_PROF, & pvm_timer_disabled_interval, NULL) != 0)
    jitter_fatal ("setitimer failed when disabling a timer");

  pvm_sample_profile_state.state_p = NULL;

  /* The rest is just for defenisveness' sake. */
  * pvm_sample_profile_state.specialized_opcode_p = -1;
  pvm_sample_profile_state.sample_no_p = NULL;
  pvm_sample_profile_state.counts = NULL;
  pvm_sample_profile_state.specialized_opcode_p = NULL;
}
#endif // #if defined (JITTER_PROFILE_SAMPLE)




/* Array re-allocation.
 * ************************************************************************** */

char *
pvm_make_place_for_slow_registers (struct pvm_state *s,
                                        jitter_int new_slow_register_no_per_class)
{
  if (new_slow_register_no_per_class < 0)
    jitter_fatal ("pvm_make_place_for_slow_registers: negative slow "
                  "register number");
  jitter_int old_slow_register_no_per_class
    = s->pvm_state_backing.jitter_slow_register_no_per_class;
  /* Change nothing if we already have enough space for the required number of
     slow registers.  The no-change case will be the most common one, and
     this function might be worth optimizing. */
  if (__builtin_expect (new_slow_register_no_per_class
                        > old_slow_register_no_per_class,
                        false))
    {
#if defined (JITTER_PROFILE_SAMPLE)
      /* If sample-profiling is currently in progress on this state suspend it
         temporarily. */
      bool suspending_sample_profiling
        = (pvm_sample_profile_state.state_p == s);
      if (suspending_sample_profiling)
        pvm_profile_sample_stop ();
#endif // #if defined (JITTER_PROFILE_SAMPLE)

#if 0
      printf ("Increasing slow register-no (per class) from %li to %li\n", (long) old_slow_register_no_per_class, (long)new_slow_register_no_per_class);
      printf ("Array size %li -> %li\n", (long) PVM_ARRAY_SIZE(old_slow_register_no_per_class), (long) PVM_ARRAY_SIZE(new_slow_register_no_per_class));
#endif
      /* Save the new value for new_slow_register_no_per_class in the state
         structure; reallocate the Array. */
      s->pvm_state_backing.jitter_slow_register_no_per_class
        = new_slow_register_no_per_class;
      s->pvm_state_backing.jitter_array
        = jitter_xrealloc ((void *) s->pvm_state_backing.jitter_array,
                           PVM_ARRAY_SIZE(new_slow_register_no_per_class));

      /* Initialise the slow registers we have just added, for every class. */
      union pvm_any_register *first_slow_register
        = ((union pvm_any_register *)
           ((char *) s->pvm_state_backing.jitter_array
            + PVM_FIRST_SLOW_REGISTER_UNBIASED_OFFSET));
      jitter_int i;
      for (i = old_slow_register_no_per_class;
           i < new_slow_register_no_per_class;
           i ++)
        {
          /* A pointer to the i-th rank of slow registers.  Every register
             in the rank is new and in general (according to its class) may
             need initialisation. */
          union pvm_any_register *rank
            = first_slow_register + (i * PVM_REGISTER_CLASS_NO);
          PVM_INITIALIZE_SLOW_REGISTER_RANK (rank);
        }
#if defined (JITTER_PROFILE_SAMPLE)
      /* Now we can resume sample-profiling on this state if we suspended it. */
      if (suspending_sample_profiling)
        pvm_profile_sample_start (s);
#endif // #if defined (JITTER_PROFILE_SAMPLE)
#if 0
      printf ("Done resizing The Array\n");
#endif
    }

  /* Return the new (or unchanged) base, by simply adding the bias to the
     Array as it is now. */
  return s->pvm_state_backing.jitter_array + JITTER_ARRAY_BIAS;
}

void
pvm_ensure_enough_slow_registers_for_executable_routine
   (const struct jitter_executable_routine *er, struct pvm_state *s)
{
  pvm_make_place_for_slow_registers (s, er->slow_register_per_class_no);
}




/* Program text frontend.
 * ************************************************************************** */

void
pvm_parse_mutable_routine_from_file_star (FILE *input_file,
                                               struct jitter_mutable_routine *p)
{
  jitter_parse_mutable_routine_from_file_star (input_file, p, pvm_vm);
}

void
pvm_parse_mutable_routine_from_file (const char *input_file_name,
                                          struct jitter_mutable_routine *p)
{
  jitter_parse_mutable_routine_from_file (input_file_name, p, pvm_vm);
}

void
pvm_parse_mutable_routine_from_string (const char *string,
                                            struct jitter_mutable_routine *p)
{
  jitter_parse_mutable_routine_from_string (string, p, pvm_vm);
}




/* Executing code: unified routine API.
 * ************************************************************************** */

void
pvm_ensure_enough_slow_registers_for_routine
   (jitter_routine r, struct pvm_state *s)
{
  struct jitter_executable_routine *e
    = jitter_routine_make_executable_if_needed (r);
  pvm_ensure_enough_slow_registers_for_executable_routine (e, s);
}

void
pvm_execute_routine (jitter_routine r,
                          struct pvm_state *s)
{
  struct jitter_executable_routine *e
    = jitter_routine_make_executable_if_needed (r);
  pvm_execute_executable_routine (e, s);
}




/* Profiling: user API.
 * ************************************************************************** */

/* These functions are all trivial wrappers around the functionality declared
   in jitter/jitter-profile.h, hiding the VM pointer. */

struct pvm_profile_runtime *
pvm_state_profile_runtime (struct pvm_state *s)
{
  volatile struct jitter_special_purpose_state_data *spd
    = PVM_ARRAY_TO_SPECIAL_PURPOSE_STATE_DATA
        (s->pvm_state_backing.jitter_array);
  return (struct pvm_profile_runtime *) & spd->profile_runtime;
}

struct pvm_profile_runtime *
pvm_profile_runtime_make (void)
{
  return jitter_profile_runtime_make (pvm_vm);
}

void
pvm_profile_runtime_clear (struct pvm_profile_runtime * p)
{
  jitter_profile_runtime_clear (pvm_vm, p);
}

void
pvm_profile_runtime_merge_from (struct pvm_profile_runtime *to,
                                     const struct pvm_profile_runtime *from)
{
  jitter_profile_runtime_merge_from (pvm_vm, to, from);
}

void
pvm_profile_runtime_merge_from_state (struct pvm_profile_runtime *to,
                                           const struct pvm_state *from_state)
{
  const struct pvm_profile_runtime* from
    = pvm_state_profile_runtime ((struct pvm_state *) from_state);
  jitter_profile_runtime_merge_from (pvm_vm, to, from);
}

void
pvm_profile_runtime_print_unspecialized
   (jitter_print_context ct,
    const struct pvm_profile_runtime *p)
{
  jitter_profile_runtime_print_unspecialized (ct, pvm_vm, p);
}

void
pvm_profile_runtime_print_specialized (jitter_print_context ct,
                                            const struct pvm_profile_runtime
                                            *p)
{
  jitter_profile_runtime_print_specialized (ct, pvm_vm, p);
}

struct pvm_profile *
pvm_profile_unspecialized_from_runtime
   (const struct pvm_profile_runtime *p)
{
  return jitter_profile_unspecialized_from_runtime (pvm_vm, p);
}

struct pvm_profile *
pvm_profile_specialized_from_runtime (const struct pvm_profile_runtime
                                           *p)
{
  return jitter_profile_specialized_from_runtime (pvm_vm, p);
}




/* Evrything following this point is machine-generated.
 * ************************************************************************** */

/* What follows could be conceptually split into several generated C files, but
   having too many of them would be inconvenient for the user to compile and
   link.  For this reason we currently generate just three files: one is this,
   which also contains the specializer, another is for the executor, and then a
   header -- a main module is optional.  The executor will be potentially very
   large, so it is best compiled separately.  The specializer might be large as
   well at this stage, even if its compilation is usually much less
   expensive. */
/* These two macros are convenient for making VM-specific identifiers
   using VM-independent macros from a public header, without polluting
   the global namespace. */
#define JITTER_VM_PREFIX_LOWER_CASE pvm
#define JITTER_VM_PREFIX_UPPER_CASE PVM

/* User-specified code, printer part: beginning. */
#line 716 "../../libpoke/pvm.jitter"
#line 716 "../../libpoke/pvm.jitter"

    static jitter_uint printer_hi;

    static void
    pvm_literal_printer_cast (jitter_print_context out, jitter_uint val)
    {
      pk_printf ("%" JITTER_PRIu, val);
      pk_term_flush ();
    }

    static void
    pvm_literal_printer (jitter_print_context out, jitter_uint val)
    {
      pvm_print_val_with_params (NULL /* not used since no
                                         pretty-print */,
                                 (pvm_val) val,
                                 1 /* depth */,
                                 PVM_PRINT_FLAT,
                                 16 /* base */,
                                 0 /* indent */,
                                 2 /* acutoff */,
                                 0 /* flags */);
      pk_term_flush ();
    }

    static void
    pvm_literal_printer_hi (jitter_print_context out, jitter_uint hi)
    {
      pk_printf ("%%hi(0x%" JITTER_PRIx ")", hi);
      pk_term_flush ();
      printer_hi = hi; /* This sucks */
    }

    static void
    pvm_literal_printer_lo (jitter_print_context out, jitter_uint lo)
    {
      pk_printf ("%%lo(0x%" JITTER_PRIx") (", lo);

      pvm_print_val_with_params (NULL /* not used since no
                                         pretty-print */,
                                 ((pvm_val) printer_hi << 32) | lo,
                                 1 /* depth */,
                                 PVM_PRINT_FLAT,
                                 16 /* base */,
                                 0 /* indent */,
                                 2 /* acutoff */,
                                 0 /* flags */);
      pk_puts (")");
      pk_term_flush ();
      printer_hi = 0;
    }

    static void
    popf_printer (jitter_print_context out, jitter_uint nframes)
    {
      pk_printf ("%" JITTER_PRIu, nframes);
      pk_term_flush ();
    }

    static void
    bits_printer (jitter_print_context out, jitter_uint val)
    {
      pk_printf ("%" JITTER_PRIu, val);
      pk_term_flush ();
    }

    static void
    endian_printer (jitter_print_context out, jitter_uint val)
    {
      pk_printf ("%s", val == IOS_ENDIAN_MSB ? "big" : "little");
      pk_term_flush ();
    }

    static void
    nenc_printer (jitter_print_context out, jitter_uint val)
    {
      pk_printf ("%s", val == IOS_NENC_1 ? "1c" : "2c");
      pk_term_flush ();
    }
  
/* User-specified code, printer part: end */

//#include <stdbool.h>

//#include <jitter/jitter.h>
//#include <jitter/jitter-instruction.h>

//#include "pvm-meta-instructions.h"

// FIXME: comment.
struct jitter_hash_table
pvm_meta_instruction_hash;


static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_ba_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_bn_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_bnn_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_bnzi_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_bnziu_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_bnzl_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_bnzlu_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_bzi_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_bziu_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_bzl_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_bzlu_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_itoi_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, pvm_literal_printer_cast } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_itoiu_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, pvm_literal_printer_cast } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_itol_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, pvm_literal_printer_cast } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_itolu_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, pvm_literal_printer_cast } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_iutoi_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, pvm_literal_printer_cast } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_iutoiu_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, pvm_literal_printer_cast } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_iutol_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, pvm_literal_printer_cast } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_iutolu_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, pvm_literal_printer_cast } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_ltoi_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, pvm_literal_printer_cast } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_ltoiu_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, pvm_literal_printer_cast } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_ltol_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, pvm_literal_printer_cast } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_ltolu_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, pvm_literal_printer_cast } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_lutoi_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, pvm_literal_printer_cast } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_lutoiu_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, pvm_literal_printer_cast } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_lutol_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, pvm_literal_printer_cast } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_lutolu_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, pvm_literal_printer_cast } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_note_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, pvm_literal_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_peekdi_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, bits_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_peekdiu_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, bits_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_peekdl_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, bits_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_peekdlu_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, bits_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_peeki_meta_instruction_parameter_types [3] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, nenc_printer }, { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, endian_printer }, { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, bits_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_peekiu_meta_instruction_parameter_types [2] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, endian_printer }, { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, bits_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_peekl_meta_instruction_parameter_types [3] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, nenc_printer }, { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, endian_printer }, { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, bits_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_peeklu_meta_instruction_parameter_types [2] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, endian_printer }, { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, bits_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_pokedi_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, bits_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_pokediu_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, bits_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_pokedl_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, bits_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_pokedlu_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, bits_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_pokei_meta_instruction_parameter_types [3] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, nenc_printer }, { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, endian_printer }, { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, bits_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_pokeiu_meta_instruction_parameter_types [2] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, endian_printer }, { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, bits_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_pokel_meta_instruction_parameter_types [3] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, nenc_printer }, { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, endian_printer }, { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, bits_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_pokelu_meta_instruction_parameter_types [2] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, endian_printer }, { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, bits_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_popf_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, popf_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_popr_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_register, & pvm_register_class_r, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_popvar_meta_instruction_parameter_types [2] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, jitter_default_literal_parameter_printer }, { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_printi_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, popf_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_printiu_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, popf_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_printl_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, popf_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_printlu_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, popf_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_push_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum_or_literal_label, NULL, pvm_literal_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_push32_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum_or_literal_label, NULL, pvm_literal_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_pushe_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_pushf_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, popf_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_pushhi_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum_or_literal_label, NULL, pvm_literal_printer_hi } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_pushlo_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum_or_literal_label, NULL, pvm_literal_printer_lo } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_pushr_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_register, & pvm_register_class_r, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_pushtopvar_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_pushvar_meta_instruction_parameter_types [2] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, jitter_default_literal_parameter_printer }, { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_restorer_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_register, & pvm_register_class_r, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_revn_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, popf_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_saver_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_register, & pvm_register_class_r, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_setr_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_register, & pvm_register_class_r, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_strace_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, jitter_default_literal_parameter_printer } };


const struct jitter_meta_instruction
pvm_meta_instructions [PVM_META_INSTRUCTION_NO]
  = {
      { 0, "addi", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 1, "addiu", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 2, "addl", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 3, "addlu", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 4, "ains", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 5, "and", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 6, "aref", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 7, "arefo", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 8, "arem", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 9, "aset", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 10, "asettb", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 11, "atr", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 12, "ba", 1, false, false, true /* FIXME: this may be wrong with replacements. */, pvm_ba_meta_instruction_parameter_types },
      { 13, "bandi", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 14, "bandiu", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 15, "bandl", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 16, "bandlu", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 17, "beghl", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 18, "begsc", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 19, "bn", 1, false, false, true /* FIXME: this may be wrong with replacements. */, pvm_bn_meta_instruction_parameter_types },
      { 20, "bnn", 1, false, false, true /* FIXME: this may be wrong with replacements. */, pvm_bnn_meta_instruction_parameter_types },
      { 21, "bnoti", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 22, "bnotiu", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 23, "bnotl", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 24, "bnotlu", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 25, "bnzi", 1, false, false, true /* FIXME: this may be wrong with replacements. */, pvm_bnzi_meta_instruction_parameter_types },
      { 26, "bnziu", 1, false, false, true /* FIXME: this may be wrong with replacements. */, pvm_bnziu_meta_instruction_parameter_types },
      { 27, "bnzl", 1, false, false, true /* FIXME: this may be wrong with replacements. */, pvm_bnzl_meta_instruction_parameter_types },
      { 28, "bnzlu", 1, false, false, true /* FIXME: this may be wrong with replacements. */, pvm_bnzlu_meta_instruction_parameter_types },
      { 29, "bori", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 30, "boriu", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 31, "borl", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 32, "borlu", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 33, "bsli", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 34, "bsliu", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 35, "bsll", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 36, "bsllu", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 37, "bsri", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 38, "bsriu", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 39, "bsrl", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 40, "bsrlu", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 41, "bxori", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 42, "bxoriu", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 43, "bxorl", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 44, "bxorlu", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 45, "bzi", 1, false, false, true /* FIXME: this may be wrong with replacements. */, pvm_bzi_meta_instruction_parameter_types },
      { 46, "bziu", 1, false, false, true /* FIXME: this may be wrong with replacements. */, pvm_bziu_meta_instruction_parameter_types },
      { 47, "bzl", 1, false, false, true /* FIXME: this may be wrong with replacements. */, pvm_bzl_meta_instruction_parameter_types },
      { 48, "bzlu", 1, false, false, true /* FIXME: this may be wrong with replacements. */, pvm_bzlu_meta_instruction_parameter_types },
      { 49, "call", 0, true, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 50, "canary", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 51, "close", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 52, "ctos", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 53, "disas", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 54, "divi", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 55, "diviu", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 56, "divl", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 57, "divlu", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 58, "drop", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 59, "drop2", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 60, "drop3", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 61, "drop4", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 62, "duc", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 63, "dup", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 64, "endhl", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 65, "endsc", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 66, "eqi", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 67, "eqiu", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 68, "eql", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 69, "eqlu", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 70, "eqs", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 71, "exit", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 72, "exitvm", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 73, "flush", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 74, "fromr", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 75, "gei", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 76, "geiu", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 77, "gel", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 78, "gelu", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 79, "ges", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 80, "getenv", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 81, "gti", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 82, "gtiu", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 83, "gtl", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 84, "gtlu", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 85, "gts", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 86, "indent", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 87, "iogetb", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 88, "iosetb", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 89, "iosize", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 90, "isa", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 91, "itoi", 1, false, false, true /* FIXME: this may be wrong with replacements. */, pvm_itoi_meta_instruction_parameter_types },
      { 92, "itoiu", 1, false, false, true /* FIXME: this may be wrong with replacements. */, pvm_itoiu_meta_instruction_parameter_types },
      { 93, "itol", 1, false, false, true /* FIXME: this may be wrong with replacements. */, pvm_itol_meta_instruction_parameter_types },
      { 94, "itolu", 1, false, false, true /* FIXME: this may be wrong with replacements. */, pvm_itolu_meta_instruction_parameter_types },
      { 95, "iutoi", 1, false, false, true /* FIXME: this may be wrong with replacements. */, pvm_iutoi_meta_instruction_parameter_types },
      { 96, "iutoiu", 1, false, false, true /* FIXME: this may be wrong with replacements. */, pvm_iutoiu_meta_instruction_parameter_types },
      { 97, "iutol", 1, false, false, true /* FIXME: this may be wrong with replacements. */, pvm_iutol_meta_instruction_parameter_types },
      { 98, "iutolu", 1, false, false, true /* FIXME: this may be wrong with replacements. */, pvm_iutolu_meta_instruction_parameter_types },
      { 99, "lei", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 100, "leiu", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 101, "lel", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 102, "lelu", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 103, "les", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 104, "lti", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 105, "ltiu", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 106, "ltl", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 107, "ltlu", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 108, "ltoi", 1, false, false, true /* FIXME: this may be wrong with replacements. */, pvm_ltoi_meta_instruction_parameter_types },
      { 109, "ltoiu", 1, false, false, true /* FIXME: this may be wrong with replacements. */, pvm_ltoiu_meta_instruction_parameter_types },
      { 110, "ltol", 1, false, false, true /* FIXME: this may be wrong with replacements. */, pvm_ltol_meta_instruction_parameter_types },
      { 111, "ltolu", 1, false, false, true /* FIXME: this may be wrong with replacements. */, pvm_ltolu_meta_instruction_parameter_types },
      { 112, "lts", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 113, "lutoi", 1, false, false, true /* FIXME: this may be wrong with replacements. */, pvm_lutoi_meta_instruction_parameter_types },
      { 114, "lutoiu", 1, false, false, true /* FIXME: this may be wrong with replacements. */, pvm_lutoiu_meta_instruction_parameter_types },
      { 115, "lutol", 1, false, false, true /* FIXME: this may be wrong with replacements. */, pvm_lutol_meta_instruction_parameter_types },
      { 116, "lutolu", 1, false, false, true /* FIXME: this may be wrong with replacements. */, pvm_lutolu_meta_instruction_parameter_types },
      { 117, "map", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 118, "mgetios", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 119, "mgetm", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 120, "mgeto", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 121, "mgets", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 122, "mgetsel", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 123, "mgetsiz", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 124, "mgetw", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 125, "mka", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 126, "mko", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 127, "mksct", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 128, "mktya", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 129, "mktyany", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 130, "mktyc", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 131, "mktyi", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 132, "mktyo", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 133, "mktys", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 134, "mktysct", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 135, "mktyv", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 136, "mm", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 137, "modi", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 138, "modiu", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 139, "modl", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 140, "modlu", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 141, "msetios", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 142, "msetm", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 143, "mseto", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 144, "msets", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 145, "msetsel", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 146, "msetsiz", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 147, "msetw", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 148, "muli", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 149, "muliu", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 150, "mull", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 151, "mullu", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 152, "muls", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 153, "negi", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 154, "negiu", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 155, "negl", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 156, "neglu", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 157, "nei", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 158, "neiu", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 159, "nel", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 160, "nelu", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 161, "nes", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 162, "nip", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 163, "nip2", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 164, "nip3", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 165, "nn", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 166, "nnn", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 167, "nop", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 168, "not", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 169, "note", 1, false, false, true /* FIXME: this may be wrong with replacements. */, pvm_note_meta_instruction_parameter_types },
      { 170, "nrot", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 171, "ogetbt", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 172, "ogetm", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 173, "ogetu", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 174, "open", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 175, "or", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 176, "osetm", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 177, "over", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 178, "pec", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 179, "peekdi", 1, false, false, true /* FIXME: this may be wrong with replacements. */, pvm_peekdi_meta_instruction_parameter_types },
      { 180, "peekdiu", 1, false, false, true /* FIXME: this may be wrong with replacements. */, pvm_peekdiu_meta_instruction_parameter_types },
      { 181, "peekdl", 1, false, false, true /* FIXME: this may be wrong with replacements. */, pvm_peekdl_meta_instruction_parameter_types },
      { 182, "peekdlu", 1, false, false, true /* FIXME: this may be wrong with replacements. */, pvm_peekdlu_meta_instruction_parameter_types },
      { 183, "peeki", 3, false, false, true /* FIXME: this may be wrong with replacements. */, pvm_peeki_meta_instruction_parameter_types },
      { 184, "peekiu", 2, false, false, true /* FIXME: this may be wrong with replacements. */, pvm_peekiu_meta_instruction_parameter_types },
      { 185, "peekl", 3, false, false, true /* FIXME: this may be wrong with replacements. */, pvm_peekl_meta_instruction_parameter_types },
      { 186, "peeklu", 2, false, false, true /* FIXME: this may be wrong with replacements. */, pvm_peeklu_meta_instruction_parameter_types },
      { 187, "peeks", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 188, "pokedi", 1, false, false, true /* FIXME: this may be wrong with replacements. */, pvm_pokedi_meta_instruction_parameter_types },
      { 189, "pokediu", 1, false, false, true /* FIXME: this may be wrong with replacements. */, pvm_pokediu_meta_instruction_parameter_types },
      { 190, "pokedl", 1, false, false, true /* FIXME: this may be wrong with replacements. */, pvm_pokedl_meta_instruction_parameter_types },
      { 191, "pokedlu", 1, false, false, true /* FIXME: this may be wrong with replacements. */, pvm_pokedlu_meta_instruction_parameter_types },
      { 192, "pokei", 3, false, false, true /* FIXME: this may be wrong with replacements. */, pvm_pokei_meta_instruction_parameter_types },
      { 193, "pokeiu", 2, false, false, true /* FIXME: this may be wrong with replacements. */, pvm_pokeiu_meta_instruction_parameter_types },
      { 194, "pokel", 3, false, false, true /* FIXME: this may be wrong with replacements. */, pvm_pokel_meta_instruction_parameter_types },
      { 195, "pokelu", 2, false, false, true /* FIXME: this may be wrong with replacements. */, pvm_pokelu_meta_instruction_parameter_types },
      { 196, "pokes", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 197, "pope", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 198, "popend", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 199, "popf", 1, false, false, true /* FIXME: this may be wrong with replacements. */, pvm_popf_meta_instruction_parameter_types },
      { 200, "popios", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 201, "popoac", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 202, "popob", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 203, "popobc", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 204, "popoc", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 205, "popod", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 206, "popoi", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 207, "popom", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 208, "popoo", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 209, "popopp", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 210, "popr", 1, false, false, true /* FIXME: this may be wrong with replacements. */, pvm_popr_meta_instruction_parameter_types },
      { 211, "popvar", 2, false, false, true /* FIXME: this may be wrong with replacements. */, pvm_popvar_meta_instruction_parameter_types },
      { 212, "powi", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 213, "powiu", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 214, "powl", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 215, "powlu", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 216, "printi", 1, false, false, true /* FIXME: this may be wrong with replacements. */, pvm_printi_meta_instruction_parameter_types },
      { 217, "printiu", 1, false, false, true /* FIXME: this may be wrong with replacements. */, pvm_printiu_meta_instruction_parameter_types },
      { 218, "printl", 1, false, false, true /* FIXME: this may be wrong with replacements. */, pvm_printl_meta_instruction_parameter_types },
      { 219, "printlu", 1, false, false, true /* FIXME: this may be wrong with replacements. */, pvm_printlu_meta_instruction_parameter_types },
      { 220, "prints", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 221, "prolog", 0, false, true, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 222, "push", 1, false, false, true /* FIXME: this may be wrong with replacements. */, pvm_push_meta_instruction_parameter_types },
      { 223, "push32", 1, false, false, true /* FIXME: this may be wrong with replacements. */, pvm_push32_meta_instruction_parameter_types },
      { 224, "pushe", 1, false, false, true /* FIXME: this may be wrong with replacements. */, pvm_pushe_meta_instruction_parameter_types },
      { 225, "pushend", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 226, "pushf", 1, false, false, true /* FIXME: this may be wrong with replacements. */, pvm_pushf_meta_instruction_parameter_types },
      { 227, "pushhi", 1, false, false, true /* FIXME: this may be wrong with replacements. */, pvm_pushhi_meta_instruction_parameter_types },
      { 228, "pushios", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 229, "pushlo", 1, false, false, true /* FIXME: this may be wrong with replacements. */, pvm_pushlo_meta_instruction_parameter_types },
      { 230, "pushoac", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 231, "pushob", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 232, "pushobc", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 233, "pushoc", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 234, "pushod", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 235, "pushoi", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 236, "pushom", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 237, "pushoo", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 238, "pushopp", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 239, "pushr", 1, false, false, true /* FIXME: this may be wrong with replacements. */, pvm_pushr_meta_instruction_parameter_types },
      { 240, "pushtopvar", 1, false, false, true /* FIXME: this may be wrong with replacements. */, pvm_pushtopvar_meta_instruction_parameter_types },
      { 241, "pushvar", 2, false, false, true /* FIXME: this may be wrong with replacements. */, pvm_pushvar_meta_instruction_parameter_types },
      { 242, "quake", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 243, "raise", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 244, "rand", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 245, "regvar", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 246, "reloc", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 247, "restorer", 1, false, false, true /* FIXME: this may be wrong with replacements. */, pvm_restorer_meta_instruction_parameter_types },
      { 248, "return", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 249, "revn", 1, false, false, true /* FIXME: this may be wrong with replacements. */, pvm_revn_meta_instruction_parameter_types },
      { 250, "rot", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 251, "saver", 1, false, false, true /* FIXME: this may be wrong with replacements. */, pvm_saver_meta_instruction_parameter_types },
      { 252, "sconc", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 253, "sel", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 254, "setr", 1, false, false, true /* FIXME: this may be wrong with replacements. */, pvm_setr_meta_instruction_parameter_types },
      { 255, "siz", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 256, "smodi", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 257, "sref", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 258, "srefi", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 259, "srefia", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 260, "srefio", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 261, "srefmnt", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 262, "srefnt", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 263, "srefo", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 264, "sset", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 265, "strace", 1, false, false, true /* FIXME: this may be wrong with replacements. */, pvm_strace_meta_instruction_parameter_types },
      { 266, "strref", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 267, "subi", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 268, "subiu", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 269, "subl", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 270, "sublu", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 271, "substr", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 272, "swap", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 273, "swapgti", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 274, "swapgtiu", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 275, "swapgtl", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 276, "swapgtlu", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 277, "sync", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 278, "time", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 279, "tor", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 280, "tuck", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 281, "tyagetb", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 282, "tyagett", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 283, "tyisc", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 284, "tyissct", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 285, "typof", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 286, "tysctn", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 287, "unmap", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 288, "unreachable", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 289, "ureloc", 0, false, false, true /* FIXME: this may be wrong with replacements. */, NULL },
      { 290, "write", 0, true, false, true /* FIXME: this may be wrong with replacements. */, NULL }
    };

/* The register class descriptor for r registers. */
const struct jitter_register_class
pvm_register_class_r
  = {
      pvm_register_class_id_r,
      'r',
      "register_class_r",
      "REGISTER_CLASS_R",
      PVM_REGISTER_r_FAST_REGISTER_NO,
      1 /* Use slow registers */
    };


/* A pointer to every existing register class descriptor. */
const struct jitter_register_class * const
pvm_regiter_classes []
  = {
      & pvm_register_class_r
    };

const struct jitter_register_class *
pvm_register_class_character_to_register_class (char c)
{
  switch (c)
    {
    case 'r': return & pvm_register_class_r;
    default:  return NULL;
    }
}

//#include "pvm-specialized-instructions.h"

const char * const
pvm_specialized_instruction_names [PVM_SPECIALIZED_INSTRUCTION_NO]
  = {
      "!INVALID",
      "!BEGINBASICBLOCK",
      "!EXITVM",
      "!DATALOCATIONS",
      "!NOP",
      "!UNREACHABLE0",
      "!UNREACHABLE1",
      "!UNREACHABLE2",
      "addi",
      "addiu",
      "addl",
      "addlu",
      "ains",
      "and",
      "aref",
      "arefo",
      "arem",
      "aset",
      "asettb",
      "atr",
      "ba/fR",
      "bandi",
      "bandiu",
      "bandl",
      "bandlu",
      "beghl",
      "begsc",
      "bn/fR",
      "bnn/fR",
      "bnoti",
      "bnotiu",
      "bnotl",
      "bnotlu",
      "bnzi/fR",
      "bnziu/fR",
      "bnzl/fR",
      "bnzlu/fR",
      "bori",
      "boriu",
      "borl",
      "borlu",
      "bsli",
      "bsliu",
      "bsll",
      "bsllu",
      "bsri",
      "bsriu",
      "bsrl",
      "bsrlu",
      "bxori",
      "bxoriu",
      "bxorl",
      "bxorlu",
      "bzi/fR",
      "bziu/fR",
      "bzl/fR",
      "bzlu/fR",
      "call/retR",
      "canary",
      "close",
      "ctos",
      "disas",
      "divi",
      "diviu",
      "divl",
      "divlu",
      "drop",
      "drop2",
      "drop3",
      "drop4",
      "duc",
      "dup",
      "endhl",
      "endsc",
      "eqi",
      "eqiu",
      "eql",
      "eqlu",
      "eqs",
      "exit",
      "exitvm",
      "flush",
      "fromr",
      "gei",
      "geiu",
      "gel",
      "gelu",
      "ges",
      "getenv",
      "gti",
      "gtiu",
      "gtl",
      "gtlu",
      "gts",
      "indent",
      "iogetb",
      "iosetb",
      "iosize",
      "isa",
      "itoi/nR",
      "itoiu/nR",
      "itol/nR",
      "itolu/nR",
      "iutoi/nR",
      "iutoiu/nR",
      "iutol/nR",
      "iutolu/nR",
      "lei",
      "leiu",
      "lel",
      "lelu",
      "les",
      "lti",
      "ltiu",
      "ltl",
      "ltlu",
      "ltoi/nR",
      "ltoiu/nR",
      "ltol/nR",
      "ltolu/nR",
      "lts",
      "lutoi/nR",
      "lutoiu/nR",
      "lutol/nR",
      "lutolu/nR",
      "map",
      "mgetios",
      "mgetm",
      "mgeto",
      "mgets",
      "mgetsel",
      "mgetsiz",
      "mgetw",
      "mka",
      "mko",
      "mksct",
      "mktya",
      "mktyany",
      "mktyc",
      "mktyi",
      "mktyo",
      "mktys",
      "mktysct",
      "mktyv",
      "mm",
      "modi",
      "modiu",
      "modl",
      "modlu",
      "msetios",
      "msetm",
      "mseto",
      "msets",
      "msetsel",
      "msetsiz",
      "msetw",
      "muli",
      "muliu",
      "mull",
      "mullu",
      "muls",
      "negi",
      "negiu",
      "negl",
      "neglu",
      "nei",
      "neiu",
      "nel",
      "nelu",
      "nes",
      "nip",
      "nip2",
      "nip3",
      "nn",
      "nnn",
      "nop",
      "not",
      "note/nR",
      "nrot",
      "ogetbt",
      "ogetm",
      "ogetu",
      "open",
      "or",
      "osetm",
      "over",
      "pec",
      "peekdi/nR",
      "peekdiu/nR",
      "peekdl/nR",
      "peekdlu/nR",
      "peeki/nR/nR/nR",
      "peekiu/nR/nR",
      "peekl/nR/nR/nR",
      "peeklu/nR/nR",
      "peeks",
      "pokedi/nR",
      "pokediu/nR",
      "pokedl/nR",
      "pokedlu/nR",
      "pokei/nR/nR/nR",
      "pokeiu/nR/nR",
      "pokel/nR/nR/nR",
      "pokelu/nR/nR",
      "pokes",
      "pope",
      "popend",
      "popf/nR",
      "popios",
      "popoac",
      "popob",
      "popobc",
      "popoc",
      "popod",
      "popoi",
      "popom",
      "popoo",
      "popopp",
      "popr/%rR",
      "popvar/nR/nR",
      "powi",
      "powiu",
      "powl",
      "powlu",
      "printi/nR",
      "printiu/nR",
      "printl/nR",
      "printlu/nR",
      "prints",
      "prolog",
      "push/nR",
      "push/lR",
      "push32/nR",
      "push32/lR",
      "pushe/lR",
      "pushend",
      "pushf/nR",
      "pushhi/nR",
      "pushhi/lR",
      "pushios",
      "pushlo/nR",
      "pushlo/lR",
      "pushoac",
      "pushob",
      "pushobc",
      "pushoc",
      "pushod",
      "pushoi",
      "pushom",
      "pushoo",
      "pushopp",
      "pushr/%rR",
      "pushtopvar/nR",
      "pushvar/n0/n0",
      "pushvar/n0/n1",
      "pushvar/n0/n2",
      "pushvar/n0/n3",
      "pushvar/n0/n4",
      "pushvar/n0/n5",
      "pushvar/n0/nR",
      "pushvar/nR/n0",
      "pushvar/nR/n1",
      "pushvar/nR/n2",
      "pushvar/nR/n3",
      "pushvar/nR/n4",
      "pushvar/nR/n5",
      "pushvar/nR/nR",
      "quake",
      "raise",
      "rand",
      "regvar",
      "reloc",
      "restorer/%rR",
      "return",
      "revn/n3",
      "revn/n4",
      "revn/nR",
      "rot",
      "saver/%rR",
      "sconc",
      "sel",
      "setr/%rR",
      "siz",
      "smodi",
      "sref",
      "srefi",
      "srefia",
      "srefio",
      "srefmnt",
      "srefnt",
      "srefo",
      "sset",
      "strace/nR",
      "strref",
      "subi",
      "subiu",
      "subl",
      "sublu",
      "substr",
      "swap",
      "swapgti",
      "swapgtiu",
      "swapgtl",
      "swapgtlu",
      "sync",
      "time",
      "tor",
      "tuck",
      "tyagetb",
      "tyagett",
      "tyisc",
      "tyissct",
      "typof",
      "tysctn",
      "unmap",
      "unreachable",
      "ureloc",
      "write/retR",
      "*ba/fR*-no-fast-branches",
      "*bn/fR*-no-fast-branches",
      "*bnn/fR*-no-fast-branches",
      "*bnzi/fR*-no-fast-branches",
      "*bnziu/fR*-no-fast-branches",
      "*bnzl/fR*-no-fast-branches",
      "*bnzlu/fR*-no-fast-branches",
      "*bzi/fR*-no-fast-branches",
      "*bziu/fR*-no-fast-branches",
      "*bzl/fR*-no-fast-branches",
      "*bzlu/fR*-no-fast-branches"
    };
// #include <stdlib.h>

// #include "pvm-specialized-instructions.h"
const size_t
pvm_specialized_instruction_residual_arities [PVM_SPECIALIZED_INSTRUCTION_NO]
  = {
      0, /* !INVALID */
      1, /* !BEGINBASICBLOCK */
      0, /* !EXITVM */
      0, /* !DATALOCATIONS */
      0, /* !NOP */
      0, /* !UNREACHABLE0 */
      0, /* !UNREACHABLE1 */
      0, /* !UNREACHABLE2 */
      0, /* addi */
      0, /* addiu */
      0, /* addl */
      0, /* addlu */
      0, /* ains */
      0, /* and */
      0, /* aref */
      0, /* arefo */
      0, /* arem */
      0, /* aset */
      0, /* asettb */
      0, /* atr */
      1, /* ba/fR */
      0, /* bandi */
      0, /* bandiu */
      0, /* bandl */
      0, /* bandlu */
      0, /* beghl */
      0, /* begsc */
      1, /* bn/fR */
      1, /* bnn/fR */
      0, /* bnoti */
      0, /* bnotiu */
      0, /* bnotl */
      0, /* bnotlu */
      1, /* bnzi/fR */
      1, /* bnziu/fR */
      1, /* bnzl/fR */
      1, /* bnzlu/fR */
      0, /* bori */
      0, /* boriu */
      0, /* borl */
      0, /* borlu */
      0, /* bsli */
      0, /* bsliu */
      0, /* bsll */
      0, /* bsllu */
      0, /* bsri */
      0, /* bsriu */
      0, /* bsrl */
      0, /* bsrlu */
      0, /* bxori */
      0, /* bxoriu */
      0, /* bxorl */
      0, /* bxorlu */
      1, /* bzi/fR */
      1, /* bziu/fR */
      1, /* bzl/fR */
      1, /* bzlu/fR */
      1, /* call/retR */
      0, /* canary */
      0, /* close */
      0, /* ctos */
      0, /* disas */
      0, /* divi */
      0, /* diviu */
      0, /* divl */
      0, /* divlu */
      0, /* drop */
      0, /* drop2 */
      0, /* drop3 */
      0, /* drop4 */
      0, /* duc */
      0, /* dup */
      0, /* endhl */
      0, /* endsc */
      0, /* eqi */
      0, /* eqiu */
      0, /* eql */
      0, /* eqlu */
      0, /* eqs */
      0, /* exit */
      0, /* exitvm */
      0, /* flush */
      0, /* fromr */
      0, /* gei */
      0, /* geiu */
      0, /* gel */
      0, /* gelu */
      0, /* ges */
      0, /* getenv */
      0, /* gti */
      0, /* gtiu */
      0, /* gtl */
      0, /* gtlu */
      0, /* gts */
      0, /* indent */
      0, /* iogetb */
      0, /* iosetb */
      0, /* iosize */
      0, /* isa */
      1, /* itoi/nR */
      1, /* itoiu/nR */
      1, /* itol/nR */
      1, /* itolu/nR */
      1, /* iutoi/nR */
      1, /* iutoiu/nR */
      1, /* iutol/nR */
      1, /* iutolu/nR */
      0, /* lei */
      0, /* leiu */
      0, /* lel */
      0, /* lelu */
      0, /* les */
      0, /* lti */
      0, /* ltiu */
      0, /* ltl */
      0, /* ltlu */
      1, /* ltoi/nR */
      1, /* ltoiu/nR */
      1, /* ltol/nR */
      1, /* ltolu/nR */
      0, /* lts */
      1, /* lutoi/nR */
      1, /* lutoiu/nR */
      1, /* lutol/nR */
      1, /* lutolu/nR */
      0, /* map */
      0, /* mgetios */
      0, /* mgetm */
      0, /* mgeto */
      0, /* mgets */
      0, /* mgetsel */
      0, /* mgetsiz */
      0, /* mgetw */
      0, /* mka */
      0, /* mko */
      0, /* mksct */
      0, /* mktya */
      0, /* mktyany */
      0, /* mktyc */
      0, /* mktyi */
      0, /* mktyo */
      0, /* mktys */
      0, /* mktysct */
      0, /* mktyv */
      0, /* mm */
      0, /* modi */
      0, /* modiu */
      0, /* modl */
      0, /* modlu */
      0, /* msetios */
      0, /* msetm */
      0, /* mseto */
      0, /* msets */
      0, /* msetsel */
      0, /* msetsiz */
      0, /* msetw */
      0, /* muli */
      0, /* muliu */
      0, /* mull */
      0, /* mullu */
      0, /* muls */
      0, /* negi */
      0, /* negiu */
      0, /* negl */
      0, /* neglu */
      0, /* nei */
      0, /* neiu */
      0, /* nel */
      0, /* nelu */
      0, /* nes */
      0, /* nip */
      0, /* nip2 */
      0, /* nip3 */
      0, /* nn */
      0, /* nnn */
      0, /* nop */
      0, /* not */
      1, /* note/nR */
      0, /* nrot */
      0, /* ogetbt */
      0, /* ogetm */
      0, /* ogetu */
      0, /* open */
      0, /* or */
      0, /* osetm */
      0, /* over */
      0, /* pec */
      1, /* peekdi/nR */
      1, /* peekdiu/nR */
      1, /* peekdl/nR */
      1, /* peekdlu/nR */
      3, /* peeki/nR/nR/nR */
      2, /* peekiu/nR/nR */
      3, /* peekl/nR/nR/nR */
      2, /* peeklu/nR/nR */
      0, /* peeks */
      1, /* pokedi/nR */
      1, /* pokediu/nR */
      1, /* pokedl/nR */
      1, /* pokedlu/nR */
      3, /* pokei/nR/nR/nR */
      2, /* pokeiu/nR/nR */
      3, /* pokel/nR/nR/nR */
      2, /* pokelu/nR/nR */
      0, /* pokes */
      0, /* pope */
      0, /* popend */
      1, /* popf/nR */
      0, /* popios */
      0, /* popoac */
      0, /* popob */
      0, /* popobc */
      0, /* popoc */
      0, /* popod */
      0, /* popoi */
      0, /* popom */
      0, /* popoo */
      0, /* popopp */
      1, /* popr/%rR */
      2, /* popvar/nR/nR */
      0, /* powi */
      0, /* powiu */
      0, /* powl */
      0, /* powlu */
      1, /* printi/nR */
      1, /* printiu/nR */
      1, /* printl/nR */
      1, /* printlu/nR */
      0, /* prints */
      0, /* prolog */
      1, /* push/nR */
      1, /* push/lR */
      1, /* push32/nR */
      1, /* push32/lR */
      1, /* pushe/lR */
      0, /* pushend */
      1, /* pushf/nR */
      1, /* pushhi/nR */
      1, /* pushhi/lR */
      0, /* pushios */
      1, /* pushlo/nR */
      1, /* pushlo/lR */
      0, /* pushoac */
      0, /* pushob */
      0, /* pushobc */
      0, /* pushoc */
      0, /* pushod */
      0, /* pushoi */
      0, /* pushom */
      0, /* pushoo */
      0, /* pushopp */
      1, /* pushr/%rR */
      1, /* pushtopvar/nR */
      0, /* pushvar/n0/n0 */
      0, /* pushvar/n0/n1 */
      0, /* pushvar/n0/n2 */
      0, /* pushvar/n0/n3 */
      0, /* pushvar/n0/n4 */
      0, /* pushvar/n0/n5 */
      1, /* pushvar/n0/nR */
      1, /* pushvar/nR/n0 */
      1, /* pushvar/nR/n1 */
      1, /* pushvar/nR/n2 */
      1, /* pushvar/nR/n3 */
      1, /* pushvar/nR/n4 */
      1, /* pushvar/nR/n5 */
      2, /* pushvar/nR/nR */
      0, /* quake */
      0, /* raise */
      0, /* rand */
      0, /* regvar */
      0, /* reloc */
      1, /* restorer/%rR */
      0, /* return */
      0, /* revn/n3 */
      0, /* revn/n4 */
      1, /* revn/nR */
      0, /* rot */
      1, /* saver/%rR */
      0, /* sconc */
      0, /* sel */
      1, /* setr/%rR */
      0, /* siz */
      0, /* smodi */
      0, /* sref */
      0, /* srefi */
      0, /* srefia */
      0, /* srefio */
      0, /* srefmnt */
      0, /* srefnt */
      0, /* srefo */
      0, /* sset */
      1, /* strace/nR */
      0, /* strref */
      0, /* subi */
      0, /* subiu */
      0, /* subl */
      0, /* sublu */
      0, /* substr */
      0, /* swap */
      0, /* swapgti */
      0, /* swapgtiu */
      0, /* swapgtl */
      0, /* swapgtlu */
      0, /* sync */
      0, /* time */
      0, /* tor */
      0, /* tuck */
      0, /* tyagetb */
      0, /* tyagett */
      0, /* tyisc */
      0, /* tyissct */
      0, /* typof */
      0, /* tysctn */
      0, /* unmap */
      0, /* unreachable */
      0, /* ureloc */
      1, /* write/retR */
      1, /* *ba/fR*-no-fast-branches */
      1, /* *bn/fR*-no-fast-branches */
      1, /* *bnn/fR*-no-fast-branches */
      1, /* *bnzi/fR*-no-fast-branches */
      1, /* *bnziu/fR*-no-fast-branches */
      1, /* *bnzl/fR*-no-fast-branches */
      1, /* *bnzlu/fR*-no-fast-branches */
      1, /* *bzi/fR*-no-fast-branches */
      1, /* *bziu/fR*-no-fast-branches */
      1, /* *bzl/fR*-no-fast-branches */
      1 /* *bzlu/fR*-no-fast-branches */
    };
const unsigned long // FIXME: shall I use a shorter type when possible?
pvm_specialized_instruction_label_bitmasks [PVM_SPECIALIZED_INSTRUCTION_NO]
  = {
      /* It's important that !BEGINBASICBLOCK has a zero here: it does not need residual patching. */
      0, /* !INVALID */
      0, /* !BEGINBASICBLOCK */
      0, /* !EXITVM */
      0, /* !DATALOCATIONS */
      0, /* !NOP */
      0, /* !UNREACHABLE0 */
      0, /* !UNREACHABLE1 */
      0, /* !UNREACHABLE2 */
      0, /* addi */
      0, /* addiu */
      0, /* addl */
      0, /* addlu */
      0, /* ains */
      0, /* and */
      0, /* aref */
      0, /* arefo */
      0, /* arem */
      0, /* aset */
      0, /* asettb */
      0, /* atr */
      0 | (1UL << 0), /* ba/fR */
      0, /* bandi */
      0, /* bandiu */
      0, /* bandl */
      0, /* bandlu */
      0, /* beghl */
      0, /* begsc */
      0 | (1UL << 0), /* bn/fR */
      0 | (1UL << 0), /* bnn/fR */
      0, /* bnoti */
      0, /* bnotiu */
      0, /* bnotl */
      0, /* bnotlu */
      0 | (1UL << 0), /* bnzi/fR */
      0 | (1UL << 0), /* bnziu/fR */
      0 | (1UL << 0), /* bnzl/fR */
      0 | (1UL << 0), /* bnzlu/fR */
      0, /* bori */
      0, /* boriu */
      0, /* borl */
      0, /* borlu */
      0, /* bsli */
      0, /* bsliu */
      0, /* bsll */
      0, /* bsllu */
      0, /* bsri */
      0, /* bsriu */
      0, /* bsrl */
      0, /* bsrlu */
      0, /* bxori */
      0, /* bxoriu */
      0, /* bxorl */
      0, /* bxorlu */
      0 | (1UL << 0), /* bzi/fR */
      0 | (1UL << 0), /* bziu/fR */
      0 | (1UL << 0), /* bzl/fR */
      0 | (1UL << 0), /* bzlu/fR */
      0, /* call/retR */
      0, /* canary */
      0, /* close */
      0, /* ctos */
      0, /* disas */
      0, /* divi */
      0, /* diviu */
      0, /* divl */
      0, /* divlu */
      0, /* drop */
      0, /* drop2 */
      0, /* drop3 */
      0, /* drop4 */
      0, /* duc */
      0, /* dup */
      0, /* endhl */
      0, /* endsc */
      0, /* eqi */
      0, /* eqiu */
      0, /* eql */
      0, /* eqlu */
      0, /* eqs */
      0, /* exit */
      0, /* exitvm */
      0, /* flush */
      0, /* fromr */
      0, /* gei */
      0, /* geiu */
      0, /* gel */
      0, /* gelu */
      0, /* ges */
      0, /* getenv */
      0, /* gti */
      0, /* gtiu */
      0, /* gtl */
      0, /* gtlu */
      0, /* gts */
      0, /* indent */
      0, /* iogetb */
      0, /* iosetb */
      0, /* iosize */
      0, /* isa */
      0, /* itoi/nR */
      0, /* itoiu/nR */
      0, /* itol/nR */
      0, /* itolu/nR */
      0, /* iutoi/nR */
      0, /* iutoiu/nR */
      0, /* iutol/nR */
      0, /* iutolu/nR */
      0, /* lei */
      0, /* leiu */
      0, /* lel */
      0, /* lelu */
      0, /* les */
      0, /* lti */
      0, /* ltiu */
      0, /* ltl */
      0, /* ltlu */
      0, /* ltoi/nR */
      0, /* ltoiu/nR */
      0, /* ltol/nR */
      0, /* ltolu/nR */
      0, /* lts */
      0, /* lutoi/nR */
      0, /* lutoiu/nR */
      0, /* lutol/nR */
      0, /* lutolu/nR */
      0, /* map */
      0, /* mgetios */
      0, /* mgetm */
      0, /* mgeto */
      0, /* mgets */
      0, /* mgetsel */
      0, /* mgetsiz */
      0, /* mgetw */
      0, /* mka */
      0, /* mko */
      0, /* mksct */
      0, /* mktya */
      0, /* mktyany */
      0, /* mktyc */
      0, /* mktyi */
      0, /* mktyo */
      0, /* mktys */
      0, /* mktysct */
      0, /* mktyv */
      0, /* mm */
      0, /* modi */
      0, /* modiu */
      0, /* modl */
      0, /* modlu */
      0, /* msetios */
      0, /* msetm */
      0, /* mseto */
      0, /* msets */
      0, /* msetsel */
      0, /* msetsiz */
      0, /* msetw */
      0, /* muli */
      0, /* muliu */
      0, /* mull */
      0, /* mullu */
      0, /* muls */
      0, /* negi */
      0, /* negiu */
      0, /* negl */
      0, /* neglu */
      0, /* nei */
      0, /* neiu */
      0, /* nel */
      0, /* nelu */
      0, /* nes */
      0, /* nip */
      0, /* nip2 */
      0, /* nip3 */
      0, /* nn */
      0, /* nnn */
      0, /* nop */
      0, /* not */
      0, /* note/nR */
      0, /* nrot */
      0, /* ogetbt */
      0, /* ogetm */
      0, /* ogetu */
      0, /* open */
      0, /* or */
      0, /* osetm */
      0, /* over */
      0, /* pec */
      0, /* peekdi/nR */
      0, /* peekdiu/nR */
      0, /* peekdl/nR */
      0, /* peekdlu/nR */
      0, /* peeki/nR/nR/nR */
      0, /* peekiu/nR/nR */
      0, /* peekl/nR/nR/nR */
      0, /* peeklu/nR/nR */
      0, /* peeks */
      0, /* pokedi/nR */
      0, /* pokediu/nR */
      0, /* pokedl/nR */
      0, /* pokedlu/nR */
      0, /* pokei/nR/nR/nR */
      0, /* pokeiu/nR/nR */
      0, /* pokel/nR/nR/nR */
      0, /* pokelu/nR/nR */
      0, /* pokes */
      0, /* pope */
      0, /* popend */
      0, /* popf/nR */
      0, /* popios */
      0, /* popoac */
      0, /* popob */
      0, /* popobc */
      0, /* popoc */
      0, /* popod */
      0, /* popoi */
      0, /* popom */
      0, /* popoo */
      0, /* popopp */
      0, /* popr/%rR */
      0, /* popvar/nR/nR */
      0, /* powi */
      0, /* powiu */
      0, /* powl */
      0, /* powlu */
      0, /* printi/nR */
      0, /* printiu/nR */
      0, /* printl/nR */
      0, /* printlu/nR */
      0, /* prints */
      0, /* prolog */
      0, /* push/nR */
      0 | (1UL << 0), /* push/lR */
      0, /* push32/nR */
      0 | (1UL << 0), /* push32/lR */
      0 | (1UL << 0), /* pushe/lR */
      0, /* pushend */
      0, /* pushf/nR */
      0, /* pushhi/nR */
      0 | (1UL << 0), /* pushhi/lR */
      0, /* pushios */
      0, /* pushlo/nR */
      0 | (1UL << 0), /* pushlo/lR */
      0, /* pushoac */
      0, /* pushob */
      0, /* pushobc */
      0, /* pushoc */
      0, /* pushod */
      0, /* pushoi */
      0, /* pushom */
      0, /* pushoo */
      0, /* pushopp */
      0, /* pushr/%rR */
      0, /* pushtopvar/nR */
      0, /* pushvar/n0/n0 */
      0, /* pushvar/n0/n1 */
      0, /* pushvar/n0/n2 */
      0, /* pushvar/n0/n3 */
      0, /* pushvar/n0/n4 */
      0, /* pushvar/n0/n5 */
      0, /* pushvar/n0/nR */
      0, /* pushvar/nR/n0 */
      0, /* pushvar/nR/n1 */
      0, /* pushvar/nR/n2 */
      0, /* pushvar/nR/n3 */
      0, /* pushvar/nR/n4 */
      0, /* pushvar/nR/n5 */
      0, /* pushvar/nR/nR */
      0, /* quake */
      0, /* raise */
      0, /* rand */
      0, /* regvar */
      0, /* reloc */
      0, /* restorer/%rR */
      0, /* return */
      0, /* revn/n3 */
      0, /* revn/n4 */
      0, /* revn/nR */
      0, /* rot */
      0, /* saver/%rR */
      0, /* sconc */
      0, /* sel */
      0, /* setr/%rR */
      0, /* siz */
      0, /* smodi */
      0, /* sref */
      0, /* srefi */
      0, /* srefia */
      0, /* srefio */
      0, /* srefmnt */
      0, /* srefnt */
      0, /* srefo */
      0, /* sset */
      0, /* strace/nR */
      0, /* strref */
      0, /* subi */
      0, /* subiu */
      0, /* subl */
      0, /* sublu */
      0, /* substr */
      0, /* swap */
      0, /* swapgti */
      0, /* swapgtiu */
      0, /* swapgtl */
      0, /* swapgtlu */
      0, /* sync */
      0, /* time */
      0, /* tor */
      0, /* tuck */
      0, /* tyagetb */
      0, /* tyagett */
      0, /* tyisc */
      0, /* tyissct */
      0, /* typof */
      0, /* tysctn */
      0, /* unmap */
      0, /* unreachable */
      0, /* ureloc */
      0, /* write/retR */
      0 | (1UL << 0), /* *ba/fR*-no-fast-branches */
      0 | (1UL << 0), /* *bn/fR*-no-fast-branches */
      0 | (1UL << 0), /* *bnn/fR*-no-fast-branches */
      0 | (1UL << 0), /* *bnzi/fR*-no-fast-branches */
      0 | (1UL << 0), /* *bnziu/fR*-no-fast-branches */
      0 | (1UL << 0), /* *bnzl/fR*-no-fast-branches */
      0 | (1UL << 0), /* *bnzlu/fR*-no-fast-branches */
      0 | (1UL << 0), /* *bzi/fR*-no-fast-branches */
      0 | (1UL << 0), /* *bziu/fR*-no-fast-branches */
      0 | (1UL << 0), /* *bzl/fR*-no-fast-branches */
      0 | (1UL << 0) /* *bzlu/fR*-no-fast-branches */
    };
#ifdef JITTER_HAVE_PATCH_IN
const unsigned long // FIXME: shall I use a shorter type when possible?
pvm_specialized_instruction_fast_label_bitmasks [PVM_SPECIALIZED_INSTRUCTION_NO]
  = {
      /* It's important that !BEGINBASICBLOCK has a zero here: it does not need residual patching. */
      0, /* !INVALID */
      0, /* !BEGINBASICBLOCK */
      0, /* !EXITVM */
      0, /* !DATALOCATIONS */
      0, /* !NOP */
      0, /* !UNREACHABLE0 */
      0, /* !UNREACHABLE1 */
      0, /* !UNREACHABLE2 */
      0, /* addi */
      0, /* addiu */
      0, /* addl */
      0, /* addlu */
      0, /* ains */
      0, /* and */
      0, /* aref */
      0, /* arefo */
      0, /* arem */
      0, /* aset */
      0, /* asettb */
      0, /* atr */
      0 | (1UL << 0), /* ba/fR */
      0, /* bandi */
      0, /* bandiu */
      0, /* bandl */
      0, /* bandlu */
      0, /* beghl */
      0, /* begsc */
      0 | (1UL << 0), /* bn/fR */
      0 | (1UL << 0), /* bnn/fR */
      0, /* bnoti */
      0, /* bnotiu */
      0, /* bnotl */
      0, /* bnotlu */
      0 | (1UL << 0), /* bnzi/fR */
      0 | (1UL << 0), /* bnziu/fR */
      0 | (1UL << 0), /* bnzl/fR */
      0 | (1UL << 0), /* bnzlu/fR */
      0, /* bori */
      0, /* boriu */
      0, /* borl */
      0, /* borlu */
      0, /* bsli */
      0, /* bsliu */
      0, /* bsll */
      0, /* bsllu */
      0, /* bsri */
      0, /* bsriu */
      0, /* bsrl */
      0, /* bsrlu */
      0, /* bxori */
      0, /* bxoriu */
      0, /* bxorl */
      0, /* bxorlu */
      0 | (1UL << 0), /* bzi/fR */
      0 | (1UL << 0), /* bziu/fR */
      0 | (1UL << 0), /* bzl/fR */
      0 | (1UL << 0), /* bzlu/fR */
      0, /* call/retR */
      0, /* canary */
      0, /* close */
      0, /* ctos */
      0, /* disas */
      0, /* divi */
      0, /* diviu */
      0, /* divl */
      0, /* divlu */
      0, /* drop */
      0, /* drop2 */
      0, /* drop3 */
      0, /* drop4 */
      0, /* duc */
      0, /* dup */
      0, /* endhl */
      0, /* endsc */
      0, /* eqi */
      0, /* eqiu */
      0, /* eql */
      0, /* eqlu */
      0, /* eqs */
      0, /* exit */
      0, /* exitvm */
      0, /* flush */
      0, /* fromr */
      0, /* gei */
      0, /* geiu */
      0, /* gel */
      0, /* gelu */
      0, /* ges */
      0, /* getenv */
      0, /* gti */
      0, /* gtiu */
      0, /* gtl */
      0, /* gtlu */
      0, /* gts */
      0, /* indent */
      0, /* iogetb */
      0, /* iosetb */
      0, /* iosize */
      0, /* isa */
      0, /* itoi/nR */
      0, /* itoiu/nR */
      0, /* itol/nR */
      0, /* itolu/nR */
      0, /* iutoi/nR */
      0, /* iutoiu/nR */
      0, /* iutol/nR */
      0, /* iutolu/nR */
      0, /* lei */
      0, /* leiu */
      0, /* lel */
      0, /* lelu */
      0, /* les */
      0, /* lti */
      0, /* ltiu */
      0, /* ltl */
      0, /* ltlu */
      0, /* ltoi/nR */
      0, /* ltoiu/nR */
      0, /* ltol/nR */
      0, /* ltolu/nR */
      0, /* lts */
      0, /* lutoi/nR */
      0, /* lutoiu/nR */
      0, /* lutol/nR */
      0, /* lutolu/nR */
      0, /* map */
      0, /* mgetios */
      0, /* mgetm */
      0, /* mgeto */
      0, /* mgets */
      0, /* mgetsel */
      0, /* mgetsiz */
      0, /* mgetw */
      0, /* mka */
      0, /* mko */
      0, /* mksct */
      0, /* mktya */
      0, /* mktyany */
      0, /* mktyc */
      0, /* mktyi */
      0, /* mktyo */
      0, /* mktys */
      0, /* mktysct */
      0, /* mktyv */
      0, /* mm */
      0, /* modi */
      0, /* modiu */
      0, /* modl */
      0, /* modlu */
      0, /* msetios */
      0, /* msetm */
      0, /* mseto */
      0, /* msets */
      0, /* msetsel */
      0, /* msetsiz */
      0, /* msetw */
      0, /* muli */
      0, /* muliu */
      0, /* mull */
      0, /* mullu */
      0, /* muls */
      0, /* negi */
      0, /* negiu */
      0, /* negl */
      0, /* neglu */
      0, /* nei */
      0, /* neiu */
      0, /* nel */
      0, /* nelu */
      0, /* nes */
      0, /* nip */
      0, /* nip2 */
      0, /* nip3 */
      0, /* nn */
      0, /* nnn */
      0, /* nop */
      0, /* not */
      0, /* note/nR */
      0, /* nrot */
      0, /* ogetbt */
      0, /* ogetm */
      0, /* ogetu */
      0, /* open */
      0, /* or */
      0, /* osetm */
      0, /* over */
      0, /* pec */
      0, /* peekdi/nR */
      0, /* peekdiu/nR */
      0, /* peekdl/nR */
      0, /* peekdlu/nR */
      0, /* peeki/nR/nR/nR */
      0, /* peekiu/nR/nR */
      0, /* peekl/nR/nR/nR */
      0, /* peeklu/nR/nR */
      0, /* peeks */
      0, /* pokedi/nR */
      0, /* pokediu/nR */
      0, /* pokedl/nR */
      0, /* pokedlu/nR */
      0, /* pokei/nR/nR/nR */
      0, /* pokeiu/nR/nR */
      0, /* pokel/nR/nR/nR */
      0, /* pokelu/nR/nR */
      0, /* pokes */
      0, /* pope */
      0, /* popend */
      0, /* popf/nR */
      0, /* popios */
      0, /* popoac */
      0, /* popob */
      0, /* popobc */
      0, /* popoc */
      0, /* popod */
      0, /* popoi */
      0, /* popom */
      0, /* popoo */
      0, /* popopp */
      0, /* popr/%rR */
      0, /* popvar/nR/nR */
      0, /* powi */
      0, /* powiu */
      0, /* powl */
      0, /* powlu */
      0, /* printi/nR */
      0, /* printiu/nR */
      0, /* printl/nR */
      0, /* printlu/nR */
      0, /* prints */
      0, /* prolog */
      0, /* push/nR */
      0, /* push/lR */
      0, /* push32/nR */
      0, /* push32/lR */
      0, /* pushe/lR */
      0, /* pushend */
      0, /* pushf/nR */
      0, /* pushhi/nR */
      0, /* pushhi/lR */
      0, /* pushios */
      0, /* pushlo/nR */
      0, /* pushlo/lR */
      0, /* pushoac */
      0, /* pushob */
      0, /* pushobc */
      0, /* pushoc */
      0, /* pushod */
      0, /* pushoi */
      0, /* pushom */
      0, /* pushoo */
      0, /* pushopp */
      0, /* pushr/%rR */
      0, /* pushtopvar/nR */
      0, /* pushvar/n0/n0 */
      0, /* pushvar/n0/n1 */
      0, /* pushvar/n0/n2 */
      0, /* pushvar/n0/n3 */
      0, /* pushvar/n0/n4 */
      0, /* pushvar/n0/n5 */
      0, /* pushvar/n0/nR */
      0, /* pushvar/nR/n0 */
      0, /* pushvar/nR/n1 */
      0, /* pushvar/nR/n2 */
      0, /* pushvar/nR/n3 */
      0, /* pushvar/nR/n4 */
      0, /* pushvar/nR/n5 */
      0, /* pushvar/nR/nR */
      0, /* quake */
      0, /* raise */
      0, /* rand */
      0, /* regvar */
      0, /* reloc */
      0, /* restorer/%rR */
      0, /* return */
      0, /* revn/n3 */
      0, /* revn/n4 */
      0, /* revn/nR */
      0, /* rot */
      0, /* saver/%rR */
      0, /* sconc */
      0, /* sel */
      0, /* setr/%rR */
      0, /* siz */
      0, /* smodi */
      0, /* sref */
      0, /* srefi */
      0, /* srefia */
      0, /* srefio */
      0, /* srefmnt */
      0, /* srefnt */
      0, /* srefo */
      0, /* sset */
      0, /* strace/nR */
      0, /* strref */
      0, /* subi */
      0, /* subiu */
      0, /* subl */
      0, /* sublu */
      0, /* substr */
      0, /* swap */
      0, /* swapgti */
      0, /* swapgtiu */
      0, /* swapgtl */
      0, /* swapgtlu */
      0, /* sync */
      0, /* time */
      0, /* tor */
      0, /* tuck */
      0, /* tyagetb */
      0, /* tyagett */
      0, /* tyisc */
      0, /* tyissct */
      0, /* typof */
      0, /* tysctn */
      0, /* unmap */
      0, /* unreachable */
      0, /* ureloc */
      0, /* write/retR */
      0, /* *ba/fR*-no-fast-branches */
      0, /* *bn/fR*-no-fast-branches */
      0, /* *bnn/fR*-no-fast-branches */
      0, /* *bnzi/fR*-no-fast-branches */
      0, /* *bnziu/fR*-no-fast-branches */
      0, /* *bnzl/fR*-no-fast-branches */
      0, /* *bnzlu/fR*-no-fast-branches */
      0, /* *bzi/fR*-no-fast-branches */
      0, /* *bziu/fR*-no-fast-branches */
      0, /* *bzl/fR*-no-fast-branches */
      0 /* *bzlu/fR*-no-fast-branches */
    };
#endif // #ifdef JITTER_HAVE_PATCH_IN

// FIXME: I may want to conditionalize this.
const bool
pvm_specialized_instruction_relocatables [PVM_SPECIALIZED_INSTRUCTION_NO]
  = {
      true, // !INVALID
      true, // !BEGINBASICBLOCK
      true, // !EXITVM
      true, // !DATALOCATIONS
      true, // !NOP
      true, // !UNREACHABLE0
      true, // !UNREACHABLE1
      true, // !UNREACHABLE2
      true, // addi
      true, // addiu
      true, // addl
      true, // addlu
      true, // ains
      true, // and
      true, // aref
      true, // arefo
      true, // arem
      true, // aset
      true, // asettb
      true, // atr
      true, // ba/fR
      true, // bandi
      true, // bandiu
      true, // bandl
      true, // bandlu
      true, // beghl
      true, // begsc
      true, // bn/fR
      true, // bnn/fR
      true, // bnoti
      true, // bnotiu
      true, // bnotl
      true, // bnotlu
      true, // bnzi/fR
      true, // bnziu/fR
      true, // bnzl/fR
      true, // bnzlu/fR
      true, // bori
      true, // boriu
      true, // borl
      true, // borlu
      true, // bsli
      true, // bsliu
      true, // bsll
      true, // bsllu
      true, // bsri
      true, // bsriu
      true, // bsrl
      true, // bsrlu
      true, // bxori
      true, // bxoriu
      true, // bxorl
      true, // bxorlu
      true, // bzi/fR
      true, // bziu/fR
      true, // bzl/fR
      true, // bzlu/fR
      true, // call/retR
      true, // canary
      true, // close
      true, // ctos
      true, // disas
      true, // divi
      true, // diviu
      true, // divl
      true, // divlu
      true, // drop
      true, // drop2
      true, // drop3
      true, // drop4
      true, // duc
      true, // dup
      true, // endhl
      true, // endsc
      true, // eqi
      true, // eqiu
      true, // eql
      true, // eqlu
      true, // eqs
      true, // exit
      true, // exitvm
      true, // flush
      true, // fromr
      true, // gei
      true, // geiu
      true, // gel
      true, // gelu
      true, // ges
      true, // getenv
      true, // gti
      true, // gtiu
      true, // gtl
      true, // gtlu
      true, // gts
      true, // indent
      true, // iogetb
      true, // iosetb
      true, // iosize
      true, // isa
      true, // itoi/nR
      true, // itoiu/nR
      true, // itol/nR
      true, // itolu/nR
      true, // iutoi/nR
      true, // iutoiu/nR
      true, // iutol/nR
      true, // iutolu/nR
      true, // lei
      true, // leiu
      true, // lel
      true, // lelu
      true, // les
      true, // lti
      true, // ltiu
      true, // ltl
      true, // ltlu
      true, // ltoi/nR
      true, // ltoiu/nR
      true, // ltol/nR
      true, // ltolu/nR
      true, // lts
      true, // lutoi/nR
      true, // lutoiu/nR
      true, // lutol/nR
      true, // lutolu/nR
      true, // map
      true, // mgetios
      true, // mgetm
      true, // mgeto
      true, // mgets
      true, // mgetsel
      true, // mgetsiz
      true, // mgetw
      true, // mka
      true, // mko
      true, // mksct
      true, // mktya
      true, // mktyany
      true, // mktyc
      true, // mktyi
      true, // mktyo
      true, // mktys
      true, // mktysct
      true, // mktyv
      true, // mm
      true, // modi
      true, // modiu
      true, // modl
      true, // modlu
      true, // msetios
      true, // msetm
      true, // mseto
      true, // msets
      true, // msetsel
      true, // msetsiz
      true, // msetw
      true, // muli
      true, // muliu
      true, // mull
      true, // mullu
      true, // muls
      true, // negi
      true, // negiu
      true, // negl
      true, // neglu
      true, // nei
      true, // neiu
      true, // nel
      true, // nelu
      true, // nes
      true, // nip
      true, // nip2
      true, // nip3
      true, // nn
      true, // nnn
      true, // nop
      true, // not
      true, // note/nR
      true, // nrot
      true, // ogetbt
      true, // ogetm
      true, // ogetu
      true, // open
      true, // or
      true, // osetm
      true, // over
      true, // pec
      true, // peekdi/nR
      true, // peekdiu/nR
      true, // peekdl/nR
      true, // peekdlu/nR
      true, // peeki/nR/nR/nR
      true, // peekiu/nR/nR
      true, // peekl/nR/nR/nR
      true, // peeklu/nR/nR
      true, // peeks
      true, // pokedi/nR
      true, // pokediu/nR
      true, // pokedl/nR
      true, // pokedlu/nR
      true, // pokei/nR/nR/nR
      true, // pokeiu/nR/nR
      true, // pokel/nR/nR/nR
      true, // pokelu/nR/nR
      true, // pokes
      true, // pope
      true, // popend
      true, // popf/nR
      true, // popios
      true, // popoac
      true, // popob
      true, // popobc
      true, // popoc
      true, // popod
      true, // popoi
      true, // popom
      true, // popoo
      true, // popopp
      true, // popr/%rR
      true, // popvar/nR/nR
      true, // powi
      true, // powiu
      true, // powl
      true, // powlu
      true, // printi/nR
      true, // printiu/nR
      true, // printl/nR
      true, // printlu/nR
      true, // prints
      true, // prolog
      true, // push/nR
      true, // push/lR
      true, // push32/nR
      true, // push32/lR
      true, // pushe/lR
      true, // pushend
      true, // pushf/nR
      true, // pushhi/nR
      true, // pushhi/lR
      true, // pushios
      true, // pushlo/nR
      true, // pushlo/lR
      true, // pushoac
      true, // pushob
      true, // pushobc
      true, // pushoc
      true, // pushod
      true, // pushoi
      true, // pushom
      true, // pushoo
      true, // pushopp
      true, // pushr/%rR
      true, // pushtopvar/nR
      true, // pushvar/n0/n0
      true, // pushvar/n0/n1
      true, // pushvar/n0/n2
      true, // pushvar/n0/n3
      true, // pushvar/n0/n4
      true, // pushvar/n0/n5
      true, // pushvar/n0/nR
      true, // pushvar/nR/n0
      true, // pushvar/nR/n1
      true, // pushvar/nR/n2
      true, // pushvar/nR/n3
      true, // pushvar/nR/n4
      true, // pushvar/nR/n5
      true, // pushvar/nR/nR
      true, // quake
      true, // raise
      true, // rand
      true, // regvar
      true, // reloc
      true, // restorer/%rR
      true, // return
      true, // revn/n3
      true, // revn/n4
      true, // revn/nR
      true, // rot
      true, // saver/%rR
      true, // sconc
      true, // sel
      true, // setr/%rR
      true, // siz
      true, // smodi
      true, // sref
      true, // srefi
      true, // srefia
      true, // srefio
      true, // srefmnt
      true, // srefnt
      true, // srefo
      true, // sset
      true, // strace/nR
      true, // strref
      true, // subi
      true, // subiu
      true, // subl
      true, // sublu
      true, // substr
      true, // swap
      true, // swapgti
      true, // swapgtiu
      true, // swapgtl
      true, // swapgtlu
      true, // sync
      true, // time
      true, // tor
      true, // tuck
      true, // tyagetb
      true, // tyagett
      true, // tyisc
      true, // tyissct
      true, // typof
      true, // tysctn
      true, // unmap
      true, // unreachable
      true, // ureloc
      true, // write/retR
      false, // *ba/fR*-no-fast-branches
      false, // *bn/fR*-no-fast-branches
      false, // *bnn/fR*-no-fast-branches
      false, // *bnzi/fR*-no-fast-branches
      false, // *bnziu/fR*-no-fast-branches
      false, // *bnzl/fR*-no-fast-branches
      false, // *bnzlu/fR*-no-fast-branches
      false, // *bzi/fR*-no-fast-branches
      false, // *bziu/fR*-no-fast-branches
      false, // *bzl/fR*-no-fast-branches
      false // *bzlu/fR*-no-fast-branches
    };

// FIXME: this is not currently accessed, and in fact may be useless.
const bool
pvm_specialized_instruction_callers [PVM_SPECIALIZED_INSTRUCTION_NO]
  = {
      false, // !INVALID
      false, // !BEGINBASICBLOCK
      false, // !EXITVM
      false, // !DATALOCATIONS
      false, // !NOP
      false, // !UNREACHABLE0
      false, // !UNREACHABLE1
      false, // !UNREACHABLE2
      false, // addi
      false, // addiu
      false, // addl
      false, // addlu
      false, // ains
      false, // and
      false, // aref
      false, // arefo
      false, // arem
      false, // aset
      false, // asettb
      false, // atr
      false, // ba/fR
      false, // bandi
      false, // bandiu
      false, // bandl
      false, // bandlu
      false, // beghl
      false, // begsc
      false, // bn/fR
      false, // bnn/fR
      false, // bnoti
      false, // bnotiu
      false, // bnotl
      false, // bnotlu
      false, // bnzi/fR
      false, // bnziu/fR
      false, // bnzl/fR
      false, // bnzlu/fR
      false, // bori
      false, // boriu
      false, // borl
      false, // borlu
      false, // bsli
      false, // bsliu
      false, // bsll
      false, // bsllu
      false, // bsri
      false, // bsriu
      false, // bsrl
      false, // bsrlu
      false, // bxori
      false, // bxoriu
      false, // bxorl
      false, // bxorlu
      false, // bzi/fR
      false, // bziu/fR
      false, // bzl/fR
      false, // bzlu/fR
      true, // call/retR
      false, // canary
      false, // close
      false, // ctos
      false, // disas
      false, // divi
      false, // diviu
      false, // divl
      false, // divlu
      false, // drop
      false, // drop2
      false, // drop3
      false, // drop4
      false, // duc
      false, // dup
      false, // endhl
      false, // endsc
      false, // eqi
      false, // eqiu
      false, // eql
      false, // eqlu
      false, // eqs
      false, // exit
      false, // exitvm
      false, // flush
      false, // fromr
      false, // gei
      false, // geiu
      false, // gel
      false, // gelu
      false, // ges
      false, // getenv
      false, // gti
      false, // gtiu
      false, // gtl
      false, // gtlu
      false, // gts
      false, // indent
      false, // iogetb
      false, // iosetb
      false, // iosize
      false, // isa
      false, // itoi/nR
      false, // itoiu/nR
      false, // itol/nR
      false, // itolu/nR
      false, // iutoi/nR
      false, // iutoiu/nR
      false, // iutol/nR
      false, // iutolu/nR
      false, // lei
      false, // leiu
      false, // lel
      false, // lelu
      false, // les
      false, // lti
      false, // ltiu
      false, // ltl
      false, // ltlu
      false, // ltoi/nR
      false, // ltoiu/nR
      false, // ltol/nR
      false, // ltolu/nR
      false, // lts
      false, // lutoi/nR
      false, // lutoiu/nR
      false, // lutol/nR
      false, // lutolu/nR
      false, // map
      false, // mgetios
      false, // mgetm
      false, // mgeto
      false, // mgets
      false, // mgetsel
      false, // mgetsiz
      false, // mgetw
      false, // mka
      false, // mko
      false, // mksct
      false, // mktya
      false, // mktyany
      false, // mktyc
      false, // mktyi
      false, // mktyo
      false, // mktys
      false, // mktysct
      false, // mktyv
      false, // mm
      false, // modi
      false, // modiu
      false, // modl
      false, // modlu
      false, // msetios
      false, // msetm
      false, // mseto
      false, // msets
      false, // msetsel
      false, // msetsiz
      false, // msetw
      false, // muli
      false, // muliu
      false, // mull
      false, // mullu
      false, // muls
      false, // negi
      false, // negiu
      false, // negl
      false, // neglu
      false, // nei
      false, // neiu
      false, // nel
      false, // nelu
      false, // nes
      false, // nip
      false, // nip2
      false, // nip3
      false, // nn
      false, // nnn
      false, // nop
      false, // not
      false, // note/nR
      false, // nrot
      false, // ogetbt
      false, // ogetm
      false, // ogetu
      false, // open
      false, // or
      false, // osetm
      false, // over
      false, // pec
      false, // peekdi/nR
      false, // peekdiu/nR
      false, // peekdl/nR
      false, // peekdlu/nR
      false, // peeki/nR/nR/nR
      false, // peekiu/nR/nR
      false, // peekl/nR/nR/nR
      false, // peeklu/nR/nR
      false, // peeks
      false, // pokedi/nR
      false, // pokediu/nR
      false, // pokedl/nR
      false, // pokedlu/nR
      false, // pokei/nR/nR/nR
      false, // pokeiu/nR/nR
      false, // pokel/nR/nR/nR
      false, // pokelu/nR/nR
      false, // pokes
      false, // pope
      false, // popend
      false, // popf/nR
      false, // popios
      false, // popoac
      false, // popob
      false, // popobc
      false, // popoc
      false, // popod
      false, // popoi
      false, // popom
      false, // popoo
      false, // popopp
      false, // popr/%rR
      false, // popvar/nR/nR
      false, // powi
      false, // powiu
      false, // powl
      false, // powlu
      false, // printi/nR
      false, // printiu/nR
      false, // printl/nR
      false, // printlu/nR
      false, // prints
      false, // prolog
      false, // push/nR
      false, // push/lR
      false, // push32/nR
      false, // push32/lR
      false, // pushe/lR
      false, // pushend
      false, // pushf/nR
      false, // pushhi/nR
      false, // pushhi/lR
      false, // pushios
      false, // pushlo/nR
      false, // pushlo/lR
      false, // pushoac
      false, // pushob
      false, // pushobc
      false, // pushoc
      false, // pushod
      false, // pushoi
      false, // pushom
      false, // pushoo
      false, // pushopp
      false, // pushr/%rR
      false, // pushtopvar/nR
      false, // pushvar/n0/n0
      false, // pushvar/n0/n1
      false, // pushvar/n0/n2
      false, // pushvar/n0/n3
      false, // pushvar/n0/n4
      false, // pushvar/n0/n5
      false, // pushvar/n0/nR
      false, // pushvar/nR/n0
      false, // pushvar/nR/n1
      false, // pushvar/nR/n2
      false, // pushvar/nR/n3
      false, // pushvar/nR/n4
      false, // pushvar/nR/n5
      false, // pushvar/nR/nR
      false, // quake
      false, // raise
      false, // rand
      false, // regvar
      false, // reloc
      false, // restorer/%rR
      false, // return
      false, // revn/n3
      false, // revn/n4
      false, // revn/nR
      false, // rot
      false, // saver/%rR
      false, // sconc
      false, // sel
      false, // setr/%rR
      false, // siz
      false, // smodi
      false, // sref
      false, // srefi
      false, // srefia
      false, // srefio
      false, // srefmnt
      false, // srefnt
      false, // srefo
      false, // sset
      false, // strace/nR
      false, // strref
      false, // subi
      false, // subiu
      false, // subl
      false, // sublu
      false, // substr
      false, // swap
      false, // swapgti
      false, // swapgtiu
      false, // swapgtl
      false, // swapgtlu
      false, // sync
      false, // time
      false, // tor
      false, // tuck
      false, // tyagetb
      false, // tyagett
      false, // tyisc
      false, // tyissct
      false, // typof
      false, // tysctn
      false, // unmap
      false, // unreachable
      false, // ureloc
      true, // write/retR
      false, // *ba/fR*-no-fast-branches
      false, // *bn/fR*-no-fast-branches
      false, // *bnn/fR*-no-fast-branches
      false, // *bnzi/fR*-no-fast-branches
      false, // *bnziu/fR*-no-fast-branches
      false, // *bnzl/fR*-no-fast-branches
      false, // *bnzlu/fR*-no-fast-branches
      false, // *bzi/fR*-no-fast-branches
      false, // *bziu/fR*-no-fast-branches
      false, // *bzl/fR*-no-fast-branches
      false // *bzlu/fR*-no-fast-branches
    };

// FIXME: this is not currently accessed, and in fact may be useless.
const bool
pvm_specialized_instruction_callees [PVM_SPECIALIZED_INSTRUCTION_NO]
  = {
      false, // !INVALID
      false, // !BEGINBASICBLOCK
      false, // !EXITVM
      false, // !DATALOCATIONS
      false, // !NOP
      false, // !UNREACHABLE0
      false, // !UNREACHABLE1
      false, // !UNREACHABLE2
      false, // addi
      false, // addiu
      false, // addl
      false, // addlu
      false, // ains
      false, // and
      false, // aref
      false, // arefo
      false, // arem
      false, // aset
      false, // asettb
      false, // atr
      false, // ba/fR
      false, // bandi
      false, // bandiu
      false, // bandl
      false, // bandlu
      false, // beghl
      false, // begsc
      false, // bn/fR
      false, // bnn/fR
      false, // bnoti
      false, // bnotiu
      false, // bnotl
      false, // bnotlu
      false, // bnzi/fR
      false, // bnziu/fR
      false, // bnzl/fR
      false, // bnzlu/fR
      false, // bori
      false, // boriu
      false, // borl
      false, // borlu
      false, // bsli
      false, // bsliu
      false, // bsll
      false, // bsllu
      false, // bsri
      false, // bsriu
      false, // bsrl
      false, // bsrlu
      false, // bxori
      false, // bxoriu
      false, // bxorl
      false, // bxorlu
      false, // bzi/fR
      false, // bziu/fR
      false, // bzl/fR
      false, // bzlu/fR
      false, // call/retR
      false, // canary
      false, // close
      false, // ctos
      false, // disas
      false, // divi
      false, // diviu
      false, // divl
      false, // divlu
      false, // drop
      false, // drop2
      false, // drop3
      false, // drop4
      false, // duc
      false, // dup
      false, // endhl
      false, // endsc
      false, // eqi
      false, // eqiu
      false, // eql
      false, // eqlu
      false, // eqs
      false, // exit
      false, // exitvm
      false, // flush
      false, // fromr
      false, // gei
      false, // geiu
      false, // gel
      false, // gelu
      false, // ges
      false, // getenv
      false, // gti
      false, // gtiu
      false, // gtl
      false, // gtlu
      false, // gts
      false, // indent
      false, // iogetb
      false, // iosetb
      false, // iosize
      false, // isa
      false, // itoi/nR
      false, // itoiu/nR
      false, // itol/nR
      false, // itolu/nR
      false, // iutoi/nR
      false, // iutoiu/nR
      false, // iutol/nR
      false, // iutolu/nR
      false, // lei
      false, // leiu
      false, // lel
      false, // lelu
      false, // les
      false, // lti
      false, // ltiu
      false, // ltl
      false, // ltlu
      false, // ltoi/nR
      false, // ltoiu/nR
      false, // ltol/nR
      false, // ltolu/nR
      false, // lts
      false, // lutoi/nR
      false, // lutoiu/nR
      false, // lutol/nR
      false, // lutolu/nR
      false, // map
      false, // mgetios
      false, // mgetm
      false, // mgeto
      false, // mgets
      false, // mgetsel
      false, // mgetsiz
      false, // mgetw
      false, // mka
      false, // mko
      false, // mksct
      false, // mktya
      false, // mktyany
      false, // mktyc
      false, // mktyi
      false, // mktyo
      false, // mktys
      false, // mktysct
      false, // mktyv
      false, // mm
      false, // modi
      false, // modiu
      false, // modl
      false, // modlu
      false, // msetios
      false, // msetm
      false, // mseto
      false, // msets
      false, // msetsel
      false, // msetsiz
      false, // msetw
      false, // muli
      false, // muliu
      false, // mull
      false, // mullu
      false, // muls
      false, // negi
      false, // negiu
      false, // negl
      false, // neglu
      false, // nei
      false, // neiu
      false, // nel
      false, // nelu
      false, // nes
      false, // nip
      false, // nip2
      false, // nip3
      false, // nn
      false, // nnn
      false, // nop
      false, // not
      false, // note/nR
      false, // nrot
      false, // ogetbt
      false, // ogetm
      false, // ogetu
      false, // open
      false, // or
      false, // osetm
      false, // over
      false, // pec
      false, // peekdi/nR
      false, // peekdiu/nR
      false, // peekdl/nR
      false, // peekdlu/nR
      false, // peeki/nR/nR/nR
      false, // peekiu/nR/nR
      false, // peekl/nR/nR/nR
      false, // peeklu/nR/nR
      false, // peeks
      false, // pokedi/nR
      false, // pokediu/nR
      false, // pokedl/nR
      false, // pokedlu/nR
      false, // pokei/nR/nR/nR
      false, // pokeiu/nR/nR
      false, // pokel/nR/nR/nR
      false, // pokelu/nR/nR
      false, // pokes
      false, // pope
      false, // popend
      false, // popf/nR
      false, // popios
      false, // popoac
      false, // popob
      false, // popobc
      false, // popoc
      false, // popod
      false, // popoi
      false, // popom
      false, // popoo
      false, // popopp
      false, // popr/%rR
      false, // popvar/nR/nR
      false, // powi
      false, // powiu
      false, // powl
      false, // powlu
      false, // printi/nR
      false, // printiu/nR
      false, // printl/nR
      false, // printlu/nR
      false, // prints
      true, // prolog
      false, // push/nR
      false, // push/lR
      false, // push32/nR
      false, // push32/lR
      false, // pushe/lR
      false, // pushend
      false, // pushf/nR
      false, // pushhi/nR
      false, // pushhi/lR
      false, // pushios
      false, // pushlo/nR
      false, // pushlo/lR
      false, // pushoac
      false, // pushob
      false, // pushobc
      false, // pushoc
      false, // pushod
      false, // pushoi
      false, // pushom
      false, // pushoo
      false, // pushopp
      false, // pushr/%rR
      false, // pushtopvar/nR
      false, // pushvar/n0/n0
      false, // pushvar/n0/n1
      false, // pushvar/n0/n2
      false, // pushvar/n0/n3
      false, // pushvar/n0/n4
      false, // pushvar/n0/n5
      false, // pushvar/n0/nR
      false, // pushvar/nR/n0
      false, // pushvar/nR/n1
      false, // pushvar/nR/n2
      false, // pushvar/nR/n3
      false, // pushvar/nR/n4
      false, // pushvar/nR/n5
      false, // pushvar/nR/nR
      false, // quake
      false, // raise
      false, // rand
      false, // regvar
      false, // reloc
      false, // restorer/%rR
      false, // return
      false, // revn/n3
      false, // revn/n4
      false, // revn/nR
      false, // rot
      false, // saver/%rR
      false, // sconc
      false, // sel
      false, // setr/%rR
      false, // siz
      false, // smodi
      false, // sref
      false, // srefi
      false, // srefia
      false, // srefio
      false, // srefmnt
      false, // srefnt
      false, // srefo
      false, // sset
      false, // strace/nR
      false, // strref
      false, // subi
      false, // subiu
      false, // subl
      false, // sublu
      false, // substr
      false, // swap
      false, // swapgti
      false, // swapgtiu
      false, // swapgtl
      false, // swapgtlu
      false, // sync
      false, // time
      false, // tor
      false, // tuck
      false, // tyagetb
      false, // tyagett
      false, // tyisc
      false, // tyissct
      false, // typof
      false, // tysctn
      false, // unmap
      false, // unreachable
      false, // ureloc
      false, // write/retR
      false, // *ba/fR*-no-fast-branches
      false, // *bn/fR*-no-fast-branches
      false, // *bnn/fR*-no-fast-branches
      false, // *bnzi/fR*-no-fast-branches
      false, // *bnziu/fR*-no-fast-branches
      false, // *bnzl/fR*-no-fast-branches
      false, // *bnzlu/fR*-no-fast-branches
      false, // *bzi/fR*-no-fast-branches
      false, // *bziu/fR*-no-fast-branches
      false, // *bzl/fR*-no-fast-branches
      false // *bzlu/fR*-no-fast-branches
    };

/* An array whose indices are specialised instruction opcodes, and
   whose elements are the corresponding unspecialised instructions
   opcodes -- or -1 when there is no mapping mapping having */
const int
pvm_specialized_instruction_to_unspecialized_instruction
   [PVM_SPECIALIZED_INSTRUCTION_NO]
  = {
    -1, /* !INVALID */
    -1, /* !BEGINBASICBLOCK */
    -1, /* !EXITVM */
    -1, /* !DATALOCATIONS */
    -1, /* !NOP */
    -1, /* !UNREACHABLE0 */
    -1, /* !UNREACHABLE1 */
    -1, /* !UNREACHABLE2 */
    pvm_meta_instruction_id_addi, /* addi */
    pvm_meta_instruction_id_addiu, /* addiu */
    pvm_meta_instruction_id_addl, /* addl */
    pvm_meta_instruction_id_addlu, /* addlu */
    pvm_meta_instruction_id_ains, /* ains */
    pvm_meta_instruction_id_and, /* and */
    pvm_meta_instruction_id_aref, /* aref */
    pvm_meta_instruction_id_arefo, /* arefo */
    pvm_meta_instruction_id_arem, /* arem */
    pvm_meta_instruction_id_aset, /* aset */
    pvm_meta_instruction_id_asettb, /* asettb */
    pvm_meta_instruction_id_atr, /* atr */
    pvm_meta_instruction_id_ba, /* ba/fR */
    pvm_meta_instruction_id_bandi, /* bandi */
    pvm_meta_instruction_id_bandiu, /* bandiu */
    pvm_meta_instruction_id_bandl, /* bandl */
    pvm_meta_instruction_id_bandlu, /* bandlu */
    pvm_meta_instruction_id_beghl, /* beghl */
    pvm_meta_instruction_id_begsc, /* begsc */
    pvm_meta_instruction_id_bn, /* bn/fR */
    pvm_meta_instruction_id_bnn, /* bnn/fR */
    pvm_meta_instruction_id_bnoti, /* bnoti */
    pvm_meta_instruction_id_bnotiu, /* bnotiu */
    pvm_meta_instruction_id_bnotl, /* bnotl */
    pvm_meta_instruction_id_bnotlu, /* bnotlu */
    pvm_meta_instruction_id_bnzi, /* bnzi/fR */
    pvm_meta_instruction_id_bnziu, /* bnziu/fR */
    pvm_meta_instruction_id_bnzl, /* bnzl/fR */
    pvm_meta_instruction_id_bnzlu, /* bnzlu/fR */
    pvm_meta_instruction_id_bori, /* bori */
    pvm_meta_instruction_id_boriu, /* boriu */
    pvm_meta_instruction_id_borl, /* borl */
    pvm_meta_instruction_id_borlu, /* borlu */
    pvm_meta_instruction_id_bsli, /* bsli */
    pvm_meta_instruction_id_bsliu, /* bsliu */
    pvm_meta_instruction_id_bsll, /* bsll */
    pvm_meta_instruction_id_bsllu, /* bsllu */
    pvm_meta_instruction_id_bsri, /* bsri */
    pvm_meta_instruction_id_bsriu, /* bsriu */
    pvm_meta_instruction_id_bsrl, /* bsrl */
    pvm_meta_instruction_id_bsrlu, /* bsrlu */
    pvm_meta_instruction_id_bxori, /* bxori */
    pvm_meta_instruction_id_bxoriu, /* bxoriu */
    pvm_meta_instruction_id_bxorl, /* bxorl */
    pvm_meta_instruction_id_bxorlu, /* bxorlu */
    pvm_meta_instruction_id_bzi, /* bzi/fR */
    pvm_meta_instruction_id_bziu, /* bziu/fR */
    pvm_meta_instruction_id_bzl, /* bzl/fR */
    pvm_meta_instruction_id_bzlu, /* bzlu/fR */
    pvm_meta_instruction_id_call, /* call/retR */
    pvm_meta_instruction_id_canary, /* canary */
    pvm_meta_instruction_id_close, /* close */
    pvm_meta_instruction_id_ctos, /* ctos */
    pvm_meta_instruction_id_disas, /* disas */
    pvm_meta_instruction_id_divi, /* divi */
    pvm_meta_instruction_id_diviu, /* diviu */
    pvm_meta_instruction_id_divl, /* divl */
    pvm_meta_instruction_id_divlu, /* divlu */
    pvm_meta_instruction_id_drop, /* drop */
    pvm_meta_instruction_id_drop2, /* drop2 */
    pvm_meta_instruction_id_drop3, /* drop3 */
    pvm_meta_instruction_id_drop4, /* drop4 */
    pvm_meta_instruction_id_duc, /* duc */
    pvm_meta_instruction_id_dup, /* dup */
    pvm_meta_instruction_id_endhl, /* endhl */
    pvm_meta_instruction_id_endsc, /* endsc */
    pvm_meta_instruction_id_eqi, /* eqi */
    pvm_meta_instruction_id_eqiu, /* eqiu */
    pvm_meta_instruction_id_eql, /* eql */
    pvm_meta_instruction_id_eqlu, /* eqlu */
    pvm_meta_instruction_id_eqs, /* eqs */
    pvm_meta_instruction_id_exit, /* exit */
    pvm_meta_instruction_id_exitvm, /* exitvm */
    pvm_meta_instruction_id_flush, /* flush */
    pvm_meta_instruction_id_fromr, /* fromr */
    pvm_meta_instruction_id_gei, /* gei */
    pvm_meta_instruction_id_geiu, /* geiu */
    pvm_meta_instruction_id_gel, /* gel */
    pvm_meta_instruction_id_gelu, /* gelu */
    pvm_meta_instruction_id_ges, /* ges */
    pvm_meta_instruction_id_getenv, /* getenv */
    pvm_meta_instruction_id_gti, /* gti */
    pvm_meta_instruction_id_gtiu, /* gtiu */
    pvm_meta_instruction_id_gtl, /* gtl */
    pvm_meta_instruction_id_gtlu, /* gtlu */
    pvm_meta_instruction_id_gts, /* gts */
    pvm_meta_instruction_id_indent, /* indent */
    pvm_meta_instruction_id_iogetb, /* iogetb */
    pvm_meta_instruction_id_iosetb, /* iosetb */
    pvm_meta_instruction_id_iosize, /* iosize */
    pvm_meta_instruction_id_isa, /* isa */
    pvm_meta_instruction_id_itoi, /* itoi/nR */
    pvm_meta_instruction_id_itoiu, /* itoiu/nR */
    pvm_meta_instruction_id_itol, /* itol/nR */
    pvm_meta_instruction_id_itolu, /* itolu/nR */
    pvm_meta_instruction_id_iutoi, /* iutoi/nR */
    pvm_meta_instruction_id_iutoiu, /* iutoiu/nR */
    pvm_meta_instruction_id_iutol, /* iutol/nR */
    pvm_meta_instruction_id_iutolu, /* iutolu/nR */
    pvm_meta_instruction_id_lei, /* lei */
    pvm_meta_instruction_id_leiu, /* leiu */
    pvm_meta_instruction_id_lel, /* lel */
    pvm_meta_instruction_id_lelu, /* lelu */
    pvm_meta_instruction_id_les, /* les */
    pvm_meta_instruction_id_lti, /* lti */
    pvm_meta_instruction_id_ltiu, /* ltiu */
    pvm_meta_instruction_id_ltl, /* ltl */
    pvm_meta_instruction_id_ltlu, /* ltlu */
    pvm_meta_instruction_id_ltoi, /* ltoi/nR */
    pvm_meta_instruction_id_ltoiu, /* ltoiu/nR */
    pvm_meta_instruction_id_ltol, /* ltol/nR */
    pvm_meta_instruction_id_ltolu, /* ltolu/nR */
    pvm_meta_instruction_id_lts, /* lts */
    pvm_meta_instruction_id_lutoi, /* lutoi/nR */
    pvm_meta_instruction_id_lutoiu, /* lutoiu/nR */
    pvm_meta_instruction_id_lutol, /* lutol/nR */
    pvm_meta_instruction_id_lutolu, /* lutolu/nR */
    pvm_meta_instruction_id_map, /* map */
    pvm_meta_instruction_id_mgetios, /* mgetios */
    pvm_meta_instruction_id_mgetm, /* mgetm */
    pvm_meta_instruction_id_mgeto, /* mgeto */
    pvm_meta_instruction_id_mgets, /* mgets */
    pvm_meta_instruction_id_mgetsel, /* mgetsel */
    pvm_meta_instruction_id_mgetsiz, /* mgetsiz */
    pvm_meta_instruction_id_mgetw, /* mgetw */
    pvm_meta_instruction_id_mka, /* mka */
    pvm_meta_instruction_id_mko, /* mko */
    pvm_meta_instruction_id_mksct, /* mksct */
    pvm_meta_instruction_id_mktya, /* mktya */
    pvm_meta_instruction_id_mktyany, /* mktyany */
    pvm_meta_instruction_id_mktyc, /* mktyc */
    pvm_meta_instruction_id_mktyi, /* mktyi */
    pvm_meta_instruction_id_mktyo, /* mktyo */
    pvm_meta_instruction_id_mktys, /* mktys */
    pvm_meta_instruction_id_mktysct, /* mktysct */
    pvm_meta_instruction_id_mktyv, /* mktyv */
    pvm_meta_instruction_id_mm, /* mm */
    pvm_meta_instruction_id_modi, /* modi */
    pvm_meta_instruction_id_modiu, /* modiu */
    pvm_meta_instruction_id_modl, /* modl */
    pvm_meta_instruction_id_modlu, /* modlu */
    pvm_meta_instruction_id_msetios, /* msetios */
    pvm_meta_instruction_id_msetm, /* msetm */
    pvm_meta_instruction_id_mseto, /* mseto */
    pvm_meta_instruction_id_msets, /* msets */
    pvm_meta_instruction_id_msetsel, /* msetsel */
    pvm_meta_instruction_id_msetsiz, /* msetsiz */
    pvm_meta_instruction_id_msetw, /* msetw */
    pvm_meta_instruction_id_muli, /* muli */
    pvm_meta_instruction_id_muliu, /* muliu */
    pvm_meta_instruction_id_mull, /* mull */
    pvm_meta_instruction_id_mullu, /* mullu */
    pvm_meta_instruction_id_muls, /* muls */
    pvm_meta_instruction_id_negi, /* negi */
    pvm_meta_instruction_id_negiu, /* negiu */
    pvm_meta_instruction_id_negl, /* negl */
    pvm_meta_instruction_id_neglu, /* neglu */
    pvm_meta_instruction_id_nei, /* nei */
    pvm_meta_instruction_id_neiu, /* neiu */
    pvm_meta_instruction_id_nel, /* nel */
    pvm_meta_instruction_id_nelu, /* nelu */
    pvm_meta_instruction_id_nes, /* nes */
    pvm_meta_instruction_id_nip, /* nip */
    pvm_meta_instruction_id_nip2, /* nip2 */
    pvm_meta_instruction_id_nip3, /* nip3 */
    pvm_meta_instruction_id_nn, /* nn */
    pvm_meta_instruction_id_nnn, /* nnn */
    pvm_meta_instruction_id_nop, /* nop */
    pvm_meta_instruction_id_not, /* not */
    pvm_meta_instruction_id_note, /* note/nR */
    pvm_meta_instruction_id_nrot, /* nrot */
    pvm_meta_instruction_id_ogetbt, /* ogetbt */
    pvm_meta_instruction_id_ogetm, /* ogetm */
    pvm_meta_instruction_id_ogetu, /* ogetu */
    pvm_meta_instruction_id_open, /* open */
    pvm_meta_instruction_id_or, /* or */
    pvm_meta_instruction_id_osetm, /* osetm */
    pvm_meta_instruction_id_over, /* over */
    pvm_meta_instruction_id_pec, /* pec */
    pvm_meta_instruction_id_peekdi, /* peekdi/nR */
    pvm_meta_instruction_id_peekdiu, /* peekdiu/nR */
    pvm_meta_instruction_id_peekdl, /* peekdl/nR */
    pvm_meta_instruction_id_peekdlu, /* peekdlu/nR */
    pvm_meta_instruction_id_peeki, /* peeki/nR/nR/nR */
    pvm_meta_instruction_id_peekiu, /* peekiu/nR/nR */
    pvm_meta_instruction_id_peekl, /* peekl/nR/nR/nR */
    pvm_meta_instruction_id_peeklu, /* peeklu/nR/nR */
    pvm_meta_instruction_id_peeks, /* peeks */
    pvm_meta_instruction_id_pokedi, /* pokedi/nR */
    pvm_meta_instruction_id_pokediu, /* pokediu/nR */
    pvm_meta_instruction_id_pokedl, /* pokedl/nR */
    pvm_meta_instruction_id_pokedlu, /* pokedlu/nR */
    pvm_meta_instruction_id_pokei, /* pokei/nR/nR/nR */
    pvm_meta_instruction_id_pokeiu, /* pokeiu/nR/nR */
    pvm_meta_instruction_id_pokel, /* pokel/nR/nR/nR */
    pvm_meta_instruction_id_pokelu, /* pokelu/nR/nR */
    pvm_meta_instruction_id_pokes, /* pokes */
    pvm_meta_instruction_id_pope, /* pope */
    pvm_meta_instruction_id_popend, /* popend */
    pvm_meta_instruction_id_popf, /* popf/nR */
    pvm_meta_instruction_id_popios, /* popios */
    pvm_meta_instruction_id_popoac, /* popoac */
    pvm_meta_instruction_id_popob, /* popob */
    pvm_meta_instruction_id_popobc, /* popobc */
    pvm_meta_instruction_id_popoc, /* popoc */
    pvm_meta_instruction_id_popod, /* popod */
    pvm_meta_instruction_id_popoi, /* popoi */
    pvm_meta_instruction_id_popom, /* popom */
    pvm_meta_instruction_id_popoo, /* popoo */
    pvm_meta_instruction_id_popopp, /* popopp */
    pvm_meta_instruction_id_popr, /* popr/%rR */
    pvm_meta_instruction_id_popvar, /* popvar/nR/nR */
    pvm_meta_instruction_id_powi, /* powi */
    pvm_meta_instruction_id_powiu, /* powiu */
    pvm_meta_instruction_id_powl, /* powl */
    pvm_meta_instruction_id_powlu, /* powlu */
    pvm_meta_instruction_id_printi, /* printi/nR */
    pvm_meta_instruction_id_printiu, /* printiu/nR */
    pvm_meta_instruction_id_printl, /* printl/nR */
    pvm_meta_instruction_id_printlu, /* printlu/nR */
    pvm_meta_instruction_id_prints, /* prints */
    pvm_meta_instruction_id_prolog, /* prolog */
    pvm_meta_instruction_id_push, /* push/nR */
    pvm_meta_instruction_id_push, /* push/lR */
    pvm_meta_instruction_id_push32, /* push32/nR */
    pvm_meta_instruction_id_push32, /* push32/lR */
    pvm_meta_instruction_id_pushe, /* pushe/lR */
    pvm_meta_instruction_id_pushend, /* pushend */
    pvm_meta_instruction_id_pushf, /* pushf/nR */
    pvm_meta_instruction_id_pushhi, /* pushhi/nR */
    pvm_meta_instruction_id_pushhi, /* pushhi/lR */
    pvm_meta_instruction_id_pushios, /* pushios */
    pvm_meta_instruction_id_pushlo, /* pushlo/nR */
    pvm_meta_instruction_id_pushlo, /* pushlo/lR */
    pvm_meta_instruction_id_pushoac, /* pushoac */
    pvm_meta_instruction_id_pushob, /* pushob */
    pvm_meta_instruction_id_pushobc, /* pushobc */
    pvm_meta_instruction_id_pushoc, /* pushoc */
    pvm_meta_instruction_id_pushod, /* pushod */
    pvm_meta_instruction_id_pushoi, /* pushoi */
    pvm_meta_instruction_id_pushom, /* pushom */
    pvm_meta_instruction_id_pushoo, /* pushoo */
    pvm_meta_instruction_id_pushopp, /* pushopp */
    pvm_meta_instruction_id_pushr, /* pushr/%rR */
    pvm_meta_instruction_id_pushtopvar, /* pushtopvar/nR */
    pvm_meta_instruction_id_pushvar, /* pushvar/n0/n0 */
    pvm_meta_instruction_id_pushvar, /* pushvar/n0/n1 */
    pvm_meta_instruction_id_pushvar, /* pushvar/n0/n2 */
    pvm_meta_instruction_id_pushvar, /* pushvar/n0/n3 */
    pvm_meta_instruction_id_pushvar, /* pushvar/n0/n4 */
    pvm_meta_instruction_id_pushvar, /* pushvar/n0/n5 */
    pvm_meta_instruction_id_pushvar, /* pushvar/n0/nR */
    pvm_meta_instruction_id_pushvar, /* pushvar/nR/n0 */
    pvm_meta_instruction_id_pushvar, /* pushvar/nR/n1 */
    pvm_meta_instruction_id_pushvar, /* pushvar/nR/n2 */
    pvm_meta_instruction_id_pushvar, /* pushvar/nR/n3 */
    pvm_meta_instruction_id_pushvar, /* pushvar/nR/n4 */
    pvm_meta_instruction_id_pushvar, /* pushvar/nR/n5 */
    pvm_meta_instruction_id_pushvar, /* pushvar/nR/nR */
    pvm_meta_instruction_id_quake, /* quake */
    pvm_meta_instruction_id_raise, /* raise */
    pvm_meta_instruction_id_rand, /* rand */
    pvm_meta_instruction_id_regvar, /* regvar */
    pvm_meta_instruction_id_reloc, /* reloc */
    pvm_meta_instruction_id_restorer, /* restorer/%rR */
    pvm_meta_instruction_id_return, /* return */
    pvm_meta_instruction_id_revn, /* revn/n3 */
    pvm_meta_instruction_id_revn, /* revn/n4 */
    pvm_meta_instruction_id_revn, /* revn/nR */
    pvm_meta_instruction_id_rot, /* rot */
    pvm_meta_instruction_id_saver, /* saver/%rR */
    pvm_meta_instruction_id_sconc, /* sconc */
    pvm_meta_instruction_id_sel, /* sel */
    pvm_meta_instruction_id_setr, /* setr/%rR */
    pvm_meta_instruction_id_siz, /* siz */
    pvm_meta_instruction_id_smodi, /* smodi */
    pvm_meta_instruction_id_sref, /* sref */
    pvm_meta_instruction_id_srefi, /* srefi */
    pvm_meta_instruction_id_srefia, /* srefia */
    pvm_meta_instruction_id_srefio, /* srefio */
    pvm_meta_instruction_id_srefmnt, /* srefmnt */
    pvm_meta_instruction_id_srefnt, /* srefnt */
    pvm_meta_instruction_id_srefo, /* srefo */
    pvm_meta_instruction_id_sset, /* sset */
    pvm_meta_instruction_id_strace, /* strace/nR */
    pvm_meta_instruction_id_strref, /* strref */
    pvm_meta_instruction_id_subi, /* subi */
    pvm_meta_instruction_id_subiu, /* subiu */
    pvm_meta_instruction_id_subl, /* subl */
    pvm_meta_instruction_id_sublu, /* sublu */
    pvm_meta_instruction_id_substr, /* substr */
    pvm_meta_instruction_id_swap, /* swap */
    pvm_meta_instruction_id_swapgti, /* swapgti */
    pvm_meta_instruction_id_swapgtiu, /* swapgtiu */
    pvm_meta_instruction_id_swapgtl, /* swapgtl */
    pvm_meta_instruction_id_swapgtlu, /* swapgtlu */
    pvm_meta_instruction_id_sync, /* sync */
    pvm_meta_instruction_id_time, /* time */
    pvm_meta_instruction_id_tor, /* tor */
    pvm_meta_instruction_id_tuck, /* tuck */
    pvm_meta_instruction_id_tyagetb, /* tyagetb */
    pvm_meta_instruction_id_tyagett, /* tyagett */
    pvm_meta_instruction_id_tyisc, /* tyisc */
    pvm_meta_instruction_id_tyissct, /* tyissct */
    pvm_meta_instruction_id_typof, /* typof */
    pvm_meta_instruction_id_tysctn, /* tysctn */
    pvm_meta_instruction_id_unmap, /* unmap */
    pvm_meta_instruction_id_unreachable, /* unreachable */
    pvm_meta_instruction_id_ureloc, /* ureloc */
    pvm_meta_instruction_id_write, /* write/retR */
    pvm_meta_instruction_id_ba, /* *ba/fR*-no-fast-branches */
    pvm_meta_instruction_id_bn, /* *bn/fR*-no-fast-branches */
    pvm_meta_instruction_id_bnn, /* *bnn/fR*-no-fast-branches */
    pvm_meta_instruction_id_bnzi, /* *bnzi/fR*-no-fast-branches */
    pvm_meta_instruction_id_bnziu, /* *bnziu/fR*-no-fast-branches */
    pvm_meta_instruction_id_bnzl, /* *bnzl/fR*-no-fast-branches */
    pvm_meta_instruction_id_bnzlu, /* *bnzlu/fR*-no-fast-branches */
    pvm_meta_instruction_id_bzi, /* *bzi/fR*-no-fast-branches */
    pvm_meta_instruction_id_bziu, /* *bziu/fR*-no-fast-branches */
    pvm_meta_instruction_id_bzl, /* *bzl/fR*-no-fast-branches */
    pvm_meta_instruction_id_bzlu /* *bzlu/fR*-no-fast-branches */
    };

#ifdef JITTER_HAVE_PATCH_IN
/* Worst-case defect table. */
const jitter_uint
pvm_worst_case_defect_table [] =
  {
    pvm_specialized_instruction_opcode__eINVALID, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode__eBEGINBASICBLOCK, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode__eEXITVM, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode__eDATALOCATIONS, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode__eNOP, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode__eUNREACHABLE0, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode__eUNREACHABLE1, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode__eUNREACHABLE2, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_addi, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_addiu, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_addl, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_addlu, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_ains, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_and, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_aref, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_arefo, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_arem, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_aset, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_asettb, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_atr, /* NOT potentially defective. */
    /*pvm_specialized_instruction_opcode__eINVALID*/pvm_specialized_instruction_opcode__Aba__fR_A_mno_mfast_mbranches, /* POTENTIALLY DEFECTIVE. */
    pvm_specialized_instruction_opcode_bandi, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_bandiu, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_bandl, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_bandlu, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_beghl, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_begsc, /* NOT potentially defective. */
    /*pvm_specialized_instruction_opcode__eINVALID*/pvm_specialized_instruction_opcode__Abn__fR_A_mno_mfast_mbranches, /* POTENTIALLY DEFECTIVE. */
    /*pvm_specialized_instruction_opcode__eINVALID*/pvm_specialized_instruction_opcode__Abnn__fR_A_mno_mfast_mbranches, /* POTENTIALLY DEFECTIVE. */
    pvm_specialized_instruction_opcode_bnoti, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_bnotiu, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_bnotl, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_bnotlu, /* NOT potentially defective. */
    /*pvm_specialized_instruction_opcode__eINVALID*/pvm_specialized_instruction_opcode__Abnzi__fR_A_mno_mfast_mbranches, /* POTENTIALLY DEFECTIVE. */
    /*pvm_specialized_instruction_opcode__eINVALID*/pvm_specialized_instruction_opcode__Abnziu__fR_A_mno_mfast_mbranches, /* POTENTIALLY DEFECTIVE. */
    /*pvm_specialized_instruction_opcode__eINVALID*/pvm_specialized_instruction_opcode__Abnzl__fR_A_mno_mfast_mbranches, /* POTENTIALLY DEFECTIVE. */
    /*pvm_specialized_instruction_opcode__eINVALID*/pvm_specialized_instruction_opcode__Abnzlu__fR_A_mno_mfast_mbranches, /* POTENTIALLY DEFECTIVE. */
    pvm_specialized_instruction_opcode_bori, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_boriu, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_borl, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_borlu, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_bsli, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_bsliu, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_bsll, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_bsllu, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_bsri, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_bsriu, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_bsrl, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_bsrlu, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_bxori, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_bxoriu, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_bxorl, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_bxorlu, /* NOT potentially defective. */
    /*pvm_specialized_instruction_opcode__eINVALID*/pvm_specialized_instruction_opcode__Abzi__fR_A_mno_mfast_mbranches, /* POTENTIALLY DEFECTIVE. */
    /*pvm_specialized_instruction_opcode__eINVALID*/pvm_specialized_instruction_opcode__Abziu__fR_A_mno_mfast_mbranches, /* POTENTIALLY DEFECTIVE. */
    /*pvm_specialized_instruction_opcode__eINVALID*/pvm_specialized_instruction_opcode__Abzl__fR_A_mno_mfast_mbranches, /* POTENTIALLY DEFECTIVE. */
    /*pvm_specialized_instruction_opcode__eINVALID*/pvm_specialized_instruction_opcode__Abzlu__fR_A_mno_mfast_mbranches, /* POTENTIALLY DEFECTIVE. */
    pvm_specialized_instruction_opcode_call__retR, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_canary, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_close, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_ctos, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_disas, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_divi, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_diviu, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_divl, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_divlu, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_drop, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_drop2, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_drop3, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_drop4, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_duc, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_dup, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_endhl, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_endsc, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_eqi, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_eqiu, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_eql, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_eqlu, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_eqs, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_exit, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_exitvm, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_flush, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_fromr, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_gei, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_geiu, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_gel, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_gelu, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_ges, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_getenv, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_gti, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_gtiu, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_gtl, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_gtlu, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_gts, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_indent, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_iogetb, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_iosetb, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_iosize, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_isa, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_itoi__nR, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_itoiu__nR, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_itol__nR, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_itolu__nR, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_iutoi__nR, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_iutoiu__nR, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_iutol__nR, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_iutolu__nR, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_lei, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_leiu, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_lel, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_lelu, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_les, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_lti, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_ltiu, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_ltl, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_ltlu, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_ltoi__nR, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_ltoiu__nR, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_ltol__nR, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_ltolu__nR, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_lts, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_lutoi__nR, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_lutoiu__nR, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_lutol__nR, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_lutolu__nR, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_map, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_mgetios, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_mgetm, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_mgeto, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_mgets, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_mgetsel, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_mgetsiz, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_mgetw, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_mka, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_mko, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_mksct, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_mktya, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_mktyany, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_mktyc, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_mktyi, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_mktyo, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_mktys, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_mktysct, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_mktyv, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_mm, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_modi, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_modiu, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_modl, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_modlu, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_msetios, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_msetm, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_mseto, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_msets, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_msetsel, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_msetsiz, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_msetw, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_muli, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_muliu, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_mull, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_mullu, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_muls, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_negi, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_negiu, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_negl, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_neglu, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_nei, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_neiu, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_nel, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_nelu, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_nes, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_nip, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_nip2, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_nip3, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_nn, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_nnn, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_nop, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_not, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_note__nR, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_nrot, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_ogetbt, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_ogetm, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_ogetu, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_open, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_or, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_osetm, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_over, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_pec, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_peekdi__nR, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_peekdiu__nR, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_peekdl__nR, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_peekdlu__nR, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_peeki__nR__nR__nR, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_peekiu__nR__nR, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_peekl__nR__nR__nR, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_peeklu__nR__nR, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_peeks, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_pokedi__nR, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_pokediu__nR, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_pokedl__nR, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_pokedlu__nR, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_pokei__nR__nR__nR, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_pokeiu__nR__nR, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_pokel__nR__nR__nR, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_pokelu__nR__nR, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_pokes, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_pope, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_popend, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_popf__nR, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_popios, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_popoac, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_popob, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_popobc, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_popoc, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_popod, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_popoi, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_popom, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_popoo, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_popopp, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_popr___rrR, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_popvar__nR__nR, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_powi, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_powiu, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_powl, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_powlu, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_printi__nR, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_printiu__nR, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_printl__nR, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_printlu__nR, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_prints, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_prolog, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_push__nR, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_push__lR, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_push32__nR, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_push32__lR, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_pushe__lR, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_pushend, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_pushf__nR, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_pushhi__nR, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_pushhi__lR, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_pushios, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_pushlo__nR, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_pushlo__lR, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_pushoac, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_pushob, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_pushobc, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_pushoc, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_pushod, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_pushoi, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_pushom, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_pushoo, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_pushopp, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_pushr___rrR, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_pushtopvar__nR, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_pushvar__n0__n0, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_pushvar__n0__n1, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_pushvar__n0__n2, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_pushvar__n0__n3, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_pushvar__n0__n4, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_pushvar__n0__n5, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_pushvar__n0__nR, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_pushvar__nR__n0, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_pushvar__nR__n1, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_pushvar__nR__n2, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_pushvar__nR__n3, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_pushvar__nR__n4, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_pushvar__nR__n5, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_pushvar__nR__nR, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_quake, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_raise, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_rand, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_regvar, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_reloc, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_restorer___rrR, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_return, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_revn__n3, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_revn__n4, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_revn__nR, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_rot, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_saver___rrR, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_sconc, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_sel, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_setr___rrR, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_siz, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_smodi, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_sref, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_srefi, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_srefia, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_srefio, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_srefmnt, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_srefnt, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_srefo, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_sset, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_strace__nR, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_strref, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_subi, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_subiu, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_subl, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_sublu, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_substr, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_swap, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_swapgti, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_swapgtiu, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_swapgtl, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_swapgtlu, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_sync, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_time, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_tor, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_tuck, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_tyagetb, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_tyagett, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_tyisc, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_tyissct, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_typof, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_tysctn, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_unmap, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_unreachable, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_ureloc, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode_write__retR, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode__Aba__fR_A_mno_mfast_mbranches, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode__Abn__fR_A_mno_mfast_mbranches, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode__Abnn__fR_A_mno_mfast_mbranches, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode__Abnzi__fR_A_mno_mfast_mbranches, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode__Abnziu__fR_A_mno_mfast_mbranches, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode__Abnzl__fR_A_mno_mfast_mbranches, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode__Abnzlu__fR_A_mno_mfast_mbranches, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode__Abzi__fR_A_mno_mfast_mbranches, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode__Abziu__fR_A_mno_mfast_mbranches, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode__Abzl__fR_A_mno_mfast_mbranches, /* NOT potentially defective. */
    pvm_specialized_instruction_opcode__Abzlu__fR_A_mno_mfast_mbranches /* NOT potentially defective. */
  };
#endif // #ifdef JITTER_HAVE_PATCH_IN


void
pvm_rewrite (struct jitter_mutable_routine *jitter_mutable_routine_p)
{
  JITTTER_REWRITE_FUNCTION_PROLOG_;

/* User-specified code, rewriter part: beginning. */

/* User-specified code, rewriter part: end */


//asm volatile ("\n# checking swap-drop-to-nip");
//fprintf (stderr, "Trying rule 1 of 9, \"swap-drop-to-nip\" (line 5804)\n");
/* Rewrite rule "swap-drop-to-nip" */
#line 5804 "../../libpoke/pvm.jitter"
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
#line 5801 "../../libpoke/pvm.jitter"
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, swap)
#line 5802 "../../libpoke/pvm.jitter"
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, drop)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule swap-drop-to-nip (line 5804) fires...\n");
#line 5804 "../../libpoke/pvm.jitter"
    //fprintf (stderr, "    rewrite: adding instruction nip\n");
    JITTER_RULE_APPEND_INSTRUCTION_(nip);
    //fprintf (stderr, "  ...End of the rule swap-drop-to-nip\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking rot-rot-to-nrot");
//fprintf (stderr, "Trying rule 2 of 9, \"rot-rot-to-nrot\" (line 5810)\n");
/* Rewrite rule "rot-rot-to-nrot" */
#line 5810 "../../libpoke/pvm.jitter"
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
#line 5807 "../../libpoke/pvm.jitter"
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, rot)
#line 5808 "../../libpoke/pvm.jitter"
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, rot)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule rot-rot-to-nrot (line 5810) fires...\n");
#line 5810 "../../libpoke/pvm.jitter"
    //fprintf (stderr, "    rewrite: adding instruction nrot\n");
    JITTER_RULE_APPEND_INSTRUCTION_(nrot);
    //fprintf (stderr, "  ...End of the rule rot-rot-to-nrot\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking nip-nip-to-nip2");
//fprintf (stderr, "Trying rule 3 of 9, \"nip-nip-to-nip2\" (line 5816)\n");
/* Rewrite rule "nip-nip-to-nip2" */
#line 5816 "../../libpoke/pvm.jitter"
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
#line 5813 "../../libpoke/pvm.jitter"
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, nip)
#line 5814 "../../libpoke/pvm.jitter"
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, nip)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule nip-nip-to-nip2 (line 5816) fires...\n");
#line 5816 "../../libpoke/pvm.jitter"
    //fprintf (stderr, "    rewrite: adding instruction nip2\n");
    JITTER_RULE_APPEND_INSTRUCTION_(nip2);
    //fprintf (stderr, "  ...End of the rule nip-nip-to-nip2\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking nip2-nip-to-nip3");
//fprintf (stderr, "Trying rule 4 of 9, \"nip2-nip-to-nip3\" (line 5822)\n");
/* Rewrite rule "nip2-nip-to-nip3" */
#line 5822 "../../libpoke/pvm.jitter"
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
#line 5819 "../../libpoke/pvm.jitter"
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, nip2)
#line 5820 "../../libpoke/pvm.jitter"
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, nip)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule nip2-nip-to-nip3 (line 5822) fires...\n");
#line 5822 "../../libpoke/pvm.jitter"
    //fprintf (stderr, "    rewrite: adding instruction nip3\n");
    JITTER_RULE_APPEND_INSTRUCTION_(nip3);
    //fprintf (stderr, "  ...End of the rule nip2-nip-to-nip3\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking drop-drop-to-drop2");
//fprintf (stderr, "Trying rule 5 of 9, \"drop-drop-to-drop2\" (line 5828)\n");
/* Rewrite rule "drop-drop-to-drop2" */
#line 5828 "../../libpoke/pvm.jitter"
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
#line 5825 "../../libpoke/pvm.jitter"
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, drop)
#line 5826 "../../libpoke/pvm.jitter"
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, drop)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule drop-drop-to-drop2 (line 5828) fires...\n");
#line 5828 "../../libpoke/pvm.jitter"
    //fprintf (stderr, "    rewrite: adding instruction drop2\n");
    JITTER_RULE_APPEND_INSTRUCTION_(drop2);
    //fprintf (stderr, "  ...End of the rule drop-drop-to-drop2\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking drop2-drop-to-drop3");
//fprintf (stderr, "Trying rule 6 of 9, \"drop2-drop-to-drop3\" (line 5834)\n");
/* Rewrite rule "drop2-drop-to-drop3" */
#line 5834 "../../libpoke/pvm.jitter"
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
#line 5831 "../../libpoke/pvm.jitter"
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, drop2)
#line 5832 "../../libpoke/pvm.jitter"
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, drop)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule drop2-drop-to-drop3 (line 5834) fires...\n");
#line 5834 "../../libpoke/pvm.jitter"
    //fprintf (stderr, "    rewrite: adding instruction drop3\n");
    JITTER_RULE_APPEND_INSTRUCTION_(drop3);
    //fprintf (stderr, "  ...End of the rule drop2-drop-to-drop3\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking drop3-drop-to-drop4");
//fprintf (stderr, "Trying rule 7 of 9, \"drop3-drop-to-drop4\" (line 5840)\n");
/* Rewrite rule "drop3-drop-to-drop4" */
#line 5840 "../../libpoke/pvm.jitter"
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
#line 5837 "../../libpoke/pvm.jitter"
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, drop3)
#line 5838 "../../libpoke/pvm.jitter"
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, drop)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule drop3-drop-to-drop4 (line 5840) fires...\n");
#line 5840 "../../libpoke/pvm.jitter"
    //fprintf (stderr, "    rewrite: adding instruction drop4\n");
    JITTER_RULE_APPEND_INSTRUCTION_(drop4);
    //fprintf (stderr, "  ...End of the rule drop3-drop-to-drop4\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking swap-over-to-tuck");
//fprintf (stderr, "Trying rule 8 of 9, \"swap-over-to-tuck\" (line 5846)\n");
/* Rewrite rule "swap-over-to-tuck" */
#line 5846 "../../libpoke/pvm.jitter"
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
#line 5843 "../../libpoke/pvm.jitter"
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, swap)
#line 5844 "../../libpoke/pvm.jitter"
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, over)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule swap-over-to-tuck (line 5846) fires...\n");
#line 5846 "../../libpoke/pvm.jitter"
    //fprintf (stderr, "    rewrite: adding instruction tuck\n");
    JITTER_RULE_APPEND_INSTRUCTION_(tuck);
    //fprintf (stderr, "  ...End of the rule swap-over-to-tuck\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking rot-swap-to-quake");
//fprintf (stderr, "Trying rule 9 of 9, \"rot-swap-to-quake\" (line 5852)\n");
/* Rewrite rule "rot-swap-to-quake" */
#line 5852 "../../libpoke/pvm.jitter"
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
#line 5849 "../../libpoke/pvm.jitter"
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, rot)
#line 5850 "../../libpoke/pvm.jitter"
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, swap)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule rot-swap-to-quake (line 5852) fires...\n");
#line 5852 "../../libpoke/pvm.jitter"
    //fprintf (stderr, "    rewrite: adding instruction quake\n");
    JITTER_RULE_APPEND_INSTRUCTION_(quake);
    //fprintf (stderr, "  ...End of the rule rot-swap-to-quake\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//fprintf (stderr, "No more rules to try\n");
}


//#include <jitter/jitter-fatal.h>

//#include <jitter/jitter.h>
//#include <jitter/jitter-instruction.h>
//#include <jitter/jitter-specialize.h>

//#include "pvm-vm.h"
//#include "pvm-meta-instructions.h"
//#include "pvm-specialized-instructions.h"


/* Recognizer function prototypes. */
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_addi (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_addiu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_addl (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_addlu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_ains (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_and (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_aref (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_arefo (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_arem (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_aset (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_asettb (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_atr (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_ba (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_ba__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bandi (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bandiu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bandl (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bandlu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_beghl (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_begsc (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bn (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bn__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bnn (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bnn__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bnoti (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bnotiu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bnotl (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bnotlu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bnzi (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bnzi__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bnziu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bnziu__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bnzl (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bnzl__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bnzlu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bnzlu__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bori (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_boriu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_borl (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_borlu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bsli (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bsliu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bsll (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bsllu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bsri (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bsriu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bsrl (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bsrlu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bxori (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bxoriu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bxorl (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bxorlu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bzi (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bzi__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bziu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bziu__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bzl (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bzl__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bzlu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bzlu__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_call (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_canary (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_close (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_ctos (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_disas (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_divi (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_diviu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_divl (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_divlu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_drop (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_drop2 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_drop3 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_drop4 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_duc (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_dup (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_endhl (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_endsc (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_eqi (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_eqiu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_eql (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_eqlu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_eqs (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_exit (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_exitvm (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_flush (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_fromr (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_gei (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_geiu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_gel (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_gelu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_ges (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_getenv (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_gti (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_gtiu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_gtl (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_gtlu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_gts (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_indent (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_iogetb (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_iosetb (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_iosize (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_isa (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_itoi (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_itoi__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_itoiu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_itoiu__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_itol (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_itol__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_itolu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_itolu__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_iutoi (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_iutoi__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_iutoiu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_iutoiu__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_iutol (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_iutol__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_iutolu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_iutolu__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_lei (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_leiu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_lel (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_lelu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_les (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_lti (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_ltiu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_ltl (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_ltlu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_ltoi (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_ltoi__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_ltoiu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_ltoiu__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_ltol (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_ltol__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_ltolu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_ltolu__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_lts (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_lutoi (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_lutoi__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_lutoiu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_lutoiu__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_lutol (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_lutol__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_lutolu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_lutolu__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_map (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_mgetios (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_mgetm (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_mgeto (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_mgets (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_mgetsel (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_mgetsiz (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_mgetw (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_mka (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_mko (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_mksct (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_mktya (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_mktyany (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_mktyc (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_mktyi (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_mktyo (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_mktys (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_mktysct (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_mktyv (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_mm (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_modi (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_modiu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_modl (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_modlu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_msetios (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_msetm (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_mseto (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_msets (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_msetsel (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_msetsiz (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_msetw (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_muli (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_muliu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_mull (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_mullu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_muls (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_negi (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_negiu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_negl (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_neglu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_nei (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_neiu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_nel (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_nelu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_nes (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_nip (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_nip2 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_nip3 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_nn (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_nnn (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_nop (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_not (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_note (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_note__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_nrot (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_ogetbt (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_ogetm (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_ogetu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_open (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_or (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_osetm (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_over (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pec (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_peekdi (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_peekdi__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_peekdiu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_peekdiu__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_peekdl (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_peekdl__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_peekdlu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_peekdlu__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_peeki (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_peeki__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_peeki__nR__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_peeki__nR__nR__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_peekiu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_peekiu__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_peekiu__nR__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_peekl (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_peekl__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_peekl__nR__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_peekl__nR__nR__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_peeklu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_peeklu__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_peeklu__nR__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_peeks (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pokedi (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pokedi__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pokediu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pokediu__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pokedl (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pokedl__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pokedlu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pokedlu__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pokei (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pokei__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pokei__nR__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pokei__nR__nR__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pokeiu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pokeiu__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pokeiu__nR__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pokel (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pokel__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pokel__nR__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pokel__nR__nR__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pokelu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pokelu__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pokelu__nR__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pokes (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pope (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_popend (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_popf (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_popf__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_popios (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_popoac (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_popob (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_popobc (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_popoc (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_popod (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_popoi (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_popom (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_popoo (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_popopp (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_popr (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_popr___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_popvar (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_popvar__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_popvar__nR__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_powi (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_powiu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_powl (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_powlu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_printi (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_printi__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_printiu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_printiu__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_printl (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_printl__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_printlu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_printlu__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_prints (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_prolog (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_push (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_push__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_push__lR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_push32 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_push32__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_push32__lR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushe (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushe__lR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushend (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushf (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushf__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushhi (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushhi__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushhi__lR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushios (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushlo (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushlo__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushlo__lR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushoac (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushob (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushobc (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushoc (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushod (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushoi (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushom (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushoo (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushopp (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushr (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushr___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushtopvar (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushtopvar__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushvar (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushvar__n0 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushvar__n0__n0 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushvar__n0__n1 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushvar__n0__n2 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushvar__n0__n3 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushvar__n0__n4 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushvar__n0__n5 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushvar__n0__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushvar__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushvar__nR__n0 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushvar__nR__n1 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushvar__nR__n2 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushvar__nR__n3 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushvar__nR__n4 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushvar__nR__n5 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushvar__nR__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_quake (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_raise (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_rand (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_regvar (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_reloc (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_restorer (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_restorer___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_return (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_revn (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_revn__n3 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_revn__n4 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_revn__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_rot (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_saver (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_saver___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_sconc (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_sel (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_setr (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_setr___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_siz (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_smodi (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_sref (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_srefi (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_srefia (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_srefio (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_srefmnt (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_srefnt (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_srefo (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_sset (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_strace (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_strace__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_strref (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_subi (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_subiu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_subl (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_sublu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_substr (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_swap (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_swapgti (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_swapgtiu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_swapgtl (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_swapgtlu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_sync (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_time (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_tor (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_tuck (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_tyagetb (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_tyagett (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_tyisc (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_tyissct (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_typof (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_tysctn (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_unmap (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_unreachable (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_ureloc (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_write (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));


/* Recognizer function definitions. */
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_addi (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_addi;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_addiu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_addiu;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_addl (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_addl;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_addlu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_addlu;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_ains (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_ains;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_and (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_and;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_aref (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_aref;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_arefo (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_arefo;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_arem (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_arem;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_aset (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_aset;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_asettb (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_asettb;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_atr (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_atr;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_ba (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = pvm_recognize_specialized_instruction_ba__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_ba__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_ba__fR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bandi (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_bandi;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bandiu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_bandiu;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bandl (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_bandl;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bandlu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_bandlu;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_beghl (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_beghl;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_begsc (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_begsc;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bn (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = pvm_recognize_specialized_instruction_bn__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bn__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_bn__fR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bnn (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = pvm_recognize_specialized_instruction_bnn__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bnn__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_bnn__fR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bnoti (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_bnoti;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bnotiu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_bnotiu;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bnotl (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_bnotl;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bnotlu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_bnotlu;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bnzi (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = pvm_recognize_specialized_instruction_bnzi__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bnzi__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_bnzi__fR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bnziu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = pvm_recognize_specialized_instruction_bnziu__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bnziu__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_bnziu__fR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bnzl (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = pvm_recognize_specialized_instruction_bnzl__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bnzl__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_bnzl__fR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bnzlu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = pvm_recognize_specialized_instruction_bnzlu__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bnzlu__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_bnzlu__fR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bori (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_bori;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_boriu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_boriu;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_borl (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_borl;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_borlu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_borlu;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bsli (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_bsli;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bsliu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_bsliu;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bsll (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_bsll;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bsllu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_bsllu;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bsri (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_bsri;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bsriu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_bsriu;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bsrl (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_bsrl;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bsrlu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_bsrlu;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bxori (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_bxori;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bxoriu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_bxoriu;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bxorl (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_bxorl;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bxorlu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_bxorlu;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bzi (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = pvm_recognize_specialized_instruction_bzi__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bzi__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_bzi__fR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bziu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = pvm_recognize_specialized_instruction_bziu__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bziu__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_bziu__fR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bzl (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = pvm_recognize_specialized_instruction_bzl__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bzl__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_bzl__fR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bzlu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = pvm_recognize_specialized_instruction_bzlu__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bzlu__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_bzlu__fR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_call (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_call__retR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_canary (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_canary;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_close (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_close;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_ctos (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_ctos;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_disas (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_disas;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_divi (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_divi;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_diviu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_diviu;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_divl (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_divl;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_divlu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_divlu;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_drop (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_drop;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_drop2 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_drop2;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_drop3 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_drop3;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_drop4 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_drop4;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_duc (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_duc;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_dup (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_dup;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_endhl (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_endhl;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_endsc (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_endsc;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_eqi (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_eqi;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_eqiu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_eqiu;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_eql (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_eql;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_eqlu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_eqlu;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_eqs (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_eqs;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_exit (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_exit;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_exitvm (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_exitvm;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_flush (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_flush;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_fromr (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_fromr;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_gei (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_gei;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_geiu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_geiu;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_gel (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_gel;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_gelu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_gelu;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_ges (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_ges;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_getenv (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_getenv;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_gti (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_gti;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_gtiu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_gtiu;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_gtl (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_gtl;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_gtlu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_gtlu;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_gts (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_gts;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_indent (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_indent;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_iogetb (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_iogetb;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_iosetb (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_iosetb;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_iosize (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_iosize;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_isa (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_isa;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_itoi (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_itoi__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_itoi__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_itoi__nR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_itoiu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_itoiu__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_itoiu__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_itoiu__nR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_itol (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_itol__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_itol__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_itol__nR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_itolu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_itolu__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_itolu__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_itolu__nR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_iutoi (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_iutoi__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_iutoi__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_iutoi__nR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_iutoiu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_iutoiu__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_iutoiu__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_iutoiu__nR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_iutol (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_iutol__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_iutol__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_iutol__nR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_iutolu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_iutolu__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_iutolu__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_iutolu__nR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_lei (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_lei;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_leiu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_leiu;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_lel (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_lel;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_lelu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_lelu;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_les (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_les;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_lti (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_lti;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_ltiu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_ltiu;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_ltl (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_ltl;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_ltlu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_ltlu;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_ltoi (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_ltoi__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_ltoi__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_ltoi__nR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_ltoiu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_ltoiu__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_ltoiu__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_ltoiu__nR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_ltol (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_ltol__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_ltol__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_ltol__nR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_ltolu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_ltolu__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_ltolu__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_ltolu__nR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_lts (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_lts;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_lutoi (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_lutoi__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_lutoi__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_lutoi__nR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_lutoiu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_lutoiu__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_lutoiu__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_lutoiu__nR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_lutol (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_lutol__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_lutol__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_lutol__nR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_lutolu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_lutolu__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_lutolu__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_lutolu__nR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_map (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_map;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_mgetios (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_mgetios;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_mgetm (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_mgetm;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_mgeto (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_mgeto;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_mgets (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_mgets;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_mgetsel (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_mgetsel;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_mgetsiz (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_mgetsiz;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_mgetw (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_mgetw;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_mka (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_mka;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_mko (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_mko;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_mksct (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_mksct;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_mktya (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_mktya;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_mktyany (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_mktyany;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_mktyc (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_mktyc;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_mktyi (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_mktyi;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_mktyo (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_mktyo;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_mktys (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_mktys;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_mktysct (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_mktysct;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_mktyv (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_mktyv;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_mm (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_mm;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_modi (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_modi;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_modiu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_modiu;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_modl (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_modl;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_modlu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_modlu;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_msetios (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_msetios;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_msetm (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_msetm;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_mseto (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_mseto;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_msets (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_msets;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_msetsel (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_msetsel;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_msetsiz (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_msetsiz;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_msetw (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_msetw;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_muli (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_muli;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_muliu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_muliu;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_mull (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_mull;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_mullu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_mullu;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_muls (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_muls;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_negi (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_negi;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_negiu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_negiu;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_negl (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_negl;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_neglu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_neglu;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_nei (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_nei;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_neiu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_neiu;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_nel (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_nel;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_nelu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_nelu;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_nes (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_nes;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_nip (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_nip;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_nip2 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_nip2;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_nip3 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_nip3;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_nn (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_nn;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_nnn (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_nnn;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_nop (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_nop;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_not (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_not;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_note (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_note__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_note__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_note__nR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_nrot (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_nrot;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_ogetbt (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_ogetbt;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_ogetm (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_ogetm;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_ogetu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_ogetu;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_open (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_open;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_or (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_or;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_osetm (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_osetm;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_over (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_over;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pec (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_pec;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_peekdi (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_peekdi__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_peekdi__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_peekdi__nR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_peekdiu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_peekdiu__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_peekdiu__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_peekdiu__nR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_peekdl (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_peekdl__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_peekdl__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_peekdl__nR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_peekdlu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_peekdlu__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_peekdlu__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_peekdlu__nR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_peeki (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_peeki__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_peeki__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_peeki__nR__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_peeki__nR__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_peeki__nR__nR__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_peeki__nR__nR__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_peeki__nR__nR__nR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_peekiu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_peekiu__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_peekiu__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_peekiu__nR__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_peekiu__nR__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_peekiu__nR__nR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_peekl (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_peekl__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_peekl__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_peekl__nR__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_peekl__nR__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_peekl__nR__nR__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_peekl__nR__nR__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_peekl__nR__nR__nR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_peeklu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_peeklu__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_peeklu__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_peeklu__nR__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_peeklu__nR__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_peeklu__nR__nR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_peeks (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_peeks;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pokedi (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_pokedi__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pokedi__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_pokedi__nR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pokediu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_pokediu__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pokediu__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_pokediu__nR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pokedl (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_pokedl__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pokedl__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_pokedl__nR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pokedlu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_pokedlu__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pokedlu__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_pokedlu__nR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pokei (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_pokei__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pokei__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_pokei__nR__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pokei__nR__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_pokei__nR__nR__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pokei__nR__nR__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_pokei__nR__nR__nR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pokeiu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_pokeiu__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pokeiu__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_pokeiu__nR__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pokeiu__nR__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_pokeiu__nR__nR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pokel (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_pokel__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pokel__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_pokel__nR__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pokel__nR__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_pokel__nR__nR__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pokel__nR__nR__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_pokel__nR__nR__nR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pokelu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_pokelu__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pokelu__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_pokelu__nR__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pokelu__nR__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_pokelu__nR__nR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pokes (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_pokes;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pope (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_pope;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_popend (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_popend;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_popf (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_popf__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_popf__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_popf__nR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_popios (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_popios;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_popoac (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_popoac;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_popob (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_popob;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_popobc (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_popobc;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_popoc (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_popoc;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_popod (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_popod;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_popoi (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_popoi;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_popom (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_popom;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_popoo (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_popoo;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_popopp (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_popopp;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_popr (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = pvm_recognize_specialized_instruction_popr___rrR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_popr___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_popr___rrR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_popvar (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_popvar__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_popvar__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_popvar__nR__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_popvar__nR__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_popvar__nR__nR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_powi (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_powi;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_powiu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_powiu;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_powl (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_powl;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_powlu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_powlu;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_printi (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_printi__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_printi__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_printi__nR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_printiu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_printiu__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_printiu__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_printiu__nR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_printl (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_printl__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_printl__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_printl__nR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_printlu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_printlu__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_printlu__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_printlu__nR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_prints (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_prints;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_prolog (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_prolog;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_push (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_push__nR (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = pvm_recognize_specialized_instruction_push__lR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_push__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_push__nR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_push__lR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_push__lR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_push32 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_push32__nR (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = pvm_recognize_specialized_instruction_push32__lR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_push32__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_push32__nR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_push32__lR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_push32__lR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushe (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = pvm_recognize_specialized_instruction_pushe__lR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushe__lR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_pushe__lR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushend (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_pushend;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushf (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_pushf__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushf__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_pushf__nR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushhi (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_pushhi__nR (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = pvm_recognize_specialized_instruction_pushhi__lR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushhi__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_pushhi__nR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushhi__lR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_pushhi__lR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushios (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_pushios;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushlo (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_pushlo__nR (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = pvm_recognize_specialized_instruction_pushlo__lR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushlo__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_pushlo__nR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushlo__lR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_pushlo__lR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushoac (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_pushoac;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushob (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_pushob;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushobc (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_pushobc;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushoc (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_pushoc;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushod (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_pushod;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushoi (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_pushoi;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushom (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_pushom;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushoo (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_pushoo;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushopp (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_pushopp;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushr (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = pvm_recognize_specialized_instruction_pushr___rrR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushr___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_pushr___rrR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushtopvar (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_pushtopvar__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushtopvar__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_pushtopvar__nR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushvar (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 0 && enable_fast_literals)
      && (res = pvm_recognize_specialized_instruction_pushvar__n0 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_pushvar__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushvar__n0 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 0 && enable_fast_literals)
      && (res = pvm_recognize_specialized_instruction_pushvar__n0__n0 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 1 && enable_fast_literals)
      && (res = pvm_recognize_specialized_instruction_pushvar__n0__n1 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 2 && enable_fast_literals)
      && (res = pvm_recognize_specialized_instruction_pushvar__n0__n2 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 3 && enable_fast_literals)
      && (res = pvm_recognize_specialized_instruction_pushvar__n0__n3 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 4 && enable_fast_literals)
      && (res = pvm_recognize_specialized_instruction_pushvar__n0__n4 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 5 && enable_fast_literals)
      && (res = pvm_recognize_specialized_instruction_pushvar__n0__n5 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_pushvar__n0__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushvar__n0__n0 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_pushvar__n0__n0;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushvar__n0__n1 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_pushvar__n0__n1;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushvar__n0__n2 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_pushvar__n0__n2;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushvar__n0__n3 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_pushvar__n0__n3;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushvar__n0__n4 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_pushvar__n0__n4;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushvar__n0__n5 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_pushvar__n0__n5;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushvar__n0__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_pushvar__n0__nR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushvar__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 0 && enable_fast_literals)
      && (res = pvm_recognize_specialized_instruction_pushvar__nR__n0 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 1 && enable_fast_literals)
      && (res = pvm_recognize_specialized_instruction_pushvar__nR__n1 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 2 && enable_fast_literals)
      && (res = pvm_recognize_specialized_instruction_pushvar__nR__n2 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 3 && enable_fast_literals)
      && (res = pvm_recognize_specialized_instruction_pushvar__nR__n3 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 4 && enable_fast_literals)
      && (res = pvm_recognize_specialized_instruction_pushvar__nR__n4 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 5 && enable_fast_literals)
      && (res = pvm_recognize_specialized_instruction_pushvar__nR__n5 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_pushvar__nR__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushvar__nR__n0 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_pushvar__nR__n0;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushvar__nR__n1 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_pushvar__nR__n1;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushvar__nR__n2 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_pushvar__nR__n2;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushvar__nR__n3 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_pushvar__nR__n3;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushvar__nR__n4 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_pushvar__nR__n4;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushvar__nR__n5 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_pushvar__nR__n5;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushvar__nR__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_pushvar__nR__nR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_quake (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_quake;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_raise (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_raise;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_rand (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_rand;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_regvar (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_regvar;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_reloc (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_reloc;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_restorer (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = pvm_recognize_specialized_instruction_restorer___rrR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_restorer___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_restorer___rrR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_return (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_return;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_revn (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 3 && enable_fast_literals)
      && (res = pvm_recognize_specialized_instruction_revn__n3 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 4 && enable_fast_literals)
      && (res = pvm_recognize_specialized_instruction_revn__n4 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_revn__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_revn__n3 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_revn__n3;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_revn__n4 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_revn__n4;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_revn__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_revn__nR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_rot (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_rot;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_saver (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = pvm_recognize_specialized_instruction_saver___rrR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_saver___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_saver___rrR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_sconc (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_sconc;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_sel (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_sel;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_setr (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = pvm_recognize_specialized_instruction_setr___rrR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_setr___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_setr___rrR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_siz (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_siz;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_smodi (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_smodi;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_sref (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_sref;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_srefi (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_srefi;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_srefia (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_srefia;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_srefio (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_srefio;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_srefmnt (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_srefmnt;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_srefnt (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_srefnt;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_srefo (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_srefo;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_sset (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_sset;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_strace (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_strace__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_strace__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_strace__nR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_strref (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_strref;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_subi (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_subi;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_subiu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_subiu;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_subl (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_subl;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_sublu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_sublu;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_substr (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_substr;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_swap (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_swap;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_swapgti (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_swapgti;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_swapgtiu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_swapgtiu;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_swapgtl (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_swapgtl;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_swapgtlu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_swapgtlu;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_sync (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_sync;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_time (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_time;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_tor (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_tor;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_tuck (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_tuck;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_tyagetb (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_tyagetb;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_tyagett (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_tyagett;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_tyisc (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_tyisc;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_tyissct (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_tyissct;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_typof (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_typof;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_tysctn (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_tysctn;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_unmap (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_unmap;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_unreachable (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_unreachable;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_ureloc (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_ureloc;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_write (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_write__retR;
}



/* Recognizer entry point. */
static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction (struct jitter_mutable_routine *p,
                                            const struct jitter_instruction *ins)
{
  bool fl = ! p->options.slow_literals_only;
  const struct jitter_meta_instruction *mi = ins->meta_instruction;
  switch (mi->id)
    {
    case pvm_meta_instruction_id_addi:
      return pvm_recognize_specialized_instruction_addi (ins->parameters, fl);
    case pvm_meta_instruction_id_addiu:
      return pvm_recognize_specialized_instruction_addiu (ins->parameters, fl);
    case pvm_meta_instruction_id_addl:
      return pvm_recognize_specialized_instruction_addl (ins->parameters, fl);
    case pvm_meta_instruction_id_addlu:
      return pvm_recognize_specialized_instruction_addlu (ins->parameters, fl);
    case pvm_meta_instruction_id_ains:
      return pvm_recognize_specialized_instruction_ains (ins->parameters, fl);
    case pvm_meta_instruction_id_and:
      return pvm_recognize_specialized_instruction_and (ins->parameters, fl);
    case pvm_meta_instruction_id_aref:
      return pvm_recognize_specialized_instruction_aref (ins->parameters, fl);
    case pvm_meta_instruction_id_arefo:
      return pvm_recognize_specialized_instruction_arefo (ins->parameters, fl);
    case pvm_meta_instruction_id_arem:
      return pvm_recognize_specialized_instruction_arem (ins->parameters, fl);
    case pvm_meta_instruction_id_aset:
      return pvm_recognize_specialized_instruction_aset (ins->parameters, fl);
    case pvm_meta_instruction_id_asettb:
      return pvm_recognize_specialized_instruction_asettb (ins->parameters, fl);
    case pvm_meta_instruction_id_atr:
      return pvm_recognize_specialized_instruction_atr (ins->parameters, fl);
    case pvm_meta_instruction_id_ba:
      return pvm_recognize_specialized_instruction_ba (ins->parameters, fl);
    case pvm_meta_instruction_id_bandi:
      return pvm_recognize_specialized_instruction_bandi (ins->parameters, fl);
    case pvm_meta_instruction_id_bandiu:
      return pvm_recognize_specialized_instruction_bandiu (ins->parameters, fl);
    case pvm_meta_instruction_id_bandl:
      return pvm_recognize_specialized_instruction_bandl (ins->parameters, fl);
    case pvm_meta_instruction_id_bandlu:
      return pvm_recognize_specialized_instruction_bandlu (ins->parameters, fl);
    case pvm_meta_instruction_id_beghl:
      return pvm_recognize_specialized_instruction_beghl (ins->parameters, fl);
    case pvm_meta_instruction_id_begsc:
      return pvm_recognize_specialized_instruction_begsc (ins->parameters, fl);
    case pvm_meta_instruction_id_bn:
      return pvm_recognize_specialized_instruction_bn (ins->parameters, fl);
    case pvm_meta_instruction_id_bnn:
      return pvm_recognize_specialized_instruction_bnn (ins->parameters, fl);
    case pvm_meta_instruction_id_bnoti:
      return pvm_recognize_specialized_instruction_bnoti (ins->parameters, fl);
    case pvm_meta_instruction_id_bnotiu:
      return pvm_recognize_specialized_instruction_bnotiu (ins->parameters, fl);
    case pvm_meta_instruction_id_bnotl:
      return pvm_recognize_specialized_instruction_bnotl (ins->parameters, fl);
    case pvm_meta_instruction_id_bnotlu:
      return pvm_recognize_specialized_instruction_bnotlu (ins->parameters, fl);
    case pvm_meta_instruction_id_bnzi:
      return pvm_recognize_specialized_instruction_bnzi (ins->parameters, fl);
    case pvm_meta_instruction_id_bnziu:
      return pvm_recognize_specialized_instruction_bnziu (ins->parameters, fl);
    case pvm_meta_instruction_id_bnzl:
      return pvm_recognize_specialized_instruction_bnzl (ins->parameters, fl);
    case pvm_meta_instruction_id_bnzlu:
      return pvm_recognize_specialized_instruction_bnzlu (ins->parameters, fl);
    case pvm_meta_instruction_id_bori:
      return pvm_recognize_specialized_instruction_bori (ins->parameters, fl);
    case pvm_meta_instruction_id_boriu:
      return pvm_recognize_specialized_instruction_boriu (ins->parameters, fl);
    case pvm_meta_instruction_id_borl:
      return pvm_recognize_specialized_instruction_borl (ins->parameters, fl);
    case pvm_meta_instruction_id_borlu:
      return pvm_recognize_specialized_instruction_borlu (ins->parameters, fl);
    case pvm_meta_instruction_id_bsli:
      return pvm_recognize_specialized_instruction_bsli (ins->parameters, fl);
    case pvm_meta_instruction_id_bsliu:
      return pvm_recognize_specialized_instruction_bsliu (ins->parameters, fl);
    case pvm_meta_instruction_id_bsll:
      return pvm_recognize_specialized_instruction_bsll (ins->parameters, fl);
    case pvm_meta_instruction_id_bsllu:
      return pvm_recognize_specialized_instruction_bsllu (ins->parameters, fl);
    case pvm_meta_instruction_id_bsri:
      return pvm_recognize_specialized_instruction_bsri (ins->parameters, fl);
    case pvm_meta_instruction_id_bsriu:
      return pvm_recognize_specialized_instruction_bsriu (ins->parameters, fl);
    case pvm_meta_instruction_id_bsrl:
      return pvm_recognize_specialized_instruction_bsrl (ins->parameters, fl);
    case pvm_meta_instruction_id_bsrlu:
      return pvm_recognize_specialized_instruction_bsrlu (ins->parameters, fl);
    case pvm_meta_instruction_id_bxori:
      return pvm_recognize_specialized_instruction_bxori (ins->parameters, fl);
    case pvm_meta_instruction_id_bxoriu:
      return pvm_recognize_specialized_instruction_bxoriu (ins->parameters, fl);
    case pvm_meta_instruction_id_bxorl:
      return pvm_recognize_specialized_instruction_bxorl (ins->parameters, fl);
    case pvm_meta_instruction_id_bxorlu:
      return pvm_recognize_specialized_instruction_bxorlu (ins->parameters, fl);
    case pvm_meta_instruction_id_bzi:
      return pvm_recognize_specialized_instruction_bzi (ins->parameters, fl);
    case pvm_meta_instruction_id_bziu:
      return pvm_recognize_specialized_instruction_bziu (ins->parameters, fl);
    case pvm_meta_instruction_id_bzl:
      return pvm_recognize_specialized_instruction_bzl (ins->parameters, fl);
    case pvm_meta_instruction_id_bzlu:
      return pvm_recognize_specialized_instruction_bzlu (ins->parameters, fl);
    case pvm_meta_instruction_id_call:
      return pvm_recognize_specialized_instruction_call (ins->parameters, fl);
    case pvm_meta_instruction_id_canary:
      return pvm_recognize_specialized_instruction_canary (ins->parameters, fl);
    case pvm_meta_instruction_id_close:
      return pvm_recognize_specialized_instruction_close (ins->parameters, fl);
    case pvm_meta_instruction_id_ctos:
      return pvm_recognize_specialized_instruction_ctos (ins->parameters, fl);
    case pvm_meta_instruction_id_disas:
      return pvm_recognize_specialized_instruction_disas (ins->parameters, fl);
    case pvm_meta_instruction_id_divi:
      return pvm_recognize_specialized_instruction_divi (ins->parameters, fl);
    case pvm_meta_instruction_id_diviu:
      return pvm_recognize_specialized_instruction_diviu (ins->parameters, fl);
    case pvm_meta_instruction_id_divl:
      return pvm_recognize_specialized_instruction_divl (ins->parameters, fl);
    case pvm_meta_instruction_id_divlu:
      return pvm_recognize_specialized_instruction_divlu (ins->parameters, fl);
    case pvm_meta_instruction_id_drop:
      return pvm_recognize_specialized_instruction_drop (ins->parameters, fl);
    case pvm_meta_instruction_id_drop2:
      return pvm_recognize_specialized_instruction_drop2 (ins->parameters, fl);
    case pvm_meta_instruction_id_drop3:
      return pvm_recognize_specialized_instruction_drop3 (ins->parameters, fl);
    case pvm_meta_instruction_id_drop4:
      return pvm_recognize_specialized_instruction_drop4 (ins->parameters, fl);
    case pvm_meta_instruction_id_duc:
      return pvm_recognize_specialized_instruction_duc (ins->parameters, fl);
    case pvm_meta_instruction_id_dup:
      return pvm_recognize_specialized_instruction_dup (ins->parameters, fl);
    case pvm_meta_instruction_id_endhl:
      return pvm_recognize_specialized_instruction_endhl (ins->parameters, fl);
    case pvm_meta_instruction_id_endsc:
      return pvm_recognize_specialized_instruction_endsc (ins->parameters, fl);
    case pvm_meta_instruction_id_eqi:
      return pvm_recognize_specialized_instruction_eqi (ins->parameters, fl);
    case pvm_meta_instruction_id_eqiu:
      return pvm_recognize_specialized_instruction_eqiu (ins->parameters, fl);
    case pvm_meta_instruction_id_eql:
      return pvm_recognize_specialized_instruction_eql (ins->parameters, fl);
    case pvm_meta_instruction_id_eqlu:
      return pvm_recognize_specialized_instruction_eqlu (ins->parameters, fl);
    case pvm_meta_instruction_id_eqs:
      return pvm_recognize_specialized_instruction_eqs (ins->parameters, fl);
    case pvm_meta_instruction_id_exit:
      return pvm_recognize_specialized_instruction_exit (ins->parameters, fl);
    case pvm_meta_instruction_id_exitvm:
      return pvm_recognize_specialized_instruction_exitvm (ins->parameters, fl);
    case pvm_meta_instruction_id_flush:
      return pvm_recognize_specialized_instruction_flush (ins->parameters, fl);
    case pvm_meta_instruction_id_fromr:
      return pvm_recognize_specialized_instruction_fromr (ins->parameters, fl);
    case pvm_meta_instruction_id_gei:
      return pvm_recognize_specialized_instruction_gei (ins->parameters, fl);
    case pvm_meta_instruction_id_geiu:
      return pvm_recognize_specialized_instruction_geiu (ins->parameters, fl);
    case pvm_meta_instruction_id_gel:
      return pvm_recognize_specialized_instruction_gel (ins->parameters, fl);
    case pvm_meta_instruction_id_gelu:
      return pvm_recognize_specialized_instruction_gelu (ins->parameters, fl);
    case pvm_meta_instruction_id_ges:
      return pvm_recognize_specialized_instruction_ges (ins->parameters, fl);
    case pvm_meta_instruction_id_getenv:
      return pvm_recognize_specialized_instruction_getenv (ins->parameters, fl);
    case pvm_meta_instruction_id_gti:
      return pvm_recognize_specialized_instruction_gti (ins->parameters, fl);
    case pvm_meta_instruction_id_gtiu:
      return pvm_recognize_specialized_instruction_gtiu (ins->parameters, fl);
    case pvm_meta_instruction_id_gtl:
      return pvm_recognize_specialized_instruction_gtl (ins->parameters, fl);
    case pvm_meta_instruction_id_gtlu:
      return pvm_recognize_specialized_instruction_gtlu (ins->parameters, fl);
    case pvm_meta_instruction_id_gts:
      return pvm_recognize_specialized_instruction_gts (ins->parameters, fl);
    case pvm_meta_instruction_id_indent:
      return pvm_recognize_specialized_instruction_indent (ins->parameters, fl);
    case pvm_meta_instruction_id_iogetb:
      return pvm_recognize_specialized_instruction_iogetb (ins->parameters, fl);
    case pvm_meta_instruction_id_iosetb:
      return pvm_recognize_specialized_instruction_iosetb (ins->parameters, fl);
    case pvm_meta_instruction_id_iosize:
      return pvm_recognize_specialized_instruction_iosize (ins->parameters, fl);
    case pvm_meta_instruction_id_isa:
      return pvm_recognize_specialized_instruction_isa (ins->parameters, fl);
    case pvm_meta_instruction_id_itoi:
      return pvm_recognize_specialized_instruction_itoi (ins->parameters, fl);
    case pvm_meta_instruction_id_itoiu:
      return pvm_recognize_specialized_instruction_itoiu (ins->parameters, fl);
    case pvm_meta_instruction_id_itol:
      return pvm_recognize_specialized_instruction_itol (ins->parameters, fl);
    case pvm_meta_instruction_id_itolu:
      return pvm_recognize_specialized_instruction_itolu (ins->parameters, fl);
    case pvm_meta_instruction_id_iutoi:
      return pvm_recognize_specialized_instruction_iutoi (ins->parameters, fl);
    case pvm_meta_instruction_id_iutoiu:
      return pvm_recognize_specialized_instruction_iutoiu (ins->parameters, fl);
    case pvm_meta_instruction_id_iutol:
      return pvm_recognize_specialized_instruction_iutol (ins->parameters, fl);
    case pvm_meta_instruction_id_iutolu:
      return pvm_recognize_specialized_instruction_iutolu (ins->parameters, fl);
    case pvm_meta_instruction_id_lei:
      return pvm_recognize_specialized_instruction_lei (ins->parameters, fl);
    case pvm_meta_instruction_id_leiu:
      return pvm_recognize_specialized_instruction_leiu (ins->parameters, fl);
    case pvm_meta_instruction_id_lel:
      return pvm_recognize_specialized_instruction_lel (ins->parameters, fl);
    case pvm_meta_instruction_id_lelu:
      return pvm_recognize_specialized_instruction_lelu (ins->parameters, fl);
    case pvm_meta_instruction_id_les:
      return pvm_recognize_specialized_instruction_les (ins->parameters, fl);
    case pvm_meta_instruction_id_lti:
      return pvm_recognize_specialized_instruction_lti (ins->parameters, fl);
    case pvm_meta_instruction_id_ltiu:
      return pvm_recognize_specialized_instruction_ltiu (ins->parameters, fl);
    case pvm_meta_instruction_id_ltl:
      return pvm_recognize_specialized_instruction_ltl (ins->parameters, fl);
    case pvm_meta_instruction_id_ltlu:
      return pvm_recognize_specialized_instruction_ltlu (ins->parameters, fl);
    case pvm_meta_instruction_id_ltoi:
      return pvm_recognize_specialized_instruction_ltoi (ins->parameters, fl);
    case pvm_meta_instruction_id_ltoiu:
      return pvm_recognize_specialized_instruction_ltoiu (ins->parameters, fl);
    case pvm_meta_instruction_id_ltol:
      return pvm_recognize_specialized_instruction_ltol (ins->parameters, fl);
    case pvm_meta_instruction_id_ltolu:
      return pvm_recognize_specialized_instruction_ltolu (ins->parameters, fl);
    case pvm_meta_instruction_id_lts:
      return pvm_recognize_specialized_instruction_lts (ins->parameters, fl);
    case pvm_meta_instruction_id_lutoi:
      return pvm_recognize_specialized_instruction_lutoi (ins->parameters, fl);
    case pvm_meta_instruction_id_lutoiu:
      return pvm_recognize_specialized_instruction_lutoiu (ins->parameters, fl);
    case pvm_meta_instruction_id_lutol:
      return pvm_recognize_specialized_instruction_lutol (ins->parameters, fl);
    case pvm_meta_instruction_id_lutolu:
      return pvm_recognize_specialized_instruction_lutolu (ins->parameters, fl);
    case pvm_meta_instruction_id_map:
      return pvm_recognize_specialized_instruction_map (ins->parameters, fl);
    case pvm_meta_instruction_id_mgetios:
      return pvm_recognize_specialized_instruction_mgetios (ins->parameters, fl);
    case pvm_meta_instruction_id_mgetm:
      return pvm_recognize_specialized_instruction_mgetm (ins->parameters, fl);
    case pvm_meta_instruction_id_mgeto:
      return pvm_recognize_specialized_instruction_mgeto (ins->parameters, fl);
    case pvm_meta_instruction_id_mgets:
      return pvm_recognize_specialized_instruction_mgets (ins->parameters, fl);
    case pvm_meta_instruction_id_mgetsel:
      return pvm_recognize_specialized_instruction_mgetsel (ins->parameters, fl);
    case pvm_meta_instruction_id_mgetsiz:
      return pvm_recognize_specialized_instruction_mgetsiz (ins->parameters, fl);
    case pvm_meta_instruction_id_mgetw:
      return pvm_recognize_specialized_instruction_mgetw (ins->parameters, fl);
    case pvm_meta_instruction_id_mka:
      return pvm_recognize_specialized_instruction_mka (ins->parameters, fl);
    case pvm_meta_instruction_id_mko:
      return pvm_recognize_specialized_instruction_mko (ins->parameters, fl);
    case pvm_meta_instruction_id_mksct:
      return pvm_recognize_specialized_instruction_mksct (ins->parameters, fl);
    case pvm_meta_instruction_id_mktya:
      return pvm_recognize_specialized_instruction_mktya (ins->parameters, fl);
    case pvm_meta_instruction_id_mktyany:
      return pvm_recognize_specialized_instruction_mktyany (ins->parameters, fl);
    case pvm_meta_instruction_id_mktyc:
      return pvm_recognize_specialized_instruction_mktyc (ins->parameters, fl);
    case pvm_meta_instruction_id_mktyi:
      return pvm_recognize_specialized_instruction_mktyi (ins->parameters, fl);
    case pvm_meta_instruction_id_mktyo:
      return pvm_recognize_specialized_instruction_mktyo (ins->parameters, fl);
    case pvm_meta_instruction_id_mktys:
      return pvm_recognize_specialized_instruction_mktys (ins->parameters, fl);
    case pvm_meta_instruction_id_mktysct:
      return pvm_recognize_specialized_instruction_mktysct (ins->parameters, fl);
    case pvm_meta_instruction_id_mktyv:
      return pvm_recognize_specialized_instruction_mktyv (ins->parameters, fl);
    case pvm_meta_instruction_id_mm:
      return pvm_recognize_specialized_instruction_mm (ins->parameters, fl);
    case pvm_meta_instruction_id_modi:
      return pvm_recognize_specialized_instruction_modi (ins->parameters, fl);
    case pvm_meta_instruction_id_modiu:
      return pvm_recognize_specialized_instruction_modiu (ins->parameters, fl);
    case pvm_meta_instruction_id_modl:
      return pvm_recognize_specialized_instruction_modl (ins->parameters, fl);
    case pvm_meta_instruction_id_modlu:
      return pvm_recognize_specialized_instruction_modlu (ins->parameters, fl);
    case pvm_meta_instruction_id_msetios:
      return pvm_recognize_specialized_instruction_msetios (ins->parameters, fl);
    case pvm_meta_instruction_id_msetm:
      return pvm_recognize_specialized_instruction_msetm (ins->parameters, fl);
    case pvm_meta_instruction_id_mseto:
      return pvm_recognize_specialized_instruction_mseto (ins->parameters, fl);
    case pvm_meta_instruction_id_msets:
      return pvm_recognize_specialized_instruction_msets (ins->parameters, fl);
    case pvm_meta_instruction_id_msetsel:
      return pvm_recognize_specialized_instruction_msetsel (ins->parameters, fl);
    case pvm_meta_instruction_id_msetsiz:
      return pvm_recognize_specialized_instruction_msetsiz (ins->parameters, fl);
    case pvm_meta_instruction_id_msetw:
      return pvm_recognize_specialized_instruction_msetw (ins->parameters, fl);
    case pvm_meta_instruction_id_muli:
      return pvm_recognize_specialized_instruction_muli (ins->parameters, fl);
    case pvm_meta_instruction_id_muliu:
      return pvm_recognize_specialized_instruction_muliu (ins->parameters, fl);
    case pvm_meta_instruction_id_mull:
      return pvm_recognize_specialized_instruction_mull (ins->parameters, fl);
    case pvm_meta_instruction_id_mullu:
      return pvm_recognize_specialized_instruction_mullu (ins->parameters, fl);
    case pvm_meta_instruction_id_muls:
      return pvm_recognize_specialized_instruction_muls (ins->parameters, fl);
    case pvm_meta_instruction_id_negi:
      return pvm_recognize_specialized_instruction_negi (ins->parameters, fl);
    case pvm_meta_instruction_id_negiu:
      return pvm_recognize_specialized_instruction_negiu (ins->parameters, fl);
    case pvm_meta_instruction_id_negl:
      return pvm_recognize_specialized_instruction_negl (ins->parameters, fl);
    case pvm_meta_instruction_id_neglu:
      return pvm_recognize_specialized_instruction_neglu (ins->parameters, fl);
    case pvm_meta_instruction_id_nei:
      return pvm_recognize_specialized_instruction_nei (ins->parameters, fl);
    case pvm_meta_instruction_id_neiu:
      return pvm_recognize_specialized_instruction_neiu (ins->parameters, fl);
    case pvm_meta_instruction_id_nel:
      return pvm_recognize_specialized_instruction_nel (ins->parameters, fl);
    case pvm_meta_instruction_id_nelu:
      return pvm_recognize_specialized_instruction_nelu (ins->parameters, fl);
    case pvm_meta_instruction_id_nes:
      return pvm_recognize_specialized_instruction_nes (ins->parameters, fl);
    case pvm_meta_instruction_id_nip:
      return pvm_recognize_specialized_instruction_nip (ins->parameters, fl);
    case pvm_meta_instruction_id_nip2:
      return pvm_recognize_specialized_instruction_nip2 (ins->parameters, fl);
    case pvm_meta_instruction_id_nip3:
      return pvm_recognize_specialized_instruction_nip3 (ins->parameters, fl);
    case pvm_meta_instruction_id_nn:
      return pvm_recognize_specialized_instruction_nn (ins->parameters, fl);
    case pvm_meta_instruction_id_nnn:
      return pvm_recognize_specialized_instruction_nnn (ins->parameters, fl);
    case pvm_meta_instruction_id_nop:
      return pvm_recognize_specialized_instruction_nop (ins->parameters, fl);
    case pvm_meta_instruction_id_not:
      return pvm_recognize_specialized_instruction_not (ins->parameters, fl);
    case pvm_meta_instruction_id_note:
      return pvm_recognize_specialized_instruction_note (ins->parameters, fl);
    case pvm_meta_instruction_id_nrot:
      return pvm_recognize_specialized_instruction_nrot (ins->parameters, fl);
    case pvm_meta_instruction_id_ogetbt:
      return pvm_recognize_specialized_instruction_ogetbt (ins->parameters, fl);
    case pvm_meta_instruction_id_ogetm:
      return pvm_recognize_specialized_instruction_ogetm (ins->parameters, fl);
    case pvm_meta_instruction_id_ogetu:
      return pvm_recognize_specialized_instruction_ogetu (ins->parameters, fl);
    case pvm_meta_instruction_id_open:
      return pvm_recognize_specialized_instruction_open (ins->parameters, fl);
    case pvm_meta_instruction_id_or:
      return pvm_recognize_specialized_instruction_or (ins->parameters, fl);
    case pvm_meta_instruction_id_osetm:
      return pvm_recognize_specialized_instruction_osetm (ins->parameters, fl);
    case pvm_meta_instruction_id_over:
      return pvm_recognize_specialized_instruction_over (ins->parameters, fl);
    case pvm_meta_instruction_id_pec:
      return pvm_recognize_specialized_instruction_pec (ins->parameters, fl);
    case pvm_meta_instruction_id_peekdi:
      return pvm_recognize_specialized_instruction_peekdi (ins->parameters, fl);
    case pvm_meta_instruction_id_peekdiu:
      return pvm_recognize_specialized_instruction_peekdiu (ins->parameters, fl);
    case pvm_meta_instruction_id_peekdl:
      return pvm_recognize_specialized_instruction_peekdl (ins->parameters, fl);
    case pvm_meta_instruction_id_peekdlu:
      return pvm_recognize_specialized_instruction_peekdlu (ins->parameters, fl);
    case pvm_meta_instruction_id_peeki:
      return pvm_recognize_specialized_instruction_peeki (ins->parameters, fl);
    case pvm_meta_instruction_id_peekiu:
      return pvm_recognize_specialized_instruction_peekiu (ins->parameters, fl);
    case pvm_meta_instruction_id_peekl:
      return pvm_recognize_specialized_instruction_peekl (ins->parameters, fl);
    case pvm_meta_instruction_id_peeklu:
      return pvm_recognize_specialized_instruction_peeklu (ins->parameters, fl);
    case pvm_meta_instruction_id_peeks:
      return pvm_recognize_specialized_instruction_peeks (ins->parameters, fl);
    case pvm_meta_instruction_id_pokedi:
      return pvm_recognize_specialized_instruction_pokedi (ins->parameters, fl);
    case pvm_meta_instruction_id_pokediu:
      return pvm_recognize_specialized_instruction_pokediu (ins->parameters, fl);
    case pvm_meta_instruction_id_pokedl:
      return pvm_recognize_specialized_instruction_pokedl (ins->parameters, fl);
    case pvm_meta_instruction_id_pokedlu:
      return pvm_recognize_specialized_instruction_pokedlu (ins->parameters, fl);
    case pvm_meta_instruction_id_pokei:
      return pvm_recognize_specialized_instruction_pokei (ins->parameters, fl);
    case pvm_meta_instruction_id_pokeiu:
      return pvm_recognize_specialized_instruction_pokeiu (ins->parameters, fl);
    case pvm_meta_instruction_id_pokel:
      return pvm_recognize_specialized_instruction_pokel (ins->parameters, fl);
    case pvm_meta_instruction_id_pokelu:
      return pvm_recognize_specialized_instruction_pokelu (ins->parameters, fl);
    case pvm_meta_instruction_id_pokes:
      return pvm_recognize_specialized_instruction_pokes (ins->parameters, fl);
    case pvm_meta_instruction_id_pope:
      return pvm_recognize_specialized_instruction_pope (ins->parameters, fl);
    case pvm_meta_instruction_id_popend:
      return pvm_recognize_specialized_instruction_popend (ins->parameters, fl);
    case pvm_meta_instruction_id_popf:
      return pvm_recognize_specialized_instruction_popf (ins->parameters, fl);
    case pvm_meta_instruction_id_popios:
      return pvm_recognize_specialized_instruction_popios (ins->parameters, fl);
    case pvm_meta_instruction_id_popoac:
      return pvm_recognize_specialized_instruction_popoac (ins->parameters, fl);
    case pvm_meta_instruction_id_popob:
      return pvm_recognize_specialized_instruction_popob (ins->parameters, fl);
    case pvm_meta_instruction_id_popobc:
      return pvm_recognize_specialized_instruction_popobc (ins->parameters, fl);
    case pvm_meta_instruction_id_popoc:
      return pvm_recognize_specialized_instruction_popoc (ins->parameters, fl);
    case pvm_meta_instruction_id_popod:
      return pvm_recognize_specialized_instruction_popod (ins->parameters, fl);
    case pvm_meta_instruction_id_popoi:
      return pvm_recognize_specialized_instruction_popoi (ins->parameters, fl);
    case pvm_meta_instruction_id_popom:
      return pvm_recognize_specialized_instruction_popom (ins->parameters, fl);
    case pvm_meta_instruction_id_popoo:
      return pvm_recognize_specialized_instruction_popoo (ins->parameters, fl);
    case pvm_meta_instruction_id_popopp:
      return pvm_recognize_specialized_instruction_popopp (ins->parameters, fl);
    case pvm_meta_instruction_id_popr:
      return pvm_recognize_specialized_instruction_popr (ins->parameters, fl);
    case pvm_meta_instruction_id_popvar:
      return pvm_recognize_specialized_instruction_popvar (ins->parameters, fl);
    case pvm_meta_instruction_id_powi:
      return pvm_recognize_specialized_instruction_powi (ins->parameters, fl);
    case pvm_meta_instruction_id_powiu:
      return pvm_recognize_specialized_instruction_powiu (ins->parameters, fl);
    case pvm_meta_instruction_id_powl:
      return pvm_recognize_specialized_instruction_powl (ins->parameters, fl);
    case pvm_meta_instruction_id_powlu:
      return pvm_recognize_specialized_instruction_powlu (ins->parameters, fl);
    case pvm_meta_instruction_id_printi:
      return pvm_recognize_specialized_instruction_printi (ins->parameters, fl);
    case pvm_meta_instruction_id_printiu:
      return pvm_recognize_specialized_instruction_printiu (ins->parameters, fl);
    case pvm_meta_instruction_id_printl:
      return pvm_recognize_specialized_instruction_printl (ins->parameters, fl);
    case pvm_meta_instruction_id_printlu:
      return pvm_recognize_specialized_instruction_printlu (ins->parameters, fl);
    case pvm_meta_instruction_id_prints:
      return pvm_recognize_specialized_instruction_prints (ins->parameters, fl);
    case pvm_meta_instruction_id_prolog:
      return pvm_recognize_specialized_instruction_prolog (ins->parameters, fl);
    case pvm_meta_instruction_id_push:
      return pvm_recognize_specialized_instruction_push (ins->parameters, fl);
    case pvm_meta_instruction_id_push32:
      return pvm_recognize_specialized_instruction_push32 (ins->parameters, fl);
    case pvm_meta_instruction_id_pushe:
      return pvm_recognize_specialized_instruction_pushe (ins->parameters, fl);
    case pvm_meta_instruction_id_pushend:
      return pvm_recognize_specialized_instruction_pushend (ins->parameters, fl);
    case pvm_meta_instruction_id_pushf:
      return pvm_recognize_specialized_instruction_pushf (ins->parameters, fl);
    case pvm_meta_instruction_id_pushhi:
      return pvm_recognize_specialized_instruction_pushhi (ins->parameters, fl);
    case pvm_meta_instruction_id_pushios:
      return pvm_recognize_specialized_instruction_pushios (ins->parameters, fl);
    case pvm_meta_instruction_id_pushlo:
      return pvm_recognize_specialized_instruction_pushlo (ins->parameters, fl);
    case pvm_meta_instruction_id_pushoac:
      return pvm_recognize_specialized_instruction_pushoac (ins->parameters, fl);
    case pvm_meta_instruction_id_pushob:
      return pvm_recognize_specialized_instruction_pushob (ins->parameters, fl);
    case pvm_meta_instruction_id_pushobc:
      return pvm_recognize_specialized_instruction_pushobc (ins->parameters, fl);
    case pvm_meta_instruction_id_pushoc:
      return pvm_recognize_specialized_instruction_pushoc (ins->parameters, fl);
    case pvm_meta_instruction_id_pushod:
      return pvm_recognize_specialized_instruction_pushod (ins->parameters, fl);
    case pvm_meta_instruction_id_pushoi:
      return pvm_recognize_specialized_instruction_pushoi (ins->parameters, fl);
    case pvm_meta_instruction_id_pushom:
      return pvm_recognize_specialized_instruction_pushom (ins->parameters, fl);
    case pvm_meta_instruction_id_pushoo:
      return pvm_recognize_specialized_instruction_pushoo (ins->parameters, fl);
    case pvm_meta_instruction_id_pushopp:
      return pvm_recognize_specialized_instruction_pushopp (ins->parameters, fl);
    case pvm_meta_instruction_id_pushr:
      return pvm_recognize_specialized_instruction_pushr (ins->parameters, fl);
    case pvm_meta_instruction_id_pushtopvar:
      return pvm_recognize_specialized_instruction_pushtopvar (ins->parameters, fl);
    case pvm_meta_instruction_id_pushvar:
      return pvm_recognize_specialized_instruction_pushvar (ins->parameters, fl);
    case pvm_meta_instruction_id_quake:
      return pvm_recognize_specialized_instruction_quake (ins->parameters, fl);
    case pvm_meta_instruction_id_raise:
      return pvm_recognize_specialized_instruction_raise (ins->parameters, fl);
    case pvm_meta_instruction_id_rand:
      return pvm_recognize_specialized_instruction_rand (ins->parameters, fl);
    case pvm_meta_instruction_id_regvar:
      return pvm_recognize_specialized_instruction_regvar (ins->parameters, fl);
    case pvm_meta_instruction_id_reloc:
      return pvm_recognize_specialized_instruction_reloc (ins->parameters, fl);
    case pvm_meta_instruction_id_restorer:
      return pvm_recognize_specialized_instruction_restorer (ins->parameters, fl);
    case pvm_meta_instruction_id_return:
      return pvm_recognize_specialized_instruction_return (ins->parameters, fl);
    case pvm_meta_instruction_id_revn:
      return pvm_recognize_specialized_instruction_revn (ins->parameters, fl);
    case pvm_meta_instruction_id_rot:
      return pvm_recognize_specialized_instruction_rot (ins->parameters, fl);
    case pvm_meta_instruction_id_saver:
      return pvm_recognize_specialized_instruction_saver (ins->parameters, fl);
    case pvm_meta_instruction_id_sconc:
      return pvm_recognize_specialized_instruction_sconc (ins->parameters, fl);
    case pvm_meta_instruction_id_sel:
      return pvm_recognize_specialized_instruction_sel (ins->parameters, fl);
    case pvm_meta_instruction_id_setr:
      return pvm_recognize_specialized_instruction_setr (ins->parameters, fl);
    case pvm_meta_instruction_id_siz:
      return pvm_recognize_specialized_instruction_siz (ins->parameters, fl);
    case pvm_meta_instruction_id_smodi:
      return pvm_recognize_specialized_instruction_smodi (ins->parameters, fl);
    case pvm_meta_instruction_id_sref:
      return pvm_recognize_specialized_instruction_sref (ins->parameters, fl);
    case pvm_meta_instruction_id_srefi:
      return pvm_recognize_specialized_instruction_srefi (ins->parameters, fl);
    case pvm_meta_instruction_id_srefia:
      return pvm_recognize_specialized_instruction_srefia (ins->parameters, fl);
    case pvm_meta_instruction_id_srefio:
      return pvm_recognize_specialized_instruction_srefio (ins->parameters, fl);
    case pvm_meta_instruction_id_srefmnt:
      return pvm_recognize_specialized_instruction_srefmnt (ins->parameters, fl);
    case pvm_meta_instruction_id_srefnt:
      return pvm_recognize_specialized_instruction_srefnt (ins->parameters, fl);
    case pvm_meta_instruction_id_srefo:
      return pvm_recognize_specialized_instruction_srefo (ins->parameters, fl);
    case pvm_meta_instruction_id_sset:
      return pvm_recognize_specialized_instruction_sset (ins->parameters, fl);
    case pvm_meta_instruction_id_strace:
      return pvm_recognize_specialized_instruction_strace (ins->parameters, fl);
    case pvm_meta_instruction_id_strref:
      return pvm_recognize_specialized_instruction_strref (ins->parameters, fl);
    case pvm_meta_instruction_id_subi:
      return pvm_recognize_specialized_instruction_subi (ins->parameters, fl);
    case pvm_meta_instruction_id_subiu:
      return pvm_recognize_specialized_instruction_subiu (ins->parameters, fl);
    case pvm_meta_instruction_id_subl:
      return pvm_recognize_specialized_instruction_subl (ins->parameters, fl);
    case pvm_meta_instruction_id_sublu:
      return pvm_recognize_specialized_instruction_sublu (ins->parameters, fl);
    case pvm_meta_instruction_id_substr:
      return pvm_recognize_specialized_instruction_substr (ins->parameters, fl);
    case pvm_meta_instruction_id_swap:
      return pvm_recognize_specialized_instruction_swap (ins->parameters, fl);
    case pvm_meta_instruction_id_swapgti:
      return pvm_recognize_specialized_instruction_swapgti (ins->parameters, fl);
    case pvm_meta_instruction_id_swapgtiu:
      return pvm_recognize_specialized_instruction_swapgtiu (ins->parameters, fl);
    case pvm_meta_instruction_id_swapgtl:
      return pvm_recognize_specialized_instruction_swapgtl (ins->parameters, fl);
    case pvm_meta_instruction_id_swapgtlu:
      return pvm_recognize_specialized_instruction_swapgtlu (ins->parameters, fl);
    case pvm_meta_instruction_id_sync:
      return pvm_recognize_specialized_instruction_sync (ins->parameters, fl);
    case pvm_meta_instruction_id_time:
      return pvm_recognize_specialized_instruction_time (ins->parameters, fl);
    case pvm_meta_instruction_id_tor:
      return pvm_recognize_specialized_instruction_tor (ins->parameters, fl);
    case pvm_meta_instruction_id_tuck:
      return pvm_recognize_specialized_instruction_tuck (ins->parameters, fl);
    case pvm_meta_instruction_id_tyagetb:
      return pvm_recognize_specialized_instruction_tyagetb (ins->parameters, fl);
    case pvm_meta_instruction_id_tyagett:
      return pvm_recognize_specialized_instruction_tyagett (ins->parameters, fl);
    case pvm_meta_instruction_id_tyisc:
      return pvm_recognize_specialized_instruction_tyisc (ins->parameters, fl);
    case pvm_meta_instruction_id_tyissct:
      return pvm_recognize_specialized_instruction_tyissct (ins->parameters, fl);
    case pvm_meta_instruction_id_typof:
      return pvm_recognize_specialized_instruction_typof (ins->parameters, fl);
    case pvm_meta_instruction_id_tysctn:
      return pvm_recognize_specialized_instruction_tysctn (ins->parameters, fl);
    case pvm_meta_instruction_id_unmap:
      return pvm_recognize_specialized_instruction_unmap (ins->parameters, fl);
    case pvm_meta_instruction_id_unreachable:
      return pvm_recognize_specialized_instruction_unreachable (ins->parameters, fl);
    case pvm_meta_instruction_id_ureloc:
      return pvm_recognize_specialized_instruction_ureloc (ins->parameters, fl);
    case pvm_meta_instruction_id_write:
      return pvm_recognize_specialized_instruction_write (ins->parameters, fl);
    default:
      jitter_fatal ("invalid meta-instruction id %i", (int)mi->id);
    }
  __builtin_unreachable ();
}

/* Specializer entry point: the only non-static function here. */
int
pvm_specialize_instruction (struct jitter_mutable_routine *p,
                                 const struct jitter_instruction *ins)
{
  enum pvm_specialized_instruction_opcode opcode
    = pvm_recognize_specialized_instruction (p, ins);
  if (opcode == pvm_specialized_instruction_opcode__eINVALID)
    jitter_fatal ("specialization failed: %s", ins->meta_instruction->name);

#ifdef JITTER_HAVE_PATCH_IN
  /* Replace the opcode with its non-defective counterpart. */
  opcode = pvm_defect_table [opcode];
#endif // #ifdef JITTER_HAVE_PATCH_IN

  jitter_add_specialized_instruction_opcode (p, opcode);


  /* FIXME: in the old shell-based generator I grouped specialized instructions by
     their "residual parameter map", yielding a switch with a lot of different
     specialized instructions mapping to the same case.  I should redo that here. */
  switch (opcode)
    {
    case pvm_specialized_instruction_opcode__eINVALID:
      break;

    case pvm_specialized_instruction_opcode__eBEGINBASICBLOCK:
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      break;

    case pvm_specialized_instruction_opcode__eEXITVM:
      break;

    case pvm_specialized_instruction_opcode__eDATALOCATIONS:
      break;

    case pvm_specialized_instruction_opcode__eNOP:
      break;

    case pvm_specialized_instruction_opcode__eUNREACHABLE0:
      break;

    case pvm_specialized_instruction_opcode__eUNREACHABLE1:
      break;

    case pvm_specialized_instruction_opcode__eUNREACHABLE2:
      break;

    case pvm_specialized_instruction_opcode_addi:
      break;

    case pvm_specialized_instruction_opcode_addiu:
      break;

    case pvm_specialized_instruction_opcode_addl:
      break;

    case pvm_specialized_instruction_opcode_addlu:
      break;

    case pvm_specialized_instruction_opcode_ains:
      break;

    case pvm_specialized_instruction_opcode_and:
      break;

    case pvm_specialized_instruction_opcode_aref:
      break;

    case pvm_specialized_instruction_opcode_arefo:
      break;

    case pvm_specialized_instruction_opcode_arem:
      break;

    case pvm_specialized_instruction_opcode_aset:
      break;

    case pvm_specialized_instruction_opcode_asettb:
      break;

    case pvm_specialized_instruction_opcode_atr:
      break;

    case pvm_specialized_instruction_opcode_ba__fR:
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      break;

    case pvm_specialized_instruction_opcode_bandi:
      break;

    case pvm_specialized_instruction_opcode_bandiu:
      break;

    case pvm_specialized_instruction_opcode_bandl:
      break;

    case pvm_specialized_instruction_opcode_bandlu:
      break;

    case pvm_specialized_instruction_opcode_beghl:
      break;

    case pvm_specialized_instruction_opcode_begsc:
      break;

    case pvm_specialized_instruction_opcode_bn__fR:
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      break;

    case pvm_specialized_instruction_opcode_bnn__fR:
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      break;

    case pvm_specialized_instruction_opcode_bnoti:
      break;

    case pvm_specialized_instruction_opcode_bnotiu:
      break;

    case pvm_specialized_instruction_opcode_bnotl:
      break;

    case pvm_specialized_instruction_opcode_bnotlu:
      break;

    case pvm_specialized_instruction_opcode_bnzi__fR:
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      break;

    case pvm_specialized_instruction_opcode_bnziu__fR:
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      break;

    case pvm_specialized_instruction_opcode_bnzl__fR:
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      break;

    case pvm_specialized_instruction_opcode_bnzlu__fR:
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      break;

    case pvm_specialized_instruction_opcode_bori:
      break;

    case pvm_specialized_instruction_opcode_boriu:
      break;

    case pvm_specialized_instruction_opcode_borl:
      break;

    case pvm_specialized_instruction_opcode_borlu:
      break;

    case pvm_specialized_instruction_opcode_bsli:
      break;

    case pvm_specialized_instruction_opcode_bsliu:
      break;

    case pvm_specialized_instruction_opcode_bsll:
      break;

    case pvm_specialized_instruction_opcode_bsllu:
      break;

    case pvm_specialized_instruction_opcode_bsri:
      break;

    case pvm_specialized_instruction_opcode_bsriu:
      break;

    case pvm_specialized_instruction_opcode_bsrl:
      break;

    case pvm_specialized_instruction_opcode_bsrlu:
      break;

    case pvm_specialized_instruction_opcode_bxori:
      break;

    case pvm_specialized_instruction_opcode_bxoriu:
      break;

    case pvm_specialized_instruction_opcode_bxorl:
      break;

    case pvm_specialized_instruction_opcode_bxorlu:
      break;

    case pvm_specialized_instruction_opcode_bzi__fR:
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      break;

    case pvm_specialized_instruction_opcode_bziu__fR:
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      break;

    case pvm_specialized_instruction_opcode_bzl__fR:
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      break;

    case pvm_specialized_instruction_opcode_bzlu__fR:
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      break;

    case pvm_specialized_instruction_opcode_call__retR:
      /* Caller instruction: make place for the return address,
         whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    case pvm_specialized_instruction_opcode_canary:
      break;

    case pvm_specialized_instruction_opcode_close:
      break;

    case pvm_specialized_instruction_opcode_ctos:
      break;

    case pvm_specialized_instruction_opcode_disas:
      break;

    case pvm_specialized_instruction_opcode_divi:
      break;

    case pvm_specialized_instruction_opcode_diviu:
      break;

    case pvm_specialized_instruction_opcode_divl:
      break;

    case pvm_specialized_instruction_opcode_divlu:
      break;

    case pvm_specialized_instruction_opcode_drop:
      break;

    case pvm_specialized_instruction_opcode_drop2:
      break;

    case pvm_specialized_instruction_opcode_drop3:
      break;

    case pvm_specialized_instruction_opcode_drop4:
      break;

    case pvm_specialized_instruction_opcode_duc:
      break;

    case pvm_specialized_instruction_opcode_dup:
      break;

    case pvm_specialized_instruction_opcode_endhl:
      break;

    case pvm_specialized_instruction_opcode_endsc:
      break;

    case pvm_specialized_instruction_opcode_eqi:
      break;

    case pvm_specialized_instruction_opcode_eqiu:
      break;

    case pvm_specialized_instruction_opcode_eql:
      break;

    case pvm_specialized_instruction_opcode_eqlu:
      break;

    case pvm_specialized_instruction_opcode_eqs:
      break;

    case pvm_specialized_instruction_opcode_exit:
      break;

    case pvm_specialized_instruction_opcode_exitvm:
      break;

    case pvm_specialized_instruction_opcode_flush:
      break;

    case pvm_specialized_instruction_opcode_fromr:
      break;

    case pvm_specialized_instruction_opcode_gei:
      break;

    case pvm_specialized_instruction_opcode_geiu:
      break;

    case pvm_specialized_instruction_opcode_gel:
      break;

    case pvm_specialized_instruction_opcode_gelu:
      break;

    case pvm_specialized_instruction_opcode_ges:
      break;

    case pvm_specialized_instruction_opcode_getenv:
      break;

    case pvm_specialized_instruction_opcode_gti:
      break;

    case pvm_specialized_instruction_opcode_gtiu:
      break;

    case pvm_specialized_instruction_opcode_gtl:
      break;

    case pvm_specialized_instruction_opcode_gtlu:
      break;

    case pvm_specialized_instruction_opcode_gts:
      break;

    case pvm_specialized_instruction_opcode_indent:
      break;

    case pvm_specialized_instruction_opcode_iogetb:
      break;

    case pvm_specialized_instruction_opcode_iosetb:
      break;

    case pvm_specialized_instruction_opcode_iosize:
      break;

    case pvm_specialized_instruction_opcode_isa:
      break;

    case pvm_specialized_instruction_opcode_itoi__nR:
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      break;

    case pvm_specialized_instruction_opcode_itoiu__nR:
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      break;

    case pvm_specialized_instruction_opcode_itol__nR:
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      break;

    case pvm_specialized_instruction_opcode_itolu__nR:
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      break;

    case pvm_specialized_instruction_opcode_iutoi__nR:
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      break;

    case pvm_specialized_instruction_opcode_iutoiu__nR:
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      break;

    case pvm_specialized_instruction_opcode_iutol__nR:
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      break;

    case pvm_specialized_instruction_opcode_iutolu__nR:
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      break;

    case pvm_specialized_instruction_opcode_lei:
      break;

    case pvm_specialized_instruction_opcode_leiu:
      break;

    case pvm_specialized_instruction_opcode_lel:
      break;

    case pvm_specialized_instruction_opcode_lelu:
      break;

    case pvm_specialized_instruction_opcode_les:
      break;

    case pvm_specialized_instruction_opcode_lti:
      break;

    case pvm_specialized_instruction_opcode_ltiu:
      break;

    case pvm_specialized_instruction_opcode_ltl:
      break;

    case pvm_specialized_instruction_opcode_ltlu:
      break;

    case pvm_specialized_instruction_opcode_ltoi__nR:
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      break;

    case pvm_specialized_instruction_opcode_ltoiu__nR:
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      break;

    case pvm_specialized_instruction_opcode_ltol__nR:
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      break;

    case pvm_specialized_instruction_opcode_ltolu__nR:
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      break;

    case pvm_specialized_instruction_opcode_lts:
      break;

    case pvm_specialized_instruction_opcode_lutoi__nR:
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      break;

    case pvm_specialized_instruction_opcode_lutoiu__nR:
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      break;

    case pvm_specialized_instruction_opcode_lutol__nR:
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      break;

    case pvm_specialized_instruction_opcode_lutolu__nR:
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      break;

    case pvm_specialized_instruction_opcode_map:
      break;

    case pvm_specialized_instruction_opcode_mgetios:
      break;

    case pvm_specialized_instruction_opcode_mgetm:
      break;

    case pvm_specialized_instruction_opcode_mgeto:
      break;

    case pvm_specialized_instruction_opcode_mgets:
      break;

    case pvm_specialized_instruction_opcode_mgetsel:
      break;

    case pvm_specialized_instruction_opcode_mgetsiz:
      break;

    case pvm_specialized_instruction_opcode_mgetw:
      break;

    case pvm_specialized_instruction_opcode_mka:
      break;

    case pvm_specialized_instruction_opcode_mko:
      break;

    case pvm_specialized_instruction_opcode_mksct:
      break;

    case pvm_specialized_instruction_opcode_mktya:
      break;

    case pvm_specialized_instruction_opcode_mktyany:
      break;

    case pvm_specialized_instruction_opcode_mktyc:
      break;

    case pvm_specialized_instruction_opcode_mktyi:
      break;

    case pvm_specialized_instruction_opcode_mktyo:
      break;

    case pvm_specialized_instruction_opcode_mktys:
      break;

    case pvm_specialized_instruction_opcode_mktysct:
      break;

    case pvm_specialized_instruction_opcode_mktyv:
      break;

    case pvm_specialized_instruction_opcode_mm:
      break;

    case pvm_specialized_instruction_opcode_modi:
      break;

    case pvm_specialized_instruction_opcode_modiu:
      break;

    case pvm_specialized_instruction_opcode_modl:
      break;

    case pvm_specialized_instruction_opcode_modlu:
      break;

    case pvm_specialized_instruction_opcode_msetios:
      break;

    case pvm_specialized_instruction_opcode_msetm:
      break;

    case pvm_specialized_instruction_opcode_mseto:
      break;

    case pvm_specialized_instruction_opcode_msets:
      break;

    case pvm_specialized_instruction_opcode_msetsel:
      break;

    case pvm_specialized_instruction_opcode_msetsiz:
      break;

    case pvm_specialized_instruction_opcode_msetw:
      break;

    case pvm_specialized_instruction_opcode_muli:
      break;

    case pvm_specialized_instruction_opcode_muliu:
      break;

    case pvm_specialized_instruction_opcode_mull:
      break;

    case pvm_specialized_instruction_opcode_mullu:
      break;

    case pvm_specialized_instruction_opcode_muls:
      break;

    case pvm_specialized_instruction_opcode_negi:
      break;

    case pvm_specialized_instruction_opcode_negiu:
      break;

    case pvm_specialized_instruction_opcode_negl:
      break;

    case pvm_specialized_instruction_opcode_neglu:
      break;

    case pvm_specialized_instruction_opcode_nei:
      break;

    case pvm_specialized_instruction_opcode_neiu:
      break;

    case pvm_specialized_instruction_opcode_nel:
      break;

    case pvm_specialized_instruction_opcode_nelu:
      break;

    case pvm_specialized_instruction_opcode_nes:
      break;

    case pvm_specialized_instruction_opcode_nip:
      break;

    case pvm_specialized_instruction_opcode_nip2:
      break;

    case pvm_specialized_instruction_opcode_nip3:
      break;

    case pvm_specialized_instruction_opcode_nn:
      break;

    case pvm_specialized_instruction_opcode_nnn:
      break;

    case pvm_specialized_instruction_opcode_nop:
      break;

    case pvm_specialized_instruction_opcode_not:
      break;

    case pvm_specialized_instruction_opcode_note__nR:
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      break;

    case pvm_specialized_instruction_opcode_nrot:
      break;

    case pvm_specialized_instruction_opcode_ogetbt:
      break;

    case pvm_specialized_instruction_opcode_ogetm:
      break;

    case pvm_specialized_instruction_opcode_ogetu:
      break;

    case pvm_specialized_instruction_opcode_open:
      break;

    case pvm_specialized_instruction_opcode_or:
      break;

    case pvm_specialized_instruction_opcode_osetm:
      break;

    case pvm_specialized_instruction_opcode_over:
      break;

    case pvm_specialized_instruction_opcode_pec:
      break;

    case pvm_specialized_instruction_opcode_peekdi__nR:
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      break;

    case pvm_specialized_instruction_opcode_peekdiu__nR:
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      break;

    case pvm_specialized_instruction_opcode_peekdl__nR:
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      break;

    case pvm_specialized_instruction_opcode_peekdlu__nR:
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      break;

    case pvm_specialized_instruction_opcode_peeki__nR__nR__nR:
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      jitter_add_specialized_instruction_literal (p, ins->parameters[1]->literal.ufixnum);
      jitter_add_specialized_instruction_literal (p, ins->parameters[2]->literal.ufixnum);
      break;

    case pvm_specialized_instruction_opcode_peekiu__nR__nR:
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      jitter_add_specialized_instruction_literal (p, ins->parameters[1]->literal.ufixnum);
      break;

    case pvm_specialized_instruction_opcode_peekl__nR__nR__nR:
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      jitter_add_specialized_instruction_literal (p, ins->parameters[1]->literal.ufixnum);
      jitter_add_specialized_instruction_literal (p, ins->parameters[2]->literal.ufixnum);
      break;

    case pvm_specialized_instruction_opcode_peeklu__nR__nR:
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      jitter_add_specialized_instruction_literal (p, ins->parameters[1]->literal.ufixnum);
      break;

    case pvm_specialized_instruction_opcode_peeks:
      break;

    case pvm_specialized_instruction_opcode_pokedi__nR:
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      break;

    case pvm_specialized_instruction_opcode_pokediu__nR:
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      break;

    case pvm_specialized_instruction_opcode_pokedl__nR:
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      break;

    case pvm_specialized_instruction_opcode_pokedlu__nR:
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      break;

    case pvm_specialized_instruction_opcode_pokei__nR__nR__nR:
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      jitter_add_specialized_instruction_literal (p, ins->parameters[1]->literal.ufixnum);
      jitter_add_specialized_instruction_literal (p, ins->parameters[2]->literal.ufixnum);
      break;

    case pvm_specialized_instruction_opcode_pokeiu__nR__nR:
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      jitter_add_specialized_instruction_literal (p, ins->parameters[1]->literal.ufixnum);
      break;

    case pvm_specialized_instruction_opcode_pokel__nR__nR__nR:
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      jitter_add_specialized_instruction_literal (p, ins->parameters[1]->literal.ufixnum);
      jitter_add_specialized_instruction_literal (p, ins->parameters[2]->literal.ufixnum);
      break;

    case pvm_specialized_instruction_opcode_pokelu__nR__nR:
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      jitter_add_specialized_instruction_literal (p, ins->parameters[1]->literal.ufixnum);
      break;

    case pvm_specialized_instruction_opcode_pokes:
      break;

    case pvm_specialized_instruction_opcode_pope:
      break;

    case pvm_specialized_instruction_opcode_popend:
      break;

    case pvm_specialized_instruction_opcode_popf__nR:
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      break;

    case pvm_specialized_instruction_opcode_popios:
      break;

    case pvm_specialized_instruction_opcode_popoac:
      break;

    case pvm_specialized_instruction_opcode_popob:
      break;

    case pvm_specialized_instruction_opcode_popobc:
      break;

    case pvm_specialized_instruction_opcode_popoc:
      break;

    case pvm_specialized_instruction_opcode_popod:
      break;

    case pvm_specialized_instruction_opcode_popoi:
      break;

    case pvm_specialized_instruction_opcode_popom:
      break;

    case pvm_specialized_instruction_opcode_popoo:
      break;

    case pvm_specialized_instruction_opcode_popopp:
      break;

    case pvm_specialized_instruction_opcode_popr___rrR:
      /* A slow register is passed as a residual literal offset. */      jitter_add_specialized_instruction_literal (p, PVM_SLOW_REGISTER_OFFSET(r, ins->parameters[0]->register_index));
      break;

    case pvm_specialized_instruction_opcode_popvar__nR__nR:
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      jitter_add_specialized_instruction_literal (p, ins->parameters[1]->literal.ufixnum);
      break;

    case pvm_specialized_instruction_opcode_powi:
      break;

    case pvm_specialized_instruction_opcode_powiu:
      break;

    case pvm_specialized_instruction_opcode_powl:
      break;

    case pvm_specialized_instruction_opcode_powlu:
      break;

    case pvm_specialized_instruction_opcode_printi__nR:
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      break;

    case pvm_specialized_instruction_opcode_printiu__nR:
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      break;

    case pvm_specialized_instruction_opcode_printl__nR:
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      break;

    case pvm_specialized_instruction_opcode_printlu__nR:
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      break;

    case pvm_specialized_instruction_opcode_prints:
      break;

    case pvm_specialized_instruction_opcode_prolog:
      break;

    case pvm_specialized_instruction_opcode_push__nR:
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      break;

    case pvm_specialized_instruction_opcode_push__lR:
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      break;

    case pvm_specialized_instruction_opcode_push32__nR:
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      break;

    case pvm_specialized_instruction_opcode_push32__lR:
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      break;

    case pvm_specialized_instruction_opcode_pushe__lR:
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      break;

    case pvm_specialized_instruction_opcode_pushend:
      break;

    case pvm_specialized_instruction_opcode_pushf__nR:
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      break;

    case pvm_specialized_instruction_opcode_pushhi__nR:
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      break;

    case pvm_specialized_instruction_opcode_pushhi__lR:
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      break;

    case pvm_specialized_instruction_opcode_pushios:
      break;

    case pvm_specialized_instruction_opcode_pushlo__nR:
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      break;

    case pvm_specialized_instruction_opcode_pushlo__lR:
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      break;

    case pvm_specialized_instruction_opcode_pushoac:
      break;

    case pvm_specialized_instruction_opcode_pushob:
      break;

    case pvm_specialized_instruction_opcode_pushobc:
      break;

    case pvm_specialized_instruction_opcode_pushoc:
      break;

    case pvm_specialized_instruction_opcode_pushod:
      break;

    case pvm_specialized_instruction_opcode_pushoi:
      break;

    case pvm_specialized_instruction_opcode_pushom:
      break;

    case pvm_specialized_instruction_opcode_pushoo:
      break;

    case pvm_specialized_instruction_opcode_pushopp:
      break;

    case pvm_specialized_instruction_opcode_pushr___rrR:
      /* A slow register is passed as a residual literal offset. */      jitter_add_specialized_instruction_literal (p, PVM_SLOW_REGISTER_OFFSET(r, ins->parameters[0]->register_index));
      break;

    case pvm_specialized_instruction_opcode_pushtopvar__nR:
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      break;

    case pvm_specialized_instruction_opcode_pushvar__n0__n0:
      break;

    case pvm_specialized_instruction_opcode_pushvar__n0__n1:
      break;

    case pvm_specialized_instruction_opcode_pushvar__n0__n2:
      break;

    case pvm_specialized_instruction_opcode_pushvar__n0__n3:
      break;

    case pvm_specialized_instruction_opcode_pushvar__n0__n4:
      break;

    case pvm_specialized_instruction_opcode_pushvar__n0__n5:
      break;

    case pvm_specialized_instruction_opcode_pushvar__n0__nR:
      jitter_add_specialized_instruction_literal (p, ins->parameters[1]->literal.ufixnum);
      break;

    case pvm_specialized_instruction_opcode_pushvar__nR__n0:
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      break;

    case pvm_specialized_instruction_opcode_pushvar__nR__n1:
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      break;

    case pvm_specialized_instruction_opcode_pushvar__nR__n2:
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      break;

    case pvm_specialized_instruction_opcode_pushvar__nR__n3:
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      break;

    case pvm_specialized_instruction_opcode_pushvar__nR__n4:
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      break;

    case pvm_specialized_instruction_opcode_pushvar__nR__n5:
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      break;

    case pvm_specialized_instruction_opcode_pushvar__nR__nR:
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      jitter_add_specialized_instruction_literal (p, ins->parameters[1]->literal.ufixnum);
      break;

    case pvm_specialized_instruction_opcode_quake:
      break;

    case pvm_specialized_instruction_opcode_raise:
      break;

    case pvm_specialized_instruction_opcode_rand:
      break;

    case pvm_specialized_instruction_opcode_regvar:
      break;

    case pvm_specialized_instruction_opcode_reloc:
      break;

    case pvm_specialized_instruction_opcode_restorer___rrR:
      /* A slow register is passed as a residual literal offset. */      jitter_add_specialized_instruction_literal (p, PVM_SLOW_REGISTER_OFFSET(r, ins->parameters[0]->register_index));
      break;

    case pvm_specialized_instruction_opcode_return:
      break;

    case pvm_specialized_instruction_opcode_revn__n3:
      break;

    case pvm_specialized_instruction_opcode_revn__n4:
      break;

    case pvm_specialized_instruction_opcode_revn__nR:
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      break;

    case pvm_specialized_instruction_opcode_rot:
      break;

    case pvm_specialized_instruction_opcode_saver___rrR:
      /* A slow register is passed as a residual literal offset. */      jitter_add_specialized_instruction_literal (p, PVM_SLOW_REGISTER_OFFSET(r, ins->parameters[0]->register_index));
      break;

    case pvm_specialized_instruction_opcode_sconc:
      break;

    case pvm_specialized_instruction_opcode_sel:
      break;

    case pvm_specialized_instruction_opcode_setr___rrR:
      /* A slow register is passed as a residual literal offset. */      jitter_add_specialized_instruction_literal (p, PVM_SLOW_REGISTER_OFFSET(r, ins->parameters[0]->register_index));
      break;

    case pvm_specialized_instruction_opcode_siz:
      break;

    case pvm_specialized_instruction_opcode_smodi:
      break;

    case pvm_specialized_instruction_opcode_sref:
      break;

    case pvm_specialized_instruction_opcode_srefi:
      break;

    case pvm_specialized_instruction_opcode_srefia:
      break;

    case pvm_specialized_instruction_opcode_srefio:
      break;

    case pvm_specialized_instruction_opcode_srefmnt:
      break;

    case pvm_specialized_instruction_opcode_srefnt:
      break;

    case pvm_specialized_instruction_opcode_srefo:
      break;

    case pvm_specialized_instruction_opcode_sset:
      break;

    case pvm_specialized_instruction_opcode_strace__nR:
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      break;

    case pvm_specialized_instruction_opcode_strref:
      break;

    case pvm_specialized_instruction_opcode_subi:
      break;

    case pvm_specialized_instruction_opcode_subiu:
      break;

    case pvm_specialized_instruction_opcode_subl:
      break;

    case pvm_specialized_instruction_opcode_sublu:
      break;

    case pvm_specialized_instruction_opcode_substr:
      break;

    case pvm_specialized_instruction_opcode_swap:
      break;

    case pvm_specialized_instruction_opcode_swapgti:
      break;

    case pvm_specialized_instruction_opcode_swapgtiu:
      break;

    case pvm_specialized_instruction_opcode_swapgtl:
      break;

    case pvm_specialized_instruction_opcode_swapgtlu:
      break;

    case pvm_specialized_instruction_opcode_sync:
      break;

    case pvm_specialized_instruction_opcode_time:
      break;

    case pvm_specialized_instruction_opcode_tor:
      break;

    case pvm_specialized_instruction_opcode_tuck:
      break;

    case pvm_specialized_instruction_opcode_tyagetb:
      break;

    case pvm_specialized_instruction_opcode_tyagett:
      break;

    case pvm_specialized_instruction_opcode_tyisc:
      break;

    case pvm_specialized_instruction_opcode_tyissct:
      break;

    case pvm_specialized_instruction_opcode_typof:
      break;

    case pvm_specialized_instruction_opcode_tysctn:
      break;

    case pvm_specialized_instruction_opcode_unmap:
      break;

    case pvm_specialized_instruction_opcode_unreachable:
      break;

    case pvm_specialized_instruction_opcode_ureloc:
      break;

    case pvm_specialized_instruction_opcode_write__retR:
      /* Caller instruction: make place for the return address,
         whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    case pvm_specialized_instruction_opcode__Aba__fR_A_mno_mfast_mbranches:
      /* Non-relocatable instruction: make place for the return label,
         whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    case pvm_specialized_instruction_opcode__Abn__fR_A_mno_mfast_mbranches:
      /* Non-relocatable instruction: make place for the return label,
         whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    case pvm_specialized_instruction_opcode__Abnn__fR_A_mno_mfast_mbranches:
      /* Non-relocatable instruction: make place for the return label,
         whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    case pvm_specialized_instruction_opcode__Abnzi__fR_A_mno_mfast_mbranches:
      /* Non-relocatable instruction: make place for the return label,
         whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    case pvm_specialized_instruction_opcode__Abnziu__fR_A_mno_mfast_mbranches:
      /* Non-relocatable instruction: make place for the return label,
         whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    case pvm_specialized_instruction_opcode__Abnzl__fR_A_mno_mfast_mbranches:
      /* Non-relocatable instruction: make place for the return label,
         whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    case pvm_specialized_instruction_opcode__Abnzlu__fR_A_mno_mfast_mbranches:
      /* Non-relocatable instruction: make place for the return label,
         whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    case pvm_specialized_instruction_opcode__Abzi__fR_A_mno_mfast_mbranches:
      /* Non-relocatable instruction: make place for the return label,
         whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    case pvm_specialized_instruction_opcode__Abziu__fR_A_mno_mfast_mbranches:
      /* Non-relocatable instruction: make place for the return label,
         whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    case pvm_specialized_instruction_opcode__Abzl__fR_A_mno_mfast_mbranches:
      /* Non-relocatable instruction: make place for the return label,
         whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    case pvm_specialized_instruction_opcode__Abzlu__fR_A_mno_mfast_mbranches:
      /* Non-relocatable instruction: make place for the return label,
         whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    default:
      jitter_fatal ("invalid specialized instruction opcode %i", (int)opcode);
    }
  return 1; // FIXME: I should rethink this return value.
}

void
pvm_state_initialize (struct pvm_state *jitter_state)
{
  struct pvm_state_backing * const jitter_state_backing
    __attribute__ ((unused))
    = & jitter_state->pvm_state_backing;
  struct pvm_state_runtime * const jitter_state_runtime
    __attribute__ ((unused))
    = & jitter_state->pvm_state_runtime;

  /* Initialize the Array. */
  jitter_state_backing->jitter_slow_register_no_per_class = 0; // FIXME: raise?
  jitter_state_backing->jitter_array
    = jitter_xmalloc (PVM_ARRAY_SIZE(jitter_state_backing
                         ->jitter_slow_register_no_per_class));

  /* Initialize special-purpose data. */
  pvm_initialize_special_purpose_data (PVM_ARRAY_TO_SPECIAL_PURPOSE_STATE_DATA (jitter_state_backing->jitter_array));

  /* Initialize stack backings and stack runtime data structures, pointing
     to memory from the backings. */
  jitter_stack_initialize_tos_backing(& jitter_state_backing->jitter_stack_stack_backing,
                                      sizeof (pvm_val),
                                      65536,
                                      NULL,
                                      1,
                                      1);
  JITTER_STACK_TOS_INITIALIZE(pvm_val, jitter_state_runtime-> ,
                              stack, jitter_state_backing->jitter_stack_stack_backing);
  jitter_stack_initialize_ntos_backing(& jitter_state_backing->jitter_stack_returnstack_backing,
                                      sizeof (pvm_val),
                                      65536,
                                      NULL,
                                      1,
                                      1);
  JITTER_STACK_NTOS_INITIALIZE(pvm_val, jitter_state_runtime-> ,
                              returnstack, jitter_state_backing->jitter_stack_returnstack_backing);
  jitter_stack_initialize_ntos_backing(& jitter_state_backing->jitter_stack_exceptionstack_backing,
                                      sizeof (struct pvm_exception_handler),
                                      65536,
                                      NULL,
                                      1,
                                      1);
  JITTER_STACK_NTOS_INITIALIZE(struct pvm_exception_handler, jitter_state_runtime-> ,
                              exceptionstack, jitter_state_backing->jitter_stack_exceptionstack_backing);

  /* Initialise the link register, if present. */
#if    defined(JITTER_DISPATCH_SWITCH)                   \
    || defined(JITTER_DISPATCH_DIRECT_THREADING)         \
    || defined(JITTER_DISPATCH_MINIMAL_THREADING)        \
    || (   defined(JITTER_DISPATCH_NO_THREADING)         \
        && ! defined(JITTER_MACHINE_SUPPORTS_PROCEDURE))
  jitter_state_runtime->_jitter_link = NULL;
#endif

  /* No need to initialise r-class fast registers. */


  /* User code for state initialization. */
#line 845 "../../libpoke/pvm.jitter"
#line 845 "../../libpoke/pvm.jitter"

      jitter_state_backing->vm = NULL;
      jitter_state_backing->canary = NULL;
      jitter_state_backing->exit_code = PVM_EXIT_OK;
      jitter_state_backing->result_value = PVM_NULL;
      jitter_state_runtime->endian = IOS_ENDIAN_MSB;
      jitter_state_runtime->nenc = IOS_NENC_2;
      jitter_state_runtime->pretty_print = 0;
      jitter_state_runtime->omode = PVM_PRINT_FLAT;
      jitter_state_runtime->obase = 10;
      jitter_state_runtime->omaps = 0;
      jitter_state_runtime->odepth = 0;
      jitter_state_runtime->oindent = 2;
      jitter_state_runtime->oacutoff = 0;
  
  /* End of the user code for state initialization. */

  /* Link this new state to the list of states. */
  JITTER_LIST_LINK_LAST (pvm_state, links, & pvm_vm->states, jitter_state);

}

void
pvm_state_finalize (struct pvm_state *jitter_state)
{
  /* Unlink this new state from the list of states. */
  JITTER_LIST_UNLINK (pvm_state, links, & pvm_vm->states, jitter_state);

  struct pvm_state_backing * const jitter_state_backing
    __attribute__ ((unused))
    = & jitter_state->pvm_state_backing;
  struct pvm_state_runtime * const jitter_state_runtime
    __attribute__ ((unused))
    = & jitter_state->pvm_state_runtime;

  /* Finalize special-purpose data. */
  pvm_finalize_special_purpose_data (PVM_ARRAY_TO_SPECIAL_PURPOSE_STATE_DATA (jitter_state_backing->jitter_array));

  /* Finalize stack backings -- There is no need to finalize the stack
     runtime data structures, as they hold no heap data of their own. */
  jitter_stack_finalize_backing (& jitter_state_backing->jitter_stack_stack_backing);
  jitter_stack_finalize_backing (& jitter_state_backing->jitter_stack_returnstack_backing);
  jitter_stack_finalize_backing (& jitter_state_backing->jitter_stack_exceptionstack_backing);


  /* User code for state finalization. */
#line 863 "../../libpoke/pvm.jitter"
#line 863 "../../libpoke/pvm.jitter"

   /* Finalize extra state here.  */
  
  /* End of the user code for state finalization. */

  /* Finalize the Array. */
  free ((void *) jitter_state_backing->jitter_array);

}

