/* A Bison parser, made by GNU Bison 3.6.2.  */

/* Bison interface for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015, 2018-2020 Free Software Foundation,
   Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* DO NOT RELY ON FEATURES THAT ARE NOT DOCUMENTED in the manual,
   especially those whose name start with YY_ or yy_.  They are
   private implementation details that can be changed or removed.  */

#ifndef YY_PKL_TAB_PKL_TAB_TAB_H_INCLUDED
# define YY_PKL_TAB_PKL_TAB_TAB_H_INCLUDED
/* Debug traces.  */
#ifndef PKL_TAB_DEBUG
# if defined YYDEBUG
#if YYDEBUG
#   define PKL_TAB_DEBUG 1
#  else
#   define PKL_TAB_DEBUG 0
#  endif
# else /* ! defined YYDEBUG */
#  define PKL_TAB_DEBUG 1
# endif /* ! defined YYDEBUG */
#endif  /* ! defined PKL_TAB_DEBUG */
#if PKL_TAB_DEBUG
extern int pkl_tab_debug;
#endif

/* Token kinds.  */
#ifndef PKL_TAB_TOKENTYPE
# define PKL_TAB_TOKENTYPE
  enum pkl_tab_tokentype
  {
    PKL_TAB_EMPTY = -2,
    PKL_TAB_EOF = 0,               /* "end of file"  */
    PKL_TAB_error = 256,           /* error  */
    PKL_TAB_UNDEF = 257,           /* "invalid token"  */
    INTEGER = 258,                 /* "integer literal"  */
    INTEGER_OVERFLOW = 259,        /* INTEGER_OVERFLOW  */
    CHAR = 260,                    /* "character literal"  */
    STR = 261,                     /* "string"  */
    IDENTIFIER = 262,              /* "identifier"  */
    TYPENAME = 263,                /* "type name"  */
    UNIT = 264,                    /* "offset unit"  */
    ENUM = 265,                    /* "keyword `enum'"  */
    PINNED = 266,                  /* "keyword `pinned'"  */
    STRUCT = 267,                  /* "keyword `struct'"  */
    token = 268,                   /* token  */
    UNION = 269,                   /* "keyword `union'"  */
    CONST = 270,                   /* "keyword `const'"  */
    CONTINUE = 271,                /* "keyword `continue'"  */
    ELSE = 272,                    /* "keyword `else'"  */
    IF = 273,                      /* "keyword `if'"  */
    WHILE = 274,                   /* "keyword `while"  */
    UNTIL = 275,                   /* "keyword `until'"  */
    FOR = 276,                     /* "keyword `for'"  */
    IN = 277,                      /* "keyword `in'"  */
    WHERE = 278,                   /* "keyword `where'"  */
    SIZEOF = 279,                  /* "keyword `sizeof'"  */
    ASSERT = 280,                  /* "keyword `assert'"  */
    ERR = 281,                     /* "token"  */
    ALIEN = 282,                   /* ALIEN  */
    INTCONSTR = 283,               /* "int type constructor"  */
    UINTCONSTR = 284,              /* "uint type constructor"  */
    OFFSETCONSTR = 285,            /* "offset type constructor"  */
    DEFUN = 286,                   /* "keyword `fun'"  */
    DEFSET = 287,                  /* "keyword `defset'"  */
    DEFTYPE = 288,                 /* "keyword `type'"  */
    DEFVAR = 289,                  /* "keyword `var'"  */
    DEFUNIT = 290,                 /* "keyword `unit'"  */
    METHOD = 291,                  /* "keyword `method'"  */
    RETURN = 292,                  /* "keyword `return'"  */
    BREAK = 293,                   /* "keyword `break'"  */
    STRING = 294,                  /* "string type specifier"  */
    TRY = 295,                     /* "keyword `try'"  */
    CATCH = 296,                   /* "keyword `catch'"  */
    RAISE = 297,                   /* "keyword `raise'"  */
    VOID = 298,                    /* "void type specifier"  */
    ANY = 299,                     /* "any type specifier"  */
    PRINT = 300,                   /* "keyword `print'"  */
    PRINTF = 301,                  /* "keyword `printf'"  */
    LOAD = 302,                    /* "keyword `load'"  */
    LAMBDA = 303,                  /* "keyword `lambda'"  */
    BUILTIN_RAND = 304,            /* BUILTIN_RAND  */
    BUILTIN_GET_ENDIAN = 305,      /* BUILTIN_GET_ENDIAN  */
    BUILTIN_SET_ENDIAN = 306,      /* BUILTIN_SET_ENDIAN  */
    BUILTIN_GET_IOS = 307,         /* BUILTIN_GET_IOS  */
    BUILTIN_SET_IOS = 308,         /* BUILTIN_SET_IOS  */
    BUILTIN_OPEN = 309,            /* BUILTIN_OPEN  */
    BUILTIN_CLOSE = 310,           /* BUILTIN_CLOSE  */
    BUILTIN_IOSIZE = 311,          /* BUILTIN_IOSIZE  */
    BUILTIN_GETENV = 312,          /* BUILTIN_GETENV  */
    BUILTIN_FORGET = 313,          /* BUILTIN_FORGET  */
    BUILTIN_GET_TIME = 314,        /* BUILTIN_GET_TIME  */
    BUILTIN_STRACE = 315,          /* BUILTIN_STRACE  */
    BUILTIN_TERM_RGB_TO_COLOR = 316, /* BUILTIN_TERM_RGB_TO_COLOR  */
    BUILTIN_TERM_GET_COLOR = 317,  /* BUILTIN_TERM_GET_COLOR  */
    BUILTIN_TERM_SET_COLOR = 318,  /* BUILTIN_TERM_SET_COLOR  */
    BUILTIN_TERM_GET_BGCOLOR = 319, /* BUILTIN_TERM_GET_BGCOLOR  */
    BUILTIN_TERM_SET_BGCOLOR = 320, /* BUILTIN_TERM_SET_BGCOLOR  */
    BUILTIN_TERM_BEGIN_CLASS = 321, /* BUILTIN_TERM_BEGIN_CLASS  */
    BUILTIN_TERM_END_CLASS = 322,  /* BUILTIN_TERM_END_CLASS  */
    BUILTIN_TERM_BEGIN_HYPERLINK = 323, /* BUILTIN_TERM_BEGIN_HYPERLINK  */
    BUILTIN_TERM_END_HYPERLINK = 324, /* BUILTIN_TERM_END_HYPERLINK  */
    POWA = 325,                    /* "power-and-assign operator"  */
    MULA = 326,                    /* "multiply-and-assign operator"  */
    DIVA = 327,                    /* "divide-and-assing operator"  */
    MODA = 328,                    /* "modulus-and-assign operator"  */
    ADDA = 329,                    /* "add-and-assing operator"  */
    SUBA = 330,                    /* "subtract-and-assign operator"  */
    SLA = 331,                     /* "shift-left-and-assign operator"  */
    SRA = 332,                     /* "shift-right-and-assign operator"  */
    BANDA = 333,                   /* "bit-and-and-assign operator"  */
    XORA = 334,                    /* "bit-xor-and-assign operator"  */
    IORA = 335,                    /* "bit-or-and-assign operator"  */
    RANGEA = 336,                  /* "range separator"  */
    OR = 337,                      /* "logical or operator"  */
    AND = 338,                     /* "logical and operator"  */
    EQ = 339,                      /* "equality operator"  */
    NE = 340,                      /* "inequality operator"  */
    LE = 341,                      /* "less-or-equal operator"  */
    GE = 342,                      /* "bigger-or-equal-than operator"  */
    SL = 343,                      /* "left shift operator"  */
    SR = 344,                      /* "right shift operator"  */
    CEILDIV = 345,                 /* "ceiling division operator"  */
    POW = 346,                     /* "power operator"  */
    BCONC = 347,                   /* "bit-concatenation operator"  */
    NSMAP = 348,                   /* "non-strict map operator"  */
    INC = 349,                     /* "increment operator"  */
    DEC = 350,                     /* "decrement operator"  */
    AS = 351,                      /* "cast operator"  */
    ISA = 352,                     /* "type identification operator"  */
    ATTR = 353,                    /* "attribute"  */
    UNMAP = 354,                   /* "unmap operator"  */
    BIG = 355,                     /* "keyword `big'"  */
    LITTLE = 356,                  /* "keyword `little'"  */
    SIGNED = 357,                  /* "keyword `signed'"  */
    UNSIGNED = 358,                /* "keyword `unsigned'"  */
    THREEDOTS = 359,               /* "varargs indicator"  */
    THEN = 360,                    /* THEN  */
    UNARY = 361,                   /* UNARY  */
    HYPERUNARY = 362,              /* HYPERUNARY  */
    START_EXP = 363,               /* START_EXP  */
    START_DECL = 364,              /* START_DECL  */
    START_STMT = 365,              /* START_STMT  */
    START_PROGRAM = 366            /* START_PROGRAM  */
  };
  typedef enum pkl_tab_tokentype pkl_tab_token_kind_t;
#endif

/* Value type.  */
#if ! defined PKL_TAB_STYPE && ! defined PKL_TAB_STYPE_IS_DECLARED
union PKL_TAB_STYPE
{
#line 338 "pkl-tab.y"

  pkl_ast_node ast;
  pkl_ast_node astpair[2];
  enum pkl_ast_op opcode;
  int integer;

#line 190 "pkl-tab.h"

};
typedef union PKL_TAB_STYPE PKL_TAB_STYPE;
# define PKL_TAB_STYPE_IS_TRIVIAL 1
# define PKL_TAB_STYPE_IS_DECLARED 1
#endif

/* Location type.  */
#if ! defined PKL_TAB_LTYPE && ! defined PKL_TAB_LTYPE_IS_DECLARED
typedef struct PKL_TAB_LTYPE PKL_TAB_LTYPE;
struct PKL_TAB_LTYPE
{
  int first_line;
  int first_column;
  int last_line;
  int last_column;
};
# define PKL_TAB_LTYPE_IS_DECLARED 1
# define PKL_TAB_LTYPE_IS_TRIVIAL 1
#endif



int pkl_tab_parse (struct pkl_parser *pkl_parser);

#endif /* !YY_PKL_TAB_PKL_TAB_TAB_H_INCLUDED  */
