/* A Bison parser, made by GNU Bison 3.6.2.  */

/* Bison implementation for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015, 2018-2020 Free Software Foundation,
   Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* DO NOT RELY ON FEATURES THAT ARE NOT DOCUMENTED in the manual,
   especially those whose name start with YY_ or yy_.  They are
   private implementation details that can be changed or removed.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "3.6.2"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 2

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1

/* Substitute the type names.  */
#define YYSTYPE         PKL_TAB_STYPE
#define YYLTYPE         PKL_TAB_LTYPE
/* Substitute the variable and function names.  */
#define yyparse         pkl_tab_parse
#define yylex           pkl_tab_lex
#define yyerror         pkl_tab_error
#define yydebug         pkl_tab_debug
#define yynerrs         pkl_tab_nerrs

/* First part of user prologue.  */
#line 35 "pkl-tab.y"

#include <config.h>
#include <stdlib.h>
#include <stdio.h>
#include <xalloc.h>
#include <assert.h>
#include <string.h>
#include <gettext.h>
#define _(str) gettext (str)

#include "pk-utils.h"

#include "pkl.h"
#include "pkl-diag.h"
#include "pkl-ast.h"
#include "pkl-parser.h" /* For struct pkl_parser.  */

#include "pvm.h"

#define PKL_TAB_LTYPE pkl_ast_loc
#define YYDEBUG 1
#include "pkl-tab.h"
#include "pkl-lex.h"

#ifdef PKL_DEBUG
# include "pkl-gen.h"
#endif

#define scanner (pkl_parser->scanner)

/* YYLLOC_DEFAULT -> default code for computing locations.  */

#define PKL_AST_CHILDREN_STEP 12


/* Emit an error.  */

static void
pkl_tab_error (YYLTYPE *llocp,
               struct pkl_parser *pkl_parser,
               char const *err)
{
    pkl_error (pkl_parser->compiler, pkl_parser->ast, *llocp, "%s", err);
}

/* These are used in the defun_or_method rule.  */

#define IS_DEFUN 0
#define IS_METHOD 1

/* Register an argument in the compile-time environment.  This is used
   by function specifiers and try-catch statements.

   Return 0 if there was an error registering, 1 otherwise.  */

static int
pkl_register_arg (struct pkl_parser *parser, pkl_ast_node arg)
{
  pkl_ast_node arg_decl;
  pkl_ast_node arg_identifier = PKL_AST_FUNC_ARG_IDENTIFIER (arg);

  pkl_ast_node dummy
    = pkl_ast_make_integer (parser->ast, 0);
  PKL_AST_TYPE (dummy) = ASTREF (PKL_AST_FUNC_ARG_TYPE (arg));

  arg_decl = pkl_ast_make_decl (parser->ast,
                                PKL_AST_DECL_KIND_VAR,
                                arg_identifier,
                                dummy,
                                NULL /* source */);
  PKL_AST_LOC (arg_decl) = PKL_AST_LOC (arg);

  if (!pkl_env_register (parser->env,
                         PKL_ENV_NS_MAIN,
                         PKL_AST_IDENTIFIER_POINTER (arg_identifier),
                         arg_decl))
    {
      pkl_error (parser->compiler, parser->ast,PKL_AST_LOC (arg_identifier),
                 "duplicated argument name `%s' in function declaration",
                 PKL_AST_IDENTIFIER_POINTER (arg_identifier));
      /* Make sure to pop the function frame.  */
      parser->env = pkl_env_pop_frame (parser->env);
      return 0;
    }

  return 1;
}

/* Assert statement is a syntatic sugar that transforms to invocation of
   _pkl_assert function with appropriate arguments.

   This function accepts AST nodes corresponding to the condition and
   optional message of the assert statement, and also the location info
   of the statement.

   Returns NULL on failure, and expression statement AST node on success.  */

static pkl_ast_node
pkl_make_assertion (struct pkl_parser *p, pkl_ast_node cond, pkl_ast_node msg,
                    struct pkl_ast_loc stmt_loc)
{
  pkl_ast_node vfunc, call, call_arg;
  pkl_ast_node arg_cond, arg_msg, arg_lineinfo; /* _pkl_assert args */

  /* Make variable for `_pkl_assert` function */
  {
    const char *name = "_pkl_assert";
    pkl_ast_node vfunc_init;
    int back, over;

    vfunc_init = pkl_env_lookup (p->env, PKL_ENV_NS_MAIN, name, &back, &over);
    if (!vfunc_init
        || (PKL_AST_DECL_KIND (vfunc_init) != PKL_AST_DECL_KIND_FUNC))
      {
        pkl_error (p->compiler, p->ast, stmt_loc, "undefined function '%s'",
                   name);
        return NULL;
      }
    vfunc = pkl_ast_make_var (p->ast, pkl_ast_make_identifier (p->ast, name),
                              vfunc_init, back, over);
  }

  /* First argument of _pkl_assert */
  arg_cond = pkl_ast_make_funcall_arg (p->ast, cond, NULL);
  PKL_AST_LOC (arg_cond) = PKL_AST_LOC (cond);

  /* Second argument of _pkl_assert */
  if (msg == NULL)
    {
      pkl_ast_node stype = pkl_ast_make_string_type (p->ast);

      msg = pkl_ast_make_string (p->ast, "");
      PKL_AST_TYPE (msg) = ASTREF (stype);
    }
  arg_msg = pkl_ast_make_funcall_arg (p->ast, msg, NULL);
  arg_msg = ASTREF (arg_msg);
  PKL_AST_LOC (arg_msg) = PKL_AST_LOC (msg);

  /* Third argument of _pkl_assert to report the location of the assert
     statement with the following format "<FILENAME>:<LINE>:<COLUMN>".  */
  {
    char *str;
    pkl_ast_node lineinfo, stype;

    if (asprintf (&str, "%s:%d:%d", p->filename ? p->filename : "<stdin>",
                  stmt_loc.first_line, stmt_loc.first_column)
        == -1)
      return NULL;
    lineinfo = pkl_ast_make_string (p->ast, str);
    free (str);
    stype = pkl_ast_make_string_type (p->ast);
    PKL_AST_TYPE (lineinfo) = ASTREF (stype);

    arg_lineinfo = pkl_ast_make_funcall_arg (p->ast, lineinfo, NULL);
    arg_lineinfo = ASTREF (arg_lineinfo);
  }

  call_arg
      = pkl_ast_chainon (arg_cond, pkl_ast_chainon (arg_msg, arg_lineinfo));
  call = pkl_ast_make_funcall (p->ast, vfunc, call_arg);
  return pkl_ast_make_exp_stmt (p->ast, call);
}

#if 0
/* Register a list of arguments in the compile-time environment.  This
   is used by function specifiers and try-catch statements.

   Return 0 if there was an error registering, 1 otherwise.  */

static int
pkl_register_args (struct pkl_parser *parser, pkl_ast_node arg_list)
{
  pkl_ast_node arg;

  for (arg = arg_list; arg; arg = PKL_AST_CHAIN (arg))
    {
      pkl_ast_node arg_decl;
      pkl_ast_node arg_identifier = PKL_AST_FUNC_ARG_IDENTIFIER (arg);

      pkl_ast_node dummy
        = pkl_ast_make_integer (parser->ast, 0);
      PKL_AST_TYPE (dummy) = ASTREF (PKL_AST_FUNC_ARG_TYPE (arg));

      arg_decl = pkl_ast_make_decl (parser->ast,
                                    PKL_AST_DECL_KIND_VAR,
                                    arg_identifier,
                                    dummy,
                                    NULL /* source */);
      PKL_AST_LOC (arg_decl) = PKL_AST_LOC (arg);

      if (!pkl_env_register (parser->env,
                             PKL_ENV_NS_MAIN,
                             PKL_AST_IDENTIFIER_POINTER (arg_identifier),
                             arg_decl))
        {
          pkl_error (parser->compiler, parser->ast, PKL_AST_LOC (arg_identifier),
                     "duplicated argument name `%s' in function declaration",
                     PKL_AST_IDENTIFIER_POINTER (arg_identifier));
          /* Make sure to pop the function frame.  */
          parser->env = pkl_env_pop_frame (parser->env);
          return 0;
        }
    }

  return 1;
}
#endif

/* Register N dummy entries in the compilation environment.  */

static void
pkl_register_dummies (struct pkl_parser *parser, int n)
{
  int i;
  for (i = 0; i < n; ++i)
    {
      char *name;
      pkl_ast_node id;
      pkl_ast_node decl;
      int r;

      asprintf (&name, "@*UNUSABLE_OFF_%d*@", i);
      id = pkl_ast_make_identifier (parser->ast, name);
      decl = pkl_ast_make_decl (parser->ast,
                                PKL_AST_DECL_KIND_VAR,
                                id, NULL /* initial */,
                                NULL /* source */);

      r = pkl_env_register (parser->env, PKL_ENV_NS_MAIN, name, decl);
      assert (r);
    }
}

/* Load a module, given its name.
   If the module file cannot be read, return 1.
   If there is a parse error loading the module, return 2.
   Otherwise, return 0.  */

static int
load_module (struct pkl_parser *parser,
             const char *module, pkl_ast_node *node,
             int filename_p, char **filename)
{
  char *module_filename = NULL;
  pkl_ast ast;
  FILE *fp;

  module_filename = pkl_resolve_module (parser->compiler,
                                        module,
                                        filename_p);
  if (module_filename == NULL)
    /* No file found.  */
    return 1;

  if (pkl_module_loaded_p (parser->compiler, module_filename))
    {
      /* Module already loaded.  */
      *node = NULL;
      return 0;
    }

  fp = fopen (module_filename, "rb");
  if (!fp)
    {
      free (module_filename);
      return 1;
    }

  /* Parse the file, using the given environment.  The declarations
     found in the parsed file are appended to that environment, so
     nothing extra should be done about that.  */
  if (pkl_parse_file (parser->compiler, &parser->env, &ast, fp,
                      module_filename)
      != 0)
    {
      fclose (fp);
      free (module_filename);
      return 2;
    }

  /* Add the module to the compiler's list of loaded modules.  */
  pkl_add_module (parser->compiler, module_filename);

  /* However, the AST nodes shall be appended explicitly, which is
     achieved by returning them to the caller in the NODE
     argument.  */
  *node = PKL_AST_PROGRAM_ELEMS (ast->ast);

  /* Dirty hack is dirty, but it works.  */
  PKL_AST_PROGRAM_ELEMS (ast->ast) = NULL;
  pkl_ast_free (ast);

  /* Set the `filename' output argument if needed.  */
  if (filename)
    *filename = strdup (module_filename);

  fclose (fp);
  free (module_filename);
  return 0;
}


#line 381 "pkl-tab.c"

# ifndef YY_CAST
#  ifdef __cplusplus
#   define YY_CAST(Type, Val) static_cast<Type> (Val)
#   define YY_REINTERPRET_CAST(Type, Val) reinterpret_cast<Type> (Val)
#  else
#   define YY_CAST(Type, Val) ((Type) (Val))
#   define YY_REINTERPRET_CAST(Type, Val) ((Type) (Val))
#  endif
# endif
# ifndef YY_NULLPTR
#  if defined __cplusplus
#   if 201103L <= __cplusplus
#    define YY_NULLPTR nullptr
#   else
#    define YY_NULLPTR 0
#   endif
#  else
#   define YY_NULLPTR ((void*)0)
#  endif
# endif

/* Use api.header.include to #include this header
   instead of duplicating it here.  */
#ifndef YY_PKL_TAB_PKL_TAB_TAB_H_INCLUDED
# define YY_PKL_TAB_PKL_TAB_TAB_H_INCLUDED
/* Debug traces.  */
#ifndef PKL_TAB_DEBUG
# if defined YYDEBUG
#if YYDEBUG
#   define PKL_TAB_DEBUG 1
#  else
#   define PKL_TAB_DEBUG 0
#  endif
# else /* ! defined YYDEBUG */
#  define PKL_TAB_DEBUG 1
# endif /* ! defined YYDEBUG */
#endif  /* ! defined PKL_TAB_DEBUG */
#if PKL_TAB_DEBUG
extern int pkl_tab_debug;
#endif

/* Token kinds.  */
#ifndef PKL_TAB_TOKENTYPE
# define PKL_TAB_TOKENTYPE
  enum pkl_tab_tokentype
  {
    PKL_TAB_EMPTY = -2,
    PKL_TAB_EOF = 0,               /* "end of file"  */
    PKL_TAB_error = 256,           /* error  */
    PKL_TAB_UNDEF = 257,           /* "invalid token"  */
    INTEGER = 258,                 /* "integer literal"  */
    INTEGER_OVERFLOW = 259,        /* INTEGER_OVERFLOW  */
    CHAR = 260,                    /* "character literal"  */
    STR = 261,                     /* "string"  */
    IDENTIFIER = 262,              /* "identifier"  */
    TYPENAME = 263,                /* "type name"  */
    UNIT = 264,                    /* "offset unit"  */
    ENUM = 265,                    /* "keyword `enum'"  */
    PINNED = 266,                  /* "keyword `pinned'"  */
    STRUCT = 267,                  /* "keyword `struct'"  */
    token = 268,                   /* token  */
    UNION = 269,                   /* "keyword `union'"  */
    CONST = 270,                   /* "keyword `const'"  */
    CONTINUE = 271,                /* "keyword `continue'"  */
    ELSE = 272,                    /* "keyword `else'"  */
    IF = 273,                      /* "keyword `if'"  */
    WHILE = 274,                   /* "keyword `while"  */
    UNTIL = 275,                   /* "keyword `until'"  */
    FOR = 276,                     /* "keyword `for'"  */
    IN = 277,                      /* "keyword `in'"  */
    WHERE = 278,                   /* "keyword `where'"  */
    SIZEOF = 279,                  /* "keyword `sizeof'"  */
    ASSERT = 280,                  /* "keyword `assert'"  */
    ERR = 281,                     /* "token"  */
    ALIEN = 282,                   /* ALIEN  */
    INTCONSTR = 283,               /* "int type constructor"  */
    UINTCONSTR = 284,              /* "uint type constructor"  */
    OFFSETCONSTR = 285,            /* "offset type constructor"  */
    DEFUN = 286,                   /* "keyword `fun'"  */
    DEFSET = 287,                  /* "keyword `defset'"  */
    DEFTYPE = 288,                 /* "keyword `type'"  */
    DEFVAR = 289,                  /* "keyword `var'"  */
    DEFUNIT = 290,                 /* "keyword `unit'"  */
    METHOD = 291,                  /* "keyword `method'"  */
    RETURN = 292,                  /* "keyword `return'"  */
    BREAK = 293,                   /* "keyword `break'"  */
    STRING = 294,                  /* "string type specifier"  */
    TRY = 295,                     /* "keyword `try'"  */
    CATCH = 296,                   /* "keyword `catch'"  */
    RAISE = 297,                   /* "keyword `raise'"  */
    VOID = 298,                    /* "void type specifier"  */
    ANY = 299,                     /* "any type specifier"  */
    PRINT = 300,                   /* "keyword `print'"  */
    PRINTF = 301,                  /* "keyword `printf'"  */
    LOAD = 302,                    /* "keyword `load'"  */
    LAMBDA = 303,                  /* "keyword `lambda'"  */
    BUILTIN_RAND = 304,            /* BUILTIN_RAND  */
    BUILTIN_GET_ENDIAN = 305,      /* BUILTIN_GET_ENDIAN  */
    BUILTIN_SET_ENDIAN = 306,      /* BUILTIN_SET_ENDIAN  */
    BUILTIN_GET_IOS = 307,         /* BUILTIN_GET_IOS  */
    BUILTIN_SET_IOS = 308,         /* BUILTIN_SET_IOS  */
    BUILTIN_OPEN = 309,            /* BUILTIN_OPEN  */
    BUILTIN_CLOSE = 310,           /* BUILTIN_CLOSE  */
    BUILTIN_IOSIZE = 311,          /* BUILTIN_IOSIZE  */
    BUILTIN_GETENV = 312,          /* BUILTIN_GETENV  */
    BUILTIN_FORGET = 313,          /* BUILTIN_FORGET  */
    BUILTIN_GET_TIME = 314,        /* BUILTIN_GET_TIME  */
    BUILTIN_STRACE = 315,          /* BUILTIN_STRACE  */
    BUILTIN_TERM_RGB_TO_COLOR = 316, /* BUILTIN_TERM_RGB_TO_COLOR  */
    BUILTIN_TERM_GET_COLOR = 317,  /* BUILTIN_TERM_GET_COLOR  */
    BUILTIN_TERM_SET_COLOR = 318,  /* BUILTIN_TERM_SET_COLOR  */
    BUILTIN_TERM_GET_BGCOLOR = 319, /* BUILTIN_TERM_GET_BGCOLOR  */
    BUILTIN_TERM_SET_BGCOLOR = 320, /* BUILTIN_TERM_SET_BGCOLOR  */
    BUILTIN_TERM_BEGIN_CLASS = 321, /* BUILTIN_TERM_BEGIN_CLASS  */
    BUILTIN_TERM_END_CLASS = 322,  /* BUILTIN_TERM_END_CLASS  */
    BUILTIN_TERM_BEGIN_HYPERLINK = 323, /* BUILTIN_TERM_BEGIN_HYPERLINK  */
    BUILTIN_TERM_END_HYPERLINK = 324, /* BUILTIN_TERM_END_HYPERLINK  */
    POWA = 325,                    /* "power-and-assign operator"  */
    MULA = 326,                    /* "multiply-and-assign operator"  */
    DIVA = 327,                    /* "divide-and-assing operator"  */
    MODA = 328,                    /* "modulus-and-assign operator"  */
    ADDA = 329,                    /* "add-and-assing operator"  */
    SUBA = 330,                    /* "subtract-and-assign operator"  */
    SLA = 331,                     /* "shift-left-and-assign operator"  */
    SRA = 332,                     /* "shift-right-and-assign operator"  */
    BANDA = 333,                   /* "bit-and-and-assign operator"  */
    XORA = 334,                    /* "bit-xor-and-assign operator"  */
    IORA = 335,                    /* "bit-or-and-assign operator"  */
    RANGEA = 336,                  /* "range separator"  */
    OR = 337,                      /* "logical or operator"  */
    AND = 338,                     /* "logical and operator"  */
    EQ = 339,                      /* "equality operator"  */
    NE = 340,                      /* "inequality operator"  */
    LE = 341,                      /* "less-or-equal operator"  */
    GE = 342,                      /* "bigger-or-equal-than operator"  */
    SL = 343,                      /* "left shift operator"  */
    SR = 344,                      /* "right shift operator"  */
    CEILDIV = 345,                 /* "ceiling division operator"  */
    POW = 346,                     /* "power operator"  */
    BCONC = 347,                   /* "bit-concatenation operator"  */
    NSMAP = 348,                   /* "non-strict map operator"  */
    INC = 349,                     /* "increment operator"  */
    DEC = 350,                     /* "decrement operator"  */
    AS = 351,                      /* "cast operator"  */
    ISA = 352,                     /* "type identification operator"  */
    ATTR = 353,                    /* "attribute"  */
    UNMAP = 354,                   /* "unmap operator"  */
    BIG = 355,                     /* "keyword `big'"  */
    LITTLE = 356,                  /* "keyword `little'"  */
    SIGNED = 357,                  /* "keyword `signed'"  */
    UNSIGNED = 358,                /* "keyword `unsigned'"  */
    THREEDOTS = 359,               /* "varargs indicator"  */
    THEN = 360,                    /* THEN  */
    UNARY = 361,                   /* UNARY  */
    HYPERUNARY = 362,              /* HYPERUNARY  */
    START_EXP = 363,               /* START_EXP  */
    START_DECL = 364,              /* START_DECL  */
    START_STMT = 365,              /* START_STMT  */
    START_PROGRAM = 366            /* START_PROGRAM  */
  };
  typedef enum pkl_tab_tokentype pkl_tab_token_kind_t;
#endif

/* Value type.  */
#if ! defined PKL_TAB_STYPE && ! defined PKL_TAB_STYPE_IS_DECLARED
union PKL_TAB_STYPE
{
#line 338 "pkl-tab.y"

  pkl_ast_node ast;
  pkl_ast_node astpair[2];
  enum pkl_ast_op opcode;
  int integer;

#line 557 "pkl-tab.c"

};
typedef union PKL_TAB_STYPE PKL_TAB_STYPE;
# define PKL_TAB_STYPE_IS_TRIVIAL 1
# define PKL_TAB_STYPE_IS_DECLARED 1
#endif

/* Location type.  */
#if ! defined PKL_TAB_LTYPE && ! defined PKL_TAB_LTYPE_IS_DECLARED
typedef struct PKL_TAB_LTYPE PKL_TAB_LTYPE;
struct PKL_TAB_LTYPE
{
  int first_line;
  int first_column;
  int last_line;
  int last_column;
};
# define PKL_TAB_LTYPE_IS_DECLARED 1
# define PKL_TAB_LTYPE_IS_TRIVIAL 1
#endif



int pkl_tab_parse (struct pkl_parser *pkl_parser);

#endif /* !YY_PKL_TAB_PKL_TAB_TAB_H_INCLUDED  */
/* Symbol kind.  */
enum yysymbol_kind_t
{
  YYSYMBOL_YYEMPTY = -2,
  YYSYMBOL_YYEOF = 0,                      /* "end of file"  */
  YYSYMBOL_YYerror = 1,                    /* error  */
  YYSYMBOL_YYUNDEF = 2,                    /* "invalid token"  */
  YYSYMBOL_INTEGER = 3,                    /* "integer literal"  */
  YYSYMBOL_INTEGER_OVERFLOW = 4,           /* INTEGER_OVERFLOW  */
  YYSYMBOL_CHAR = 5,                       /* "character literal"  */
  YYSYMBOL_STR = 6,                        /* "string"  */
  YYSYMBOL_IDENTIFIER = 7,                 /* "identifier"  */
  YYSYMBOL_TYPENAME = 8,                   /* "type name"  */
  YYSYMBOL_UNIT = 9,                       /* "offset unit"  */
  YYSYMBOL_ENUM = 10,                      /* "keyword `enum'"  */
  YYSYMBOL_PINNED = 11,                    /* "keyword `pinned'"  */
  YYSYMBOL_STRUCT = 12,                    /* "keyword `struct'"  */
  YYSYMBOL_token = 13,                     /* token  */
  YYSYMBOL_UNION = 14,                     /* "keyword `union'"  */
  YYSYMBOL_CONST = 15,                     /* "keyword `const'"  */
  YYSYMBOL_CONTINUE = 16,                  /* "keyword `continue'"  */
  YYSYMBOL_ELSE = 17,                      /* "keyword `else'"  */
  YYSYMBOL_IF = 18,                        /* "keyword `if'"  */
  YYSYMBOL_WHILE = 19,                     /* "keyword `while"  */
  YYSYMBOL_UNTIL = 20,                     /* "keyword `until'"  */
  YYSYMBOL_FOR = 21,                       /* "keyword `for'"  */
  YYSYMBOL_IN = 22,                        /* "keyword `in'"  */
  YYSYMBOL_WHERE = 23,                     /* "keyword `where'"  */
  YYSYMBOL_SIZEOF = 24,                    /* "keyword `sizeof'"  */
  YYSYMBOL_ASSERT = 25,                    /* "keyword `assert'"  */
  YYSYMBOL_ERR = 26,                       /* "token"  */
  YYSYMBOL_ALIEN = 27,                     /* ALIEN  */
  YYSYMBOL_INTCONSTR = 28,                 /* "int type constructor"  */
  YYSYMBOL_UINTCONSTR = 29,                /* "uint type constructor"  */
  YYSYMBOL_OFFSETCONSTR = 30,              /* "offset type constructor"  */
  YYSYMBOL_DEFUN = 31,                     /* "keyword `fun'"  */
  YYSYMBOL_DEFSET = 32,                    /* "keyword `defset'"  */
  YYSYMBOL_DEFTYPE = 33,                   /* "keyword `type'"  */
  YYSYMBOL_DEFVAR = 34,                    /* "keyword `var'"  */
  YYSYMBOL_DEFUNIT = 35,                   /* "keyword `unit'"  */
  YYSYMBOL_METHOD = 36,                    /* "keyword `method'"  */
  YYSYMBOL_RETURN = 37,                    /* "keyword `return'"  */
  YYSYMBOL_BREAK = 38,                     /* "keyword `break'"  */
  YYSYMBOL_STRING = 39,                    /* "string type specifier"  */
  YYSYMBOL_TRY = 40,                       /* "keyword `try'"  */
  YYSYMBOL_CATCH = 41,                     /* "keyword `catch'"  */
  YYSYMBOL_RAISE = 42,                     /* "keyword `raise'"  */
  YYSYMBOL_VOID = 43,                      /* "void type specifier"  */
  YYSYMBOL_ANY = 44,                       /* "any type specifier"  */
  YYSYMBOL_PRINT = 45,                     /* "keyword `print'"  */
  YYSYMBOL_PRINTF = 46,                    /* "keyword `printf'"  */
  YYSYMBOL_LOAD = 47,                      /* "keyword `load'"  */
  YYSYMBOL_LAMBDA = 48,                    /* "keyword `lambda'"  */
  YYSYMBOL_BUILTIN_RAND = 49,              /* BUILTIN_RAND  */
  YYSYMBOL_BUILTIN_GET_ENDIAN = 50,        /* BUILTIN_GET_ENDIAN  */
  YYSYMBOL_BUILTIN_SET_ENDIAN = 51,        /* BUILTIN_SET_ENDIAN  */
  YYSYMBOL_BUILTIN_GET_IOS = 52,           /* BUILTIN_GET_IOS  */
  YYSYMBOL_BUILTIN_SET_IOS = 53,           /* BUILTIN_SET_IOS  */
  YYSYMBOL_BUILTIN_OPEN = 54,              /* BUILTIN_OPEN  */
  YYSYMBOL_BUILTIN_CLOSE = 55,             /* BUILTIN_CLOSE  */
  YYSYMBOL_BUILTIN_IOSIZE = 56,            /* BUILTIN_IOSIZE  */
  YYSYMBOL_BUILTIN_GETENV = 57,            /* BUILTIN_GETENV  */
  YYSYMBOL_BUILTIN_FORGET = 58,            /* BUILTIN_FORGET  */
  YYSYMBOL_BUILTIN_GET_TIME = 59,          /* BUILTIN_GET_TIME  */
  YYSYMBOL_BUILTIN_STRACE = 60,            /* BUILTIN_STRACE  */
  YYSYMBOL_BUILTIN_TERM_RGB_TO_COLOR = 61, /* BUILTIN_TERM_RGB_TO_COLOR  */
  YYSYMBOL_BUILTIN_TERM_GET_COLOR = 62,    /* BUILTIN_TERM_GET_COLOR  */
  YYSYMBOL_BUILTIN_TERM_SET_COLOR = 63,    /* BUILTIN_TERM_SET_COLOR  */
  YYSYMBOL_BUILTIN_TERM_GET_BGCOLOR = 64,  /* BUILTIN_TERM_GET_BGCOLOR  */
  YYSYMBOL_BUILTIN_TERM_SET_BGCOLOR = 65,  /* BUILTIN_TERM_SET_BGCOLOR  */
  YYSYMBOL_BUILTIN_TERM_BEGIN_CLASS = 66,  /* BUILTIN_TERM_BEGIN_CLASS  */
  YYSYMBOL_BUILTIN_TERM_END_CLASS = 67,    /* BUILTIN_TERM_END_CLASS  */
  YYSYMBOL_BUILTIN_TERM_BEGIN_HYPERLINK = 68, /* BUILTIN_TERM_BEGIN_HYPERLINK  */
  YYSYMBOL_BUILTIN_TERM_END_HYPERLINK = 69, /* BUILTIN_TERM_END_HYPERLINK  */
  YYSYMBOL_POWA = 70,                      /* "power-and-assign operator"  */
  YYSYMBOL_MULA = 71,                      /* "multiply-and-assign operator"  */
  YYSYMBOL_DIVA = 72,                      /* "divide-and-assing operator"  */
  YYSYMBOL_MODA = 73,                      /* "modulus-and-assign operator"  */
  YYSYMBOL_ADDA = 74,                      /* "add-and-assing operator"  */
  YYSYMBOL_SUBA = 75,                      /* "subtract-and-assign operator"  */
  YYSYMBOL_SLA = 76,                       /* "shift-left-and-assign operator"  */
  YYSYMBOL_SRA = 77,                       /* "shift-right-and-assign operator"  */
  YYSYMBOL_BANDA = 78,                     /* "bit-and-and-assign operator"  */
  YYSYMBOL_XORA = 79,                      /* "bit-xor-and-assign operator"  */
  YYSYMBOL_IORA = 80,                      /* "bit-or-and-assign operator"  */
  YYSYMBOL_RANGEA = 81,                    /* "range separator"  */
  YYSYMBOL_OR = 82,                        /* "logical or operator"  */
  YYSYMBOL_AND = 83,                       /* "logical and operator"  */
  YYSYMBOL_84_bit_wise_or_operator_ = 84,  /* "bit-wise or operator"  */
  YYSYMBOL_85_bit_wise_xor_operator_ = 85, /* "bit-wise xor operator"  */
  YYSYMBOL_86_bit_wise_and_operator_ = 86, /* "bit-wise and operator"  */
  YYSYMBOL_EQ = 87,                        /* "equality operator"  */
  YYSYMBOL_NE = 88,                        /* "inequality operator"  */
  YYSYMBOL_LE = 89,                        /* "less-or-equal operator"  */
  YYSYMBOL_GE = 90,                        /* "bigger-or-equal-than operator"  */
  YYSYMBOL_91_less_than_operator_ = 91,    /* "less-than operator"  */
  YYSYMBOL_92_bigger_than_operator_ = 92,  /* "bigger-than operator"  */
  YYSYMBOL_SL = 93,                        /* "left shift operator"  */
  YYSYMBOL_SR = 94,                        /* "right shift operator"  */
  YYSYMBOL_95_addition_operator_ = 95,     /* "addition operator"  */
  YYSYMBOL_96_subtraction_operator_ = 96,  /* "subtraction operator"  */
  YYSYMBOL_97_multiplication_operator_ = 97, /* "multiplication operator"  */
  YYSYMBOL_98_division_operator_ = 98,     /* "division operator"  */
  YYSYMBOL_CEILDIV = 99,                   /* "ceiling division operator"  */
  YYSYMBOL_100_modulus_operator_ = 100,    /* "modulus operator"  */
  YYSYMBOL_POW = 101,                      /* "power operator"  */
  YYSYMBOL_BCONC = 102,                    /* "bit-concatenation operator"  */
  YYSYMBOL_103_map_operator_ = 103,        /* "map operator"  */
  YYSYMBOL_NSMAP = 104,                    /* "non-strict map operator"  */
  YYSYMBOL_INC = 105,                      /* "increment operator"  */
  YYSYMBOL_DEC = 106,                      /* "decrement operator"  */
  YYSYMBOL_AS = 107,                       /* "cast operator"  */
  YYSYMBOL_ISA = 108,                      /* "type identification operator"  */
  YYSYMBOL_109_dot_operator_ = 109,        /* "dot operator"  */
  YYSYMBOL_ATTR = 110,                     /* "attribute"  */
  YYSYMBOL_UNMAP = 111,                    /* "unmap operator"  */
  YYSYMBOL_BIG = 112,                      /* "keyword `big'"  */
  YYSYMBOL_LITTLE = 113,                   /* "keyword `little'"  */
  YYSYMBOL_SIGNED = 114,                   /* "keyword `signed'"  */
  YYSYMBOL_UNSIGNED = 115,                 /* "keyword `unsigned'"  */
  YYSYMBOL_THREEDOTS = 116,                /* "varargs indicator"  */
  YYSYMBOL_THEN = 117,                     /* THEN  */
  YYSYMBOL_118_ = 118,                     /* '?'  */
  YYSYMBOL_119_ = 119,                     /* ':'  */
  YYSYMBOL_UNARY = 120,                    /* UNARY  */
  YYSYMBOL_HYPERUNARY = 121,               /* HYPERUNARY  */
  YYSYMBOL_START_EXP = 122,                /* START_EXP  */
  YYSYMBOL_START_DECL = 123,               /* START_DECL  */
  YYSYMBOL_START_STMT = 124,               /* START_STMT  */
  YYSYMBOL_START_PROGRAM = 125,            /* START_PROGRAM  */
  YYSYMBOL_126_ = 126,                     /* ','  */
  YYSYMBOL_127_ = 127,                     /* ';'  */
  YYSYMBOL_128_ = 128,                     /* '('  */
  YYSYMBOL_129_ = 129,                     /* ')'  */
  YYSYMBOL_130_ = 130,                     /* '{'  */
  YYSYMBOL_131_ = 131,                     /* '}'  */
  YYSYMBOL_132_ = 132,                     /* '~'  */
  YYSYMBOL_133_ = 133,                     /* '!'  */
  YYSYMBOL_134_ = 134,                     /* '['  */
  YYSYMBOL_135_ = 135,                     /* ']'  */
  YYSYMBOL_136_ = 136,                     /* '='  */
  YYSYMBOL_YYACCEPT = 137,                 /* $accept  */
  YYSYMBOL_pushlevel = 138,                /* pushlevel  */
  YYSYMBOL_start = 139,                    /* start  */
  YYSYMBOL_program = 140,                  /* program  */
  YYSYMBOL_program_elem_list = 141,        /* program_elem_list  */
  YYSYMBOL_program_elem = 142,             /* program_elem  */
  YYSYMBOL_load = 143,                     /* load  */
  YYSYMBOL_identifier = 144,               /* identifier  */
  YYSYMBOL_expression_list = 145,          /* expression_list  */
  YYSYMBOL_expression_opt = 146,           /* expression_opt  */
  YYSYMBOL_expression = 147,               /* expression  */
  YYSYMBOL_bconc = 148,                    /* bconc  */
  YYSYMBOL_mapop = 149,                    /* mapop  */
  YYSYMBOL_map = 150,                      /* map  */
  YYSYMBOL_unary_operator = 151,           /* unary_operator  */
  YYSYMBOL_primary = 152,                  /* primary  */
  YYSYMBOL_153_1 = 153,                    /* $@1  */
  YYSYMBOL_funcall = 154,                  /* funcall  */
  YYSYMBOL_funcall_arg_list = 155,         /* funcall_arg_list  */
  YYSYMBOL_funcall_arg = 156,              /* funcall_arg  */
  YYSYMBOL_opt_comma = 157,                /* opt_comma  */
  YYSYMBOL_struct_field_list = 158,        /* struct_field_list  */
  YYSYMBOL_struct_field = 159,             /* struct_field  */
  YYSYMBOL_array = 160,                    /* array  */
  YYSYMBOL_array_initializer_list = 161,   /* array_initializer_list  */
  YYSYMBOL_array_initializer = 162,        /* array_initializer  */
  YYSYMBOL_pushlevel_args = 163,           /* pushlevel_args  */
  YYSYMBOL_function_specifier = 164,       /* function_specifier  */
  YYSYMBOL_function_arg_list = 165,        /* function_arg_list  */
  YYSYMBOL_function_arg = 166,             /* function_arg  */
  YYSYMBOL_function_arg_initial = 167,     /* function_arg_initial  */
  YYSYMBOL_type_specifier = 168,           /* type_specifier  */
  YYSYMBOL_typename = 169,                 /* typename  */
  YYSYMBOL_string_type_specifier = 170,    /* string_type_specifier  */
  YYSYMBOL_simple_type_specifier = 171,    /* simple_type_specifier  */
  YYSYMBOL_cons_type_specifier = 172,      /* cons_type_specifier  */
  YYSYMBOL_integral_type_specifier = 173,  /* integral_type_specifier  */
  YYSYMBOL_integral_type_sign = 174,       /* integral_type_sign  */
  YYSYMBOL_offset_type_specifier = 175,    /* offset_type_specifier  */
  YYSYMBOL_array_type_specifier = 176,     /* array_type_specifier  */
  YYSYMBOL_function_type_specifier = 177,  /* function_type_specifier  */
  YYSYMBOL_function_type_arg_list = 178,   /* function_type_arg_list  */
  YYSYMBOL_function_type_arg = 179,        /* function_type_arg  */
  YYSYMBOL_struct_type_specifier = 180,    /* struct_type_specifier  */
  YYSYMBOL_181_2 = 181,                    /* $@2  */
  YYSYMBOL_struct_or_union = 182,          /* struct_or_union  */
  YYSYMBOL_struct_type_pinned = 183,       /* struct_type_pinned  */
  YYSYMBOL_integral_struct = 184,          /* integral_struct  */
  YYSYMBOL_struct_type_elem_list = 185,    /* struct_type_elem_list  */
  YYSYMBOL_endianness = 186,               /* endianness  */
  YYSYMBOL_struct_type_field = 187,        /* struct_type_field  */
  YYSYMBOL_188_3 = 188,                    /* $@3  */
  YYSYMBOL_struct_type_field_identifier = 189, /* struct_type_field_identifier  */
  YYSYMBOL_struct_type_field_label = 190,  /* struct_type_field_label  */
  YYSYMBOL_struct_type_field_constraint_and_init = 191, /* struct_type_field_constraint_and_init  */
  YYSYMBOL_struct_type_field_optcond = 192, /* struct_type_field_optcond  */
  YYSYMBOL_simple_declaration = 193,       /* simple_declaration  */
  YYSYMBOL_declaration = 194,              /* declaration  */
  YYSYMBOL_195_4 = 195,                    /* @4  */
  YYSYMBOL_defun_or_method = 196,          /* defun_or_method  */
  YYSYMBOL_defvar_list = 197,              /* defvar_list  */
  YYSYMBOL_defvar = 198,                   /* defvar  */
  YYSYMBOL_deftype_list = 199,             /* deftype_list  */
  YYSYMBOL_deftype = 200,                  /* deftype  */
  YYSYMBOL_defunit_list = 201,             /* defunit_list  */
  YYSYMBOL_defunit = 202,                  /* defunit  */
  YYSYMBOL_comp_stmt = 203,                /* comp_stmt  */
  YYSYMBOL_builtin = 204,                  /* builtin  */
  YYSYMBOL_stmt_decl_list = 205,           /* stmt_decl_list  */
  YYSYMBOL_ass_exp_op = 206,               /* ass_exp_op  */
  YYSYMBOL_simple_stmt_list = 207,         /* simple_stmt_list  */
  YYSYMBOL_simple_stmt = 208,              /* simple_stmt  */
  YYSYMBOL_stmt = 209,                     /* stmt  */
  YYSYMBOL_210_5 = 210,                    /* @5  */
  YYSYMBOL_211_6 = 211,                    /* @6  */
  YYSYMBOL_print_stmt_arg_list = 212,      /* print_stmt_arg_list  */
  YYSYMBOL_funcall_stmt = 213,             /* funcall_stmt  */
  YYSYMBOL_funcall_stmt_arg_list = 214,    /* funcall_stmt_arg_list  */
  YYSYMBOL_funcall_stmt_arg = 215          /* funcall_stmt_arg  */
};
typedef enum yysymbol_kind_t yysymbol_kind_t;




#ifdef short
# undef short
#endif

/* On compilers that do not define __PTRDIFF_MAX__ etc., make sure
   <limits.h> and (if available) <stdint.h> are included
   so that the code can choose integer types of a good width.  */

#ifndef __PTRDIFF_MAX__
# include <limits.h> /* INFRINGES ON USER NAME SPACE */
# if defined __STDC_VERSION__ && 199901 <= __STDC_VERSION__
#  include <stdint.h> /* INFRINGES ON USER NAME SPACE */
#  define YY_STDINT_H
# endif
#endif

/* Narrow types that promote to a signed type and that can represent a
   signed or unsigned integer of at least N bits.  In tables they can
   save space and decrease cache pressure.  Promoting to a signed type
   helps avoid bugs in integer arithmetic.  */

#ifdef __INT_LEAST8_MAX__
typedef __INT_LEAST8_TYPE__ yytype_int8;
#elif defined YY_STDINT_H
typedef int_least8_t yytype_int8;
#else
typedef signed char yytype_int8;
#endif

#ifdef __INT_LEAST16_MAX__
typedef __INT_LEAST16_TYPE__ yytype_int16;
#elif defined YY_STDINT_H
typedef int_least16_t yytype_int16;
#else
typedef short yytype_int16;
#endif

#if defined __UINT_LEAST8_MAX__ && __UINT_LEAST8_MAX__ <= __INT_MAX__
typedef __UINT_LEAST8_TYPE__ yytype_uint8;
#elif (!defined __UINT_LEAST8_MAX__ && defined YY_STDINT_H \
       && UINT_LEAST8_MAX <= INT_MAX)
typedef uint_least8_t yytype_uint8;
#elif !defined __UINT_LEAST8_MAX__ && UCHAR_MAX <= INT_MAX
typedef unsigned char yytype_uint8;
#else
typedef short yytype_uint8;
#endif

#if defined __UINT_LEAST16_MAX__ && __UINT_LEAST16_MAX__ <= __INT_MAX__
typedef __UINT_LEAST16_TYPE__ yytype_uint16;
#elif (!defined __UINT_LEAST16_MAX__ && defined YY_STDINT_H \
       && UINT_LEAST16_MAX <= INT_MAX)
typedef uint_least16_t yytype_uint16;
#elif !defined __UINT_LEAST16_MAX__ && USHRT_MAX <= INT_MAX
typedef unsigned short yytype_uint16;
#else
typedef int yytype_uint16;
#endif

#ifndef YYPTRDIFF_T
# if defined __PTRDIFF_TYPE__ && defined __PTRDIFF_MAX__
#  define YYPTRDIFF_T __PTRDIFF_TYPE__
#  define YYPTRDIFF_MAXIMUM __PTRDIFF_MAX__
# elif defined PTRDIFF_MAX
#  ifndef ptrdiff_t
#   include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  endif
#  define YYPTRDIFF_T ptrdiff_t
#  define YYPTRDIFF_MAXIMUM PTRDIFF_MAX
# else
#  define YYPTRDIFF_T long
#  define YYPTRDIFF_MAXIMUM LONG_MAX
# endif
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif defined __STDC_VERSION__ && 199901 <= __STDC_VERSION__
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned
# endif
#endif

#define YYSIZE_MAXIMUM                                  \
  YY_CAST (YYPTRDIFF_T,                                 \
           (YYPTRDIFF_MAXIMUM < YY_CAST (YYSIZE_T, -1)  \
            ? YYPTRDIFF_MAXIMUM                         \
            : YY_CAST (YYSIZE_T, -1)))

#define YYSIZEOF(X) YY_CAST (YYPTRDIFF_T, sizeof (X))


/* Stored state numbers (used for stacks). */
typedef yytype_int16 yy_state_t;

/* State numbers in computations.  */
typedef int yy_state_fast_t;

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(Msgid) dgettext ("bison-runtime", Msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(Msgid) Msgid
# endif
#endif

#ifndef N_
# define N_(Msgid) Msgid
#endif


#ifndef YY_ATTRIBUTE_PURE
# if defined __GNUC__ && 2 < __GNUC__ + (96 <= __GNUC_MINOR__)
#  define YY_ATTRIBUTE_PURE __attribute__ ((__pure__))
# else
#  define YY_ATTRIBUTE_PURE
# endif
#endif

#ifndef YY_ATTRIBUTE_UNUSED
# if defined __GNUC__ && 2 < __GNUC__ + (7 <= __GNUC_MINOR__)
#  define YY_ATTRIBUTE_UNUSED __attribute__ ((__unused__))
# else
#  define YY_ATTRIBUTE_UNUSED
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(E) ((void) (E))
#else
# define YYUSE(E) /* empty */
#endif

#if defined __GNUC__ && ! defined __ICC && 407 <= __GNUC__ * 100 + __GNUC_MINOR__
/* Suppress an incorrect diagnostic about yylval being uninitialized.  */
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN                            \
    _Pragma ("GCC diagnostic push")                                     \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")              \
    _Pragma ("GCC diagnostic ignored \"-Wmaybe-uninitialized\"")
# define YY_IGNORE_MAYBE_UNINITIALIZED_END      \
    _Pragma ("GCC diagnostic pop")
#else
# define YY_INITIAL_VALUE(Value) Value
#endif
#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
# define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif

#if defined __cplusplus && defined __GNUC__ && ! defined __ICC && 6 <= __GNUC__
# define YY_IGNORE_USELESS_CAST_BEGIN                          \
    _Pragma ("GCC diagnostic push")                            \
    _Pragma ("GCC diagnostic ignored \"-Wuseless-cast\"")
# define YY_IGNORE_USELESS_CAST_END            \
    _Pragma ("GCC diagnostic pop")
#endif
#ifndef YY_IGNORE_USELESS_CAST_BEGIN
# define YY_IGNORE_USELESS_CAST_BEGIN
# define YY_IGNORE_USELESS_CAST_END
#endif


#define YY_ASSERT(E) ((void) (0 && (E)))

#if 1

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's 'empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined YYMALLOC || defined malloc) \
             && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined EXIT_SUCCESS
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
# define YYCOPY_NEEDED 1
#endif /* 1 */

#if (! defined yyoverflow \
     && (! defined __cplusplus \
         || (defined PKL_TAB_LTYPE_IS_TRIVIAL && PKL_TAB_LTYPE_IS_TRIVIAL \
             && defined PKL_TAB_STYPE_IS_TRIVIAL && PKL_TAB_STYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yy_state_t yyss_alloc;
  YYSTYPE yyvs_alloc;
  YYLTYPE yyls_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (YYSIZEOF (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (YYSIZEOF (yy_state_t) + YYSIZEOF (YYSTYPE) \
             + YYSIZEOF (YYLTYPE)) \
      + 2 * YYSTACK_GAP_MAXIMUM)

# define YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)                           \
    do                                                                  \
      {                                                                 \
        YYPTRDIFF_T yynewbytes;                                         \
        YYCOPY (&yyptr->Stack_alloc, Stack, yysize);                    \
        Stack = &yyptr->Stack_alloc;                                    \
        yynewbytes = yystacksize * YYSIZEOF (*Stack) + YYSTACK_GAP_MAXIMUM; \
        yyptr += yynewbytes / YYSIZEOF (*yyptr);                        \
      }                                                                 \
    while (0)

#endif

#if defined YYCOPY_NEEDED && YYCOPY_NEEDED
/* Copy COUNT objects from SRC to DST.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(Dst, Src, Count) \
      __builtin_memcpy (Dst, Src, YY_CAST (YYSIZE_T, (Count)) * sizeof (*(Src)))
#  else
#   define YYCOPY(Dst, Src, Count)              \
      do                                        \
        {                                       \
          YYPTRDIFF_T yyi;                      \
          for (yyi = 0; yyi < (Count); yyi++)   \
            (Dst)[yyi] = (Src)[yyi];            \
        }                                       \
      while (0)
#  endif
# endif
#endif /* !YYCOPY_NEEDED */

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  82
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   3228

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  137
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  79
/* YYNRULES -- Number of rules.  */
#define YYNRULES  274
/* YYNSTATES -- Number of states.  */
#define YYNSTATES  478

#define YYMAXUTOK   366


/* YYTRANSLATE(TOKEN-NUM) -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex, with out-of-bounds checking.  */
#define YYTRANSLATE(YYX)                                \
  (0 <= (YYX) && (YYX) <= YYMAXUTOK                     \
   ? YY_CAST (yysymbol_kind_t, yytranslate[YYX])        \
   : YYSYMBOL_YYUNDEF)

/* YYTRANSLATE[TOKEN-NUM] -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex.  */
static const yytype_uint8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,   133,     2,     2,     2,   100,    86,     2,
     128,   129,    97,    95,   126,    96,   109,    98,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,   119,   127,
      91,   136,    92,   118,   103,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,   134,     2,   135,    85,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,   130,    84,   131,   132,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    56,    57,    58,    59,    60,    61,    62,    63,    64,
      65,    66,    67,    68,    69,    70,    71,    72,    73,    74,
      75,    76,    77,    78,    79,    80,    81,    82,    83,    87,
      88,    89,    90,    93,    94,    99,   101,   102,   104,   105,
     106,   107,   108,   110,   111,   112,   113,   114,   115,   116,
     117,   120,   121,   122,   123,   124,   125
};

#if PKL_TAB_DEBUG
  /* YYRLINEYYN -- Source line where rule number YYN was defined.  */
static const yytype_int16 yyrline[] =
{
       0,   550,   550,   565,   571,   578,   584,   590,   596,   606,
     612,   618,   627,   631,   635,   636,   646,   647,   648,   652,
     686,   726,   727,   735,   737,   738,   745,   746,   750,   751,
     757,   762,   769,   775,   781,   787,   793,   798,   803,   809,
     815,   821,   827,   833,   839,   845,   851,   857,   863,   869,
     875,   881,   887,   893,   898,   903,   909,   923,   943,   958,
     973,   979,   985,   986,   990,   999,  1000,  1004,  1010,  1019,
    1020,  1021,  1022,  1023,  1027,  1056,  1062,  1069,  1075,  1081,
    1089,  1090,  1096,  1101,  1107,  1113,  1119,  1125,  1131,  1132,
    1137,  1136,  1151,  1157,  1166,  1175,  1177,  1178,  1185,  1194,
    1195,  1199,  1201,  1202,  1209,  1216,  1227,  1238,  1239,  1246,
    1252,  1265,  1279,  1289,  1302,  1303,  1310,  1320,  1346,  1347,
    1355,  1356,  1357,  1361,  1376,  1384,  1389,  1394,  1395,  1396,
    1397,  1398,  1402,  1403,  1404,  1408,  1419,  1420,  1424,  1455,
    1466,  1472,  1480,  1487,  1497,  1498,  1505,  1511,  1518,  1537,
    1557,  1555,  1617,  1618,  1622,  1623,  1627,  1628,  1632,  1633,
    1634,  1636,  1641,  1642,  1643,  1648,  1647,  1756,  1757,  1761,
    1765,  1773,  1778,  1784,  1790,  1798,  1809,  1813,  1825,  1826,
    1827,  1832,  1831,  1894,  1898,  1899,  1903,  1904,  1909,  1931,
    1932,  1937,  1961,  1962,  1967,  2006,  2014,  2022,  2033,  2034,
    2035,  2036,  2037,  2038,  2039,  2040,  2041,  2042,  2043,  2044,
    2045,  2046,  2047,  2048,  2049,  2050,  2051,  2052,  2056,  2057,
    2059,  2060,  2065,  2066,  2067,  2068,  2069,  2070,  2071,  2072,
    2073,  2074,  2075,  2079,  2080,  2081,  2086,  2092,  2103,  2109,
    2115,  2121,  2130,  2137,  2144,  2153,  2154,  2159,  2163,  2169,
    2175,  2191,  2211,  2229,  2228,  2282,  2281,  2334,  2339,  2344,
    2350,  2356,  2362,  2368,  2378,  2389,  2395,  2401,  2407,  2419,
    2423,  2434,  2443,  2444,  2451
};
#endif

/** Accessing symbol of state STATE.  */
#define YY_ACCESSING_SYMBOL(State) YY_CAST (yysymbol_kind_t, yystos[State])

#if 1
/* The user-facing name of the symbol whose (internal) number is
   YYSYMBOL.  No bounds checking.  */
static const char *yysymbol_name (yysymbol_kind_t yysymbol) YY_ATTRIBUTE_UNUSED;

static const char *
yysymbol_name (yysymbol_kind_t yysymbol)
{
  static const char *const yy_sname[] =
  {
  N_("end of file"), N_("error"), N_("invalid token"),
  N_("integer literal"), "INTEGER_OVERFLOW", N_("character literal"),
  N_("string"), N_("identifier"), N_("type name"), N_("offset unit"),
  N_("keyword `enum'"), N_("keyword `pinned'"), N_("keyword `struct'"),
  "token", N_("keyword `union'"), N_("keyword `const'"),
  N_("keyword `continue'"), N_("keyword `else'"), N_("keyword `if'"),
  N_("keyword `while"), N_("keyword `until'"), N_("keyword `for'"),
  N_("keyword `in'"), N_("keyword `where'"), N_("keyword `sizeof'"),
  N_("keyword `assert'"), N_("token"), "ALIEN", N_("int type constructor"),
  N_("uint type constructor"), N_("offset type constructor"),
  N_("keyword `fun'"), N_("keyword `defset'"), N_("keyword `type'"),
  N_("keyword `var'"), N_("keyword `unit'"), N_("keyword `method'"),
  N_("keyword `return'"), N_("keyword `break'"),
  N_("string type specifier"), N_("keyword `try'"), N_("keyword `catch'"),
  N_("keyword `raise'"), N_("void type specifier"),
  N_("any type specifier"), N_("keyword `print'"), N_("keyword `printf'"),
  N_("keyword `load'"), N_("keyword `lambda'"), "BUILTIN_RAND",
  "BUILTIN_GET_ENDIAN", "BUILTIN_SET_ENDIAN", "BUILTIN_GET_IOS",
  "BUILTIN_SET_IOS", "BUILTIN_OPEN", "BUILTIN_CLOSE", "BUILTIN_IOSIZE",
  "BUILTIN_GETENV", "BUILTIN_FORGET", "BUILTIN_GET_TIME", "BUILTIN_STRACE",
  "BUILTIN_TERM_RGB_TO_COLOR", "BUILTIN_TERM_GET_COLOR",
  "BUILTIN_TERM_SET_COLOR", "BUILTIN_TERM_GET_BGCOLOR",
  "BUILTIN_TERM_SET_BGCOLOR", "BUILTIN_TERM_BEGIN_CLASS",
  "BUILTIN_TERM_END_CLASS", "BUILTIN_TERM_BEGIN_HYPERLINK",
  "BUILTIN_TERM_END_HYPERLINK", N_("power-and-assign operator"),
  N_("multiply-and-assign operator"), N_("divide-and-assing operator"),
  N_("modulus-and-assign operator"), N_("add-and-assing operator"),
  N_("subtract-and-assign operator"), N_("shift-left-and-assign operator"),
  N_("shift-right-and-assign operator"), N_("bit-and-and-assign operator"),
  N_("bit-xor-and-assign operator"), N_("bit-or-and-assign operator"),
  N_("range separator"), N_("logical or operator"),
  N_("logical and operator"), N_("bit-wise or operator"),
  N_("bit-wise xor operator"), N_("bit-wise and operator"),
  N_("equality operator"), N_("inequality operator"),
  N_("less-or-equal operator"), N_("bigger-or-equal-than operator"),
  N_("less-than operator"), N_("bigger-than operator"),
  N_("left shift operator"), N_("right shift operator"),
  N_("addition operator"), N_("subtraction operator"),
  N_("multiplication operator"), N_("division operator"),
  N_("ceiling division operator"), N_("modulus operator"),
  N_("power operator"), N_("bit-concatenation operator"),
  N_("map operator"), N_("non-strict map operator"),
  N_("increment operator"), N_("decrement operator"), N_("cast operator"),
  N_("type identification operator"), N_("dot operator"), N_("attribute"),
  N_("unmap operator"), N_("keyword `big'"), N_("keyword `little'"),
  N_("keyword `signed'"), N_("keyword `unsigned'"),
  N_("varargs indicator"), "THEN", "'?'", "':'", "UNARY", "HYPERUNARY",
  "START_EXP", "START_DECL", "START_STMT", "START_PROGRAM", "','", "';'",
  "'('", "')'", "'{'", "'}'", "'~'", "'!'", "'['", "']'", "'='", "$accept",
  "pushlevel", "start", "program", "program_elem_list", "program_elem",
  "load", "identifier", "expression_list", "expression_opt", "expression",
  "bconc", "mapop", "map", "unary_operator", "primary", "$@1", "funcall",
  "funcall_arg_list", "funcall_arg", "opt_comma", "struct_field_list",
  "struct_field", "array", "array_initializer_list", "array_initializer",
  "pushlevel_args", "function_specifier", "function_arg_list",
  "function_arg", "function_arg_initial", "type_specifier", "typename",
  "string_type_specifier", "simple_type_specifier", "cons_type_specifier",
  "integral_type_specifier", "integral_type_sign", "offset_type_specifier",
  "array_type_specifier", "function_type_specifier",
  "function_type_arg_list", "function_type_arg", "struct_type_specifier",
  "$@2", "struct_or_union", "struct_type_pinned", "integral_struct",
  "struct_type_elem_list", "endianness", "struct_type_field", "$@3",
  "struct_type_field_identifier", "struct_type_field_label",
  "struct_type_field_constraint_and_init", "struct_type_field_optcond",
  "simple_declaration", "declaration", "@4", "defun_or_method",
  "defvar_list", "defvar", "deftype_list", "deftype", "defunit_list",
  "defunit", "comp_stmt", "builtin", "stmt_decl_list", "ass_exp_op",
  "simple_stmt_list", "simple_stmt", "stmt", "@5", "@6",
  "print_stmt_arg_list", "funcall_stmt", "funcall_stmt_arg_list",
  "funcall_stmt_arg", YY_NULLPTR
  };
  /* YYTRANSLATABLE[SYMBOL-NUM] -- Whether YY_SNAME[SYMBOL-NUM] is
     internationalizable.  */
  static yytype_int8 yytranslatable[] =
  {
       1,     1,     1,     1,     0,     1,     1,     1,     1,     1,
       1,     1,     1,     0,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     0,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0
  };
  return (yysymbol < YYNTOKENS && yytranslatable[yysymbol]
          ? _(yy_sname[yysymbol])
          : yy_sname[yysymbol]);
}
#endif

#ifdef YYPRINT
/* YYTOKNUM[NUM] -- (External) token number corresponding to the
   (internal) symbol number NUM (which must be that of a token).  */
static const yytype_int16 yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290,   291,   292,   293,   294,
     295,   296,   297,   298,   299,   300,   301,   302,   303,   304,
     305,   306,   307,   308,   309,   310,   311,   312,   313,   314,
     315,   316,   317,   318,   319,   320,   321,   322,   323,   324,
     325,   326,   327,   328,   329,   330,   331,   332,   333,   334,
     335,   336,   337,   338,   124,    94,    38,   339,   340,   341,
     342,    60,    62,   343,   344,    43,    45,    42,    47,   345,
      37,   346,   347,    64,   348,   349,   350,   351,   352,    46,
     353,   354,   355,   356,   357,   358,   359,   360,    63,    58,
     361,   362,   363,   364,   365,   366,    44,    59,    40,    41,
     123,   125,   126,    33,    91,    93,    61
};
#endif

#define YYPACT_NINF (-379)

#define yypact_value_is_default(Yyn) \
  ((Yyn) == YYPACT_NINF)

#define YYTABLE_NINF (-256)

#define yytable_value_is_error(Yyn) \
  ((Yyn) == YYTABLE_NINF)

  /* YYPACTSTATE-NUM -- Index in YYTABLE of the portion describing
     STATE-NUM.  */
static const yytype_int16 yypact[] =
{
     109,  1694,    52,  1205,   659,    36,  -379,  -379,  -379,  -379,
    -379,  -379,  -379,   -86,  -379,  -379,   345,  -379,  -379,  -379,
    -379,  -379,  -379,  1694,  1694,  -379,  1694,  -379,  -379,  1391,
    2606,  -379,  -379,  1694,   -82,  -379,  -379,    -6,    10,   -70,
      21,  -379,    50,  -379,    24,  -379,    89,    89,    89,  -379,
      14,    29,    89,    47,    60,    61,    62,    68,  1523,    51,
    1337,  1531,  1694,     0,   126,  -379,  3098,    70,  2794,    67,
      72,   517,  -379,    77,    82,  -379,  -379,   794,  -379,  -379,
    -379,  -379,  -379,   345,  -379,  -379,   -78,  -379,    37,   139,
     139,  2166,   -99,    83,    79,  2794,    85,  -379,  -379,  1694,
    1694,  1694,  1694,  1694,  1694,  1694,  1694,  1694,  1694,  1694,
    1694,  1694,  1694,  1694,  1694,  1694,  1694,  1694,  1694,  1694,
    1694,  -379,  -379,   345,   345,  -379,  1694,  -379,    59,    89,
    1694,  1577,  1740,  -379,  -379,   840,  1694,  1694,   124,  -379,
    -379,    81,    95,  -379,    88,    96,  -379,    90,    99,  -379,
    -379,  -379,  -379,  -379,  1694,  1694,    -4,  1694,  -379,  2418,
    -379,   -11,  -379,  2466,  2512,  -379,   223,   115,   121,  -379,
    -379,  -379,  -379,  -379,  -379,  -379,  -379,  -379,  -379,  -379,
    -379,  -379,  -379,  -379,  -379,  -379,  -379,  -379,  -379,  1027,
    -379,  -379,  1694,  1694,  -379,  -379,  -379,  -379,  -379,  -379,
    -379,  -379,  -379,  -379,  -379,   229,  1694,  1694,   131,  -379,
    -379,  -379,  -379,   -66,    86,  -379,  -379,   -75,  -379,  -379,
    1694,  1391,   117,  2915,  2842,  2888,  2942,  2982,  3009,  3036,
    3036,  2860,  2860,  2860,  2860,   651,   651,   184,   184,    75,
      75,    75,    75,   133,   166,   120,   120,  2654,  -379,  2794,
    -122,  -379,   848,  1794,   128,   -98,   132,  2794,   130,  -379,
    -379,  1848,  2700,   134,  2794,  -379,   129,    89,  1694,    89,
    1694,    89,   141,  2218,  2266,   240,  1694,   127,  2118,  -379,
    1694,    19,  -379,  -379,    44,  -379,  -379,  -379,  -379,  -379,
    1159,  -379,  2794,  2794,  1694,  2794,  2794,  -379,  -379,   173,
     186,   320,  -379,  1902,  -379,  -379,  1694,  1694,  -379,  -379,
    1956,  1694,   981,  -379,  1694,  1740,   149,  -379,  1694,  1694,
     168,    11,   252,  -379,   120,  -379,  -379,  -379,  2794,  -379,
    2794,  -379,    37,  1337,  1337,  1694,   143,  2794,   180,  1694,
    -379,  2560,  1694,  -379,  -379,  1694,  -379,   -55,  -379,  -379,
    -379,  2794,  -379,  -379,   182,   170,   183,     5,  -379,   179,
    2794,  -379,  -379,  2010,  -379,  2064,  2794,  -379,  -379,  2794,
    2794,  -379,  -379,   345,  -103,   187,   193,  -379,   172,  -379,
     304,  -379,  2794,  1383,  1694,  2318,  -379,  2794,   320,  2794,
    -379,  -379,   345,   320,   188,  -379,  1694,  -379,  -379,   120,
    -379,   345,   171,  -379,  -379,   345,  1337,   299,   195,   -47,
    -379,   199,  -379,  -379,   200,   -72,  -379,  1694,  -379,  2794,
     120,  -379,   120,   203,  -379,   201,   314,  1383,  1337,  1383,
    -379,  -379,  2794,   208,  1337,  1694,  -379,  -379,   -34,  -379,
    -379,  -379,   449,  -379,  2366,  1337,  -379,  -379,   374,   129,
    -379,  -379,  1337,  -379,  -379,  -379,  -379,    89,  -379,  -379,
    -379,   -93,  1694,  1694,   239,   626,  2748,  1694,   322,  1694,
    1694,  2794,  1694,   216,  2794,  2794,  2794,  -379
};

  /* YYDEFACTSTATE-NUM -- Default reduction number in state STATE-NUM.
     Performed when YYTABLE does not specify something else to do.  Zero
     means the default is an error.  */
static const yytype_int16 yydefact[] =
{
       0,     0,     0,     2,     2,     0,    75,    76,    77,    78,
      74,   123,    58,     0,   136,   137,     0,   124,   126,   125,
      90,    70,    69,     0,     0,    73,     0,    71,    72,     0,
       3,    62,    63,     0,    28,    88,    80,   127,   131,     0,
       0,   128,     0,   129,   130,   184,     0,     0,     0,   185,
       0,     5,     0,     0,     0,     0,     0,     0,     0,     0,
       2,     0,     0,     0,     0,   246,     0,     9,   240,    62,
      63,    28,   245,     0,     7,   244,    11,     2,    14,    18,
      16,    17,     1,     0,   127,   131,     0,   130,     0,    60,
      61,     0,    28,     0,     0,   109,    99,   107,    59,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,    92,    93,     0,     0,    31,     0,     4,    29,     0,
      95,     0,   101,    65,    66,     0,     0,    23,     0,    22,
      21,     0,   179,   189,     0,   178,   186,     0,   180,   192,
     183,     6,   181,   258,     0,     0,     2,     0,   259,     0,
     257,     0,   265,     0,     0,   269,     0,     0,     0,   198,
     199,   200,   201,   202,   203,   204,   205,   206,   207,   208,
     209,   210,   211,   212,   213,   214,   215,   216,   217,     2,
     197,    10,     0,     0,   222,   223,   224,   225,   226,   227,
     228,   229,   230,   232,   231,     0,     0,     0,   271,   272,
     247,     8,    15,     0,     0,   111,    91,     0,    79,    89,
       0,   100,     0,    52,    51,    50,    47,    48,    49,    41,
      42,    45,    46,    43,    44,    39,    40,    32,    33,    34,
      35,    36,    38,    37,    64,    53,    54,     0,    81,    98,
       0,    96,     0,     0,    74,   123,     0,   104,    99,   102,
     140,     0,    67,    99,    24,   135,     2,     0,     0,     0,
       0,     0,     0,     0,     0,     0,    26,     0,     0,   260,
       0,     2,   266,   267,     0,   269,    20,    19,   195,   220,
       2,   218,   238,   239,     0,   236,   237,   273,    30,     0,
       0,     0,   111,     0,   108,   106,     0,     0,    94,    85,
       0,     0,     0,    82,     0,   100,     0,   141,     0,   100,
       0,     0,   154,   191,   120,   122,   121,   190,   188,   187,
     194,   193,     0,     2,     2,     0,     0,    27,     0,     0,
     242,     0,     0,     2,   261,     0,   268,     0,   196,   221,
     219,   274,   139,   138,     0,     0,   114,     0,     2,     0,
      55,    97,    86,     0,    87,     0,   105,   103,    57,    68,
      25,    56,   148,     0,   146,     0,   144,   155,     0,   182,
     248,   250,     2,   233,    26,     0,   264,     2,     0,   270,
     241,   117,     0,     0,   118,   113,     0,    83,    84,   143,
     147,     0,     0,   152,   153,   156,     2,   253,     0,     0,
     234,     0,   243,   262,     0,     0,   115,     0,   116,   110,
     142,   145,   157,     0,   249,     0,     0,     0,     2,   233,
       2,     2,   119,   150,     2,     0,   235,   252,     0,   263,
     112,   149,   162,   254,     0,     2,   164,   163,   162,     2,
     158,   159,     2,   251,   151,   161,   160,   167,   256,   168,
     165,   171,     0,     0,   169,   172,   173,     0,   176,     0,
       0,   170,     0,     0,   175,   174,   177,   166
};

  /* YYPGOTONTERM-NUM.  */
static const yytype_int16 yypgoto[] =
{
    -379,  -145,  -379,  -379,  -379,   268,   351,    98,  -379,   -29,
      -1,    13,  -379,    66,  -379,    -2,  -379,  -379,  -379,    55,
    -186,  -379,    42,  -379,  -379,   137,    64,    38,   -25,   -17,
    -379,   -77,   135,   178,   212,  -379,  -379,  -379,  -379,   237,
    -379,   -26,  -379,  -379,  -379,  -379,  -379,  -379,  -379,  -379,
     -71,  -379,  -379,  -379,  -379,  -379,   101,     6,  -379,  -379,
    -379,   110,  -379,   113,  -379,   111,  -267,  -379,  -379,  -379,
     -44,  -378,    18,  -379,  -379,   102,   360,  -379,   185
};

  /* YYDEFGOTONTERM-NUM.  */
static const yytype_int16 yydefgoto[] =
{
      -1,    66,     5,    76,    77,    78,    79,   354,   263,   336,
      68,    31,   136,    32,    33,    34,    88,    35,   250,   251,
     222,   258,   259,    36,    96,    97,   301,   216,   355,   356,
     418,   323,    37,    38,    39,    40,    41,    42,    43,    44,
     325,   375,   376,   326,   442,   405,   378,   423,   448,   449,
     450,   461,   460,   468,   464,   473,    50,    80,   272,    52,
     145,   146,   142,   143,   148,   149,    72,   190,   290,   207,
     409,    73,    81,   425,   426,   284,    75,   208,   209
};

  /* YYTABLEYYPACT[STATE-NUM] -- What to do in state STATE-NUM.  If
     positive, shift that token.  If negative, reduce the rule whose
     number is the opposite.  If YYTABLE_NINF, syntax error.  */
static const yytype_int16 yytable[] =
{
      30,    71,    71,   275,   307,   410,   165,   308,    51,   280,
     129,   277,   139,   140,   344,   400,    69,    69,   -21,    11,
     205,    74,    89,    90,    92,    91,   462,   129,    95,   130,
     281,   135,   128,   133,   134,   131,    82,   342,   -21,    14,
      15,    16,    83,   463,   302,    11,   130,   431,   214,   436,
      17,   410,   131,   138,    18,    19,   135,   159,    71,   135,
     163,   164,   135,   298,   135,    14,    15,    16,   135,    70,
      70,   345,   316,    69,   390,    71,    17,   320,   161,   427,
      18,    19,   428,    45,    98,    46,    47,    48,    49,   299,
      69,   395,   427,   139,   140,   445,   139,   140,   223,   224,
     225,   226,   227,   228,   229,   230,   231,   232,   233,   234,
     235,   236,   237,   238,   239,   240,   241,   242,   243,   244,
     413,   322,  -132,   276,   132,   247,    70,   372,   166,   249,
     253,   257,   167,   168,   261,   262,   264,    11,  -134,   135,
     373,   150,    98,    70,   141,   144,   147,   343,  -256,   137,
     152,    84,  -133,   273,   274,   151,   278,    14,    15,    16,
      46,    47,    48,   439,   440,   215,   123,   124,    17,   125,
     345,   346,    18,    19,   153,    98,   119,   120,   160,    11,
     121,   122,   123,   124,   403,   125,   404,    71,   154,   155,
     156,   292,   293,    98,    85,   289,   157,   191,   388,    14,
      15,    16,    69,   192,   210,   295,   296,   291,   193,   211,
      17,   221,   219,   220,    18,    19,   265,   266,    84,   303,
      95,   267,   269,    84,   268,   271,   270,   248,    86,   285,
     256,     1,     2,     3,     4,   120,   294,   407,   121,   122,
     123,   124,   286,   125,  -256,  -256,   123,   124,   287,   125,
     205,   310,   305,    87,   135,    70,   315,   321,    84,    84,
     319,    85,   335,   377,   -22,   352,    85,   328,   314,   330,
     383,   121,   122,   123,   124,   337,   125,   332,   353,   341,
     368,   115,   116,   117,   118,   119,   120,   372,    71,   121,
     122,   123,   124,   351,   125,   213,   349,   371,   391,   392,
     217,    85,    85,    69,   322,   360,   249,   384,   350,   393,
     363,   365,   300,   366,   257,   396,   401,   369,   370,   402,
      87,   406,  -255,   166,   417,    87,   429,   139,   255,   430,
     434,    71,    71,   433,   382,   245,   246,   435,   385,   441,
     472,   387,   467,   477,   389,   212,    69,    69,    14,    15,
      16,   380,   381,    11,    67,   411,    70,   367,   304,    17,
      87,    87,   361,    18,    19,   141,   358,   144,   416,   147,
     379,   414,   457,    14,    15,    16,   421,   455,   338,   329,
     327,    71,   331,   337,    17,   438,    93,   347,    18,    19,
       0,     0,     0,   297,     0,   419,    69,     0,     0,    70,
      70,    84,     0,     0,    71,    45,     0,    46,    47,    48,
      49,     0,     0,   256,     0,     0,   432,     0,     0,    69,
       0,     0,     0,     0,   424,    71,    71,    71,     0,     0,
       0,     0,    71,     0,   444,     0,    84,     0,     0,     0,
      69,    69,    69,    71,    85,     0,   437,    69,   451,    70,
      71,     0,   443,     0,   456,   394,    84,     0,    69,     0,
       0,   465,   466,   453,     0,    69,   471,    84,   474,   475,
     458,   476,    70,     0,     0,     0,     0,     0,   324,    85,
      45,     0,    46,    47,    48,    49,   446,   447,     0,     0,
       0,     0,     0,    70,    70,    70,     0,     0,     0,    85,
      70,     0,     0,    87,     0,   454,     0,     0,    84,     0,
      85,    70,     0,   357,     0,     0,     0,     0,    70,     0,
       0,     0,     0,    84,     0,     0,     0,    84,    84,     0,
       0,     0,     0,   374,     0,     0,    84,    84,    87,     0,
      84,     0,     0,     0,   217,     0,     0,     0,     0,     0,
       0,    85,     0,     0,     0,   459,     0,     0,    87,     0,
       0,   446,   447,     0,     0,     0,    85,     0,     0,    87,
      85,    85,     0,     0,     0,     0,     0,     0,     0,    85,
      85,     0,     0,    85,    84,   399,     0,   194,   195,   196,
     197,   198,   199,   200,   201,   202,   203,   204,     0,     0,
     357,     0,     0,     0,   415,   357,     0,     0,     0,     0,
      87,     0,     0,   420,   374,     0,     0,   422,     0,     0,
       0,     0,     0,     0,     0,    87,   129,    85,     0,    87,
      87,     0,     0,     0,     0,    98,   205,     0,    87,    87,
       0,     0,    87,     0,     0,   130,     0,     0,    99,     0,
       0,   131,     0,   206,     0,     0,     0,     0,     0,   -12,
      98,   324,     6,     7,     8,     9,    10,    11,    12,     0,
       0,     0,     0,     0,     0,    53,     0,    54,    55,     0,
      56,     0,     0,    13,    57,     0,    87,    14,    15,    16,
      45,     0,    46,    47,    48,    49,    58,    59,    17,    60,
       0,    61,    18,    19,    62,    63,    64,    20,   100,   101,
     102,   103,   104,   105,   106,   107,   108,   109,   110,   111,
     112,   113,   114,   115,   116,   117,   118,   119,   120,     0,
       0,   121,   122,   123,   124,     0,   125,     0,     0,     0,
       0,     0,     0,     0,   126,     0,   113,   114,   115,   116,
     117,   118,   119,   120,    21,    22,   121,   122,   123,   124,
       0,   125,   469,     0,    23,    24,     0,     0,     0,     0,
      25,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,    65,    26,     0,     0,
       0,    27,    28,    29,   -13,     0,     0,     6,     7,     8,
       9,    10,    11,    12,     0,     0,     0,     0,     0,     0,
      53,     0,    54,    55,     0,    56,     0,     0,    13,    57,
       0,     0,    14,    15,    16,    45,     0,    46,    47,    48,
      49,    58,    59,    17,    60,     0,    61,    18,    19,    62,
      63,    64,    20,     6,     7,     8,     9,    10,    11,    12,
       0,     6,     7,     8,     9,    10,    11,    12,     0,     0,
       0,     0,     0,     0,    13,     0,     0,     0,    14,    15,
      16,     0,    13,     0,     0,     0,    14,    15,    16,    17,
       0,     0,     0,    18,    19,     0,     0,    17,    20,    21,
      22,    18,    19,     0,     0,     0,    20,     0,     0,    23,
      24,     0,     0,     0,     0,    25,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,    65,    26,     0,     0,     0,    27,    28,    29,     0,
       0,     0,     0,     0,     0,    21,    22,     0,     0,     0,
       0,     0,     0,    21,    22,    23,    24,     0,     0,     0,
       0,    25,     0,    23,    24,     0,     0,     0,     0,    25,
       0,     0,     0,     0,     0,     0,     0,     0,    26,     0,
       0,     0,    27,    28,    29,   260,    26,     0,     0,     0,
      27,    28,    29,   309,     6,     7,     8,     9,    10,    11,
      12,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,    13,     0,     0,     0,    14,
      15,    16,     0,     0,     0,     0,     0,     0,     0,     0,
      17,     0,     0,     0,    18,    19,     0,     0,     0,    20,
       6,     7,     8,     9,    10,    11,    12,     0,     0,     0,
       0,     0,     0,    53,     0,    54,    55,     0,    56,     0,
       0,    13,    57,     0,     0,    14,    15,    16,    45,     0,
      46,    47,    48,    49,    58,    59,    17,    60,     0,    61,
      18,    19,    62,    63,     0,    20,    21,    22,     0,     0,
       0,     0,     0,     0,     0,     0,    23,    24,     0,     0,
       0,     0,    25,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,    26,
       0,     0,     0,    27,    28,    29,   364,     0,     0,     0,
       0,     0,    21,    22,     0,     0,     0,     0,     0,     0,
       0,     0,    23,    24,     0,     0,     0,     0,    25,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,    65,    26,     0,     0,   288,    27,
      28,    29,     6,     7,     8,     9,    10,    11,    12,     0,
       0,     0,     0,     0,     0,    53,     0,    54,    55,     0,
      56,     0,     0,    13,    57,     0,     0,    14,    15,    16,
      45,     0,    46,    47,    48,    49,    58,    59,    17,    60,
       0,    61,    18,    19,    62,    63,     0,    20,     6,     7,
       8,     9,    10,    11,    12,     0,     0,     0,     0,     0,
       0,    53,     0,    54,    55,     0,    56,     0,     0,    13,
      57,     0,     0,    14,    15,    16,     0,     0,     0,     0,
       0,     0,    58,    59,    17,    60,     0,    61,    18,    19,
      62,    63,    64,    20,    21,    22,     0,     0,     0,     0,
       0,     0,     0,     0,    23,    24,     0,     0,     0,     0,
      25,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,    65,    26,     0,     0,
     348,    27,    28,    29,     0,     0,     0,     0,     0,     0,
      21,    22,     0,     0,     0,     0,     0,     0,     0,     0,
      23,    24,     0,     0,     0,     0,    25,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,    65,    26,     0,     0,     0,    27,    28,    29,
       6,     7,     8,     9,    10,    11,    12,     0,     0,     0,
       0,     0,     0,    53,     0,    54,    55,     0,    56,     0,
       0,    13,    57,     0,     0,    14,    15,    16,     0,     0,
       0,     0,     0,     0,    58,    59,    17,    60,     0,    61,
      18,    19,    62,    63,     0,    20,     6,     7,     8,     9,
      10,    11,    12,     0,     6,     7,     8,     9,    10,    11,
      12,     0,     0,     0,     0,     0,     0,    13,    57,     0,
       0,    14,    15,    16,     0,    13,     0,     0,     0,    14,
      15,    16,    17,     0,     0,     0,    18,    19,     0,   408,
      17,    20,    21,    22,    18,    19,     0,     0,     0,    20,
       0,     0,    23,    24,     0,     0,     0,     0,    25,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,    65,    26,     0,     0,     0,    27,
      28,    29,     0,     0,     0,     0,     0,     0,    21,    22,
       0,     0,     0,     0,     0,     0,    21,    22,    23,    24,
       0,     0,     0,     0,    25,     0,    23,    24,     0,     0,
      94,     0,    25,     0,     0,     0,     0,     0,     0,     0,
       0,    26,     0,     0,     0,    27,    28,    29,     0,    26,
       0,     0,     0,    27,    28,    29,     6,     7,     8,     9,
      10,    11,    12,     0,     6,     7,     8,     9,    10,    11,
      12,     0,     0,     0,     0,     0,     0,    13,     0,     0,
       0,    14,    15,    16,     0,    13,     0,     0,     0,    14,
      15,    16,    17,     0,     0,     0,    18,    19,     0,     0,
      17,    20,     0,     0,    18,    19,     0,     0,     0,    20,
       6,     7,     8,     9,    10,    11,    12,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,    13,     0,     0,     0,    14,    15,    16,     0,     0,
       0,     0,     0,     0,     0,     0,    17,     0,    21,    22,
      18,    19,     0,     0,     0,    20,    21,    22,    23,    24,
       0,     0,     0,     0,    25,     0,    23,    24,     0,     0,
       0,     0,    25,     0,     0,     0,     0,     0,     0,     0,
     158,    26,     0,     0,     0,    27,    28,    29,   162,    26,
       0,     0,     0,    27,    28,    29,     0,     0,     0,     0,
       0,     0,    21,    22,     0,     0,     0,     0,     0,     0,
       0,     0,    23,    24,     0,     0,     0,     0,    25,     0,
       0,     0,     0,     0,     0,     0,   252,     6,     7,     8,
       9,    10,    11,    12,     0,    26,     0,     0,     0,    27,
      28,    29,     0,     0,     0,     0,     0,     0,    13,     0,
       0,     0,    14,    15,    16,     0,     0,     0,     0,     0,
       0,     0,     0,    17,     0,     0,     0,    18,    19,     0,
       0,     0,    20,     6,     7,     8,     9,   254,   255,    12,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,    13,     0,     0,     0,    14,    15,
      16,     0,     0,     0,     0,     0,     0,     0,     0,    17,
       0,     0,     0,    18,    19,     0,     0,     0,    20,    21,
      22,     0,     0,     0,     0,     0,     0,     0,     0,    23,
      24,     0,     0,    98,     0,    25,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,    99,     0,     0,     0,
       0,     0,    26,     0,     0,     0,    27,    28,    29,     0,
       0,     0,     0,     0,     0,    21,    22,     0,     0,     0,
       0,     0,     0,     0,     0,    23,    24,     0,     0,     0,
       0,    25,     0,     0,     0,     0,     0,    98,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,    26,     0,
      99,     0,    27,    28,    29,   311,   100,   101,   102,   103,
     104,   105,   106,   107,   108,   109,   110,   111,   112,   113,
     114,   115,   116,   117,   118,   119,   120,     0,     0,   121,
     122,   123,   124,     0,   125,     0,     0,     0,     0,     0,
       0,    98,   126,   312,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,    99,     0,     0,     0,     0,   313,
     100,   101,   102,   103,   104,   105,   106,   107,   108,   109,
     110,   111,   112,   113,   114,   115,   116,   117,   118,   119,
     120,     0,     0,   121,   122,   123,   124,     0,   125,     0,
       0,     0,     0,     0,     0,    98,   126,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,    99,     0,
       0,     0,     0,   317,   100,   101,   102,   103,   104,   105,
     106,   107,   108,   109,   110,   111,   112,   113,   114,   115,
     116,   117,   118,   119,   120,     0,     0,   121,   122,   123,
     124,     0,   125,     0,     0,     0,     0,     0,     0,    98,
     126,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,    99,     0,     0,     0,     0,   359,   100,   101,
     102,   103,   104,   105,   106,   107,   108,   109,   110,   111,
     112,   113,   114,   115,   116,   117,   118,   119,   120,     0,
       0,   121,   122,   123,   124,     0,   125,     0,     0,     0,
       0,     0,     0,    98,   126,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,    99,     0,     0,     0,
       0,   362,   100,   101,   102,   103,   104,   105,   106,   107,
     108,   109,   110,   111,   112,   113,   114,   115,   116,   117,
     118,   119,   120,     0,     0,   121,   122,   123,   124,     0,
     125,     0,     0,     0,     0,     0,     0,    98,   126,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
      99,     0,     0,     0,     0,   397,   100,   101,   102,   103,
     104,   105,   106,   107,   108,   109,   110,   111,   112,   113,
     114,   115,   116,   117,   118,   119,   120,     0,     0,   121,
     122,   123,   124,     0,   125,    98,     0,     0,     0,     0,
       0,     0,   126,     0,     0,     0,     0,     0,    99,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   398,
     100,   101,   102,   103,   104,   105,   106,   107,   108,   109,
     110,   111,   112,   113,   114,   115,   116,   117,   118,   119,
     120,     0,     0,   121,   122,   123,   124,    98,   125,     0,
       0,     0,     0,     0,     0,     0,   126,     0,     0,     0,
      99,     0,     0,     0,   339,     0,     0,   340,   100,   101,
     102,   103,   104,   105,   106,   107,   108,   109,   110,   111,
     112,   113,   114,   115,   116,   117,   118,   119,   120,     0,
       0,   121,   122,   123,   124,    98,   125,     0,     0,     0,
       0,     0,     0,     0,   126,     0,     0,     0,    99,     0,
       0,     0,     0,     0,     0,   218,     0,     0,     0,     0,
     100,   101,   102,   103,   104,   105,   106,   107,   108,   109,
     110,   111,   112,   113,   114,   115,   116,   117,   118,   119,
     120,     0,     0,   121,   122,   123,   124,    98,   125,     0,
       0,     0,     0,     0,     0,     0,   126,     0,     0,     0,
      99,     0,     0,     0,     0,     0,     0,   333,   100,   101,
     102,   103,   104,   105,   106,   107,   108,   109,   110,   111,
     112,   113,   114,   115,   116,   117,   118,   119,   120,     0,
       0,   121,   122,   123,   124,    98,   125,     0,     0,     0,
       0,     0,     0,     0,   126,     0,     0,     0,    99,     0,
       0,     0,     0,     0,     0,   334,     0,     0,     0,     0,
     100,   101,   102,   103,   104,   105,   106,   107,   108,   109,
     110,   111,   112,   113,   114,   115,   116,   117,   118,   119,
     120,     0,     0,   121,   122,   123,   124,    98,   125,     0,
       0,     0,     0,     0,     0,     0,   126,     0,     0,     0,
      99,     0,     0,     0,     0,     0,     0,   412,   100,   101,
     102,   103,   104,   105,   106,   107,   108,   109,   110,   111,
     112,   113,   114,   115,   116,   117,   118,   119,   120,     0,
       0,   121,   122,   123,   124,    98,   125,     0,     0,     0,
       0,     0,     0,     0,   126,     0,     0,     0,    99,     0,
       0,     0,     0,     0,     0,   452,     0,     0,     0,     0,
     100,   101,   102,   103,   104,   105,   106,   107,   108,   109,
     110,   111,   112,   113,   114,   115,   116,   117,   118,   119,
     120,    98,     0,   121,   122,   123,   124,     0,   125,     0,
       0,     0,     0,     0,    99,     0,   126,     0,     0,     0,
       0,     0,     0,     0,     0,   279,     0,     0,   100,   101,
     102,   103,   104,   105,   106,   107,   108,   109,   110,   111,
     112,   113,   114,   115,   116,   117,   118,   119,   120,    98,
       0,   121,   122,   123,   124,     0,   125,     0,     0,     0,
       0,     0,    99,     0,   126,     0,     0,     0,     0,     0,
       0,     0,     0,   282,   100,   101,   102,   103,   104,   105,
     106,   107,   108,   109,   110,   111,   112,   113,   114,   115,
     116,   117,   118,   119,   120,    98,     0,   121,   122,   123,
     124,     0,   125,     0,     0,     0,     0,     0,    99,     0,
     126,     0,     0,     0,     0,     0,     0,     0,     0,   283,
       0,     0,   100,   101,   102,   103,   104,   105,   106,   107,
     108,   109,   110,   111,   112,   113,   114,   115,   116,   117,
     118,   119,   120,    98,     0,   121,   122,   123,   124,     0,
     125,     0,     0,     0,     0,     0,    99,     0,   126,     0,
       0,     0,     0,     0,     0,     0,     0,   386,   100,   101,
     102,   103,   104,   105,   106,   107,   108,   109,   110,   111,
     112,   113,   114,   115,   116,   117,   118,   119,   120,    98,
       0,   121,   122,   123,   124,     0,   125,     0,     0,     0,
       0,     0,    99,     0,   126,     0,     0,     0,     0,     0,
       0,     0,   127,     0,     0,     0,   100,   101,   102,   103,
     104,   105,   106,   107,   108,   109,   110,   111,   112,   113,
     114,   115,   116,   117,   118,   119,   120,    98,     0,   121,
     122,   123,   124,     0,   125,     0,     0,     0,     0,     0,
      99,     0,   126,   306,     0,     0,     0,     0,     0,     0,
       0,     0,   100,   101,   102,   103,   104,   105,   106,   107,
     108,   109,   110,   111,   112,   113,   114,   115,   116,   117,
     118,   119,   120,    98,     0,   121,   122,   123,   124,     0,
     125,     0,     0,     0,     0,     0,    99,     0,   126,   318,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     100,   101,   102,   103,   104,   105,   106,   107,   108,   109,
     110,   111,   112,   113,   114,   115,   116,   117,   118,   119,
     120,    98,     0,   121,   122,   123,   124,     0,   125,     0,
       0,     0,     0,     0,    99,     0,   126,   470,     0,    98,
       0,     0,     0,     0,     0,     0,   100,   101,   102,   103,
     104,   105,   106,   107,   108,   109,   110,   111,   112,   113,
     114,   115,   116,   117,   118,   119,   120,    98,     0,   121,
     122,   123,   124,     0,   125,     0,     0,     0,     0,     0,
      99,     0,   126,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,    98,   101,   102,   103,   104,   105,
     106,   107,   108,   109,   110,   111,   112,   113,   114,   115,
     116,   117,   118,   119,   120,     0,     0,   121,   122,   123,
     124,    98,   125,   111,   112,   113,   114,   115,   116,   117,
     118,   119,   120,     0,     0,   121,   122,   123,   124,     0,
     125,     0,   102,   103,   104,   105,   106,   107,   108,   109,
     110,   111,   112,   113,   114,   115,   116,   117,   118,   119,
     120,    98,     0,   121,   122,   123,   124,     0,   125,   102,
     103,   104,   105,   106,   107,   108,   109,   110,   111,   112,
     113,   114,   115,   116,   117,   118,   119,   120,    98,     0,
     121,   122,   123,   124,     0,   125,     0,   103,   104,   105,
     106,   107,   108,   109,   110,   111,   112,   113,   114,   115,
     116,   117,   118,   119,   120,    98,     0,   121,   122,   123,
     124,     0,   125,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,   104,   105,
     106,   107,   108,   109,   110,   111,   112,   113,   114,   115,
     116,   117,   118,   119,   120,     0,     0,   121,   122,   123,
     124,     0,   125,     0,     0,     0,   105,   106,   107,   108,
     109,   110,   111,   112,   113,   114,   115,   116,   117,   118,
     119,   120,     0,     0,   121,   122,   123,   124,     0,   125,
       0,     0,     0,     0,     0,   107,   108,   109,   110,   111,
     112,   113,   114,   115,   116,   117,   118,   119,   120,     0,
       0,   121,   122,   123,   124,     0,   125,   169,   170,   171,
     172,   173,   174,   175,   176,   177,   178,   179,   180,     0,
     181,   182,   183,   184,   185,   186,   187,   188,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,   189
};

static const yytype_int16 yycheck[] =
{
       1,     3,     4,     7,   126,   383,     6,   129,     2,    20,
     109,   156,     7,     8,   281,   118,     3,     4,   116,     8,
     119,     3,    23,    24,    26,    26,   119,   109,    29,   128,
      41,   134,    33,   103,   104,   134,     0,    18,   136,    28,
      29,    30,   128,   136,   119,     8,   128,   119,   126,   427,
      39,   429,   134,     3,    43,    44,   134,    58,    60,   134,
      61,    62,   134,   129,   134,    28,    29,    30,   134,     3,
       4,   126,   258,    60,   129,    77,    39,   263,    60,   126,
      43,    44,   129,    31,     9,    33,    34,    35,    36,     3,
      77,   358,   126,     7,     8,   129,     7,     8,    99,   100,
     101,   102,   103,   104,   105,   106,   107,   108,   109,   110,
     111,   112,   113,   114,   115,   116,   117,   118,   119,   120,
     387,   266,   128,   127,   130,   126,    60,   116,   128,   130,
     131,   132,     6,     7,   135,   136,   137,     8,   128,   134,
     129,   127,     9,    77,    46,    47,    48,   128,     9,   128,
      52,    16,   128,   154,   155,   126,   157,    28,    29,    30,
      33,    34,    35,   430,   431,   128,   107,   108,    39,   110,
     126,   127,    43,    44,   127,     9,   101,   102,   127,     8,
     105,   106,   107,   108,    12,   110,    14,   189,   128,   128,
     128,   192,   193,     9,    16,   189,   128,   127,   343,    28,
      29,    30,   189,   136,   127,   206,   207,   189,   136,   127,
      39,   126,   129,   134,    43,    44,    92,   136,    83,   220,
     221,   126,   126,    88,   136,   126,   136,   129,    16,     6,
     132,   122,   123,   124,   125,   102,     7,   382,   105,   106,
     107,   108,   127,   110,   105,   106,   107,   108,   127,   110,
     119,   252,   135,    16,   134,   189,   126,   128,   123,   124,
     126,    83,    22,    11,   136,    92,    88,   268,   136,   270,
     127,   105,   106,   107,   108,   276,   110,   136,    92,   280,
     131,    97,    98,    99,   100,   101,   102,   116,   290,   105,
     106,   107,   108,   294,   110,    83,   290,   129,   116,   129,
      88,   123,   124,   290,   449,   306,   307,   127,   290,   126,
     311,   312,   214,   314,   315,   136,   129,   318,   319,   126,
      83,    17,    23,   128,   136,    88,   127,     7,     8,   129,
     129,   333,   334,   130,   335,   123,   124,    23,   339,   131,
      18,   342,   103,   127,   345,    77,   333,   334,    28,    29,
      30,   333,   334,     8,     3,   384,   290,   315,   221,    39,
     123,   124,   307,    43,    44,   267,   302,   269,   393,   271,
     332,   388,   449,    28,    29,    30,   402,   448,   277,   269,
     267,   383,   271,   384,    39,   429,    26,   285,    43,    44,
      -1,    -1,    -1,   208,    -1,   396,   383,    -1,    -1,   333,
     334,   266,    -1,    -1,   406,    31,    -1,    33,    34,    35,
      36,    -1,    -1,   315,    -1,    -1,   417,    -1,    -1,   406,
      -1,    -1,    -1,    -1,   406,   427,   428,   429,    -1,    -1,
      -1,    -1,   434,    -1,   435,    -1,   301,    -1,    -1,    -1,
     427,   428,   429,   445,   266,    -1,   428,   434,   442,   383,
     452,    -1,   434,    -1,   448,   357,   321,    -1,   445,    -1,
      -1,   462,   463,   445,    -1,   452,   467,   332,   469,   470,
     452,   472,   406,    -1,    -1,    -1,    -1,    -1,   266,   301,
      31,    -1,    33,    34,    35,    36,   112,   113,    -1,    -1,
      -1,    -1,    -1,   427,   428,   429,    -1,    -1,    -1,   321,
     434,    -1,    -1,   266,    -1,   131,    -1,    -1,   373,    -1,
     332,   445,    -1,   301,    -1,    -1,    -1,    -1,   452,    -1,
      -1,    -1,    -1,   388,    -1,    -1,    -1,   392,   393,    -1,
      -1,    -1,    -1,   321,    -1,    -1,   401,   402,   301,    -1,
     405,    -1,    -1,    -1,   332,    -1,    -1,    -1,    -1,    -1,
      -1,   373,    -1,    -1,    -1,   457,    -1,    -1,   321,    -1,
      -1,   112,   113,    -1,    -1,    -1,   388,    -1,    -1,   332,
     392,   393,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   401,
     402,    -1,    -1,   405,   449,   373,    -1,    70,    71,    72,
      73,    74,    75,    76,    77,    78,    79,    80,    -1,    -1,
     388,    -1,    -1,    -1,   392,   393,    -1,    -1,    -1,    -1,
     373,    -1,    -1,   401,   402,    -1,    -1,   405,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   388,   109,   449,    -1,   392,
     393,    -1,    -1,    -1,    -1,     9,   119,    -1,   401,   402,
      -1,    -1,   405,    -1,    -1,   128,    -1,    -1,    22,    -1,
      -1,   134,    -1,   136,    -1,    -1,    -1,    -1,    -1,     0,
       9,   449,     3,     4,     5,     6,     7,     8,     9,    -1,
      -1,    -1,    -1,    -1,    -1,    16,    -1,    18,    19,    -1,
      21,    -1,    -1,    24,    25,    -1,   449,    28,    29,    30,
      31,    -1,    33,    34,    35,    36,    37,    38,    39,    40,
      -1,    42,    43,    44,    45,    46,    47,    48,    82,    83,
      84,    85,    86,    87,    88,    89,    90,    91,    92,    93,
      94,    95,    96,    97,    98,    99,   100,   101,   102,    -1,
      -1,   105,   106,   107,   108,    -1,   110,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   118,    -1,    95,    96,    97,    98,
      99,   100,   101,   102,    95,    96,   105,   106,   107,   108,
      -1,   110,   136,    -1,   105,   106,    -1,    -1,    -1,    -1,
     111,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   127,   128,    -1,    -1,
      -1,   132,   133,   134,     0,    -1,    -1,     3,     4,     5,
       6,     7,     8,     9,    -1,    -1,    -1,    -1,    -1,    -1,
      16,    -1,    18,    19,    -1,    21,    -1,    -1,    24,    25,
      -1,    -1,    28,    29,    30,    31,    -1,    33,    34,    35,
      36,    37,    38,    39,    40,    -1,    42,    43,    44,    45,
      46,    47,    48,     3,     4,     5,     6,     7,     8,     9,
      -1,     3,     4,     5,     6,     7,     8,     9,    -1,    -1,
      -1,    -1,    -1,    -1,    24,    -1,    -1,    -1,    28,    29,
      30,    -1,    24,    -1,    -1,    -1,    28,    29,    30,    39,
      -1,    -1,    -1,    43,    44,    -1,    -1,    39,    48,    95,
      96,    43,    44,    -1,    -1,    -1,    48,    -1,    -1,   105,
     106,    -1,    -1,    -1,    -1,   111,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   127,   128,    -1,    -1,    -1,   132,   133,   134,    -1,
      -1,    -1,    -1,    -1,    -1,    95,    96,    -1,    -1,    -1,
      -1,    -1,    -1,    95,    96,   105,   106,    -1,    -1,    -1,
      -1,   111,    -1,   105,   106,    -1,    -1,    -1,    -1,   111,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   128,    -1,
      -1,    -1,   132,   133,   134,   135,   128,    -1,    -1,    -1,
     132,   133,   134,   135,     3,     4,     5,     6,     7,     8,
       9,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    24,    -1,    -1,    -1,    28,
      29,    30,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      39,    -1,    -1,    -1,    43,    44,    -1,    -1,    -1,    48,
       3,     4,     5,     6,     7,     8,     9,    -1,    -1,    -1,
      -1,    -1,    -1,    16,    -1,    18,    19,    -1,    21,    -1,
      -1,    24,    25,    -1,    -1,    28,    29,    30,    31,    -1,
      33,    34,    35,    36,    37,    38,    39,    40,    -1,    42,
      43,    44,    45,    46,    -1,    48,    95,    96,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   105,   106,    -1,    -1,
      -1,    -1,   111,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   128,
      -1,    -1,    -1,   132,   133,   134,   135,    -1,    -1,    -1,
      -1,    -1,    95,    96,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,   105,   106,    -1,    -1,    -1,    -1,   111,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   127,   128,    -1,    -1,   131,   132,
     133,   134,     3,     4,     5,     6,     7,     8,     9,    -1,
      -1,    -1,    -1,    -1,    -1,    16,    -1,    18,    19,    -1,
      21,    -1,    -1,    24,    25,    -1,    -1,    28,    29,    30,
      31,    -1,    33,    34,    35,    36,    37,    38,    39,    40,
      -1,    42,    43,    44,    45,    46,    -1,    48,     3,     4,
       5,     6,     7,     8,     9,    -1,    -1,    -1,    -1,    -1,
      -1,    16,    -1,    18,    19,    -1,    21,    -1,    -1,    24,
      25,    -1,    -1,    28,    29,    30,    -1,    -1,    -1,    -1,
      -1,    -1,    37,    38,    39,    40,    -1,    42,    43,    44,
      45,    46,    47,    48,    95,    96,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   105,   106,    -1,    -1,    -1,    -1,
     111,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   127,   128,    -1,    -1,
     131,   132,   133,   134,    -1,    -1,    -1,    -1,    -1,    -1,
      95,    96,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     105,   106,    -1,    -1,    -1,    -1,   111,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,   127,   128,    -1,    -1,    -1,   132,   133,   134,
       3,     4,     5,     6,     7,     8,     9,    -1,    -1,    -1,
      -1,    -1,    -1,    16,    -1,    18,    19,    -1,    21,    -1,
      -1,    24,    25,    -1,    -1,    28,    29,    30,    -1,    -1,
      -1,    -1,    -1,    -1,    37,    38,    39,    40,    -1,    42,
      43,    44,    45,    46,    -1,    48,     3,     4,     5,     6,
       7,     8,     9,    -1,     3,     4,     5,     6,     7,     8,
       9,    -1,    -1,    -1,    -1,    -1,    -1,    24,    25,    -1,
      -1,    28,    29,    30,    -1,    24,    -1,    -1,    -1,    28,
      29,    30,    39,    -1,    -1,    -1,    43,    44,    -1,    46,
      39,    48,    95,    96,    43,    44,    -1,    -1,    -1,    48,
      -1,    -1,   105,   106,    -1,    -1,    -1,    -1,   111,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   127,   128,    -1,    -1,    -1,   132,
     133,   134,    -1,    -1,    -1,    -1,    -1,    -1,    95,    96,
      -1,    -1,    -1,    -1,    -1,    -1,    95,    96,   105,   106,
      -1,    -1,    -1,    -1,   111,    -1,   105,   106,    -1,    -1,
     109,    -1,   111,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   128,    -1,    -1,    -1,   132,   133,   134,    -1,   128,
      -1,    -1,    -1,   132,   133,   134,     3,     4,     5,     6,
       7,     8,     9,    -1,     3,     4,     5,     6,     7,     8,
       9,    -1,    -1,    -1,    -1,    -1,    -1,    24,    -1,    -1,
      -1,    28,    29,    30,    -1,    24,    -1,    -1,    -1,    28,
      29,    30,    39,    -1,    -1,    -1,    43,    44,    -1,    -1,
      39,    48,    -1,    -1,    43,    44,    -1,    -1,    -1,    48,
       3,     4,     5,     6,     7,     8,     9,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    24,    -1,    -1,    -1,    28,    29,    30,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    39,    -1,    95,    96,
      43,    44,    -1,    -1,    -1,    48,    95,    96,   105,   106,
      -1,    -1,    -1,    -1,   111,    -1,   105,   106,    -1,    -1,
      -1,    -1,   111,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     127,   128,    -1,    -1,    -1,   132,   133,   134,   127,   128,
      -1,    -1,    -1,   132,   133,   134,    -1,    -1,    -1,    -1,
      -1,    -1,    95,    96,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,   105,   106,    -1,    -1,    -1,    -1,   111,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   119,     3,     4,     5,
       6,     7,     8,     9,    -1,   128,    -1,    -1,    -1,   132,
     133,   134,    -1,    -1,    -1,    -1,    -1,    -1,    24,    -1,
      -1,    -1,    28,    29,    30,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    39,    -1,    -1,    -1,    43,    44,    -1,
      -1,    -1,    48,     3,     4,     5,     6,     7,     8,     9,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    24,    -1,    -1,    -1,    28,    29,
      30,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    39,
      -1,    -1,    -1,    43,    44,    -1,    -1,    -1,    48,    95,
      96,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   105,
     106,    -1,    -1,     9,    -1,   111,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    22,    -1,    -1,    -1,
      -1,    -1,   128,    -1,    -1,    -1,   132,   133,   134,    -1,
      -1,    -1,    -1,    -1,    -1,    95,    96,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   105,   106,    -1,    -1,    -1,
      -1,   111,    -1,    -1,    -1,    -1,    -1,     9,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   128,    -1,
      22,    -1,   132,   133,   134,    81,    82,    83,    84,    85,
      86,    87,    88,    89,    90,    91,    92,    93,    94,    95,
      96,    97,    98,    99,   100,   101,   102,    -1,    -1,   105,
     106,   107,   108,    -1,   110,    -1,    -1,    -1,    -1,    -1,
      -1,     9,   118,   119,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    22,    -1,    -1,    -1,    -1,   135,
      82,    83,    84,    85,    86,    87,    88,    89,    90,    91,
      92,    93,    94,    95,    96,    97,    98,    99,   100,   101,
     102,    -1,    -1,   105,   106,   107,   108,    -1,   110,    -1,
      -1,    -1,    -1,    -1,    -1,     9,   118,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    22,    -1,
      -1,    -1,    -1,   135,    82,    83,    84,    85,    86,    87,
      88,    89,    90,    91,    92,    93,    94,    95,    96,    97,
      98,    99,   100,   101,   102,    -1,    -1,   105,   106,   107,
     108,    -1,   110,    -1,    -1,    -1,    -1,    -1,    -1,     9,
     118,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    22,    -1,    -1,    -1,    -1,   135,    82,    83,
      84,    85,    86,    87,    88,    89,    90,    91,    92,    93,
      94,    95,    96,    97,    98,    99,   100,   101,   102,    -1,
      -1,   105,   106,   107,   108,    -1,   110,    -1,    -1,    -1,
      -1,    -1,    -1,     9,   118,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    22,    -1,    -1,    -1,
      -1,   135,    82,    83,    84,    85,    86,    87,    88,    89,
      90,    91,    92,    93,    94,    95,    96,    97,    98,    99,
     100,   101,   102,    -1,    -1,   105,   106,   107,   108,    -1,
     110,    -1,    -1,    -1,    -1,    -1,    -1,     9,   118,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      22,    -1,    -1,    -1,    -1,   135,    82,    83,    84,    85,
      86,    87,    88,    89,    90,    91,    92,    93,    94,    95,
      96,    97,    98,    99,   100,   101,   102,    -1,    -1,   105,
     106,   107,   108,    -1,   110,     9,    -1,    -1,    -1,    -1,
      -1,    -1,   118,    -1,    -1,    -1,    -1,    -1,    22,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   135,
      82,    83,    84,    85,    86,    87,    88,    89,    90,    91,
      92,    93,    94,    95,    96,    97,    98,    99,   100,   101,
     102,    -1,    -1,   105,   106,   107,   108,     9,   110,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   118,    -1,    -1,    -1,
      22,    -1,    -1,    -1,   126,    -1,    -1,   129,    82,    83,
      84,    85,    86,    87,    88,    89,    90,    91,    92,    93,
      94,    95,    96,    97,    98,    99,   100,   101,   102,    -1,
      -1,   105,   106,   107,   108,     9,   110,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   118,    -1,    -1,    -1,    22,    -1,
      -1,    -1,    -1,    -1,    -1,   129,    -1,    -1,    -1,    -1,
      82,    83,    84,    85,    86,    87,    88,    89,    90,    91,
      92,    93,    94,    95,    96,    97,    98,    99,   100,   101,
     102,    -1,    -1,   105,   106,   107,   108,     9,   110,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   118,    -1,    -1,    -1,
      22,    -1,    -1,    -1,    -1,    -1,    -1,   129,    82,    83,
      84,    85,    86,    87,    88,    89,    90,    91,    92,    93,
      94,    95,    96,    97,    98,    99,   100,   101,   102,    -1,
      -1,   105,   106,   107,   108,     9,   110,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   118,    -1,    -1,    -1,    22,    -1,
      -1,    -1,    -1,    -1,    -1,   129,    -1,    -1,    -1,    -1,
      82,    83,    84,    85,    86,    87,    88,    89,    90,    91,
      92,    93,    94,    95,    96,    97,    98,    99,   100,   101,
     102,    -1,    -1,   105,   106,   107,   108,     9,   110,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   118,    -1,    -1,    -1,
      22,    -1,    -1,    -1,    -1,    -1,    -1,   129,    82,    83,
      84,    85,    86,    87,    88,    89,    90,    91,    92,    93,
      94,    95,    96,    97,    98,    99,   100,   101,   102,    -1,
      -1,   105,   106,   107,   108,     9,   110,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   118,    -1,    -1,    -1,    22,    -1,
      -1,    -1,    -1,    -1,    -1,   129,    -1,    -1,    -1,    -1,
      82,    83,    84,    85,    86,    87,    88,    89,    90,    91,
      92,    93,    94,    95,    96,    97,    98,    99,   100,   101,
     102,     9,    -1,   105,   106,   107,   108,    -1,   110,    -1,
      -1,    -1,    -1,    -1,    22,    -1,   118,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   127,    -1,    -1,    82,    83,
      84,    85,    86,    87,    88,    89,    90,    91,    92,    93,
      94,    95,    96,    97,    98,    99,   100,   101,   102,     9,
      -1,   105,   106,   107,   108,    -1,   110,    -1,    -1,    -1,
      -1,    -1,    22,    -1,   118,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   127,    82,    83,    84,    85,    86,    87,
      88,    89,    90,    91,    92,    93,    94,    95,    96,    97,
      98,    99,   100,   101,   102,     9,    -1,   105,   106,   107,
     108,    -1,   110,    -1,    -1,    -1,    -1,    -1,    22,    -1,
     118,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   127,
      -1,    -1,    82,    83,    84,    85,    86,    87,    88,    89,
      90,    91,    92,    93,    94,    95,    96,    97,    98,    99,
     100,   101,   102,     9,    -1,   105,   106,   107,   108,    -1,
     110,    -1,    -1,    -1,    -1,    -1,    22,    -1,   118,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   127,    82,    83,
      84,    85,    86,    87,    88,    89,    90,    91,    92,    93,
      94,    95,    96,    97,    98,    99,   100,   101,   102,     9,
      -1,   105,   106,   107,   108,    -1,   110,    -1,    -1,    -1,
      -1,    -1,    22,    -1,   118,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,   126,    -1,    -1,    -1,    82,    83,    84,    85,
      86,    87,    88,    89,    90,    91,    92,    93,    94,    95,
      96,    97,    98,    99,   100,   101,   102,     9,    -1,   105,
     106,   107,   108,    -1,   110,    -1,    -1,    -1,    -1,    -1,
      22,    -1,   118,   119,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    82,    83,    84,    85,    86,    87,    88,    89,
      90,    91,    92,    93,    94,    95,    96,    97,    98,    99,
     100,   101,   102,     9,    -1,   105,   106,   107,   108,    -1,
     110,    -1,    -1,    -1,    -1,    -1,    22,    -1,   118,   119,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      82,    83,    84,    85,    86,    87,    88,    89,    90,    91,
      92,    93,    94,    95,    96,    97,    98,    99,   100,   101,
     102,     9,    -1,   105,   106,   107,   108,    -1,   110,    -1,
      -1,    -1,    -1,    -1,    22,    -1,   118,   119,    -1,     9,
      -1,    -1,    -1,    -1,    -1,    -1,    82,    83,    84,    85,
      86,    87,    88,    89,    90,    91,    92,    93,    94,    95,
      96,    97,    98,    99,   100,   101,   102,     9,    -1,   105,
     106,   107,   108,    -1,   110,    -1,    -1,    -1,    -1,    -1,
      22,    -1,   118,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,     9,    83,    84,    85,    86,    87,
      88,    89,    90,    91,    92,    93,    94,    95,    96,    97,
      98,    99,   100,   101,   102,    -1,    -1,   105,   106,   107,
     108,     9,   110,    93,    94,    95,    96,    97,    98,    99,
     100,   101,   102,    -1,    -1,   105,   106,   107,   108,    -1,
     110,    -1,    84,    85,    86,    87,    88,    89,    90,    91,
      92,    93,    94,    95,    96,    97,    98,    99,   100,   101,
     102,     9,    -1,   105,   106,   107,   108,    -1,   110,    84,
      85,    86,    87,    88,    89,    90,    91,    92,    93,    94,
      95,    96,    97,    98,    99,   100,   101,   102,     9,    -1,
     105,   106,   107,   108,    -1,   110,    -1,    85,    86,    87,
      88,    89,    90,    91,    92,    93,    94,    95,    96,    97,
      98,    99,   100,   101,   102,     9,    -1,   105,   106,   107,
     108,    -1,   110,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    86,    87,
      88,    89,    90,    91,    92,    93,    94,    95,    96,    97,
      98,    99,   100,   101,   102,    -1,    -1,   105,   106,   107,
     108,    -1,   110,    -1,    -1,    -1,    87,    88,    89,    90,
      91,    92,    93,    94,    95,    96,    97,    98,    99,   100,
     101,   102,    -1,    -1,   105,   106,   107,   108,    -1,   110,
      -1,    -1,    -1,    -1,    -1,    89,    90,    91,    92,    93,
      94,    95,    96,    97,    98,    99,   100,   101,   102,    -1,
      -1,   105,   106,   107,   108,    -1,   110,    49,    50,    51,
      52,    53,    54,    55,    56,    57,    58,    59,    60,    -1,
      62,    63,    64,    65,    66,    67,    68,    69,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   130
};

  /* YYSTOSSTATE-NUM -- The (internal number of the) accessing
     symbol of state STATE-NUM.  */
static const yytype_uint8 yystos[] =
{
       0,   122,   123,   124,   125,   139,     3,     4,     5,     6,
       7,     8,     9,    24,    28,    29,    30,    39,    43,    44,
      48,    95,    96,   105,   106,   111,   128,   132,   133,   134,
     147,   148,   150,   151,   152,   154,   160,   169,   170,   171,
     172,   173,   174,   175,   176,    31,    33,    34,    35,    36,
     193,   194,   196,    16,    18,    19,    21,    25,    37,    38,
      40,    42,    45,    46,    47,   127,   138,   143,   147,   148,
     150,   152,   203,   208,   209,   213,   140,   141,   142,   143,
     194,   209,     0,   128,   169,   170,   171,   176,   153,   147,
     147,   147,   152,   213,   109,   147,   161,   162,     9,    22,
      82,    83,    84,    85,    86,    87,    88,    89,    90,    91,
      92,    93,    94,    95,    96,    97,    98,    99,   100,   101,
     102,   105,   106,   107,   108,   110,   118,   126,   147,   109,
     128,   134,   130,   103,   104,   134,   149,   128,     3,     7,
       8,   144,   199,   200,   144,   197,   198,   144,   201,   202,
     127,   126,   144,   127,   128,   128,   128,   128,   127,   147,
     127,   209,   127,   147,   147,     6,   128,     6,     7,    49,
      50,    51,    52,    53,    54,    55,    56,    57,    58,    59,
      60,    62,    63,    64,    65,    66,    67,    68,    69,   130,
     204,   127,   136,   136,    70,    71,    72,    73,    74,    75,
      76,    77,    78,    79,    80,   119,   136,   206,   214,   215,
     127,   127,   142,   171,   126,   128,   164,   171,   129,   129,
     134,   126,   157,   147,   147,   147,   147,   147,   147,   147,
     147,   147,   147,   147,   147,   147,   147,   147,   147,   147,
     147,   147,   147,   147,   147,   171,   171,   147,   144,   147,
     155,   156,   119,   147,     7,     8,   144,   147,   158,   159,
     135,   147,   147,   145,   147,    92,   136,   126,   136,   126,
     136,   126,   195,   147,   147,     7,   127,   138,   147,   127,
      20,    41,   127,   127,   212,     6,   127,   127,   131,   194,
     205,   209,   147,   147,     7,   147,   147,   215,   129,     3,
     144,   163,   119,   147,   162,   135,   119,   126,   129,   135,
     147,    81,   119,   135,   136,   126,   157,   135,   119,   126,
     157,   128,   138,   168,   171,   177,   180,   200,   147,   198,
     147,   202,   136,   129,   129,    22,   146,   147,   193,   126,
     129,   147,    18,   128,   203,   126,   127,   212,   131,   194,
     209,   147,    92,    92,   144,   165,   166,   171,   163,   135,
     147,   156,   135,   147,   135,   147,   147,   159,   131,   147,
     147,   129,   116,   129,   171,   178,   179,    11,   183,   164,
     209,   209,   147,   127,   127,   147,   127,   147,   138,   147,
     129,   116,   129,   126,   144,   203,   136,   135,   135,   171,
     118,   129,   126,    12,    14,   182,    17,   138,    46,   207,
     208,   146,   129,   203,   166,   171,   165,   136,   167,   147,
     171,   178,   171,   184,   209,   210,   211,   126,   129,   127,
     129,   119,   147,   130,   129,    23,   208,   209,   207,   203,
     203,   131,   181,   209,   147,   129,   112,   113,   185,   186,
     187,   194,   129,   209,   131,   187,   194,   168,   209,   144,
     189,   188,   119,   136,   191,   147,   147,   103,   190,   136,
     119,   147,    18,   192,   147,   147,   147,   127
};

  /* YYR1YYN -- Symbol number of symbol that rule YYN derives.  */
static const yytype_uint8 yyr1[] =
{
       0,   137,   138,   139,   139,   139,   139,   139,   139,   139,
     139,   139,   140,   140,   141,   141,   142,   142,   142,   143,
     143,   144,   144,   145,   145,   145,   146,   146,   147,   147,
     147,   147,   147,   147,   147,   147,   147,   147,   147,   147,
     147,   147,   147,   147,   147,   147,   147,   147,   147,   147,
     147,   147,   147,   147,   147,   147,   147,   147,   147,   147,
     147,   147,   147,   147,   148,   149,   149,   150,   150,   151,
     151,   151,   151,   151,   152,   152,   152,   152,   152,   152,
     152,   152,   152,   152,   152,   152,   152,   152,   152,   152,
     153,   152,   152,   152,   154,   155,   155,   155,   156,   157,
     157,   158,   158,   158,   159,   159,   160,   161,   161,   162,
     162,   163,   164,   164,   165,   165,   166,   166,   167,   167,
     168,   168,   168,   169,   170,   171,   171,   171,   171,   171,
     171,   171,   172,   172,   172,   173,   174,   174,   175,   175,
     176,   176,   177,   177,   178,   178,   179,   179,   179,   180,
     181,   180,   182,   182,   183,   183,   184,   184,   185,   185,
     185,   185,   186,   186,   186,   188,   187,   189,   189,   190,
     190,   191,   191,   191,   191,   191,   192,   192,   193,   193,
     193,   195,   194,   194,   196,   196,   197,   197,   198,   199,
     199,   200,   201,   201,   202,   203,   203,   203,   204,   204,
     204,   204,   204,   204,   204,   204,   204,   204,   204,   204,
     204,   204,   204,   204,   204,   204,   204,   204,   205,   205,
     205,   205,   206,   206,   206,   206,   206,   206,   206,   206,
     206,   206,   206,   207,   207,   207,   208,   208,   208,   208,
     208,   208,   208,   208,   208,   209,   209,   209,   209,   209,
     209,   209,   209,   210,   209,   211,   209,   209,   209,   209,
     209,   209,   209,   209,   209,   209,   209,   209,   209,   212,
     212,   213,   214,   214,   215
};

  /* YYR2YYN -- Number of symbols on the right hand side of rule YYN.  */
static const yytype_int8 yyr2[] =
{
       0,     2,     0,     2,     3,     2,     3,     2,     3,     2,
       3,     2,     0,     1,     1,     2,     1,     1,     1,     3,
       3,     1,     1,     0,     1,     3,     0,     1,     1,     2,
       4,     2,     3,     3,     3,     3,     3,     3,     3,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     3,
       3,     3,     3,     3,     3,     5,     5,     5,     1,     2,
       2,     2,     1,     1,     3,     1,     1,     3,     5,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     3,
       1,     3,     4,     6,     6,     4,     5,     5,     1,     3,
       0,     3,     2,     2,     4,     0,     1,     3,     1,     0,
       1,     0,     1,     3,     1,     3,     4,     1,     3,     1,
       6,     0,     7,     4,     1,     3,     3,     2,     0,     2,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     3,     1,     1,     5,     5,
       3,     4,     4,     3,     1,     3,     1,     2,     1,     6,
       0,     8,     1,     1,     0,     1,     0,     1,     1,     1,
       2,     2,     0,     1,     1,     0,     8,     0,     1,     0,
       2,     0,     2,     2,     4,     4,     0,     2,     2,     2,
       2,     0,     5,     2,     1,     1,     1,     3,     3,     1,
       3,     3,     1,     3,     3,     3,     4,     2,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     2,
       1,     2,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     0,     1,     3,     3,     3,     3,     3,
       1,     5,     4,     6,     1,     1,     1,     2,     5,     7,
       5,    10,     8,     0,     9,     0,    11,     2,     2,     2,
       3,     4,     6,     8,     5,     2,     3,     3,     4,     0,
       3,     2,     1,     2,     3
};


enum { YYENOMEM = -2 };

#define yyerrok         (yyerrstatus = 0)
#define yyclearin       (yychar = PKL_TAB_EMPTY)

#define YYACCEPT        goto yyacceptlab
#define YYABORT         goto yyabortlab
#define YYERROR         goto yyerrorlab


#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)                                    \
  do                                                              \
    if (yychar == PKL_TAB_EMPTY)                                        \
      {                                                           \
        yychar = (Token);                                         \
        yylval = (Value);                                         \
        YYPOPSTACK (yylen);                                       \
        yystate = *yyssp;                                         \
        YY_LAC_DISCARD ("YYBACKUP");                              \
        goto yybackup;                                            \
      }                                                           \
    else                                                          \
      {                                                           \
        yyerror (&yylloc, pkl_parser, YY_("syntax error: cannot back up")); \
        YYERROR;                                                  \
      }                                                           \
  while (0)

/* Backward compatibility with an undocumented macro.
   Use PKL_TAB_error or PKL_TAB_UNDEF. */
#define YYERRCODE PKL_TAB_UNDEF

/* YYLLOC_DEFAULT -- Set CURRENT to span from RHS[1] to RHS[N].
   If N is 0, then set CURRENT to the empty location which ends
   the previous symbol: RHS[0] (always defined).  */

#ifndef YYLLOC_DEFAULT
# define YYLLOC_DEFAULT(Current, Rhs, N)                                \
    do                                                                  \
      if (N)                                                            \
        {                                                               \
          (Current).first_line   = YYRHSLOC (Rhs, 1).first_line;        \
          (Current).first_column = YYRHSLOC (Rhs, 1).first_column;      \
          (Current).last_line    = YYRHSLOC (Rhs, N).last_line;         \
          (Current).last_column  = YYRHSLOC (Rhs, N).last_column;       \
        }                                                               \
      else                                                              \
        {                                                               \
          (Current).first_line   = (Current).last_line   =              \
            YYRHSLOC (Rhs, 0).last_line;                                \
          (Current).first_column = (Current).last_column =              \
            YYRHSLOC (Rhs, 0).last_column;                              \
        }                                                               \
    while (0)
#endif

#define YYRHSLOC(Rhs, K) ((Rhs)[K])


/* Enable debugging if requested.  */
#if PKL_TAB_DEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)                        \
do {                                            \
  if (yydebug)                                  \
    YYFPRINTF Args;                             \
} while (0)


/* YY_LOCATION_PRINT -- Print the location on the stream.
   This macro was not mandated originally: define only if we know
   we won't break user code: when these are the locations we know.  */

# ifndef YY_LOCATION_PRINT
#  if defined PKL_TAB_LTYPE_IS_TRIVIAL && PKL_TAB_LTYPE_IS_TRIVIAL

/* Print *YYLOCP on YYO.  Private, do not rely on its existence. */

YY_ATTRIBUTE_UNUSED
static int
yy_location_print_ (FILE *yyo, YYLTYPE const * const yylocp)
{
  int res = 0;
  int end_col = 0 != yylocp->last_column ? yylocp->last_column - 1 : 0;
  if (0 <= yylocp->first_line)
    {
      res += YYFPRINTF (yyo, "%d", yylocp->first_line);
      if (0 <= yylocp->first_column)
        res += YYFPRINTF (yyo, ".%d", yylocp->first_column);
    }
  if (0 <= yylocp->last_line)
    {
      if (yylocp->first_line < yylocp->last_line)
        {
          res += YYFPRINTF (yyo, "-%d", yylocp->last_line);
          if (0 <= end_col)
            res += YYFPRINTF (yyo, ".%d", end_col);
        }
      else if (0 <= end_col && yylocp->first_column < end_col)
        res += YYFPRINTF (yyo, "-%d", end_col);
    }
  return res;
 }

#   define YY_LOCATION_PRINT(File, Loc)          \
  yy_location_print_ (File, &(Loc))

#  else
#   define YY_LOCATION_PRINT(File, Loc) ((void) 0)
#  endif
# endif /* !defined YY_LOCATION_PRINT */


# define YY_SYMBOL_PRINT(Title, Kind, Value, Location)                    \
do {                                                                      \
  if (yydebug)                                                            \
    {                                                                     \
      YYFPRINTF (stderr, "%s ", Title);                                   \
      yy_symbol_print (stderr,                                            \
                  Kind, Value, Location, pkl_parser); \
      YYFPRINTF (stderr, "\n");                                           \
    }                                                                     \
} while (0)


/*-----------------------------------.
| Print this symbol's value on YYO.  |
`-----------------------------------*/

static void
yy_symbol_value_print (FILE *yyo,
                       yysymbol_kind_t yykind, YYSTYPE const * const yyvaluep, YYLTYPE const * const yylocationp, struct pkl_parser *pkl_parser)
{
  FILE *yyoutput = yyo;
  YYUSE (yyoutput);
  YYUSE (yylocationp);
  YYUSE (pkl_parser);
  if (!yyvaluep)
    return;
# ifdef YYPRINT
  if (yykind < YYNTOKENS)
    YYPRINT (yyo, yytoknum[yykind], *yyvaluep);
# endif
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YYUSE (yykind);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}


/*---------------------------.
| Print this symbol on YYO.  |
`---------------------------*/

static void
yy_symbol_print (FILE *yyo,
                 yysymbol_kind_t yykind, YYSTYPE const * const yyvaluep, YYLTYPE const * const yylocationp, struct pkl_parser *pkl_parser)
{
  YYFPRINTF (yyo, "%s %s (",
             yykind < YYNTOKENS ? "token" : "nterm", yysymbol_name (yykind));

  YY_LOCATION_PRINT (yyo, *yylocationp);
  YYFPRINTF (yyo, ": ");
  yy_symbol_value_print (yyo, yykind, yyvaluep, yylocationp, pkl_parser);
  YYFPRINTF (yyo, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

static void
yy_stack_print (yy_state_t *yybottom, yy_state_t *yytop)
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)                            \
do {                                                            \
  if (yydebug)                                                  \
    yy_stack_print ((Bottom), (Top));                           \
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

static void
yy_reduce_print (yy_state_t *yyssp, YYSTYPE *yyvsp, YYLTYPE *yylsp,
                 int yyrule, struct pkl_parser *pkl_parser)
{
  int yylno = yyrline[yyrule];
  int yynrhs = yyr2[yyrule];
  int yyi;
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %d):\n",
             yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr,
                       YY_ACCESSING_SYMBOL (+yyssp[yyi + 1 - yynrhs]),
                       &yyvsp[(yyi + 1) - (yynrhs)],
                       &(yylsp[(yyi + 1) - (yynrhs)]), pkl_parser);
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)          \
do {                                    \
  if (yydebug)                          \
    yy_reduce_print (yyssp, yyvsp, yylsp, Rule, pkl_parser); \
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !PKL_TAB_DEBUG */
# define YYDPRINTF(Args) ((void) 0)
# define YY_SYMBOL_PRINT(Title, Kind, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !PKL_TAB_DEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif


/* Given a state stack such that *YYBOTTOM is its bottom, such that
   *YYTOP is either its top or is YYTOP_EMPTY to indicate an empty
   stack, and such that *YYCAPACITY is the maximum number of elements it
   can hold without a reallocation, make sure there is enough room to
   store YYADD more elements.  If not, allocate a new stack using
   YYSTACK_ALLOC, copy the existing elements, and adjust *YYBOTTOM,
   *YYTOP, and *YYCAPACITY to reflect the new capacity and memory
   location.  If *YYBOTTOM != YYBOTTOM_NO_FREE, then free the old stack
   using YYSTACK_FREE.  Return 0 if successful or if no reallocation is
   required.  Return YYENOMEM if memory is exhausted.  */
static int
yy_lac_stack_realloc (YYPTRDIFF_T *yycapacity, YYPTRDIFF_T yyadd,
#if PKL_TAB_DEBUG
                      char const *yydebug_prefix,
                      char const *yydebug_suffix,
#endif
                      yy_state_t **yybottom,
                      yy_state_t *yybottom_no_free,
                      yy_state_t **yytop, yy_state_t *yytop_empty)
{
  YYPTRDIFF_T yysize_old =
    *yytop == yytop_empty ? 0 : *yytop - *yybottom + 1;
  YYPTRDIFF_T yysize_new = yysize_old + yyadd;
  if (*yycapacity < yysize_new)
    {
      YYPTRDIFF_T yyalloc = 2 * yysize_new;
      yy_state_t *yybottom_new;
      /* Use YYMAXDEPTH for maximum stack size given that the stack
         should never need to grow larger than the main state stack
         needs to grow without LAC.  */
      if (YYMAXDEPTH < yysize_new)
        {
          YYDPRINTF ((stderr, "%smax size exceeded%s", yydebug_prefix,
                      yydebug_suffix));
          return YYENOMEM;
        }
      if (YYMAXDEPTH < yyalloc)
        yyalloc = YYMAXDEPTH;
      yybottom_new =
        YY_CAST (yy_state_t *,
                 YYSTACK_ALLOC (YY_CAST (YYSIZE_T,
                                         yyalloc * YYSIZEOF (*yybottom_new))));
      if (!yybottom_new)
        {
          YYDPRINTF ((stderr, "%srealloc failed%s", yydebug_prefix,
                      yydebug_suffix));
          return YYENOMEM;
        }
      if (*yytop != yytop_empty)
        {
          YYCOPY (yybottom_new, *yybottom, yysize_old);
          *yytop = yybottom_new + (yysize_old - 1);
        }
      if (*yybottom != yybottom_no_free)
        YYSTACK_FREE (*yybottom);
      *yybottom = yybottom_new;
      *yycapacity = yyalloc;
    }
  return 0;
}

/* Establish the initial context for the current lookahead if no initial
   context is currently established.

   We define a context as a snapshot of the parser stacks.  We define
   the initial context for a lookahead as the context in which the
   parser initially examines that lookahead in order to select a
   syntactic action.  Thus, if the lookahead eventually proves
   syntactically unacceptable (possibly in a later context reached via a
   series of reductions), the initial context can be used to determine
   the exact set of tokens that would be syntactically acceptable in the
   lookahead's place.  Moreover, it is the context after which any
   further semantic actions would be erroneous because they would be
   determined by a syntactically unacceptable token.

   YY_LAC_ESTABLISH should be invoked when a reduction is about to be
   performed in an inconsistent state (which, for the purposes of LAC,
   includes consistent states that don't know they're consistent because
   their default reductions have been disabled).  Iff there is a
   lookahead token, it should also be invoked before reporting a syntax
   error.  This latter case is for the sake of the debugging output.

   For parse.lac=full, the implementation of YY_LAC_ESTABLISH is as
   follows.  If no initial context is currently established for the
   current lookahead, then check if that lookahead can eventually be
   shifted if syntactic actions continue from the current context.
   Report a syntax error if it cannot.  */
#define YY_LAC_ESTABLISH                                                \
do {                                                                    \
  if (!yy_lac_established)                                              \
    {                                                                   \
      YYDPRINTF ((stderr,                                               \
                  "LAC: initial context established for %s\n",          \
                  yysymbol_name (yytoken)));                            \
      yy_lac_established = 1;                                           \
      switch (yy_lac (yyesa, &yyes, &yyes_capacity, yyssp, yytoken))    \
        {                                                               \
        case YYENOMEM:                                                  \
          goto yyexhaustedlab;                                          \
        case 1:                                                         \
          goto yyerrlab;                                                \
        }                                                               \
    }                                                                   \
} while (0)

/* Discard any previous initial lookahead context because of Event,
   which may be a lookahead change or an invalidation of the currently
   established initial context for the current lookahead.

   The most common example of a lookahead change is a shift.  An example
   of both cases is syntax error recovery.  That is, a syntax error
   occurs when the lookahead is syntactically erroneous for the
   currently established initial context, so error recovery manipulates
   the parser stacks to try to find a new initial context in which the
   current lookahead is syntactically acceptable.  If it fails to find
   such a context, it discards the lookahead.  */
#if PKL_TAB_DEBUG
# define YY_LAC_DISCARD(Event)                                           \
do {                                                                     \
  if (yy_lac_established)                                                \
    {                                                                    \
      YYDPRINTF ((stderr, "LAC: initial context discarded due to "       \
                  Event "\n"));                                          \
      yy_lac_established = 0;                                            \
    }                                                                    \
} while (0)
#else
# define YY_LAC_DISCARD(Event) yy_lac_established = 0
#endif

/* Given the stack whose top is *YYSSP, return 0 iff YYTOKEN can
   eventually (after perhaps some reductions) be shifted, return 1 if
   not, or return YYENOMEM if memory is exhausted.  As preconditions and
   postconditions: *YYES_CAPACITY is the allocated size of the array to
   which *YYES points, and either *YYES = YYESA or *YYES points to an
   array allocated with YYSTACK_ALLOC.  yy_lac may overwrite the
   contents of either array, alter *YYES and *YYES_CAPACITY, and free
   any old *YYES other than YYESA.  */
static int
yy_lac (yy_state_t *yyesa, yy_state_t **yyes,
        YYPTRDIFF_T *yyes_capacity, yy_state_t *yyssp, yysymbol_kind_t yytoken)
{
  yy_state_t *yyes_prev = yyssp;
  yy_state_t *yyesp = yyes_prev;
  /* Reduce until we encounter a shift and thereby accept the token.  */
  YYDPRINTF ((stderr, "LAC: checking lookahead %s:", yysymbol_name (yytoken)));
  if (yytoken == YYSYMBOL_YYUNDEF)
    {
      YYDPRINTF ((stderr, " Always Err\n"));
      return 1;
    }
  while (1)
    {
      int yyrule = yypact[+*yyesp];
      if (yypact_value_is_default (yyrule)
          || (yyrule += yytoken) < 0 || YYLAST < yyrule
          || yycheck[yyrule] != yytoken)
        {
          /* Use the default action.  */
          yyrule = yydefact[+*yyesp];
          if (yyrule == 0)
            {
              YYDPRINTF ((stderr, " Err\n"));
              return 1;
            }
        }
      else
        {
          /* Use the action from yytable.  */
          yyrule = yytable[yyrule];
          if (yytable_value_is_error (yyrule))
            {
              YYDPRINTF ((stderr, " Err\n"));
              return 1;
            }
          if (0 < yyrule)
            {
              YYDPRINTF ((stderr, " S%d\n", yyrule));
              return 0;
            }
          yyrule = -yyrule;
        }
      /* By now we know we have to simulate a reduce.  */
      YYDPRINTF ((stderr, " R%d", yyrule - 1));
      {
        /* Pop the corresponding number of values from the stack.  */
        YYPTRDIFF_T yylen = yyr2[yyrule];
        /* First pop from the LAC stack as many tokens as possible.  */
        if (yyesp != yyes_prev)
          {
            YYPTRDIFF_T yysize = yyesp - *yyes + 1;
            if (yylen < yysize)
              {
                yyesp -= yylen;
                yylen = 0;
              }
            else
              {
                yyesp = yyes_prev;
                yylen -= yysize;
              }
          }
        /* Only afterwards look at the main stack.  */
        if (yylen)
          yyesp = yyes_prev -= yylen;
      }
      /* Push the resulting state of the reduction.  */
      {
        yy_state_fast_t yystate;
        {
          const int yylhs = yyr1[yyrule] - YYNTOKENS;
          const int yyi = yypgoto[yylhs] + *yyesp;
          yystate = (0 <= yyi && yyi <= YYLAST && yycheck[yyi] == *yyesp
                     ? yytable[yyi]
                     : yydefgoto[yylhs]);
        }
        if (yyesp == yyes_prev)
          {
            yyesp = *yyes;
            YY_IGNORE_USELESS_CAST_BEGIN
            *yyesp = YY_CAST (yy_state_t, yystate);
            YY_IGNORE_USELESS_CAST_END
          }
        else
          {
            if (yy_lac_stack_realloc (yyes_capacity, 1,
#if PKL_TAB_DEBUG
                                      " (", ")",
#endif
                                      yyes, yyesa, &yyesp, yyes_prev))
              {
                YYDPRINTF ((stderr, "\n"));
                return YYENOMEM;
              }
            YY_IGNORE_USELESS_CAST_BEGIN
            *++yyesp = YY_CAST (yy_state_t, yystate);
            YY_IGNORE_USELESS_CAST_END
          }
        YYDPRINTF ((stderr, " G%d", yystate));
      }
    }
}

/* Context of a parse error.  */
typedef struct
{
  yy_state_t *yyssp;
  yy_state_t *yyesa;
  yy_state_t **yyes;
  YYPTRDIFF_T *yyes_capacity;
  yysymbol_kind_t yytoken;
  YYLTYPE *yylloc;
} yypcontext_t;

/* Put in YYARG at most YYARGN of the expected tokens given the
   current YYCTX, and return the number of tokens stored in YYARG.  If
   YYARG is null, return the number of expected tokens (guaranteed to
   be less than YYNTOKENS).  Return YYENOMEM on memory exhaustion.
   Return 0 if there are more than YYARGN expected tokens, yet fill
   YYARG up to YYARGN. */
static int
yypcontext_expected_tokens (const yypcontext_t *yyctx,
                            yysymbol_kind_t yyarg[], int yyargn)
{
  /* Actual size of YYARG. */
  int yycount = 0;

  int yyx;
  for (yyx = 0; yyx < YYNTOKENS; ++yyx)
    {
      yysymbol_kind_t yysym = YY_CAST (yysymbol_kind_t, yyx);
      if (yysym != YYSYMBOL_YYerror && yysym != YYSYMBOL_YYUNDEF)
        switch (yy_lac (yyctx->yyesa, yyctx->yyes, yyctx->yyes_capacity, yyctx->yyssp, yysym))
          {
          case YYENOMEM:
            return YYENOMEM;
          case 1:
            continue;
          default:
            if (!yyarg)
              ++yycount;
            else if (yycount == yyargn)
              return 0;
            else
              yyarg[yycount++] = yysym;
          }
    }
  if (yyarg && yycount == 0 && 0 < yyargn)
    yyarg[0] = YYSYMBOL_YYEMPTY;
  return yycount;
}




/* The kind of the lookahead of this context.  */
static yysymbol_kind_t
yypcontext_token (const yypcontext_t *yyctx) YY_ATTRIBUTE_UNUSED;

static yysymbol_kind_t
yypcontext_token (const yypcontext_t *yyctx)
{
  return yyctx->yytoken;
}

/* The location of the lookahead of this context.  */
static YYLTYPE *
yypcontext_location (const yypcontext_t *yyctx) YY_ATTRIBUTE_UNUSED;

static YYLTYPE *
yypcontext_location (const yypcontext_t *yyctx)
{
  return yyctx->yylloc;
}

/* User defined function to report a syntax error.  */
static int
yyreport_syntax_error (const yypcontext_t *yyctx, struct pkl_parser *pkl_parser);

/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

static void
yydestruct (const char *yymsg,
            yysymbol_kind_t yykind, YYSTYPE *yyvaluep, YYLTYPE *yylocationp, struct pkl_parser *pkl_parser)
{
  YYUSE (yyvaluep);
  YYUSE (yylocationp);
  YYUSE (pkl_parser);
  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yykind, yyvaluep, yylocationp);

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  switch (yykind)
    {
    case 3: /* "integer literal"  */
#line 345 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 2868 "pkl-tab.c"
        break;

    case 5: /* "character literal"  */
#line 345 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 2896 "pkl-tab.c"
        break;

    case 6: /* "string"  */
#line 345 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 2924 "pkl-tab.c"
        break;

    case 7: /* "identifier"  */
#line 345 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 2952 "pkl-tab.c"
        break;

    case 8: /* "type name"  */
#line 345 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 2980 "pkl-tab.c"
        break;

    case 9: /* "offset unit"  */
#line 345 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 3008 "pkl-tab.c"
        break;

    case 110: /* "attribute"  */
#line 345 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 3036 "pkl-tab.c"
        break;

    case 139: /* start  */
#line 345 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 3064 "pkl-tab.c"
        break;

    case 140: /* program  */
#line 345 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 3092 "pkl-tab.c"
        break;

    case 141: /* program_elem_list  */
#line 345 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 3120 "pkl-tab.c"
        break;

    case 142: /* program_elem  */
#line 345 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 3148 "pkl-tab.c"
        break;

    case 143: /* load  */
#line 345 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 3176 "pkl-tab.c"
        break;

    case 144: /* identifier  */
#line 345 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 3204 "pkl-tab.c"
        break;

    case 145: /* expression_list  */
#line 345 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 3232 "pkl-tab.c"
        break;

    case 146: /* expression_opt  */
#line 345 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 3260 "pkl-tab.c"
        break;

    case 147: /* expression  */
#line 345 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 3288 "pkl-tab.c"
        break;

    case 148: /* bconc  */
#line 345 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 3316 "pkl-tab.c"
        break;

    case 150: /* map  */
#line 345 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 3344 "pkl-tab.c"
        break;

    case 152: /* primary  */
#line 345 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 3372 "pkl-tab.c"
        break;

    case 154: /* funcall  */
#line 345 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 3400 "pkl-tab.c"
        break;

    case 155: /* funcall_arg_list  */
#line 345 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 3428 "pkl-tab.c"
        break;

    case 156: /* funcall_arg  */
#line 345 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 3456 "pkl-tab.c"
        break;

    case 158: /* struct_field_list  */
#line 345 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 3484 "pkl-tab.c"
        break;

    case 159: /* struct_field  */
#line 345 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 3512 "pkl-tab.c"
        break;

    case 160: /* array  */
#line 345 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 3540 "pkl-tab.c"
        break;

    case 161: /* array_initializer_list  */
#line 345 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 3568 "pkl-tab.c"
        break;

    case 162: /* array_initializer  */
#line 345 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 3596 "pkl-tab.c"
        break;

    case 164: /* function_specifier  */
#line 345 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 3624 "pkl-tab.c"
        break;

    case 165: /* function_arg_list  */
#line 345 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 3652 "pkl-tab.c"
        break;

    case 166: /* function_arg  */
#line 345 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 3680 "pkl-tab.c"
        break;

    case 167: /* function_arg_initial  */
#line 345 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 3708 "pkl-tab.c"
        break;

    case 168: /* type_specifier  */
#line 345 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 3736 "pkl-tab.c"
        break;

    case 169: /* typename  */
#line 345 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 3764 "pkl-tab.c"
        break;

    case 170: /* string_type_specifier  */
#line 345 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 3792 "pkl-tab.c"
        break;

    case 171: /* simple_type_specifier  */
#line 345 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 3820 "pkl-tab.c"
        break;

    case 172: /* cons_type_specifier  */
#line 345 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 3848 "pkl-tab.c"
        break;

    case 173: /* integral_type_specifier  */
#line 345 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 3876 "pkl-tab.c"
        break;

    case 175: /* offset_type_specifier  */
#line 345 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 3904 "pkl-tab.c"
        break;

    case 176: /* array_type_specifier  */
#line 345 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 3932 "pkl-tab.c"
        break;

    case 177: /* function_type_specifier  */
#line 345 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 3960 "pkl-tab.c"
        break;

    case 178: /* function_type_arg_list  */
#line 345 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 3988 "pkl-tab.c"
        break;

    case 179: /* function_type_arg  */
#line 345 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 4016 "pkl-tab.c"
        break;

    case 180: /* struct_type_specifier  */
#line 345 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 4044 "pkl-tab.c"
        break;

    case 184: /* integral_struct  */
#line 345 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 4072 "pkl-tab.c"
        break;

    case 185: /* struct_type_elem_list  */
#line 345 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 4100 "pkl-tab.c"
        break;

    case 187: /* struct_type_field  */
#line 345 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 4128 "pkl-tab.c"
        break;

    case 189: /* struct_type_field_identifier  */
#line 345 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 4156 "pkl-tab.c"
        break;

    case 190: /* struct_type_field_label  */
#line 345 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 4184 "pkl-tab.c"
        break;

    case 192: /* struct_type_field_optcond  */
#line 345 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 4212 "pkl-tab.c"
        break;

    case 193: /* simple_declaration  */
#line 345 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 4240 "pkl-tab.c"
        break;

    case 194: /* declaration  */
#line 345 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 4268 "pkl-tab.c"
        break;

    case 197: /* defvar_list  */
#line 345 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 4296 "pkl-tab.c"
        break;

    case 198: /* defvar  */
#line 345 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 4324 "pkl-tab.c"
        break;

    case 199: /* deftype_list  */
#line 345 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 4352 "pkl-tab.c"
        break;

    case 200: /* deftype  */
#line 345 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 4380 "pkl-tab.c"
        break;

    case 201: /* defunit_list  */
#line 345 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 4408 "pkl-tab.c"
        break;

    case 202: /* defunit  */
#line 345 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 4436 "pkl-tab.c"
        break;

    case 203: /* comp_stmt  */
#line 345 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 4464 "pkl-tab.c"
        break;

    case 205: /* stmt_decl_list  */
#line 345 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 4492 "pkl-tab.c"
        break;

    case 207: /* simple_stmt_list  */
#line 345 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 4520 "pkl-tab.c"
        break;

    case 208: /* simple_stmt  */
#line 345 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 4548 "pkl-tab.c"
        break;

    case 209: /* stmt  */
#line 345 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 4576 "pkl-tab.c"
        break;

    case 212: /* print_stmt_arg_list  */
#line 345 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 4604 "pkl-tab.c"
        break;

    case 213: /* funcall_stmt  */
#line 345 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 4632 "pkl-tab.c"
        break;

    case 214: /* funcall_stmt_arg_list  */
#line 345 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 4660 "pkl-tab.c"
        break;

    case 215: /* funcall_stmt_arg  */
#line 345 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 4688 "pkl-tab.c"
        break;

      default:
        break;
    }
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}






/*----------.
| yyparse.  |
`----------*/

int
yyparse (struct pkl_parser *pkl_parser)
{
/* The lookahead symbol.  */
int yychar;


/* The semantic value of the lookahead symbol.  */
/* Default value used for initialization, for pacifying older GCCs
   or non-GCC compilers.  */
YY_INITIAL_VALUE (static YYSTYPE yyval_default;)
YYSTYPE yylval YY_INITIAL_VALUE (= yyval_default);

/* Location data for the lookahead symbol.  */
static YYLTYPE yyloc_default
# if defined PKL_TAB_LTYPE_IS_TRIVIAL && PKL_TAB_LTYPE_IS_TRIVIAL
  = { 1, 1, 1, 1 }
# endif
;
YYLTYPE yylloc = yyloc_default;

    /* Number of syntax errors so far.  */
    int yynerrs;

    yy_state_fast_t yystate;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus;

    /* The stacks and their tools:
       'yyss': related to states.
       'yyvs': related to semantic values.
       'yyls': related to locations.

       Refer to the stacks through separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* Their size.  */
    YYPTRDIFF_T yystacksize;

    /* The state stack.  */
    yy_state_t yyssa[YYINITDEPTH];
    yy_state_t *yyss;
    yy_state_t *yyssp;

    /* The semantic value stack.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs;
    YYSTYPE *yyvsp;

    /* The location stack.  */
    YYLTYPE yylsa[YYINITDEPTH];
    YYLTYPE *yyls;
    YYLTYPE *yylsp;

    yy_state_t yyesa[20];
    yy_state_t *yyes;
    YYPTRDIFF_T yyes_capacity;

  /* Whether LAC context is established.  A Boolean.  */
  int yy_lac_established = 0;
  int yyn;
  /* The return value of yyparse.  */
  int yyresult;
  /* Lookahead token as an internal (translated) token number.  */
  yysymbol_kind_t yytoken = YYSYMBOL_YYEMPTY;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;
  YYLTYPE yyloc;

  /* The locations where the error started and ended.  */
  YYLTYPE yyerror_range[3];



#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N), yylsp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  yynerrs = 0;
  yystate = 0;
  yyerrstatus = 0;

  yystacksize = YYINITDEPTH;
  yyssp = yyss = yyssa;
  yyvsp = yyvs = yyvsa;
  yylsp = yyls = yylsa;

  yyes = yyesa;
  yyes_capacity = 20;
  if (YYMAXDEPTH < yyes_capacity)
    yyes_capacity = YYMAXDEPTH;


  YYDPRINTF ((stderr, "Starting parse\n"));

  yychar = PKL_TAB_EMPTY; /* Cause a token to be read.  */

/* User initialization code.  */
#line 30 "pkl-tab.y"
{
    yylloc.first_line = yylloc.last_line = 1;
    yylloc.first_column = yylloc.last_column = 1;
}

#line 4813 "pkl-tab.c"

  yylsp[0] = yylloc;
  goto yysetstate;


/*------------------------------------------------------------.
| yynewstate -- push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;


/*--------------------------------------------------------------------.
| yysetstate -- set current state (the top of the stack) to yystate.  |
`--------------------------------------------------------------------*/
yysetstate:
  YYDPRINTF ((stderr, "Entering state %d\n", yystate));
  YY_ASSERT (0 <= yystate && yystate < YYNSTATES);
  YY_IGNORE_USELESS_CAST_BEGIN
  *yyssp = YY_CAST (yy_state_t, yystate);
  YY_IGNORE_USELESS_CAST_END
  YY_STACK_PRINT (yyss, yyssp);

  if (yyss + yystacksize - 1 <= yyssp)
#if !defined yyoverflow && !defined YYSTACK_RELOCATE
    goto yyexhaustedlab;
#else
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYPTRDIFF_T yysize = yyssp - yyss + 1;

# if defined yyoverflow
      {
        /* Give user a chance to reallocate the stack.  Use copies of
           these so that the &'s don't force the real ones into
           memory.  */
        yy_state_t *yyss1 = yyss;
        YYSTYPE *yyvs1 = yyvs;
        YYLTYPE *yyls1 = yyls;

        /* Each stack pointer address is followed by the size of the
           data in use in that stack, in bytes.  This used to be a
           conditional around just the two extra args, but that might
           be undefined if yyoverflow is a macro.  */
        yyoverflow (YY_("memory exhausted"),
                    &yyss1, yysize * YYSIZEOF (*yyssp),
                    &yyvs1, yysize * YYSIZEOF (*yyvsp),
                    &yyls1, yysize * YYSIZEOF (*yylsp),
                    &yystacksize);
        yyss = yyss1;
        yyvs = yyvs1;
        yyls = yyls1;
      }
# else /* defined YYSTACK_RELOCATE */
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
        goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
        yystacksize = YYMAXDEPTH;

      {
        yy_state_t *yyss1 = yyss;
        union yyalloc *yyptr =
          YY_CAST (union yyalloc *,
                   YYSTACK_ALLOC (YY_CAST (YYSIZE_T, YYSTACK_BYTES (yystacksize))));
        if (! yyptr)
          goto yyexhaustedlab;
        YYSTACK_RELOCATE (yyss_alloc, yyss);
        YYSTACK_RELOCATE (yyvs_alloc, yyvs);
        YYSTACK_RELOCATE (yyls_alloc, yyls);
#  undef YYSTACK_RELOCATE
        if (yyss1 != yyssa)
          YYSTACK_FREE (yyss1);
      }
# endif

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;
      yylsp = yyls + yysize - 1;

      YY_IGNORE_USELESS_CAST_BEGIN
      YYDPRINTF ((stderr, "Stack size increased to %ld\n",
                  YY_CAST (long, yystacksize)));
      YY_IGNORE_USELESS_CAST_END

      if (yyss + yystacksize - 1 <= yyssp)
        YYABORT;
    }
#endif /* !defined yyoverflow && !defined YYSTACK_RELOCATE */

  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;


/*-----------.
| yybackup.  |
`-----------*/
yybackup:
  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yypact_value_is_default (yyn))
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either empty, or end-of-input, or a valid lookahead.  */
  if (yychar == PKL_TAB_EMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token\n"));
      yychar = yylex (&yylval, &yylloc, scanner);
    }

  if (yychar <= PKL_TAB_EOF)
    {
      yychar = PKL_TAB_EOF;
      yytoken = YYSYMBOL_YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else if (yychar == PKL_TAB_error)
    {
      /* The scanner already issued an error message, process directly
         to error recovery.  But do not keep the error token as
         lookahead, it is too special and may lead us to an endless
         loop in error recovery. */
      yychar = PKL_TAB_UNDEF;
      yytoken = YYSYMBOL_YYerror;
      yyerror_range[1] = yylloc;
      goto yyerrlab1;
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    {
      YY_LAC_ESTABLISH;
      goto yydefault;
    }
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yytable_value_is_error (yyn))
        goto yyerrlab;
      YY_LAC_ESTABLISH;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);
  yystate = yyn;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END
  *++yylsp = yylloc;

  /* Discard the shifted token.  */
  yychar = PKL_TAB_EMPTY;
  YY_LAC_DISCARD ("shift");
  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     '$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];

  /* Default location. */
  YYLLOC_DEFAULT (yyloc, (yylsp - yylen), yylen);
  yyerror_range[1] = yyloc;
  YY_REDUCE_PRINT (yyn);
  {
    int yychar_backup = yychar;
    switch (yyn)
      {
  case 2:
#line 551 "pkl-tab.y"
                {
                  pkl_parser->env = pkl_env_push_frame (pkl_parser->env);
                }
#line 5034 "pkl-tab.c"
    break;

  case 3:
#line 566 "pkl-tab.y"
                  {
                  (yyval.ast) = pkl_ast_make_program (pkl_parser->ast, (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                  pkl_parser->ast->ast = ASTREF ((yyval.ast));
                }
#line 5044 "pkl-tab.c"
    break;

  case 4:
#line 572 "pkl-tab.y"
                  {
                  (yyval.ast) = pkl_ast_make_program (pkl_parser->ast, (yyvsp[-1].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                  pkl_parser->ast->ast = ASTREF ((yyval.ast));
                  YYACCEPT;
                }
#line 5055 "pkl-tab.c"
    break;

  case 5:
#line 579 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_program (pkl_parser->ast, (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                  pkl_parser->ast->ast = ASTREF ((yyval.ast));
                }
#line 5065 "pkl-tab.c"
    break;

  case 6:
#line 585 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_program (pkl_parser->ast, (yyvsp[-1].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                  pkl_parser->ast->ast = ASTREF ((yyval.ast));
                }
#line 5075 "pkl-tab.c"
    break;

  case 7:
#line 591 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_program (pkl_parser->ast, (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                  pkl_parser->ast->ast = ASTREF ((yyval.ast));
                }
#line 5085 "pkl-tab.c"
    break;

  case 8:
#line 597 "pkl-tab.y"
                {
                  /* This rule is to allow the presence of an extra
                     ';' after the sentence.  This to allow the poke
                     command manager to ease the handling of
                     semicolons in the command line.  */
                  (yyval.ast) = pkl_ast_make_program (pkl_parser->ast, (yyvsp[-1].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                  pkl_parser->ast->ast = ASTREF ((yyval.ast));
                }
#line 5099 "pkl-tab.c"
    break;

  case 9:
#line 607 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_program (pkl_parser->ast, (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                  pkl_parser->ast->ast = ASTREF ((yyval.ast));
                }
#line 5109 "pkl-tab.c"
    break;

  case 10:
#line 613 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_program (pkl_parser->ast, (yyvsp[-1].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                  pkl_parser->ast->ast = ASTREF ((yyval.ast));
                }
#line 5119 "pkl-tab.c"
    break;

  case 11:
#line 619 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_program (pkl_parser->ast, (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                  pkl_parser->ast->ast = ASTREF ((yyval.ast));
                }
#line 5129 "pkl-tab.c"
    break;

  case 12:
#line 628 "pkl-tab.y"
                {
                  (yyval.ast) = NULL;
                }
#line 5137 "pkl-tab.c"
    break;

  case 15:
#line 637 "pkl-tab.y"
                {
                  if ((yyvsp[0].ast) != NULL)
                    (yyval.ast) = pkl_ast_chainon ((yyvsp[-1].ast), (yyvsp[0].ast));
                  else
                    (yyval.ast) = (yyvsp[-1].ast);
                }
#line 5148 "pkl-tab.c"
    break;

  case 19:
#line 653 "pkl-tab.y"
                {
                  char *filename = NULL;
                  int ret = load_module (pkl_parser,
                                         PKL_AST_IDENTIFIER_POINTER ((yyvsp[-1].ast)),
                                         &(yyval.ast), 0 /* filename_p */, &filename);
                  if (ret == 2)
                    /* The sub-parser should have emitted proper error
                       messages.  No need to be verbose here.  */
                    YYERROR;
                  else if (ret == 1)
                    {
                      pkl_error (pkl_parser->compiler, pkl_parser->ast, (yylsp[-1]),
                                 "cannot load `%s'",
                                 PKL_AST_IDENTIFIER_POINTER ((yyvsp[-1].ast)));
                      YYERROR;
                    }

                  /* Prepend and append SRC nodes to handle the change of
                     source files.  */
                  {
                      pkl_ast_node src1 = pkl_ast_make_src (pkl_parser->ast,
                                                            filename);
                      pkl_ast_node src2 = pkl_ast_make_src (pkl_parser->ast,
                                                            pkl_parser->filename);

                      (yyval.ast) = pkl_ast_chainon (src1, (yyval.ast));
                      (yyval.ast) = pkl_ast_chainon ((yyval.ast), src2);
                  }

                  (yyvsp[-1].ast) = ASTREF ((yyvsp[-1].ast));
                  pkl_ast_node_free ((yyvsp[-1].ast));
                  free (filename);
                }
#line 5186 "pkl-tab.c"
    break;

  case 20:
#line 687 "pkl-tab.y"
                {
                  char *filename = PKL_AST_STRING_POINTER ((yyvsp[-1].ast));
                  int ret = load_module (pkl_parser,
                                         filename,
                                         &(yyval.ast), 1 /* filename_p */, NULL);
                  if (ret == 2)
                    /* The sub-parser should have emitted proper error
                       messages.  No need to be verbose here.  */
                    YYERROR;
                  else if (ret == 1)
                    {
                      pkl_error (pkl_parser->compiler, pkl_parser->ast, (yylsp[-1]),
                                 "cannot load module from file `%s'",
                                 filename);
                      YYERROR;
                    }

                  /* Prepend and append SRC nodes to handle the change of
                     source files.  */
                  {
                      pkl_ast_node src1 = pkl_ast_make_src (pkl_parser->ast,
                                                            filename);
                      pkl_ast_node src2 = pkl_ast_make_src (pkl_parser->ast,
                                                            pkl_parser->filename);

                      (yyval.ast) = pkl_ast_chainon (src1, (yyval.ast));
                      (yyval.ast) = pkl_ast_chainon ((yyval.ast), src2);
                  }

                  (yyvsp[-1].ast) = ASTREF ((yyvsp[-1].ast));
                  pkl_ast_node_free ((yyvsp[-1].ast));
                }
#line 5223 "pkl-tab.c"
    break;

  case 23:
#line 736 "pkl-tab.y"
                  { (yyval.ast) = NULL; }
#line 5229 "pkl-tab.c"
    break;

  case 25:
#line 739 "pkl-tab.y"
                  {
                    (yyval.ast) = pkl_ast_chainon ((yyvsp[-2].ast), (yyvsp[0].ast));
                  }
#line 5237 "pkl-tab.c"
    break;

  case 26:
#line 745 "pkl-tab.y"
                 { (yyval.ast) = NULL; }
#line 5243 "pkl-tab.c"
    break;

  case 29:
#line 752 "pkl-tab.y"
                  {
                  (yyval.ast) = pkl_ast_make_unary_exp (pkl_parser->ast,
                                               (yyvsp[-1].opcode), (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yylsp[-1]);
                }
#line 5253 "pkl-tab.c"
    break;

  case 30:
#line 758 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_unary_exp (pkl_parser->ast, PKL_AST_OP_SIZEOF, (yyvsp[-1].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yylsp[-3]);
                }
#line 5262 "pkl-tab.c"
    break;

  case 31:
#line 763 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_binary_exp (pkl_parser->ast, PKL_AST_OP_ATTR,
                                                (yyvsp[-1].ast), (yyvsp[0].ast));
                  PKL_AST_LOC ((yyvsp[0].ast)) = (yylsp[0]);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 5273 "pkl-tab.c"
    break;

  case 32:
#line 770 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_binary_exp (pkl_parser->ast, PKL_AST_OP_ADD,
                                                (yyvsp[-2].ast), (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 5283 "pkl-tab.c"
    break;

  case 33:
#line 776 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_binary_exp (pkl_parser->ast, PKL_AST_OP_SUB,
                                                (yyvsp[-2].ast), (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 5293 "pkl-tab.c"
    break;

  case 34:
#line 782 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_binary_exp (pkl_parser->ast, PKL_AST_OP_MUL,
                                                (yyvsp[-2].ast), (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 5303 "pkl-tab.c"
    break;

  case 35:
#line 788 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_binary_exp (pkl_parser->ast, PKL_AST_OP_DIV,
                                                (yyvsp[-2].ast), (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 5313 "pkl-tab.c"
    break;

  case 36:
#line 794 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_binary_exp (pkl_parser->ast, PKL_AST_OP_CEILDIV, (yyvsp[-2].ast), (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 5322 "pkl-tab.c"
    break;

  case 37:
#line 799 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_binary_exp (pkl_parser->ast, PKL_AST_OP_POW, (yyvsp[-2].ast), (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 5331 "pkl-tab.c"
    break;

  case 38:
#line 804 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_binary_exp (pkl_parser->ast, PKL_AST_OP_MOD,
                                                (yyvsp[-2].ast), (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 5341 "pkl-tab.c"
    break;

  case 39:
#line 810 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_binary_exp (pkl_parser->ast, PKL_AST_OP_SL,
                                                (yyvsp[-2].ast), (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 5351 "pkl-tab.c"
    break;

  case 40:
#line 816 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_binary_exp (pkl_parser->ast, PKL_AST_OP_SR,
                                                (yyvsp[-2].ast), (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 5361 "pkl-tab.c"
    break;

  case 41:
#line 822 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_binary_exp (pkl_parser->ast, PKL_AST_OP_EQ,
                                                (yyvsp[-2].ast), (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 5371 "pkl-tab.c"
    break;

  case 42:
#line 828 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_binary_exp (pkl_parser->ast, PKL_AST_OP_NE,
                                                (yyvsp[-2].ast), (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 5381 "pkl-tab.c"
    break;

  case 43:
#line 834 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_binary_exp (pkl_parser->ast, PKL_AST_OP_LT,
                                                (yyvsp[-2].ast), (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 5391 "pkl-tab.c"
    break;

  case 44:
#line 840 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_binary_exp (pkl_parser->ast, PKL_AST_OP_GT,
                                                (yyvsp[-2].ast), (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 5401 "pkl-tab.c"
    break;

  case 45:
#line 846 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_binary_exp (pkl_parser->ast, PKL_AST_OP_LE,
                                                (yyvsp[-2].ast), (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 5411 "pkl-tab.c"
    break;

  case 46:
#line 852 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_binary_exp (pkl_parser->ast, PKL_AST_OP_GE,
                                                (yyvsp[-2].ast), (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 5421 "pkl-tab.c"
    break;

  case 47:
#line 858 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_binary_exp (pkl_parser->ast, PKL_AST_OP_IOR,
                                                (yyvsp[-2].ast), (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 5431 "pkl-tab.c"
    break;

  case 48:
#line 864 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_binary_exp (pkl_parser->ast, PKL_AST_OP_XOR,
                                                (yyvsp[-2].ast), (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 5441 "pkl-tab.c"
    break;

  case 49:
#line 870 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_binary_exp (pkl_parser->ast, PKL_AST_OP_BAND,
                                                (yyvsp[-2].ast), (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 5451 "pkl-tab.c"
    break;

  case 50:
#line 876 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_binary_exp (pkl_parser->ast, PKL_AST_OP_AND,
                                                (yyvsp[-2].ast), (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 5461 "pkl-tab.c"
    break;

  case 51:
#line 882 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_binary_exp (pkl_parser->ast, PKL_AST_OP_OR,
                                                (yyvsp[-2].ast), (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 5471 "pkl-tab.c"
    break;

  case 52:
#line 888 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_binary_exp (pkl_parser->ast, PKL_AST_OP_IN,
                                                (yyvsp[-2].ast), (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 5481 "pkl-tab.c"
    break;

  case 53:
#line 894 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_cast (pkl_parser->ast, (yyvsp[0].ast), (yyvsp[-2].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 5490 "pkl-tab.c"
    break;

  case 54:
#line 899 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_isa (pkl_parser->ast, (yyvsp[0].ast), (yyvsp[-2].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 5499 "pkl-tab.c"
    break;

  case 55:
#line 904 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_cond_exp (pkl_parser->ast,
                                              (yyvsp[-4].ast), (yyvsp[-2].ast), (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 5509 "pkl-tab.c"
    break;

  case 56:
#line 910 "pkl-tab.y"
                {
                  /* This syntax is only used for array
                     constructors.  */
                  if (PKL_AST_TYPE_CODE ((yyvsp[-4].ast)) != PKL_TYPE_ARRAY)
                    {
                      pkl_error (pkl_parser->compiler, pkl_parser->ast, (yylsp[-4]),
                                 "expected array type in constructor");
                      YYERROR;
                    }

                  (yyval.ast) = pkl_ast_make_cons (pkl_parser->ast, (yyvsp[-4].ast), (yyvsp[-2].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 5527 "pkl-tab.c"
    break;

  case 57:
#line 924 "pkl-tab.y"
                {
                  pkl_ast_node astruct;

                  /* This syntax is only used for struct
                     constructors.  */
                  if (PKL_AST_TYPE_CODE ((yyvsp[-4].ast)) != PKL_TYPE_STRUCT)
                    {
                      pkl_error (pkl_parser->compiler, pkl_parser->ast, (yylsp[-4]),
                                 "expected struct type in constructor");
                      YYERROR;
                    }

                  astruct = pkl_ast_make_struct (pkl_parser->ast,
                                           0 /* nelem */, (yyvsp[-2].ast));
                  PKL_AST_LOC (astruct) = (yyloc);

                  (yyval.ast) = pkl_ast_make_cons (pkl_parser->ast, (yyvsp[-4].ast), astruct);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 5551 "pkl-tab.c"
    break;

  case 58:
#line 944 "pkl-tab.y"
                {
                  if ((yyvsp[0].ast) == NULL)
                    {
                      pkl_error (pkl_parser->compiler, pkl_parser->ast, (yylsp[0]),
                                 "invalid unit in offset");
                      YYERROR;
                    }

                    (yyval.ast) = pkl_ast_make_offset (pkl_parser->ast, NULL, (yyvsp[0].ast));
                    PKL_AST_LOC ((yyvsp[0].ast)) = (yylsp[0]);
                    if (PKL_AST_TYPE ((yyvsp[0].ast)))
                        PKL_AST_LOC (PKL_AST_TYPE ((yyvsp[0].ast))) = (yylsp[0]);
                    PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 5570 "pkl-tab.c"
    break;

  case 59:
#line 959 "pkl-tab.y"
                {
                  if ((yyvsp[0].ast) == NULL)
                    {
                      pkl_error (pkl_parser->compiler, pkl_parser->ast, (yylsp[0]),
                                 "invalid unit in offset");
                      YYERROR;
                    }

                    (yyval.ast) = pkl_ast_make_offset (pkl_parser->ast, (yyvsp[-1].ast), (yyvsp[0].ast));
                    PKL_AST_LOC ((yyvsp[0].ast)) = (yylsp[0]);
                    if (PKL_AST_TYPE ((yyvsp[0].ast)))
                        PKL_AST_LOC (PKL_AST_TYPE ((yyvsp[0].ast))) = (yylsp[0]);
                    PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 5589 "pkl-tab.c"
    break;

  case 60:
#line 974 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_incrdecr (pkl_parser->ast, (yyvsp[0].ast),
                                              PKL_AST_ORDER_PRE, PKL_AST_SIGN_INCR);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 5599 "pkl-tab.c"
    break;

  case 61:
#line 980 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_incrdecr (pkl_parser->ast, (yyvsp[0].ast),
                                              PKL_AST_ORDER_PRE, PKL_AST_SIGN_DECR);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 5609 "pkl-tab.c"
    break;

  case 64:
#line 991 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_binary_exp (pkl_parser->ast, PKL_AST_OP_BCONC,
                                                (yyvsp[-2].ast), (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 5619 "pkl-tab.c"
    break;

  case 65:
#line 999 "pkl-tab.y"
             { (yyval.integer) = 1; }
#line 5625 "pkl-tab.c"
    break;

  case 66:
#line 1000 "pkl-tab.y"
                { (yyval.integer) = 0; }
#line 5631 "pkl-tab.c"
    break;

  case 67:
#line 1005 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_map (pkl_parser->ast, (yyvsp[-1].integer),
                                         (yyvsp[-2].ast), NULL, (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 5641 "pkl-tab.c"
    break;

  case 68:
#line 1011 "pkl-tab.y"
                 {
                   (yyval.ast) = pkl_ast_make_map (pkl_parser->ast, (yyvsp[-3].integer),
                                          (yyvsp[-4].ast), (yyvsp[-2].ast), (yyvsp[0].ast));
                   PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 5651 "pkl-tab.c"
    break;

  case 69:
#line 1019 "pkl-tab.y"
                             { (yyval.opcode) = PKL_AST_OP_NEG; }
#line 5657 "pkl-tab.c"
    break;

  case 70:
#line 1020 "pkl-tab.y"
                             { (yyval.opcode) = PKL_AST_OP_POS; }
#line 5663 "pkl-tab.c"
    break;

  case 71:
#line 1021 "pkl-tab.y"
                             { (yyval.opcode) = PKL_AST_OP_BNOT; }
#line 5669 "pkl-tab.c"
    break;

  case 72:
#line 1022 "pkl-tab.y"
                             { (yyval.opcode) = PKL_AST_OP_NOT; }
#line 5675 "pkl-tab.c"
    break;

  case 73:
#line 1023 "pkl-tab.y"
                               { (yyval.opcode) = PKL_AST_OP_UNMAP; }
#line 5681 "pkl-tab.c"
    break;

  case 74:
#line 1028 "pkl-tab.y"
                  {
                  /* Search for a variable definition in the
                     compile-time environment, and create a
                     PKL_AST_VAR node with it's lexical environment,
                     annotated with its initialization.  */

                  int back, over;
                  const char *name = PKL_AST_IDENTIFIER_POINTER ((yyvsp[0].ast));

                  pkl_ast_node decl
                    = pkl_env_lookup (pkl_parser->env,
                                      PKL_ENV_NS_MAIN,
                                      name, &back, &over);
                  if (!decl
                      || (PKL_AST_DECL_KIND (decl) != PKL_AST_DECL_KIND_VAR
                          && PKL_AST_DECL_KIND (decl) != PKL_AST_DECL_KIND_FUNC))
                    {
                      pkl_error (pkl_parser->compiler, pkl_parser->ast, (yylsp[0]),
                                 "undefined variable '%s'", name);
                      YYERROR;
                    }

                  (yyval.ast) = pkl_ast_make_var (pkl_parser->ast,
                                         (yyvsp[0].ast), /* name.  */
                                         decl,
                                         back, over);
                  PKL_AST_LOC ((yyval.ast)) = (yylsp[0]);
                }
#line 5714 "pkl-tab.c"
    break;

  case 75:
#line 1057 "pkl-tab.y"
                {
                  (yyval.ast) = (yyvsp[0].ast);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                  PKL_AST_LOC (PKL_AST_TYPE ((yyval.ast))) = (yyloc);
                }
#line 5724 "pkl-tab.c"
    break;

  case 76:
#line 1063 "pkl-tab.y"
                {
                  (yyval.ast) = NULL; /* To avoid bison warning.  */
                  pkl_error (pkl_parser->compiler, pkl_parser->ast, (yylsp[0]),
                             "integer literal is too big");
                  YYERROR;
                }
#line 5735 "pkl-tab.c"
    break;

  case 77:
#line 1070 "pkl-tab.y"
                {
                  (yyval.ast) = (yyvsp[0].ast);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                  PKL_AST_LOC (PKL_AST_TYPE ((yyval.ast))) = (yyloc);
                }
#line 5745 "pkl-tab.c"
    break;

  case 78:
#line 1076 "pkl-tab.y"
                {
                  (yyval.ast) = (yyvsp[0].ast);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                  PKL_AST_LOC (PKL_AST_TYPE ((yyval.ast))) = (yyloc);
                }
#line 5755 "pkl-tab.c"
    break;

  case 79:
#line 1082 "pkl-tab.y"
                {
                  if (PKL_AST_CODE ((yyvsp[-1].ast)) == PKL_AST_VAR)
                    PKL_AST_VAR_IS_PARENTHESIZED ((yyvsp[-1].ast)) = 1;
                  else if (PKL_AST_CODE ((yyvsp[-1].ast)) == PKL_AST_STRUCT_REF)
                    PKL_AST_STRUCT_REF_IS_PARENTHESIZED ((yyvsp[-1].ast)) = 1;
                  (yyval.ast) = (yyvsp[-1].ast);
                }
#line 5767 "pkl-tab.c"
    break;

  case 81:
#line 1091 "pkl-tab.y"
                {
                    (yyval.ast) = pkl_ast_make_struct_ref (pkl_parser->ast, (yyvsp[-2].ast), (yyvsp[0].ast));
                    PKL_AST_LOC ((yyvsp[0].ast)) = (yylsp[0]);
                    PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 5777 "pkl-tab.c"
    break;

  case 82:
#line 1097 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_indexer (pkl_parser->ast, (yyvsp[-3].ast), (yyvsp[-1].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 5786 "pkl-tab.c"
    break;

  case 83:
#line 1102 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_trimmer (pkl_parser->ast,
                                             (yyvsp[-5].ast), (yyvsp[-3].ast), NULL, (yyvsp[-1].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 5796 "pkl-tab.c"
    break;

  case 84:
#line 1108 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_trimmer (pkl_parser->ast,
                                             (yyvsp[-5].ast), (yyvsp[-3].ast), (yyvsp[-1].ast), NULL);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 5806 "pkl-tab.c"
    break;

  case 85:
#line 1114 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_trimmer (pkl_parser->ast,
                                             (yyvsp[-3].ast), NULL, NULL, NULL);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 5816 "pkl-tab.c"
    break;

  case 86:
#line 1120 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_trimmer (pkl_parser->ast,
                                             (yyvsp[-4].ast), NULL, (yyvsp[-1].ast), NULL);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 5826 "pkl-tab.c"
    break;

  case 87:
#line 1126 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_trimmer (pkl_parser->ast,
                                             (yyvsp[-4].ast), (yyvsp[-2].ast), NULL, NULL);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 5836 "pkl-tab.c"
    break;

  case 89:
#line 1133 "pkl-tab.y"
                {
                  (yyval.ast) = (yyvsp[-1].ast);
                }
#line 5844 "pkl-tab.c"
    break;

  case 90:
#line 1137 "pkl-tab.y"
                {
                  /* function_specifier needs to know whether we are
                     in a function declaration or a method
                     declaration.  */
                  pkl_parser->in_method_decl_p = 0;
                }
#line 5855 "pkl-tab.c"
    break;

  case 91:
#line 1144 "pkl-tab.y"
                {
                  /* Annotate the contained RETURN statements with
                     their function and their lexical nest level
                     within the function.  */
                  pkl_ast_finish_returns ((yyvsp[0].ast));
                  (yyval.ast) = pkl_ast_make_lambda (pkl_parser->ast, (yyvsp[0].ast));
                }
#line 5867 "pkl-tab.c"
    break;

  case 92:
#line 1152 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_incrdecr (pkl_parser->ast, (yyvsp[-1].ast),
                                              PKL_AST_ORDER_POST, PKL_AST_SIGN_INCR);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 5877 "pkl-tab.c"
    break;

  case 93:
#line 1158 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_incrdecr (pkl_parser->ast, (yyvsp[-1].ast),
                                              PKL_AST_ORDER_POST, PKL_AST_SIGN_DECR);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 5887 "pkl-tab.c"
    break;

  case 94:
#line 1167 "pkl-tab.y"
                  {
                  (yyval.ast) = pkl_ast_make_funcall (pkl_parser->ast,
                                             (yyvsp[-3].ast), (yyvsp[-1].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 5897 "pkl-tab.c"
    break;

  case 95:
#line 1176 "pkl-tab.y"
                { (yyval.ast) = NULL; }
#line 5903 "pkl-tab.c"
    break;

  case 97:
#line 1179 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_chainon ((yyvsp[-2].ast), (yyvsp[0].ast));
                }
#line 5911 "pkl-tab.c"
    break;

  case 98:
#line 1186 "pkl-tab.y"
                  {
                  (yyval.ast) = pkl_ast_make_funcall_arg (pkl_parser->ast,
                                                 (yyvsp[0].ast), NULL /* name */);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 5921 "pkl-tab.c"
    break;

  case 101:
#line 1200 "pkl-tab.y"
                { (yyval.ast) = NULL; }
#line 5927 "pkl-tab.c"
    break;

  case 103:
#line 1203 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_chainon ((yyvsp[-2].ast), (yyvsp[0].ast));
                }
#line 5935 "pkl-tab.c"
    break;

  case 104:
#line 1210 "pkl-tab.y"
                  {
                    (yyval.ast) = pkl_ast_make_struct_field (pkl_parser->ast,
                                                    NULL /* name */,
                                                    (yyvsp[0].ast));
                    PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 5946 "pkl-tab.c"
    break;

  case 105:
#line 1217 "pkl-tab.y"
                {
                    (yyval.ast) = pkl_ast_make_struct_field (pkl_parser->ast,
                                                    (yyvsp[-2].ast),
                                                    (yyvsp[0].ast));
                    PKL_AST_LOC ((yyvsp[-2].ast)) = (yylsp[-2]);
                    PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 5958 "pkl-tab.c"
    break;

  case 106:
#line 1228 "pkl-tab.y"
                {
                    (yyval.ast) = pkl_ast_make_array (pkl_parser->ast,
                                             0 /* nelem */,
                                             0 /* ninitializer */,
                                             (yyvsp[-2].ast));
                    PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 5970 "pkl-tab.c"
    break;

  case 108:
#line 1240 "pkl-tab.y"
                  {
                  (yyval.ast) = pkl_ast_chainon ((yyvsp[-2].ast), (yyvsp[0].ast));
                }
#line 5978 "pkl-tab.c"
    break;

  case 109:
#line 1247 "pkl-tab.y"
                  {
                    (yyval.ast) = pkl_ast_make_array_initializer (pkl_parser->ast,
                                                         NULL, (yyvsp[0].ast));
                    PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 5988 "pkl-tab.c"
    break;

  case 110:
#line 1253 "pkl-tab.y"
                {
                    (yyval.ast) = pkl_ast_make_array_initializer (pkl_parser->ast,
                                                         (yyvsp[-3].ast), (yyvsp[0].ast));
                    PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 5998 "pkl-tab.c"
    break;

  case 111:
#line 1266 "pkl-tab.y"
                {
                  /* Push the lexical frame for the function's
                     arguments.  */
                  pkl_parser->env = pkl_env_push_frame (pkl_parser->env);

                  /* If in a method, register a dummy for the initial
                     implicit argument.  */
                  if (pkl_parser->in_method_decl_p)
                    pkl_register_dummies (pkl_parser, 1);
                }
#line 6013 "pkl-tab.c"
    break;

  case 112:
#line 1280 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_func (pkl_parser->ast,
                                          (yyvsp[-2].ast), (yyvsp[-4].ast), (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);

                  /* Pop the frame introduced by `pushlevel'
                     above.  */
                  pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);
                }
#line 6027 "pkl-tab.c"
    break;

  case 113:
#line 1290 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_func (pkl_parser->ast,
                                          (yyvsp[-3].ast), NULL, (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);

                  /* Pop the frame introduced by `pushlevel'
                     above.  */
                  pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);
                }
#line 6041 "pkl-tab.c"
    break;

  case 115:
#line 1304 "pkl-tab.y"
                  {
                  (yyval.ast) = pkl_ast_chainon ((yyvsp[-2].ast), (yyvsp[0].ast));
                }
#line 6049 "pkl-tab.c"
    break;

  case 116:
#line 1311 "pkl-tab.y"
                  {
                  (yyval.ast) = pkl_ast_make_func_arg (pkl_parser->ast,
                                              (yyvsp[-2].ast), (yyvsp[-1].ast), (yyvsp[0].ast));
                  PKL_AST_LOC ((yyvsp[-1].ast)) = (yylsp[-1]);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);

                  if (!pkl_register_arg (pkl_parser, (yyval.ast)))
                      YYERROR;
                }
#line 6063 "pkl-tab.c"
    break;

  case 117:
#line 1321 "pkl-tab.y"
                {
                  pkl_ast_node type
                    = pkl_ast_make_any_type (pkl_parser->ast);
                  pkl_ast_node array_type
                    = pkl_ast_make_array_type (pkl_parser->ast,
                                               type,
                                               NULL /* bound */);

                  PKL_AST_LOC (type) = (yylsp[-1]);
                  PKL_AST_LOC (array_type) = (yylsp[-1]);

                  (yyval.ast) = pkl_ast_make_func_arg (pkl_parser->ast,
                                              array_type,
                                              (yyvsp[-1].ast),
                                              NULL /* initial */);
                  PKL_AST_FUNC_ARG_VARARG ((yyval.ast)) = 1;
                  PKL_AST_LOC ((yyvsp[-1].ast)) = (yylsp[-1]);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);

                  if (!pkl_register_arg (pkl_parser, (yyval.ast)))
                      YYERROR;
                }
#line 6090 "pkl-tab.c"
    break;

  case 118:
#line 1346 "pkl-tab.y"
                                      { (yyval.ast) = NULL; }
#line 6096 "pkl-tab.c"
    break;

  case 119:
#line 1347 "pkl-tab.y"
                                { (yyval.ast) = (yyvsp[0].ast); }
#line 6102 "pkl-tab.c"
    break;

  case 123:
#line 1362 "pkl-tab.y"
                  {
                  pkl_ast_node decl = pkl_env_lookup (pkl_parser->env,
                                                      PKL_ENV_NS_MAIN,
                                                      PKL_AST_IDENTIFIER_POINTER ((yyvsp[0].ast)),
                                                      NULL, NULL);
                  assert (decl != NULL
                          && PKL_AST_DECL_KIND (decl) == PKL_AST_DECL_KIND_TYPE);
                  (yyval.ast) = PKL_AST_DECL_INITIAL (decl);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                  (yyvsp[0].ast) = ASTREF ((yyvsp[0].ast)); pkl_ast_node_free ((yyvsp[0].ast));
                }
#line 6118 "pkl-tab.c"
    break;

  case 124:
#line 1377 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_string_type (pkl_parser->ast);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 6127 "pkl-tab.c"
    break;

  case 125:
#line 1385 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_any_type (pkl_parser->ast);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 6136 "pkl-tab.c"
    break;

  case 126:
#line 1390 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_void_type (pkl_parser->ast);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 6145 "pkl-tab.c"
    break;

  case 135:
#line 1409 "pkl-tab.y"
                {
                    (yyval.ast) = pkl_ast_make_integral_type (pkl_parser->ast,
                                                     PKL_AST_INTEGER_VALUE ((yyvsp[-1].ast)),
                                                     (yyvsp[-2].integer));
                    (yyvsp[-1].ast) = ASTREF ((yyvsp[-1].ast)); pkl_ast_node_free ((yyvsp[-1].ast));
                    PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 6157 "pkl-tab.c"
    break;

  case 136:
#line 1419 "pkl-tab.y"
                           { (yyval.integer) = 1; }
#line 6163 "pkl-tab.c"
    break;

  case 137:
#line 1420 "pkl-tab.y"
                            { (yyval.integer) = 0; }
#line 6169 "pkl-tab.c"
    break;

  case 138:
#line 1425 "pkl-tab.y"
                {
                  pkl_ast_node decl
                    = pkl_env_lookup (pkl_parser->env,
                                      PKL_ENV_NS_UNITS,
                                      PKL_AST_IDENTIFIER_POINTER ((yyvsp[-1].ast)),
                                      NULL, NULL);

                  if (!decl)
                    {
                      /* This could be the name of a type.  Try it out.  */
                      decl = pkl_env_lookup (pkl_parser->env,
                                             PKL_ENV_NS_MAIN,
                                             PKL_AST_IDENTIFIER_POINTER ((yyvsp[-1].ast)),
                                             NULL, NULL);

                      if (!decl)
                        {
                          pkl_error (pkl_parser->compiler, pkl_parser->ast, (yylsp[-1]),
                                     "invalid unit in offset type");
                          YYERROR;
                        }
                    }

                  (yyval.ast) = pkl_ast_make_offset_type (pkl_parser->ast,
                                                 (yyvsp[-3].ast),
                                                 PKL_AST_DECL_INITIAL (decl));

                  (yyvsp[-1].ast) = ASTREF ((yyvsp[-1].ast)); pkl_ast_node_free ((yyvsp[-1].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 6204 "pkl-tab.c"
    break;

  case 139:
#line 1456 "pkl-tab.y"
                {
                    (yyval.ast) = pkl_ast_make_offset_type (pkl_parser->ast,
                                                   (yyvsp[-3].ast), (yyvsp[-1].ast));
                    PKL_AST_LOC (PKL_AST_TYPE ((yyvsp[-1].ast))) = (yylsp[-1]);
                    PKL_AST_LOC ((yyvsp[-1].ast)) = (yylsp[-1]);
                    PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 6216 "pkl-tab.c"
    break;

  case 140:
#line 1467 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_array_type (pkl_parser->ast, (yyvsp[-2].ast),
                                                NULL /* bound */);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 6226 "pkl-tab.c"
    break;

  case 141:
#line 1473 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_array_type (pkl_parser->ast, (yyvsp[-3].ast), (yyvsp[-1].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 6235 "pkl-tab.c"
    break;

  case 142:
#line 1481 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_function_type (pkl_parser->ast,
                                                   (yyvsp[0].ast), 0 /* narg */,
                                                   (yyvsp[-2].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 6246 "pkl-tab.c"
    break;

  case 143:
#line 1488 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_function_type (pkl_parser->ast,
                                                   (yyvsp[0].ast), 0 /* narg */,
                                                   NULL);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 6257 "pkl-tab.c"
    break;

  case 145:
#line 1499 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_chainon ((yyvsp[-2].ast), (yyvsp[0].ast));
                }
#line 6265 "pkl-tab.c"
    break;

  case 146:
#line 1506 "pkl-tab.y"
                  {
                  (yyval.ast) = pkl_ast_make_func_type_arg (pkl_parser->ast,
                                                   (yyvsp[0].ast), NULL /* name */);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 6275 "pkl-tab.c"
    break;

  case 147:
#line 1512 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_func_type_arg (pkl_parser->ast,
                                                   (yyvsp[-1].ast), NULL /* name */);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                  PKL_AST_FUNC_TYPE_ARG_OPTIONAL ((yyval.ast)) = 1;
                }
#line 6286 "pkl-tab.c"
    break;

  case 148:
#line 1519 "pkl-tab.y"
                {
                  pkl_ast_node type
                    = pkl_ast_make_any_type (pkl_parser->ast);
                  pkl_ast_node array_type
                    = pkl_ast_make_array_type (pkl_parser->ast,
                                               type, NULL /* bound */);

                  PKL_AST_LOC (type) = (yylsp[0]);
                  PKL_AST_LOC (array_type) = (yylsp[0]);

                  (yyval.ast) = pkl_ast_make_func_type_arg (pkl_parser->ast,
                                                   array_type, NULL /* name */);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                  PKL_AST_FUNC_TYPE_ARG_VARARG ((yyval.ast)) = 1;
                }
#line 6306 "pkl-tab.c"
    break;

  case 149:
#line 1539 "pkl-tab.y"
                  {
                    (yyval.ast) = pkl_ast_make_struct_type (pkl_parser->ast,
                                                   0 /* nelem */,
                                                   0 /* nfield */,
                                                   0 /* ndecl */,
                                                   (yyvsp[-2].ast),
                                                   NULL /* elems */,
                                                   (yyvsp[-4].integer), (yyvsp[-3].integer));
                    PKL_AST_LOC ((yyval.ast)) = (yyloc);

                    /* The pushlevel in this rule and the subsequent
                       pop_frame, while not strictly needed, is to
                       avoid shift/reduce conflicts with the next
                       rule.  */
                    pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);
                }
#line 6327 "pkl-tab.c"
    break;

  case 150:
#line 1557 "pkl-tab.y"
                {
                  /* Register dummies for the locals used in
                     pkl-gen.pks:struct_mapper (not counting
                     OFFSET).  */
                  pkl_register_dummies (pkl_parser, 5);

                  /* Now register OFFSET with a type of
                     offset<uint<64>,1> */
                  {
                    pkl_ast_node decl, type;
                    pkl_ast_node offset_identifier
                      = pkl_ast_make_identifier (pkl_parser->ast, "OFFSET");
                    pkl_ast_node offset_magnitude
                      = pkl_ast_make_integer (pkl_parser->ast, 0);
                    pkl_ast_node offset_unit
                      = pkl_ast_make_integer (pkl_parser->ast, 1);
                    pkl_ast_node offset;

                    type = pkl_ast_make_integral_type (pkl_parser->ast, 64, 0);
                    PKL_AST_TYPE (offset_magnitude) = ASTREF (type);
                    PKL_AST_TYPE (offset_unit) = ASTREF (type);

                    offset = pkl_ast_make_offset (pkl_parser->ast,
                                                  offset_magnitude,
                                                  offset_unit);
                    type = pkl_ast_make_offset_type (pkl_parser->ast,
                                                     type,
                                                     offset_unit);
                    PKL_AST_TYPE (offset) = ASTREF (type);

                    decl = pkl_ast_make_decl (pkl_parser->ast,
                                              PKL_AST_DECL_KIND_VAR,
                                              offset_identifier,
                                              offset,
                                              NULL /* source */);

                    if (!pkl_env_register (pkl_parser->env,
                                           PKL_ENV_NS_MAIN,
                                           PKL_AST_IDENTIFIER_POINTER (offset_identifier),
                                           decl))
                      assert (0);
                  }
                }
#line 6375 "pkl-tab.c"
    break;

  case 151:
#line 1601 "pkl-tab.y"
                {
                    (yyval.ast) = pkl_ast_make_struct_type (pkl_parser->ast,
                                                   0 /* nelem */,
                                                   0 /* nfield */,
                                                   0 /* ndecl */,
                                                   (yyvsp[-4].ast),
                                                   (yyvsp[-1].ast),
                                                   (yyvsp[-6].integer), (yyvsp[-5].integer));
                    PKL_AST_LOC ((yyval.ast)) = (yyloc);

                    /* Pop the frame pushed in the `pushlevel' above.  */
                    pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);
                }
#line 6393 "pkl-tab.c"
    break;

  case 152:
#line 1617 "pkl-tab.y"
                        { (yyval.integer) = 0; }
#line 6399 "pkl-tab.c"
    break;

  case 153:
#line 1618 "pkl-tab.y"
                        { (yyval.integer) = 1; }
#line 6405 "pkl-tab.c"
    break;

  case 154:
#line 1622 "pkl-tab.y"
                        { (yyval.integer) = 0; }
#line 6411 "pkl-tab.c"
    break;

  case 155:
#line 1623 "pkl-tab.y"
                        { (yyval.integer) = 1; }
#line 6417 "pkl-tab.c"
    break;

  case 156:
#line 1627 "pkl-tab.y"
                         { (yyval.ast) = NULL; }
#line 6423 "pkl-tab.c"
    break;

  case 157:
#line 1628 "pkl-tab.y"
                                { (yyval.ast) = (yyvsp[0].ast); }
#line 6429 "pkl-tab.c"
    break;

  case 160:
#line 1635 "pkl-tab.y"
                  { (yyval.ast) = pkl_ast_chainon ((yyvsp[-1].ast), (yyvsp[0].ast)); }
#line 6435 "pkl-tab.c"
    break;

  case 161:
#line 1637 "pkl-tab.y"
                { (yyval.ast) = pkl_ast_chainon ((yyvsp[-1].ast), (yyvsp[0].ast)); }
#line 6441 "pkl-tab.c"
    break;

  case 162:
#line 1641 "pkl-tab.y"
                        { (yyval.integer) = PKL_AST_ENDIAN_DFL; }
#line 6447 "pkl-tab.c"
    break;

  case 163:
#line 1642 "pkl-tab.y"
                        { (yyval.integer) = PKL_AST_ENDIAN_LSB; }
#line 6453 "pkl-tab.c"
    break;

  case 164:
#line 1643 "pkl-tab.y"
                             { (yyval.integer) = PKL_AST_ENDIAN_MSB; }
#line 6459 "pkl-tab.c"
    break;

  case 165:
#line 1648 "pkl-tab.y"
                  {
                    /* Register a variable in the current environment
                       for the field.  We do it in this mid-rule so
                       the element can be used in the constraint.  */

                    pkl_ast_node dummy, decl;
                    pkl_ast_node identifier
                      = ((yyvsp[0].ast) != NULL
                         ? (yyvsp[0].ast)
                         : pkl_ast_make_identifier (pkl_parser->ast, ""));


                    dummy = pkl_ast_make_integer (pkl_parser->ast, 0);
                    PKL_AST_TYPE (dummy) = ASTREF ((yyvsp[-1].ast));
                    decl = pkl_ast_make_decl (pkl_parser->ast,
                                              PKL_AST_DECL_KIND_VAR,
                                              identifier, dummy,
                                              NULL /* source */);
                    PKL_AST_DECL_STRUCT_FIELD_P (decl) = 1;
                    PKL_AST_LOC (decl) = (yyloc);

                    if (!pkl_env_register (pkl_parser->env,
                                           PKL_ENV_NS_MAIN,
                                           PKL_AST_IDENTIFIER_POINTER (identifier),
                                           decl))
                      {
                        pkl_error (pkl_parser->compiler, pkl_parser->ast, (yylsp[0]),
                                   "duplicated struct element '%s'",
                                   PKL_AST_IDENTIFIER_POINTER ((yyvsp[0].ast)));
                        YYERROR;
                      }

                    if (identifier)
                      {
                        identifier = ASTREF (identifier);
                        pkl_ast_node_free (identifier);
                      }
                  }
#line 6502 "pkl-tab.c"
    break;

  case 166:
#line 1688 "pkl-tab.y"
                  {
                    pkl_ast_node constraint = (yyvsp[-3].astpair)[0];
                    pkl_ast_node initializer = (yyvsp[-3].astpair)[1];

                    if (initializer)
                      {
                        pkl_ast_node field_decl, field_var;
                        int back, over;

                        /* We need a field name.  */
                        if ((yyvsp[-5].ast) == NULL)
                          {
                            pkl_error (pkl_parser->compiler, pkl_parser->ast, (yyloc),
                                       "no initializer allowed in anonymous field");
                            YYERROR;
                          }

                        /* Build a constraint derived from the
                           initializer if a constraint has not been
                           specified.  */
                        if (constraint == NULL)
                          {
                            field_decl = pkl_env_lookup (pkl_parser->env,
                                                         PKL_ENV_NS_MAIN,
                                                         PKL_AST_IDENTIFIER_POINTER ((yyvsp[-5].ast)),
                                                         &back, &over);
                            assert (field_decl);

                            field_var = pkl_ast_make_var (pkl_parser->ast,
                                                          (yyvsp[-5].ast),
                                                          field_decl,
                                                          back, over);
                            PKL_AST_LOC (field_var) = PKL_AST_LOC (initializer);

                            constraint = pkl_ast_make_binary_exp (pkl_parser->ast,
                                                                  PKL_AST_OP_EQ,
                                                                  field_var,
                                                                  initializer);
                            PKL_AST_LOC (constraint) = PKL_AST_LOC (initializer);
                          }
                      }

                    (yyval.ast) = pkl_ast_make_struct_type_field (pkl_parser->ast, (yyvsp[-5].ast), (yyvsp[-6].ast),
                                                         constraint, initializer,
                                                         (yyvsp[-2].ast), (yyvsp[-7].integer), (yyvsp[-1].ast));
                    PKL_AST_LOC ((yyval.ast)) = (yyloc);

                    /* If endianness is empty, bison includes the
                       blank characters before the type field as if
                       they were part of this rule.  Therefore the
                       location should be adjusted here.  */
                    if ((yyvsp[-7].integer) == PKL_AST_ENDIAN_DFL)
                      {
                        PKL_AST_LOC ((yyval.ast)).first_line = (yylsp[-6]).first_line;
                        PKL_AST_LOC ((yyval.ast)).first_column = (yylsp[-6]).first_column;
                      }

                    if ((yyvsp[-5].ast) != NULL)
                      {
                        PKL_AST_LOC ((yyvsp[-5].ast)) = (yylsp[-5]);
                        PKL_AST_TYPE ((yyvsp[-5].ast)) = pkl_ast_make_string_type (pkl_parser->ast);
                        PKL_AST_TYPE ((yyvsp[-5].ast)) = ASTREF (PKL_AST_TYPE ((yyvsp[-5].ast)));
                        PKL_AST_LOC (PKL_AST_TYPE ((yyvsp[-5].ast))) = (yylsp[-5]);
                      }
                  }
#line 6572 "pkl-tab.c"
    break;

  case 167:
#line 1756 "pkl-tab.y"
                        { (yyval.ast) = NULL; }
#line 6578 "pkl-tab.c"
    break;

  case 168:
#line 1757 "pkl-tab.y"
                            { (yyval.ast) = (yyvsp[0].ast); }
#line 6584 "pkl-tab.c"
    break;

  case 169:
#line 1762 "pkl-tab.y"
                {
                  (yyval.ast) = NULL;
                }
#line 6592 "pkl-tab.c"
    break;

  case 170:
#line 1766 "pkl-tab.y"
                {
                  (yyval.ast) = (yyvsp[0].ast);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 6601 "pkl-tab.c"
    break;

  case 171:
#line 1774 "pkl-tab.y"
                {
                  (yyval.astpair)[0] = NULL;
                  (yyval.astpair)[1] = NULL;
                }
#line 6610 "pkl-tab.c"
    break;

  case 172:
#line 1779 "pkl-tab.y"
                {
                  (yyval.astpair)[0] = (yyvsp[0].ast);
                  (yyval.astpair)[1] = NULL;
                  PKL_AST_LOC ((yyval.astpair)[0]) = (yyloc);
                }
#line 6620 "pkl-tab.c"
    break;

  case 173:
#line 1785 "pkl-tab.y"
                {
                  (yyval.astpair)[0] = NULL;
                  (yyval.astpair)[1] = (yyvsp[0].ast);
                  PKL_AST_LOC ((yyval.astpair)[1]) = (yyloc);
                }
#line 6630 "pkl-tab.c"
    break;

  case 174:
#line 1791 "pkl-tab.y"
                {
                  (yyval.astpair)[0] = (yyvsp[0].ast);
                  (yyval.astpair)[1] = (yyvsp[-2].ast);

                  PKL_AST_LOC ((yyval.astpair)[0]) = (yylsp[0]);
                  PKL_AST_LOC ((yyval.astpair)[1]) = (yylsp[-2]);
                }
#line 6642 "pkl-tab.c"
    break;

  case 175:
#line 1799 "pkl-tab.y"
                {
                  (yyval.astpair)[0] = (yyvsp[-2].ast);
                  (yyval.astpair)[1] = (yyvsp[0].ast);

                  PKL_AST_LOC ((yyval.astpair)[0]) = (yylsp[-2]);
                  PKL_AST_LOC ((yyval.astpair)[1]) = (yylsp[0]);
                }
#line 6654 "pkl-tab.c"
    break;

  case 176:
#line 1810 "pkl-tab.y"
                {
                  (yyval.ast) = NULL;
                }
#line 6662 "pkl-tab.c"
    break;

  case 177:
#line 1814 "pkl-tab.y"
                {
                  (yyval.ast) = (yyvsp[0].ast);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 6671 "pkl-tab.c"
    break;

  case 178:
#line 1825 "pkl-tab.y"
                               { (yyval.ast) = (yyvsp[0].ast); }
#line 6677 "pkl-tab.c"
    break;

  case 179:
#line 1826 "pkl-tab.y"
                               { (yyval.ast) = (yyvsp[0].ast); }
#line 6683 "pkl-tab.c"
    break;

  case 180:
#line 1827 "pkl-tab.y"
                               { (yyval.ast) = (yyvsp[0].ast); }
#line 6689 "pkl-tab.c"
    break;

  case 181:
#line 1832 "pkl-tab.y"
                {
                  /* In order to allow for the function to be called
                     from within itself (recursive calls) we should
                     register a partial declaration in the
                     compile-time environment before processing the
                     `function_specifier' below.  */

                  (yyval.ast) = pkl_ast_make_decl (pkl_parser->ast,
                                               PKL_AST_DECL_KIND_FUNC, (yyvsp[0].ast),
                                               NULL /* initial */,
                                               pkl_parser->filename);
                  PKL_AST_LOC ((yyvsp[0].ast)) = (yylsp[0]);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);

                  if (!pkl_env_register (pkl_parser->env,
                                         PKL_ENV_NS_MAIN,
                                         PKL_AST_IDENTIFIER_POINTER ((yyvsp[0].ast)),
                                         (yyval.ast)))
                    {
                      pkl_error (pkl_parser->compiler, pkl_parser->ast, (yylsp[0]),
                                 "function or variable `%s' already defined",
                                 PKL_AST_IDENTIFIER_POINTER ((yyvsp[0].ast)));
                      YYERROR;
                    }

                  /* function_specifier needs to know whether we are
                     in a function declaration or a method
                     declaration.  */
                  pkl_parser->in_method_decl_p = ((yyvsp[-1].integer) == IS_METHOD);
                }
#line 6724 "pkl-tab.c"
    break;

  case 182:
#line 1863 "pkl-tab.y"
                {
                  /* Complete the declaration registered above with
                     it's initial value, which is the specifier of the
                     function being defined.  */
                  PKL_AST_DECL_INITIAL ((yyvsp[-2].ast))
                    = ASTREF ((yyvsp[0].ast));
                  (yyval.ast) = (yyvsp[-2].ast);

                  /* If the reference counting of the declaration is
                     bigger than 1, this means there are recursive
                     calls in the function body.  Reset the refcount
                     to 1, since these references are weak.  */
                  if (PKL_AST_REFCOUNT ((yyvsp[-2].ast)) > 1)
                    PKL_AST_REFCOUNT ((yyvsp[-2].ast)) = 1;

                  /* Annotate the contained RETURN statements with
                     their function and their lexical nest level
                     within the function.  */
                  pkl_ast_finish_returns ((yyvsp[0].ast));

                  /* Annotate the function to be a method whenever
                     appropriate.  */
                  if ((yyvsp[-4].integer) == IS_METHOD)
                    PKL_AST_FUNC_METHOD_P ((yyvsp[0].ast)) = 1;

                  /* XXX: move to trans1.  */
                  PKL_AST_FUNC_NAME ((yyvsp[0].ast))
                    = xstrdup (PKL_AST_IDENTIFIER_POINTER ((yyvsp[-3].ast)));

                  pkl_parser->in_method_decl_p = 0;
                }
#line 6760 "pkl-tab.c"
    break;

  case 183:
#line 1894 "pkl-tab.y"
                                 { (yyval.ast) = (yyvsp[-1].ast); }
#line 6766 "pkl-tab.c"
    break;

  case 184:
#line 1898 "pkl-tab.y"
                               { (yyval.integer) = IS_DEFUN; }
#line 6772 "pkl-tab.c"
    break;

  case 185:
#line 1899 "pkl-tab.y"
                        { (yyval.integer) = IS_METHOD; }
#line 6778 "pkl-tab.c"
    break;

  case 187:
#line 1905 "pkl-tab.y"
          { (yyval.ast) = pkl_ast_chainon ((yyvsp[-2].ast), (yyvsp[0].ast)); }
#line 6784 "pkl-tab.c"
    break;

  case 188:
#line 1910 "pkl-tab.y"
            {
                (yyval.ast) = pkl_ast_make_decl (pkl_parser->ast,
                                        PKL_AST_DECL_KIND_VAR, (yyvsp[-2].ast), (yyvsp[0].ast),
                                        pkl_parser->filename);
                PKL_AST_LOC ((yyvsp[-2].ast)) = (yylsp[-2]);
                PKL_AST_LOC ((yyval.ast)) = (yyloc);

                if (!pkl_env_register (pkl_parser->env,
                                       PKL_ENV_NS_MAIN,
                                       PKL_AST_IDENTIFIER_POINTER ((yyvsp[-2].ast)),
                                       (yyval.ast)))
                  {
                    pkl_error (pkl_parser->compiler, pkl_parser->ast, (yylsp[-2]),
                               "the variable `%s' is already defined",
                               PKL_AST_IDENTIFIER_POINTER ((yyvsp[-2].ast)));
                    YYERROR;
                  }
          }
#line 6807 "pkl-tab.c"
    break;

  case 190:
#line 1933 "pkl-tab.y"
          { (yyval.ast) = pkl_ast_chainon ((yyvsp[-2].ast), (yyvsp[0].ast)); }
#line 6813 "pkl-tab.c"
    break;

  case 191:
#line 1938 "pkl-tab.y"
          {
            (yyval.ast) = pkl_ast_make_decl (pkl_parser->ast,
                                    PKL_AST_DECL_KIND_TYPE, (yyvsp[-2].ast), (yyvsp[0].ast),
                                    pkl_parser->filename);
            PKL_AST_LOC ((yyvsp[-2].ast)) = (yylsp[-2]);
            PKL_AST_LOC ((yyval.ast)) = (yyloc);

            PKL_AST_TYPE_NAME ((yyvsp[0].ast)) = ASTREF ((yyvsp[-2].ast));

            if (!pkl_env_register (pkl_parser->env,
                                   PKL_ENV_NS_MAIN,
                                   PKL_AST_IDENTIFIER_POINTER ((yyvsp[-2].ast)),
                                   (yyval.ast)))
              {
                pkl_error (pkl_parser->compiler, pkl_parser->ast, (yylsp[-2]),
                           "the type `%s' is already defined",
                           PKL_AST_IDENTIFIER_POINTER ((yyvsp[-2].ast)));
                YYERROR;
              }
          }
#line 6838 "pkl-tab.c"
    break;

  case 193:
#line 1963 "pkl-tab.y"
          { (yyval.ast) = pkl_ast_chainon ((yyvsp[-2].ast), (yyvsp[0].ast)); }
#line 6844 "pkl-tab.c"
    break;

  case 194:
#line 1968 "pkl-tab.y"
            {
              /* We need to cast the expression to uint<64> here,
                 instead of pkl-promo, because the installed
                 initializer is used as earlier as in the lexer.  Not
                 pretty.  */
              pkl_ast_node type
                = pkl_ast_make_integral_type (pkl_parser->ast,
                                              64, 0);
              pkl_ast_node cast
                = pkl_ast_make_cast (pkl_parser->ast,
                                     type, (yyvsp[0].ast));

              (yyval.ast) = pkl_ast_make_decl (pkl_parser->ast,
                                      PKL_AST_DECL_KIND_UNIT, (yyvsp[-2].ast), cast,
                                      pkl_parser->filename);

              PKL_AST_LOC (type) = (yylsp[0]);
              PKL_AST_LOC (cast) = (yylsp[0]);
              PKL_AST_LOC ((yyvsp[-2].ast)) = (yylsp[-2]);
              PKL_AST_LOC ((yyval.ast)) = (yyloc);

              if (!pkl_env_register (pkl_parser->env,
                                     PKL_ENV_NS_UNITS,
                                     PKL_AST_IDENTIFIER_POINTER ((yyvsp[-2].ast)),
                                     (yyval.ast)))
                {
                  pkl_error (pkl_parser->compiler, pkl_parser->ast, (yylsp[-2]),
                             "the unit `%s' is already defined",
                             PKL_AST_IDENTIFIER_POINTER ((yyvsp[-2].ast)));
                  YYERROR;
                }
            }
#line 6881 "pkl-tab.c"
    break;

  case 195:
#line 2007 "pkl-tab.y"
            {
              (yyval.ast) = pkl_ast_make_comp_stmt (pkl_parser->ast, NULL);
              PKL_AST_LOC ((yyval.ast)) = (yyloc);

              /* Pop the frame pushed by the `pushlevel' above.  */
              pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);
            }
#line 6893 "pkl-tab.c"
    break;

  case 196:
#line 2015 "pkl-tab.y"
            {
              (yyval.ast) = pkl_ast_make_comp_stmt (pkl_parser->ast, (yyvsp[-1].ast));
              PKL_AST_LOC ((yyval.ast)) = (yyloc);

              /* Pop the frame pushed by the `pushlevel' above.  */
              pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);
            }
#line 6905 "pkl-tab.c"
    break;

  case 197:
#line 2023 "pkl-tab.y"
        {
          (yyval.ast) = pkl_ast_make_builtin (pkl_parser->ast, (yyvsp[0].integer));
          PKL_AST_LOC ((yyval.ast)) = (yyloc);

          /* Pop the frame pushed by the `pushlevel' above.  */
          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);
        }
#line 6917 "pkl-tab.c"
    break;

  case 198:
#line 2033 "pkl-tab.y"
                                { (yyval.integer) = PKL_AST_BUILTIN_RAND; }
#line 6923 "pkl-tab.c"
    break;

  case 199:
#line 2034 "pkl-tab.y"
                                { (yyval.integer) = PKL_AST_BUILTIN_GET_ENDIAN; }
#line 6929 "pkl-tab.c"
    break;

  case 200:
#line 2035 "pkl-tab.y"
                                { (yyval.integer) = PKL_AST_BUILTIN_SET_ENDIAN; }
#line 6935 "pkl-tab.c"
    break;

  case 201:
#line 2036 "pkl-tab.y"
                                { (yyval.integer) = PKL_AST_BUILTIN_GET_IOS; }
#line 6941 "pkl-tab.c"
    break;

  case 202:
#line 2037 "pkl-tab.y"
                                { (yyval.integer) = PKL_AST_BUILTIN_SET_IOS; }
#line 6947 "pkl-tab.c"
    break;

  case 203:
#line 2038 "pkl-tab.y"
                                { (yyval.integer) = PKL_AST_BUILTIN_OPEN; }
#line 6953 "pkl-tab.c"
    break;

  case 204:
#line 2039 "pkl-tab.y"
                                { (yyval.integer) = PKL_AST_BUILTIN_CLOSE; }
#line 6959 "pkl-tab.c"
    break;

  case 205:
#line 2040 "pkl-tab.y"
                                { (yyval.integer) = PKL_AST_BUILTIN_IOSIZE; }
#line 6965 "pkl-tab.c"
    break;

  case 206:
#line 2041 "pkl-tab.y"
                                { (yyval.integer) = PKL_AST_BUILTIN_GETENV; }
#line 6971 "pkl-tab.c"
    break;

  case 207:
#line 2042 "pkl-tab.y"
                                { (yyval.integer) = PKL_AST_BUILTIN_FORGET; }
#line 6977 "pkl-tab.c"
    break;

  case 208:
#line 2043 "pkl-tab.y"
                                { (yyval.integer) = PKL_AST_BUILTIN_GET_TIME; }
#line 6983 "pkl-tab.c"
    break;

  case 209:
#line 2044 "pkl-tab.y"
                                { (yyval.integer) = PKL_AST_BUILTIN_STRACE; }
#line 6989 "pkl-tab.c"
    break;

  case 210:
#line 2045 "pkl-tab.y"
                                 { (yyval.integer) = PKL_AST_BUILTIN_TERM_GET_COLOR; }
#line 6995 "pkl-tab.c"
    break;

  case 211:
#line 2046 "pkl-tab.y"
                                 { (yyval.integer) = PKL_AST_BUILTIN_TERM_SET_COLOR; }
#line 7001 "pkl-tab.c"
    break;

  case 212:
#line 2047 "pkl-tab.y"
                                   { (yyval.integer) = PKL_AST_BUILTIN_TERM_GET_BGCOLOR; }
#line 7007 "pkl-tab.c"
    break;

  case 213:
#line 2048 "pkl-tab.y"
                                   { (yyval.integer) = PKL_AST_BUILTIN_TERM_SET_BGCOLOR; }
#line 7013 "pkl-tab.c"
    break;

  case 214:
#line 2049 "pkl-tab.y"
                                   { (yyval.integer) = PKL_AST_BUILTIN_TERM_BEGIN_CLASS; }
#line 7019 "pkl-tab.c"
    break;

  case 215:
#line 2050 "pkl-tab.y"
                                 { (yyval.integer) = PKL_AST_BUILTIN_TERM_END_CLASS; }
#line 7025 "pkl-tab.c"
    break;

  case 216:
#line 2051 "pkl-tab.y"
                                       { (yyval.integer) = PKL_AST_BUILTIN_TERM_BEGIN_HYPERLINK; }
#line 7031 "pkl-tab.c"
    break;

  case 217:
#line 2052 "pkl-tab.y"
                                     { (yyval.integer) = PKL_AST_BUILTIN_TERM_END_HYPERLINK; }
#line 7037 "pkl-tab.c"
    break;

  case 219:
#line 2058 "pkl-tab.y"
                  { (yyval.ast) = pkl_ast_chainon ((yyvsp[-1].ast), (yyvsp[0].ast)); }
#line 7043 "pkl-tab.c"
    break;

  case 221:
#line 2061 "pkl-tab.y"
                  { (yyval.ast) = pkl_ast_chainon ((yyvsp[-1].ast), (yyvsp[0].ast)); }
#line 7049 "pkl-tab.c"
    break;

  case 222:
#line 2065 "pkl-tab.y"
               { (yyval.integer) = PKL_AST_OP_POW; }
#line 7055 "pkl-tab.c"
    break;

  case 223:
#line 2066 "pkl-tab.y"
               { (yyval.integer) = PKL_AST_OP_MUL; }
#line 7061 "pkl-tab.c"
    break;

  case 224:
#line 2067 "pkl-tab.y"
               { (yyval.integer) = PKL_AST_OP_DIV; }
#line 7067 "pkl-tab.c"
    break;

  case 225:
#line 2068 "pkl-tab.y"
               { (yyval.integer) = PKL_AST_OP_MOD; }
#line 7073 "pkl-tab.c"
    break;

  case 226:
#line 2069 "pkl-tab.y"
               { (yyval.integer) = PKL_AST_OP_ADD; }
#line 7079 "pkl-tab.c"
    break;

  case 227:
#line 2070 "pkl-tab.y"
               { (yyval.integer) = PKL_AST_OP_SUB; }
#line 7085 "pkl-tab.c"
    break;

  case 228:
#line 2071 "pkl-tab.y"
               { (yyval.integer) = PKL_AST_OP_SL; }
#line 7091 "pkl-tab.c"
    break;

  case 229:
#line 2072 "pkl-tab.y"
               { (yyval.integer) = PKL_AST_OP_SR; }
#line 7097 "pkl-tab.c"
    break;

  case 230:
#line 2073 "pkl-tab.y"
                { (yyval.integer) = PKL_AST_OP_BAND; }
#line 7103 "pkl-tab.c"
    break;

  case 231:
#line 2074 "pkl-tab.y"
               { (yyval.integer) = PKL_AST_OP_IOR; }
#line 7109 "pkl-tab.c"
    break;

  case 232:
#line 2075 "pkl-tab.y"
               { (yyval.integer) = PKL_AST_OP_XOR; }
#line 7115 "pkl-tab.c"
    break;

  case 233:
#line 2079 "pkl-tab.y"
                 { (yyval.ast) = NULL; }
#line 7121 "pkl-tab.c"
    break;

  case 235:
#line 2082 "pkl-tab.y"
                 { (yyval.ast) = pkl_ast_chainon ((yyvsp[-2].ast), (yyvsp[0].ast)); }
#line 7127 "pkl-tab.c"
    break;

  case 236:
#line 2087 "pkl-tab.y"
                  {
                  (yyval.ast) = pkl_ast_make_ass_stmt (pkl_parser->ast,
                                              (yyvsp[-2].ast), (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 7137 "pkl-tab.c"
    break;

  case 237:
#line 2093 "pkl-tab.y"
                {
                  pkl_ast_node exp
                    = pkl_ast_make_binary_exp (pkl_parser->ast,
                                               (yyvsp[-1].integer), (yyvsp[-2].ast), (yyvsp[0].ast));

                  (yyval.ast) = pkl_ast_make_ass_stmt (pkl_parser->ast,
                                              (yyvsp[-2].ast), exp);
                  PKL_AST_LOC (exp) = (yyloc);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 7152 "pkl-tab.c"
    break;

  case 238:
#line 2104 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_ass_stmt (pkl_parser->ast,
                                              (yyvsp[-2].ast), (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 7162 "pkl-tab.c"
    break;

  case 239:
#line 2110 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_ass_stmt (pkl_parser->ast,
                                              (yyvsp[-2].ast), (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 7172 "pkl-tab.c"
    break;

  case 240:
#line 2116 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_exp_stmt (pkl_parser->ast,
                                              (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 7182 "pkl-tab.c"
    break;

  case 241:
#line 2122 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_print_stmt (pkl_parser->ast,
                                                (yyvsp[-2].ast), (yyvsp[-1].ast));
                  PKL_AST_LOC ((yyvsp[-2].ast)) = (yylsp[-2]);
                  if (PKL_AST_TYPE ((yyvsp[-2].ast)))
                    PKL_AST_LOC (PKL_AST_TYPE ((yyvsp[-2].ast))) = (yylsp[-2]);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 7195 "pkl-tab.c"
    break;

  case 242:
#line 2131 "pkl-tab.y"
                {
                  if (((yyval.ast) = pkl_make_assertion (pkl_parser, (yyvsp[-1].ast), NULL, (yyloc)))
                      == NULL)
                    YYERROR;
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 7206 "pkl-tab.c"
    break;

  case 243:
#line 2138 "pkl-tab.y"
                {
                  if (((yyval.ast) = pkl_make_assertion (pkl_parser, (yyvsp[-3].ast), (yyvsp[-1].ast), (yyloc)))
                      == NULL)
                    YYERROR;
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 7217 "pkl-tab.c"
    break;

  case 244:
#line 2145 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_exp_stmt (pkl_parser->ast,
                                              (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 7227 "pkl-tab.c"
    break;

  case 246:
#line 2155 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_null_stmt (pkl_parser->ast);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 7236 "pkl-tab.c"
    break;

  case 247:
#line 2160 "pkl-tab.y"
                {
                  (yyval.ast) = (yyvsp[-1].ast);
                }
#line 7244 "pkl-tab.c"
    break;

  case 248:
#line 2164 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_if_stmt (pkl_parser->ast,
                                             (yyvsp[-2].ast), (yyvsp[0].ast), NULL);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 7254 "pkl-tab.c"
    break;

  case 249:
#line 2170 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_if_stmt (pkl_parser->ast,
                                             (yyvsp[-4].ast), (yyvsp[-2].ast), (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 7264 "pkl-tab.c"
    break;

  case 250:
#line 2176 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_loop_stmt (pkl_parser->ast,
                                               PKL_AST_LOOP_STMT_KIND_WHILE,
                                               NULL, /* iterator */
                                               (yyvsp[-2].ast),   /* condition */
                                               NULL, /* head */
                                               NULL, /* tail */
                                               (yyvsp[0].ast));  /* body */
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);

                  /* Annotate the contained BREAK and CONTINUE
                     statements with their lexical level within this
                     loop.  */
                  pkl_ast_finish_breaks ((yyval.ast), (yyvsp[0].ast));
                }
#line 7284 "pkl-tab.c"
    break;

  case 251:
#line 2192 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_loop_stmt (pkl_parser->ast,
                                               PKL_AST_LOOP_STMT_KIND_FOR,
                                               NULL, /* iterator */
                                               (yyvsp[-4].ast),   /* condition */
                                               (yyvsp[-6].ast),   /* head */
                                               (yyvsp[-2].ast),   /* tail */
                                               (yyvsp[0].ast)); /* body */
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);

                  /* Annotate the contained BREAK and CONTINUE
                     statements with their lexical level within this
                     loop.  */
                  pkl_ast_finish_breaks ((yyval.ast), (yyvsp[0].ast));

                  /* Pop the frame introduced by `pushlevel'
                     above.  */
                  pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);
                }
#line 7308 "pkl-tab.c"
    break;

  case 252:
#line 2212 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_loop_stmt (pkl_parser->ast,
                                               PKL_AST_LOOP_STMT_KIND_FOR,
                                               NULL, /* iterator */
                                               (yyvsp[-4].ast),   /* condition */
                                               NULL, /* head */
                                               (yyvsp[-2].ast),   /* tail */
                                               (yyvsp[0].ast));  /* body */

                  /* Annotate the contained BREAK and CONTINUE
                     statements with their lexical level within this
                     loop.  */
                  pkl_ast_finish_breaks ((yyval.ast), (yyvsp[0].ast));

                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 7329 "pkl-tab.c"
    break;

  case 253:
#line 2229 "pkl-tab.y"
                {
                  /* Push a new lexical level and register a variable
                     with name IDENTIFIER.  Note that the variable is
                     created with a dummy INITIAL, as there is none.  */

                  pkl_ast_node dummy = pkl_ast_make_integer (pkl_parser->ast,
                                                             0);
                  PKL_AST_LOC (dummy) = (yylsp[-3]);

                  (yyval.ast) = pkl_ast_make_decl (pkl_parser->ast,
                                               PKL_AST_DECL_KIND_VAR,
                                               (yyvsp[-3].ast),
                                               dummy,
                                               pkl_parser->filename);
                  PKL_AST_LOC ((yyval.ast)) = (yylsp[-3]);

                  if (!pkl_env_register (pkl_parser->env,
                                         PKL_ENV_NS_MAIN,
                                         PKL_AST_IDENTIFIER_POINTER ((yyvsp[-3].ast)),
                                         (yyval.ast)))
                    /* This should never happen.  */
                    assert (0);
                }
#line 7357 "pkl-tab.c"
    break;

  case 254:
#line 2253 "pkl-tab.y"
                {
                  pkl_ast_node iterator
                    = pkl_ast_make_loop_stmt_iterator (pkl_parser->ast,
                                                       (yyvsp[-2].ast), /* decl */
                                                       (yyvsp[-4].ast)); /* container */
                  PKL_AST_LOC (iterator) = (yyloc);

                  (yyval.ast) = pkl_ast_make_loop_stmt (pkl_parser->ast,
                                               PKL_AST_LOOP_STMT_KIND_FOR_IN,
                                               iterator,
                                               NULL, /* condition */
                                               NULL, /* head */
                                               NULL, /* tail */
                                               (yyvsp[0].ast));  /* body */
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);

                  /* Free the identifier.  */
                  (yyvsp[-6].ast) = ASTREF ((yyvsp[-6].ast)); pkl_ast_node_free ((yyvsp[-6].ast));

                  /* Annotate the contained BREAK and CONTINUE
                     statements with their lexical level within this
                     loop.  */
                  pkl_ast_finish_breaks ((yyval.ast), (yyvsp[0].ast));

                  /* Pop the frame introduced by `pushlevel'
                     above.  */
                  pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);
                }
#line 7390 "pkl-tab.c"
    break;

  case 255:
#line 2282 "pkl-tab.y"
                {
                  /* XXX: avoid code replication here.  */

                  /* Push a new lexical level and register a variable
                     with name IDENTIFIER.  Note that the variable is
                     created with a dummy INITIAL, as there is none.  */

                  pkl_ast_node dummy = pkl_ast_make_integer (pkl_parser->ast,
                                                             0);
                  PKL_AST_LOC (dummy) = (yylsp[-3]);

                  (yyval.ast) = pkl_ast_make_decl (pkl_parser->ast,
                                               PKL_AST_DECL_KIND_VAR,
                                               (yyvsp[-3].ast),
                                               dummy,
                                               pkl_parser->filename);
                  PKL_AST_LOC ((yyval.ast)) = (yylsp[-3]);

                  if (!pkl_env_register (pkl_parser->env,
                                         PKL_ENV_NS_MAIN,
                                         PKL_AST_IDENTIFIER_POINTER ((yyvsp[-3].ast)),
                                         (yyval.ast)))
                    /* This should never happen.  */
                    assert (0);
                }
#line 7420 "pkl-tab.c"
    break;

  case 256:
#line 2308 "pkl-tab.y"
                {
                  pkl_ast_node iterator
                    = pkl_ast_make_loop_stmt_iterator (pkl_parser->ast,
                                                       (yyvsp[-4].ast), /* decl */
                                                       (yyvsp[-6].ast)); /* container */
                  PKL_AST_LOC (iterator) = (yyloc);

                  (yyval.ast) = pkl_ast_make_loop_stmt (pkl_parser->ast,
                                               PKL_AST_LOOP_STMT_KIND_FOR_IN,
                                               iterator,
                                               (yyvsp[-2].ast), /* condition */
                                               NULL, /* head */
                                               NULL, /* tail */
                                               (yyvsp[0].ast)); /* body */
                  PKL_AST_LOC ((yyvsp[-8].ast)) = (yylsp[-8]);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);

                  /* Annotate the contained BREAK and CONTINUE
                     statements with their lexical level within this
                     loop.  */
                  pkl_ast_finish_breaks ((yyval.ast), (yyvsp[0].ast));

                  /* Pop the frame introduced by `pushlevel'
                     above.  */
                  pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);
                }
#line 7451 "pkl-tab.c"
    break;

  case 257:
#line 2335 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_break_stmt (pkl_parser->ast);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 7460 "pkl-tab.c"
    break;

  case 258:
#line 2340 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_continue_stmt (pkl_parser->ast);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 7469 "pkl-tab.c"
    break;

  case 259:
#line 2345 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_return_stmt (pkl_parser->ast,
                                                 NULL);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 7479 "pkl-tab.c"
    break;

  case 260:
#line 2351 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_return_stmt (pkl_parser->ast,
                                                 (yyvsp[-1].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 7489 "pkl-tab.c"
    break;

  case 261:
#line 2357 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_try_catch_stmt (pkl_parser->ast,
                                                    (yyvsp[-2].ast), (yyvsp[0].ast), NULL, NULL);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 7499 "pkl-tab.c"
    break;

  case 262:
#line 2363 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_try_catch_stmt (pkl_parser->ast,
                                                    (yyvsp[-4].ast), (yyvsp[0].ast), NULL, (yyvsp[-1].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 7509 "pkl-tab.c"
    break;

  case 263:
#line 2369 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_try_catch_stmt (pkl_parser->ast,
                                                    (yyvsp[-6].ast), (yyvsp[0].ast), (yyvsp[-2].ast), NULL);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);

                  /* Pop the frame introduced by `pushlevel'
                     above.  */
                  pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);
                }
#line 7523 "pkl-tab.c"
    break;

  case 264:
#line 2379 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_try_until_stmt (pkl_parser->ast,
                                                    (yyvsp[-3].ast), (yyvsp[-1].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);

                  /* Annotate the contained BREAK and CONTINUE
                     statements with their lexical level within this
                     loop.  */
                  pkl_ast_finish_breaks ((yyval.ast), (yyvsp[-3].ast));
                }
#line 7538 "pkl-tab.c"
    break;

  case 265:
#line 2390 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_raise_stmt (pkl_parser->ast,
                                                NULL);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 7548 "pkl-tab.c"
    break;

  case 266:
#line 2396 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_raise_stmt (pkl_parser->ast,
                                                (yyvsp[-1].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 7558 "pkl-tab.c"
    break;

  case 267:
#line 2402 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_print_stmt (pkl_parser->ast,
                                                NULL /* fmt */, (yyvsp[-1].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 7568 "pkl-tab.c"
    break;

  case 268:
#line 2408 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_print_stmt (pkl_parser->ast,
                                                (yyvsp[-2].ast), (yyvsp[-1].ast));
                  PKL_AST_LOC ((yyvsp[-2].ast)) = (yylsp[-2]);
                  if (PKL_AST_TYPE ((yyvsp[-2].ast)))
                    PKL_AST_LOC (PKL_AST_TYPE ((yyvsp[-2].ast))) = (yylsp[-2]);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 7581 "pkl-tab.c"
    break;

  case 269:
#line 2420 "pkl-tab.y"
                {
                  (yyval.ast) = NULL;
                }
#line 7589 "pkl-tab.c"
    break;

  case 270:
#line 2424 "pkl-tab.y"
                {
                  pkl_ast_node arg
                    = pkl_ast_make_print_stmt_arg (pkl_parser->ast, (yyvsp[0].ast));
                  PKL_AST_LOC (arg) = (yylsp[0]);

                  (yyval.ast) = pkl_ast_chainon ((yyvsp[-2].ast), arg);
                }
#line 7601 "pkl-tab.c"
    break;

  case 271:
#line 2435 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_funcall (pkl_parser->ast,
                                             (yyvsp[-1].ast), (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 7611 "pkl-tab.c"
    break;

  case 273:
#line 2445 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_chainon ((yyvsp[-1].ast), (yyvsp[0].ast));
                }
#line 7619 "pkl-tab.c"
    break;

  case 274:
#line 2452 "pkl-tab.y"
                  {
                  (yyval.ast) = pkl_ast_make_funcall_arg (pkl_parser->ast,
                                                 (yyvsp[0].ast), (yyvsp[-1].ast));
                  PKL_AST_LOC ((yyvsp[-1].ast)) = (yylsp[-1]);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 7630 "pkl-tab.c"
    break;


#line 7634 "pkl-tab.c"

        default: break;
      }
    if (yychar_backup != yychar)
      YY_LAC_DISCARD ("yychar change");
  }
  /* User semantic actions sometimes alter yychar, and that requires
     that yytoken be updated with the new translation.  We take the
     approach of translating immediately before every use of yytoken.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     YYABORT, YYACCEPT, or YYERROR immediately after altering yychar or
     if it invokes YYBACKUP.  In the case of YYABORT or YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of YYERROR or YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  YY_SYMBOL_PRINT ("-> $$ =", YY_CAST (yysymbol_kind_t, yyr1[yyn]), &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;

  *++yyvsp = yyval;
  *++yylsp = yyloc;

  /* Now 'shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */
  {
    const int yylhs = yyr1[yyn] - YYNTOKENS;
    const int yyi = yypgoto[yylhs] + *yyssp;
    yystate = (0 <= yyi && yyi <= YYLAST && yycheck[yyi] == *yyssp
               ? yytable[yyi]
               : yydefgoto[yylhs]);
  }

  goto yynewstate;


/*--------------------------------------.
| yyerrlab -- here on detecting error.  |
`--------------------------------------*/
yyerrlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  yytoken = yychar == PKL_TAB_EMPTY ? YYSYMBOL_YYEMPTY : YYTRANSLATE (yychar);
  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
      {
        yypcontext_t yyctx
          = {yyssp, yyesa, &yyes, &yyes_capacity, yytoken, &yylloc};
        if (yychar != PKL_TAB_EMPTY)
          YY_LAC_ESTABLISH;
        if (yyreport_syntax_error (&yyctx, pkl_parser) == 2)
          goto yyexhaustedlab;
      }
    }

  yyerror_range[1] = yylloc;
  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
         error, discard it.  */

      if (yychar <= PKL_TAB_EOF)
        {
          /* Return failure if at end of input.  */
          if (yychar == PKL_TAB_EOF)
            YYABORT;
        }
      else
        {
          yydestruct ("Error: discarding",
                      yytoken, &yylval, &yylloc, pkl_parser);
          yychar = PKL_TAB_EMPTY;
        }
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:
  /* Pacify compilers when the user code never invokes YYERROR and the
     label yyerrorlab therefore never appears in user code.  */
  if (0)
    YYERROR;

  /* Do not reclaim the symbols of the rule whose action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;      /* Each real token shifted decrements this.  */

  /* Pop stack until we find a state that shifts the error token.  */
  for (;;)
    {
      yyn = yypact[yystate];
      if (!yypact_value_is_default (yyn))
        {
          yyn += YYSYMBOL_YYerror;
          if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYSYMBOL_YYerror)
            {
              yyn = yytable[yyn];
              if (0 < yyn)
                break;
            }
        }

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
        YYABORT;

      yyerror_range[1] = *yylsp;
      yydestruct ("Error: popping",
                  YY_ACCESSING_SYMBOL (yystate), yyvsp, yylsp, pkl_parser);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  /* If the stack popping above didn't lose the initial context for the
     current lookahead token, the shift below will for sure.  */
  YY_LAC_DISCARD ("error recovery");

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END

  yyerror_range[2] = yylloc;
  ++yylsp;
  YYLLOC_DEFAULT (*yylsp, yyerror_range, 2);

  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", YY_ACCESSING_SYMBOL (yyn), yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;


/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;


#if 1
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (&yylloc, pkl_parser, YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif


/*-----------------------------------------------------.
| yyreturn -- parsing is finished, return the result.  |
`-----------------------------------------------------*/
yyreturn:
  if (yychar != PKL_TAB_EMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      yytoken = YYTRANSLATE (yychar);
      yydestruct ("Cleanup: discarding lookahead",
                  yytoken, &yylval, &yylloc, pkl_parser);
    }
  /* Do not reclaim the symbols of the rule whose action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
                  YY_ACCESSING_SYMBOL (+*yyssp), yyvsp, yylsp, pkl_parser);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
  if (yyes != yyesa)
    YYSTACK_FREE (yyes);

  return yyresult;
}

#line 2491 "pkl-tab.y"


/* Handle syntax errors.  */

int
yyreport_syntax_error (const yypcontext_t *ctx,
                       struct pkl_parser *pkl_parser)
{
  int res = 0;
  char *errmsg;
  yysymbol_kind_t lookahead = yypcontext_token (ctx);

  errmsg = strdup ("syntax error");
  if (!errmsg)
    return YYENOMEM;

  /* if the unexpected token is alien, then report
     pkl_parser->alien_err_msg.  */
  if (lookahead == YYSYMBOL_ALIEN)
    {
      pkl_tab_error (yypcontext_location (ctx),
                     pkl_parser,
                     pkl_parser->alien_errmsg);
      free (pkl_parser->alien_errmsg);
      pkl_parser->alien_errmsg = NULL;
    }
  else
    {
      /* report tokens expected at this point.  */
      yysymbol_kind_t expected[YYNTOKENS];
      int nexpected = yypcontext_expected_tokens (ctx, expected, YYNTOKENS);

      if (nexpected < 0)
        /* forward errors to yyparse.  */
        res = nexpected;
      else
        {
          /* XXX use expected?  */
#if 0
          int i;

          for (i = 0; i < nexpected; ++i)
            {
              char *tmp = pk_str_concat (errmsg,
                                         i == 0 ? ": expected " : " or ",
                                         yysymbol_name (expected[i]),
                                         NULL);
              free (errmsg);
              errmsg = tmp;
            }
#endif
          /* XXX use a table with better names for tokens.  */
          if (lookahead != YYSYMBOL_YYEMPTY)
            {
              char *tmp = pk_str_concat (errmsg,
                                         ": unexpected ",
                                         yysymbol_name (lookahead),
                                         NULL);
              free (errmsg);
              if (!tmp)
                return YYENOMEM;
              errmsg = tmp;
            }

          pkl_tab_error (yypcontext_location (ctx), pkl_parser, errmsg);
          free (errmsg);
        }
    }

  return res;
}
